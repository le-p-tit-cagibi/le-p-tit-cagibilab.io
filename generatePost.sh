#!/bin/bash

for d in content/post/*/ ; do
    if [ -e $d/chapter1.md ] && [ -e $d/header.txt ]
      then
	cat $d/header.txt > $d/index.md
	cat $d/chapter1.md >> $d/index.md
	git add $d/index.md
	git commit -m "MAJ du Post $d"
      else
        echo "Generation Post echouee; fichier manquant"
    fi
done
