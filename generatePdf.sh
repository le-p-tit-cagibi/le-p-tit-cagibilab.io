#!/bin/bash

for d in content/post/*/ ; do
    if [ -e $d/chapter1.md ] && [ -e $d/title.txt ] && [ -e ressources/footer.md ]
      then
	pandoc --pdf-engine=xelatex -V mainfont="Liberation Serif" -o $d/livre.pdf $d/title.txt $d/chapter1.md ressources/footer.md
	git add $d/livre.pdf
	git commit -m "MAJ Pdf $d"
      else
        echo "Generation Pdf echouee: fichier manquant"
    fi
done
