+++
title = "Mille mercis"
date = "2022-04-09"
author = "SB & Rico"
+++


> "Écrire, c'est la seule chose que j'ai toujours aimé faire depuis toute gosse. Je ne dis pas que je le fais bien, mais je ne sais pas faire sans. Mon entourage subit depuis bientôt un demi-siècle poésies, proses, et plus récemment, nouvelles. Je les remercie de leur loyauté, indéfectible, et de leurs encouragements constants. Depuis peu, le cercle de mes lecteurs s'agrandit, j'en suis honorée, et j'espère continuer à mériter leur attention"

*SB*
#
> "[Rico](https://gitlab.com/rrico542) a concocté ceci pour vous sur [Gitlab](https://gitlab.com/) avec pour seuls ingrédients des logiciels comme [Hugo](https://gohugo.io/), [Grammalecte](https://grammalecte.net/) et [Pandoc](https://pandoc.org/) le tout dans des casseroles à la sauce [Debian](https://www.debian.org) arrosée de [papercss](https://github.com/zwbetz-gh/papercss-hugo-theme)"

*Rico*


