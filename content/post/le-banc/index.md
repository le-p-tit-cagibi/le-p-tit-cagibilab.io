---
title: Le banc
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: ""
epub: true
pdf: true
draft: true
---
Le petit corps flasque brinquebale contre ma cuisse tandis que j’avance vers le garage. Derrière la porte, la corde m'attend, le nœud coulant m'attend, et derrière tout ça, le destin. Je prends une grande inspiration, l'air frais m'emplit les poumons : Me reviennent sans pudeur dans mes yeux bordés de larmes amères toutes ces années de souffrance. Je me remémore notre première rencontre et le sentier que j'emprunte m’apparaît telle ma ligne de vie au creux de ma paume ; tortueuse, crevassée, semée de plis en barrant la rectitude comme autant d'embûches.

— Qu'est-ce que tu fiches là à ct'heure ?

Bonne question. Si j'avais pu y répondre, je serai sûrement pas sur ce foutu banc, à regarder bêtement la nuit tomber sur le patelin. J'adressai une mine renfrognée et un grognement animal à mon interlocutrice. Mais la vieille madame Kolski ne se découragea pas pour autant. Elle étreignit son cabas à deux mains (elle ne le quittait jamais) et entreprit d'asseoir son gros popotin sur un coin de banc. Ça lui prit au moins deux minutes, avec des ahanements de bœuf attelé à une charrue. Je crus bon de me pousser à l'autre l'extremité.

— J'te parle mon garçon. J't'aperçois à travers ma vitrine, soir après soir, le cul sur ce vieux banc, la figure amochée, à chialer comme un môme.

Machinalement, je redressai mes lunettes d'aviateur sur mon nez, et rien que le geste fut douloureux, ma pommette gauche m'élançait particulièrement. Je me recroquevillai un peu plus sur moi-même extérieurement et intérieurement. Je n'avais pas conscience d'avoir pleuré, comme quoi le corps fait bien ce qu'il veut, l'hypocrite ! J'essuyai vivement ces traces de ma faiblesse, comme les appelait Tammie, en serrant les dents.

— Tu peux pas continuer comme ça, gamin, cette pouffiasse te mérite pas !

— L'appelez pas comme ça ! Me rebiffai-je.

— Pourtant, ç'en est une, mon grand ! Et une sacrée ! C'est familial, y a pas à dire, j'ai bien connu sa mère, avant qu'une voiture vienne salutairement couper court à sa malfaisance.

— Elle a eu un accident de la circulation ! protestai-je, fait rare, preuve que j'étais déstabilisé par les jacasseries de ma voisine.

— Ou le bon Dieu, dans sa grande mansuétude l'a envoyée là où elle aurait dû crécher dès sa naissance, pt'ête même avant !

Elle hocha la tête d'un air entendu.

— Laissez-moi tranquille, c'est pas vos oignons ! Je jappai comme un chabot pris dans une nasse.

— Un jour, qu'on te ramassera étalé dans un caniveau au milieu des ordures, ça le sera gamin !

Je resserrai mon caban autour de mon corps glacé. La vision de moi dans un caniveau me fichait un sacré coup quand même, la vieille avait frappé fort. Elle soliloqua de plus belle et j'écoutai distraitement, absorbé par mes propres atermoiements mais mon mutisme ne lui fit ni chaud, ni froid.

— T'as pas seize ans, tu passes tes journées dans la rue, tu vis chez une salope qui pourrait presque être ta mère ! Ousse qui sont tes parents ?

Ses petits yeux fureteurs ne me quittaient pas. Elle battait des paupières avec une lenteur calculée, comme un chat sur le point de sauter sur le râble d'un lapereau. Le lapereau, c'était moi, bien entendu.

— Dans le nord.

Ma langue m'avait trahi – garce de vieille fouine – Au diable sa curiosité. Je soupirai mais ne proférai pas un son.

— Ouais. T'es seul, conclua-t-elle, une main sous le menton, l'autre se débattant avec la poche d'un antique tablier d'un bleu passé, le regard toujours fixé sur moi.

Seul. Le mot résonna salement dans le gris de la nuit. Sacrée évidence. Cette vieille bique avait la déduction féroce.

— Je vis en couple avec Tammie, je vous apprends rien, vous êtes toujours à fureter. Fichez-moi la paix ! ajoutai-je, lui concèdant un « s'il vous plaît » du bout des lèvres.

— Mon garçon, si personne te secoue les puces, tu finiras au fond d'un trou, entre quatre planches, ou bien tout comme. Tu disparaîtras de c'te brave monde, pfuiiit ! en un claquement de doigts. Et on te croisera errant comme un chien galeux que tu seras devenu, à l'ancienne gare de tri (là où squattent tous les drogués de la région, on l'appelle la gare du Paradis, humour junky, je suppose).

Entrevoir sa vie future à travers le prisme du grand âge ne me gratifia pas d'un regain d'optimisme.

— Madame Kolski !

Je tentai le ton de reproche, voir si ça stoppait le flux pessimiste et grinçant devenu soudain incontrôlable dans la bouche de la bonne femme. « On vous a jamais dit que vous mêler des affaires d'autrui, c'est pas bien ? »

— Pass'que laisser un môme détruire sa vie avant de l'avoir commencée, c'est bien pt'ête ?

Je passai mes mains dans mes cheveux, puis me levai, ça ne servait à rien de philosopher avec cette vieille chouette.

— Avant de partir, je vais te dire un truc, poursuivit la boutiquière (elle tenait une cordonnerie de l'autre côté de la rue), « La belle Tammie, elle a fait tourner bien des têtes déjà, mais tous ses marlous, aussi stupides qu'ils aient pu être ont fini par prendre leurs jambes, et pas que, à leur cou ! Qu'esse t'attends ? »

Je l'aime.

Bon sang, je faillis répondre ça. Quel abruti. Autant donner un bazooka à la mégère. Mais c'est vrai après tout. J'éprouvai un amour fou pour Tammie. Et c'est d'ailleurs ce que j'étais, dingue d'elle. Mais j'allais pas balancer ça à la cancanière et alimenter la mitraille. Elle comprendrait rien, Kolski, personne ne comprenait d'ailleurs et moi non plus.

— Je vous laisse, Madame Kolski, faut que je rentre.

— À bientôt mon gars.

Certainement.
Mais ça aussi je le gardais pour ma pomme.
Ce n'était hélas pas la première fois qu'elle m'imposait sa présence mais jusque-là, se contentait des banalités d'usage.
Ce soir, la mégère avait passé la vitesse supérieure et décidé d'en découdre, façon sermon : pas le genre câlin, Kolski.
 Bon, après, une chose sûre, elle avait raison sur un point : pas une semaine ne passait sans que j'achève au moins une soirée sur ce banc déglingué, dans le jardin public, à deux pas de sa petite boutique.

Je filai en vitesse avant qu'elle ne continue de m'assommer avec sa morale à deux balles. Le sang afflua dans mon cerveau quand je me levai brusquement, vertige. Vertige que cette drôle de vie. Je m'enfonçai dans la nuit.



Je me souviens d'un autre évènement, le genre qui vous revient trop tard, et dont vous supposez qu'il aurait dû (évidemment) vous alerter qu'il se passait quelque chose de pas net, vous voyez ? Ce genre de réflexion a posteriori.

Quoi qu'il en soit, je fis un détour chez Flore ce jour-là et commandai deux chocolats chauds à emporter, elle adorait le chocolat chaud, Tammie.

C'est juste pour l'amadouer, pauvre lopette, me susurra ma petite voix des plus joviales.
Oui, je pouvais pas le nier, je savais que ça allait chauffer, j'étais en retard et Tammie détestait que je traîne dehors si ce n'était pas elle qui m'y envoyait. Et encore, même en ce cas, je lui rendais des comptes à la moindre anicroche, très à cheval sur les horaires, ma Tammie : spécialement sur les miens. Parfois (souvent, pour être tout à fait honnête, car ce furent mes jours de prédilection pour galoper jusqu'au jardin public) elle se permettait de rentrer au petit matin voire pas du tout.
y a pas à t'en faire, qu'elle minaudait à peine franchi le seuil de chez nous. Et je laissais courir après m'être rongé les sangs toute la nuit.

— T'étais où bordel ?

L'accueil était à la hauteur des prédictions de ma petite voix, les gobelets dans les mains, je reçus cette question comme une gifle.

— Je voulais te rapporter une surprise, mais y avait du monde chez Flore…

— Putain ! tu veux que je prenne dix kilo dans les hanches et que je devienne une truie ou quoi ?

Tammie me choppa l'oreille et pinça sans vergogne ce petit bout de chair de ses longs ongles à la manucure parfaite. J'entrepris de résister mollement, je connaissais le scénario par cœur, moins je protesterai, plus la scène cesserait rapidement.

— Désolé ma chérie, je sais que tu t'inquiètes…

— Te fous pas de ma gueule, en plus. T'as fait ce que je t'ai dit au moins ? Glapit-elle.

— Ouais, la station de lavage, la bagnole est propre, comme neuve, j'ai…

— La moquette était dégueulasse, t'as shampooiné la moquette ?

le silence suivant sa question valait un aveu.
Elle resserra sa prise sur mon oreille, et je me tassai encore un peu. La douleur, vive, traçait un fil brûlant jusqu'à ma tempe.

— Tu fais chier ! Dégage !

Elle me lâcha, me projetant sur la cloison de la cuisine contre laquelle je m’affalai maladroitement, déséquilibré par la poussée soudaine autant que le mépris émanant de cette voix dure me condamnant à la disgrâce. Je résistai à l'envie de rester là, comme une souche inanimée et me redressai, m'aidant de mes bras gourds, me tenant à nouveau face à elle, dans un regain de dignité. Elle me saisit par le col et m'embrassa fougueusement.

— Viens, on va remédier à tes mauvaises habitudes.

— Tammie…

Son beau visage angélique, aux grands yeux de biche couleur noisette m'interrogèrent et un de ses fins sourcils au tracé adorable se leva. La mise en garde, imperceptible pour tout autre que moi, saisit mes tripes, et je corrigeai le tir : instinct de survie.

C'est ça l'amour gamin ? J'entendis l'écho de la voix caustique de Mme Kolski, tandis que je pelotai ma belle amazone en la poussant vers la chambre. Un corps de rêve, une bouche de rêve, un bouton de rose, à peine éclos, toujours affamé, toujours gourmand, une poitrine… 
Un démon, me tança la voix de la vieille au creux de mon esgourde douloureuse.



Etrange de visionner son passé, tel un spectateur curieux devant un film qu'il connait par coeur et dont pourtant, il sursaute à chaque chausse-trappes. Je suis subjugué tandis que je chemine vers cette corde ridicule, subjugué et atterré de m'attarder une dernière fois sur ce garçon, cet homme, ce moi-même devenu esclave d'un monstre, un esclave consentant, qui plus est.



— Une put ! Cette fille a dix ans de plus que toi et c'est une putain, ou je n'y connais rien. Je t'interdis de remonter dans sa voiture, je t'interdis de la fréquenter !

Ma mère me rabrouait durement. Elle gesticulait, et sa mine se colorait, signe chez elle d'une sourde colère.

— Ça va être compliqué môm, c'est mon maitre de stage.

Je temporisai, calme, espérant la raisonner.

— Quitte ton stage !

Solution radicale. Le ton péremptoire ne me laissa guère espérer.

— Tu sais bien que c'est ma dernière année et aussi ma dernière chance d'avoir ce diplôme !

Je m'éfforçais de crier un peu comme un oisillon fond de son nid : J'ai jamais su m'imposer. J'ai jamais su.

— Cette bonne femme devrait aller en tôle de s'amouracher d'un gosse ! Je devrais aller porter plainte !

— C'est ridicule !

— C'est toi qui est parfaitement ridicule ! T'es un abruti de môme ridicule ! Elle va user de toi, sucer ta jeunesse et te recracher sec comme une feuille à l'automne.

— Maman ! Ce que je fais te regarde plus, j'ai grandi ! Ouvre les yeux ! Depuis que papa est mort, j'ai beaucoup changé tu sais.

— Grandi ? Tu n'as pas cessé de faire n'importe quoi depuis que ton pov' père nous a quitté ! Des fugues, des dépressions, si jeune ? T'as donc rien dans la tête ? Et moi t'y penses ? Je vais devenir quoi si tu mènes ta vie comme un bâton de chaise ?

— C'est juste une relation…

— Des coucheries ! Je veux pas la voir chez nous ! Si ça continue, tes escapades, je te rapatrie dans le nord, chez ton oncle, t'as bien compris ?

— Tu la verras pas.

Je m'abstins de lui révéler que Tammie se montrait verbalement bien cruelle envers ma mère, qu'elle traitait copieusement de «grosse vache broutante», ne tarissant pas de surnoms épouvantables qui me peinaient, même si je rigolais avec elle, l'air bravache, en fourrant ma main dans son jean quand elle s'abandonnait sur le sofa de notre joli salon aux teintes rose et crème. Une vraie boite à bonbons et le plus délicieux d'entre eux, devinez !

— Cesses de la voir immédiatement, je suis sérieuse. Sinon… Ma mère bégaya, un léger trémolo dans la voix, « Sinon » m’assena-t-elle, « tu ne remets plus les pieds dans cette maison ! »

Ultimatum.
Je m'y attendais pas. Maman c'était avant tout une gentille dame, bonne épouse, veuve éplorée, mère irréprochable.
Ultimatum.
Preuve que j'avais dû, inconsciemment, passer cruellement les bornes de sa maternelle abnégation. Je me tenais au milieu du couloir, pendant qu'elle haletait comme un soufflet de forge. De la morve goutait de ses larges narines et ses yeux humides me jaugeaient. 

Le courage de répondre ? Vous vous doutez bien que je ne l'ai pas trouvé ! Ni dans ma tête creuse, ni au fond de mon pantalon – couille molle – Tammie m’affublait souvent de ce sympathique patronyme. Pourtant, soit dit en passant, je la satisfaisais chaque fois qu'elle réclamait, qu'elle exigeait. Ses frasques m'amusait beaucoup au début. J'adorai qu'elle me bouscule un peu et qu'elle s'acharne sur moi (même si parfois j'aurai bien passé la soirée à reluquer un manga en grignotant du pop-corn au caramel). Elle m'entreprenait toujours au dépourvu, détestait qu'on lui dise non, avec son beau regard aux longs cils d'automne, comment lui refuser ? Impossible de la repousser, alors qu'elle jurait toujours qu'elle n'aimait que moi, qu'elle m'aimait à mourir. Ses serments cajolaient mon ego, ses seins tressautaient sous son tee-shirt, éternelle invite, et sa bouche humide faisait le reste.

J'ai pas répondu à ma mère, j'ai juste reculé jusqu'à la porte d'entrée, par laquelle je suis sorti, avec un lamentable,

— Je passerai chercher mes affaires, qui se perdit dans les vociférations aigus de ma génitrice. J'étais libre, croyais-je naivement du haut de mes seize ans.
Fumisteri ! j'avais juste choisis ma Boadicée en la superbe Tammie, aux formes si généreuses, que tout les copains de stage lorgnaient en roulant de ces billes ! Ah, J'étais fier ! et amoureux à en crever.

Ma mère, je l'ai jamais revue. Elle a déménagé peu de temps après mon départ dans son maudit nord auprès de sa famille de dégénérés. Enfin, je les connais pas, mais j'aime bien les imaginer ainsi. Une vengeance à peu de frais, en somme, tout à fait dans mes cordes.

Le temps s'égrennait auprès de Tammie, rythmé par Kolski et ses sempitenelles sarcasmes. Si ses mots avaient été des balles, j'aurai fini écumoire. Intransigeante, vache, tellement vache parfois. Je bouillonais de l'entendre sans me résoudre à barbouiller ma peine et mes tracas du moment ailleurs que sur ce banc merdique.

— Ça fait trois jours que je t'avais point vu, t'étais où gamin ?

Mme Kolski se hissa sur le banc sans me demander la permission. Elle se racla la gorge, me fixant salement, ce qui m'obligea à répondre (enfin, non, j'aurai pu ne pas lui répondre, mais les conflits, j'ai jamais supporté, jamais.)

— Occupé, je dis.

— À quoi donc ? On m'a dit que t'avais rompu ta convention de stage ?

— On va se marier avec Tammie.

— Peuh ! Comment tu peux croire un seul mot qui sort de la gueule de cette traînée !

Elle tapota une de ses cigarettes roulées contre le banc et fit claquer son briquet tempête dans le vide abyssal qui nous séparait. Sans y prendre garde, elle m'envoya la fumée en pleine figure, scrutant mon visage. je tentai de tenir bon devant son persiflage À croire qu'un rayon X était implanté dans ses pupilles étrécies : elle fouaillait jusque dans mon âme tandis que je ravalai la fumée âcre en même temps que mon assurance juvénile. Je résistai pourtant, puisai plus profondement en mes maigres ressources, faisant bonne figure afin d'avoir l'air d'autre chose que ce qu'elle me donnait à voir, cette Gorgone.

— Parlez pas d'elle comme ç ! regimbai-je.

Ce qui n'empêcha nullement cette vieille emmerdeuse de poursuivre sa rengaine.

— Te marier ! tu vas pas faire cette bêtise hei ?

— Si,si. Je pris un air buté. « Tammie a acheté les bagues, deux anneaux d'or blanc, gravés – toi et moi – dans un cœur »

— C'est point nominatif, cett'affaire. Quel engagement ! Comme ça, elle pourra les recycler avec le prochain couillon qui se pointera, tes anneaux ! Réveille-toi bon Dieu !

Elle se signa pour se faire pardonner d'avoir juré.

— Je suis heureux avec Tammie et me marier, c'est géant. C'est un peu comme un rêve !

Pourtant je chuchotai en exprimant cela, comme si cette affirmation allait s'évanouir si je la lançai trop fort, trop loin, à la face d'un monde qui ne comprenait rien et qui, en l'occurrence se résumait à cette satané Kolski. Pourquoi s'acharnait-elle sur moi ?

Sans rien ajouter, la boutiquière repoussa la main avec laquelle je tenais ma figure et prit le ciel à témoin, avisant mon œil au beurre noir, couleur figue.
Et elle patienta.
Je pris le parti de ne pas surenchérir.
Silence.
Je tins bon.
Grossière erreur ! Elle n'attendait que ça et redémarra aussi sec.

— Et ce gnion là ! C'est une preuve d'amour, tu vas m'chanter ? Ses yeux bleus délavés roulaient dans leurs orbites de manière grotesque.

— Tammie est impétueuse, possessive, elle a du mal à gérer ses émotions, c'est tout !

Mais je grognai à bas bruit et gigotai tout en même temps, mal à l'aise sous le feu de ses mots assassins.

— C'est une chienne mauvaise et damnée, un point c'est tout.

Sa voix rauque tranchait l'air comme un rasoir une viande grasse.

— Arrêtez à la fin ! Je me bouchai rageusement les oreilles, comme un gosse capricieux.

— Je vais te dire, tu te fais du mal, mon garçon. Elle, ça la fait jouir le mal, c'est une enfant du mal, mais toi, t'es un bon gamin, quitte cette fille. Ça finira en tragédie, un peu comme Roméo et Juliette. Tu mènes une trop mauvaise vie.

Elle hocha la tête d'un air entendu, écrasant son mégot d'un talon rageur.

— Si personne s'en mêle, ça finira pa ! explosai-je à retardement tel un pétard oublié après une fête foraine, « on va se marier ! Et c'est pas vos affaires ! »

Je déguerpis, prenant mes jambes à mon cou, mon peu de courage aussi et me sauvai, fantôme pitoyable dans un crépuscule lugubre nimbé d'un voile de brumes méphitiques.


Souvenirs. Souvenirs. Tandis que je descends vers les enfers, mon garage en l'occurrence, portant mon petit colis amorphe sous le bras, ils me sautent tous sur le dos et je sens leur poids, comme si leur but ultime était de m'empêcher de faire un pas de plus, un pas de plus…



— Viens Lapin que j'te présente mon nouveau collègue !

Un grand type d'une vingtaine d'années se tenait dans l'embrasure de la porte.

— Hello ! Il me salua de la main, une bière dans l'autre.

Je passai devant lui, les bras chargés de paquets. J'étais éreinté. Tammie m'avait fait courir tout le patelin et ceux alentours pour mettre des flyers dans les boites aux lettres. Elle se lançait dans le métier d'agent immobilier à temps partiel, en plus des cours qu'elle dispensait au lycée. Et je lui servais en quelque sorte, de commis.

Elle m'accueillit d'un regard interrogateur. Je la devançai, avec beaucoup d'enthousiasme, espérant sa clémence.

— Tout est fait !

— Ouais, t'as intérêt, faut qu'on gagne du flouze sinon on va ramper comme des cafards toute notre vie.

Le type acquiesça et hennit d'un air entendu. Sa physionomie ne laissait pas transparaître un gramme d’intelligence mais en lieu et place, il affichait un corps avantageusement pourvu de muscles, fringué d'un de ces jeans moulant dont ma tendre épouse raffolait et qu'elle m'offrait sans cesse, à ma plus grande déconvenue. « Parce qu'au moins là, on voit tes couilles, sinon, Lapin, (un de ces surnoms ridicules dont elle m'afflubait !) parfois, je sais plus si je baise avec une fille ou un gars… » Et elle souriait quand elle disait ça, me caressant gentiment comme un chaton. Sa langue curieuse taquinait mon torse imberbe, ses mains expertes effleuraient ma peau comme autant de brandons et invariablement je m'enflammais, crevant d'envie d'elle. J'ai toujours eu envie de Tammie. Elle a dû le savoir dès la première fois où j'ai croisé son regard, en cours de techno. Elle me fixait alors comme un cobra affamé jusqu'à temps que je devienne rouge comme une pivoine, ou que je palisse sous ses yeux dévorants et lève la main pour reprendre haleine, dans un couloir sale et défraîchi.



— Ç’a toujours été une salope. Tu peux m'en accroire.

Madame Kolski passait de la pommade sur une grosse bosse que j'avais au-dessus de la tempe. Depuis une certain temps, elle s'amenait avec tout un attirail, pansements, alcool à quatre-vingt-dix degrés, crème à l'arnica, aspirine, le tout niché au fond de son tablier qu'elle portait toujours par dessus ses frusques.

— Elle t'a frappé en quel honneur c'te fois ?

— Noon ! Qu'est ce que vous allez chercher encore, c'est le portrait de Rita Mitsuko qui s'est malencontreusment décroché et a atterri sur ma tronche…

— Et elle t'avait balancé quoi avant que le phénomène se produise ?

— Putain, vous êtes…

— C'était quoi ? Ses yeux clairs me sondaient avec ironie.

Je déclarai forfait.

— Le dessous de plat en marbre…

— Vache ! ça a du faire un beau trou dan'l'mur !

Le ton était caustique mais une bienveillance toute ridée et ratatinée émanait de la vieille rombière. Une sorte d'aura à la fois déroutante et réconfortante. Ça faisait un bail de confidences maintenant, entre nous, et si les passes d'armes s'avéraient virulentes, c'était surtout de la poudre au yeux, j'avais pris conscience du fait. Chacun campait sur sa réserve parce que chacun préservait sa pudeur. Elle ne m'en tapota pourtant pas moins l'épaule et soupira.
Connivence.

— C'est pas faux, convins-je. « Un cratère ! » Je visualisai cet énorme trou ornant maintenant le mur de notre bonbonière et ris de bon coeur.
Mon crâne m'élança aussitôt, me reprochant ma légèreté. Mes yeux se fermèrent en me remémorant ce pénible moment. Elle m'avait loupé de peu, ma Tammie ce jour-là. J'étais rentré plus tôt, sans prévenir (entre vous et moi, je doute que ce soit tout à fait normal de devoir s'annoncer avant de rentrer chez soi). J'ébauchai à peine un pas dans la maison qu'elle me hurlait déjà de sortir alors que l'autre connard, son collègue aux cuisses de grenouille, rafistolait sa chemise, une de ses sales pattes tenant encore la taille de ma femme. Pour la calmer, soit disant ! tu parles, ses yeux causaient autrement mieux que sa bouche pleine de purin.

— Mais je suis sorti, racontai-je à la boutiquière fidèle et attentive à mes déconfitures, pas fier tout de même, baissant les yeux manifestement à la recherche d'un putois ou d'une tortue marine planqué sous le banc… 

— Quand Tammie sera plus calme, on discutera tranquillement de tout ça. 

— T'as jamais eu une discussion sensée avec cette catin-là, arrête de me raconter des nanars. y a rien qui peut arraisonner une garce comme elle. Prends tes cliques et tes claques, mon garçon et va t'en.

— Je l'aime.

Cette fois je l'ai dit. Bon sang !

Elle ne répondit d'abord rien. Elle prenait son temps la commère.

— Alors t'es cuit.

Le ton était funèbre, la sentence définitive. Je la regardai carrement et approuvai.

— Je suis cuit. En plus, j'ai appris un truc horrible il y a quelques jours.

Ses yeux me transpercèrent, ou ma propre culpabilité, ou les deux.

— Ah ? Encore pire qu'ta vie de merde à te torturer l'esprit du matin au soir et le retourner dans tous les sens pour te faire souffrir ? A te prendre des raclées et te faire violer comme une poupée de chiffon ?

— Exagérez pas ! 

— J'exagère rien, c'toi gamin, qui vois pu clair, avec tes yeux bouchés par ce fléau que t'appelles amour. Ca ressemble plutôt à une glu puante et collante tartinée tous les jours un peu plus jusqu'au tréfond d'ton pov'cerveau !

Tirade digne d'un Molière. Elle ramollissait pas, Kolski, mais j'en avais une sous le coude, plus courte, mais oh combien plus fracassante. Ouais. Je passai aux aveux.

— Je vais être papa.

— Mon dieu !

Elle serrait les mâchoires, la vieille Kolski et les jointures de ses mains sur les miennes avaient soudain blanchi, comme son teint. De ses yeux bleu délavé de grosses larmes coulaient sans retenue. Je lui en bouchai un coin, mais pas plus d'une poignée de secondes.

— Qu'ess tu vas faire, bordel de Dieu ?

Elle se signa, bien sûr, derrière cette nouvelle offense à son copain, là-haut, qui devait bien se marrer de nos déboires cosmiques.

— Je viens de prendre une branlée parce que je lui ai suggéré… enfin je…

— Tu lui as demandé d'avorter ?

Elle ne me jugeait pas À cet instant, dans son regard, de la compassion, un zeste de dégoût aussi, enfin, il me sembla. Le dégoût, on le sent de loin, quand il se pointe, il fait très mal, celui-là.

— Elle a refusé, vu que c'est pour moi qu'elle a fait l'enfant. En fait, c'est de ma faute tout ça.

Ma voix est devenue inaudible.

Silence.

Qui se prolongea jusqu'à devenir insoutenable. Je me tortillai sur mon banc comme une anguille dans un seau. Elle, droite comme un « i », me lâchait pas du regard. Un vieux doberman montant la garde devant son os, vieux, mais teigneux. Elle reprit les hostilités non sans se torcher d'abord la figure avec un mouchoir à carreaux de la taille d'une nappe.

— Ess'plique moi ça, mon gars, ch'ui sûre que t'aurais pas la tête assez creuse pour réclamer un chiard au milieu d'un sac de nœud pareil ! S'exclama-t-elle, sa main tatônnant sa poche de blouse à la recherche de sa boite en métal toute cabossée contenant ses cigarettes dégueulasses.

— Non, bien évidemment, c'est un malentendu mais le mal est fait…

— Ou elle a réussi à t’enchaîner un peu plus ! Cloué au pilori, le môme !

Elle prit un air plus qu'agacée et moi, plus que coupable.

— Vous n'y êtes pas, Mme Kolski.

L'emploi de « Madame » la mit furieusement sur ses gardes. Je vis bien à sa mine qu'elle affûtait déjà une cinglante réplique. Je m'empêtrai dans mes explications, et sentis que l'empathie avait disparu de son regard d'acier. Je poursuivis quand même, un tantinet défaitiste : quand un navire va couler, autant le saborder non ?

— C'est vrai, tout est de ma faute ! On délirait un peu et j'ai suggéré qu'un enfant, c'était l’aboutissement pour un couple, mais…

— T'as dit ça à cette furie ?

Cette manie qu'elle avait de me couper la parole.
Elle s'arrachait les cheveux qui bordait sa coiffe durant mon bref mea culpea et passa à deux doigts de foutre le feu dans sa tignasse grise avec sa clop.

— Mais qu'esse t'as dans la caboche gamin ? Se lamenta-t-elle interloquée.

Tant de bêtise (ou de naïveté), ça doit pas être possible chez les polack. Je m'affaissai sous le reproche, rentrant la tête dans les épaules et parlai à mon menton. L'air con total. Je ressentais de la honte mais paradoxalement j'aimais Tammie plus fort que tout. Et j'avais beau me dire qu'elle n'avait pas tort, l'épicière, de pointer le gouffre de ma stupidité, toujours dans l'autre plateau de la balance, gisait cet amour irraisonné et animal pour ma Tammie. Et le plateau penchait en sa faveur. Toujours.

Je m'empêtrai de plus belle dans un flot de paroles quasi innterrompu et la vieille prit tout en pleine face, comme un jet de vomi. Je parlai sans discontinuer jusqu'à recracher le moindre petit morceau de ce magma infâme coincé depuis des jours au creux de mon bide. Je ne l'ai pas épargné, Kolski, son putain de dieu m'en préserve, ou pas; ce fut presque une confession.

— Tammie venait de découvrir que j'avais causé deux secondes avec Laurie, mon ancienne pionne, au lycée Barbusse, rencontrée par hasard, en déposant ses vestes chez le teinturier et elle a littéralement pété un câble. Elle a foutu je ne sais quoi dans ma bière, je me suis réveillé enchaîné au sous-sol après un vieux tuyau de la chaudière. Un adhésif sur la bouche. Je me suis tortillé, j'ai appelé comme j'ai pu, mais rien n'y a fait, j'ai passé la nuit en bas, à me geler et à me fabuler cent scénarios. Elle est jalouse, vous voyez, je le sais, j'aurai pas dû…

Sidérée, la vieille coupa court à mes interprétations oiseuses, sa peau parcheminée avait pris une teinte vert-de-gris, comme si subitement elle avait des nausées, elle aussi (c'est souvent communicatif).

— Et après ? Questionna-t-elle d'une voix d'outre-tombe.

— Après ?

— Tu t'es libéré ?

— Non, elle m'a détaché le lendemain soir, j'avais pissé et chié sur moi…

— Pleure, ça te fera du bien, gamin.

— Arrêtez de m'appelez comme ça !

Ma voix se brisa, un barrage qui se rompt n'aurait pas été plus soudain et je pleurai comme un veau.

— Si je t'appelais homme, ça changerait rien à l'affaire. Continue, m'incita-t-elle plus conciliante, « elle t'a libéré et après ? »

— Elle m'a consolé. Elle m'a traité de vilain garçon, que je lui faisais beaucoup de peine, qu'elle avait l'impression que notre couple partait en cacahuètes. Elle m'a aidé à remonter du sous-sol, je tenais pas sur mes jambes, et j'étais…

— Couvert de saletés.

— Oui c'est ça, mais elle, elle en avait rien à faire, elle m'a fait couler un bon bain, elle m'a lavé…

— Et baiser comme une putain, je gage.

— Madame Kolski !

Vaine protestation.

— Vrai ou pas vrai ?

Tergiverser ne sert à rien avec les polack. Une chose que j'ai apprise, cette vieille bique finit toujours par me faire cracher le morceau.

— C'était un instant tellement fort ! m'indignai-je.
Je crus brailler mais n'émis qu'un couinement. Une souris coincée dans un piège.
La boutiquière interrompit cette vision qui m'assaillait brutalement. Il me sembla entendre le claquement du piège se refermant sur moi.

— C'est de la perversité enfin ! Cette femme est un démon avec des mamelles. Tu dois partir, mon gars, scanda-t-elle, « tu vois pas que t'es plus que l'ombre de toi-même ? » Sa voix grondait.

— Mais elle attend un enfant ! Mon enfant !

— Qui te dit que c'est le tien ? Hein ? Couillon ?

Elle lâcha mes mains et les agita par-devers elle dans un simulacre d'impuissance.

— Dites pas ça !

Cette fois mon cri emplit le parc, la nuit, l'espace tout entier.

Elle se tut, respectant ma douleur, ma responsabilité, ma faiblesse aussi. Ça me donna le courage de poursuivre.

— Bref, ce soir-là, qu'elle me demandait, qu'est-ce qui me manquait pour que j'aille baguenauder dans les sales pattes de cette Laurie sans nichons, je lui ai juste confié que la vision de la famille idéale, pour moi, c'était des gosses jouant et s'éparpillant autour de la maison.

— Foutu bon Dieu ! Répéta plusieurs fois la vieille harpie. « Mais qu'est-ce qui t'es donc passé par la caboche nom de Dieu ? » Elle mit la main devant sa bouche après cette salve de jurons et se reprit, avec un petit dernier ;

— Nom d'un chien !

— J'étais chamboulé, je vous rappelle …

— Oui… j'ai bien conscience, la cave, attaché, et la séance de câlins, ah oui… bravo ! une sacrée connerie que t'as été lui souffler là, j't'assure.

— Elle est pas si tordue que vous pensez, faut rien exagérer, Tammie, elle a besoin de beaucoup d'affection, de tendresse, de réassurance, elle paraît forte…

— Une bougresse pareille ! En tôle, j'te la foutrai, moi. Si t'avais le cou…

Et comme elle me regardait, penaud, une fesse sur un coin de banc, l'air perdu, elle se tut. C'est moi qui lancai l'info du siècle,

— Vous pouvez le dire, j'ai aucun courage, je m'en rends compte, mais de toute manière la quitter, je pourrais pas, c'est au-dessus de mes forces, j'en crèverai, c'est tout vu, cet amour que j'ai, c'est comme une maladie, il rampe, il m'étreint, mais sans lui, je suis plus rien. Je n'ai qu'elle. Elle a toujours été là pour moi.
Je terminai en chuchotant.

— Si un petit sort de son ventre, t'auras tâche de prendre soin de lui, doute pas une seconde qu'elle en aura cure, d'ce marmot. Elle chuchota, elle aussi.

— Je ne peux pas vous croire, elle a un cœur tout de même ! Elle est capable…

Kolski c'était une Kalachnikov, une fois armée, vous pouviez toujours courir pour la stopper ! Et ses balles, lâchées à bout portant, faisaient un mal de chien.

— Un cœur ? S'exclama-t-elle, sans hausser la voix, t'crois que le diable en a un ?

Je repoussai sa main secourable et m'échappai, une habitude. Le froid cinglait mes larmes aux coins des yeux, des yeux que j'ouvrai grand et pourtant, je n'y voyais goutte ni ce soir-là, ni aucun soir qui s’annonçait. Qu'est-ce que j'avais fait ?



— Il est bea ! s'extasia-t-elle.

— Vraiment splendide, oui ! Renchéris-je, sous le charme.

— Heureux choix madame ! Approuva le vendeur, un type d'une trentaine d'années, cheveux blonds, longs, yeux gris, costume trois pièces, une vraie gravure de mode, m'ignorant totalement.

— Nous le prenons ! me pressa tammie.

— Heu, chérie, il est…

— Quoi ?

— Paiement en trois fois, si cela peut convenir, précisa le vendeur avec un clin d’œil tout à fait déplacé.

— Comptant, rugit Tammie, gonflant sa poitrine, devenue carrément imposante depuis son septième mois de grossesse.

— Formidable ! Applaudit le vendeur conquit, le regard torve, la main sur la brayette. Pour votre plaisir, une petite ristourne, allez c'est la maison qui régale !

Ma femme lui prit le bras et entreprit de le remercier d'une voix plus que chaleureuse, enjôleuse. Une voix de chatte. Je me révulsai, confus, devant cette scène à peine déguisée de séduction tandis que je poussai la poussette dernier cri. Je m’écœurais, au bord de la nausée, cloîtré dans mon silence, mon acceptation. Je constatai, horrifié, que le type frôlait de ses paluches obscènes, les reins généreux de mon épouse, faisant mine de l'accompagner jusqu'à la caisse. J'étais devenu invisible, comme souvent, je m'en rendais compte. Mais bon, me disai-je, après quelques temps, le mariage, ça lasse, c'est normal et moi, j'ai jamais guère eu d’imagination. Ma beauté brune, avec son tempérament généreux, c'était inévitable qu'elle ait besoin de se mirer dans les yeux d'autres mecs. Même des connards en costume.



Je chasse vite ce souvenir pour un autre.



— Fais bien longtemps qu' t'étais point venu, ça va avec ton petit ?

Un vagissement s'éleva de sous mon caban, la même vielle frusque que trois ans plus tôt. Et la même vieille voix éraillée, interrogatrice de Mme Kolski.

— Montre moi c'te petit là !

Je découvris le nourrisson, il était beau, potelé, de grands yeux noisettes, une peau diaphane, une petite bouche en cœur, un chérubin.

— L'a quel âge ?

— Trois mois.

— Ça s'passe bien à la maison ?

La vieille se mordit la langue. Si j'étais là, sur ce foutu banc à moitié pourri au fin fond d'un parc municipal ringard, au crépuscule, à l'orée de l'hiver, gageons que c'était pas pour déballer ma joie et mon bonheur absolu. Elle toussa longuement, et revint à la charge, c'était plus fort qu'elle.

— C'est quoi qu'elle a encore inventé pour te torturer ?

— Rien, c'est juste que le petit est turbulent à présent.

— Y pète la forme, y a qu'à le regarder, répliqua la vieille en me toisant comme si j'étais idiot, « l'est comme un bébé quoi, y braille, y mange, je suppose »

— Oui, ben justement il braille beaucoup.
Je soupirai douloureusement.

Elle n'avait pas bougé, les lattes du vieux banc pliaient sous son poids tandis qu'elle réajustait la couverture autour de l'enfant. Elle ne me regarda pas, ce qui m'encouragea à entamer quelques confidences À vider mon sac, disait ma mère. Mais ma mère était morte à présent.

— Il me faut le faire taire, c'est, euh, c'est mieux en fait.

Je me mordis la lèvre comme un gosse prit en flagrant délit de mensonge. Le banc grinça.

— Mieux ?

Mon inquisitrice leva ses deux sourcils, qu'elle avait broussailleux et sa mine prit un air comique, mais loin de moi l'idée d'en rire, j'étais trop tourneboulé.

— Mieux qu'il soit calme. Quand elle rentre du boulot, Tammie supporte pas qu'il crie.

La vielle rosse souleva le bonnet de marin qui me cachait en partie la figure. Ses mains glacées frôlèrent mon visage.

— Eh ! me récriai-je.

Elle ne tint aucun compte de mon air outragé.

— Pas de bosse ?

J'ignorai son insinuation.

— Je lui donne du sirop, pour qu'il dorme.

— Tu vas pas pouvoir lui donner du sirop toute sa vie, ça suffira pas longtemps, même si tu lui donnais des doses de cheval, à moins qu't'es dans l'idée de te débarrasser de ce pov p'tit être.

Elle tenait à présent une de ses menottes chaudes dans sa main ridée.

— Pourquoi vous dites une chose pareille ? Vous êtes dingue ! Je protège mon enfant ! objectai-je sur la défensive.

Ces derniers mots sortis de ma propre bouche me parurent tellement creux. Kolski réveillait en moi des tourments que je m’efforçais de réduire à néant. Manifestement en vain. Elle poursuivit son raisonnement, impitoyable.

— Tu vas vite arrivé au bout des machineries possibles. C'te gosse y va pleurer, colèrer, bouger, avoir des besoins de tout enfant de son âge et tu pourras guère l'empêcher avec un bon Dieu de siro ! Elle se mordit le poing d'avoir encore juré.

— Je fais ce qu'il y a à faire, aboyai-je, décontenancé par ses insinuations.

— Je vais te dénoncer, je peux pas laisser faire ça mon garçon.

— Faites pas ça, suppliai-je.

Elle se rendit compte que quelque chose tournait encore moins rond que ce qui ne tournait pas rond depuis ces trois dernières années.

— Raconte !

Le mot, lancé comme un marteau sur une cloche, résonna dans mes tripes. Je m'inclinai.

— C'est juste une intuition, ou une impression.

Je tergiversai. Lâche !

Elle, impatiente à présent, claquant le capot de son vieux briquet tempête.

— Raconte.

Je mesurai la persévérance de la vieille femme. Elle grogna comme un ours en cage – Un grognement familier – Curieusement, je me sentis soudain plus détendu pour aborder la suite. Légèrement seulement. Parce que ses yeux me transperçaient et fouaillait salement dans ma carcasse.

— J'ai le sentiment parfois qu'elle ne serait pas mécontente s'il arrivait quelque chose à Valentin.

— Valentin ?

Son regard s'attarda sur le bébé qui sommeillait avec l'œil de la jeune femme tendre et douce qu'elle avait dû être un siècle avant, au moins. L'enfant se mit à babiller doucement, plongé dans un rêve de fées et de lutins, espérai-je.

— Ça lui va bien. T'as bien choisi.

— Merci.

— Continue !

Ton péremptoire.

 Je répondis à l'invite sans ergoter. Les mots se bousculaient déjà sans mon autorisation, les mots me braquaient : coucou c'est un hold up ! T'as plus le choix mec ! Haut les mains, passe à la caisse…

— Par quelques allusions, repris-je, essayant de me calmer. Parce que Tammie se plaint souvent, regrettant notre intimité, notre complicité. Elle trouve que le gosse complique tout.

— Mais d'un autre coté, tu fais toujours ses quatre volontés, je gage.

— Je fais dans le consensus.

— Ah !

Valentin sursauta.

— Niais tu es, niais tu restes, mon garçon et je te dis ça très affectueusement. Dieu nous préserve ! Comment t'peux écouter tout ce venin qui sort de la bouche de cette margoton ?

Elle n'attendait pas de moi que je me justifie, aussi poursuivi-je, tant que j'en avais les c…

— Bref, je me demandais si vous me le garderiez quelques heures parfois quand j'ai besoin, je vous paierai, bien sûr.

— Bien sûr.

Pas de reproche. La vieille chouette était peut-être à bout d'argument. Je la scrutai attentivement. Elle profita de ce moment pour m'asséner son ultimatum,

— Mais je te préviens, si je trouve un seul bleu sur ce p'tit ange, je filerai droit à la police, passe qu'là c'est plus tes fesses que tu risques, ce sont celles d'un innocent.
Elle me sonda entre deux bouffées de nicotine bleutée. Toutes les rides de son visage exprimaient une douleur indéfinissable. Puis d'une voix lente, elle termina sa tirade. Un uppercut en plein l'estomac.

— Enfin un innocent encore pire que toi t'étais innocent. Tu comprends ? Faut que tu trouves la force de partir !

— Je peux pas.

— J'comprends pas.

— Moi non plus, Madame Kolski, mais c'est comme ça, je peux pas et je crois que je pourrais jamais. Mais je protégerai Valentin, un peu de sirop, ça peut pas lui faire de mal…

Je geignis mollement, quêtant son approbation. J'en fus pour mes frais. Elle s'éloigna dans la nuit et je restai comme un crétin sur mon banc, le petit s'était rendormi.



Je fermai les yeux. Putain de souvenirs. Tout me péte à la gueule tandis que ce chemin n'en finit pas de me mener au garage, derrière la maison que je partage avec un monstre. Ma maison, mon monstre.



— Bonjour Madame, officier de police Oveira, voici mon collègue Harles, nous avons eu plusieurs plaintes du voisinage concernant des cris d'enfants.

— Bonjour Messieurs, excusez-moi, je me lève, permettez-moi d'aller passer une robe de chambre par dessus ma nuisette.

— Heu, oui, bien entendu Madame. Votre mari est là ?

— Il est absent pour l'instant.

Discrètement, de la main, elle me fit signe de gagner le sous-sol, ce que je fis, mon visage tuméfié aurait été trop parlant, je veux bien croire, vu la toise qu'elle m'avait mis la veille. J'ai jamais pu frapper une femme. Encore moins celle que j'aime. Vous comprendrez pas, je peux pas vous en vouloir, vous vous dites quel dégonflé ce mec, quel nullos, il peut pas lui mettre deux torgnoles à sa salope de bonne femme ? Eh ben non. Y peut pas. Bien que maintenant, j'ai dix-neuf ans, bientôt vingt, je ne suis plus un gringalet, je fais dans les 1,85 m pour 75 kg, c'est vrai je suis pas très étoffé. Pourtant, Tammie me gave de protéines matin midi et soir, pas moyen d'y couper !

La conversation entre les flics et Tammie se perdit dans les escaliers que je descendis à pas de loups. Le petit se mit à pleurer très doucement, je lui donnais sa peluche, Mr Panda, qu'il adorait, il se calma de suite et quelques minutes plus tard, le moteur d'une voiture vrombit puis s'éloigna. J'en déduisis que ma chère et tendre femme avait convaincu les deux sbires. Rien à signaler !

— Tu peux remonter ! Sa voix grinça désagréablement à mes oreilles. Je frissonnai.

C'était devenu une affaire convenue. Pour ne pas être embarrassée, quand elle recevait des collègues, des amis, son chef de secteur, bref, pour briller en société, le gosse et moi, on disparaissait au sous-sol. J'avais même aménagé une chambrette, avec un vieux divan élimé et des couvertures, coussins… et puis j'avais dégotté ce grand singe en peluche, aux quatre membres démesurés que Tammie surnommait Mr Panda, pure dérision je suppose. Valentin se prit immédiatement d'affection pour cette peluche déglinguée qui sentait bon la lavande. Je lave tout à la lavande. Ça me rappelle… je sais pas, l'enfance que j'ai pas eu, certainement, ou celle que j'aurai voulu avoir, si le monde était parfait. Quand on touchait une des mains de Mr Panda, il en possédait quatre, une petite lumière colorée clignotait puis s’éteignait et à chaque fois, ça ravissait le petit. Il pouvait rester des heures à jouer avec sans bruit. Ça tombait bien, parce qu'il ne fallait pas faire de bruit, justement, ne pas déranger Tammie et ses invités. Quand ça marchait plus, le coup de Mr Panda, je lui refilais son sirop, plusieurs cuillers à présent, et l'effet se faisait sentir plusieurs heures. C'était mieux que de laisser le gamin tout seul quand je devais remonter. Tammie aimait bien qu'on passe du bon temps seul, tous les deux, sans le regard curieux du gosse. Je mettais double, triple doses, et ça se passait bien. Même si parfois, mais ça, je le gardais pour moi, (la vieille mégère croupie sur le banc m'aurait sauté à la gorge) Valentin restait amorphe très longtemps, et c'était assez flippant.

— Comment j'ai cru que ça pourrait marcher ? Me lamentai-je en couvrant le petit jusqu'aux oreilles, endormi dans sa poussette devenue un peu juste pour lui.

— Tu diras pas que je t'avais pas prévenu, hein, râla Mme Kolski en toussant dans ses grandes mains toutes tordues. Elle avait de la polyarthrite, ça se dégradait à présent, et elle toussait comme pas possible, ce qui ne l'empêchait pas de fumer plus qu'un poêle à charbon.

— L'amour excuse tout, qu'on dit. Balivernes ! martela-t-elle.

— Elle a passé les bornes, avouai-je en serrant les dents, si bien que je crus tout d'abord que la vieille maritorne ne m'avait pas entendu.

— Ah ? y avait des bornes ? Rétorqua-t-elle vivement, hochant la tête de manière anarchique. Ben putain de bon Dieu, mon garçon ! Elle se signa inconsciemment.

Je rendis les armes.

— Vous moquez pas, c'est que je me suis rendu compte aujourd'hui, vous comprenez ?

— J'ai pas le cœur à me moquer, crois-moi, mon garçon, c'est une tragédie. C't banc, c'est devenu ma croix de misère. Et toi… toi…

La vieille avait tapé juste, comme toujours. Tragédie. Le mot était fort, mais congru. J'accusai le coup.

— Faut croire que peut-être j'ai atteins un cap, je sais pas. Ce soir, je suis largué, Mme Kolski, vous auriez votre crème ?

— Pourquoi ? Souffla-t-elle.

— J'ai les avant-bras en compote.

— Elle t'a frappé avec quoi encore ?

— Avec le fer à repasser.

Je sentis le poids de son regard et cru bon d'ajouter,

— Il était éteint, heureusement.

— Non mais tu t’entends mon garçon ? Elle te traite de fiotte toute la sainte journée, et toi tu lui embrasses le cul parce qu'elle te frappe avec un fer à repasser froid ! Elle se signa, pardonnez-moi Seigneur ! Et elle continua, impitoyable, son interrogatoire.


— Et le sujet de la dispute, c'tait quoi cte fois ?
 
— Une broutille, au départ, on jouait, Valentin et moi, avec Mr Panda et puis Tammie l'a fichu dans la poubelle pour taquiner le petit. C'est devenue une manie en ce moment, elle le cache dans les placards, ça l'amuse que le gosse panique et le cherche partout. ou elle l'accroche hors de sa portée et il la regarde tout malheureux parce qu'elle lui tord ses grandes paluches simiesques.

Kolski mouftait pas. Je continuai.

— Enfin, bon, il s'est mit à protester, j'ai pas pu le calmer assez vite cette fois…

— Et elle l'a frappé, assena la boutiquière, d'une voix sans émotion, le regard perdu au loin, comme si je n'existais déjà plus.

Un silence lourd et oppressant s'installa.

La vieille Kolski me fourra quelque chose dans la main puis reprit en chevrotant, après avoir toussé comme une vielle locomotive durant une bonne minute.

— Elle l'a frappé, ce p'tit ange, et toi t'es là en train de geindre tandis que j'te tartine d'une pommade de merde inflammatoire. Tu me dégoûtes mon petit, j'suis navrée, fallait que je te le dise, là.

Elle rangea son tube de crème dans son sac et farfouilla dedans.

Le silence, douloureux, glaçant à présent, s’immisçait entre nous.

Dans ma paume, apparu un trousseau de clés. Je levai vers elle des yeux interrogateurs. Une boule s'était formée au fond de ma gorge, les mots restaient coincés tout au fond.

— C'est les clefs d'ma vieille Buick. J'en aurai plus besoin.

Elle soupira.

— Tu devrais répondre quelque chose, parce que ce mutisme là, ça me donnerait presque envie de te frapper aussi.

Je me raclai la gorge.

Le parc s'était couvert de givre durant notre conversation, il faisait scintiller les lampadaires et le gravier des chemins. La buée sortit de ma bouche, tandis que la bonne femme se levait, abandonnant notre banc, m'abandonnant moi. La boule que j'avais dans la gorge dégringola dans mon ventre, j'avais mal comme si j'avais avalé des pierres au déjeuner. J'émergeai enfin de cette léthargie du lâche, je protestai, faiblement, d'une voix de crécelle,

— Mme Kolski ! C'était la première fois !

Mais c'était trop tard, quelque part en dedans de moi, je le savais. La vieille s'éloigna sans se retourner, avec tous ses abatis, en serrant son vieux cabas.



Et me voilà aujourd'hui, j'ai remonté le chemin qui mène derrière la maison, serrant cette petite chose amorphe entre mes bras. Ses membres mous cognent contre ma jambe tandis que j’ouvre la porte du garage. Mon cœur bat à tout rompre. Je n'ai qu'une crainte : ne pas aller au bout. Je lève les yeux sur la corde installée depuis ce matin en travers de la large poutre métallique traversant l'humble bâtisse. La corde se balance légèrement, me narguant d'avoir le courage d'en finir. Je ne veux plus réfléchir, je l'aime. Je crois que j'ai enfin saisi le sens de ce mot.

Je serre le petit corps élastique, effaré de ce que je m'apprête à faire, effaré de ce que sera ma vie, si je n'y parviens pas. Je respire par à-coups, l'air s'est raréfiée autour de moi, la lumière fuit, je suis dans l'ombre, j'étais l'ombre. Bon Dieu, pardonnez-moi !

Lorsque je repousse enfin le battant de la porte, vidé de tout sentiment, un rayon de soleil effleure mon visage, le vent agite ma crinière brune, et sèche les sillons de mes larmes. Dans ma poche, le cliquetis d'un trousseau de clefs.

À l’intérieur du garage, cette même brise agite doucement Mr Panda, pendu au bout de la corde, prisonnier d'un nœud coulant impeccable, répéter dix fois, vingt fois, avant que je ne fus pleinement satisfait. Sur le gros bidon du singe en peluche, j'ai pris soin d'épingler une petite phrase, de ma plus belle écriture,

« Va te faire foutre, Tammie »

Aujourd'hui, c'est Tammie qui a ingurgité du sirop, dans sa vodka cerise, et elle n'y a vu que du feu. Le gosse m'attend dans le siège auto, au coin de la maison, dans la vieille Buick. C'est cette râleuse de Kolski qui n'en reviendra pas, si elle apprend un jour le fin mot de l'histoire.

Mr Panda tournicote sans fin, au bout de sa corde. La scène m'hypnotise, pourtant, faut pas que je traîne.

« Va te faire foutre, Tammie »

Pas très élégant, j'en conviens, mais libérateur. Une phrase que j'ai mis plus de quatre années à écrire. Cette phrase, nulle doute que je te la dois, mon Valentin ; maintenant que tu es en âge de comprendre les choses, je te devais de faire preuve d'un peu d’honnêteté. Tu dis toujours que tu as un papa parfait, voilà qui va sans nul doute te donner une toute autre image de moi, plus moche peut-être, mais la vérité a rarement la beauté des fées. Et les lutins vont peut-être se moquer. Mais j'ai quitté mon banc de misère, et à présent, je t'ai toi.
