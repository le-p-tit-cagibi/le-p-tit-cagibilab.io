La lune gibbeuse, sphère quasi parfaite, lumignon familier pour tous flâneurs et retardataires, disparut soudain de la voute céleste, tel un fantôme tremblotant dans le brouillard épais, et une autre lueur, safranée, éclipsa sans transition la nuit finissante. À travers le pare-brise recouvert de givre, en dépit du chauffage, Lou écarquillait ses yeux qui le brûlaient déjà, sur un paysage d'une lactescence éclatante. La neige voltigeait sous un vent complice, accrochant les essuies-glaces, ripant sur la carrosserie, épaississant la couche déjà non négligeable sur la petite route de campagne. Nul doute qu'il était perdu. La dernière déviation l'avait entraîné sur un itinéraire secondaire, fait de voies de plus en plus étroites aux congères monumentales. Il jeta un regard dans le rétroviseur ; l'espace se refermait sur le véhicule au fur et à mesure de son avancée désormais chaotique. Aucune visibilité sur les bas-côtés ne lui permettait de s'orienter. Le GPS avait rendu les armes environ une heure auparavant, et, à présent, seul le soleil levant ouvrait un modeste chemin devant lui.

Les prémisses d'une panne se firent entendre. Le moteur hoqueta de manière alarmante. Lou appuya sur l’accélérateur, mais la petite routière répondit mal, glissant dangereusement vers la droite, le long du mur de neige. Il redressa l'automobile délicatement, et soupira d'aise lorsque celle-ci reprit sa trajectoire. Rémittence qui ne perdura que le temps d'atteindre le virage suivant, en épingle, et de ce fait totalement invisible. Lou ne put alors que jurer de la plus vulgaire des façons en jouant vainement des pédales et du volant puis la neige molle l’accueillit avec un bruit mat. Le véhicule s'encastra jusqu'aux portes arrières et cala. Son conducteur s'entêta, cherchant à redémarrer, mais une succession de tentatives infructueuses suivie de râles de plus en plus oppressants du moteur l'amenèrent à penser qu'il devait à présent se résoudre à abandonner la voiture et chercher un refuge.

Le jeune homme se résolut à passer par le coffre pour s'extirper de l'auto et affronter des éléments déchaînés, muni seulement d'une paire de baskets et d'un blouson léger. Il vérifia une ultime fois : son smartphone affichait hors service.

Fuir un endroit pour être piégé dans un autre, tout aussi inamical, voilà qui ne s’annonçait guère encourageant, rumina-t-il un instant, tout en resserrant ses maigres vêtements autour de son buste. Il grelottait déjà. Ses cheveux, d'un blond roux, balayaient ses épaules osseuses aux grés des bourrasques, et devinrent rapidement blancs et glacés alors qu'il s'éloignait péniblement du lieu de l'accident, pataugeant dans la neige immaculée.

Il maugréa. Il prévoyait de se faire oublier dans les montagnes, à seulement quelques centaines de kilomètres de là. Le temps lui était compté, et jusqu'ici, la chance lui avait plutôt souri. Jusqu'à cette déviation. Un retard qui pouvait se révéler lourd de conséquences… Il leva le nez au vent, huma l'air froid, chercha quelque chose, entre ciel et terre, qu'il ne trouva point, et se remit à avancer laborieusement.

Soit, ce projet est à remiser momentanément dans les cartons ! Songea-t-il amèrement, plongé dans ses réflexions, tandis que la route se rétrécissait encore, devenait chemin, layon, pour aboutir enfin devant le pas d'une double porte cintrée, aux vantaux de bois sculptés d'entrelacs de palmes et rameaux gracieux. Un mur en pierres de taille grises dévoila les vagues contours d'une bâtisse. Un corps de ferme peut-être, se dit-il, puisqu'il semblait évident qu'il était échoué au fin fond d'une campagne.

Les doigts gelés, il tambourina contre le montant de chêne. Une partie de la neige collée sur le mur glissa dans un bruit d'avalanche et révéla une petite cloche de bronze fixée entre deux pierres. Une chaînette dorée pendait jusqu'à ses yeux. Lou la saisit et tira comme un forcené ; un tintinnabulement inattendu et charmant, tel le timide roucoulement d'un oiseau de printemps, ricocha mollement le long de la façade, et décrut rapidement. Il réitéra son geste sans succès et la rage débordant sa patience, entreprit de frapper sur l'huis de son poing. Contre toute attente, la porte s’entrebâilla doucement, laissant pénétrer neige et hôte sans discernement. Le jeune homme s'engouffra dans l'espace offert et le large battant se referma violemment derrière lui.

Un calme sépulcral régnait dans la cour qu'il découvrit. Planté là, transi, surpris, il admira le lieu flanqué d'une remarquable verrière couvrant tout l'espace entre les toits. Ce dôme, opalisé par la neige, ajoutait une certaine solennité à l'endroit.

Une ambiance particulière pour un visiteur particulier, sourit Lou, à présent plus serein, bien que toujours grelottant. Il secoua son blazer trempé, et s'avança le long d'un chemin de briques bien entretenu agrémenté de bosquets de plantes odorantes, entre deux ailes de bâtiments ressemblant à des granges, dépourvus de fenêtre, au profit de meurtrières étroites, sans vitre. Celui de gauche possédait une porte de bois massif, à galandage, fermée par une lourde chaîne et un cadenas non moins énorme, celui de droite était simplement barré de quelques planches clouées en travers de l'ouverture. Lou ne s'attarda pas, et enfin, après quelques pas supplémentaires, au fond de la cour, la partie habitation de la construction s'offrit à sa vue, émergeant des ombres; le visiteur eut un hoquet de surprise.

Un fronton bas, d'où jaillissait une gargouille aux pattes de grenouille et à tête de chat l'accueillit en grimaçant, ramassée sur son corps scrofuleux. Des feuillages, façonnés à même la pierre, du même acabit que le mur extérieur, encadraient le pourtour d'une porte trapue, faites de traverses de bois massif couleur réglisse, bordées de clous dorés à tête étoilée. L'ensemble dégageait une odeur rance de cire et d'essence de térébenthine.

Les poils de Lou se hérissèrent. Il haussa les épaules et cogna contre le battant. Ses coups n'ameutèrent âme qui vive. Il tourna la poignée, une boule de céramique noire aux reflets d'or, et celle-ci céda à la première sollicitation. Il tergiversa un court instant, gouttant de la tête aux pieds devant la porte, et lorsqu'un grand frisson le parcourut, il se décida à pénétrer dans la maison, poussant délibérément le lourd vantail.

Lou jura. Il se tenait dans un vaste hall où régnait une pénombre dense et oppressante. Des particules de poussières voletaient dans le timide halo de la seule source lumineuse du lieu, une lucarne au fond d'un escalier droit et raide lui faisant face. La rampe, magistrale, d'ivoire poli, luisait presque, palpitante liane à l'émail improbable, grimpant, vertigineuse, à l'assaut de cette étonnante demeure. Le hall était vide. Aucun meuble, tapis, tableau n'agrémentait l'endroit. Les murs, tapissés d'un papier hors d'âge, semblaient avoir avalé toute couleur et le jeune homme se prit à douter de leur consistance. De chaque côté de l'escalier, d'humbles portes d'acajou, ouvertes à demi, ne révélaient qu'obscurité.

Lou se mit à appeler. Il devait bien se trouver quelqu'un dans cette grande baraque ! Supposa-t-il.
Il s’époumona quelques minutes, ses cris se répercutant de manière indécente sans pour autant alarmer qui que ce soit. Il grogna et s'engouffra à gauche, sans plus de façon. Une bibliothèque nantie d'interminables rayonnages, de fauteuils de cuir et d'un sofa rebondi l'accueillit sans chaleur. De larges fenêtres, barrées de volets de bois, obstruaient la lumière du jour. Au fond de la pièce, une cheminée d’albâtre et son coffre rempli de bûches l'interpella. Fébrile, il en fit le tour, préleva une boite d'allumettes sur un guéridon hors d'âge, aux pieds frêles imitant ceux d'une biche ou encore d'un chevreau puis il se mit à l'ouvrage ; il avait besoin d'un bon feu.

Quelque temps plus tard, le bois craquait en cédant aux flammes et une douce chaleur commença à se répandre parmi les livres. Lou se déshabilla, trop glacé pour réfléchir plus avant, et pendit ses vêtements à la diable pour les faire sécher. Il poussa le sofa près de la cheminée, s'enroula dans un plaid confortable, bien qu'il sentît le moisi, et sombra rapidement dans un sommeil agité emplit de peurs, de sang, de hurlements et de souffrances.

Il se réveilla haletant, gémissant, en proie aux affres d'une mélancolie dont il n'avait pas l'explication. Un homme d'une grande élégance, d'un âge avancé, au catogan impeccable, vêtu d'un costume bleu canard et d'un foulard argenté se tenait face à lui, fumant un cigare, confortablement assis dans une bergère au velours défraîchi. Les flammes renvoyaient dans ses yeux une lueur à la fois curieuse et amusée. Lou se redressa d'un bond, rabattit la couverture sur lui et se mit à bafouiller.

— Je… désolé… j'aurai pas dû rentrer…

L'autre le dévisageait sans se départir de son flegme.

— Nous étions absents, vous êtes perdu je suppose, vous avez eu raison de profiter de l'hospitalité de notre logis.

Le maitre de maison offrait une mine hâve. Son regard, aux orbites creuses bordées de noir, sa voix flutée et son allure de dandy déstabilisèrent le jeune homme. A sa grande surprise, sans que ses sens ne l'aient alerté, trois silhouettes blafardes sortirent de l'ombre des murs. Lou sursauta et son échine se hérissa malgré lui.

— N'ayez crainte, poursuivit son hôte, « je me présente, Desmond Rotundus, voici ma fille, Diphylla et mon père, Yougi. »

Lou marmotta d'abruptes salutations, mais s'attarda sur la jeune femme, qui tremblait légèrement lui sembla-t-il. Elle portait une jupe tulipe de moire, un chemisier couleur de lait, en dentelle, ainsi qu'un châle vaporeux sur les épaules. Ses yeux en amande, étirés en un galbe délicat lui donnaient une expression féline. De longues jambes et des escarpins vertigineux contrastaient avec son expression timide. Il lui adressa un sourire gêné.

— Je voudrais me rhabiller, coassa-t-il, tendant maladroitement la main en direction de ses vêtements, à présent secs et raides. Il émergea alors des dernières brumes de son sommeil, et songea qu'il devait être bien tard.

— Quelle heure est-il ? S'enquit-il.

— Le soleil est couché, et nous voilà prêts pour un festin, susurra un vieillard à sa droite. Un long visage décharné, aux lèvres charnues, surmontait un corps efflanqué affublé d'une redingote anthracite aux plis impeccables. L'homme était d'une maigreur cachectique.

— J'ai dormi tout le jour ! S'exclama Lou perplexe.

— Votre marche forcée à travers la tempête vous a affaibli. Nous avons pris la liberté de vous concocter un repas afin de vous réconforter. Passez vos vêtements, et rejoignez-nous dans la salle à manger lorsque vous vous sentirez prêt, ajouta Desmond, se levant.

Il offrit galamment son bras à la jeune femme, et s'éloigna dans le sillage de son père, qui ouvrait la marche d'un pas économe et éreinté.

Lou prit son temps. Les mains tendues aux flammes, l'esprit encore embrouillé, des flashes l'assaillirent. Des hurlements dans la nuit. Une odeur carnée, des bruits de lutte, un goût de sang dans sa bouche. Il claqua de la langue et mordit sa lèvre. Un filet rouge sombre goutta parcimonieusement sur le parquet. Il lécha la plaie et réprima une folle envie de hurler. Puis un abattement bref et soudain le saisit à nouveau.

Il aurait dû atteindre les montagnes avant cette nuit !

Lou franchit le hall, et passa la tête dans l'encadrement de la porte de droite.

— Vous voilà présentable, constata le grand-père, assis au bout d'une longue table de bois d'ébène dont les quatre pieds figuraient ceux d'un dragon. À chaque coin, une tête d'écailles et de cornes semblait observer le visiteur avec attention. Lou s'ébroua, fit mine de s'avancer et Diphylla, tira un siège antique voisin du sien, large et lourd, avec une agilité et une vivacité déconcertante en l'honorant d'une courbette amène.

— Asseyez-vous, murmura-t-elle, « j'apporte le souper. »

Une seule assiette trônait sur la table, avec ses couverts d'argent, ses verres de cristal, et le jeune homme se tendit, dubitatif. Un silence surnaturel sembla suivre le fil de ses pensées.

— Ne vous offusquez pas, justifia Desmond, « nous avons dîné bien avant vous, et nos prochaines libations sont pour une heure plus lointaine. Mangez ! Nous vous tiendrons compagnie d'une dernière lampée de ce nectar. » Ce faisant, il s'empara d'un carafon ouvragé au contenu vermeil et s'apprêta à en verser dans la coupe de Lou.

— Merci, dit celui-ci en posant sa main sur le verre, de l'eau pour moi.

Le dandy leva un sourcil épais, soupira et conclut avec une légèreté qui parut feinte au jeune homme,

— Eh bien soit, de l'eau pour notre invité !

— Trinquons à notre improbable rencontre, invita Diphylla.

Desmond et Yougi gloussèrent et levèrent leur verre. Des chandeliers d'étain éclairaient faiblement la pièce haute de plafond aux caissons de bois ouvragés. L'air sentait le vieux papier, l'humus et quelque chose d'autre encore, indéfinissable. Lou, d'instinct, se recroquevilla sur sa chaise.

— Je ne vais pas vous déranger plus longtemps, commença-t-il soudain aux abois, le cœur battant la chamade, ne comprenant pourquoi. « il faut que je retrouve mon chemin, je dois me rendre dans la montagne sans tarder. »

— Il est bien tard pour s'aventurer sur les routes, le raisonna Desmond, « vous reprendrez votre voyage à l'aurore, ce sera plus prudent. Qui sait ce qui rôde dans ces chemins perdus à cette heure. Nous habitons un coin très isolé vous savez. »

— Très isolé, renchérit la jeune femme, en posant ses lèvres exsangues sur le bord de sa coupe.

Demain, il sera trop tard, médita Lou silencieusement.

Un fumet de rôti s'éleva soudain et lui chatouilla les babines. Son estomac gronda, indélicat, rappelant qu'il avait jeûné tout le jour. Sur la table, une volaille, un gigot et une soupe au fumet de châtaignes s'étaient brusquement dévoilés. Diphylla se saisit d'une splendide aiguière de porcelaine et emplit avec grâce son verre d'une eau claire.

Lou ne se départissait pas d'une sorte d'instinct qui lui susurrait à l'oreille des mots ridicules, où il était question de fuir et de mordre, de nuit et de lune. Il se força à l'apaisement, et porta sa main vers les couverts, mais comme si soudain il n'en connaissait plus l'usage, il saisit finalement sa cuisse de pintade à pleine main et la croqua voracement. Ses hôtes, attentifs, le regardèrent dévorer les mets d'un regard satisfait, sans s'outrager de ses mauvaises manières. Le jus de viande coulait sur son menton, Lou se rendit compte qu'il arborait un appétit d'ogre. Il bascula ses oreilles vers l'arrière et entendit le trottinement d'un lapin, dehors, près du grand mur d'enceinte. Il ferma les yeux et un sentiment de culpabilité le déchira à nouveau. Il n'aurait pas dû se trouver là, au milieu de ces gens, il les mettait en péril. 
Quelle heure était-il donc à présent ?
Ses chairs filamenteuses et ses os durs se rappelèrent à son bon souvenir. Sa mâchoire, douloureuse, broya un os gros comme un petit doigt d'enfant sans qu'il ait à forcer pour en extraire la moelle dont il se régala.
— Mangez ! Entendit-il encore.
Alors il sombra dans une léthargie funeste.

Une chaleur d'étuve assaillit Lou. Il lui semblait que sa tête avait doublé de volume. Il s'agita faiblement et se rendit compte qu'il était suspendu au-dessus d'une cuve, attaché par les pieds, tête en bas, les mains liées dans le dos. Il se débattit plus fermement, mais son poids joua sur les cordes et les resserra toutes fort désagréablement. Il suffoqua. Son sang battait ses tempes. Il claqua des dents dans l'espoir d'atteindre ses liens, en vain. Il inspecta rapidement les lieux de sa vue aiguisée.

Trois mètres sous lui environ, dans une des granges attenantes à la maison, la famille Rotundus s'activait sans relâche autour d'une ample et étrange machine hors d'âge, composée de cuivre et de céramique. De longs tuyaux de diamètres variables descendaient ou montaient le long de cylindres cuivreux en d'abracadabrantesques circuits, rampaient le long de colonnes percées de manettes et de hublots, tournicotaient, s'enroulaient tels de gros boas paresseux autour de jarres antiques agrémentées de siphons et de filtres. De lourdes volutes de vapeur s'échappaient de la cuve principale qui se trouvait sous lui, telle un énorme bain-marie, et un feu en léchait avec gourmandise les parois métalliques. À sa droite, un col de signe en zinc plongeait dans un tube de verre épais d'une longueur démesurée filant jusqu'à terre. Il apercut quelques robinets, au bas d'une petite bonbonne. Des dame-jeanne gisaient pêle-mêle, un peu plus loin, le long d'un mur. La machine lui rappelait étrangement l'alambic de son père.

Tous ses poils se hérissèrent tandis qu'il suait à grosses gouttes. Il tenta à nouveau de se libérer, claquant des mâchoires, grattant les liens dans son dos de ses ongles calleux, pesant sur la bride qui le maintenait au-dessus de la machine infernale. Exaspéré, il s'efforça de deviner la voûte céleste, à travers le toit de tuiles brunes, puis se rendit compte que ses sens la lui criaient déjà : la pleine lune était levée, il était passé minuit.

Grand-père Yougi leva les yeux, des yeux jaunes aux pupilles sans âme. Avec effroi, Lou ne se souvint pas de l'avoir remarqué auparavant, et le vieillard lui fit un clin d’œil grotesque.

Diphylla toupillait des leviers, guettait des cadrans, tapait de ses mains délicates sur les hublots puis semblait réfléchir, le bout de sa langue rose entre deux canines proéminentes. Son attitude lubrique, manches relevées au-dessus de la cuve de cuivre, et son regard, soudain affamé, sur lui, achevèrent de terroriser Lou. Il hurla. Un hurlement sinistre, lupin, qui fit dresser les cheveux sur la tête des trois vampires besognant bravement autour de l'alambic.

— Ne vous agitez pas, cher voyageur, l'exhorta Desmond, un stylet à la main. Je vous promets une fin rapide, mon ami. La température est presque atteinte, nous allons pouvoir procéder, n'est-ce pas ma chère ?

Il coula un regard tendre à Diphylla. Dans le même temps, le vieux pompait vigoureusement l'air d'un soufflet, ce qui donna une vigueur nouvelle aux flammes sous la grande cuve. L'eau bouillonnait. Une humidité chaude et accablante pesait dans l'air. La jeune femme moulina de toutes ses forces une barre de bois, et à l'aide d'un treuil, fit descendre Lou au plus près des bouillons.

— Mais qu'est ce qui vous prend bande de demeurés ? les invectiva-t-il, le visage cuit par la vapeur.

— Nous distillons en ces murs, mon cher, un nectar de choix, lui confia Diphylla soudain suspendue le long de la corde où lui même pendait lamentablement, ballerine simultanément gracieuse et sinistre, vénale et vénéneuse, son corps parfait collé contre le sien, la bouche avide. Le stylet de son complice brillait à présent dans sa main.

Lou sentit ses muscles croître et sa peau entamer sa desquamation. Il gronda, son mufle râpeux contre la bouche gourmande.

En réponse, elle dévoila ses canines aiguisées.

A son tour, lou démasqua des crocs encore sanguinolents venant tout juste de percer de sa large gueule au museau et gencives à peine formés; sa transformation s'accélérait. Les deux bêtes aussi surprises l'une que l'autre, s'enlacèrent et tournoyèrent, suspendues dangereusement au-dessus du baquet. Diphylla cherchait son cou, pour le mordre, et dans un même temps, de son arme, tentait de lui trancher la gorge.

— Desmond, va l'aider, il se passe quelque chose, ordonna Yougi, qui n'y voyait guère, les deux compères étant embrumés par les émanations. « Il est temps de récolter son sang pour notre distillation, aide là ! Tranche sa gorge ! Commanda-t-il encore, furieux de ce contre-temps. »

Desmond opéra un surprenant bond de gazelle, s'accrocha à la corde, dominant le couple en furie, mais découvrit alors un hôte en tous points changé ! Le lycanthrope rugissait, toujours maintenu par ses liens aux pieds, mais il avait réussi à libérer ses mains monstrueuses et serrait à présent le cou de Diphylla dont les yeux charbonneux sortaient de la tête. Desmond saisit vivement le stylet de sa main, pourtant, il gâcha sa cible. Le loup-garou abaissa ses larges oreilles pointus, et ouvrit une large gueule en direction du vampire. Il manqua le bras de Desmond, mais acheva d'arracher la tête de Diphylla. Elle tomba dans la cuve d'étain, rebondissant sans fin contre les parois bombées.

Yougi, d'en bas, poussa un feulement strident, et découvrit une mâchoire acérée ; Lou répondit par un feulement lugubre en frétillant des vibrisses. Ses pattes ayant atteint une taille et une puissance fantastique, il se libéra enfin d'une poussée de la corde à laquelle il était suspendu et se récupéra sur un sol meuble, à côté du soi-disant grand-père. Il ne lui laissa pas le temps de réagir, et l'envoya dinguer contre l'alambic. Mais le vieux avait de la ressource, il ne fut guère déstabilisé par l'attaque et se reprit immédiatement, fonçant droit sur la bête, ses canines démesurées en avant.

Desmond profita de ce répit, récupéra la dague, puis avec fulgurance, plongea sur le loup-garou et, avec habileté, traça un sillon rouge vif sur la gorge du Monstre. Il fut cependant repoussé d'un brutal coup d'épaule. Le poil épais et la peau cornée évitèrent à Lou d'être transpercé. À peine une goutte de sang perla-t-elle. Mais elle rendit fou les deux suceurs de sang. Yougi quant à lui fut accueilli par les bras puissants du lycanthrope, qui l'enserra avec force jusqu'à l'explosion de sa squelettique cage thoracique. Un bruit de bouchon en résulta. Lou ne perdit pas une seconde et arracha la tête de la deuxième strige de ses griffes effilées. Elle rejoignit la cuve en tressautant.

La dernière goule, recroquevillée à terre, perdit de sa prestance et vit sa dernière heure arrivée. Elle rampa sous l'alambic, à l’abri des coups de pattes furieux de Lou, se ramassa, et saisit un outil dans sa main. Le loup-garou agrippa la jambe de Desmond et l'attira avec force hors de son refuge. Mais il comprit son erreur lorsqu'il vit dans la main du dandy une longue tige d'argent. Il eut tout juste le réflexe de se porter de côté, et de rouler au loin. Le vampire se projeta de nouveau auprès de lui avec promptitude, sa lance improvisée en avant ; le lycanthrope ne se laissa pas surprendre une deuxième fois. Il fit un bond qui l’amena derrière sa proie et lui rompit l'échine en un instant, puis de sa gueule puissante, il raccourcit le dernier vampire avec acharnement, dans un déferlement de rage. La troisième tête rejoignit bientôt ses compagnes.

L'alambic était chargé.

Lou sourit, un sourire sardonique, puis il ferma habilement le couvercle de la cuve de sa grosse patte noire et vérifia les cadrans. Il n'y avait plus qu'à attendre que le condensateur fasse son œuvre.

La bête songea avec philosophie qu'elle n'avait jamais goûté ce genre de nectar. Mais peut-être qu'il serait bon de déguster cette étrange liqueur, dans la bibliothèque, au coin de la cheminée. Il n'avait plus besoin de rejoindre des montagnes perdues à présent, il avait trouvé son havre.
