---
title: Vous avez un message
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: ""
epub: true
pdf: true
draft: true
---
Seize heures cinquante minutes

Emma lutte un instant avec la serrure, la porte refusant tout d'abord de s'ouvrir puis la jeune femme fouraille de nouveau la clé d'un geste rageur, les bras encombrés de paquets quand soudain le penne cède ; la porte s'ouvre toute grande sur un vestibule à la peinture jaunasse. Posant la clé dans le vide poche émaillé en forme de canard sauvage, à côté du portable de son père, elle pousse la porte pour la refermer adroitement avec son pied. La sonnerie de son téléphone retentit, assourdie, car il se trouve au fond de sa poche. Elle prend le temps de poser ses affaires sur le sofa, dans le salon et sortant le smartphone le pose sur le coin de la commode qui jouxte le canapé. Le mode haut-parleur est activé. Elle se débarrasse ensuite de la bouteille qu'elle tient dans la main, la rangeant dans le vieux meuble-bar en chêne près de la porte.

Elle appuie sur la touche « messages » et l'appareil se met à débiter d'une voix monocorde :

– Vous avez 7 messages enregistrés. 

DING ! *Huit heures vingt-cinq minutes*

— Bonjour ma chérie c'est maman, décroche si tu es là. Tu n'es pas là ? Bon écoute, je sais qu'on s'est quitté un peu fâchées la dernière fois mais il faudrait qu'on en reparle. Je te rappelle dans la journée. Je t'embrasse…[pause] Il m'est arrivé un truc étrange ces jours-ci, j'ai eu plusieurs fois l'impression d'être suivie.[pause plus longue] De te le dire ça paraît complètement ridicule, je suis sous pression en ce moment, oublie ça. [rire forcé].

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 6 messages enregistrés.*

La jeune femme fronce les sourcils, encore tout au contenu du message puis sursaute : quelque chose respire dans cette pièce. Elle entend un souffle. Elle n'en croit pas ses yeux ! Son père se tient tout au fond du vaste salon, installé confortablement dans son large fauteuil en cuir. La pénombre qui règne l'avait dissimulé jusque là. Il triture dans sa main un cigare, non déballé, et fixe sa fille étrangement.

— Bonsoir Emma.

— Bonsoir papa. 

Elle s'approche, la démarche féline, un sourire pincé aux lèvres.

DING ! Le téléphone poursuivit son soliloque. *Onze heure quarante-cinq minutes*

— C'est encore maman. Je pensais que tu m'aurais rappelé ce matin; ne fais pas l'enfant Emma, si tu réfléchis un peu, tu n'as pas à être en colère, ce sont mes décisions, je te demande juste de les respecter, après tout, ton père est d'accord non ? A ce propos, tu l'as vu dernièrement ? Tu es là ? [un bruit de trousseau de clés qu'on agite]. Encore une chose bizarre, tu vas dire que je psychote; depuis quelques jours, c'est un calvaire de faire fonctionner la porte d'entrée [pause]. Je me demande si la serrure n'a pas été forcée. Je ne me rappelle plus si ton père possède encore un passe. C'est idiot mais bon, pas tant que ça, enfin, parfois quand je rentre à la maison, j'ai l'impression d'une présence, comme si quelqu'un avait visité les lieux en mon absence. [soupir] Décidément ce vernissage me stresse. Appelle s'il te plaît. 

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 5 messages enregistrés.*

Le père et la fille s'observent sans broncher jusqu'à la fin du message puis la jeune femme s'approche déposer un rapide baiser sur la joue rugueuse de son géniteur. Il a mal vieilli, songe-t-elle. Son visage est marqué des rides qui creusent ses traits les rendant lourds et disgracieux. Sa chevelure poivre et sel, coupée court, vire au blanc, tout comme son teint, trop hâve, maladif. Ses gestes sont devenus lents, empruntés, son corps empâté. Emma se retient de tout commentaire et revient vers le bar.

— Un petit whisky ?

— Un double, je suis crevé, peut être que ça va me remettre d'aplomb. Ces séances de relecture, ça me bouffe ! 

Son père passe tous ses mercredis après-midi à relire ses plaidoyers. Il est avocat, et non des moindres, car il possède un certain talent oratoire qui l'a bien servi dans quelques affaires délicates.

La jeune femme prend la bouteille et sert généreusement une double rasade.

— Tu ne prends rien ? Mr Prodhon la regarde sans sourire, un léger tic au coin de la paupière supérieure droite le fait inconsciemment cligner de l’œil, rendant son regard étrange, déplacé.

— Plus tard, peut-être. Je pensais te trouver à l'atelier… 

— Et bien j'en suis revenu depuis quelques minutes. Pour une fois, je me suis donné la permission de terminer plus tôt, se justifie-t-il. Il lui semble qu'elle est fâchée.

— je vois » dit-elle. Il lui parait contrarié.

DING ! *Quatorze heure deux minutes*

— Emma je suis sûre que tu as eu mes messages [pause longue, bruit de porte qu'on claque]. Je vais faire un tour en forêt, parcours habituel. J'en ai pour une paire d'heures. Tu peux me joindre à tout moment.[des pas frappent les marches d'un escalier]. J'aimerais vraiment qu'on se parle à cœur ouvert. Sans ton père, ce sera mieux, je crois qu'il n'apprécie que modérément mes choix. Euh… j'ai oublié de te dire, on m'a rayé ma voiture, tout le côté gauche. Avec une clé ou un tournevis m'a dit le garagiste. Irréparable, il faudra changer la carrosserie.[une autre porte, plus lourde, claque] Bon allez je te laisse, à tout à l'heure »

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 4 messages enregistrés.*

Elle soupire, tend le verre à l'homme et celui-ci se permet une question tout en saisissant l'alcool ambré :

— Merci. Que me vaut ta visite ? Un rapport avec les problèmes de ta mère et de sa voiture ? 
On sent une certaine ironie dans sa voix grave.

Emma jette un œil distrait vers le téléphone et vient s'asseoir sur un fauteuil voltaire à l'angle opposé de la pièce, près du couloir, non loin du sofa.

— Non, du tout. Maman a toujours un problème ou un autre avec sa voiture. 

Son père tourne son whisky dans son verre en cristal, change de position comme s'il avait soudain découvert qu'il se trouve assis sur un clou.

— Il faudrait qu'on reparle… 

— Je ne suis pas venu pour ça, on en a déjà parlé et reparlé. Et maman a pris sa décision, je pense qu'il est inutile de revenir encore là-dessus.

— Et bien il me semble…

Le téléphone s’immisce dans la conversation comme un malotru.

DING ! *Quinze heures et douze minutes*

— Emma, [respiration hachée, le son est de mauvaise qualité] il faut que tu viennes. Emma, écoute bien… [pause suivis de halètements] écoute bien ce que je te dis, je [pause bruits inaudibles] je me suis fais tirer dessus, du sang, [pause longue, un soupir suivi d'une toux rauque] j'ai du sang partout, on m'a tiré dessus, je ne sais pas qui, [pause, les mots sont inaudibles]… Fusil… [pause encore des mots incompréhensibles suivis de raclements]… ien vu. Je vais [bruits de gorge, toux rauque, gémissements]… je vais essayer de me traîner jusqu'au petit pont. Tu vois où c'est ? Emma ? Emma ? Tu sais où est ton père ? »

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 3 messages enregistrés.*

Mr Prodhon grimace À la fin de la lecture du message, il lève les yeux et scrute le visage de sa fille.

Emma lui rend son interrogation par un haussement d'épaule, sa mère est toujours un peu à la limite de la crise hystérique, une manière de se faire plaindre.

— Encore du cinéma, lance-t-elle.

— Ne manque pas de respect à ta mère. Tu passes trop souvent les bornes ces derniers temps.

— Et toi non ? Cette secrétaire, ce n'était pas une borne à ne pas dépasser peut-être ?

— Le divorce est une option raisonnable, il y a longtemps que ta mère et moi …

DING ! *Quinze heures et cinquante-sept minutes*

L’avertissement fait désagréablement écho dans le salon où la tension est soudain montée d'un cran. L'homme s'est à demi levé et sa fille également, se penchant vers un grand sac de sport qu’elle a apporté avec elle.

— [respiration rapide entremêlées de pleurs et geignements] Emma, bon sang où es-tu ?… Ton père ne répond pas, vous le faites exprès ou quoi ? Je n'ai pas pu aller [pause, bruits in-identifiables comme des crissements]… Je n'arrive plus à marcher, trop de sang [pause, longue toux, sanglots] je vais crever là, Emma, répond ! Répond ! [pause, le téléphone semble coupé puis]… secours mais je ne sais pas s'ils ont compris [silence prolongé]… ttention à toi, ma petite fille »

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 2 messages enregistrés.*

La petite fille en question se concentre à présent sur les gestes de son père. Il a posé son verre à terre, ses yeux aux iris noirs, soudain agrandis, observent la jeune femme, sa main comme amollie, glisse subrepticement dans son gilet de veston. Quelle manie de toujours porter un costume même un jour de repos ! C'est d'un pédant, songe Emma.

— Rappelle ta mère immédiatement, je pense que c'est grave. 

Le téléphone trône sur la commode, le son, réglé au plus fort, impudique, métronome mécanique, rythme un terrible mélodrame. La jeune femme semble hypnotisée. Immobile, elle dévisage son père, effarée, comme s'il représentait une menace, et seule sa main s'évade dans le sac, cherchant frénétiquement quelque chose.

DING ! *Seize heures trois minutes*

— Emma [chuintements] j'ai besoin de toi, je [toux et sanglots]… fatiguée [toux plus longue]… e reposer un peu [toux et râles]… t'attendre, je t'aime Emma, [pause] pas normal…[pause longue puis des râles]… on père… comprends pas…[coupure] »

*Pour rappeler, taper 5, pour effacer, taper 2. Vous avez 1 messages enregistré.*

— Emma ! Le cri de Mr Prodhon fuse lorsqu'il aperçoit le canon du fusil de chasse dépasser du sac. La jeune femme déverrouille l'arme et la charge sous le regard incrédule du vieil homme qui lâche son briquet en or de sous sa veste et porte les mains devant lui en un geste presque comique et dérisoire de supplication. Emma vise son père sans ciller et le mépris qu'il lit alors dans ses grands yeux noisette lui rend la scène douloureusement compréhensible. Bien sûr ! Elle n'a accepté ni leur séparation, ni sa trahison, et surtout… Il a du mal à réfléchir, ses pensées, ralenties, se bousculent aux frontières de son cerveau embrumé de whisky… Il regarde son verre incrédule.

À présent, Emma met en joue son père qui regarde affolé le canon noir de l'arme. Dressée telle une furie, elle gronde :

— Je n'accepte pas qu'à cause de toi maman verse sa fortune et ses tableaux à une association de protection canine ! » Tonne-t-elle en déchargeant l'arme sur son père. Un coup en pleine tête. Un seul, à bout portant.

Sa rancœur explose dans sa tête, écho qui pulse et grandit, grandit, jusqu'à remplir tout son être.

Ce soufflet que ces parents, indignes, lui ont mis, à elle, leur fille unique, en la déshéritant, prétextant qu'il y avait assez des possessions de son père pour faire d'elle une petite rein ! Et tous ces tableaux qui valent une fortune ! L’œuvre de sa mère ! Et elle n'y aurait aucune part ? Une bile aigre lui remonte à la gorge, amenant une larme, une seule, roulant le long du sillon naso-génien avant de s'écraser sur son giron.

Mr Proudhon s’effondre dans son fauteuil, s'y tasse. Du sang imbibe la tapisserie fleurie derrière lui en une tache gluante et rosée, immonde. Emma positionne l'arme entre ses mains : le coup reçu presque à bout portant ferait l'affaire, se rassure-t-elle.

L'homme a reconnu son fusil… mais trop tard. La jeune femme approche le sac du mort, il contient le nécessaire de nettoyage de l'arme, sa sacoche de protection et une boite de cartouche. Elle se saisit du verre, le fourre dans son propre sac, et l'échange contre un autre, qu'elle a collecté lors d'une de ses précédentes visites, ainsi que le fusil.

La bonne idée que ces passe-partout qui lui permettait d'entrer et sortir comme un fantôme des résidences de ses parents et de se tenir au courant de tous leurs petits secrets ! Elle sourit. Son plan était parfait, sa réalisation également.

DING ! *Seize heure vingt quatre minutes*.

Une voix d'outre-tombe, débite son ultime lugubre message.

— [cliquetis, des sons ressemblants à des frottements] Emma je… je [toux déchirante]… poumons…[toux et gémissements]… ourir ici ! Pas… toi [pause, les mots se perdent dans une toux déchirante]… on père où… as possible [pause, on entend des pleurs] Emma … [bruit sourd, la liaison est brusquement coupée] »

*Pour rappeler, taper 5, pour effacer, taper 2. Vous n'avez plus de nouveaux messages.*

Emma esquisse une moue. La garce a mis du temps à rendre l'âme. Elle pouffe, relâchant ses nerfs mit à rude épreuve et appuie enfin sur  *effacer tous les messages*.

Elle fourrage une dernière fois dans le sac de sport, dans la boite de cartouche, il manque deux munitions. Une pour son père et une pour sa mère ;

— Bien fait ! Assène-t-elle au cadavre.

Sa mère… Elle n'a eu aucun mal à la faucher dans la petite descente, très isolée, non loin du petit pont tandis qu'elle s'évertuait à courir en maintenant une petite foulée… Bon, elle n'avait pas prévu que cette gourde mettrait des heures à mourir… et lui enverrait autant de messages.

Cependant cela ne la perturbe pas, ni ne l'afflige, c'est juste une légère contrariété. Sa colère et son dépit sont trop grands, son appétit de richesses également, au point qu'aucun remord ni sentiment autre que le travail bien fait ne peut venir la tarauder en cet instant.

Pour parfaire son petit scénario, elle sort de sa poche une lettre que son père lui a écrit il y a quelques mois, après son incartade fatale avec Jenny, sa jeune et dévouée secrétaire. Il s'excusait et demandait comment se faire pardonner … Voilà des mots que la police lira et traduira de fait avec une grande limpidité.

Emma sourit.

Et d'ici peu, fortune. Il lui suffit de récupérer le brouillon de ce testament ignoble, dans l'atelier de son père…

Elle traverse le salon et se dirige vers l'atelier : C'est une grande structure carrée, d'un seul tenant, entièrement réaménagé en bureau confortable, cheminée grandiose, bureau de ministre, bibliothèques en chêne courant le long des murs de pierres. Elle franchit le seuil de ce nid d'aigle, scrute l'obscurité et se dirige droit vers le bureau. Le manuscrit est négligemment posé, ouvert. Le feu testament de sa mère, qui la dépossédait de tout, exemplaire unique, s'expose, fragile, devant ses yeux. Elle se saisit du dossier, le referme sans y jeter plus d'un regard, et le dépose au milieu des brandons rougeoyants, dans la monumentale cheminée de briques. En un instant, il se recroqueville, léché avec gourmandise, puis, dévorer par les flammes ; il disparaît. La jeune femme ne s'attarde pas, ne jette pas un regard en arrière, elle fait rapidement le chemin inverse.

Arrivée dans l’entrée de la maison, elle passe une main gantée sur le dos du canard bleu et jaune et récupère le double du jeu de clé qu'elle a fait réaliser.

Elle s’imprègne de la scène, fixant pour toujours dans son esprit l'image de ce vieil homme effondré au creux de son fauteuil, un sang rouge sombre gouttant à présent sur le parquet aux chevrons centenaires. Il faudra faire récurer tout ça, songe-t-elle. Elle respire profondément, prête à jouer demain la malheureuse fille éplorée.

DING ! Emma sursaute et consulte la pendule murale.

*Dix sept heure vingt-quatre*

Telle une éructation impromptue, la sonnerie du portable laisse la jeune femme pantelante, le cœur battant. Elle se saisit de l'appareil mais laisse son doigt en suspens, incapable de répondre.

La sonnerie cesse bientôt. Le répondeur prend instantanément le relais.

*Vous êtes sur la messagerie de Mlle Proudhon, je suis indisponible pour le moment, laissez-moi un message et je vous rappellerai.*

— Bonjour, Serge Dolokian, avocat à la cour, je tenais à prendre contact avec vous. En effet, votre mère, Mme Tarkkian, ex Mme Proudhon, a révoqué votre père, Mr Paul Proudhon en tant que son avocat et représentant légal. De ce fait, je suis depuis ce matin, en charge du dossier de succession de votre mère, et nous avons rédigé un nouveau testament qui n'est pas encore finalisé. Je souhaiterais que nous nous rencontrions afin que vous preniez connaissance du document dans son entièreté, comme le souhaite votre mère. Rappelez-moi sans tardez, chère mademoiselle, afin que nous convenions d'un rendez-vous… 
