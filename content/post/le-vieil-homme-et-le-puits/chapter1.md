Voici contée l'histoire du vieil homme et du puits, installez-vous confortablement, et laissez-vous emporter…

L'aurore pointe à peine ses ardents rayons en ce début d'été et déjà une forte chaleur écrase la campagne. De sa maisonnette de pierres blanches, sort, ce matin-là, comme à chaque aurore, Grand-père en salopette et chapeau de paille, muni d'un arrosoir en zinc. Il prend lentement le chemin de cailloux qui borde sa maison, abandonne ses confortables chaussons et enfile ses croquenots passé le coin de sa demeure puis, poursuivant sa route en souriant, admire quelques pâquerettes aux délicates pétales immaculées qui tapissent le gazon. Chien, à sa suite, flaire les pistils poudrés de pollen des pivoines écarlates bordant le sentier, tandis que Merle, lisse son plumage bleu comme la nuit de son fin bec orangé, jasant à tue-tête depuis le lierre grimpant accroché à la maison. De son perchoir improvisé, l'oiseau jouit d'un point de vue imprenable sur les environs, observant Chat, tout de blanc vêtu, assis tranquillement sur un muret, l'oreille et l’œil aux aguets. Grand-père s'engage alors d'un bon pas sur le chemin de terre menant au jardin. Il s'arrête devant son potager, les mains sur les hanches, admirant choux, carottes et salades.

« Peste ! Mes salades » bougonne-t-il, et il saisit délicatement Escargot entre ses doigts, se mettant à lui faire la leçon sans perdre en rien sa bonne humeur :

« Ah, petit galopin, tu prends un peu trop tes aises, mon ami, va donc voir ce pissenlit ! les salades sont pour mon propre usage ! »

Ainsi, tout en faisant la causette, le vieil homme conduit sieur Escargot dans un coin de verdure, loin des plantations de légumes. Il reprend ensuite sa route, caressant les tulipes multicolores à peine écloses, qui dodelinent leur cap au gré du vent, le long du sentier, et s'amuse des facéties d'Orvet, louvoyant un peu plus loin, entre les hautes herbes.

« A cache-cache mon petit, tu es perdant chaque fois ! Ta peau d'argent miroite sous le soleil naissant, elle m'éblouit ! » s'exclame-t-il gaiement à l'attention du petit serpent, tout en poursuivant toujours sa marche à travers le jardin. Orvet, découvert et timide, se glisse alors sous les larges pierres d'un puits apparu au détour de la sente. Il va rejoindre Salamandre, dont la demeure se cache dans les profondeurs, sous la terre. Grand-père n'en prend pas ombrage, chaque jour, le même rituel matinal réjouit ses sens : Les parfums saturent l'air vivifiant et ravigotent le plus endormi ou le plus bougon, le chant joyeux des oiseaux ravit l’ouïe, et les soyeuses pétales des fleurs chatouillent la paume en un charmant salut. Les yeux peinent à saisir tout ce qui habite ce petit coin de paradis, et bientôt, une pure merveille, sur la langue, le plaisir de sentir la fraîcheur de l'eau en se désaltérant au puits ! Voilà une vie simple remplie de bonheurs simples, se félicite Grand-père, heureux.

Arrivé à hauteur de la margelle, il pose son arrosoir à terre et saisit un seau qui gît de guingois, accroché à une corde. Au centre du puits, se trouve une antique potence de bois munie d'une poulie où la corde se loge parfaitement dans la gorge de la poulie ; ainsi, pour puiser de l'eau, il suffit de descendre le seau relié à la corde tout au fond du puits, le remplir puis le hisser, gouttant d'eau ras le bord, à la force des bras. Un jeu d'enfant !

La matinée s’égrène lentement, à charrier l'eau, du puits au seau, et du seau à l'arrosoir sous le regard tranquille de Chat, Chien et Merle batifolant autour du maitre de maison. Le vieil homme, jovial et infatigable, encourage ses légumes, répandant l'eau bienfaitrice, puis flatte les arbres fruitiers, versant un généreux arrosoir de-ci, de-là, sans oublier les merveilleuses roses, au fond du pré et les pivoines le long de l'allée et … la ronde se poursuit, chaque plante arrosée à sa juste mesure.

Enfin, c'est l'heure du petit déjeuner ; les fleurs ont les pieds baigné de frais, les fanes des carottes luisent d'un beau vert, les salades et les choux font bonne mine, encore parsemés de gouttelettes. Les feuilles du pêcher, aux fruits encore immatures, brillent et bruissent dans la brise. Chaque jour, Grand-père s'en retourne comblé, après son dur labeur, et chaque jour, sans relâche, sous un soleil de plus en plus brûlant, alors que l'été sera bientôt à son apogée, il arrose invariablement jardin et potager.

Et puis un matin, c'est la consternation.

« A sec ! » l'écho du puits renvoie la voix fluette de Salamandre atterrée.

Grand-père, tout d'abord surpris, ne veut y croire et envoie le seau au fond du puits, persuadé de remonter bientôt le précieux liquide. Mais force lui est de constater que le seau cogne sur le sable là tout en bas !

« Attention ! Je suis là moi ! » crie Salamandre de sa minuscule voix.

Imperturbable, Chat, depuis la margelle, un rien penché au-dessus du vide en acrobate hors pair, passe sa patte sur ses oreilles pointues, une fois à gauche, une fois à droite.

« Demain, c'est sûr il y aura de l'eau ! » affirme le vieil homme ragaillardi à la vue de son matou, contemplant ses pivoines dont les belles têtes ébouriffées penchent à terre, alanguies.

Le lendemain, Chien se précipite dans le jardin, sa truffe hume l'air déjà lourd et cuisant du matin, alors que du fond du puits,

« A sec ! » geint Salamandre dépitée, de sa voix fluette.

Orvet et Escargot renâclent et vite, se terrent au frais, sous les pierres. Mais il y fait bien peu frais, on étouffe.

Grand-père se gratte la tête puis regarde le ciel, très bleu, sans nuage. Il observe ensuite le sol, tâte de sa main l'herbe devenue sèche, sans la rosée du matin quand soudain, un discret vent se lève, et quelques bourrasques soulèvent le chapeau de paille du vieil homme. Celui-ci reprend espoir, il s'écrie,

« Ah ! Mes amis, août nous met à rude épreuve, mais l'orage vient, sentez cet alizé qui se gonfle ! Demain, c'est sûr, il y aura de l'eau ! »

Le lendemain matin, Merle alerte la petite troupe depuis le lierre aux feuilles racornies et jaunies qui lui sert de gîte dès qu’apparaît notre jardinier.

« A sec » assène Salamandre de son antre à peine humide.

Cette fois, tous les habitants de la maisonnée sont réunis autour du puits ;

Chat s'allonge sur la margelle cherchant le frais, Chien halète à l'ombre de son maitre, pendant que celui-ci se désole des salades du potager aux larges feuilles pâmées.

« Mon beau jardin ! » soupire-t-il tristement. Du creux des larges pierres, Orvet et Escargot défaillent. Un soleil de plomb étend ses terribles rayons.

Grand-père sort sa pioche et se décide à descendre dans le puits, pour creuser plus profondément, afin de trouver de l'eau et venir en aide à ses petits amis et à ses plantes qu'ils ne veut pas voir dépérir encore.

L'échelle passée par-dessus bord, il se glisse avec difficulté dans le conduit étroit, encouragé par Chat qui miaule doucement depuis la margelle et Chien qui aboie frénétiquement et tourne en rond autour du vieux puits.

« On n'y voit guère » souffle Grand-père, « es-tu là Salamandre ? »

« Tout au fond, maitre des fleurs, sur un coin de roche à ta gauche » guide-t-elle de sa minuscule voix.

Grand-père pose précautionneusement un pied à terre, puis l'autre, et se penche vers la petite salamandre noire et jaune, dont les yeux le fixent désespérément.

« A sec ! » constatent-ils ensemble, et leurs voix caverneuses résonnent et font écho tout le long du conduit, remontent jusqu'à la surface où elles se répercutent encore, lugubres.

« A sec, à sec, à sec, à sec… »

Quelques coups de pioches maladroits plus tard, force est de se rendre à l'évidence ; l'eau a déserté le puits !

« Qu'allons-nous faire ? » se lamente Salamandre le lendemain matin.

« Il nous faut chanter » scande Merle, piétinant fébrilement de ses petites pattes grises la vieille potence de bois, « les chants attirent les nuages curieux ». Et le voilà qui babille, flûte, babille encore, tandis que chat tente un contre-alto et que chien pousse un aboiement sonore. Chacun y met du sien et joint l'orchestre improvisé. Orvet se tortille dans l'herbe sèche, faisant tintinnabuler des cosses des gros pavots secs, Escargot cogne sa coquille brune sur les pierres et Grand-père, ému de ce vacarme pastoral, entame une chanson de son enfance, pour accompagner tout ce tintamarre où s'il n'est question de puits, il est question de cascade.

« Qu'à cela ne tienne ! » Songe-t-il, dans un regain d'optimisme, « demain, c'est évident, avec tout ce raffut, viendrons à nous de beaux nuages gorgés d'eau »

Le lendemain pourtant,

« A sec ! » se désole Salamandre du fond de son antre.

« A sec ! » reprennent en chœur Chien, Chat, Escargot, Merle, Orvet, les fidèles compagnons du vieil homme, réunis autour du puits comme au chevet d'un malade, levant leurs yeux vers un ciel bleu sans nuage et un soleil implacable.

« A sec ! » s'écrie Grand-père atterré, la tête entre les mains, assis sur la margelle du puits. Il n'ose plus parcourir son jadis verdoyant jardin ;

Les tiges des pivoines s'affaiblissent et ces dames s'étalent sur le sol, leurs habits de fêtes éparpillés autour d'elles, rêches et cassants. Les cosmos affichent grise mine, leurs couleurs de mauve et de roses s'étiolent et les marguerites, au petit coeur jaune rabougri, tentent de se cacher parmi les herbes. Plus loin, dans le potager, salades autant que choux sèchent sur leurs racines assoiffées, et leurs feuilles anémiées se ratatinent. Même les carottes se rident et leurs fânes gisent amollie sur la terre brune aride et craquelée.

« Quel désastre ! » se lamente Grand-père.

« Quel désastre » gémissent Chat, Chien, Merle, Escargot, Orvet et Salamandre de sa frêle voix qui semble sortir d'un tombeau, réfugiée tout au fond du puits où il fait à présent une tiédeur anormale.

Grand-père fouille dans sa mémoire ainsi que dans le grenier : il y découvre une baguette de sourcier qui appartenait à son propre arrière-grand-père, et qui n'a pas servit depuis que le puits a été découvert, il y a fort longtemps. Il explique à ses compagnons d'infortune que ce simple bâton de bois possède une utilité précise : il indique les sources d'eau cachées dans les profondeurs de la terre.

« Il est envisageable que l'eau circule quelque part, sous nos pieds, peut-être cette canicule l'a fait changer son cours, ou un autre événement géologique minuscule l'a détourné de notre vieux puits. Allons à sa recherche mes amis ! » décide Grand-père, se jetant à corps perdu dans cette ultime tentative.

Sous cette chaleur, les jours suivants, chacun des habitants de la maisonnée va et vient de la maison au puits et du puits à la maison, en procession, derrière Grand-père muni de sa baguette. Il la tient devant lui, bras tendus, et arpente chaque centimètre carré du jardin et du potager dans un sens puis dans l'autre. Il espère qu'elle s'agite soudain et pique vers le sol ayant enfin trouvé trace du précieux liquide. Mais la procession se répète en vain jusqu'à épuisement.

Tout est dévastation !

Le pauvre Orvet est retrouvé, langue pendante, au milieu des campanules elles-même pâmant. Grand-père se résoud à lui prêter abri, au fond de sa remise, dans l'ancien cuvier à charbon.

L'arrosoir repose, depuis un certain temps maintenant, abandonné, inutile, près du potager et le seau, pendu à la corde, reste tristement vide.

C'est un paysage sinistré qui accueille le vieil homme lorsqu'il sort de chez lui, aussi en vient-il à limiter ses excursions. Il envoie Chat ou Chien en reconnaissance et hélas, l'émissaire s'en retourne invariablement avec ce maudit mot ;

« A sec ! »

Lorsque Merle piaille, tous savent d'avance sa rengaine ;

« A sec ! »

Salamandre est mal en point, sa voix s'étiole et on entend à peine son cri poussif ;

« A sec ! »

« C'est le plus terrible été de toute ma vie » déplore Grand-père, sans ressource aucune pour sauver son jardin.

Chat reste affalé sur les pavés dans la maison, hébété. Chien halète constamment, trouvant refuge sous les meubles. La situation est désespérée !

Une nuit, alors que la journée s'est encore avérée plus rude et plus sèche que la précédente, et que le vieil homme, constatant la faiblesse d'Escargot, s'est résolu à lui accorder asile, à lui aussi, au fond de la cave, notre histoire va prendre un tour bien inattendu.

Grand-père ne pouvant dormir, inquiet de la situation qui chaque jour s’aggrave, chausse ses godillots et, vêtu de son pyjama à carreaux bleus, sort sous la voûte céleste confier son tourment à la nuit.

Il marche jusqu'au puits, s'assoie sur la margelle, admirant les étoiles et la lune, face ronde et lisse, qui parait si proche, si proche qu'avec la main il pourrait la toucher, lui semble-t-il.

La nuit n'est pas noire, tous ces astres éclairent la peine du vieil homme ; il a l'air plus vieux, abattu par ce méchant tour du destin. Chat, l'a rejoint à pas de velours et se cale dans son giron. Chien se couche à ses pieds, après lui avoir copieusement lécher la main.

« A sec ! » pleure Salamandre, du fond de son repaire.

Orvet, Escargot, et Merle restent silencieux.

Grand-père les regarde, désolé.

« Mes petits amis, je ne sais plus quoi faire, j'ai si peur qu'il vous arrive malheur ! »

D'un coup, il lève la tête vers les nuées, très en colère.

« Pourquoi n'entendez-vous pas un vieil homme sans ressource ? Toi ! Ciel sans nuage ! Et vous ! Vous qui brillez, inutiles et indifférentes à notre sort, soyez maudites ! » et il leur montre son poing ridé, puis la tête dans les mains, se met à sangloter doucement.

Alors Chien hurle comme ses ancêtres loups, un hurlement terriblement triste, dans le plus profond de la nuit ; il parait s'amplifier et couvrir toutes les directions en un écho douloureux.

Dame Lune, perchée tout là-haut, encore somnolente, tourne sa large bouille vers le petit groupe.

« Plaît-il ? » gronde-t-elle d'une voix de rocaille.

Surpris, Chien stoppe net ses glapissements.

Stupéfait, Grand-père essuie ses larmes, relevant la tête, incrédule.

Chat, Orvet, Merle, Escargot et même Salamandre, se mettent alors à exposer la situation à la noble dame, et celle-ci, miraculeusement, dans la cacophonie générale, comprend la nature du problème.

« J'y vois une solution fort simple » conclue-t-elle.

« Laquelle ? » questionnent en chœur homme et bêtes.

« Dame Lune approche sa généreuse rondeur du puits, et tous s’aperçoivent qu'elle a aux coins des yeux, deux larmes d'argent.

« Votre malheur m'émeut, dit-elle encore, en conséquence de quoi, je vous offre ceci, et plus jamais le soleil, mon capricieux mari, ne pourra vous contrarier »

Elle se penche encore, et lorsqu'elle est à l'aplomb du puits, deux grosses larmes roulent, roulent le long de son visage blafard puis, avec un « ploc, ploc » retentissant, tombent au fond du puits.

« Jardinier » dit-elle d'une douce voix à présent, « continue à prendre soin de tes amis, qu'ils soient animaux ou plantes, soigne-les toujours mieux que toi même » puis, vivement, l'astre reprend sa place dans la voûte sombre, et de nouveau il semble indifférent aux affaires des hommes.

 « De l'ea ! De l'eau… » s'exclame avec entrain Salamandre de tout en bas, vite descendue constater par elle-même ce prodigieux événement.

Chacun se penche et se bouscule : En effet, l'eau miroite tout au fond du puits, au clair de Lune.

« Ah ! Le jardin va retrouver ses couleurs » se réjouissent ses habitants et le vieil homme, avec entrain, se met sitôt à puiser l'eau fraîche et claire, arrosant les fleurs assoiffées, les grimpants aux tristes mines, la pelouse jaunie. Sans relâche, il s'active jusqu'en matinée.

Le résultat ne se fait pas attendre : Les plantes reprennent vie, exposant aux visiteurs à deux, quatre ou sans patte, leurs plus beaux atours.

Ainsi, bientôt, de nouveau, Orvet se faufile avec délice dans l'herbe tendre, Merle se perche dans l'émeraude d'un lierre feuillu et piaille à tue-tête, Escargot fouit sous la mousse humide, Chien baguenaude par les sentiers du jardin, humant le parfum des pivoines et des tulipes ragaillardies, Chat penche la tête par dessus la margelle admirant son reflet étincelant à la source provide et Salamandre s'endort, tout au fond du puits, bercée par le réconfortant clapotis de l'eau !
