---
title: Fantôme du passé
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
date: 2021-02-07
summary: "Dig et fall sont des archéologues, ils parcourent les galaxies, explorant les planètes inconnues qui croisent leur chemin. Mais le sont-elles toutes vraiment ?"
epub: true
pdf: true
draft: false
---
Ils descendirent le long de la faille. Leurs longs tentacules adhéraient sans difficulté à la paroi lisse. Fall imprima dans l'esprit de son collaborateur sa hâte de parvenir en bas. Dig lui répondit en accélérant la cadence et ils allongèrent leurs prises telles deux araignées pressées à tisser leur toile entre deux murs. Le signal battait leur tempe. Sur l'écran de leur vaisseau d'explo, il était apparu d'un coup, tel un fanal sur une mer d'huile. Les deux créatures touchèrent le fond de la crevasse et d'un même mouvement, non concerté, admirèrent le ciel au-dessus d'eux. Là d'où ils venaient, il n'y avait pas de nuages batifolant dans l'azur. Les capteurs indiquaient un taux de radioactivité mortel pour toute vie carbonée. Se réceptionnant sur ses ergots, Dig accueillit sa collaboratrice avec ce qui pouvait être un sourire mental, chez cette espèce étrange. Haut de deux bons mètres, les tents avaient une tête proéminente, de petits yeux blancs, les oreilles et la bouche n'étaient plus nécessaires, ils communiquaient uniquement par la pensée. Dotés d'un corps trapu monté sur deux pattes puissantes se terminant par trois ergots rétractiles ayant principalement un rôle dans l'équilibre et le déplacement horizontal sur plan, ils pouvaient se révéler une arme redoutablement affûtée. Au niveau du torse, six tentacules extensibles d'un nacré quasi transparent, agrémentées de ventouses rose pâle, servaient à la préhension.

Plus bas, le signal palpitait, les appelant, irrésistiblement. De leur cabine de pilotage, ils avaient aperçu cette planète d'un jaune sale, recouverte de volcans et de déserts brûlants dans sa grande majorité. Quelques îlots montagneux aux pics vertigineux, semblaient plus accueillants, dans le sens où leur engin pouvait envisager de s'y poser si la Tour leur accordait le privilège de se renseigner sur ce signal mystérieux et c'est avec enthousiasme qu'ils reçurent l'avis favorable à leur requête après seulement un millième de seconde d'attente. La Tour se trouvait en orbite stable à plusieurs millions de kilomètres de là. Les tents habitaient dans de grands cargos, car c'était un peuple de voyageurs, de découvreurs. La Tour hébergeait les dirigeants de ce peuple nomade. De là partaient toutes les décisions, et vers elle toutes informations recueillies se voyaient automatiquement dirigées.

Les deux explorateurs s'injectèrent alors leur ration quotidienne de bioproTECHTour3, la qualité supérieure, une chance quand on songeait au nombre croissant de tents survivant avec des rations de base type 1, vendues une fortune, et pourtant, représentant à peine de quoi exécuter son ouvrage sans être défaillant. Ils procédèrent ensuite aux check-up de routine avant de s'équiper pour leur sortie. Une mission simplissime.

À présent, Fall se concentrait sur le son basique et lancinant qui la narguait : aucun ordi-cerveau n'avait pu après analyse, trouver l'origine de l'émission. Elle imprima à Dig sa certitude que la provenance du « tac tac tac » répétitif se cachait sous la couche de basalte sur laquelle ils stationnaient actuellement. La tour les avisa être en phase recherche sur cette planète connue sous le nom de e-85623F. Une planète en voie d'effacement.

Les ancêtres de Fall et Dig n'avaient pas toujours été des colonisateurs ; en des temps immémoriaux, c'étaient des sédentaires. Un cataclysme à l'échelle de leur système solaire, les avait obligés à fuir. Depuis lors, chaque fois que l'occasion se présentait pour les explorateurs de poser les pattes sur une planète, l'excitation était palpable. Et voilà cette planète qui émettait un signal ; c'était inespéré. Si leur recherche s'avérait positive, ils pourraient probablement tous deux intégrer la Bande. La Bande habitait dans la Tour, elle réunissait les plus perspicaces des explo et les mieux lotis.

« L'entité nous appelle » avait suggéré Fall lors de l’atterrissage de leur petit vaisseau.

– Mon analyse ne mentionne pas cette éventualité, l'avait tancé mentalement Dig.

Une entité. Jusqu'ici, toutes les entités mises au jour avaient été annihilées sur ordre de la Tour.

Fall et Dig recevaient invariablement un avertissement : « **non comestible** » et ils balançaient la sauce. Jamais ils n'avaient eu l'occasion de tomber sur quelque chose comme ça. Oh non. En trois cents ans, jamais.

À l'aide d'un thermo-sondeur, Dig entreprit de visionner le sous-sol. Ses yeux affichèrent les relevés topographiques, et il constata qu'une cave était visible 896 m en dessous d'eux. Le tents copia l'ensemble des données et transmit le tout à sa collaboratrice ainsi qu'à la Tour.

Fall commençait déjà le forage. La mini tête foreuse émit un chuintement et perfora la pierre comme une motte de beurre, il s'agissait à présent de la laisser creuser sur 895,8 mètres : Autant ne pas risquer d’abîmer ce qui se logeait dans le trou, sous la dalle de basalte.

Dig admira Fall ; d'une grande dextérité, elle se servait de deux tentacules pour manier la foreuse tandis que ses autres bras repoussaient la roche que Dig réduisait en poudre avec sa masse à neutrons.

Au fur et à mesure qu'ils s’enfonçaient dans la roche, les deux tents s'agrippaient le long du tunnel que formait la foreuse. Dig alluma son falot, car de sombre, la cavité devenait à chaque mètre parcouru d'un noir plus opaque, abyssal et oppressant.

« Nous y sommes » commenta mentalement Fall.

– Je scanne, l'informa son collaborateur. Ses yeux d'un blanc laiteux s'agrandirent, papillotèrent et explorèrent la zone sous leurs pieds. Le « tac tac tac » était devenu assourdissant, proche, attirant, hypnotisant.

« Regarde, on dirait un vieux blockhaus ou un abri anti-nucléaire de l'ère anté-Tour » lui dit-il dépité.

– j'aperçois la chose qui émet. C'est… c'est une roue crantée, je crois.

– Tu plaisantes ? Il imprima trop durement sa déception et la tents le repoussa mentalement sans ménagement. Son cerveau battait la campagne.

– C'est une roue crantée. Elle a été inventée… commença-t-elle à raisonner, fouillant dans ses nomenclatures à la recherche d'indices.

– Je sais quand elle a été inventée ! Il avait crié mentalement.

C'était d'une grossièreté !

Fall lui fit sentir son incongruité et poursuivit d'une voix de pure analyste.

« Si une roue crantée émet ce « tac tac tac » elle se meut, si elle se meut, une entité doit forcément l'y aider. Scanne la cave à nouveau. »

Dig émit sa plus grande désapprobation en scannant vulgairement, sans tact ni précaution, au grand dépit de sa collaboratrice.

Celle-ci ignora l'attitude immature de Dig et régla la foreuse au minimum. La machine rabota les vingt derniers centimètres de roche, qui se révélèrent être de béton armé, un matériau non sécurisé, interdit depuis des lustres. Sous eux, dans le silo, le « tac tac tac » claquait en signe de bienvenue alors que se révélait par la brèche pratiquée, une salle de quatre mètres sur cinq dont les murs entièrement couverts d'une peinture blanche scintillaient légèrement sous la lueur froide du falot. On y apercevait des débris, objets tombés en poussière, reliquats d'un monde et d'un temps depuis longtemps révolu. Au fond, un grand coffre de plastique, prônait. Il paraissait intact.

Fall contempla la boite, une impossibilité physique en soi, constata-t-elle, pour son collaborateur autant que pour elle-même. Rien ne résiste ainsi au temps.

Elle agrandit le trou jusqu'à ce qu'il lui laisse passage et sauta dans la salle. Ses ergots résonnèrent à travers les murs de sinistre façon et la roue crantée cessa sa course.

La Tour, alertée, interpréta ce phénomène comme potentiellement dangereux et renforça le niveau d'alerte passant de 3 à 2.

Fall n'en tint pas compte, soudain, elle savait pourquoi elle était là : Dans cette boite jaune faite de pétrole fossile, se cachait un trésor. Un trésor qui se révélait à elle aujourd'hui.

Dig tenta d'engager une auto-correction, sa collaboratrice ne semblait plus en mesure de maîtriser cette découverte. Elle paraissait subjuguer par cette caisse d'un jaune rutilant qui gisait dans le fond du bunker.

« Refus ? Comment ? » Il suffoqua mentalement de tant d’impertinence. « La curiosité te retourne les neurones Fall, je te demande de te désengager de cette mission. »

La suggestion fut renforcée par l’émission d'un ordre similaire de la part de la Tour.

Fall ne prit pas en compte les vociférations mentales des uns et des autres, elle effleura la roue crantée, qui se désagrégea sous ses ventouses sans qu'elle n'en ait compris le fonctionnement interne, ce qui la déstabilisa. Cependant, elle reprit sa progression rapidement, se doutant que, sous peu, Dig ne lui autoriserait plus ses propres décisions.

Elle racla le sol de ses ergots, traînant derrière elle ce qui ressemblait à une toile peinte sans y prêter la moindre attention. Dig la contourna et s'approcha vivement de ce qui avait été un lavabo, d'après son catalogue interne, pendouillant lamentablement au coin gauche du bunker mais à peine l'eut-il frôlé de sa ventouse que l'affreuse petite chose explosa dans une nuée de poussière blanchâtre. Il fit face à Fall.

« La Tour nous demande d'annihiler ce container. »

– Je veux voir ce qui est dans ce coffre, Dig.

Le tents scanna la boite. Il envoya une onde mentale apaisante et Fall lui en fut reconnaissante.

« Rien de dangereux » constata Fall.

– Rien de comestible, fut la réponse de Dig ;

– Ça, j'en doute. Elle fit levier sur le couvercle du caisson jaune après avoir ôté les verrous de part et d'autre et celui-ci se décrocha comme une mâchoire au terme d'un bâillement trop vif. À l’intérieur, se trouvait,

« Des sachets de plastique… » imprima Dig, au comble de la stupeur « leur niveau de pollution rend mon compteur complètement dingue. »

– Il y a quelque chose dans ces sachets, je lance une analyse, rétorqua fall.

Elle se concentra et l'effort mental déployé lui permit d'informer la Tour et dans le même temps de saisir des bribes d'informations qui ne lui étaient pas destinées.

« Pousse-toi, je vais éliminer ces déchets. Le plastique est une matière… »

– Hautement toxique, je sais, imprima sévèrement Fall dans l'esprit de Dig.

Elle tourna et retourna délicatement les petits sachets dans ses tentacules ; dans ses rêves programmés les plus fous, elle n'aurait pas osé cela.

« Ce sont des graines. Regarde, il y a les restes d'un herbier au fond. »

– Un quoi ?

– Un herbier. Les humains de la planète Terre, notre actuelle e-85623F, y dessinaient les plantes qu'ils rencontraient dans la nature et parfois y ajoutait des échantillons. Il y a un accès aux vieilles banques de données, sur le cargo VII, le premier de la flotte à être parti en explo, le plus vieux, parfois je scanne quelques informations là-dedans, c'est trop souvent peu compréhensible, pourtant, on ne sait pas, au cas où… je scanne.

Mais Dig n'écoutait pas cette logorrhée mentale, il évacua de son cerveau les pensées confuses et intrusives émises par sa collaboratrice et confirma l'ordre de la Tour bourdonnant en périphérie de la zone parasite. Il dégaina son arme laser et abattit sa collaboratrice, entrée apparemment en phase rétrograde. Elle s'effondra mollement, répandant les petites graines autour d'elle, comme autant de larmes, rondes, vertes, grises, noires, orange, d'autres minuscules en forme d'étoiles, d'épis, tant et tant encore, que Dig piétina sans vergogne. Il scanna Fall ; elle était inactive. Il lui arracha son convecteur de données et tourna les ergots.

La Tour entérina mentalement l'acte de destruction, et engagea la procédure suivante afin de désintégrer le blockhaus.

Il restait peu de temps à l'explorateur pour s'extirper du trou, remonter le tunnel puis grimper le long de la paroi qui, à présent, sous la voûte étoilée, luisait comme un soleil. Mais le tents n'était pas sensible à ce genre de choses.

L'ordre « **non comestible** » était parvenu, il fallait s'en tenir aux ordres.

Peut-être, un jour, lui et sa nouvelle collaboratrice, découvriraient-ils enfin de quoi nourrir la population tents qui lentement succombait à la faim et la soif, dans des cargos devenus tombeaux.

Il décolla au moment où une forte explosion émanait de la planète jaunâtre.
