Au commencement

— Laisse-les petit, ce n'est plus qu'un tas de viande froide. Il faut filer.

J'écarquillai les yeux pour tenter d'apercevoir mon sauveur au cœur de la nuit : Je ne distinguai qu'une ombre. Au loin, on entendait des grincements et des claquements lugubres.

— Dépêche-toi, ils ne vont pas laisser tomber l'affaire.

Il posa sa main sur mon épaule. On aurait dit une main de robot, ferme, faite pour écraser. Il perçut mon hésitation, grogna, et me chargea sur son dos comme un vulgaire sac de pommes de terre. Je n'eus même pas la force de protester.

Nous traversâmes les ténèbres accompagnés par de lointains murmures sortis de bouches invisibles À dire vrai, je bénis cette obscurité. Revoir ces choses me regarder avec gourmandise aurait suffit à me tuer net je crois À peine incommodé par mon poids, mon sauveur parcourut ce qui me sembla des kilomètres sans jamais trébucher ou hésiter, nous montâmes puis descendîmes nombres d'escaliers, et je m'abîmais en moi-même durant cette fuite sinistre. J'avais perdu le décompte du temps lorsqu’il me déposa enfin sur un sol dur et froid.

— Nous sommes momentanément hors de leur portée.

J’acquiesçai dans le noir.

— Ferme les yeux, commanda mon sauveur.

Un crépitement retentit, et au travers de mes paupières, je vis qu'il faisait jour à présent. Pour autant, je maintins mes yeux clos, savourant le silence, et le fait de ne plus être ballotté sur des épaules étrangères. Sans considération pour mes aspirations personnelles, il m'ordonna :

— Ouvre les yeux.

Ce que je fis précautionneusement. La lumière crue d'une lampe plafonnière me fit plisser les yeux. Et je le vis lui, véritable géant dans cette minuscule pièce aveugle où nous étions réfugiés. Ou plutôt je vis son ombre, car il se tenait devant la source lumineuse. Une ombre qui me fit frissonner. Grande et forte, sa silhouette se découpait dans l'éclat du jour artificiel et je me tassai malgré moi devant cette évidence : J'étais à sa merci. Cet état des lieux hâtif m'empêcha de lui montrer la moindre reconnaissance. Ce dont il ne sembla guère s'offusquer À sa main droite, pendait une vieille paire de lunette infra-rouges. La réponse à la question qui m'avait taraudé lors de notre rencontre. Qui était-il s'il n'était pas l'un des leurs pour être lui-même nyctalope ?

— Ça va ?

Il se pencha vers moi et l'ombre me parut soudain si menaçante, que je me rencognai encore dans les murs de parpaings bruts tel un insecte sous la menace d'un talon.

— Tu n'as aucune crainte à avoir.

Il hésita, puis son vieux visage s'éclaira d'une grimace.

— Pour l'instant.

Je le découvris enfin, comme il s'assit à mes côtés. Grand, je l'ai déjà dit je crois, vraiment grand, pas loin de deux mètres, et musclé sous sa combinaison dont la matière me fit tiquer. Du cuir ? On aurait pu penser à une combinaison de ski, mais elle était trop près du corps, et pourtant, à le voir évoluer, elle ne gênait aucunement ses mouvements. Son visage portait les traces de combats, de petites cicatrices blanches, à peine perceptibles au milieu des rides. Elles traçaient de profonds sillons le long de son nez, et de part en part de la mâchoire, carrée. Des yeux, gris, doux, me regardaient avec intérêt et complétaient une figure somme toute aimable, auxquelles je ne m'attendait pas. Pas ici. Pas maintenant. Je fus incapable d'évaluer son âge. Il sortit d'une poche fort bien dissimulée de son vêtement, une petite gourde qu'il me tendit.

— Bois !

Ma méfiance céda devant l’étendue de ma soif. Je m'en saisis et bus goulûment.

— Nous allons repartir. Pourras-tu marcher à présent ?

— Je… un filet de voix sortit de mon gosier comme je lui rendais son eau. Je tentai de l'affermir sans vraiment y parvenir. « Je ne crois pas que je pourrais aller comme vous. Je n'y vois rien dans le noir »

Je n'avais pas terminé mes coassements qu'il me tendait une paire de lunettes à vision nocturne.

— Sais-tu l'ajuster ?

Oui, j'avais déjà eu l'occasion d'en porter, quand j'étais là-bas. Où déjà ? Je n'arrivai plus à m'en souvenir. Il n'attendait pas de réponse, heureusement, mais m'enfonça les lunettes sur le front. De sa ceinture, il détacha un long poignard — qui ne tenait pas la comparaison avec le sabre maintenu par un large ceinturon croisé et tressé, passé en travers de son dos — et me le tendit.

— Et ceci, sauras-tu t'en servir ?

Je hochai la tête. Évidemment, avais-je envie de crier, sinon je serai mort depuis… epuis longtemps. Mais je restai silencieux.

Il comprit, se releva et me tendit la main, elle était effectivement longue et large, gantée de cuir brun un peu passé. Les coutures, grossières, marquaient chaque phalange. Il y avait un logo sur chacune. Je tiquai. Il perçu mon trouble. Enfin, je crois que oui, que c'est ce qui le poussa à se présenter. Il se peut aussi que j’interprète, et que son intention est toujours été de s'identifier. Que risquait-il, dans tous les cas ?

— Je m'appelle Saint John Amoura, et je vais essayer de te tirer de là.

— Vous êtes l'un d'eux ? Dis-je de ma voix de corbeau mal dégrossie.

— J'appartiens à la sécurité civile, oui.

Je déglutis avec difficulté. La SC n'était pas une légende alors ? Je réfléchis à vive allure, mais ne trouvai rien de plus à lui demander. C'était trop invraisemblable.

— Prêt ?

Je crus bon de me présenter à mon tour, je savais, au fond, que c'était inutile, qu'il était là pour moi, il y en avait tellement à sauver, sa présence ne pouvait pas être fortuite, pourtant, cela me sembla important sur le moment.

— Je m'appelle Craine, Moriel Crane.

— Chausse tes lunettes, Moriel Craine, et sois prêt à déguerpir aussi vite que tes jambes te porteront. Ne me quitte pas d'une semelle. Compris ?

Je hochai la tête. Il tourna l’interrupteur et l'obscurité fut sur nous, presque palpable. Mon pouls s'accéléra. Je rabattis les lunettes sur mon visage, les sanglant au plus près, mon coutelas en main. La porte s'ouvrit sans un bruit, et nous dévalâmes des rampes étroites et humides conduisant dieu sait où. Expression au combien idiote, quand j'y pense. Il y a bien longtemps que dieu à abandonner cet endroit maudit.

Au fur et à mesure de notre course à travers la ville, mes jambes se dérouillèrent et sans jamais espérer le battre à la course, je restai dans le sillage de St John Amoura, me doutant qu'il avait adopté une allure adaptée à mes forces.

Les faubourgs apparurent. Nous avions abandonné derrière nous les gratte-ciels, les parkings sous-terrains, et les rues étroites où se tassaient de vieux immeubles aux façades lépreuses.

Nous courions à présent sur la large voie d'un échangeur autoroutier. La pénombre s'était matérialisée en une atmosphère fibreuse, une présence quasi physique qui donnait l'impression d'avoir à être forcée pour nous accorder le passage. St John ne ralentissait pas l'allure et comme je commençai à fatiguer, je jetai rapidement un œil derrière moi. Après tout, si personne ne nous poursuivait, on pouvait peut-être continuer en marchant.

Où allait-on d'ailleurs ? Au château ?

Mes pensées s'interrompirent brutalement car au coin de mon œil quelque chose de flou modifia la consistance de cette purée de pois nommée nuit À travers les lunettes, je n’identifiais pas la forme qui avait traversée mon champ de vison, mais la cataloguait comme ennemi jusqu'à avoir la preuve du contraire. J'entendis le son caractéristique d'une lame que l'on tire de son fourreau, comme dans les films de capes et d'épées de ma jeunesse. Sauf qu'à présent, il n'y avait plus rien de noble dans le geste, et plus rien de ludique non plus, c'était une question de survie. Rien que ça. Instinctivement je cherchais mes pistolets, mais cela faisait belle lurette que je ne les possédais plus, sans savoir pour quelle raison, aussi fus-je heureux,en un certain sens, que St John m'eut fait don de ce poignard un peu ridicule. On aurait dit un katana miniature de dessin animé. Je l'agitai sans conviction tout en continuant de progresser sur l'échangeur, filant le train de mon sauveur.

St John s’immobilisa le sabre en main et instinctivement, je me mis dos à lui. L'air se chargeait déjà d’effluves électriques, et des formes trapues se stabilisèrent bientôt dans ce magma gris presque gluant comme une pâtée de riz. Les grincements et grattements résonnèrent désagréablement à mes oreilles. Les choses nous cernaient.

St John Amoura fit tournoyer sa lame et un cri strident retentit : l'une d'elle avait été touché ! Les grincements redoublèrent. La nuée se resserra autour de nous. Je plongeai sur l'une des formes qui tentait d'abattre son proboscis sur mon pied tout en prenant soin de l'éviter. La créature disparut dans le magma brun qui nous entourait. Je constatai avec une certaine joie malsaine que tous mes réflexes ne m'avaient pas abandonné, et avec une certaine candeur que nous pourrions bien réchapper de cette attaque, car St John abattait sa lame avec la régularité d'un bûcheron, et rare étaient les cibles qu'il manquait. Je suais à grande eau sous mes lunettes et ma vue devint moins précise tandis qu'une autre créature me lançait sa trompe venimeuse à hauteur du thorax. Mon vêtement ne me protégeait en rien. Mais mon sauveur me poussa brutalement et reçut la lancette mortelle à ma place dans le revêtement épais de sa combinaison. Je tanguai, récupérai mon équilibre, et jouait à nouveau du couteau sur chaque tentative ennemie. L'adversaire sembla déstabilisé, c'était ou jamais le moment de tenter une percée.

— Parton ! Souffla St John saisissant l'opportunité.

Et sans m’attendre, il se précipita sur l'échangeur en direction du tunnel. Je ne pus retenir un geignement puéril en voyant la direction qu'il empruntait mais n'en rompit pas moins le combat et m’élançai derrière lui. J'aurai pu lui dire que j'en venais, mais à quoi bon ? Il le savait sans doute. Probable qu'il n'y eut plus d'autre chemin. Pourtant, des routes s'offraient partout à notre regard. Et les champs, les bois autour, mais depuis bien longtemps, la plupart des voies n'étaient plus sûres. J'aurai moi-même eu du mal à me souvenir de la dernière fois où j'avais eu l'occasion de traverser une forêt sans danger… En un sens, malgré mon appréhension envers le tunnel, je constatai qu'au moins mon intuition était la bonne : nous nous dirigions vers le château.

Semant nos poursuivants, — à moins que ce ne soient eux qui aient décidé d'abandonner un combat à l'issue trop incertaine —, nous reprîmes notre train d'enfer, et avançâmes un long moment sur l’autoroute semée uniquement d’embûches mécaniques. Quelques blindés automatisés ayant perdu la boule tiraient sur tout ce qui bouge, mais nous eûmes largement le temps d'anticiper l'interaction. Des squelettes d'oiseaux morts balayèrent notre vision nocturne et ils craquèrent bientôt sous nos pieds sans que nous ralentissions notre allure, tandis que des ultrasons saturaient l'air par l’intermédiaire des gros hygiaphones plantés en haut de pylônes longeant l'autoroute.

L’aéroport se trouvait juste derrière cette portion de route, et le tunnel que nous devions rejoindre se situait lui juste à la sortie de l'aéroport.

— On va se trouver un moyen de transport, me glissa St John, ralentissant légèrement pour que je me retrouve à sa hauteur.

Je réfléchis à ce qu'il venait de me dire mais je ne comprenais pas où il voulait en venir. Plus rien ne fonctionnait. Nous ne trouverions que machines inutiles à l'aéroport. Avec le risque d'une concentrations d’ennemis. Ils aimaient les endroits sombres et confinés, même si la nuit ne nous lâchait jamais vraiment en vérité. Une nuit sale, — saupoudrage de différents gris —, rarement d'un noir d'encre, sauf à certaines heures. Une nuit éternelle qui nous digérait lentement.

Je sentis sa main frapper mon dos. Cela me fit du bien. Par moment, je sombrai dans une malencontreuse déprime qui me rendait inefficient À pleurer. Je renonçai à m’appesantir sur mon sort, et accélérait l'allure. St John me doubla sans effort, et à peine un crissement ou un chuintement lointain vint perturber notre course jusqu'à l'abord du parking de l'aéroport. Nous avions définitivement laissé le tissu urbain derrière nous.

En poussant les barrières automatiques pour accéder au parking, St John déclencha une alarme et un son strident nous déchira les tympans. Des centaines de carcasses d'automobiles gisaient, définitivement hors service, sur les emplacements qui leur étaient réservés. Des panneaux rouillés indiquaient l'entrée de l'aéroport. On apercevait l'immense cage de verre du grand hall, tout au fond du parking, légèrement surélevée. La vision me fascina.

On aurait pu se dire qu'on avait fait un bon bout de chemin, même si ma destination finale me semblait obscure, accompagné de ce type étrange et pas vraiment bavard. Une relative confiance me saisit, quel fou je fus ! Et elle faillit bien m’ôter la vie, une vie que St John Amoura s’évertuait à sauver, coûte que coûte.

— Avance ! Avance !Me tança-t-il.

Il poursuivit sa course effrénée, sans un regard en arrière, mais quelque chose attira de nouveau mon regard et je stoppai net alors que tout mon corps me disait — Cours !—, mais déjà mon esprit n'était plus en capacité d'analyser ce qui s'offrait à mes yeux. La vue de ce grand hall me happa totalement. Elle me renvoya une autre, puissante et vivifiante, animée des vies de voyageurs pressés, de manutentionnaires survoltés, de voitures klaxonnant, de gamins hilares, de mères au bord de la crise de larmes, d’hôtesses accueillant cette foule fébrile et bientôt si morte…

Si bien que je n'ai pas vu s'approcher subrepticement quelques ombres molles déterminées à me faire rejoindre leur rang, subjugué par ce spectacle imaginaire tandis que l'un d'entre eux affirmait sa prise sur ma cheville, crépitant d'une joie mauvaise à l'idée de dévorer tout cru cette chair pleine de sang, palpitante, et offerte, qu'un venin aurait rendue soumise.

On m'arracha brutalement à ma rêverie létale alors que je me sentais défaillir sans opposer la moindre résistance, et de nouveau, je brimbalai sur l'épaule de St John Amoura. Je repris un semblant de contact avec la réalité en avisant avec horreur le proboscis planté au niveau de ma cheville dénudée, exerçant une ultime danse macabre avant de pendre, raide, au bout de ma jambe. Hystérique, je fus pris de convulsions, mais elles n'eurent pour effet que de resserrer la prise de mon sauveur sur mon pauvre corps disloqué sans ralentir son allure. Nous nous engouffrâmes dans le hall. St John pris immédiatement la direction de l'embarquement, il connaissait les lieux, et sans jamais ralentir, n'hésitait devant aucune bifurcation. J'avais la nausée et je voyais trouble, ou plus exactement, je ne voyais plus grand-chose à travers mes lunettes. Un étau enserrerait ma poitrine, et je compris que le venin du monstre était en train de me tuer, très probablement. St John siffla entre ses dents,

— Tiens bon !

et après un dernier couloir, enfonça une porte — salle d'attente —. Elle claqua en se refermant. Il me posa à terre.

— Retire tes lunettes.

Ce que je fis sans attendre, et grand bien m'en pris car aussitôt la lumière fut sur nous, dévoilant des fauteuils orange et d'autres violets. De grandes baies vitrées avaient été remplacées par des panneaux métalliques À l'autre bout de la salle, une autre porte soigneusement barricadée et soudée sur son chambranle nous fermait toute retraite. Je vomis sur ma veste. Une odeur écœurante envahit le local, mais St John ne parut pas le moins du monde indisposé par mes régurgitations. Il sortit de sous sa combinaison un objet que je pris tout d'abord pour un stylet, mais qui s'avéra être une injection. Tirant sur ma manche, un carré de peau apparut et l'homme me piqua sans douceur.

— C'est bon. Dans quelques minutes, tu seras sur pieds. Bois !

Bien que je n'en eus nulle envie, mon estomac faisant encore le yo-yo, je m'appliquais à faire ce qu'il attendait de moi. J'en versais un peu à côté.

— Fais attention, je n'ai que ça ! Son regard réprobateur m'inquiéta un peu. Que voulait-il dire par là ? L'eau était-elle une denrée rare ? Je n'arrivais pas à m'en souvenir. Je ne me souvenais pas de grand-chose d'ailleurs. Dans quelle ville étions-nous ? Et pourquoi cet homme s'acharnait-il à me tirer d'affaire ?

— On va ressortir par cette trappe, ça nous mènera en bout de piste, juste à côté des garages où sont stationnés de petits avions privés. Si on arrive jusque là, ça ira. Bois !

Il parlait d'une voix monocorde, plus pour lui-même que pour moi, je crois. Je me suis pourtant permis une question :

— Mais pourquoi tout ça ?

— Ma mission, c'est de te sortir d'ici, Moriel Crane, et si on y arrive, ça changera beaucoup de choses. Bois !

Je fis ce qu'il me demandait et me sentis un peu rasséréné. Il réajusta sa combinaison, en sortit des gants, et m'en passa une paire.

— Ne touche à rien dans le tunnel avant d'avoir mis ces gants.

— Quoi ?

La lumière s'éteignit,et sans surprise, il me tapa sur le crâne,

— Fais ce que je t'ai dis !

Je me levai, encore un peu abasourdi par le poison, le sol tanguait légèrement, mais les nausées avaient disparues. J'enfilais gants et lunettes sans attendre, mon couteau passé dans ma ceinture.

— Je ne peux pas te porter là-dedans, c'est très étroit, commenta mon sauveur, alors démerde-toi pour rester en vie !

Son injonction me parut lugubre, et encore plus lorsque je distinguai de nouveau grattements et chuintements derrière la porte. Le tunnel. Je frissonnai sous les réminiscences qui affleuraient quelque part, dans mon esprit, et pourtant trop loin de la surface encore.

Saint John Amoura tira une clé, un peu comme une clé de borne à incendie, d'une des poches de sa combinaison, et je ne pus m’empêcher de songer qu'il était d'une rare prévoyance. La clé ouvrit sans peine le couvercle de la trappe et nous nous tortillâmes à l'intérieur du goulot, attrapant une échelle fixé le long. Pas un mot ne fut prononcé durant la descente. Saint John devant, je suivais laborieusement. Soudain, il n'y eut plus de barreau et mon pied fut happé par quelque chose. Je poussai un cri de souris.

— Tais-toi ! Je te tiens, agrippe-toi à moi, et une fois à terre, baisse-toi. Nous abordons le tunnel. il faudra ramper, ce ne sera pas long, passe devant.

Comme je ne bougeai pas, il me posa et dans le même temps me donna une bonne poussée. Je me cognai au plafond, serrant les dents pour ne pas crier de nouveau et commençai ma reptation. Je l'entendais juste derrière moi. Il m'encouragea une ou deux fois, avec un entrain de robot qui me glaça les os, avant d'annoncer,

— Nous sommes arrivés.

Je le vis en effet se dresser à la verticale, et de nouveau, après quelques barreaux d'échelle, une trappe s'ouvrit. Nous émergeâmes dans la nuit d'un hangar immense. St John Amoura sortit une fois de plus des clés de sa combinaison, petites et fines cette fois, et les fit osciller devant moi. Je trouvai que son habit était une vraie corne d'abondance, et pourtant, il lui collait si près du corps que je ne comprenais pas où toutes ces choses se cachaient l'instant d'avant qu'elle soient sous mes yeux.

— Celles-là m'ont coûté bonbon, tu peux me croire, Moriel, dit-il sur un ton de confidence. Pourtant les mots me parvenaient étrangement sans substance, comme s'il ne lui appartenaient pas. « Allons ! L'avion est juste là, derrière ce jet. Un petit bimoteur jaune et vert. Ok ? »

Je ne savais pas quoi répondre à ça. — Merci — me parut tout aussi ridicule que — combien de mètres à parcourir ? — et pire que le stupide — Ah bon — générationnel.

— Tu sais piloter ? Me contentai-je de demander.

— Depuis la dernière mise à jour, oui. En route !

Je n'eus guère le temps d'épiloguer là-dessus.

Il y eut de nouveau ces drôles de grincements. Mais leur nombre parut d'un coup démultiplié.

À ce moment-là, je me rendis compte que quelque chose ne tournait pas rond. Mon sauveur également, car sabre au clair, il tira de sa poche un pistolet minuscule, qu'il me tendit en disant,

— J'aurai du m'en douter, ces saloperies sont passées par les gaines d'alimentation de chauffage et nous ont tendu une embuscade.

Il semblait dépité mais se redressa pourtant de toute sa hauteur, arborant fièrement son sabre, qui se mit à luire d'un éclat bleuté.

— Ça ne suffira peut-être pas à les arrêter, dit-il de sa voix neutre, comme désincarnée. « Et toi, ajouta-t-il en me regardant droit dans les yeux, « économise les charges, tu n'as droit qu'à trois coups. Attends qu'ils soient tout près pour tirer »

Dans la pénombre, je voyais des ombres, des ombres agitées et laides s'avancer et toujours ces bruits dans l'air qui me hérissaient le cuir chevelu, et faisait dresser mes poils le long de mon épine dorsale.

Puis une alarme stridente se déclencha au dessus de nos têtes, et contre toute attente, un panneau lumineux énorme troua la nuit et barra soudainement le passage entre notre salut et l’ennemi. Dans un bel ensemble, nous arrachâmes nos lunettes, un instant éblouis mais St John Amoura ne paraissait pas vraiment surpris : il grogna cependant quelques insanités qu'il n'est pas utile de répéter ici.

Un instant oublieux de l'assaut des monstres, je lus la pancarte qui tournoyait, sans comprendre, — ou n'osant pas comprendre — et envisager l'impensable.

Nos attaquants lançaient déjà leur proboscis, grattaient le sol, chuintaient de manière épouvantable et st John Amoura maniait de nouveau son arme avec une dextérité et une rapidité hallucinante, quasiment inhumaine. Il dégomma quelques spécimens trop empressés, et comme j'avais les yeux à demi fermés, je m'empressai de retarder le moment de tirer avec mon mini pistolet. Il tailla encore un monstre en pièce, mais il en arrivait des quatre coins du hangar.

— Tire !

L'ordre résonna non loin de moi.

Je haussai les épaules. Je n'ai jamais aimé les armes. Elles me font peur. Puis, m'indiquant de la main le grand panneau qui tournoyait dans ce qui me semblait le vide, St John se dirigea résolument vers la pancarte tout en repoussant les créatures. D'une nouvelle poche de son vêtement, il récupéra quelques billes qu'il lança parmi nos attaquants. Elles explosèrent dans un bel ensemble, dégageant une fumée épaisse qui repoussa momentanément nos ennemis.

— Vite, il faut rallier la sauvegarde ! Je suis à court de munitions. Aucune vie supplémentaire. Pas de bonus. Une prochaine fois, Moriel Crane ! Je te sauverai la prochaine fois ! Dit-il et il n'y avait ni pitié dans cette voix,ni empathie au fond de ces yeux clairs. Il se propulsa vers la pancarte rutilante, en me répétant son ordre,

— Tire !

Je me pris à le détester. Il m'apparut alors que c'était un sentiment d'une rare justesse. Oui, je détestais Saint John Amoura en vérité.

Le panneau clignotait toujours, et le décompte qu'il affichait, amorcé dès son apparition, filait sans nous attendre. Ça disait :

« Votre temps d'écran est à son terme. Point de sauvegarde N° 89. Il vous reste 20 secondes pour sauvegarder votre partie. 19, 18… »

Les bruissements des monstres me vrillaient les oreilles.

J'appuyai sur la détente, sans savoir qu'espérer. Le minuscule pistolet envoya une déflagration de tous les diables à travers le hangar.

Ayez pitié de moi !
