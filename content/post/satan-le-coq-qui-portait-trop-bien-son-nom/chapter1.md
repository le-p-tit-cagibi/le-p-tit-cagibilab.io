Le gars tenait son chapeau mou d'une main, l'autre en visière sur ses yeux. Il faut dire que c'était une fameuse journée de printemps, un franc soleil émoustillait l'atmosphère, le drapé bleu du ciel, sans rayures ni moutonnement était si pur qu'il tapait l’œil douloureusement. Un vieillard, trapu, chauve, et attifé d'une antique blouse de coton gris râpée qui lui tombait aux genoux s'approcha de l'homme au chapeau d'une démarche chaloupée, qu'une canne de noyer tentait de contrer sans réel succès. On aurait pu croire à un vieux navire sur le point de sombrer, sans ce regard d'ébène empli de malice.

— Vous cherchez quelque chose là-haut ?
— J'admire votre girouette, monsieur, à vrai dire, c'est la première fois que j'en découvre une si admirable ! Cette élégance, ce mouvement gracieux, et puis cette matière ! Tout à fait hors du commun ! Extraordinaire, même dirais-je.

Le spectateur ne bougeait pas d'un pouce, toujours le nez en l'air.

— Comme je vous comprends, dit en hochant la tête le vieux monsieur. Il leva sa canne en direction du sommet du clocher, et poursuivit, « Aucune girouette n'est aussi vivante et réaliste que la nôtre, c'est sûr ! »

Il abaissa son bras, le bâton au bout ferré claqua sur le parvis mais le vieillard ne l'entendit même pas, pris dans l'incessant ressac de ses souvenirs ; l'un d'entre eux battait à présent tout contre la falaise de l'oubli. Il le laissa refluer, et l'écume des petites choses enfouies l'emporta bientôt, loin, vers un passé qu'il s'autorisa à revivre cette fois, peut-être était-ce la dernière fois, vu son âge, ses guibolles qui ne le supportaient plus et sa tête, un peu plus vide chaque jour.

On entendait une mouche voler. En fait, il y en avait plusieurs, dans la salle de classe, bourdonnant grassement autour des fenêtres, se cognant aux carreaux avec un zonzonnement énervant. Mais au-delà de ce petit cirque naturel d'une poignée d'insectes, le silence régnait. Les élèves s'appliquaient sur un devoir de grammaire, les uns tirant la langue, les autres interrogeant leur mine de stylo, espérant sans doute y trouver quelques bonnes réponses. Au mur, la pendule indiquait dix heures, loin, très loin de la récréation. Un doigt se leva. Le maître d'école, installé confortablement à son bureau, abaissa son journal, la mine fripée comme sa feuille de chou, le crâne luisant, les yeux d'un bleu profond. Mr Guidiccelli apostropha le garçon à la main levée.

— Quoi encore Bonifac ?

L'interpellé rougit, baissa le nez en marmottant.

— Répétez, Boniface, en détachant les syllabes, je vous prie, on dirait une chèvre en train de brouter ! Ordonna l'instituteur, parcourant la salle de son regard orageux, ce qui stoppa net les jacassements.
— Je ne comprends pas la question quatre, Monsieur…
— Eh bien, ça nous change, Boniface, vous n'aviez pas compris les précédentes non plus, mais au moins vous avancez dans votre devoir.

Le maître se leva, descendit de l'estrade, posant à regret son journal. C'était un petit homme râblé, mat de peau, nerveux, sanguin, un corse pour tout dire. Il s'approcha de l'élève Boniface et relut avec lui l'intitulé du problème. Le garçon, un grand rouquin aux yeux verts, à la mine épanouie, mais sans intelligence, louchait sans succès vers la feuille de sa voisine et tapotait sa copie désespérément, écoutant les explications patientes de son instituteur. La classe ne broncha pas lorsque Mr Guidiccelli entreprit de démontrer une des règles de grammaire à l'aide d'un exemple sur l'ardoise de son élève. Et pas un ne leva le nez de son pupitre quand, à bout d'arguments, l'instituteur, rouge de colère fini par vouloir faire rentrer dans le crâne à coups de ladite ardoise, ladite règle de grammaire.

— Indécrottable Boniface ! Conclue-t-il en regagnant vivement sa place, et reprenant sa lecture, caché derrière son journal.

Une grosse fille rougeaude, très brune, assise à la même table d'écolier que Boniface, fit une discrète grimace en direction du bureau.

— Albane, je vois ton manège, et je t'encourage à te replonger illico dans ce merveilleux devoir plutôt que recopier les insanités de ton voisin !

La jeune fille soupira, et se remit à écrire. Les minutes s'étirèrent, Falban et Claude, les grands du fond de la classe, commencèrent à gigoter sur leurs chaises, Claudine et Prêlette, deux blondinettes au teint pâle, les cheveux remontés en une impeccable queue de cheval, assises sous la première fenêtre, attendaient sagement, les mains croisées sur leur devoir. Médor et Scrofulle, sous la deuxième fenêtre, dodelinaient de la tête, plus très loin de céder à la tentation de la sieste. Sanpain, un nerveux, larges d'épaules et Bidule, un blondinet amène, bon en mathématiques, face au bureau de l'instituteur, ne pipaient mot, gribouillant des notes sur leur feuille de brouillon, d'un air entendu. Ils trichaient, c'était flagrant, et Fourmi, un brun athlétique aux yeux verts, dans son rôle habituel de bon élève indigné, les guigna du coin de l’œil, depuis sa table, près du grand tableau noir, la mine outrée. Il envoya un coup de coude discret à sa voisine, Cathy, apprêtée, sous sa blouse, telle une véritable gravure de mode de seulement douze ans bientôt. Gratifiée d'immenses yeux de biche, dont elle surjouait sans cesse, s'en était agaçant, elle ne prêta guère attention à son camarade, toute à la tâche, délicate, de réarranger le nœud du ruban rose réunissant sa chevelure, dont la longueur assez exceptionnelle, dessinait une cascade auburn indisciplinée autour d'un visage somme toute commun. Elle essayait toujours, avec ses gestes et attitudes précieuses, d'attirer l'attention de JP, mais lui et son ami Ragondin, une table derrière, ne s’intéressaient guère à ce qui se passait dans la classe. Leurs curiosité pour les sciences, l'histoire, la géographie, n'avait point de limite ; ils se partageaient des petits papiers, des petits mots d'amitié, des dessins, à longueur de journée. Mr Guidiccelli en possédait un grand nombre, qu'il collectionnait dans un bocal, rangés parmi les nombreuses choses confisquées ; champignons, feuilles ; noix, marrons, et bien d'autres encore, moins identifiables.

— Bon, c'est terminé ! Jappa l'instituteur, ignorant les marottes des uns et des autres. « La Fouine, ramasse les copies ».

Un gamin déboula de derrière le bureau du maître d'école. Court sur pattes, pataud comme un jeune chien, il en avait un peu les manières d'ailleurs, ne tenant pas en place, ceci expliquant l'endroit, assez peu usuel, où l'instituteur l'avait installé ; seul, sur une minuscule table en retrait de l'estrade, et loin de ses camarades, qu'il avait trop tendance à dissiper par ses facéties, ses cabrioles, son air éternellement dans la lune, version hyperactif.

La cloche sonna miraculeusement tandis que les feuillets recouverts d'encre atterrissaient au coin du bureau. Cependant, personne ne bougea, respectant une des règles tacites du cours moyen première et deuxième année. Mr Guidiccelli n'avait pas pour habitude de se passer de la discipline, et ses élèves le craignaient suffisamment pour être obéissants. Ils attendirent.

— Vous pouvez sortir, dit-il finalement, « mais avant, j'ai une nouvelle à vous annoncer, en rapport avec notre projet de sciences naturelles, sur la reproduction animale »

Falban et Claude émirent un gloussement, vite réprimé mais pas assez cependant.

— Je constate que cela inspire certains de vos camarades, messieurs, vous resterez cinq minutes de plus en classe avant de sortir, vous n'êtes pas des dindons, je vous demande donc un minimum de correction durant mon cours.

Une élève leva la main.

— Oui Prêlette ?
— C'est quoi la nouvelle Monsieur ?
— Ah ! Bien, c'est à propos de la couveuse, je l'ai reçue hier, par le convoi postal du matin.

Les gamins s'agitèrent. Leurs bouches restèrent closes avec difficulté, dévorés qu'ils étaient par la curiosité.

— Nous irons la voir à la fin de la récréation. Sortez !conclue le maître d'école, satisfait de la retenue de ses élèves.

Ce fut là une joyeuse débandade. Une fois passé le seuil de sa classe, Mr Guidiccelli tolérait les frasques de la jeunesse, enfin, un peu. Ils s'évadèrent dans la cour, certains sous le tilleul, d'autres aux pieds des balançoires, mais la plupart formèrent un groupe gesticulant et piaillant, que Mme Guidiccelli, tenta sans succès de disperser.

L'homme sourit, alluma son cigare, et se rendit dans la cour. Il se doutait bien du sujet de conversations de ses élèves, et ma foi, n'était pas peu fier d'avoir réussi à soutirer quelque argent de la commune, en vue d'éduquer les enfants de manière, disons, moins traditionnelle. Les sciences ouvraient l'esprit, et ces enfants là avaient bien besoin qu'on leur baille la porte ! Le maire, Joseph Danton, d'un comique, le patronyme ! Quand on songeait que ce type était maire suite à la défection d'un colistié. Ah ! Il avait grogné, un radin, c'était peu de le dire, mais la première conseillère, dont la fille, Cathy, se trouvait dans sa classe, ne pouvait décemment pas s'opposer au projet. Le docteur Ferrand avait appuyé l'idée, et le curé Bresson, toujours invité d'honneur aux réunions de conseil, s'était contenté de croiser ses grosses paluches sur sa poitrine épaisse ; le cou ceint de blanc n'avait pas viré au rouge, plutôt bon signe. Finalement le maître d'école avait emporté l'affaire, au bout d'un conseil municipal houleux, mais il en avait la pratique, il était troisième conseiller. Mr Guidiccelli salua sa femme, ainsi qu'une autre, qui consolaient un petit chouineur, puis s’assit sur l'unique banc offrant vue sur toute la cour de récréation, face à la grand rue, en compagnie de son collègue de cours élémentaire, Mr Béziot.

La récréation achevée, ils allèrent à pieds, en rang par deux, et pas un ne bronchait. On entendait le rétameur, Jules Grandin, un géant aux mains comme des enclumes, quelques rues plus loin, taper sur ses ustensiles cabossés, étamer, c'est-à-dire, avant de les rétamer à nouveau dans un grand bain d'étain. Le son vibrait dans l'air tel une cloche d'un autre monde, un bruit mat, comme filtré à travers une épaisse couche de coton. La ribambelle traversa la cour d'école, longea la maison des maîtres, une demeure cossue, accrochée au mur d'enceinte de l'école, composé d'un étage unique et élégant, de balconières débordantes de géraniums multicolores, et plus haut encore, d'un toit de tuiles dont le rouge pimpant ravigotait même un ciel de novembre. En cette demeure habitaient Mr et Mme Guidiccelli mais aussi un autre couple d'instituteurs, Mr et Mme Béziot, qui enseignait les cours élémentaires. Le petit groupe franchit une courette située derrière la maison, et fit encore une cinquantaines de mètres jusqu'à un appentis de briques rouges. Au-delà, on apercevait le jardin et le potager, en enfilade. Du linge séchait sur des fils tendus ; draps d'une blancheur à faire pâlir la lune, taies d'oreillers fleuries battant au vent en cette fin de matinée de printemps. Le maître d'école prit une grosse clé de fer dans la poche de sa blouse grise, et ouvrit la porte de l'appentis À l'intérieur, en face de l'entrée, apparut une large planche de bois posée sur des tréteaux solides, sur laquelle reposait la couveuse. La minuscule fenêtre du local filtrait la lumière du jour et il régnait là une pénombre et une fraîcheur tranquille. Les élèves s'agglutinèrent autour de la caisse, se penchant pour mieux voir.

— Qui me rappelle le principe de cette machine ? Questionna l'instituteur parcourant la petite assistance d'un regard circulaire.

Un doigt se leva ; Prêlette, comme souvent ; cette gamine était vive comme un geai dans l'aulnaie.

Il hocha la tête dans sa direction et la petite se mit à réciter d'une voix haut perchée mais supportable ;

— C'est une boite isolée pour éviter les déperditions de chaleur avec dedans une résistance chauffante pour diffuser la chaleur et qui est réglable avec un thermostat.
— Très bien. Et qui peut me dire comment nous allons procéder avec les œufs ?

Un autre doigt se leva ; Sanpain. Celui-là n'avait pas inventé l'eau tiède, mais il relevait malgré tout de la catégorie des bons gosses. Enfin, c'est ainsi que Mr Guidiccelli rangeait ses élèves. D'une part, les gosses qui cherchaient à apprendre, à comprendre, avec plus ou moins de réussite, en fonction de ce que les parents et l'entourage leur avait fait don à la naissance et d'autre part, les mauvais gosses, ceux pour qui tout était perdu d'avance, la tête non pas vide, mais déjà emplie à ras bord de mauvaises idées, dès la naissance, peut-être avant, pourrissant leurs esprits.

Il ignora le doigt levé et se tourna vers un autre écolier.

— Raconte-nous le processus, Ragondin, sans bégaiements, je te prie.

— Oui, maître.

Le garçon s'appliqua à parler lentement, en détachant les syllabes, comme recommandé, afin d'améliorer sa diction, fort laborieuse. « On va mettre les œufs qu… que le père Oudard va nous donner, sous réserve qu'on lui garde un pou… oussin, dans la ma… dans la machine » se reprit-il avec effort, « et au bout de vin… ingt-et-un jours, les poussins éclo… »

— écloront, l'aida le maître, encourageant l’effort visible du gamin pour terminer son exposé de manière convenable.

Celui-ci reprit, en confiance,

— écloront, mais il faudra pen… penser à re…retourner les œufs tous les jours. On aura droit à un œuf cha… chacun avec notre nom de… essus, et si le poussin éc… éclot, on aura droit de le rem… emporter chez nous p… our en faire une poule ou un coq.

Il souffla avec soulagement, rosi par l’effort de concentration .

— Bien, salua Mr Guidiccelli en se tournant vers Falban. Et quelle température dans cette couveuse pour faire naître les poussins ?

Le fils Oudard ouvrit de grands yeux où ne se reflétait qu'un lac de stupidité. Claude vint au secours de son frère en agitant la main au-dessus de la petite troupe. L'instituteur n'en tint pas compte, dans un premier temps, fixant ses yeux cobalt sur l'ignorant avec une insistance lourde de sous-entendus. Une vraie graine de cornichon. L'autre restait muet, se passant la langue sur les lèvres avec le subit espoir peut-être d'y trouver la réponse au milieu d'une salive brillante qui s'étalait et débordait déjà de la bouche pour tartiner le menton. L'instituteur soupira, et invita Claude à poursuivre.

— Y faut 37,7 degrés centigrades Maître, pour voir les poussins éclore et pi faut penser à maintenir l'humidité, en mettant de l'eau dans le petit réservoir, là. Il tendit un index à la propreté douteuse sur le côté droit de la machine, d'où dépassait un petit bouchon rouge. On rempli avec le petit entonnoir qu'a prêté la mère de la Cathy.

Monsieur Guidiccelli fronça ses sourcils broussailleux, l'élève se reprit avec un hoquet,

— Heu, qu'a prêté la mère de Cathy.
— Bien, et qui peut me rappeler ce qu'il nous faut avant toute chose pour obtenir un poussin ?

Ses yeux survolèrent l'assemblée, tassée dans l'appentis, attentive, sauf…

— La Fouine !

L'enfant se redressa, épousseta ses mains qu'il avait au sol, dans le sable, un instant plus tôt, contre sa blouse et interrogea son maître en dessinant un mince tiret, à l'obturation maximale avec sa bouche.

— La Fouine, ce poussin, alors, dites-moi, que faut-il pour qu'il y soit, dans l’œuf ?
— Euh, le gosse se gratta la tête, et s'agita d'un pied sur l'autre, entamant une danse discrète mais incessante. « Euh, faudra la poule et pi le coq pour coqueter l’œuf, sinon ça fera rien »

L'instituteur tendit le bras, lui posa fermement sa large main poilue sur l 'épaule, et le garçon cessa son tremblement.

— Très bien. Je vois que vous avez retenu notre dernière leçon de sciences naturelles, sauf Falban qui recopiera son cours pendant la récréation de midi, et à la fin de la journée si nécessaire.

On entendit l'élève en question marmotter quelque chose, gratter sa chaussure contre le sable en guise de protestation, mais il baissa finalement la tête évitant de croiser le fer avec son maître d'école.

— – Allons, rentrons à présent. Après le repas, nous irons chercher nos œufs, et vous porterez avec une belle écriture vos initiales sur la coquille, sans l’écraser, bien évidemment, ce qui va être une rude tâche pour certains ! En rang ! Cathy et Scrofulle, fermez vos clapets et passez devant, conduisez vos camarades jusqu'à la salle de classe. Une leçon de calcul mental vous y attend.

Le retour s’effectua dans un calme relatif, les élèves se gaussaient un peu de faire pareil expérience, une couveuse, des poussins, c'était peu ordinaire ! Et le maître laissa d'abord libre cours à leur enthousiasme. Puis la cloche de l'église St Pierre et Paul, à deux pas, face à l'école, de l’autre côté du pont de pierres qui enjambait un filet d'eau nommé « le Ru »,sans plus de façon, sonna onze heures ; pas question de prendre du retard sur le cours de mathématiques ! L'homme en blouse grise pressa ses élèves avec une énergique frappe dans les mains et la double cordée, de main en main, à grandes enjambées, regagna la salle de classe.

— Boniface, avez-vous le panier ?
— Oui Monsieur.

La classe était suspendue aux lèvres du dit Boniface.

— Combien d’œufs faut-il quérir auprès de Mr Oudard, pour que chacun de vous en possède un, et que le fermier en hérite d'un également, je vous prie ?

L'élève jeta un regard à l'assemblée. La classe ayant été très dissipée en grammaire et le résultat de la dictée du jour s'étant révélé particulièrement catastrophique, Mr Guidiccelli avait émit un ultimatum : soit les travaux de mathématiques se révélaient irréprochables, soit il y aurait des conséquences déplaisantes. Avait suivi un petit discours piquant sur le relâchement intolérable de la classe. Chaque élève devait donc se soumettre aux problèmes de mathématiques avec, en tête, la possibilité de se voir retirer son œuf s'il échouait lamentablement.

— Alors Boniface ?

Le relança impatiemment l'instituteur, les deux poings dans les poches de sa blouse, la mine renfrogné par les mauvais résultats du jour. Même Prêlette marquait un huit sur dix en orthograph !inimaginable ! Rumina-t-il dans un coin de son esprit.

— Alors, reprit Boniface, la voix tremblotante, il y a euh… quinze élèves dans la classe, plus notre instituteur…

Quelqu'un toussa au fond de la rangée près du radiateur. Boniface corrigea ;

— Euh, quinze plus Mr Oudard.

Un silence faisant office d'assentiment accueillit sa proposition. Le maître resta coi, droit comme un « i », dominant son petit monde du bout de l'estrade.

– Euh… quinze plus un donc, personne d'autre…

Même silence dans la salle, avec un sourire de Cathy en plus.

— Ça fait seize. Il nous faudra seize œufs, Monsieur, pour nous tous.
— Bravo. Et bravo à vos camarades, toujours aidants et bienveillants. Vous me copierez cependant votre table d'addition, dans son entièreté, pour lundi, c'est compris sacrebleu d'âne tire-bouchonné ?

Boniface, qui avait espéré passé entre les gouttes d'un orage corse typiquement dru et sec, regimba du bout des lèvres puis s'assit lourdement, vaincu.

— Oui Monsieur.

À la récréation de quinze heures, la classe se rassembla sous le préau pour un dernier comptage et les ultimes recommandations de Mr Guidiccelli ;

— je vous demande de la correction, de la tenue, nous allons jusqu'à la ferme, sans débordement d'aucune sorte, en arrivant, n'oubliez pas la politesse, vous me feriez honte, et à vous-même d'autant ! On ne grimpe nulle part, son regard alla immédiatement à La Fouine, qui se grattait le fondement d'un air distrait en dansant d'un pied sur l'autre. « On ne chahute pas » ses billes aux tons de malachite fusèrent comme des balles en direction de Falban et Claude, « et enfin, pas de pipelettes ! » Il abattit en douceur ses deux index sur les silhouettes respectives de Cathy et Prêlette, en grande conversation, l'une penchée sur l'oreille de l'autre avec connivence.

Elles se turent, leurs joues se colorant d'un fuchsia fugace.

— En route !

L'injonction fut suivit d'un claquement de doigts, un seul, et le serpentin d'écoliers aux blouses de serge bleue se mit en branle et s'étira paresseusement.

Tous traversèrent le pont cambant le ru, sous les coups vaillants du rétameur, vaquant à sa rude tâche, et l'on discernait même, en tendant un peu l'oreille, il est vrai, tant les pinsons et rossignols s'en donnaient à cœur joie, les lentes expirations du soufflet de forge, signe que l'étain était à la fonte dans le grand godet. Il devait faire affreusement chaud sous l'atelier de l'homme, alors qu'il s’annonçait déjà une chaleur accablante dehors. Ils coupèrent ensuite par le parvis, passèrent sous l'ombre du clocher silencieux, longèrent la rue des fontaines, bien que la guerre de 1890 eut ravi les points d'eau en question, par on ne sait quelle malchance, l'approvisionnement cessa dès lors, sans jamais reparaître… Probablement qu'un obus avait fait sauter la source, c'était l'avis général. La classe, rangée deux par deux, poursuivit encore son chemin jusqu'à la sortie du bourg, pris le sentier de gravier nommé « La tirette » allez savoir pourquoi, d'où l'on apercevait déjà, à bonne distance, les bâtiments trapus de la ferme des Quatre-vents, la ferme des parents de Claude et Falban. Le père valait mieux que les fils, songea l'instituteur, à se demander d'où sortaient ces deux là, lui avait confié un jour sa femme, Mme Guidiccelli, très remontée après une mauvaise farce de trop d'un des deux frères qu'elle supportait alors en cours préparatoire. Et c'était vrai que le père Oudard appartenait à l'ordre des courageux, sa ferme s'en portait bien, d'ailleurs, mais peut-être l'excès de gâtisme dont faisait souvent preuve Madame à l'égard de leurs enfants, encourageait leurs mauvais penchants, dont le pire, la fainéantise. Le fermier en souffrait, tandis que sa moitié, aveugle aux incartades récurrentes de ses garçons, rossait les chiens et le vacher qu'elle accusait de tout et de rien. Une femme acariâtre, en rien agréable. Aussi, le maître d'école, tout comme ses élèves, furent-ils soulagés de l'accueil de Mr Oudard seul, s'excusant pour l'absence de son épouse occupée à nourrir leur troisième, Philistin, né dans l'année.Tout en disant cela, il se dandinait fiérot, à son âge, une petite cinquantaine, il ne s'attendait évidement plus à un troisième fils, et ce don du ciel lui apportait réconfort, et bon espoir, aussi, pour l'avenir de son exploitation. Il serra d'une poigne virile la main brune et poilue du corse, et ils s'attardèrent quelques instants à discuter de choses et d'autres, ayant lâcher la bride aux enfants, qui s'égayaient dans la cour. Les uns visitant les clapiers, caressant les lapereaux tout peureux, d'autres jetant quelques cailloux dans la mare aux canards, tout au plaisir de voir ceux-ci s'ébrouer, indignés, battant des ailes et cancanant à grosses voix, protestant d'être dérangés. Quelques-uns encore, discrets, s’éloignaient déjà, tentant de lancer de l'herbe aux oies blanches et grises, agglutinées et vindicatives, le long d'une pâture.

— Allons, allons, on se regroupe jeunes gens ! Mr Guidiccelli rappela ses élèves, et telle une envolée de moineaux, les enfants s'approchèrent des deux adultes et gratifièrent leur hôte d'un enjoué « Bonjour monsieur Oudard ! » auquel le fermier fut sensible.

Il entreprit les enfants sur le sujet du jour.

— Savez-vous où nous allons chercher les œufs à présent ?
— Oui ! Oui ! Plusieurs élèves crièrent ensemble, et le maître pressentant le capharnaüm, interpella Scrofule, un meneur, lorsqu'il s'agissait de faire preuve d'irrespect, ou autre débordements.
— Scrofule, répondez, je vous prie, et les autres, silence !

Tous s’attroupèrent un peu plus autour de Scrofule, bien ennuyé par l’intérêt qu'il soulevait soudain.

— Ben on va au poulailler, j'pense. Les poules auront pondu dans l'pondoir, graillonna-t-il de sa voix criarde, en pleine mutation pubère.
— Exact !

S'exclama Mr Oudard, et il enjoignit la classe à le suivre vers le fond de la cour, où de nombreuses poules, tourterelles, et dindes s'éparpillaient autour d'une cabane en rondins.

— Original, ce poulailler, commenta l'instituteur.
— Fabrication maison, figurez-vous, j'ai vu dans le magasine mode et travaux de ma femme qu'au canada, on utilise les rondins, tels quels, sans s'user à en faire des planches, et comme, il y a deux ans, le vieux poulailler a pris l'orage, le vent, bref, tout démantibulé par la colère du temps, je l'ai refait de matériau brut, j'avais les vêlages, pas de temps pour débiter les planches, le toit, tout ça c'est du temps, ma foi,exposa l'homme, heureux de l'intérêt porté par l'instituteur à son humble construction…
— C'est très étonnant, mais ça m'a l'air bien robuste, commenta de nouveau l'instituteur, caressant les gros rondins bruts. « la bâtisse n'est pas commune, mais étanche, et certainement bien isolée, finalement, belle idée, dites, père Oudard, vous avez bien travaillé !

Tant de compliments, c'était bien inhabituel pour le fermier, plus coutumier des plaintes et remontrances de son épouse. Il souffla de contentement et enjoignit de sa voix de ténor la troupe désordonnée à le suivre jusque derrière le bâtiment où se trouvait une porte. Lorsqu'il ouvrit grand, quelques volailles s'échappèrent en piaillant, à la plus grande joie des enfants ; le maître d'école dû refréner les fantaisies de Falban, tentant d'arracher au passage des plumes à une jolie poule safran, et imposer le silence aux sempiternelles bavardes, oublieuses des recommandations de départ.

— Que voyez-vous sur les deux cotés de ce poulailler les enfants ? Interrogea l'instituteur, ne perdant pas de vue un seul instant la démarche pédagogique de la visite.

Un, puis deux, puis trois doigts se levèrent, et gigotèrent, impatients.

— Claude, à toi de nous expliquer.

Il fallait bien flatter un peu le père, tout de même, concilia le maître d'école.

— Ben c'est les pondoirs, Y a qu'à prendre les œufs que les poules ont pondu du jour pour les ranger dans la couveuse après. Y a un coq dans la basse cour, Attila, y s'appelle, et papa a vérifié hier soir à travers la bougie, les œufs y sont coquetés.
— Bien Claude, malgré tout, moins de « y » dans ton discours me conviendrais mieux, j'en ai mal aux oreilles.

Il s'adressa ensuite au reste de ses élèves :

— « Alors je vous veux en file indienne dans la seconde. Chacun va recevoir de la main de Mr Oudard, un œuf À partir de ce moment là, il en est RESPONSABLE ! S'il le fait tomber, s'il le casse, l'écrase, c'est tant pis pour lui. Est-ce clair ? Boniface, sortez les boites à œufs du panier, et donnez-en une à chacun de vos camarades, et que ça saute !

Le gamin s’exécuta, et bientôt, chacun tendit sa boite à Mr Oudard puis reçu dans un silence quasi religieux son œuf, qu'il remisa délicatement dans une des alvéoles disponibles. Certains étaient presque blancs, d'autres beiges, certains encore, plus bruns, ou tachetés. Enfin, La petite cérémonie prit fin, Falban, Claude, Albane, Claudine, Cathy, Médor, Scrofulle, La Fouine, Boniface, Sanpain, Prêlette, Bidule, JP, Ragondin, Fourmi, chacun eut son trésor et le tint par devers lui précieusement ; dans leurs yeux, brillait l'excitation, à imaginer leur futur poussin. Il le voyait déjà, minuscule plumetis jaune au bec encore plus minuscule, dont le pépiement sonore emplirait l'appentis de l'école.

— Ben heureusement qui Y a pas un autre élève, Y a pu rien à ramasser, tous les coco y sont passé ! Commenta Claude, en posant le dernier dans la boite d'Albane.

Mr Guidiccelli dodelina de la tête devant le langage calamiteux de l'écolier, mais s'abstint pour une fois de le corriger, par égard pour son père.

Tout ce petit monde s'en retourna dans la cour, saluant le fermier, après avoir reçu de l'expert es volailles quelques derniers conseils pour obtenir la meilleure chance d'une couvaison réussie. Au moment du départ, La Fouine, pressée ou poussée, qui peut dire, écrasa de son godillot ferré la boite de Ragondin, posée momentanément à terre, et, au bruit, pas de miracle, tous s'attendirent à ce que le maître, rougeaud et furieux, annonça bientôt ;

— Et voilà ! Un œuf de fichu avant même sa mise en couveuse ! La fouine, rugit-il, il le tenait par une oreille, et ne la lâchait pas, tel un vieux molosse hargneux sur un os de gigot, « tu vas donner ton œuf à ton camarade, et c'est tant pis pour toi, espèce d'avorton insupportable ! »

Le gamin pleurait, les autres silencieux, baissaient le nez sur leur boite, gênés. La Fouine, désespéré, s'apprêtait à poser son œuf dans la boite ratatinée de son camarade.

— Ma foi, tenez, dit soudain le fermier, fendant la petite troupe, contrit et mal à l'aise, « on va pas le priver dès ce jour-là, ce gamin, ça ne serait pas un bon présage, ça non » ajouta-t-il en secouant sa large figure. « J'te donne çui-là mon gars, en remplacement, un de collection, je l'ai troqué au camelot qu'est passé samedi dernier.» et il posa l’œuf en question, d'un bleu saphir extraordinaire, dans la boite de Ragondin.

Mr Guidiccelli avala un peu de travers, mécontent de l'intervention du brave homme. La Fouine avait besoin d'une leçon. Elle était reportée, l'instituteur avait trop d'amitié pour le fermier, il ne désirait pas le vexer, ou le peiner par un refus de réparer l'accident. Il acquiesça donc du chef, sans commenter, puis lui serra la main au moment de s'éloigner. Les gamins s'étaient attroupés autour de de leur camarade, admirant ce cadeau étrange et très beau. Le clocher sonna seize heures, il était temps de reprendre le chemin de l'école.

— Ferme ta boite, Ragondin, et toi, en ran ! intima le maître d'école à l'élève saboteur, qui se massait l'oreille avec une grimace.

Le silence se fit, doucement, sans bruit ni chamaillerie. Le ruban d'élèves reprit son déroulement à l'envers et s'en retourna d'un pas tranquille, chacun plus précautionneux que son voisin pour ne pas réitérer le malencontreux événement subi par l’œuf de Ragondin. Et tout se passa à merveille cette fois : Les quinze œufs arrivèrent à bon port ! La fin d'après-midi fut ensuite consacrée à l’inscription des initiales de chaque élève sur son œuf, au crayon gras, « sans appuyer comme un fondu crétin » avait précisé le maître d'école, retenant la main d'un Alban un peu trop brusque ou d'une Claudine, trop appliquée. Mais finalement, les coquilles, parées d'enluminures, comme pour les Pâques, purent prendre place dans la couveuse sans accident. Il fut décidé qu'à tour de rôle, chaque élève en aurait la charge ; vérification de son fonctionnement, de la température, ajout d'eau dans le réservoir et retournement des œufs. Tous se prêtèrent au jeu avec assiduité. Ce fut vite le rituel préféré de la classe.

— Maiiitre !

Mr Guidiccelli leva le nez de son journal, la page politique l'absorbait toujours beaucoup, il en avait oublié Médor, un gosse qu'on pouvait hélas, très facilement oublié. Il l'avait envoyer retourner les œufs, routine du matin depuis une huitaine qu'ils se trouvaient en couveuse. Il observa son élève, qui n'avait pas l'air à son aise, au pied du grand tableau noir.

— Qu'y a-t-il mon garçon ?
— Y a eu du grabuge dans l'appentis, maître, et pi les œufs sont tout de travers, et y'en a un qu'est cassé, on dirait qu'il a comme explosé !

L'instituteur posa le journal et leva un sourcil dubitatif. Il lui fit signe de se rapprocher du bureau, et se pencha, lorsqu'il fut au ras de l'estrade vers son élève, la tête rentrée au creux des épaules à présent, pressentant un coup de sang typiquement corse.

— Tu me racontes la vérité, nécessairement, mon garçon ?
— Oui Maître. Y a plein de jaune collé sur la porte de la couveuse et un œuf en miette. Quand j'ai ouvert la porte, c'est comme ça que je l'ai trouvé, protesta le garçon, fixant courageusement son instituteur droit dans les yeux.

Et quels yeux ! Précisément d'un bleu si abyssal, rendant honneur au couleur de poil de ce fameux porc nustrale, mais point tendres à l'égal du savoureux jambon, non, des yeux durs et furibonds.

L'homme se leva vivement, plaça ses mains derrière le dos, et embrassa la salle de classe : Les élèves, dans l'expectative, ne pipaient mot. On aurait entendu le pet d'une mouche, si un tel insecte avait eu l'indélicatesse de se soulager à cet instant. Il pointa un doigt court et boudiné.

— Fourmi, tu es le chef de classe, pas un bruit jusqu'à mon retour.

L'élève en question, un grand échalas musclé mais en pleine crise adolescente, aux longs bras noueux comme des lianes, acquiesça, se leva, et vint prendre place au centre de la classe, sérieux, l'air de prendre son rôle très à cœur. Fourmi prenait toujours tout à cœur, d’où son surnom ; prévoyant, travailleur, calme, vif d'esprit, il subissait les quolibets de Falban et Claude qui l’affublaient de « fayot » à tout bout de champ sans se départir d'une indifférence admirable, bien qu'elle frôla un certain mépris.

La classe avait cependant du mal à garder son calme, la révélation de Médor les inquiétait tous. Quel œuf était fichu ? Quel malveillant avait oser s'introduire dans l'appentis de l'école ? Pourquoi ?

Fourmi mit son doigt sur ses lèvres en direction des élèves qui se trémoussaient, chuchotaient, s'envoyaient des bouts de papier minuscules d'une table à l'autre.

En arrivant devant la couveuse, dont la porte, dans la panique, était restée ouverte après la triste découverte, Mr Guidiccelli ne put que constater le désastre ; un œuf avait effectivement littéralement implosé, et de la matière collante entachait à présent les œufs à proximité, mais, plus grave, en y regardant de plus près, il découvrit un second œuf fendillé, perdu lui aussi. Il renvoya Médor, toujours dans son sillage, chercher de quoi nettoyer ce carnage, et, après avoir remis de l'ordre, retourna lui-même, un à un les œufs, vérifiant chacun avec précaution. Il referma la couveuse, ainsi que la porte de l'appentis, se promettant d'ajouter un verrou dès ce soir, car il ne croyait pas le gamin capable d'une si laide et gratuite action envers tous ses camarades. Il contempla la porte, perplexe, la réflexion dérangée bientôt par le tapage du rétameur, là-bas dans la rue des fours. L'artisan mettait du cœur à l'ouvrage ; les coups pleuvaient à verse, une véritable cacophonie métallique.

Le plus difficile fut d'annoncer à Falban et Bidule que l'expérience s'achevait déjà pour eux deux. Les enfants furent chamboulés jusqu'à la fin du cours À la récréation, le maître d'école les observa qui discutaient fiévreusement de l'affaire, et consolait Bidule. Falban avait pris le parti de faire le fier, de jouer au Grand. Mais il ne taquina personne ce jour là, plus déçu qu'il ne voulait bien le laisser paraître.

La deuxième semaine de couvaison s'écoulait sans incident, quand à la fin d'un cours de sport, le jeudi, alors que Prêlette, de service pour la couveuse, s'était éclipsée, un cri à déchirer les tympans d'un sourd s'éleva de l'appentis au moment de se mettre en rang pour regagner la classe. Ni une ni deux, le brave instituteur fonça à toutes jambes, suivit cette fois de sa palanquée d'élèves alarmés.

— Cesse de hurler ! intima-t-il à Prêlette, en déboulant devant la porte de l'appentis.

La gamine, blanche comme la neige, les joues tracées du ruisseau sale des larmes, fixait la couveuse dont la porte béait. L’instituteur en scruta machinalement l'intérieur avant de reculer vivement, écrasant pesamment un pied au passage, celui de JP, entré à la suite de l'homme, par curiosité. Mais ni l'un, ni l'autre ne s'attardèrent sur l'incident  ; une odeur pestilentielle chatouillait les narines et les yeux à faire pleurer. L'homme repoussa les enfants vers l’extérieur :

— Fourmi, va quérir Mme Bovi immédiatement !

Mme Bovi était la femme à tout faire de l'école, et accessoirement des instituteurs. Elle s'employait toute le journée à ranger et nettoyer les classes, tables, bancs, tableaux, faire luire les carreaux, également ceux des appartements des maîtres, s'arrangeait de leurs lessives, et bien d'autres tâches ménagères encore ; c'était une brave femme, discrète, courageuse, embauchée par la mairie depuis son veuvage, voilà dix ans tout juste. Fourmi partit en trombe claironnant le prénom de la femme de ménage sur tous les tons.

– Josy ! Jooosy ! Josyyyyy !

Pendant ce temps-là, Mr Guidiccelli prit sur lui, arrangea son grand mouchoir à carreaux sur son nez, enfila une vieille paire de gants de jardinage oubliées sur l'étagère, se saisit d'un vieux pot en grès qui traînait là et entreprit de débarrasser les œufs irrémédiablement détruits. Les poussins, préformés, ensanglantés, béaient pour moitié hors de leur coquille, certains aplatis, démembrés, d'autres dans des positions grotesques, tous gluants et bien évidemment sans vie. Cette fois, le carnage se révélait plus important que la première fois ; six œufs étaient totalement écrabouillés et un autre fêlé.

La bonne arriva enfin à toutes jambes, prit les ordres du maître d'école en même temps qu'une mine parfaitement dégoûtée, repartit en courant chercher son matériel de ménage, virevolta ensuite au milieu des enfants, poussant les uns, houspillant les autres, versant de l'eau partout, puis se mit à briquer méthodiquement l’intérieur de la couveuse dont tous les œufs encore intacts avaient été extraits, et entreposés momentanément à l'abri dans un panier d'osier, avec un linge par dessus, pour éviter une trop grande déperdition de chaleur. Heureusement, c'était juin, le soleil dardait généreusement ses chauds rayons sur le panier. Les enfants, devant l'appentis, réunis autour du maître d'école, commentaient l'attentat avec force. Cathy et Scrofule pleuraient devant le massacre étalé sous leurs yeux ; ces morceaux sanguinolents de pauvres futures poussins tartinés sur les parois de la couveuse et dont la vie s'achevait, sans autre cérémonie, au fond d'un vieux pot en terre cuite. Finalement, Mr Guidiccelli retrouva ses esprits, et frappa dans ses mains.

— Reculez, reculez, allons les enfants, rangez-vous, nous retournons en classe, laissons madame Bovi finir ce pénible travail, et remettre le plus rapidement possible les œufs au chaud.

Il fut obligé de taper deux fois dans ses mains, signe d'un grand chambardement, mais de même que ses élèves, il se trouvait ému par cet événement et ne leur tint pas rigueur de leur dissipation. Le calme revenu, les coups de l'étameur sur ses marmites, à quelques encablures de là, rythmés tels un sonneur de cloches, s'abattaient maintenant lugubrement dans l'air doux du printemps ; ils sonnaient tel un glas, tandis que les élèves, en rangs par deux, regagnaient leur classe.

Néanmoins, Mr Guidiccelli digéra difficilement cette nouvelle infamie, et ne décoléra pas de sitôt. Sa femme en entendit parler toute la soirée, et le maire le lendemain. Il fut convenu que Gérald, le garde champêtre, ferait des rondes autour et dans l'école jusqu’à la fin de la couvaison. Pourtant, quelque chose chagrinait beaucoup l'instituteur ; le verrou n'avait pas été forcé et le cadenas non plus. Alors comment ? Restait la fenêtre, très étroite, mais en tout état de cause, il l'avait trouvé fermée à chaque sabotage. Un vrai mystère ! Il se mit à ruminer en classe, chez lui, au café, lors de sa partie de belote du samedi soir, dans son jardin, le sécateur à la main, ou l'arrosoir, il ne pouvait s'en empêcher. Il portait trop souvent un œil soucieux sur la porte de l'appentis, et vérifiait régulièrement le contenu de la couveuse, méfiant. Il ruminait encore la nuit, dormant mal, le jour, mangeant peu, et désagréable avec sa femme, soupçonneux envers qui traversait la cour ou pénétrait dans l'école ; les représentants de commerce et ambulants n'étaient pas les bienvenus ces jours-ci, loin s'en fallait. Et pour couronner le tout, parler aux enfants, perturbés par ces saccages incompréhensibles lui avait été fort pénible. Leurs bouilles accablées affligeait l'instituteur.

— Cathy, Scrofulle, Albane, Claude, Sanpain, Médor, La Fouine, je suis navré les enfants, mais vos œufs sont fichus, leur avait déclaré le maître d'école, la rage au ventre. « Je vous promets que nous découvrirons le fin mot de cette histoire. Allez, prenez votre ardoise et écrivez la table des sept. Claudine, ne louches pas sur ta voisine, s'il te plaît !

La troisième semaine se déroula sans aucun trouble, aucun enfant n’apparut sur le seuil de la classe en braillant, et les œufs terminèrent leur lente transformation, au chaud, dans la couveuse À la porte de l'appentis, en plus du verrou, du cadenas, et des rondes régulières, le garde champêtre, compatissant, avait ajouté des clochettes au dessus du chambranle, et leurs tintinnabulements avaient rythmé ces derniers jours d'allers et venues.

— Nous y voilà, dit solennellement l'instituteur, terminant l'écriture de la date du jour sur le grand tableau noir. Il se retourna, et contempla ses élèves, qui ne bronchaient pas, attentifs, sauf Falban, affalé sur sa table, à sa triste habitude. Il s'abstint pourtant de le morigéner, tout à la joie d'annoncer une plaisante nouvelle aux enfants :

— En vérifiant la couveuse ce matin, j'ai aperçu un nouveau phénomène.

Les cris fusèrent, tous remuèrent sur leur chaise, soudain nerveux, fixant fiévreusement leur instituteur, imperturbable. JP se tenait la tête, Médor et Boniface rongeaient leurs ongles fébrilement et Cathy tripotait ses nattes, resserrant puis desserrant les rubans rose, inlassablement. Alban, le visage anormalement grave, résumait à lui seul les tourments des enfants.

— Une très bonne nouvelle les enfants, précisa encore l'instituteur, observant le regard mouillé de Claudine, ainsi que Sanpain, le poing enfoncé dans la bouche. « Je vous propose donc de vous ranger pour aller constater vous même ce changement ! »

Il n'eut pas à frapper dans ses mains ; tous furent au garde-à-vous en un instant.

Arrivés sur place, ils se poussaient un peu, étiraient le cou, grimpaient sur la pointe des pieds,espérant mieux voir, rassemblés de part et d'autre de la couveuse. Les commentaires allaient bon train.

— – Ah ! Il est en train de sortir, le poussin de Fourm ! S'exclamèrent Prêlette et Boniface en souriant jusqu'aux oreilles.
— Il va p… pas y arriver le mien, je v… vois rien de fen… fen… fendillé sur la co… co… uille, s'inquiéta laborieusement Ragondin, se penchant au plus près de son œuf bleu.
— Oh le mien il bouge ! L’œuf boug ! Piailla Boniface, tandis que Fourmi, Prêlette et Claudine touchait le leur, hésitant entre déception et espérance face à la coquille vierge de toute craquelure.
— Maiiitre !
— Oui, Médor ?
— On peut les mettre dans la boite avec la lampe qui chauffe ?
— Pas encore, nous allons attendre que les poussins soient hors de leur coquille pour les installer sous la lampe, dans leur petit enclos.

L'enclos en question consistait en une boite de carton épais, don de Madame Guidiccelli, au fond recouvert de pages de journal, auquel avait été fixé, sur un coté, une lampe, destinée à tenir au chaud les poussins nouvellement nés. Deux petites mangeoires orange vif, anciennement destinées aux perruches de l'institutrice, reposaient dans un coin, et « l'enclos » provisoirement rangée sous la fenêtre de l'appentis. Il avait été avantageusement décorée par les petits de maternelle, chacun d'eux ayant apposé une myriade de gommettes de couleurs sur les faces extérieures.

— Faut mettre du grain ! Et pi de l'ea ! enchaîna Albane, anticipant les naissances.
— Très bien, oui, confirma Mr Guidiccelli, heureux de l'attention que prêtait sa petite troupe aux futurs nouveaux nés.
— Mais faut vite les emporter, sinon, ils vont se faire exploser par le méchant ! Ajouta Fourmi solennellement.

Tous se turent.

— C'est vrai maître, un méchant va peut être venir les bouffer ! Renchérit Falban un sourire sardonique aux coins des lèvres.
— Manger ! Le reprit rondement Mr Guidiccelli, gommant le faciès réjoui de Falban en un clignement de paupière. Quel oiseau de mauvaise augure, celui-là décidement !
— Ben c'est pareil hein, au fond, on les aura pu ! Croassa Bidule, contemplant l'œuf bleu trônant sur le tamis de la couveuse. Les initiales de son propriétaire, tracées à la craie grasse, dessinaient une guirlande grotesque autour du globe ovoïde. « RR » : Ragondin Raphaël.
— On ne les aura plus ! Corrigea l'instituteur. Vous vous exprimez comme des charretiers ma parole ! Reprenez-vous bande de linottes !
— Maiiitre !
— Oui Médor ? Soupira-t-il.
— On va les ramener chez nous bientôt ?
— Nous en avons déjà parlé, ceux qui ont un poussin le remporteront à la fin de la semaine, le temps pour vous d'étudier un peu ces volatiles, et de les dessiner dans votre cahier de sciences naturelles.

Médor ébaucha un sourire, puis sa mine se ferma. Il se rappelait qu'il n'avait plus d’œuf.

Le premier poussin émergea enfin de sa coquille, il appartenait à Claudine. Elle le prit dans ses mains, tout jaune qu'il était, et le déposa avec délicatesse dans le carton aménagé, sous la lampe qui diffusait une lumière chaleureuse. Albane distribua la nourriture et Scrofulle se chargea de la boisson. Les enfants étaient étrangement calmes à présent, serrés dans l'appentis, ils observaient les derniers efforts des trois autres volatiles pour se débarrasser de leur coquille.

— Boniface, ton poussin l'est rayé ! Cria Sanpain.
— Inutile de braire, le morigéna l'instituteur en motivant sa remarque par une moue peu amène.
— JP, le tien l'est jaune, merde on va pas les reconnaître ! Poursuivit le garçonnet, tout à son observation.
— Cette fois, tu passes les bornes ! S'offusqua l'homme en lui tirant l'oreille. L’élève Sanpain rougit violemment et frotta vigoureusement l'appendice meurtri.
— C'est vrai Maiiiitre, on va pas s'y retrouver !

L'instituteur saisit le poussin qui protesta faiblement, le retourna prestement et fixa à l'une de ses pattes un bout de ruban Jaune. « Jaune, J comme JP, voilà, on saura l'identifier à présent, conclut le maître d'école en reposant l'oiseau à coté de ses deux congénères.

— C'est comme une bague, gloussa Prêlette, la plus coquette.
— C'est pê'te une princesse, ajouta Albane.
— Nan mais ça va pas vous deux ? C'est un coq mon Jaune ! Protesta énergiquement JP.
— Suffit ! Voilà les retardataires ! Les interrompit l'instituteur.
— Fourmi, le tien il a les pattes rouges ! C'est Fourmi roug !Alban se tapa sur la cuisse, roulant des yeux à l'intention de Fourmi, décontenancé.
— L'est malade Maiiiitre ? Questionna Médor.
— Bien sûr que non ! Toutes les couleurs sont possibles chez la volaille, il n'y a aucune inquiétude à avoir, voyons.
— Celui de Prêlette est d'un jaune très pâle souligna Fourmi.
— Pou… P… Pourquoi mon œuf y b… Bou… ouge pas ? Bougonna Ragondin, poussant son œuf bleu qui roula dangereusement sur quelques centimètres, découvrant une surface parfaitement lisse, sans nul indice de naissance prochaine.
— Certains œufs sont un peu en retard, nous allons lui accorder un sursis de quelques jours.
— C'est un œuf puna, c'est comme Rara, l'est puna son œuf ! Se moqua Claude en lui tirant la langue.
— Ah ! Saperlipopette, Alban et Claude, allez me faire 5 tours de cour pour calmer votre charmant état d'esprit ! Et que ça saute ! Gronda l'instituteur, soudain fâché.

Les deux grands s’éloignèrent, leurs ricanements couverts par le marteau de l'étameur, qui avait repris sa danse sur le fond d'une marmite ou autre ustensile.

La classe abandonna l’œuf bleu dans sa couveuse, et les cinq poussins dans leur carton. Claudine, qui avait un don pour le dessin, les avait croqué à la mine de charbon, pelotonnés sous la chaleur de la lampe. Bien lui en prit. Le carnage se reproduit dès le lendemain.

Mme Guidiccelli prit le petit corps dans sa main.

*Il a prit un coup de froid,* supputa-t-elle. Elle était venue en catimini de bon matin admirer les nouveaux-nés. Le petit ruban à sa patte rendait la scène pathétique. *Voilà un gamin qui sera bien déçu…*

— Maiiitre, de quoi il est mort Jaune ?
— C'est le Méchant qui l'a emporté ! Persifla Alban.
— Y a pas d'Méchant, qu'à dit le maître ! Couina La fouine.
— Les enfants, ça suffit, ce sont les aléas de l’élevage, certains ne sont pas assez forts pour survivre. C'est tout. La cloche de la récréation va sonner, allons voir nos petits pensionnaires.

Hélas, trois petits corps sans vie gisait dans un coin du carton, le poussin rayé de brun, dressé sur ses minuscules pattes, les regardait, et picorait leurs corps flasques.

— C'est répugnant, il essaye de les manger, c'est un cannibale ton poussin, Boniface, suggéra Fourmi avec dégoût.
— On peut les enterrer, Maiiiitre ?
— Oh oui ! On fera une petite cérémonie, avec un poème comme Mr le curé ! Renchérit Prêlette.
— C'est une prière pas un poème, Prêlette, que lit le curé, et bien que l'idée soit charitable, non, les enfants, pas de cérémonie. Les animaux ne vont pas au ciel de toute manière.
— On va les manger ? Alban hennit de plaisir devant l'effarement des filles.
— Eux non, mais toi, je vais te cuisiner de ce pas sur tes tables de soustractions, rétorqua le maître d'école, lui faisant signe d'approcher. « Incorrigible sacripant. Tu me les copieras dix fois en plus ! Et pour hier ! »

Plus d'un élève sourit, mais aucun ne broncha devant la plaisanterie, à un Mr Guidiccelli en colère, chauve et corse de surcroît, il valait mieux ne pas se frotter. Et puis la vue des poussins flapis les attristait, nul n'avait le cœur à rire, sauf Falban, évidemment.

Mr Guidiccelli disposa des petits corps sans vie, qu'il escamota jusqu'au composteur ; il ne comprenait goutte à ces morts en série, mais l'examen des victimes ne révélait rien. Mort en parfaite santé, aurait pu t-on dire ! Il rumina de plus belle.

L'après midi, dans la cour, les papotages reprirent allègrement, les enfants se consolent vite des pertes passagères, et surtout, tous espéraient la venue de « Bleu », ainsi nommé le futur et attendu poussin de Ragondin, toujours en couveuse.

Insidieusement, le « Méchant » planait encore au-dessus des jeunes têtes, et chacun avait une théorie pour expliquer ces catastrophes à répétitions.

— Un gars qui en veut au maître, un anti corse peut-être avait lancé Alban, en secouant sa grosse tête chevaline.

— Un pu… utois, qui veut pas partager l'appentis, avait suggéré Ragondin, plus pragmatique.

— Un lutin, martelait Albane et Prêlette, toujours le nez dans les fables.

— Un manouche, sûrement, pour Bidule, dont le père accusait ces gens de mille maux à chaque disparition de poulets, d'oignons, ou d'outils sur sa propriété.

— Une maladie contagieuse, enfin, de l'avis de Fourmi, dont le père, exerçait le métier d'herboriste.

Mr Guidiccelli n'accordait foi à aucun ragot en général, ce qui ne l'empêchait pas de mener une discrète enquête auprès des villageois, et des ragots finirent ainsi par se colporter dans le petit village. Les habitants, pères et mères, grands parents de ses élèves, ainsi que d'autres, à l’affût de colportages, ou tout bonnement inquiets de la tournure des événements. Le rétameur en personne, lâcha son outil et s'en vint un matin interpeller son voisin. Tous, quasiment, interrogèrent l'instituteur. Il prit le parti de dédramatiser les faits. Soit, des œufs avaient été détruits, puis des poussins étaient crevés, mais quoi ? A la ferme, on en ferait pas tant d'histoire pour des volailles !

Trois jours après la naissance du premier poussin, la coquille bleue se fendilla enfin. Ragondin battit des mains lorsqu' il découvrit le phénomène en allant soigner « Rayé », l'unique survivant. Toute la classe reprit un peu d'espoir, laissant de coté les théories fumeuses, mais dès le lendemain, le pauvre Rayé fut découvert tout roussi, sous la lampe.

— L'a cuit ! Boniface pleurait à chaud de larmes. Il tenait encore une feuille de salade à la main, qu'il voulait lui apporter pour le déjeuner. Alban le saisit par une patte, et le regarda sous toutes les coutures.
— L'est cuit. Il le reposa dans le carton et haussa les épaules.

Le maître d’école en vint à penser que son expérience tournait court, et qu'elle laisserait un souvenir bien amer à ses jeunes élèves. Il s'en ouvrit dans la soirée à Madame, plus pragmatique.

— Enfin ! Bienvenuto ! C’est une expérience menée dans le cadre d'un cours de sciences naturelles, elle a mal tournée, eh bien ! Tu recommenceras l'an prochain, et pense à changer de fournisseur d’œufs, ce fermier t'a peut-être offert une matière première de mauvaise qualité !

L'homme ne fut qu'à moitié convaincu, mais on ne contrarie pas une femme corse.

Enfin, le quatrième jour, la coquille bleue, craquelée de partout, s'agita en tout sens, et un bec noir luisant comme l'ébène apparu, frappant énergiquement l'enveloppe rigide, infatigablement.

— Y va sortir aujourd'hui, annonça sans l'ombre d'un doute JP.

Les enfants s'attroupèrent à la récréation de l’après-midi autour de la couveuse, ne ménageant pas leurs encouragements à ce « Bleu », tant attendu. Ragondin n'en perdait pas une miette, les yeux rivés sur l’œuf en train d'éclore. L'instituteur décida d'un cours de dessin sur site, envoya La Fouine chercher carnets et mines de charbon, puis chacun se mit à crayonner l’événement.

— Ton œuf ressemble à un ballon de foot ! Taquina Claude, en découvrant le dessin de sa camarade.
— Le tien est tout raturé ! Répondit Albane du tac au tac.
— C'est pas des rayures ! Bougre d'andouille, c'est des craquelures !
— Ce sont les craquelures ! Corrigea l'instituteur. Albane et Claude silence, ou je vous renvoie en maternelle apprendre à dessiner.

La menace fut prise au sérieux ; aucun grand ne souhaitait passer une seule minute chez les petits braillards de Mme Guidiccelli !

— REGARDEZ ! Y passe sa tête ! Il est tout noir ! Oh ! S'écria Prêlette, la tête quasiment enfournée dans la couveuse.
— Pousse-toi !
— Pousse-toi Prêlette !

Chacun se resserra autour de l'autre dans le petit appentis afin de découvrir le poussin. Celui-ci poursuivait sa rude tâche, et déployant son cou. Il s'acharnait à présent entamant le bas de la coquille qui l'enserrait encore. Effectivement, son duvet, d'un noir de jais, et ses yeux, deux billes d'obsidienne, en faisait un petit volatile peu ordinaire. Après un dernier effort, encouragé par les enfants dans un concert de « Oh ! » et de « Ah !», l'oiseau parvint à s'extirper entièrement de son ancien refuge, s'ébroua gauchement sur ses pattes frêles, et émit son premier pépiement.

— L'est noir comme le diable ! Constata Albane, et, en effet, du bout des griffes à la pointe du bec, le poussin était noir.
— C'est p… pas le diable, c'est Sa… atan ! La réprimanda Ragondin, en saisissant délicatement la petite bestiole dans ses mains.
— Ben c'est pas un beau nom, ça ! Glapit La Fouine.
— Y fait comme y veut ! Protesta Médor, qui s'entendait bien avec Ragondin, toujours à le défendre contre les grands.
— Y doit l'appeler Noir ! Contesta Scrofulle.
— J'vois pas pourquoi, c'est son poussin, y fait comme y veut le Ragondin ! S'offusqua Claudine.

Un vigoureux claquement de mains mit fin aux discussions.

— Posez-le dans le carton à présent, Ragondin, porte-lui à manger.
— Faut pas le laissez là ! Le Méchant va l'emporter ! Chouina Bidule, conforté par Fourmi, qui regardait le maître d'un regard si suppliant, que le corse, un dur à cuire pourtant, céda.
— Nous allons installer la boite dans le couloir de la classe.

Un Youpi général accueillit la nouvelle.

— Mais je vous préviens, cette mesure exceptionnelle ne vous exempte pas de vos travaux, corvées et devoirs habituels. Et je ne veux plus un bruit ! En rang ! Ragondin, Alban, vous vous chargez du déménagement, à la moindre incartade, c'est la retenue, samedi après-midi, c'est bien compris les lascars ? » Et les gosses, plutôt fiers d'opérer le transfert sans être chapeautés, s'en tirèrent avec brio. Les pépiements joyeux de Satan investirent bientôt le couloir ainsi que la classe, car l'animal, pour minuscule qu'il était, s'avérait vigoureux, vif, et muni d'un appétit d'ogre. Il avait hélas mauvais caractère, et chacun s'en aperçut à ses dépens ; le volatile béquetait et piquait ceux qui tendait leurs doigts vers lui, sautait sur les mains accortes, griffes en avant. L'oiseau n'acceptait que Ragondin, qui pouvait même se permettre le luxe de lui caresser le dos. Au bout de quelques jours, Satan, désespérément antipathique, n'intéressa plus personne. Son agressivité le rendait trop impopulaire, et l'instituteur proposa à son élève de le remporter chez lui sans plus tarder.

Quelques jours plus tard, Ragondin apparu sur le seuil de la cour avec un énorme coquard . Le maître d'école craignit le pire, le père buvait sec, mais l'enfant raconta une toute autre histoire, à propos de laquelle il émit quelques réserves ; en allant nourrir Satan, le gamin avait cru voir une ombre, et, prenant peur, il s'était mis à courir dans la grange, jusqu'à rencontrer un coin de poutre dépassant d'un mur. Il en était resté assommé pour de bon durant plusieurs minutes, et c'est le poussin, sorti on ne sait comment, qui, juché sur son épaule, lui picotant le lobe d'une de ses oreilles, l'avait fait reprendre ses esprits.

— Tu ne me raconterais pas de salades, Ragondin ? Interrogea Mr Guidiccelli, dubitatif.
— Non, maî… maitre, j'ai cru voir un mé… méchant, mais Y avait rien ! Mon pè… père a dit que même un pou… poussin l'est plus courageux que moi.

Il soupira, attendant les remontrances, mais l'instituteur se garda bien d'ajouter quoi que ce soit. En son fort intérieur, il doutait de la véracité de l'histoire servie.

Environ deux semaines plus tard, comme Ragondin annonçait à ses camarades durant la récréation du matin, avec une certaine crânerie, les premières plumes à son Satan, « d'un noir ! Vous verriez ça ! Le cul de l'enfer a dit mon pè… père ! » Médor avança l'idée de voir l'animal de ses propres yeux, et ce n'est pas peu fiers, que les deux garçons sortirent ensemble de l'école pour se diriger vers la maison de Ragondin, située en face de la cour du rétameur. Celui-ci les salua distraitement, un marteau dans chaque main, la face luisante, échaudée sous le soufflet de forge qu'on entendait ronfler. Les gamins goûtèrent d'une tranche de pain et de deux sucres, sous le regard bienveillant de Madame Ragondin, serrant dans ses bras la petite Manon, trois ans, qui babillait comme un pinson au coin d'un bois. Ils s'éclipsèrent ensuite jusqu’à la grange, situé derrière la maison, une large bâtisse semi-enterrée dont le toit d'ardoises touchait presque terre. Dès le grincement de la lourde porte coulissante, un caquètement sonore retentit, et résonna d'un mur à l'autre dans un écho troublant.

— L'est où ? Interrogea Médor, se grattant le fond de la culotte, manie inconvenante que même l'instituteur ne parvenait pas à lui passer.
— Au fond, dans l'an… ncien enclos de Pa… atatras, le jar qu'est mort l'an dernier, emp… mporté par goupil.
— J'vois goutte ! Reprit Médor, plissant les yeux dans la pénombre des entrelacs de poutres, que seuls quelques rayons de lumière parvenaient à percer, révélant une pluie fine de particules d'or tombant des hauteurs des greniers, bien au-dessus de leurs têtes.

L'autre lui tira la manche, les bruits de gorge du volatile se firent plus sonores, ils se rapprochaient. Soudain, un craquement se fit entendre, et dans un raffut digne d'un tremblement de terre, une lourde pluie d'objets venant des greniers s'abattit sur les gamins. Ragondin tira brutalement son comparse sous la charrette à bras, et sans conteste, ce geste sauva la vie de Médor, ou pour le moins, son intégrité, car à l'emplacement où il se trouvait précédemment, une fourche était à présent plantée, le manche vibrant encore de la force de l'impact. Une masse gisait plus loin, ainsi qu'une lourde boite métallique, dont le contenu, des chevilles de bois, s'était répandu autour d'eux. Ils levèrent les yeux ; un grand trou béait, révélant un coin de tuiles. Le poussin, nullement impressionné par tout ce tapage, poursuivait ses pépiements, comme pour leur souhaiter la bienvenue. 

— Moins une, mon pote, constata Médor en découvrant le bazar.

— Mon pè… ère va être furax, déplora aussitôt Ragondin.

Ils s'avancèrent pour s'assurer que l'oiseau n'avait rien, et avant que Ragondin ait pu intervenir, Médor tendit le cou par dessus l'enclos. Satan n'attendait que cela, aurait-on cru, tant il s'acharna sur la figure du garçonnet, surpris, battant des bras sous l'assaut hargneux de l'animal. Son camarade chassa enfin l'oiseau, mais la face de Médor était labourée de plaies sanglantes, heureusement peu profondes.

— Fa… allait m'attendre lui reprocha Ragondin, lui essuyant le visage avec un vieux chiffon.

Il s'approcha de l'enclos, et souleva le poussin, qui avait beaucoup grandit. Instinctivement, Médor ébaucha un mouvement de recul.

— Le lâche pas hein ! Beugla-t-il.
— Y te fera rien, regarde comme l'est b… eau !

Médor jeta un coup d’œil par la fente de ses doigts, qu'il avait par précaution, placés devant son visage meurtri.

— Il a un plumage terrible ! On dirait du bleu du ciel la nuit, commenta-t-il, admiratif de la bête, malgré sa peur.

Satan paradait, perché sur le bras de son propriétaire, ses premières plumes, luisait effectivement tels des saphirs, sur une carcasse d'un noir sans défaut ; c’en était déconcertant.

— L'as triplé de volume, c'te bête ! Concéda Médor, reculant imperceptiblement vers la sortie.
— Y mange co… omme un ogre ! S'esclaffa ragondin, décidément remit de ses émotions. « Il aime pas les étr…étrangers, c'est tout, et pis tu l'as surpris là, après tout ce cham… hambard qu'est dé… égringolé »

L'oiseau fixait à présent le gamin, et Médor poursuivait sa reculade, croyant percevoir dans le fond de l’œil noir de Satan, un scintillement rouge, une lueur mauvaise.

— Faut que j'rentre ! Il prit la poudre d’escampette dès que sa main toucha la porte derrière lui.
— On se voit demain ! Ragondin caressait l'oiseau, sans s'offusquer du départ brutal de son camarade. Le jeune volatile semblait sourire, oh ! Un sourire d'oiseau, bien sûr, mais un drôle de sourire tout de même…

L'instituteur s'inquiéta de la mine de Médor, qu'il découvrit avec stupeur le lendemain, couturée de Mercurochrome. Les explications vinrent sans tarder, une version plus inquiétante que l'autre. L'homme sentit de la peur dans l'attitude de Médor, un garçon pourtant loin d'être couard. Mr Guidiccelli voulut en avoir le cœur net, il décida de rendre une visite à Satan, et voir par lui-même de quoi il retournait. Mais les jours, puis les semaines passèrent avant qu'il ne se décide, les vacances vinrent, installant une nonchalance toute estivale.

Un jeudi après-midi pourtant, délaissant son confortable fauteuil, son journal et sa pipe, Mr Guidiccelli prit son parapluie, il pleuvait à verse depuis la veille, et entreprit de rendre visite à la famille Ragondin.

Traversant la cour à grands pas, en prenant soin de contrer les flaques faisant un malin plaisir de lui barrer le passage, il tourna le dos à la place des justes, et s'engouffra dans la ruelle des fours, puis sous les cliquetis assommant du rétameur, poursuivit son chemin en face de sa cour, pour s'en aller toquer à la porte de son élève. La mère vint ouvrir, Manon, la benjamine, agrippée à sa taille de manière simiesque. Elle enfouit sa bouille malicieuse dans son tablier dès qu'elle aperçut l'instituteur.

La femme salua le maitre d'école, s'attendant à ce qu'il l'entreprenne sur les diverses incompétences de son rejeton, et fut un peu décontenancée par le sujet de la visite.

— Voir le coq de Raphaël ? Dit-elle en se dandinant comme si elle n'avait pas bien compris la requête du maitre d'école.
— Ah, c'est un coq finalement ? Oui, voir cette bête que nous avons fait naître à l'école…
— Ma foi, une mauvaise idée, si vous souhaitez mon avis, sans vous offenser, Monsieur l'instituteur, reprit la femme, « l'est pas ordinaire, ce bestiau, toujours à vous guigner de son coin d’œil noir de nuit, comme s'il était toujours furieux, et ma foi, l'est souvent, c'est peu dire ! Hier encore Séverin a failli se casser une patte, l'oiseau de malheur lui a déboulé dans les jambes alors qu'il avait la brassée de fourrage pour la bique, s'est étendu de tout son long à deux pas du fer du râteau, les dents prêtes à le mordre » Elle hocha la tête. « Ma foi, l'apprendra à ranger ses affaires, pas faute de toujours lui dire c'te rengaine ! » Elle hocha la tête de plus belle. « Le gamin va vous conduire, avec lui, il est plus amical. Séverin dit qu'il a le diable au corps, c'te bête, et que dès qui fait un bon poids, on le passera tout droit à la casserole avant qui nous esquinte. Il est fait de mauvaiseté, que je sais même pas si c'est une riche idée que de le manger, voyez »

Elle roulant des yeux sans plus d'explications, elle se tourna vers le couloir. Manon avait sorti la tête des jupons de sa mère et tendait à l’instituteur un chiffon baveux aux vagues formes d'ours.

— RAPHAEL ! Descend ! Y a le maitre pour toi ! RAPHAEL !
— J'a… J'arrive m'man.

Il fut bientôt là, saluant son instituteur un peu gêné ; lui aussi s'attendait à des remontrances, sa composition de fin d'année était proche du charabia, et la note avait énervé son paternel, doublant ses corvées du dimanche. Qu'allait-on faire de lui à la rentrée ? Aussi la requête de son instituteur le soulagea tout d'abord, avant qu'il ne s'en étonne.

— Voir Satan ? L'expression sans intelligence de Ragondin fit songer au maitre d'école qu'aucun doute n'était possible sur la filiation de cet enfant. Le portrait craché de sa mère…
— Oui, voir un peu son évolution, et raconter à la classe, dès la rentrée prochaine, les progrès de notre élève adoptif, dit l'instituteur, se trouvant spirituel.

Mais sa tirade tomba plus qu'à plat, balayant le désert aride et infertile qu'abritaient les crânes creux de ses hôtes.

Le gamin s'engagea derrière la maison, l'instituteur sur les talons, les mains dans les poches de sa large blouse grise qu'il ne quittait presque jamais, sauf pour la messe, et la guinguette, le dimanche, avec Madame.

Lorsque l'imposante porte coulissante glissa sur son rail, un râle terrifiant leur parvient de l’intérieur de la grange. Les deux protagonistes se précipitèrent, il faisait sombre, mais pas assez pour ne pas apercevoir la scène d'horreur quand ils débouchèrent pantelants devant l'enclos de Satan. Le volatile, agrippé grâce à ses ergots griffus au cou d'un chat roux, finissait d'extirper un œil de la bête à l'agonie, et de le boulotter, comme des merles goberaient des cerises. La bête avait prit de l'ampleur ; grand, le poitrail large, un port de tête de combattant, une crête noire suie, et des plumes d'un jais scintillant, splendides, en somme une allure terrible. Indifférent à leur intrusion, il pesait de tout son poids sur le matou vaincu, à peine frémissant, et s'acharnait en poussant des cris. Ragondin pénétra dans le parc et tenta de chasser son coq, mais celui-ci ne bougeait pas, battant des ailes pour assurer sa prise sur sa pitoyable proie. Mr Guidiccelli ne perdit pas son sang froid, il avisa une fourche un peu plus loin, s'en saisit, et entra à son tour dans l'enclos à la suite de son élève. Il réussit à coincer adroitement Satan entre deux dents de la fourche, pendant que Ragondin récupérait le matou, immobile à présent. Ils sortirent tout deux à reculons dans un synchronisme parfait, pendant que la bête énervée caquetait de plus belle et tendait le cou, les défiant, le regard flamboyant de colère.

— L'est mort c'te gre… reffier. Constata Ragondin. « C'est çui du ré… rétameur, va pas app… pprécier. Mon pè… ère va encore m'en pa… asser une bonne.
— Mais qu'est-ce que cet animal, un démon  ? S'exclama l'instituteur, à peine remis de ses émotions, ayant relâcher le coq qui parcourait à présent son enclos tel un fauve en cage.
— Il est sau… auvage avec les autres, mais il m'aime bien, c'est mon coq, affirma le gamin, en regardant son instituteur de travers.
— Je crois que si une volaille de 8 semaines est capable de tuer un chat qui fait le double de son poids, il y a de la malice là-dessous, mon garçon, il faut te débarrasser de cette bête, tenta de le raisonner l'homme.
— Papa m'a déjà chan… hanter cette chan… hanson là, il en a pu pour long, je sais, mon Sa… atan et ça me peine, faut pas croi… roire, mon Satan, il est splendide, intelligent et…
— Mauvais, conclut le maitre d'école, en prenant le gamin par l'épaule avec un peu trop de force. Tu vas me promettre de ne plus l'approcher, ton père s'en occupera, je vais lui dire deux mots.

Et ainsi fut fait. La dépouille du chat fut rendue à son propriétaire, mais le rétameur, d'ordinaire conciliant, d'abord incrédule au récit de l'instituteur, demanda réparation et surtout une sanction immédiate et définitive pour le coupable de ce forfait abominable. Mr Guidiccelli, promit un chaton dès qu’il en dénicherait un, et rassura l'artisan sur le fait que, incessamment, ce maudit coq allait passer à la casserole. Mr Grandin se laissa convaincre, mais le reste de cette journée, il fit retentir ses marteaux et ses pinces si bien que la moitié du village eut les oreilles farcies avant le soir tombé. Et l'instituteur fit ensuite la morale à la famille Ragondin, et promesse fut renouvelée que Satan vivait ses derniers jours.

Le dimanche suivant,lors de la messe, qui réunissait, a vrai dire, la majorité des habitants du village, le curé Bresson fit un étonnant sermon sur les différentes apparitions du diable, caché parmi les hommes afin d'abattre sa malice sur les pauvres pêcheurs. Il y était question du rôle de certaines bêtes, vecteur infâme du malin. Cela ne ressemblait guère à l'abbé, et méritait d'y prêter attention car cela dénotait que les bavardages à propos des frasques du coq des Ragondin allaient toujours bon train. Mme Guidicelli rendit compte de tout ceci à son mari, un peu dépitée.

Les ouailles avaient écouté, attentives, rapportait encore la femme, et l'instituteur hocha la tête, préoccupé. Il avait su par son voisin, que, la veille encore, le père de Raphaël avait voulu l'enfermer dans le poulailler, car il tentait à présent régulièrement de s'envoler de l'enclos, mais bien qu'il eut réussi à le fourrer dans un sac, et le relâcher dans la basse cour, il avait regretté très rapidement son geste ; le coq s’était mis à vociférer et attaquer les jeunes poulettes de l'année, leur arrachant les plumes, leur saisissant le dos de ses ergots tranchants comme des rasoirs et les poules de s'enfuir, se cogner et s'éborgner, rendues moitié folles par les assauts de Satan. Raphaël, faisant preuve d'une illumination soudaine, avait alors persuadé son père, furieux, de le laisser intervenir ; et le gamin était parvenu de justesse à s'emparer du coq à l'aide d 'une grosse épuisette puis le traîner sans ménagement, jusqu'en son enclos que le père avait obturé d'un toit grillagé, parant à toute escapade jusqu'au lendemain : ce dimanche, précisément, date de la mise sa mort du coq infernal.

Mais la messe ne pouvait être différée, aussi, habillé de frais, son costume bleu le boudinant un peu, Mr Ragondin priait, sa femme à son coté, en robe jaune fleurie et les deux gosses, endimanchés eux aussi. Il priait, que dieu et ses saints lui assurent tout à l'heure une main ferme pour assommer ce coq de malheur et lui ouvrir le gosier une bonne fois pour toute. Amen !

Le repas dominical achevé, il régnait dans le village un calme apaisant d'après midi. Le vent avait viré à l'ouest, mais la pluie se faisait attendre, et le soleil encore chaleureux, dardait ses rayons le long des ruelles et dans les rues, sur les places et les jardins, invitant au repos. Manon quitta sa mère endormie, se coulant hors de la chambre, parcourut à petits pas le couloir, entendit son père ronfler, dans le fauteuil de la véranda, puis se faufila jusqu'à la porte du jardin. Les parents, abrutis du labeur de la semaine, savourait une sieste d'un sommeil lourd et sans rêve. Personne ne prit garde à l'escapade du bambin.

Ragondin, libéré de ses corvées, jouait dans la cour du rétameur, avec Séverine et François Grappin, deux plus petits, du cours élémentaire. Perchés sur un tas de sable, les enfants aménageait un cirque, avec tous ce qui tombait sous leurs mains. Une cheville de bois devint un hippopotame, une ficelle un boa, ils dessinaient des alvéoles de sable dans lesquelles ils enfermaient, en attente du spectacle, leurs animaux savants. Ils s'amusait de Séverine qui clamait le refrain d'un Mr Loyal imaginaire ;

— Venez admirez les animaux extraordinaires du cirque des trois amis !

Et s'en suivait la présentation de la troupe au rythme de trompettes, toutes aussi fantastiques ! Manon les entendit et passa la tête au coin de la maison, attirée par leurs rires. Mais une autre mélodie lui parvint, du fond de la cour, et sa curiosité toute enfantine l'emporta sur la prudence, car tout comme son frère, ces derniers jours, interdiction était de s'approcher de la grange. Mais la petite à cet instant suivit les gloussements sonores qui lui parvenaient de derrière la maison. La gamine poussa la lourde porte, sans succès, mais comme ce n'était pas la première fois qu'elle s'échappait, elle se mit à croupetons, puis rampa sous le gros battant de bois, où il y avait plus que place pour un petit gabarit d'enfant. Les gloussements s’intensifièrent, Manon babilla gaiement, et, en réponse, des froufroutements lui parvinrent. Elle s'avança dans la bâtisse, puis s'enfonça à l'intérieur sous l'écho du chant entêtant s'éparpillant dans l'air. Elle se dirigeait de confiance, elle avait trois ans, et en découvrant Satan, dans sa robe noire, roulant sa voix, se trémoussant le long de la porte de l'enclos, elle applaudit joyeusement tandis que le volatile multipliait ses facéties. Au bout d'un temps, il se tut soudain, plongeant ses yeux d'encre dans ceux de la gamine et celle-ci cessa tout mouvement. Elle pencha la tête, semblant écouter, pourtant, le silence à présent régnait dans l'endroit. Le coq et l'enfant se touchait presque, à travers les planches de l'enclos. Manon sourit. Puis elle tendit la main vers le verrou de la petite porte de la cage en condamnant l'entrée.

Quelques minutes plus tard, Raphaël, Séverine et François levèrent la tête. Monsieur et madame Ragondin s'éveillèrent en sursaut, et d’autres habitants, non loin de là, stoppèrent net leur ouvrage ; qui de malaxer une pâte à pain, qui de tricoter ou encore trier les haricots. Le maitre d'école, abandonnant sa lecture, s'avança sur le trottoir pour découvrir d'où venait le tintamarre. Le rétameur, un peu en retrait, dans son atelier, tirait sec sur le soufflet de forge chauffant le godet d'étain, qui bouillonnait doucement, attendant patiemment la température parfaite, afin d'en enduire ses casseroles, bassines et autres ustensiles. Il leva un sourcil, essayant d'identifier ce bruit qui l'avait dérangé.

Un hurlement strident trancha l'air paisible, tel un couteau du beurre frais. Ragondin sauta du tas de sable, ayant reconnu l'origine de ce vagissement, sa petite sœur ! Il intima aux deux autres de ne point bouger, avec difficulté, car son bégaiement allait croissant dès qu'il s'énervait. L'étameur sortit sur le pas de son atelier, grattant son front, ses yeux interloqués suivant la brusque débandade du gamin, franchissant à longues enjambées, l'allée de sa maison, qu'il contourna pour foncer dans la cour. Il faillit renverser sa sœur arrivant en sens inverse, rendu aveugle par le sang qui coulait à flot de son petit visage. Son œil droit pendait lamentablement le long de sa joue, rattaché uniquement par un fin ligament sanglant. Ses cheveux maculés de terre, ébouriffés, sa robe sale et chiffonnée, tout indiquait l'accident. Le garçon la prit dans ses bras, comme elle se débattait encore, tremblante et affolée, il lui parla sur un ton d'apaisement, et elle se laissa finalement aller contre lui en sanglotant, incapable de répondre à ses questions. Mais il n'eut point besoin d'en poser beaucoup, le responsable de tout ce charivari traversait à présent la cour, perché sur ses grands ergot noirs, avec au fond de son œil de ténèbres, un reflet d'or, une diablerie. Ragondin lui fit face, mais encombré de sa sœur, il ne put l'empêcher de passer, et la bête fila, flèche noire, gloussant de plus belle, en direction de la rue. Ragondin déposa la gamine devant la maison, avec une rapidité qui aurait bien surprit Mr Guidiccelli, et que le gamin n'avait, hélas, pas dans la résolution de ses devoirs de calculs. Il se mit à pourchasser l'animal, saisissant au passage une vieille canne de croquet qui traînait là. Le coq se percha à grands coups d'ailes sur un muret, Ragondin lui lança son arme de fortune sans succès autre qu'effaroucher l'oiseau. Il sauta côté rue, et, après un instant d'hésitation, et un coup d’œil rancunier sur son poursuivant, s'engouffra chez le rétameur. Mr Guidiccelli abasourdi, avait suivi la scène depuis son observatoire, il s'avança prestement en direction de toute cette agitation.

Aux cris d'alerte de Ragondin, les deux enfants, Séverine et François, se réfugièrent dans leur maison, tandis que Grappin père courut au devant de la bête enragée qui lui fila adroitement entre les jambes, mais n'eut d'autre choix, bloquée dans le goulet de la cour, que de s'enfourner dans l'atelier. Ragondin y fut d'un bond, le rétameur ferma la marche et la porte par la même occasion. L'instituteur se trouva malheureusement coincé dehors, d'aucun secours pour les deux autres. Une chaleur intenable les accueillit, ainsi que les gloussements rageurs de Satan, acculé. Ragondin saisit un marteau au long manche, et délogea l'oiseau dessous des planches, puis de l'établi où il s'était perché. Le rétameur tenta de l'attraper par derrière mais récolta un violent coup de bec et des coups d'ailes au visage, il recula, surpris, et grogna de colère.

Ragondin lui lança un vieux sac de toile dans les pattes, mais le coq ne se laissa point impressionner, il s'élança à travers l'atelier, évita le grand soufflet, survola le godet emplit d'étain, et se fracassa contre une fenêtre, fermée, par une heureuse providence. Le maitre d'école regarda l'oiseau s'acharner contre la vitre et se hasarda à trouver une arme pour en défendre l'accès. Le rétameur contourna la bête folle, mais le volatile avait déjà reprit ses efforts pour opérer un demi-tour, et traverser de nouveau l'atelier pour atteindre une lucarne, située à l'opposé, entrouverte, celle-là… De nouveau, il s'élança majestueusement, ses grandes ailes noires miroitaient sous le feu de forge, il entama son chant de victoire, cocoriquetant, croyant en sa chance, entrevoyant déjà sa liberté, quand l'étameur, d'un élan de taureau saisit la queue d'une lourde poêle, lui en assena un coup à pleine volée et l'envoya valdinguer dans le godet bouillant.

Ragondin poussa un cri de désespoir, voyant son Satan sombrer dans l'étain fumant, mais l'animal continuait de se débattre, tout en s’enfonçant dans le creuset de métal en fusion. Il criait à présent comme personne depuis n'a entendu crier un coq. De rage, il osa l'impossible : il ouvrit plus largement encore ses ailes désormais luisantes, recouvertes d'étain, extirpa son corps en fusion et s'élança à l'assaut du lucarnon. Mais le métal se solidifia, figeant en un instant le coq alourdi par tout cet étain flamboyant qu'il avait sur lui. Il chuta lourdement sur le sol, et mourut, là, le bec dans le sable, les ailes déployées, égaré dans un éternel et majestueux simulacre de vol. Ses yeux avaient brûlé, et pourtant, on aurait juré qu'il y avait tout au fond comme un éclat rubis miroitant.

Le jeune type, toujours enthousiaste, commentait allègrement, le cou penché à récolter un sévère torticolis, la bouille tendu vers la flèche de l'église Saint Pierre et Paul ;

– Non, mais regardez moi cette prestance ! On le croirait prêt à prendre son envol ce coq ! Ce naturel ! C'est confondant !

– Pour sûr, marmotta le vieillard entre ses dents qu'il avait rare, émergeant avec peine de ses souvenirs, resserrant sa blouse grise élimée autour de ses abatis fatigués, « une girouette comme la nôtre, jeune homme, vous n'en verrez pas de sitôt ! »
