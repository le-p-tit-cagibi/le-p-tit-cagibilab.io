---
title: Johnny
author: SB
rights: Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Quand Johnny découvre le Rutilant, il ne sait pas encore que sa vie a déjà prit un virage à 180 degrés.<br>Arrivera t-il à en reprendre les commandes ?"
epub: true
pdf: true
draft: false
---
Johnny extirpa la mobilette du garage avec difficulté. L'endroit ressemblait à un animal repu dont la panse béait de s'être trop goinfré. La porte basculante, bloquée à mi-hauteur n'avait cédé que lorsque le garçon avait réussi à se faufiler à l'intérieur pour déplacer de vieux cartons empilés au niveau du vérin cassé commandant l'ouverture de la porte. C'est alors qu'était apparu l'engin, un solex, accroché par la roue avant à un crochet métallique fixé sur un des murs. Il avait encore fallu enjamber d'autres cartons éventrés à terre comme des animaux écrasés qu'on aperçoit au bord des routes, au petit matin, par la fenêtre d'une voiture filant bon train. Avec dégoût, Johnny ne ménagea pas son effort pour pousser ces rebuts d'un autre temps, piétiner des piles de journaux jaunis, rouler une jante rouillée jusqu'à l'extérieur, escalader un vieil escabeau aux marches défoncées ou absentes venu mourir là, en prenant soin de ne pas se coincer un pied dedans ou se blesser avec les vis rouillées qui pointaient de-ci delà. Il atteignit enfin l'objet de son attention ; le carénage se devinait bleu pétrole sous une épaisse couche de poussière. Il tira, secoua mais ne parvint pas, dans un premier temps, à libérer le cyclomoteur, qui pesait au moins autant que lui-même. Il fit un effort supplémentaire pour se coller le long du mur, repoussant des rouleaux de papiers peints qui chutèrent brusquement dans un vacarme retentissant.

– Ça va mon grand ? cria son père un peu inquiet, par une fenêtre du premier étage donnant sur la façade avant de la maison où se situait l'entrée du garage.

– Tout est OK ! répondit Johnny, ahanant sous l'effort. Enfin il souleva suffisamment l'engin pour décrocher la roue, mais il se sentit glisser, emporté par la force d'inertie du poids du vélomoteur et ils s'affalèrent ensemble, dans un ralenti de cinéma, sur des coussins de canapé hors d'âge, échoués par un hasard heureux juste sous lui, parmi un amoncellement de vieilleries plus ou moins identifiables. Le garçon souffla et sourit, le guidon dans les bras. C'était un guidon bien rouillé, dont les manchons de plastique noir, troués et usés, éraillaient douloureusement les paumes de ses mains. Il ne s'en soucia pas vraiment, tout à la tâche d'extirper enfin sa trouvaille à l’extérieur.

Sous le soleil de juillet, l'engin accusait les marques des ans, la corrosion piquait l'ensemble du cadre et du cylindre tel une maladie de peau, et la couleur bleue, altérée, en accentuait encore l'effet. Mais dans les yeux de Johnny, il était magnifique. Sur la face extérieure du réservoir, on pouvait encore lire, de grandes lettres cursives majuscules argentées formant le mot « le Rutilant ».

Un nom épatant, songea le garçon, de plus en plus ravi de sa trouvaille. Des pas se firent entendre dans l'escalier menant au sous-sol et son père apparut, à l'autre extrémité du garage, la tignasse recouverte de toiles d'araignées et de saletés.

– Tu t'en sors ?

– Impec ’pa ! Tu crois que tu pourras m'aider à le retaper ?

L'homme se tordit le cou afin d'apercevoir de quoi il s'agissait. Il se fit la réflexion que si ce tas de ferraille fonctionnait un jour, il avalerait sa clef à molette, voire le tournevis avec ! Mais tout à la joie qu'il découvrait sur le visage de son fils, si rare, depuis le deuil qui les avait frappés, il opta pour un optimisme sans borne.

– On verra ce qu'on peut faire mon grand ! Allez, pose ta bécane et viens me donner un coup de main si tu veux dormir dans ta chambre ce soir !

Mr Lebrun gara sa Dyane Caban noire et blanche devant le 7, rue de la Noxe. Lucie descendit du siège passager en aboyant mollement, dans l'intention de s'annoncer, comme à son habitude. L'homme claqua la portière, et s'approcha de la maison.

– Il y a quelqu'un ? Ho Ho ?

Les fenêtres baillaient grandes ouvertes, un tas de gravats gisait dans la cour. On entendait des coups de marteau et des voix. Il s'avança jusque la porte d'entrée, la poussa, et réitéra son appel.

Une cavalcade dans les escaliers précéda l'arrivée d'un homme d'une quarantaine d'années, aux tempes grisonnantes, et d'un garçon brun de poil et de peau, aux yeux étrangement clairs, d'une quinzaine d'années peut-être. Une belle connivence émanait d'eux, une tristesse aussi. Le vieil homme chassa cette dernière, et s'avançant encore un peu, se présenta ;

– Bonjour, je suis Mr Lebrun, un de vos voisins. Vous n'aurez qu'à m'appeler Jacques, comme tout le monde. Il tendit sa main et l'enfant s'en saisit promptement, l'agitant avec détermination.

– Bien le bonjour, Jacques, mon père et moi sommes ravis de faire votre connaissance ! Je m'appelle Johnny Brûlefer et papa c'est Bruno. Le dit Bruno sourit et serra à son tour vigoureusement la main de nouveau tendue.

– Vous arrivez pile pour le petit déjeuner ! Un café ?

– Avec plaisir, si je ne dérange pas !

– Du tout ! Je vous fais la visite, si vous voulez, après ! Proposa le garçon, déjà en haut de l'escalier à croquer dans un croissant.

– Je vois que madame Choppin a un nouveau client, plaisanta Jacques, en arrivant sur le palier de la cuisine, découvrant une corbeille remplie de viennoiseries.

– Trop bon ! Confirma le gamin en l'invitant à se servir, tandis que son père versait le café, bouillant, dans d'antiques mazagrans de grès.

– Vous m'avez l'air bien installé déjà, constata Mr Lebrun en opérant un tour sur lui-même, découvrant ainsi la salle à manger attenante à la cuisine, et le couloir qui menait aux chambres. Les pièces visibles avaient été lessivées, les vitres nettoyées, le sol seul, portait les traces d'une activité intense, recouverts de petits débris de plâtres, de papiers peints et d'autres in-identifiables.

– On avance oui, renchérit Bruno, souriant, en écartant ses larges mains, il reste les chambres à retapisser, un peu de peinture sur les murs de la salle de bain et surtout…

– Déblayer le garage ! S'exclama Johnny en riant. « Un vrai bazar là-dedans, mais aussi une mine d'or ! »

– Comment ça une mine d'or ? Reprit le vieil homme intrigué.

– Ah, rien d'extraordinaire en fait, pas de lingot, précisa Bruno, « mon fils s'est pris de passion pour un vieux cyclomoteur découvert pendu au fond du sous-sol. Il s'avère que, finalement et contre toute attente, il ne faudra pas grand-chose pour le remettre en état de marche ! »

– Il est magnifique ! approuva Johnny, des étoiles dans ses yeux azuréens « si vous êtes d'accord, vous ferez connaissance avec le Rutilant ! »

Mr Lebrun eut du mal à garder un visage impassible, et encore plus à ne pas s'étrangler avec la bouchée de brioche qu'il mastiquait.

– Le Rutilant ? Tiens donc, il a un nom ton engin ? Questionna-t-il un peu brusquement .

– Oui ! Oui ! Et une sacrée personnalité ! Ajouta le gamin sans se rendre compte du malaise tangible du voisin.

– Arrête un peu, Johnny, laisse Mr Lebrun prendre son café tranquille, va débroussailler le chemin, qu'on puisse accéder en bas, s'il te plaît, et arrête de t'exciter comme une puce sur un chien !

Lucie justement, chienne golden retriever de son état, aboya joyeusement, en signe d'acquiescement, et accompagna le garçon, dégringolant comme lui, les escaliers à fond de train, tandis que les deux adultes s'attardaient en haut.

– Ça va ? Vous n'avez pas l'air dans votre assiette ? S'enquit Mr Brûlefer, un peu dépité par le silence soudain de Jacques, d'abord si jovial.

– Oui, c'est juste, ces engins à moteur, avec les gosses, il faut faire attention, il joue un peu à faire les fous avec ça. C'est…

– Ne vous inquiétez pas, le coupa Bruno, mon fils est un bon gamin, pas stupide, enfin pas trop pour son âge, la machine sera fonctionnelle d'ici quelque temps, mais son utilisation, elle, sera sous conditions.

– Sous quelles conditions ? Si je peux me permettre, insista Mr Lebrun.

– Eh bien ? Travailler à l'école, faire ses tâches à la maison, vous voyez, enfin, ce qu'on attend d'un ado pas trop mal élevé ! Ironisa l'hôte un peu dépité par la mine sérieuse du vieillard.

– C'est bien, vous avez raison, approuva l'homme en entendant les appels de Johnny depuis le garage, « Excusez-moi, parfois, je suis vraiment un vieux ronchon, allez faite moi la visite » conclue-t-il sur un ton plus accort.

Bien que le pas traînant des nouveaux venus se fut entendu au moins cinq bonnes minutes avant leur passage devant le garage, Johnny se trouvait tellement concentré sur sa tâche qu'il sursauta lorsque Denis, le plus âgé des frères Braconi s'adressa à lui mâchouillant une allumette, « pour se donner un genre », renâclait son père, employé municipal et garde chasse de surcroît, car son aîné typiquement, il le rangeait dans la catégorie qu'il intitulait avec dégoût des « bons à riens ». Et ce malgré un nombre incalculable de punitions, brimades et autres réjouissances.

– Alors le môme, on bricole ? Tu espères quoi ? Qu'elle va démarrer ta pétrolette ? Denis pouffa, et son rire fut repris par deux énergumènes qui l'accompagnaient toujours, Franck, son frère, le benjamin de la famille, et José, le petit-fils de Mme Choppin, l'épicière, qui avait pris le mauvais pli, comme se lamentait la vieille dame, de traîner tout le temps avec eux. Les grandes vacances ne faisaient que débuter, et ces trois-là passaient beaucoup de leur temps libre ensemble, au château d'eau, le plus souvent, en haut de la cote de l'ancienne départementale 120, où ils avaient élu leur « quartier général » buvant des bières et fumant ce qui leur tombait sous la main, ou encore folâtrant dans la zone de friches au-dessus du vieux rond-point, à l'embranchement de la station essence désaffectée et de l'ancien dépotoir, fermé par décision préfectorale depuis plusieurs décennies.

– L'est moche ta mob, mec ! L'apostropha José. Sa mine chafouine le classait d'office dans les gars mielleux, faux comme une roue sans moyeu, et pas courageux pour un sou, même sa grand-mère en convenait.

– T'as essayé de la démarrer ou c'est juste un joujou de gosse pour rigoler ? Insista Denis, en s'approchant de Johnny, la mine rigolarde, les yeux plissés, essayant de déchiffrer le mot gravé sur le réservoir. « le Rutilant, ah ouais ! Dis donc, ce serait pas plutôt « la pourrave » ton engin ? »

– Putain, il est sourd en plus ce môme, regarde les yeux de merlan frit qu'y nous jette ! L'a jamais vu de vrais mecs ou quoi, ce naze ? S'esclaffa encore José.

Johnny releva la tête sous les injures, décidé à ne pas entrer dans leur jeu stupide.

– Avec mon père, on lui a donné un bon coup de jeune, elle ne va pas tarder à rouler, je nettoie les bougies, ça devrait être bon après.

– On veut une démonstration, le môme, lui intima Denis, le fixant de ses yeux ronds, atteint d'une légère myopie. « On veut une démonstration tout de suite, allez, grimpe là-dessus et pédale, voir qu'elle démarre cette mob pourrie ! »

– Je parie trois clopes qu'elle démarre pas ! Lança José en riant.

– Vas-y ! démarre-la ! L'enjoignit Franck, un petit blond à lunettes, en le poussant sans douceur.

Johnny remonta la dernière bougie et s'essuya les mains après un vieux torchon. Pour une fois, il eut bien apprécié que son père se pointe en lui demandant de faire la vaisselle ou autre chose, mais il était parti à la ville voisine effectuer quelques courses. Le garçon jeta un œil du côté gauche de la rue, mais il n'y avait rien à attendre de ce côté-là, c'était la sortie du village, personne n'habitait plus loin. Il soupira.

– Grouille un peu, Tartempion ! Tu saoules ! Allez démarre ta bécane ! Denis le tira par le bras de sa chemisette et celle-ci craqua le long de la couture avec un bruit sec.

Johnny sauta sur la selle. Il avait un peu peur maintenant ; ces types-là sans conteste étaient abrutis, mais en plus, ils avaient l'air particulièrement peu amènes. Il se dit que si le Rutilant démarrait, il s'éviterait peut-être les coups, sinon… Alors il s'élança, crânement debout sur les pédales, serrant la poignée noire dans sa main et se mit à mouliner comme un dératé. Le pédalier sollicité pour la première fois depuis bien longtemps, fut difficile à entraîner au début, et Johnny sua bientôt à grosses gouttes sans que rien ne se passe au niveau du démarrage.

– Vaut rien du tout ton engin ! Bon pour le dépotoir ! Siffla Denis en lui envoyant une légère pichenette sur la tête.

– Ouais, j'ai gagné mes clopes ! S'exclama José, tapant dans la main de son comparse, puis claquant ses doigts à l'oreille du gamin apeuré qui se remit à pédaler de plus belle, ne sachant comment échapper à leurs quolibets. Le Rutilant émit un chuintement poussif, et les trois larrons restèrent une seconde sans voix, puis la bécane se tut, et ils explosèrent de rire, applaudissant comme devant une bête de foire.

C'est à ce moment qu'une voix grave venue du côté droit de la rue, auquel nos protagonistes tournaient le dos, vociféra ;

– Fichez-moi le camp d'ici bougraille ! Et ôtez vos sales pattes mesquines de ce jeune gars ! Fissa !

– Madre ! Le curé Bresson, souffla Denis en se retournant, déjà sur la défensive.

– Z'inquiétez pas, padre, c'était pour rigoler, on se présente à notre nouveau voisin, pure politesse ! Ajouta pour l’esbroufe, Franck, amorçant un pas de repli.

– Dégagez ! Choppin ? Qu'est-ce que tu fiches encore avec ces deux-là ? Gronda le prêtre en gesticulant des deux battoirs qui lui servaient de mains et vous, les Braconi, sortez de mon champ de vision ou j'envoie le diable vous bouffer les orteils dès cette nuit, DEGAGEZ !

Debout sur les pédales, immobile, Johnny assistait à l'échange verbal en serrant les dents. De honte tout d'abord, et de colère aussi. Voilà qu'il avait besoin de l'aide d'un prêtre pour se sortir d'une situation délicate, c'était d'un cocasse ! Et en plus la trouille lui torpillait le ventre, quel idiot. Il s'assit lourdement sur sa selle en regardant les trois jeunes s'éloigner.

– Merci, osa-t-il au bout d'une minute en relevant la tête alors que l'abbé Bresson l'observait les bras croisés sur son giron, sans mot dire, mais sans reproche non plus.

– De rien, fiston. Ton père est absent je présume ? Je venais lui porter le petit livre de l'histoire du village. En guise de bienvenu, en quelque sorte, nous nous sommes croisé l'autre jour chez l'épicière. Tu te nommes ?

– Johnny, Monsieur le curé. Mon père n'est pas là, non, sinon ces trois-là n'auraient pas moqué le Rutilant !

L'homme de dieu s'approcha, ouvrant grands les yeux à présent ;

– Mais oui, bougre de diable cornu, mais c'est bien le Rutilant ! Tu l'as retapé ? Tu l'as trouvé où ? Mitrailla le curé, très étonné et ému de cette découverte sans que le garçon n'en comprenne l'origine.

Johnny entreprit donc de lui raconter sa trouvaille et le travail acharné qu'il pratiquait dessus dans l'espoir de la voir rouler de nouveau, l'arrivée inopinée des trois larrons et sa déconfiture.

Le père Bresson hocha la tête, la mine sévère. Il n'hésita qu'un instant, puis lui conseilla d'un ton sans réplique :

– En premier lieu, jeune homme, ne jamais t'aventurer avec les trois zigotos de tout à l'heure. Si tu aperçois ne serait-ce que le bout de leurs chaussures, fuit. Ce sont de sales types. Et secundo, je t'admire toi et ton père d'avoir récupéré cette machine, et je comprends que tu aies à cœur de la remettre en état, cependant, le Rutilant a un passé, somme toute calamiteux, peut-être serait-ce judicieux que tu en prennes connaissance, qu'en penses-tu ?

Sur ces entre-faits, Mr Brûlefer fit son apparition, au volant d'un petit 4X4 jaune paille ; une musique des années 90 se répandait dans l'habitacle aux fenêtres ouvertes.

Le prêtre le salua, son petit livre à la couverture de cuir naturel toujours à la main, et tous trois ne tardèrent pas à pénétrer dans la maison, à la recherche d'un peu d'ombre et d'un grand verre de limonade.

L'homme de dieu ne dit pas un mot de l'altercation qu'il avait eue avec les frères Braconi, et d'un accord tacite, Johnny ne conta pas plus sa rencontre désastreuse avec le gang des trois débiles. Au bout de quelques politesses, le père Bresson, cependant, se mit en devoir de raconter l'histoire du Rutilant.

– Ça se passait dans les années cinquante, le village était plus vivant à l'époque, il y avait du monde, des familles ouvrières, l'usine de céramique de Villeneuve, a quelques kilomètres d'ici, marchait encore. La maison que vous habitez était celle du contremaître de l'usine. Et cette bécane, il fit un geste en direction de l'extérieur, appartenait à son fils. À l'époque, c'était un fameux engin ! Tout le monde en rêvait, beaucoup ici, allaient à pied ou à bicyclette pour se déplacer. Le gamin du contremaître se baladait partout avec. Il faisait bien des jaloux, mais bon, ça ne lui a pas porté chance. En ce temps-là, la départementale 120 longeait le garage, la vieille station essence actuelle, si vous préférez, qui n'existait pas encore, et la route remontait ensuite jusqu'au rond-point du château d'eau, un gros kilomètre et demi plus haut, un fameux kilomètre ! Une pente à près de vingt pour cent. Enfin, bref, le gamin a dévalé la pente, il a perdu le contrôle du Rutilant, c'est le nom qu'il lui avait trouvé, et que son père avait fait graver, et il s'est encastré dans la porte métallique du garage ; autant vous dire qu'il n'en est pas resté grand-chose, du môme. Mais il avait été éjecté de la bécane, qui contrairement à ce qu'on pourrait supposer, fut à peine cabossée ; Le père ne s'est jamais décidé à s'en débarrasser, ni de la maison non plus d'ailleurs, je suppose qu'il l'a remisée où vous l'avez trouvée. Lorsque l'usine a mis la clé sous la porte, quelques années plus tard, le père est parti, la mère, ça faisait bien longtemps qu'elle avait mis les voiles, après la mort de son petit, et la maison est restée close. Au début, une voisine s'occupait de l'entretenir, d’aérer et tout, et puis, plus rien ; la maison est restée fermée jusqu'à votre récente venue. Il tendit son verre vide, la gorge sèche après sa longue tirade.

– Et bien dites donc, mon père, vous avez le chic pour mettre de l'ambiance, lança Bruno, mi-figue, mi-raisin. Il regarda son fils, qui restait silencieux, et ajouta, le taquinant, « nous serons donc d'autant plus prudents avec ce solex, personne n'envisage d'avoir le moindre accident, n'est-ce pas Johnny ? »

– Je serai très prudent, affirma le gamin, appuyant sur chacune des deux syllabes. Il s'excusa auprès du prêtre, et fila jusqu'au garage peaufiner quelques réglages.

– Le grand jour est pour quand ? Demanda l'abbé Bresson en souriant malgré lui devant l'insouciance juvénile de Johnny.

– Dimanche, je pense, nous avons dû colmater le réservoir, et graisser pas mal de pièces. Il nous reste à acheter un casque en ville. Johnny est très impatient !

– je m'en doute, prenez bien soin de lui, Bruno, persista néanmoins le prêtre avant de prendre congé et n'oubliez pas de lire mon petit livre sur l'histoire du bourg. Il est instructif, Villevieille est bâtie sur des fondations millénaires.

– Je n'y manquerai pas, répondit Mr Brûlefer, en jetant vaguement un œil sur la couverture, où figurait, estampillée, la petite église du village, Sainte-Marguerite.

– Oh dites donc en voilà un raffut ! S'écria Mme Choppin en tendant deux baguettes et un pot de miel au professeur. « Il a la bouille rosie par le plaisir, votre gamin ! Ajouta-t-elle en souriant.

– Il va falloir encore quelques réglages pour éliminer ces pétarades, j'en conviens, dit le père en regardant s'éloigner son fils sur le Rutilant arborant un casque bleu de la même teinte que sa bécane.

– Ne vous inquiétez pas, mieux vaut le voir s'amuser là à parcourir la cambrousse à cyclomoteur que faire l'imbécile à boire ou autre misère ! C'est comme ça qu'il arrive des embrouilles, je vous le certifie ! Foi de Marie Choppin. Et le passé me donne raison, il n'y a qu'à voir, en 74, l'année du choc pétrolier, le premier, le seul qui vaille, eh bien, ici, on a eu un incendie de tous les diables, volontaire qu'ils ont dit les pompiers ! Vous vous rendez compte ? Volontaire !

En fait, le pauvre Bruno, fataliste, ne se rendait compte que d'une chose, il avait ses cours à préparer mais n’échapperait pas au conte de bonne femme de l'épicière. Et en effet, celle-ci repris, en même temps que son souffle, la suite de son récit.

– Alors, bien sûr, ça a été une chance que, du dépotoir, le feu ne se propage pas jusqu'aux pompes à essence ! Tout aurait pu y passer ! Elle roula de gros yeux, exorbitants, tout à fait disproportionnés vu sa figure de pomme granate ratatinée, adoptant une mine tragique, de circonstance. « Mais tout de même, reprit-elle sur le ton de la confidence, le lendemain, les pompiers ont retrouvé un petit corps dans la friche, un des gamins de la boulangère. À cette époque, on avait encore une boulangerie à Villevieille, oui monsieur, eh bien, le gosse était mort asphyxié, c'est pas malheureux ? Une histoire comme ça ? Ces gamins étaient toujours fourrés du côté de la friche, y traînaient avec des plus grands, et voilà. » Elle claqua ses deux paumes l'une contre l'autre et Bruno sursauta malgré lui.

– Quelle sombre histoire, se permit-il pour tout commentaire, en quittant la vieille dame sur un signe de tête, alors qu'elle concluait, nullement vexé du départ quelque peu précipité de son client,

– L'est mieux tout seul sur son engin, croyez-moi !

La porte se referma avalant ses dernières paroles. Le professeur se remémora la visite du prêtre, et quelque chose au fond de son esprit clignota, lui titilla la cervelle, les poils de ses bras se hérissèrent un instant puis le soleil irradia la rue et il se réjouit de jardiner l’après-midi, après ses préparations de cours, bien sûr, et s'en retourna chez lui.

Johnny s'élança du garage, racla le seuil de la porte, projetant quelques étincelles sur le ciment, et fila à gauche, sur la départementale 120, jouant sur la manette des gaz, hypnotisé par le ronronnement joyeux du Rutilant. Quelques centaines de mètres plus loin, Lucie vint à sa rencontre, frétillante, et le gamin ralentit son engin à hauteur de Mr Lebrun, qui visiblement s'accordait une balade avec sa chienne, en cette belle fin de journée.

– Tu m'accompagnes ? Lui lança-t-il, ça me fera un peu de compagnie.

– D'accord, répondit Johnny, toujours prêt à rendre service ou à faire plaisir. Il stoppa la mobylette et opéra un demi-tour. Mr Lebrun siffla avec admiration. « tu l'as drôlement bien rénovée mon gars ! Un vrai petit bijou ! Mieux vaudrait éviter la section là-derrière, conseilla-t-il, se retournant vers l'ancienne portion de route où l'on voyait pointé au loin le panneau rouge et blanc de l'antique station essence. Tu vas y laisser tes pneus, ce n'est plus entretenu par là.

– D'accord, acquiesça une nouvelle fois le garçon, accommodant.

– Viens, je vais te montrer une belle balade à faire, pose ton joujou en passant, et préviens ton père, par la même occasion !

Et ainsi firent-ils ce soir-là, délaissant sa belle monture, Johnny prit grand plaisir à parcourir la campagne, à pied, puis, après une courte halte pour se désaltérer, à bord de la splendide Dyane Caban décapotable de Mr Lebrun. Ils empruntèrent nombre de petites routes à peine plus larges qu'un chemin, qui les menèrent à la ferme de Berbe, le berger, puis au Grand Mesnil, une autre ferme. Ils s'accordèrent un bref arrêt à la source dite « du campeur » même si, de mémoire, Mr Lebrun n'avait jamais aperçu le moindre campeur dans ce coin. Ils s'en revinrent par la route de Villeneuve, mais bifurquèrent en amont afin d'admirer la Noxe, un petit ruisseau où s’ébattaient des canards et des poules d'eau, ils passèrent un pont branlant, mais solide, affirma le vieil homme, et poursuivirent ainsi jusqu'à finalement rejoindre le presbytère, niché derrière l'église.

– Un fameux tour ! Merci Jacques, s'exclama Johnny en claquant la portière de la Dyane lorsque celui-ci le déposa chez lui.

– Mais de rien mon garçon, et rappelle-toi de mon conseil, ne te fourvoie pas du côté de la station ! Allez, mon bonjour à ton père !

La petite voiture repartit en vrombissant, et le conducteur se permit un bref coup de klaxon, vu que l'heure n'était pas trop avancée, et que les gens d'ici étaient pour la plupart des anciens, des vieillards un peu sourds donc.

Un matin d’août, qu'ils déjeunaient de croissants et de brioches encore chaudes, Johnny interpella son père, un petit livre marron à la main ;

– Je peux te l'emprunter ?

– Tu peux oui, j'ai déjà trop de lectures à faire pour la rentrée, dans une vingtaine de jours, nous y sommes, il faut que je finalise mes cours alors le bouquin de monsieur le curé attendra ! Lança-t-il en riant. « il t’intéresse vraiment ? » l'interrogea-t-il sans malice.

– Oui, en fait, ça doit être sympa de connaître les vieux trucs qui se sont passés dans le village. Avant il y avait du monde ici, près de mille habitants, c'est l'abbé qui me l'a raconté, et Mr Lebrun m'a affirmé qu'il y avait de nombreuses boutiques avant, un coiffeur, un glacier, tu te rends compte ! J’aimerais bien qu'il y en ait encore un ! On ne serait pas obligé d'aller jusqu'à Villeneuve pour un cône à la vanille ! L'abbé Bresson, il en connaît un rayon sur Villevieille et ses habitants ! Ceux d'hier autant que ceux d'aujourd'hui ! Ajouta Johnny tout sourire.

– Tiens donc, il te balance des potins ! Le taquina Mr Brûlefer ;

– Bah, il sait raconter les choses, par exemple ; cette Mme Choppin, pendant la guerre, elle a caché des juifs, et son mari en est mort, il a été dénoncé. Elle est restée veuve, avec ses fils, et aujourd'hui plus personne vient la voir, sauf son petit-fils, José, qui file un mauvais coton, toujours d'après l'abbé.

– Je vois que tu n'en perds pas une miette, le moqua gentiment son père, lui ébouriffant les cheveux, bon, je dois m'absenter ce matin, pas de bêtise, on se retrouve à midi, pour le déjeuner, d'accord ?

Johnny hocha la tête d'un air entendu et le regarda partir, à bord de son indécrottable 4X4 jaune paille. Ce jour-là, il faisait doux, bien que le ciel fut fort gris, la pluie ne semblait pas vouloir laver les rues, aussi le garçon se décida pour un petit tour avec son Rutilant, emportant un sandwich et une bouteille d'eau au cas où il s'attarderait un peu. Il se saisit aussi du petit livre, qu'il bourra dans la poche arrière de son jeans, fourra le casse-croûte dans son sac et descendit au garage.

La machine scintillait malgré le temps maussade tellement il l'avait encore lustré la veille ; il la poussa précautionneusement dehors, clipsa la lanière de son casque sous sa gorge, grimpa sur la selle de cuir marron, large et confortable, puis bascula le cyclomoteur sur ses béquilles et, debout droit comme un « i », Johnny se mit à pédaler comme un fou furieux afin de la démarrer. La machine toussa, siffla, puis pétarada joyeusement, tandis que le gamin repoussait les béquilles d'un geste habile en s’élançant sur la route. Il prit à gauche, avec dans l'idée de tourner avant le rond-point abandonné, pour suivre le long des sentiers caillouteux, le cours sinueux de la Noxe et peut-être aller souhaiter le bonjour à Lucie et son maître. Mais au carrefour, ses bras engourdis et battus par la brise ne fléchirent pas le guidon. Le Rutilant poursuivit son chemin en direction de la station-essence, fiérot du tour qu'il jouait à son cavalier, lorsque soudain un claquement métallique se fit entendre et s'amplifia de manière alarmante. La station-service offrait déjà à la vue sa large face morne aux vitrines aveugles. Johnny fit jouer les gaz mais la bécane toussa poussivement, et sur ce, le moteur cala dans un dernier râle. Le solex poursuivit sa trajectoire jusqu'aux pompes rouillées dont les peintures rouge et blanche, fort lessivées par les intempéries, accentuaient une impression d'affection larvée. Le garçon, toujours assis sur sa machine désormais statique, soudain mal à l’aise, observa plus attentivement la devanture ; des morceaux de crépis décollés par endroits, comme une lèpre, pendaient, impudiques, tandis que le guichet obturé par une grande planche de bois recouverte de graffitis de couleurs criardes et obscènes juraient avec la grisaille ambiante. Et au comble de l'inquiétude, Johnny eut le sentiment qu'elle le regardait aussi, cette façade, qu'elle l'épiait de biais, à travers les lucarnes des toilettes, sur le côté. Deux panneaux publicitaires hors d'âge tournoyaient dans un vent léger, grinçant au bout de leurs chaînes, et l'impression d'être guigné se renforça encore, alors que Johnny descendait de sa pétrolette. Il frissonna ; des crampes dans le ventre lui arrachèrent une grimace dubitative.

Les tuyaux d’alimentation des pompes à essence serpentaient sur le béton fracturé et écaillé, tels de lourds et gras serpents alanguis après un copieux repas. Un relent d'hydrocarbures lui chatouilla désagréablement les narines. Son regard s’attarda sur la porte en verre, défoncée et renforcée par une plaque métallique ; celle-ci n'avait pas l'air bien fermée, comme si elle avait été forcée à maintes reprises. Johnny sentit une sueur froide couler le long de sa colonne vertébrale, il n'arrivait pas à détacher les yeux de la massive station. Le vent, plus fort à présent, hululait dans les fentes et les fenêtres mal jointées, le bâtiment dégageait une aura étrange, il attirait le gamin et simultanément, celui-ci éprouvait à son égard une réelle aversion.

Il fit un pas vers la porte.

Une voix tonna dans le vent.

– Johnny !

Un aboiement le ramena violemment dans la réalité de l'instant. Il détacha avec effort, comme à regret, ses yeux de la bâtisse et chercha l'origine de l'appel.

– Jacques ! J'allais justement chez vous, s'excusa-t-il d'une voix pâteuse, en découvrant le brave homme, immobile sur la route à quelques pas, les mains sur les hanches, d'où pendait la laisse de Lucie.

– Tu prends un sacré raccourci pour me rendre visite, mon garçon, lui lança-t-il d'un ton de reproche à peine dissimulé, « je croyais t'avoir expliqué qu'il n'y a rien de bon ni d'intéressant à faire dans ce coin perdu. Tu n'as pas écouté ? »

– Si, si, Mr Lebrun, c'est le Rutilant qui m'a fait un caprice, il ne marche plus ! Je suis en panne, expliqua-t-il, avec au fond de lui, un sentiment de malhonnêteté, car ce n'était pas la stricte vérité, il ne souhaitait pas mentir au vieil homme, et cependant, il se doutait que s'il lui disait ce qu'il en retournait, son sympathique voisin lui ferait un peu, voire beaucoup la morale, et il n'en avait pas envie ; il se sentait suffisamment malheureux sans savoir réellement pourquoi.

– Viens par ici, jeune homme !

Johnny se mit en devoir de pousser sa machine récalcitrante jusque sur la route, ahanant, et soufflant comme un bœuf ; l'antique machine pesait le poids d'un âne mort.

– Tu as essayé de la redémarrer ?

– Euh oui, je crois.

– Tu crois ? Mr Lebrun le scruta attentivement, s'approcha à le toucher, et lui saisit le coude avec douceur.

– Je crois, moi, que nous ferions mieux de nous mettre là-bas, sur le plat, derrière le rond-point, tu essaieras à nouveau. Ça va mon garçon ? Interrogea-t-il un soupçon d'inquiétude dans la voix.

– Oui, ça va maintenant. Il se retourna vers la station-essence et murmura entre ses dents « elle est bizarre, cette ruine, elle fiche un peu la trouille »

– Elle a une histoire, comme nous tous. Allez viens, j'ai dans l'idée qu'un bon lait chaud ne te fera pas de mal !

Johnny stabilisa sa machine un peu en retrait du rond-point. Sans s'être concerté, tous deux tournaient délibérément le dos à la station-service. Ils s'affairèrent sur la bécane vérifiant le niveau d'huile, la chaîne, mais rien ne semblait dysfonctionner. Le garçon monta alors sur le cyclomoteur. Il démarra immédiatement, le bruit du moteur, fluide, couvrant le couinement lascif des pancartes métalliques, et ils s'éloignèrent rapidement, leurs pas plus légers à chaque foulée qui les éloignait du rond-point. Johnny faisait des allers-retours en zigzaguant avec son Rutilant tandis que Mr Lebrun accélérait encore, accompagné de Lucie jappant et gambadant allègrement autour d'eux.

– As-tu passé une bonne journée ? Lui demanda son père lorsqu'il retrouva Johnny, le soir venu, blotti au fond du confortable canapé en train de lire le petit recueil de l'abbé Bresson.

– Extra ! Et le gamin commenta sa rencontre avec son vieux voisin, jacques, mais omit sciemment l'objet de cette rencontre fortuite. Il ne voulait pas alarmer son père. Il s'en voulait un peu d'avoir flippé pour rien, finalement, et il avait conscience d'avoir fait une peur bleue au vieil homme, ses mains tremblant alors qu'il remontait vers sa demeure, sans jeter ne serait-ce qu'un coup d’œil à la station en ruine, et pourtant, tout cela lui semblait un peu ridicule à présent. Bien que, pour être totalement honnête, Johnny non plus d'ailleurs, n'ait pas regardé derrière lui ; il aurait craint que la station-essence ne le nargue de ses lumignons aveugles, ou que sa large porte bancale, telle une bouche de métal, soit prête à claquer des dents s'il en franchissait le seuil… C'était insensé ; alors il se tut.

– Qu'as-tu appris sur Villevieille aujourd'hui ? Le questionna encore son père, coulant un regard en direction du bouquin que son fils venait de poser sur le bras du sofa.

– Avant que le garage n’existe, puis la station-service, il y avait un petit château, à peu près au même endroit ; il a été en partie détruit pendant la révolution, par un incendie, puis il a été laissé à l'abandon, car il n'y avait plus d'héritiers… Il possédait deux tours rondes, on les nommait les tours jumelles de Villevieille.

– Il y a des photographies ? Montre ! S’enthousiasma le professeur.

– Non, il n'y a que des gravures pas géniales du tout, répondit Johnny sans entrain. Il n'avait soudain plus envie de parler de tout ça, il désirait oublier combien il s'était senti mal tout à l'heure. « On va à la pêche, demain ? » poursuivit-il sachant qu'il abordait là le sujet de prédilection de son papa.

– Ah ! Une fameuse idée, mais je dois absolument travailler demain matin, on peut partir sur le coup de midi, on pique-nique si tu veux, qu'est-ce que tu en dis fiston ?

Johnny respira plus librement et se berça des paroles rassurantes de son père, le contemplant tout à la joie anticipée de passer un après-midi à taquiner les gardons.

Le buggy vrombit non loin de la rive ; ainsi tout espoir de prendre ne serait-ce qu'un chabot poilu et peu ragoutant s'évapora. Bruno soupira, ses pauvres oreilles subissant les assauts sonores répétés des bruits d'un moteur poussé plus que de raison, espérant seulement leurs rapides disparitions. Mais l'engin se rapprocha avec à son bord les deux frères Braconi et un autre, plus jeune, que ne reconnut pas le professeur. Ces deux jeunes gars, le prêtre lui en avait fait un topo guère reluisant, un jour qu'ils s'en étaient pris à un vieux matou posté sur un muret, le long du cimetière. Bresson les avait vertement assaisonnés de moult épithètes imagés agrémentés de quelques malédictions de son cru. « Ces jeunes sont toujours à faire ou préparer un mauvais coup » lui avait-il confié, las des frasques de la petite bande, « gosses, déjà ils essayaient de casser mes vitraux, troublaient l'eau bénite avec des Alka Seltzer, dévastaient le potager, des malfaisants ! Avait-il poursuivi, « pourtant je leur ai tiré les oreilles et botté le derrière méthodiquement ! » Soupira-t-il encore ce jour-là qu'il était passé inviter la famille Brûlefer à un pique-nique dominical organisé par l'association des dames de la paroisse de Sainte-Marguerite.

Le moteur stoppa son cirque infernal à seulement quelques mètres de la rive. Les trois compères descendirent, arborant des casquettes voyantes aux marques de compagnies pétrolières américaines. Johnny identifia immédiatement le plus jeune, José, à sa mine de furet.

– B'jour M'sieur ! Petite sortie familiale ? Questionna l’aîné des Braconi, la mine narquoise. Il tapa du coude son frère, dans les cotes, et celui-ci se mit bêtement à rire, en se frottant l'abdomen, soulevant sans aucune pudeur son tee-shirt d'un blanc sale parsemé de tâches et de traces, laissant apparaître une peau glabre d'une pâleur maladive.

– Bonjour les jeunes, répondit Bruno, le plus neutre possible « nous passons un moment tranquille au milieu de la nature, c'est exact. » Il se leva lorsque les trois lascars arrivèrent à sa hauteur, autant pour se dégourdir les jambes, assis qu'il était depuis un bon moment sur ce tabouret inconfortable à trois pieds, que pour marquer son rôle d'adulte, et si possible recadrer de suite ce qu'il envisageait pouvoir devenir un possible dérapage avec ce genre de garçons, parce qu'il sentait un fond de malveillance rôder autour de cette petite bande.

– Ouais, un beau coin, on vous l'a un peu salopé là, faudra bouger plus loin, désolé ! Ajouta Denis, sortant une cigarette et s'approchant près du bord de la rivière, attentif aux bouchons qui lambinaient encore dans l'eau. « vous aurez plus de chance de faire une belle prise sous le pont, ici y a rien ! Reprit-il, frappant une motte de terre de son pied et l'envoyant valser dans le cours d'eau.

– Vous avez raison, nous allons explorer le pont, temporisa Mr Brûlefer, sans se démonter, regardant Denis fumant immobile, la lippe mauvaise, droit dans les yeux.

Au même instant, Johnny se tenant un peu à l'écart, José profita du moment où les deux frères prenaient quelque peu à partie son père pour lui souffler, d'une voix aigrelette et terriblement ridicule,

– Eh mauviette, quand est-ce qu'on te voit au château d'eau ? T'as les chocottes ou quoi ? Même pas fichu de se décoller des baskets de son papounet cinq minutes !

Il cracha sentencieusement par terre, alors que Johnny restait coi.

– On se casse, aboya Franck, et son frère jeta dans l'eau son mégot encore fumant.

– Bonne journée ! Au plaisir M'sieur ! Ajouta le petit-fils de Mme Choppin et disant cela, il fixa Johnny, lui tira la langue, et se propulsa vivement dans le buggy, devancé par les deux autres. Un vacarme abrutissant accompagna leur départ.

Père et fils ne bronchèrent pas, pas même quand le petit véhicule passa tout près d'eux dans un grabuge digne d'un hélicoptère au décollage.

– Je ne les aime pas du tout ceux-là, fut le seul commentaire que se permit le professeur en remballant son matériel, et quelque chose au fond de son esprit clignota, lui titilla la cervelle, les poils de ses bras se hérissèrent un instant puis son fils s'autorisa un vigoureux hochement de tête en forme d'acquiescement, il n'y pensa plus.

Ils ne firent finalement aucune prise sous le vieux pont de pierre, mais décidés à ne pas se décourager, père et fils suivirent le cours de la Noxe, à l’affût de poissons, lançant de temps en temps une ligne, sans conviction cependant, car leur après-midi s'avérait irrémédiablement gâchée par les trois intrus et leur comportement désagréable ; ils rentrèrent donc tôt, rompus et bredouilles.

– La station-essence Azur a connu « la belle époque », mais les derniers à en avoir été gérants ont vécu un drame dont ils ne se sont jamais remis et ils ont fermé boutique.

– Ce n'est pas noté dans votre livre, mâchonna Johnny en s'accordant un second cookie, pendant que son père sirotait tranquillement une bière sous le porche du presbytère, à côté de l'abbé.

– Je ne vais pas relater tous les faits tragiques de cette petite ville ! Se récria Bresson.

– Il semblerait qu'il y en ait un certain nombre, non ? Commenta Bruno.

– Bah ! Comme dans tous les villages, vous savez, chacun connaît son lot de malheurs et de misères ! Rien de plus, hélas.

– Il est arrivé quoi à cette famille ? Insista Johnny, touillant sa tasse de chocolat un peu refroidie.

– Les Mofard ? Je crois ou Boffard… quelque chose comme ça… mince, je perds la boule, je dois avoir noté ça quelque part, malgré tout, le gamin a été baptisé dans ces murs, voyons…

L'homme d'Église s'éloigna, sa longue robe noire le grandissait encore, et lui donnait une apparence et une carrure impressionnante, intimidante, presque inquiétante, malgré son attitude affable, joviale. Il retourna à l'intérieur puis revint en fourrageant dans une caisse de bois débordant de papiers, qu'on aurait pu croire

apprêtée pour allumer la cheminée, mais non, comme le confirma l'abbé Bresson, en tendant un feuillet jauni, il s'agissait d'archives paroissiales dûment répertoriées.

– Ah, voilà, Boffard. Eugénie et Maurice, l'enfant s'appelait James. Oui, James, un petit gars joufflu et toujours de bonne humeur, ça me revient. Il n'a pas eu le temps de faire son catéchisme, quelle tragédie ! Un bel hiver, il devait avoir, six ou sept ans, nous avons essuyé de la neige ! Une de ces couches ! Et ça tombait ! Durant des jours, de la neige, avec un gros froid bien sec. Les gosses s'en donnaient à cœur joie, bonhommes au nez carotte et bras branchus, batailles matin, midi et soir, luges jusque potron-minet, sauf que le gamin, on ne sait pas comment, mais il s'est retrouvé sous les roues d'un camion qui descendait la départementale 120 ; tué sur le coup. La mère a fait une dépression nerveuse, après l'accident, à chaque poids-lourd qui venait de la cote, elle s'enfuyait de sa caisse en pleurant. Je l'ai reçu quelques fois, à confesse, une bien pauvre femme, ravagée par le chagrin. Elle affirmait que ce n'était pas normal, que le petit savait que c'était interdit d’aller sur la route, surtout la 120, ça roulait là-dessus, en ce temps-là ! Bref, ils ont quitté la station-essence, personne n'a voulu reprendre après ça et puis, la mairie était déjà en pourparler de fermer définitivement cette portion de route.

– C'est moche, assena Bruno en regardant affectueusement son fils tendre la main vers un troisième ou quatrième gâteau, il avait perdu le compte.

– Pourquoi elle n'est pas démolie ? Questionna le gamin.

– La station ? L'abbé sourcilla.

– Oui, il est vrai que ça dessert un village pittoresque comme le vôtre, des bâtiments à l'abandon comme ça, renchérit Mr Brûlefer.

– Sûr ! Mais qui financerait une telle entreprise ? Tant qu'il n'y a pas de projet, la station continuera de pourrir et rouiller sur ses fondations, conclue le curé Bresson, tendant les sucreries restantes à Johnny avec un clin d’œil.

– Je vous vois dimanche alors ? Pour le pique-nique ? C'est la rentrée le lundi suivant, venez profiter d'une belle journée, et puis vous ferez connaissance avec les ploucs du coin ! L'homme de dieu se mit à rire bruyamment, rejoint bientôt par Johnny et son père. Le curé se révélait de bon conseil, aussi convinrent-ils de passer le dimanche suivant à la fête paroissiale.

– Laisse ton tour, morveux, éructa aigrement José en passant devant Johnny dans la file d'attente de la petite épicerie. Une femme d'un certain âge, grisonnante et courbé sur son panier d'osier, se poussa d'instinct pour le laisser également passer devant.

– Tu attends comme tout le monde, José Choppin ! Protesta l'épicière, en s’essuyant les mains sur son tablier.

– J'veux juste des bières, j'en ai pas pour longtemps, Mme Miret, elle a le temps, elle est en retraite, hein Mme Miret ?

– Laisse-la tranquille, commanda Johnny entre ses dents, les yeux fixés au sol.

– Toi, t'as pas la parole, morveux, on t'a toujours pas vu avec ta bécane au bout du chemin du château d'eau. ‘spèce de trouillard sans couille !

– C'est tout ce qu'il te faut ? Demanda contrariée la grand-mère à son petit-fils.

– Impec, met sur mon compte ! Lança-t-il en gloussant.

Il sortit de l'arrière-boutique, et fila dans le sillage de son comparse, Denis, qui l'attendait sur le seuil de la boutique, raflant encore au passage quelques paquets de crackers et un pack de chewing-gum.

– José ! Protesta faiblement Mme Choppin, rose de honte pour son petit garçon. Elle jeta un pauvre regard sur Johnny, lui sourit, et le renvoya rapidement dès qu'elle l'eut servi. Il la vit s'asseoir sur le banc de la boutique avec Mme Miret, encore toute secouée. Elles se mirent à bavarder avec force de gestes et caquètements, dès qu'il eut franchi la porte de la petite épicerie.

Johnny tenta une sortie avec le Rutilant dès le lendemain, mais à peine quelques centaines de mètres parcourus, il aperçut le buggy qui venait en sens inverse. La boule au ventre, il fonça jusque sur le parvis de l'église, le traversa trop vite, et les pavés inégaux firent tressauter salement la bécane ainsi que chaque os de son corps. Il atteignit heureusement la courette du presbytère avant les trois lascars, qui passèrent sans ralentir, en gloussant.

– Je te fais visiter Sainte-Marguerite ?

Johnny, encore chamboulé de sa courte mais vive échappée, saisit la perche que le curé lui tendait, ne sachant s'il l'avait aperçu se sauvant devant les trois grands benêts, et comme celui-ci n'en parla point, il suivit docilement l'abbé Bresson le long d'un sentier gravillonné de blanc qui coupait le jardin, puis ils passèrent sous une petite porte de bois cintrée, plantée dans un mur de larges pierres, formant une sorte de rempart massif autour de la propriété.

Ils débouchèrent sur le parvis de l'église. Sa flèche fine et crénelée s'élevait dans l'azur, élégante, tandis que le long des toits, aux abouchements des gouttières de pierre, quelques gueules de gargouilles arrogantes, dragon, grenouille, rapace, grimaçaient vers leurs deux uniques spectateurs. Les vitraux, la rosace, étaient modestes, les couleurs rouge et bleu dominaient et lorsqu'ils pénétrèrent dans l'édifice, la pénombre régnait, malgré le franc soleil d'été à l'extérieur. Il faisait frais ; Johnny rabattit la main devant ses yeux, l'abbé Bresson le regarda songeur,

– C'est un effet de contraste très décontenançant, la Sainte, protectrice, aime les lieux d'ombres, sa main cajoleuse s'étend ici, apaise, sens-tu son souffle de bonté ?

– On est bien ici, il fait bon, commenta le garçon, plus pragmatique.

– Oui, hum, se reprit le prêtre, les églises sont de vrais frigidaires ! Ils se regardèrent et s'esclaffèrent, leurs rires se répercutant le long des voûtes, l'écho les gratifia de sons criards, totalement inconvenants en ce lieu de recueillement, et ils se turent.

Johnny sortit donc moins, à la fin des vacances, conscient que le défi lancé par José et ses comparses allait lui coller à la peau et ne sachant comment s'en débarrasser, ou si bien même il devait en parler à quelqu'un. Sa couardise lui insupportait, vis-à-vis de ces « grands » et en même temps, il se trouvait stupide d'y prêter attention. Alors, désœuvré, abandonnant souvent « le Rutilant » au garage dans un souci de discrétion, il rendait visite à l'abbé quelques fois et à Mr Lebrun quasiment tous les jours, déroulant en leurs compagnies l'histoire du village, qui parfois rejoignait celle de la vie des deux hommes, originaire de Villevieille. Le gamin s'empiffrait de cookies, et jouait des heures durant avec la brave Lucie, ne se lassant jamais de lui lancer son ballon, imitation taille réduite d'un ballon de rugby officiel, qu'elle rapportait fièrement en frétillant et jappant à tue-tête, en bon golden retriever qui se respecte.

Son petit livre accompagnait désormais Johnny partout, coincé au fond de sa poche, et il avait plaisir à en feuilleter une page de temps en temps, bien que le plus souvent, il oublia même qu'il était là. Il le gardait cependant toujours près de lui, tel un talisman. Quelques récits valaient particulièrement le détour d'une lecture, comme ce concours de cloches, au début du siècle, où l'on apprenait que le maréchal-ferrant, en l'honneur de son village, avait fabriqué une cloche et que, bien que celle-ci n'ait en aucune façon décrochée de prix au concours régional, elle trônait à présent fièrement dans le clocher de Sainte-Marguerite, distillant un doux tintement de bronze et de vent. Il avait admiré, en marge des pages, les esquisses de plan, de la main de l'abbé, brouillonnes mais amusantes, car l'homme s'était évertué à répertorier les petites particularités architecturales du village ainsi que les noms des rues, ruelles et venelles apparues et disparues au fil des ans. Ainsi la « ruelle des fous » côtoyait la « rue des apprentis » et « le chemin borgne » avait disparu pour faire place au « chemin de la liberté ».

Le temps s'écoulait doucement dans le petit village, et si, au fond de lui, Johnny ne pouvait faire cesser cette inquiétude qui pulsait dans ses tripes et faisait parfois battre son cœur beaucoup trop vite, il s'arrangeait pour éviter d'y penser, éviter les ennuis. Mais les ennuis rattrapent invariablement les garçons qui tentent de les éviter. N'est-ce pas ?

Le samedi, veille de la fête paroissiale, alors que tout le village apprêtait le parvis pour un repas champêtre, brinquebalant tables, chaises, auvents, bancs et autres matériels plus ou moins encombrants et utiles, Johnny s'assit dans un coin, en sueur. Il venait d'apporter le parasol familial en contribution, ainsi qu'une glacière de boites de salade de pâtes, la seule spécialité culinaire dont il se revendiquait meilleur cuisinier que son père. Chaque paroissien contribuait ainsi au pittoresque déjeuner de demain. Bientôt, il aperçut Mme Choppin avec une brassée de fleurs qu'elle arrangeait sur les tables, les piquant dans des pots de grès remplis de sable humide. Des enfants qu'il n'avait qu'aperçu jusque-là devant des maisons sur une balançoire ou tenant la main de leur mère, couraient partout, se chamaillant, accompagnés d'adultes, qui portant un saladier, qui portant une corbeille ou un autre encore, une pile de linge, des nappes sans doute. Tous se dirigeaient en discutant, à pas tranquilles, vers le presbytère, l'abbé se chargeant de caser la nourriture dans son cellier, attenant à sa maison. Les dames de la paroisse, pour la plupart, se trouvaient au frais dans l'église, à décorer la Sainte, debout sur son socle de bois peint, pour la procession du lendemain matin, procession qui entraînerait tous les habitants ou presque, le long de la Noxe, tant la tradition valait religion dans cette région.

Le gamin entendit soudain le vrombissement caractéristique du buggy, il plongea involontairement plus en retrait, à l'ombre d'un grand lierre dont les rames s'étendaient jusqu'au pied du mur contre lequel il s'était appuyé. Il se renfonça encore en entendant les frères Braconi appeler José, braillant, depuis la route, face à l'épicerie. Johnny sentit son ventre gargouiller, et ce n'était pas de faim. Le petit-fils de Mme Choppin sortit de l'échoppe en claquant la porte et sauta dans la voiture. Le gamin les entendit parler d'une virée à Villeneuve, et d'un coup jaillit une idée ; bien qu'il la trouvât stupide, tentant d'abord de se raisonner, comme on peut le faire à cet âge, et pourtant, il ne put s'empêcher de la mettre à exécution.

Il s'éloigna donc discrètement dès les trois imbéciles disparus au bout de la rue et remonta en direction de sa maison, sans s'y arrêter. Son père finissait de préparer quelques cours pour lundi et dans ces cas-là, il perdait la notion du temps, rien à craindre de ce côté. Johnny avait le champ libre. Il passa ensuite rapidement devant la station, lugubre, dont les chuintements sinistres le firent frissonner. Au-dessus de la station, un soleil estival inondait cette journée et cependant, une ombre grise collait au site abandonné. Il accéléra l'allure, déterminé. Il irait. Il monterait au château d'eau. Bon, d'accord, les Braconi n'y seraient pas, mais il l'aurait fait. Il tourna le plus vite possible le dos à la station-service, et entreprit de grimper la cote. Elle se révéla bien raide, très raide, et malgré son jeune âge, sa bonne constitution, et sa résolution toute neuve, il faillit abandonner non pas une fois, mais deux ;

La première lorsque qu'il aperçut le château d'eau, courtaud, ramassé tel un fauve guettant sa proie, au milieu d'un fouillis d'arbustes chétifs et d'épineux tordus que ronces et vignes vierges envahissaient du tronc aux branches les plus menues. Johnny suait à grosses gouttes à présent, son ventre le boxait de l’intérieur, mais il reprit sa route, grimpant la pente escarpée, forçant le pas, pressé par le temps aussi.

La seconde fois, ce fut lorsqu'il déboucha sur une grève noirâtre, au détour d'un court chemin, en haut de la cote, où se tenait la bâtisse. De là, on dominait Villevieille, ses vignes sur les coteaux ensoleillés, l'église en bas, au centre de la petite vallée. Le château d'eau massif, était en fait à demi enterré. Une porte rouillée, avec un panneau « interdiction d'entrée » grinçait dans le vent, pourtant il ne sembla pas au garçon qu'il y avait de vent. Le toit du bâtiment était entièrement recouvert de mousse et de terre, de couleur jaunâtre ; il avait la vague forme d'un champignon. Johnny se dit qu'il était allé au bout de son idée, qu'il devait s'en retourner maintenant. Mais au lieu de cela, il se dirigea vers l'entrée, poussa et poussa encore contre la porte, s'échinant jusqu'à ce qu'un interstice suffisamment large lui permit de se faufiler. Il faisait noir à l’intérieur. De vieilles chaises de jardin en plastique gisaient dans un recoin ainsi qu'un tonneau d'acier débordant de canettes de bière vides. Le repaire des trois imbéciles, à n'en pas douter, songea Johnny. Dans un silence de tombeau, un bruit d'eau, gouttant sur le béton glissant était perceptible et sembla s'amplifier lorsque le garçon s’avança dans le ventre du château d'eau. Il huma l'humidité, une odeur écœurante de moisissure, d'eau croupie, lui souleva le cœur. Par-delà une rampe, le réservoir s’offrit alors à sa vue. L'eau miroitait légèrement par l'intermédiaire des rayons de lumière filtrant par la porte entrebâillée. Johnny s’avança, se pencha au-dessus, et une torpeur l'envahit ; sa tête s'alourdit, une nausée le foudroya tandis qu'il sondait sans succès le fond de l'eau, absent. Il se retint à la rambarde métallique. Insidieusement, quelque chose ici attisait sa curiosité ; il fit un pas, puis encore un autre le long de la coursive. Ça remuait dans l'eau lui sembla-t-il, une ride ondula, contraria la plane surface du réservoir. Le cœur dans l'estomac, écarquillant les yeux, Johnny scruta de nouveau l'onde, où seules quelques bulles éclatèrent dans un ploc mat. Le garçon eut un haut-le-cœur, il détacha avec peine ses yeux de la surface noire, stimula mentalement son corps dans un élan de courage, ou de peur, et s'efforça de rejoindre la sortie. Les gouttes se mirent à tomber plus vite sur le sol bétonné et le son en était soudain plus fort, presque assourdissant. Johnny perdit momentanément le sens de l'orientation, et repartit vers le réservoir, chancelant. Il entendit encore quelque chose, là, au fond de l'eau qui clapotait. Il s'arracha presque douloureusement à sa réflexion, poussa sur ses jambes lourdes et les força à reprendre la direction de la sortie, se bouchant les oreilles. Arrivé enfin non loin de la porte, il put courir et ne s'en priva pas. Sitôt dehors, il coupa par les champs, culbuta un seau en plastique déglingué, faillit perdre l'équilibre et s'enfuit à travers la campagne vers le village. Il n’ôta les mains de sur ses oreilles qu'à mi-pente, sans oser se retourner, tremblant. Filant sans reprendre haleine jusqu'à son garage, Johnny enfourcha machinalement son Rutilant, la tête en feu, le corps secoué de sanglots ; il pleura jusqu'à épuisement.

– Qu'est-ce que tu fais encore là ? Un problème avec la mob, Johnny ? Du haut de l'escalier, le ton soucieux de Mr Brûlefer, était assez inhabituel. Le comportement de son garçon, ces derniers jours, ne lui ressemblait pas ; il s'enfermait, s'isolait et parlait peu de nouveau, comme au premier temps de leur deuil. L'un avait perdu une femme, l'autre une mère, le déménagement semblait avoir remis tout le monde en selle, et pourtant… Le professeur se remémora les mots du prêtre, les histoires de Jacques et quelque chose au fond de son esprit clignota encore, lui titilla la cervelle, les poils de ses bras se hérissèrent un instant puis la voix de son fils lui parvint.

– Nan, t'inquiète, je la briquais un peu pour la sortie de demain, au pique-nique de la paroisse ! Répondit Johnny en croisant les doigts pour que son père ne descende pas et ne s'aperçoive pas de ses vêtements débraillés et de sa mine défaite.

– Il est tard, j'ai fini pour aujourd'hui. Veux-tu qu'on aille jusque Villeneuve manger une pizza ?

– D'accord, lui répondit le garçon conciliant, de manière qu'il espérait suffisamment enthousiaste pour tromper son père. « j'arrive dans cinq minutes, je me change en vitesse et c'est bon ! »

– Ok, on décolle dans un quart d'heure alors, résuma le professeur en s'éloignant dans la maison.

Johnny souffla. Il n'était pas question qu'il narre son périple idiot de l'après-midi. Pas question. Et puis raconter quoi ? Il ne s'était finalement rien passé. Il avait eu les chocottes, voilà tout, José la fouine n'avait peut-être pas tort, après tout.

Plus tard, dans la nuit, après quelques parts de pizza, un soda et une glace à la vanille, sa mésaventure lui parut burlesque. Il l'enfouit au-dedans de lui, profondément. Il se dit qu'il en rirait d'ici quelque temps, même si, ce soir-là, il écouta fébrilement sa respiration dans la nuit, au creux de son lit, la couette rabattue par-dessus sa tête. Puis le sommeil l'appela, il se mit à songer à la journée de fête qui s’annonçait, et s'endormit sur ces espoirs de réjouissances à venir.

Johnny enfila ses vêtements rapidement, le réveil sonnait à tue-tête. Le petit livre tomba de sa poche de jeans sur le parquet ; il le ramassa, hésita, le jeta sur le lit, tout en constatant qu'il ne lui restait qu'un dernier chapitre à lire. Il finirait ce soir, se dit-il, avant la rentrée des classes, et le rendrait à l'abbé. Puis son père l'appela, et il n'y pensa plus.

– Johnny ! Eteins-moi ce ding dong insupportable, s’il te plaît et dépêche-toi de venir déjeuner ! Nous allons être en retard à la procession !

Ils filèrent en vitesse, abandonnant la vaisselle sale dans l'évier, rappelés à l'ordre par le carillon de Sainte marguerite. À grands pas, père et fils rejoignirent la D120 en direction du centre-ville alors qu'une haute silhouette s’avançait prestement en contre-sens.

– Avez-vous vu Lucie ? Les questionna brutalement Mr Lebrun, apparemment trop fortement chamboulé pour s'être aperçu qu'il n'avait, dans sa précipitation, salué personne.

– Non, du tout, elle est partie en vadrouille ? Demanda Mr Brûlefer en stoppant à hauteur du vieil homme.

– Ah ! Ça ! Ce n'est pas possible ! En neuf ans, cette brave chienne ne m'a jamais quitté plus de quelques mètres ! Rétorqua leur voisin, « Je la cherche partout depuis le lever du soleil, pas de trace. Je suis inquiet, je n'y comprends rien, poursuivit-il la voix chevrotante, « La porte de la véranda était ouverte ce matin, j'ai constaté une effraction, mais rien de volé, sauf Lucie qui a disparu ! » L'homme semblait abasourdi et hors d'état de poursuivre quelque recherche que ce soit.

– Elle a dû faire fuir les voleurs et a peut-être pris un mauvais coup, elle a eu peur, elle va revenir, le rassura Bruno.

– Il est dix heures du matin, le sermonna Jacques, si elle avait dû réapparaître, ce serait déjà fait, non, sincèrement, je suis très inquiet. Il fourra la laisse dans sa poche et resta sur le trottoir, indécis, le regard vague. Au loin, des villageois s'en allaient rejoindre la procession. Elle débuterait à dix heures trente, sous la houlette de l'abbé.

Johnny n'y prêta guère attention, il n'avait rien dit jusque-là, mais un très mauvais pressentiment l'avait progressivement envahi tandis que Jacques et son père s'entretenaient. Comme le carillon chantait toujours, entêtant, le garçon haussa la voix pour se faire entendre autant que dans l'espoir de se donner plus d'assurance, et rasséréner un peu son ami.

– Je retourne chercher le Rutilant et je fouille le village ! Lança-t-il, guignant l'approbation de son père, qui le soutint immédiatement.

– Vas-y, oui, et tu reviens vers nous si tu vois quelque chose, nous allons commencer par retourner chez Jacques, voir si Lucie n'est pas rentrée, ou terrée quelque part dans la maison.

– Elle est peut-être blessée, gémit le vieil homme en serrant la laisse au fond de sa poche. « je ne me pardonnerai pas qu'il lui arrive quelque chose ! Venez ! S'écria-t-il à l'attention de Bruno, se dirigeant vers le centre-ville, tandis que Johnny courait déjà en sens inverse.

Il ouvrit avec fracas la porte coulissante du garage, débéquilla le Rutilant, sauta sur les pédales, démarra du premier coup, et partit sur les chapeaux de roue, laissant l’appentis béant. C'est seulement alors qu'il se rendit compte que le collier de la chienne pendait après le guidon de son cyclomoteur. Instantanément, son ventre se contracta douloureusement. Il s'arrêta à hauteur du trottoir qui longeait la Noxe, mit la bécane au ralenti, extirpa le collier, une simple lanière de cuir fauve, supportant une plaque de métal sur laquelle était gravé « Lucie » mais qui à présent avait également quelques lettres supplémentaires grossièrement rédigées à l'aide marqueur rouge ; elles formaient deux mots sonnant comme une lugubre invitation : Il était écrit « château d'eau ». Johnny lut et relut encore. Son corps tout entier lui sembla se contracter sous la colère, ses oreilles bourdonnaient salement, le satané carillon résonnait encore, assourdi par son propre sang se précipitant à toute allure dans chaque partie de son être, tandis que des frissons glacés le parcouraient.

*Ils avaient osé ! Ces trois salopards avaient osé dérober Lucie afin de l'obliger à monter jusqu'à leur repaire !*

La rage lui scia tout d'abord la respiration, il s'astreignit à respirer plus doucement, expulsa d'un coup l'air vicié de ses poumons, et tenta de réfléchir.

Ces trois imbéciles devaient déjà être là-bas, à l'attendre, à rigoler comme des ânes du bon tour qu'il lui jouait.

Johnny, soudain déterminé, tourna la poignée d’accélération du Rutilant et roula rapidement jusqu'au rond-point de la station-essence. Lorsqu'il s'engagea dessus, à une vitesse plus modérée, il aperçut le ballon de Lucie, au milieu du petit terre-plein central ; Son petit ballon de rugby. Sa rage redoubla ; il joua à fond sur la manette des gaz, sortit du carrefour trop vite, un instant déstabilisé mais le Rutilant se redressa, et attaqua la cote en pétaradant et fumant comme un dragon. Un petit, mais une monture redoutable quand même, se dit le garçon.

À l'entrée du chemin de terre, en haut de la cote, Denis, Franck et José, un peu gris, une bière à la main, observait la scène. Lucie aboyait, prisonnière d'une antique cage à renard, au fond du buggy, garé juste à côté des trois idiots, face à la route.

– Y va crever sa bécane avant de finir la cote ! Beugla Denis, une canette de bière à la main, tapant l'autre sur sa cuisse.

– L'a pas peur, j'vois pas d'outils pour nous décaniller, quoique t'façon, avec ses bras de poulet, y casserait pas grand-chose ! Ajouta José riant de toutes ses dents de fouine.

– Y va pisser dans son froc en arrivant le môme ! Glapit Franck en crachant par terre, observant le Rutilant qui attaquait le replat, à peine à mi-distance, une fumée très bleue sortant à présent du pot d'échappement.

José, lapa le fond de sa canette, la jeta sur l'asphalte, et fit un geste vers leur véhicule, se penchant sans souplesse dans l'habitacle pour saisir une autre bière, entre les sièges avant. Le pack éventré était coincé entre les sièges, aussi effectua-t-il quelques mouvements rageurs de va-et-vient jusqu'à venir à bout de l'emballage cartonné et sortir triomphalement une bouteille. L'esprit embrumé par l'alcool, il ne se rendit pas compte qu'il avait désengagé le frein à main.

Johnny à présent, discernait trois silhouettes haut devant lui, et fonçait comme le vent sur la route, la tête dans le guidon, fulminant contre lui-même et contre les sales types qui se gaussaient en buvant comme des outres.

J'aurais dû régler le problème avant, être courageux, leur tenir tête une bonne fois pour toutes, ne pas les laisser s'en prendre à un animal, j'aurais dû…

Le petit buggy libéré de toute entrave, subit la loi de la gravité la plus élémentaire, et commença à s'ébranler imperceptiblement. La pente était très forte, et en quelques secondes, l'engin s'éloignait déjà, dévalant la D120, droit en direction de la station-service. Le Rutilant lui montait, arrachant chaque mètre au risque de péter son vieux moteur deux-temps, rouscaillant autant, voire bien plus que Johnny, accaparés tous deux par cette rage, cette furieuse volonté d'en découdre, qui les faisait grimper cette cote, grimper, et grimper encore. Incroyablement, le buggy ne déviait quasiment pas et restait sur la route, sa vitesse s’accélérait encore, Lucie glapissait à l'arrière, Johnny l'entendait à présent, la tête rentrée dans les épaules, pour être le plus aérodynamique possible. Il n'avait tout d'abord eu qu'une vue partielle du flanc de colline, à cause du replat, mais à présent, il regardait incrédule le bolide se diriger droit sur lui, à une allure vertigineuse, une pente à vingt pour cent, imaginez-vous, sa main paralysée sur la poignée d'accélération, qu'il n'arrivait pas à lâcher et encore moins à appliquer une décélération.

Les Braconi et José s'époumonaient maintenant, paniqués, gueulant des mots qui filaient et disparaissaient dans le vent comme le buggy filait et fonçait droit sur Johnny. Le gamin tenta de braquer le guidon, mais rien ne se passa, le Rutilant crapahutait toujours, attaquant le deuxième et dernier tronçon de la cote, tandis que le buggy, face à lui, allait incessamment le percuter. José hurla. Sa bière à peine entamée se fracassa sur le macadam. Denis avait entrepris de descendre la cote, dans l'espoir vain de rejoindre son véhicule fou, mais il se prit bientôt les pieds dans sa propre course, et s'étala sur l'asphalte, évitant de justesse de s'y briser le crâne. Son frère opta pour les cris, qu'il poussait frénétiquement sans discontinuer, y ajoutant, au gré de l'inspiration, et en regard des événements tragiques se déroulant sous ses yeux, un « braque, bordel ! » « freine, merde ! Freine » autant de suggestions que le pauvre Johnny, paralysé, ne pouvait ni entendre ni mettre en application.

Le buggy tressauta en pleine course sur une pierre, cahota, rebondit, éjecta le golden retriever de sa cage, de même que le pack de bière et autres joyeusetés, qui éclatèrent sur la chaussée. La cage céda en touchant le trottoir herbu, et même si elle hurla en atteignant le sol, la chienne eut à peine quelques contusions, au final. Johnny, lui, prit la calandre du buggy en plein torse, sa tête heurta le capot, dû au fait que, en moins d'un battement de cil, sa colonne vertébrale s'apparentait maintenant à de la pâte à modeler, le reste de son corps s'enroula autour du bas de caisse de la voiture et décolla de la selle de cuir de son cyclomoteur ; son cœur cessa de battre lorsque le buggy, après avoir semblé voler dans les airs, retomba lourdement sur le toit, une partie de Johnny disparaissant totalement dans l'amas de ferraille. Le gamin n'avait même pas poussé un cri. Le Rutilant, imperturbable, comme mue par sa propre volonté poursuivit sa course encore un instant, il avait presque atteint le haut de la cote, puis il partit en zigzaguant dans les vignes proches, et se retrouva piégé entre deux rangs de pinot noir, finissant sa course prisonnier des fils de fer qui maintenaient les cépages, comme une grasse araignée dans sa toile, râlant, râlant encore, refusant de mourir.



Épilogue

Le solex pendait à son crochet, les pneus crevés. Quelques éraflures sur le réservoir striaient à présent la belle gravure, qu'on devinait pourtant sans peine « Le Rutilant » lisait-on. Un peu d'huile gouttait sur le sol.

Il soupira mais ne s'attarda pas sur le seuil du garage, et soulevant légèrement sa robe, grimpa de son pas puissant le raide escalier menant à l'habitation.

– Vous passerez après l'office n'est-ce pas ? S'inquiéta l'abbé Bresson, pressant l'épaule de Mr Brûlefer dès qu'il l’aperçut.

– Je partirai directement, ne m'en veuillez pas, j'ai déjà prévenu Jacques, répondit dans un souffle l'homme dont la mine cireuse en disait long sur les derniers jours qui venaient de s'écouler à Villevieille.

– Je comprends, se résigna le prêtre, s'éloignant déjà dans sa longue chasuble violette de cérémonie où s’enchâssait une étole ornée de dorures. « Donnez-nous de vos nouvelles Bruno, s'il vous plaît » dit-il en guise d'adieu.

L'homme, effondré sur un carton de vêtements d'enfant ne répondit pas, et le curé Bresson, qui pourtant, n'avait pas sa langue dans sa poche, poursuivit sa reculade, lorsqu'il aperçut sur le lit de Johnny, son petit livre à la couverture de cuir tannée. Il ne put s'empêcher,

– Il l'avait presque terminé !

– Hein quoi ? Questionna fiévreusement Mr Brûlefer, sans se douter ce à quoi le curé faisait allusion.

– Mon petit ouvrage sur Villevieille !

Bruno releva la tête et chercha ce dont parlait l'homme de dieu. Il saisit le recueil, l'ouvrit à l'emplacement du marque-page et lu à haute voix :

– Villevieille, ancienne nécropole de l'âge de bronze. À l'occasion du creusement du château d'eau, il fut découvert les vestiges de ce qui pourrait avoir été une sorte de temple, au vu des nombreuses sépultures et mobilier, céramiques, mis à jour tout autour du site. Mais ensuite, le mystère s'épaissit car, lors de la construction du château, au pied de la vallée, de petits crânes humains, correspondant à ceux d'enfants, furent mis au jour, tous enfoncés et affreusement mutilés. Certains fantaisistes suggèrent qu'il s’agirait là d'un rite sacrificiel humain voué à une divinité maléfique. Les deux sites en flanc de colline semblent donc liés par un passé bien mystérieux mais aucune hypothèse certaine n'a émergé à ce jour de ces quelques découvertes effectuées, d'autant que par manque de financement, elles n'ont pu être poursuivies… »

Bruno laissa tomber le bras tenant le petit livre le long de son corps, les larmes coulaient à présent en un flot continu, il ne les retenait plus. Il lança le bouquin avec force en direction de l'abbé ;

– Allez au diable !
