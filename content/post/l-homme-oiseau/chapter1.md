Tout s'est terminé un soir, ou plus exactement, une nuit.

Il faisait noir comme la gueule d'un four dans cette chambre où logeaient trois gamins, deux frères et une sœur. Il faisait froid aussi. Le bébé dans la chambre d'à coté pleurait. La gamine, d'une dizaine d'année, se cachait sous la grande couverture avec, dans l'oreille, un petit amplificateur. La mini radio distillait de la musique new-wave à bas bruit tandis que la seconde oreille de l'auditrice restait attentive à son environnement. Il pouvait toujours se lever et la surprendre. Il était malin. Il était vicieux. Il fallait toujours rester sur ses gardes.

The Police entama son sempiternel « message in the bottle ». Maddie ferma les yeux, se remémorant tous les messages à la mer qu'elle avait pu lancer sans succès durant ces dernières années. Enfin, sans succès jusqu'à aujourd'hui, où tout avait commencé.

La journée avait été mémorable. Maman avait annoncé à Maddie et son jeune frère Simon qu'ils étaient invités au restaurant ! Événement pour le moins extraordinaire, au regard du contexte socio-économique qui laminait la famille depuis quelque temps. Le père, patron d'une petite PME avait fuit avec la caisse, laissant le foyer s'acquitter des dettes que Madame se révélait dans l'incapacité d'assumer. Elle avait repris pour l'occasion un boulot dans une usine de poterie, mais le compte en banque n'en était pas pour autant requinqué, et c'est grâce aux grands-parents qu'eux trois survivaient actuellement dans une maison de location décrépie. Le seul point positif de tout ce tralala ; le jardin était immense et les enfants, souvent livrés à eux-même, baguenaudaient allègrement dans les champs, vélos à la main À part ça, Maman pleurait tout le temps, les grands-parents inquiets montaient la garde et faisaient la chasse aux huissiers. Papa les avait laissé dans une belle mouise. Mais il ne fallait pas parler de Papa. Ça faisait pleurer de plus belle Maman et grand-père se mettait en pétard.

Enfin donc, le restaurant c'était très inattendu. Le petit frère avait revêtu son plus beau costume, bleu ciel, pattes d'eph, et la grande sœur, de deux ans l'aînée, portait pour l'occasion une robe marine à grosses marguerites qui ne la flattait pas vraiment, étant donné qu'elle était déjà un peu ronde pour son âge. Grand-mère disait toujours, mon poulet, il vaut mieux faire envie que pitié, et sûr, elle ne faisait pas pitié du tout.

Maman conduisit la R5 avec les gosses dedans au resto du bourg, à une vingtaine de kilomètres de la maison ; toute une expédition. Elle était toute jolie, Maman. De grande anneaux d'or pendaient de ses oreilles, alors que tombaient sur ses épaules étroites un grand chemisier à fleurs, et que, summum de l'élégance, gainant ses jambes élancées, un pantalon en velours côtelé beige marquait sa taille fine. De grandes bottes à talons carrés complétaient l'ensemble, lui donnant une allure cavalière. Ce qu'elle était d'ailleurs avant, du temps de Papa riche. Des cheveux châtain clair encadraient un visage fin aux yeux verts. Une bouche petite, rosée, aimable, ne gâchait rien. Ses bracelets, un semainier en or massif qui avait échappé (et échapperait toujours) aux débiteurs de toutes sortes, cliquetaient joyeusement à son poignet tandis que les enfants coincés à l'arrière faisaient les fous. Le poste déversait une musique entraînante, et chacun reprenait le refrain à tue-tête.

Grande fut la déception à l'arrivée. Devant la devanture du resto, attendait Denis, ce type entrevue plusieurs fois à la maison. Un homme d'une petite trentaine d'années, plus jeune que maman, plus moche aussi. Un crâne déjà passablement dégarni, le cheveu rare et noir, des petits yeux du même acabit, des billes de chat en colère, au dessus d'une moustache fournie, totalement hors époque, comme son look. Il accueillit la petite troupe avec une esquisse de sourire qui semblait tirer douloureusement sur le bas de sa figure. Maman l'embrassa fougueusement, devant tout le monde, c'était dégoûtant. Simon tendit sa menotte, toujours bien élevé, tandis que Maddie restait la bouche ouverte, sans aucun mot pour cet inconnu qu'elle considérait déjà comme un ennemi potentiel (la suite lui donnerait raison).

Le repas s'étira en longueur, d'autant que pour des bouts de choux de huit et six ans rester assis plus de dix minutes relèvait de la gageure À la fin du repas, alors qu'il ne leur avait pas décroché un mot, lançant juste un regard noir lorsque la gamine avait raté, tant s'en faut, la découpe de sa cuisse de poulet (qu'elle était allée repêcher laborieusement par terre), il s'adressa à eux en révélant des dents blanches parfaitement alignées, très carnivores, songea Maddie mettant en parallèle les grands crocs blancs de sa chienne berger allemand.

— Les enfants, les prit-il à témoin, sortant de sa poche une petite boite qu'il tendait à notre mère, lui coulant un regard tendre, « aujourd'hui votre vie va prendre un nouveau tournant — en fait ce fut un virage à cent-quatre-vingt degrés pour être plus près de la vérité — je viens de demander à votre maman de l'épouser »

Épouser… ça ne disait pas grand-chose aux enfants qui écarquillaient les yeux, se demandant ce que ce type sorti de nulle part attendait d'eux. Mais quand la bague surgit du paquet, l'idée se fit jour qu'en effet, tout allait changer pour eux.

Maman sauta dans ses bras, heureuse semblait-il, aussi les gamins sourirent et embrassèrent leur mère dans un même élan d'affection.

En sortant, Simon admira la moto de Denis, et le lui dit. Le fayot, songea Maddie en pulvérisant son petit frère d'un regard. Mais Simon n'avait pas eu de père à vrai dire, car celui-ci n'avait jamais pu l'encadrer, aller savoir pourquoi. Lors de ses rares incursions à la maison, il hurlait sur Maman, rapportait des cadeaux à sa fille mais ne semblait jamais s'apercevoir qu'il avait un fils. Il l'ignorait d'une manière si évidente que chaque apparition de ce père fantasque et imprévisible engendrait un véritable malaise pour l'aînée, déstabilisée et culpabilisée de ce trop plein d'attention soudaine et furtive, puis l'homme disparaissait de nouveau rapidement, de longs mois durant, sans donner de nouvelles.

Peut-être cet homme-ci ferait-il un succédané de père acceptable pour le petit ?

Évidemment, ce Denis ne tarda guère à investir la maison avec son bardas et dans ce foutras, il y avait un petit garçon, Joachim, âgé de quatre ans. Il arriva un après-midi, après l'usine, avec son père, eh oui… il travaillait à l'usine, où il avait rencontré et sympathisé avec Maman, un beau brin de fille cette Marianne, une vraie pitié que son mari l'ait abandonné avec deux gosses, se gorgeaient les gens du village ! Les grands-parents tirèrent la mine, leur dernier gendre n'était pas recommandable et celui-ci, un immigré, ne leur arrachait pas des larmes de bonheur. Avec un gamin dans les bagages en plus ! Comme si deux ne suffisaient pas !

Va sans dire que le visage de grand-mère s'allongerait encore lorsque leur petite fille chérie annoncerait le troisième, quelques mois plus tard, mais laissons cela pour l'instant.

Au retour du restaurant donc, le fiancé resta l'après-midi avec Maman, on envoya les gamins dehors, jouer sur leur tas de sable avec le petit voisin, Renaud, de quelques années plus vieux, mais éternel compagnon de jeu.

Le soir venu, tout le monde resta dîner. Alors que chacun jacassait à table, sauf le dit Joachim, déjà au parfum des lubies de son paternel, Denis explosa d'un coup, exigeant le silence pour manger dans le calme. Ses yeux de fouine lançaient des éclairs et ses mains gesticulaient dangereusement, trop grandes, trop rapides. Lorsque Maddie ouvrit la bouche une fois de trop, la claque s'abattit sans avertissement. Une claque bruyante et douloureuse. Le résultat fut immédiatement perceptible ; la gamine se tut, puis pleura bruyamment tandis que Maman protestait avant de se prendre la même punition. Il va sans dire que le repas se déroula ensuite dans un silence de plomb, ainsi que tous ceux qui suivirent.

En définitive, le nouveau mari de maman ne supportait ni le bruit, ni les voix des enfants, ni leurs jeux, ni aucun tracas qui font les jours des familles nombreuses.

Ainsi, les semonces pleuvaient aussi facilement que la rosée le matin. L'homme criait peu, mais il avait une manière bien à lui de faire la tête, de pencher son cou maigre et glabre, de fixer le ou les — coupables — qu’aussitôt chacun tremblait et pissait dans son froc. La corvée du repas devint une souffrance telle que Maddie s’arrangeait souvent pour être privée de dîner et renvoyée dans sa chambre.

C'est alors qu'arriva ce jour où tout devint différent pour Maddie, à vrai dire, non pas différent, mais extraordinaire.

Grand-père était mort depuis quelques mois déjà, et dans un élan de générosité ou plus probablement fuyant ce que ses yeux voyaient et que son cœur ne supportait pas, grand-mère fit don de sa maison à Marianne. Denis se réjouit de l'événement, grand-mère partit habiter chez les sœurs du silence, à coté du presbytère où les nonnes tenaient une « maison des vieux » et elle y loua une chambre, emporttant sa commode et son armoire.
Le caractère du beau-père prit encore une mesure ou deux du mauvais côté de la balance. Il devint irascible alors qu'il entamait les travaux dans la nouvelle maison : les punitions redoublèrent, remontrances et humiliations allèrent bon train, chacun recevait sa dose quotidienne.

Vint ce soir où Maddie entreprit de demander l'autorisation de passer le mercredi après-midi chez son amie Catherine, qui habitait dans la cour voisine, pour fêter son anniversaire. Elle émit également la malencontreuse idée d'un petit cadeau pour son amie, un tout petit. Mal lui en prit ! Elle termina par terre, son assiette de soupe sur les genoux, hurlant parce que c'était chaud et que le liquide lui brûlait les cuisses. Sa mère la renvoya dans sa chambre après lui avoir passé de l'eau froide et recommandé — d'arrêter de faire des histoires —. Ce soir-là, consignée dans sa chambre, seule dans le noir, elle ne dormit pas. Les petits vinrent se coucher, puis les parents, dans la chambre à coté. Elle ne dormait toujours pas. Sa peau la cuisait, ses larmes ne s'arrêtaient pas. Elle maudissait ce sale type, maudissait sa propre faiblesse. Un moment, dans les volets fermés, à la lueur de la nuit du dehors, où la lune tendait ses pâles et doux rayons sur le monde, elle crut voir une forme près de son lit. Elle scruta la pénombre, interdite ; s'y trouvait effectivement quelque chose qui la fixait. Deux yeux brillaient. Elle n'eut pas peur, son cœur battait fort, mais elle s'efforça de regarder la créature qui se tenait devant elle.

Et d'un coup, elle lui apparut toute entière, comme si un feu l'éclairait de l'intérieur. C'était un — homme-oiseau —. On ne pouvait être plus près de la vérité que l'appeler ainsi ; sa tête allongée, ressemblait à celle d'un perroquet, mais le plumage qui recouvrait sa tête et son cou semblait plus soyeux que de la soie, le chatoiement des couleurs, des bleus, des verts, des jaunes orangés se reflétait dans d'immenses yeux à l'ovalité aimable. Son bec était pourvu de petites dents, lui donnant une expression à la fois humaine et sauvage, ses oreilles étaient invisibles. De longs bras, d'où pendaient de grandes rémiges multicolores sortaient des manches d'une large tunique noire brillante. De là où elle se tenait, la gamine ne distinguait pas ses pieds ou peut-être étaient-ce des pattes. La créature étendit ses ailes, dont les trois doigts terminaux, de couleur argentée ressemblaient à d'épaisses griffes, des doigts de dinosaure ne put s'empêcher de penser Maddie. L'homme-oiseau rit de ses belles dents, comme s'il avait saisit la pensée de l'enfant et cela lui donna un air affable qui mit la gamine en confiance. Leurs regards se croisèrent et cessant de rire, il se présenta, n'esquissant pas un geste. Sa bouche ne s'ouvrit même pas, pourtant Maddie l'entendit clairement.

— Bonjour petite. Je m'appelle Mahaar. Tu m'as convoqué et me voilà auprès de toi aujourd'hui et pour le temps que tu souhaiteras m'avoir à tes côtés. Que puis-je faire pour toi ? 

L’enfant se frotta les yeux car elle était sujette aux rêveries et d'ailleurs nombre de reproches de son beau-père avait pour thème ce penchant. Mais la créature se tenait toujours là, même après avoir frotté ses yeux plus que de raison. Maddie était une petite fille curieuse et bavarde, elle ne put tenir sa langue plus longtemps.

— Tu es comme Aladin ? Tu exhausses les vœux ? Elle se reprocha aussitôt d'avoir parler tout haut, les parents juste à côté dans l'autre chambre allait l'entendre. Elle ferma brusquement la bouche, retenant son souffle, écoutant la nuit.

— Ne t'inquiète pas de tes parents, petite, ils dorment d'un sommeil épais et sans songe. Pour répondre à ta question, non, je ne suis pas comme cet Aladin. 

– Dommage, reprit Maddie, rassurée par le fait que personne ne pouvait les entendre. Tu aurais tué mon beau-père.

– Ah ! L'homme-oiseau secoua sa belle tête emplumée de droite à gauche, d'un air contrit. « Je ne peux, ni ne dois jamais faire aucun mal quelque soit la créature vivante »

L'enfant sembla réfléchir à cet énoncé.

— Es-tu un magicien qui fait des tours ? Michel possède une boite de — prestatidigation — elle hésita sur la prononciation, « il est très fort. Il cache des jetons sous des gobelets et ils disparaissent »

L'homme-oiseau sourit encore, sans que sa bouche-bec ne change d'apparence, et Maddie en ressentit de l'apaisement. Il expliqua d'une voix chantante sans être pour autant criarde ou jacassante comme peuvent l'être celle des oiseaux :

— Je me tiendrai à tes côtés désormais, tu ne seras plus seule et toi uniquement pourras me voir et converser avec moi. 

L'enfant garda le silence. Ce Mahaar était-il comme le compagnon imaginaire de Valérie ? Sa camarade de classe discutait sans cesse avec Jérôme, son soit-disant ami, que personne n'avait jamais vu. Certains la traitait de fofolle.

La créature rit de nouveau, et les notes de ce rire cascadèrent telles l'eau apaisante d'une source pure aux oreilles et jusque dans le cœur de Maddie.

— Non petite, je ne suis pas Jérôme, je suis bien réel. Sur cette affirmation, il s’approcha et la gamine s'aperçut que sa tunique s'arrêtait aux genoux et découvrait des pattes ressemblant fort à celles d'un flamant rose. Il s'assit sur le bord du lit et prit sa petite main entre ses griffes. Elles étaient chaudes et douces. Simone s'esclaffa.

— Je voudrais que le temps s'arrête, souhaita-t-elle, plus sérieusement que son âge ne le permettait.

– Cependant, demain viendra, petite, chantonna Mahaar en lui passant son bras plumetis sur le visage.

Le lendemain, Simone se leva et se rendit à l'école, un vague souvenir de sa drôle de nuit en tête. Elle souffrit moins de ses brûlures car en rentrant cette après-midi là, sa surprise fut totale de voir grand-mère avec ses valises, sa commode et son armoire que Denis finissait d'installer dans une des chambres vacantes.

Grand-mère passa une bonne couche de pommade sur la peau cramoisie, agrémentée d'un commentaire comme la vieille femme en avait le secret,

— Faite maison, hein, mon poulet, pas ces trucs qu'on vent chez les pharmaciens. C'est ton grand-oncle Abel, l'herboriste, qui m'a donné la recette. Tu vas voir mon poulet, dans deux jours, il n'y paraîtra plus, je te le garantis. 

Les raisons du retour de la vieille dame restèrent obscures à son arrière petite-fille. Peut-être son beau-père nourrissait-il quelques remords, ce qui paraissait peu crédible. Maman l'avait peut-être supplié une fois encore et lassé, il avait cédé, ou plus probablement, l'idée avait germé que grand-mère pourrait s'occuper des gamins et qu'ainsi maman serait en mesure de s'occuper de finir les papiers peints et peintures de la nouvelle maison.

Dans tous les cas, le sort des enfants fut quelque peu adoucit par la présence de la vénérable occupante. Grand-mère était aussi frêle que Denis était grand et fort, sa voix aussi fragile que ses os pourtant, leur tourmenteur osait plus rarement passer les bornes quand elle était présente. Bien sûr, il se vengeait souvent dès qu'elle avait le dos tourné.

Mahaar officiait régulièrement au pied du lit de Simone, quand la journée avait apporté un lot de souffrances supérieur à l'ordinaire. Il contait merveilleusement et ses histoires, ses mots à la musicalité surnaturelle, apaisaient toujours l'enfant. Cependant, il ne pouvait endiguer la croissance de cette haine viscérale émanant d'elle, nourrit par la méchanceté du beau-père, ni cette colère tournée vers elle-même qui l’abîmait chaque jour un peu plus. Alors elle se réfugiait dans les livres, la solitude, les bouderies.

Maman annonça enfin sa grossesse et chacun accueillit la nouvelle dans un silence mortifié, sauf Denis, autorisant même les enfants à sortir de table avant la fin du journal télévisé et cela même s'ils n'avaient pas fini leur assiette… Un répit qui ne dura guère. Avec cet homme, se profilait toujours un nouvel ennui.

Parmi d'autres, un problème se posait depuis qu'ils avaient emménagé dans la nouvelle maison : Denis avait une chienne, Bambou, un molosse stupide qui détestait tous les chiens et Maman possédait deux chiennes, deux bergers allemands, dont un partageait la vie de maddie depuis ses six mois. C'était Papa qui avait fait cadeaux des bêtes et de ce fait, du jour où il entra dans leur famille, Denis détesta ces deux animaux plus que tout dans la maisonnée. Il les frappait dès que possible, ne leur autorisait pas l'accès à la maison, les nourrissant au minimum, action heureusement compensée par le fait que les enfants et Maman donnaient à manger aux deux bêtes en cachette. Mais leur présence lui était insupportable et Maddie souffrait beaucoup des mauvais traitements qu'infligeait l'homme à ses bêtes chéries. La seconde, notamment, très jeune, faisait quelques bêtises et Denis se mettait dans des rages folles, maudissant ces — vacheries qui coûtaient cher et ne servaient à rien —. Un temps passa, des rats avaient investit la grange derrière la maison. Denis pesta que les chiennes n'en venaient pas à bout. Il finit pas trouver une solution radicale qui le débarrassa, à n'en point douter, des rats et des chiennes : Le poison.

Un matin, les deux bergers ne vinrent pas à l'appel de leur petite maîtresse, dans la cour, et Maddie se dirigea le pas lourd vers la grange. Ne les trouvant pas, elle partit à leur recherche — tant pis pour l'école —, redoutant le dénouement de cette journée, le cœur au fond de son estomac.

Maman se joignit aux investigations. Les deux bêtes furent retrouvées bientôt, langue pendante, leur beau corps noir et feu roide, froid, leurs yeux vitreux et morts.

On ne vit guère Denis, ce jour-là, et pas un mot ne jaillit de ses lèvres pour commenter l’événement.

Maman tira les bêtes au fond du jardin, prit une pelle, en tendit une plus petite à sa fille puis elles creusèrent ensemble la terre molle qui sentait bon les feuilles. Elles basculèrent les corps au fond du trou dans un bruit mat et définitif. Dès lors, Maddie se sentit ankylosée, tétanisée, terriblement ailleurs. Ses chiennes, c'était son univers, sa joie, ses confidentes. Leur perte lui laissait l'âme vide, âpre, aigrie. Seule une haine grandissante faisait battre son cœur.

Au cours de la nuit qui suivit ce tragique événement, Mahaar visita la petite fille. Quand il apparut, des larmes d'argent coulaient de ses yeux, et l'enfant, qui s'était interdit de pleurer devant son affreux beau-père, tortionnaire d'enfants, tueur de chiens, se laissa aller à sa peine.

L'homme-oiseau laissa le flot se tarir de lui-même. Il la berça dans ses longs bras de plumes, et lui conta comment les être vivants, après une courte vie terrestre, retournaient au sein de la terre, mère de tous, pour un nouveau cycle.

— La vie est précieuse et a de multiples aspects, petite. Souviens-toi de tes amies et elles se souviendront de toi, chantonna-t-il mélodieusement. Elle s'endormit en rêvant : elle courait dans le pré derrière la maison avec ses chiennes, et souvent, oui, bien souvent par la suite, ce rêve réconfortant et précieux revint l'apaiser.

Dès le jour suivant, jour des grandes vacances, ce fut le branle-bas de combat dans la maison : A la surprise générale, Papa accueillait ses enfants en Bretagne, un mois à la mer. Denis tempêtait et Maman protégeait son ventre arrondi. Grand-mère n'en fit pas moins les valises, ajoutant subrepticement quelques billets de dix francs dans les porte-monnaies respectifs, — pour rapporter un petit souvenir —. Papa se pointa comme une fleur, accueillit par la carabine d'un beau-père au comble de l'énervement, dans un concert de vociférations. Il n'y eut cependant ni mort, ni blessé, sauf d'amour-propre, Papa ne pouvant pas rentrer chercher ses gosses, malgré son beau costume, ses lunettes de soleil d’aviateur et ses pompes italiennes. Après de longues négociations, grand-mère fourra les gamins dans la grosse berline, prodiguant d'ultimes recommandations tandis que Denis fulminait, au coin de la porte d'entrée.

Ce fut un moment heureux ; la Bretagne offrit son sable chaud, ses roches roses aux explorations des enfants et les joies de dormir dans la caravane familiale. Au milieu d'autres gosses en vacances, Maddie s'accorda un temps de réconciliation avec elle-même.

Le mois d’août fut plus douloureux, les garçons trimaient dur dans les potagers familiaux, se levant aux aurores, bousculés et agonis tout le long du jour. Maddie partageait son temps entre grand-mère et Maman, aidant peu aux taches ménagères, bavardant de sa vie future, qu'elle envisageait sur une île déserte, entourée de bêtes qu'elle soignerait, envisageant une carrière de vétérinaire. Denis, inchangé, la rabaissait sans cesse, « fainéante », « bonne à rien », « idiote », se lisait dans chacun des regards qu'il lui adressait. Maman se taisait, elle attaquait le dernier mois de sa grossesse. Un après-midi, partie de rien, comme souvent, une dispute entre elle et son mari bascula carrément dans le mélodramatique. Grand-mère s'interposa mais fut repoussée et s'en alla valdinguer sans grâce contre le grand meuble de cuisine, sonnée. Maman gisait par terre, recroquevillée sur son gros ventre, réduite à geindre comme un animal blessé tandis que l'homme au-dessus d'elle faisait pleuvoir une averse d'insultes. Maddie assistait à la scène, s'agrippant à sa mère, tandis que ses frères, cachés sous le vieux bureau de grand-père, ne mouftaient pas, leurs grands yeux ouverts comme un cri muet.

Le capharnaüm cessa aussi vite qu'il était apparu. Denis sorti en claquant la porte, se tenant la tête comme si c'était lui qui avait mal, tandis que grand-mère relevait sa petite-fille et son arrière-petite-fille, les arrosant de ses larmes de rage impuissante. Une heure plus tard, l'ambulance était là, maman était entrée en douleurs, elle saignait, elle sanglotait et il fallut quasiment de force l'embarquer sur la civière pour la conduire à la maternité. Grand-mère aida Maddie à nettoyer le sang, qui s'étalait obscène sur le carrelage de la cuisine. Elles n'échangèrent pas un mot en rangeant les meubles bousculés, la vaisselle cassée et dans un silence abyssal, la petite cassa la cafetière neuve que son beau-père avait offert à Maman une semaine avant, pour son anniversaire.

— La goutte de trop — eh bien, cette stupide cafetière fut celle-là. Maddie s'enfuit dans sa chambre en pleurant et s'enferma là jusqu'au soir, finissant par ouvrir quand même, sous l'avalanche de prières de grand-mère, craignant le retour de Denis et un regain de furie destructrice.

La table était mise et le repas attendait d'être servi lorsqu'il apparut enfin, blanc, l’œil plus noir que le charbon. Un tic tirant le coin de sa bouche vers le bas le rendait terrible à regarder. Tout le monde garda le nez dans son assiette. Maddie ne mangea pas, ce qui lui valut d'être renvoyée, Joachim renversa une cuillerée de soupe sur la table ce qui lui attira les foudres de son père. Simon avait perdu sa langue et machinalement racla avec un bout de pain le fond de son assiette sous le regard éteint de grand-mère.

Suite à ces événements, au plus profond des ténèbres, Mahaar se matérialisa au pied du lit, sa tunique scintillante plus belle que la nuit étoilée. Mais cette fois, sa vue ne consola pas Maddie.

— Tu vas provoquer le destin, lui murmura l'homme-oiseau de sa voix flûtée.

– Je pars, répondit la gamine, assise au fond de son lit tandis que ses frères ronflaient doucement dans la chambre.

Mahaar hocha la tête,

— Tu as trouvé de la force en toi, petite. 

– Je suis lâche, aboya Simone. Elle se tourna vers le mur, et à son réveil prépara sa fuite.

Et tout se termine cette nuit.

Il fait noir comme la gueule d'un four dans cette chambre où logent trois gamins, deux frères et une sœur. Il fait froid aussi. Le bébé dans la chambre d'à coté pleure. La gamine, d'une dizaine d'année, se cache sous la grande couverture avec, dans l'oreille, un petit amplificateur. La mini radio distille de la musique new-wave à bas bruit tandis que la seconde oreille de l'auditrice reste attentive à son environnement. Il peut toujours se lever et la surprendre. Il est malin. Il est vicieux. Il faut toujours rester sur ses gardes.

Son petit sac à dos git sous le lit : à l’intérieur, une petite torche électrique, ses cahiers de poésie, un crayon bille, quelques gâteaux volés, un pull et un paquet de cigarettes avec un briquet, acheté à la sortie de l'école, ce jour-là.

Elle écoute l'obscurité, le hululement de la chouette, le bruissement des feuilles dans la tourmente de l'automne. L'homme-oiseau n'apparait pas, mais cela ne change rien au plan de Maddie. Enfin, elle ose se lever et passer les vêtements chauds qu'elle a préparé à son coucher. Elle tient ses chaussures à la main, se dirige vers la porte de la cuisine, sans faire grincer le plancher, sans faire couiner les portes. Elle pousse la grande grille dehors avec peine mais celle-ci ne la trahit pas et tourne sans effort sur ses gonds.La gamine se retrouve sous le dôme des étoiles, la lune pour guider ses pas. Elle ne se retourne pas, s'éloigne rapidement, sortant bientôt du village, elle grimpe laborieusement la grande route pentue qui trace une ligne droite vers ailleurs. Elle fait bientôt une pause sur une borne kilométrique, ouvre son sac et saisit maladroitement cigarettes et briquet. Elle allume la tige fébrilement, tousse une fois, puis deux, un mauvais goût dans la bouche.

— Tu prends là une bien mauvaise habitude, commente l'homme-oiseau, se révélant dans un éclat de lune. Il sourit cependant, ce qui rend son affirmation moins tragique. Maddie ne sursaute pas, au fond, elle savait qu'il serait là.

— Je choisis mon destin, dit-elle malicieusement soufflant la fumée en parlant, prête à reprendre la route.

– Je t'accompagne, petite, si tel est ton choix.

Elle lève la tête : En haut de la colline, sur l'accotement, plusieurs ombres, hommes-oiseaux affairés, s'agitent, silencieux tandis que leurs ailes bruissent dans le vent.

— Qui sont-ils ? Demande Maddie, bien qu'elle connaisse en partie la réponse.

– Ils sont mon peuple, petite, ils favorisent le bien quand ils le peuvent, ils accompagnent ceux qui souffrent quand ils le doivent.

– Vous avez beaucoup de travail, soupire la gamine, écrasant sa cigarette d'un geste qui n'a décidément plus rien d'enfantin.

– Allons-y, l'encourage Mahaar.
