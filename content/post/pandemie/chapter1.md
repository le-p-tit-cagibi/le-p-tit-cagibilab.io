An 2043.

[Quarante-troisième jour de l'ultime confinement].

Locaux de la clinique psychiatrique pénitentiaire « Les Eaux Vives », quelque part dans une petite ville de l'est de la province France, dans un pays nommé Europe.

Les coups frappés à la porte principale se répercutèrent dans le bâtiment silencieux. La porte blindée encaissait sans broncher, vibrant à peine mais le sas de décontamination, tel une caisse de résonance, répercutait le bruit dans les couloirs, les halls, les cages d'escaliers, les salles d'études, les bureaux, les sanitaires, les chambres, et les salles de soins.

Le bâtiment Q1 secteur adolescent se situait près du vacarme. Il était le seul encore opérationnel. Alertés par le raffut, ses employés, au nombre de trois, se concertèrent du regard, incrédules, le dos tendu, les yeux écarquillés en une prière silencieuse.

Le docteur Clemens s'immobilisa, une fesse sur le bureau le plus proche, son index sur la bouche. Fadir, l'aide-soignant, habituellement rigolard, frotta son crâne chauve un instant et répondit par un clignement de ses yeux gris, sans faire aucun autre mouvement depuis le confortable fauteuil où il se trouvait assis. Enfin, debout, un peu en retrait, Gwladys, la jeune infirmière, les sourcils froncés, secouait sa longue queue de cheval auburn, hochant la tête d'un air exaspéré. De ses doigts longs et fins, elle dessina dans l'air un point d'interrogation à l'attention de ses deux collègues et loin de se transformer, elle aussi, en statue, s'avança de son pas chaloupé vers la porte d'entrée de la salle de soin.

Les deux hommes ne purent se contenir davantage.

« Noooon ! »

Gwladys haussa les épaules sans se retourner et poursuivit son chemin.

Ses bottines à talons plats résonnèrent à travers des couloirs colorés, bleu d'abord, puis vert, et enfin orange, chacun séparé par une porte sécurisée qui pour l'heure, se trouvaient ouvertes.

« Tu ne devrais pas t'aventurer plus loin » lui souffla le docteur Clemens, se faufilant à sa suite lorsqu'elle libéra sans un mot la double porte blindée qui séparait l'aile Q1 du hall d'accueil.

« Gwlagwla, déconne pas, c'est juste encore une fausse alerte ! » beugla Fadir.

La jeune femme stoppa net et se retourna enfin, si bien que l’aide-soignant faillit lui rentrer dedans.

« Une fausse alerte qui cogne à la porte ? Tu te fous de moi ? » Sa chevelure flamboyait à peine moins que sa colère. « J'en ai assez de vos conneries. Il y a quelqu’un à la porte alors comme des gens civilisés, on va voir qui vient nous rendre visite ! » et elle reprit sa marche plus décidée que jamais.

« Civilisés ? » Farid émit un hoquet grossier, imitant l'infirmière sous le regard réprobateur du médecin.

« Écoute, jusqu'ici, on a plutôt pas mal géré, alors… »

Gwladys lui coupa net la parole sans proférer un son ; soit le geste de sa main saisissant vivement un kit test, soit son regard noisette, d'ordinaire aimable, mais qui virait au noir charbon à cet instant précis avait été perçu comme une potentielle menace.

À la porte d'entré, les coups redoublèrent. Apparemment leur hôte s'impatientait À l'oreille, on pouvait supposer l'utilisation plutôt musclée d'un objet métallique — un marteau ou peut-être une clé à molette —, en tout cas, un outil qui produisait un boucan infernal à travers l'ensemble du bâtiment.

« On va voir ce qui ce passe. On décidera ensuite » dit-elle en reprenant un énième couloir, gris cette fois, après avoir laissé derrière elle le hall central envahit de prospectus, papiers gras, et journaux jonchant le sol. Une multitude d'affiches habillait des murs beiges déprimants. Certaines, à moitié arrachées, proclamaient :

— Lavez-vous les mains et portez des gants ! —
Suivi d'une petite icône figurant une paire de mains indécemment roses, un robinet avec l'eau coulant dans un fac-similé d'évier, un pain de savon jaune fluo et enfin, une paire de gants.

Plus loin, une pancarte, plus grande, affichait en gros caractères rouges :

— Il est interdit de retirer son masque, sa blouse, sa charlotte, ses surchaussons ou ses gants. Respectez les distanciations sociales. Évitez de parler et d’ouvrir la bouche en direction du personnel et des autres visiteurs. Utilisez votre smartphone et sa fonction audio pour muets. Rendez-vous directement dans le secteur de votre visite après avoir validé votre identité à l'accueil, ne circulez pas dans les couloirs sans votre pass sanitaire. En cas de non-respect, la gendarmerie vous prendra en charge et vous accompagnera directement à l'aire d'isolement correspondant à votre lieu de résidence pour une quarantaine de 40 jours. Merci de votre compréhension.


Les trois acolytes ne prêtèrent aucune attention à tout ce cirque, traversant les couloirs au pas de charge pour bientôt faire face à une lourde porte cintrée et blindée. En son milieu, une vitre quadruple épaisseur sécurit, grillagée serré, laissait entrapercevoir un cric de voiture qu'une main, noire et vigoureuse, abattait de manière répétée sur le carreau.

« Putain c'est quoi ça encore ? Manquait plus qu'une cinglée d’immigrée ! » s'exclama Fadir en lorgnant sur la peau ébène tenant l'outil.

« Pas le moment de sortir ton pamphlet anti-immigration, va plutôt chercher les combi et tout le cirque » lui intima le doc qui semblait émerger d'une longue hibernation. Sur le même ton de commandement, en frappant à son tour sur la vitre, pendant que Gwladys déposait le kit test sur la tablette à gauche de l'entrée, il s'adressa à leur assaillant.

« Eh vous là ! Arrêtez de vouloir tout casser ! Montrez-vous ! Je suis le docteur Clemens, j’exige que vous vous identifiiez ! »

Le silence se fit immédiatement. Un choc assourdi leur fit comprendre que dehors, on abandonnait provisoirement l'offensive. Un visage rond, en sueur et à bout de souffle se colla contre la vitre, révèlant de manière peu flatteuse les traits d'une femme. Elle prononça quelques mots que ni l’infirmière, ni son patron ne comprirent.

Ils observèrent l'inconnue sans broncher, tendant l'oreille.

À travers la lourde porte, les sons, déformés, leur parvenaient sans qu'un sens n'émerge. De l'autre côté de la porte, l'inconnue s'énervait, postillonnant contre la paroi de verre.

« C'est dégueulasse de brailler comme ça, putain, elle est en train de laver le carreau avec ses particules virales la garce » s'exclama Farid, de retour avec tout un fatras médical dans les bras, dont les protections anti-virus.

« Ta gueule, Farid, on entend rien ! » Le houspilla sa collègue en collant son visage contre le hublot.

La main de l'inconnue claqua un macaron argenté sur le carreau, Gwladys recula instinctivement.

« Oh merde ! Un flic ! » commenta Farid, souriant de toute ses dents. « Manquait que ça tient, elle va nous mettre un PV peut-être, les bagnoles stationnent depuis un bail sur le parking ! » Il rit de sa blague, hoqueta, vexé, en constatant son insuccès et repris,

« T'es sérieuse Gwlagwla ? Tu veux vraiment qu'on enfile ces machins là et qu'on lui ouvre le sas ? »

La jeune femme n'eut pas le temps de répondre, Clemens remonta ses lunettes sur son nez camus avec une petite grimace au coin inférieur gauche de la bouche, et parla de nouveau avec autorité, fait assez rare pour être mis sous le coup de l'émotion ou de l'adrénaline voire les deux. Il articula ses instructions comme s'il parlait à deux demeurés.

« On va ouvrir la porte à cette fliquette quand elle nous aura montré ses quenottes. Si c'est bon, on déverrouille — MAIS — on galope fermer le sas AVANT qu'elle entre. Elle fait le test. SI c'est négatif, on verra »

« On verra… » commença Fadir, moitié convaincu « je te rappelle Doc que là, dehors, Y a une saloperie de virus et que depuis un moment plus personne ne frappe au carreau… Et pourquoi qu'on lui ouvrirait à celle-là ? Alors qu'on a pas ouvert aux autres ? Tu te rappelles pas ? Le livreur, la postière… L’aide-soignant énumérait leurs précédents visiteurs en comptant laborieusement sur ses doigts.

Gwladys enserra sa grosse main noueuse dans la sienne. On aurait dit celle d'une enfant.

« Je me souviens très bien de ceux qui sont venus avant mec » dit-elle d'un ton éteint. « et Clemens aussi, mais ça fait quarante-trois jours qu'on est enfermé là-dedans, alors il est temps de savoir ce qui se passe dehors ! »

Le médecin était déjà en train de rédiger quelques mots sur son ordonnancier. Il passa sa manche sur la vitre sale, l'essuya succintement et y apposa le papier.

La fille dehors lut et hocha la tête en signe d'assentiment.

Elle montra ses dents en une grimace hideuse plutôt réussie.

« Putain de merde ! » commenta Fadir en faisant mine de vomir. Il saisit cependant une tenue de protection et se mit à la revêtir en silence. Les deux autres en firent autant, l'atmosphère s'était soudain tendue. La fille exhibait une dentition d'aspect normal ; aucune plaque verte sur les gencives, ni de dents manquantes…
— Bien sur, ce vieux con de Fadir avait quand même raison, songea Clemens, tout allait bien pour eux, alors pourquoi risquer d'introduire une étrangère, qui plus est une étrangère contaminée ? —
Au fond de lui pourtant, ses clignotants étaient au vert : quelque chose lui disait que la fille n'avait rien, que le test se révélerait négatif : Elle serait morte depuis belle lurette sinon. Morte comme tous ceux qui étaient restés dehors.

Il colla un second papier contre la vitre après avoir griffonné quelques mots et la policière fit de nouveau signe qu'elle avait compris.

« Vous êtes prêts ? » Gwladys scruta ses deux collègues et réajusta la visière de Fadir avant de grogner un « Ça va » puis de sortir le kit de son emballage, préparant méthodiquement la plaquette et les différents réactifs du test.

L'aide-soignant rejoignit la porte du sas, sans un bruit, à cause de ses sur-chaussures violette qui lui donnaient, soit dit en passant, un faux air de ballerine ; une ballerine d'un mètre quatre-vingt-dix et dans les cent-dix kilo tout de même. Clemens vérifia machinalement le matériel éparpillé sur la tablette, puis tapota l'épaule de l'infirmière, signifiant qu'il était temps pour eux à présent de s'éloigner jusqu'à la porte intérieure du sas. De ce poste, il était possible d'ouvrir la porte d’entrée principale et verrouiller le sas afin d'y maintenir la jeune femme, le temps de connaitre le résultat du test.

« C'est parti ! » Farid avait abaissé le levier de fermeture de la porte, déclenchant les alarmes qui se mirent à hurler. Ils se ruèrent à l'extérieur du sas, regardant la suite par une des vitres étanches.

La femme actionna alors avec effort le volant de la porte extérieure qui bascula en souplesse sur ses gonds offrant juste assez d'espace à l'inconnue pour pénétrer dans la petite pièce. Elle hésita un quart de seconde, puis entra. L'imposant portail se verrouilla de nouveau derrière elle avec un claquement sec — celui d'un caveau qu'on referme —.

« Vous avez prévu quoi en cas de test positif ? Lança-t-elle sans attendre, la voix pleine de défi, « vous avez l'intention de me laisser crever ici ? »

Les trois comparses se regardèrent, émus, mais ne pipèrent mot.

« Bien ce que je pensais, reprit la fille, se redressant inconsciemment, du haut de son petit mètre cinquante. Une belle bande de salopards, je vois. Vous êtes peinards là-dedans depuis le début de l’ultime confinement, à rien foutre que bouffer gratis au frais du gouvernement. Et dehors c'est l'apocalypse. Elle détacha les syllabes À PO CA LY PSE. Vous… »

« Faites le test s'il vous plaît » hurla Clemens à travers la vitre. « Faites le test, bon sang et après vous viendrez nous faire votre morale à la con quand vous serez bien au chaud dans le burlingue avec nous »

Sa tirade eut l'effet escompté, la flic se tut et piqua le bout de son doigt avec une petite aiguille disposée sur le plateau. Un sang vermeil coula le long de son index sans qu’elle y prête attention. Elle appuya fortement sur le bout de son doigt et une grosse goutte tomba dans la cellule numéro un. Elle répéta l'opération avec la cellule deux, trois, quatre et cinq.

Gwladys, professionnelle jusqu'aux bouts des ongles, avait déjà saisit sa montre gousset et la tenait sous ses yeux. Trois minutes.

Trois longues petites minutes.

La mort ou la vie.

Clemens sortit une plasti-carte vermeil du fond de sa poche et la suspendit à quelques centimètres d'une fente jaune fluorescente. Un écriteau explicatif l'encadrait :

— Activez en cas de procédure d'urgence.

À — Phase de décontamination : Tournez la plasti-carte d'un quart de tour dans le sens des aiguilles d'une montre et ôter la plasti-carte.

Z — Phase d'élimination : tournez la plasti-carte d'un tour complet dans le sens des aiguilles d'une montre et revenez à la position initiale. Ôtez la plasti-carte.

Aurait-il à effectuer un tour complet à cette fichue carte ? Clemens sentit des gouttes de sueur couler le long de sa colonne vertébrale. Il frissonna tout en ne quittant pas des yeux l'étrangère immobile, une belle fille, constata-t-il, fine et bien proportionnée malgré sa petite taille, un visage ciselé, genre Pocahontas. Cette pensée un peu idiote le fit sourire.

Gwladys qui l'observait, se demandant s'il aurait le courage de faire ce qu'il faut, dans le cas où… interrrompit ses pensées et intercepta ce sourire sans savoir comment l’interpréter; leur relation était cordiale, forcément cordiale, pourrait-on dire, du fait du confinement imposé — difficle de se faire la gueule durant 43 jour ! — mais rien de plus, elle le trouvait psycho-rigide, il la jugeait superficielle.Et finalement elle opta pour une attitude intermédiaire, le rabrouant d'un léger coup de menton, les yeux dirigés vers le kit-test.

— aucune réaction —

« On lance les paris les gars ? » questionna, soudain euphorique, l'aide-soignant. « Je dis vert ! »

« Ta gueule Farid, je dis » Murmura l'infirmière, observant la fliquette qui tremblait légèrement à présent et se tenait d'une main hésitante à la tablette.

« Elle a une sale gueule, sûr qu'elle est infectée ! Regarde-la ! » insista le géant.

« Nan, mais c'est pas vrai, tu vas la fermer ? » Le médecin s'était retourné brutalement vers Fadir et Gwladys crut qu'il allait le frapper.

Un instant, elle vit des corps par terre, éventrés, du sang gluant souillant le carrelage. Elle imagina les cris, les coups, l’acharnement à tuer. L'odeur ferrugineuse du sang frais.
Elle huma la mort, la violence déchaînée, la haine.

Puis plus rien.

« Regardez ! Tout est blanc ! Tout est blanc ! » psalmodia-t-elle soudain remplit d'une joie qu'elle maîtrisait difficilement. « Elle a rien ! Fait partir la décontamination Clemens ! »

Personne ne semblait l'entendre.

Les deux hommes se défiaient toujours du regard, leurs corps figés dans une posture inquiétante. Dans le sas, la policière avait saisit son test et le collait à présent sur la vitre en riant.

« Allez vous faire foutre, allez vous faire foutre les trois clowns ! » Brailla-t-elle.

Gwladys agrippa la clé-carte, ce qui fit réagir le doc. La tension diminua entre les deux hommes, Farid grimaça, l'air de se demander ce qui lui avait pris, et Clemens reporta son attention sur les deux femmes.

D'un coup sec, il introduisit la carte et immédiatement des jets de fumées ocre se répandirent dans le sas en sifflant.

La prisonnière hurla, frappa la vitre frénétiquement, — tout rire avait disparu —, et elle se mit à insulter copieusement les trois agents. Farid, contre toute attente, reprit entièrement ses esprits à ce moment-là et mue par une soudaine empathie, partagea son désarroi. Il beugla tout en se collant au carreau :

« C'est la décontamination. Bouge pas. Y'en a pour quelques secondes. C'est la procédure de routine ! »

La femme tendit l'oreille et se mit à rire convulsivement, suçant son index ensanglanté.

Une fois débarrassé de leurs combinaisons et autres matériels de protection, Clemens, Gwladys et Fadir ouvrirent la porte du sas. Un lourd silence se fit. Le médecin réalisa trop tard que la fille portait un revolver à sa ceinture. Il paniqua. La policière comprit instantanément son trouble et porta sa main à son flingue.

« E ! pas de ça ! » lui intima l'infirmière d'un ton ferme « vous n'avez pas besoin d'arme ici, le secteur 1 est entièrement sécurisé. Posez votre joujou »

Pour toute réponse, la femme tendit son insigne devant elle jusque sous les yeux de l'infirmière. Elle lut à haute voi :

« Officier de Police Judiciaire Mandikowski Sandrine »

Gwladys ignora l'arme et tendit sa main à l'officier.

« Moi c'est Gwladys, tout le monde m'appelle Gwlagwla, le grand là, c'est Fadir et voici notre psychiatre, doc Clemens »

Sandrine ignora la main tendue mais rabattit le scratch sur le pistolet qui réintégra le fond de son étui.

Le soulagement que produisit ce geste se traduisit dans la seconde suivante par une prise de parole de tous les protagonistes en même temps et un certain brouhaha. Chacun avait un nombre indécent de questions à poser à la nouvelle arrivante.

« Retournons au bureau du secteur 1 », les convainquit Fadir À dire vrai, les locaux vides du grand hall lui fichaient le bourdon, a contrario du secteur 1, où ils avaient élu domicile, et qui, au fil du temps était devenu un lieu familier, rassurant, où ils ne manquaient de rien.

Ils firent donc le trajet inverse, à quatre cette fois et le doc en profita pour présenter rapidement la structure à leur invitée.

« A l'origine, il y a trois bâtiments distincts sur deux niveaux, le Q, le P et le O. Nous sommes dans le bâtiment Q. Chaque bâtiment possède 5 secteurs. Nous sommes du secteur 1, le Secteur adolescents.

Le secteur 2 était dédié aux enfants, le 3 aux femmes, le 4 aux hommes et le 5 aux éléments les plus dangereux, un quartier mixte.

« Pourquoi était ? » interrogea la fliquette.

« Parce qu'ils ont tous été évacué » expliqua Farid, « enfin tous sauf le nôtre et le 5, évidemment »

« Évidemment,» reprit Sandrine, plissant les yeux dans un effort de compréhension.

« Les décisions ne nous appartenaient pas. Le directeur nous avait ordonné d'évacuer les 2,3 et 4 avant l'heure H de l'ultime confinement, et c'est ce qu'on a fait, ainsi que joindre les familles et rapatrier les patients chez eux ou bien là où ils pouvaient trouver refuge. On a bouclé la zone après leur départ » se dédouana Clemens, devant la moue dubitative de Sandrine. Il rechaussa ses lunettes sur son nez en un geste machinal.

Sandrine se rapprocha du médecin,

« Et vous en bons petits soldats, vous, vous êtes restés avec vos ado ? Pourquoi ? » Questionna la fliquette de plus en plus intriguée.

« Parce que ces ado-là vous n'aimeriez pas les croiser en faisant vos courses, madame curieuse » la tança Farid d'un air de reproche. « Parce que ces petits enfoirés vous boufferaient tout cru s'ils en avaient l'occasion. Nous avons là-dedans Claire, 17 ans, une psychopathe avérée, Hugo, 18 ans, qui sous son air mélancolique a dévoré par deux fois la chair de ses propres géniteurs, Ambre, une sociopathe de 16 ans, elle a incité aux suicides — réussis — je précise, trois de ses camarades et Anthony, 19 ans, irrécupérable sadique doublé d'une érotomanie ostentatoire… et je pourrais poursuivre le bestiaire, madame la fliquette, pour votre simple édification personnelle. Nous comptons actuellement douze patients de cet acabit, le secteur 1 est le secteur le plus fermé de l'établissement avec le 5. Mais le 5, c'est réglé »

« Réglé ? » reprit Sandrine « Comment ça ? »

Clemens interrompit leur discussion. Quittant le couloir bleu, ils négligèrent la porte qui s'offraient face à eux — la salle de soins —, précisa le doc, et bifurquèrent légèrement, parcoururant un étroit couloir vitré d'un jaune pisse, qui longeait une grande salle où prônaient bureaux et matériel informatique.

« Nous arrivons secteur 1 À gauche, notre bureau, suffit de suivre le couloir jaune. On va vous montrer où dormir, où vous restaurer et tout le toutim. Moi, j'en ai ma claque là et sans vous offenser, je vais me reposer un peu. Ensuite, vous me trouverez dans le bureau qui jouxte la salle de soins. Gwlagwla, accompagne notre invitée jusqu'à sa chambre, disons la Q -00 /17. Farid, c'est largement l'heure de la ronde, et check la zone avec les caméras histoire de voir si nos pensionnaires sont bien sages. On se rejoint tout à l'heure » 
Il n'attendit pas la réponse de ses collègues et saluant discrètement Sandrine, s'éloigna d'un pas nonchalant, les mains enfoncées dans les poches de sa trop grande blouse blanche, le long d'un couloir jaune qui venait d’apparaître à un embranchement.

« Nous nous sommes portés volontaires pour rester » Souffla Gwladys.

« C'est-à-dire ? »

« Ben c'est simple, l'infirmière qui s'est fait larguée, le doc veuf et l'aide-soignant pédé et célibataire, un trio parfait pour prendre soin d'une douzaine de gamins complètement dingues, conclut-elle avec une moue douloureuse. « Et ça fait quarante-deux jours qu'on tient le coup. Dans l'aire des patients, tout est sécurisé, automatisé, nous n'avons aucun contact avec eux. Distribution de médicaments, nourriture, eau, tout ce qu'il leur faut pour vivre est directement fourni par des éléments robotisés situés dans les murs de chaque chambre. Nous les surveillons habituellement grâce a des campod, mais environ huit jours après le début de l'ultime confinement, les circuits vidéo ont commencé à déconner. Farid a remis en route les vieilles caméras datant du début du siècle qui, on ne sait pas pourquoi, n'avaient pas été retiré des plafonds. On n'y voit pas grand-chose, ça saute tout le temps et il n'y a plus de son À dire vrai, nous sommes plus des techniciens que des soignants, il faut bien le reconnaître » L'infirmière s'emballait, « Après la grande crise de la psychiatrie, l'Europe a définitivement tourné la page des thérapies à visage humain. Le retour à l'enfermement et la camisole chimique en systématique ont allégé les tâches et rôles des professionnels ainsi que les besoins en personnels mais… »

« Aucune relation sociale ? » s'insurgea la policière, coupant net le discours de l'infirmière, « ces jeunes n'ont aucun contact entre eux ? Ni avec vous ? Ni avec qui que ce soit ? »

« Ils ont chacun deux holo, humain et animal, c'est plus que suffisant » assura Fadir en hochant la tête. « D'ailleurs depuis quelque temps, ils ne recherchent plus vraiment d'interactions. Peut-être que malgré les doses massives de médocs ont-ils perçu le changement. La fin du monde… » Il éructa les derniers mots bruyamment en écartant les bras.

« T'es vraiment naze, Fad » Gwladys poursuivit la conversation en pénétrant dans un couloir blanc agrémenté de pois multicolores.

« Voila le quartier des chambres des internes » elle poussa la porte portant le numéro 17. « Votre chambre, pour le temps que vous resterez ici »

« Merci. Qu'y a-t-il après ces deux battants fermés ? » demanda Sandrine, intriguée.

« au-delà de cette petite portion de couloir, se situent le quartier des chambres des patients. Je vous interdis formellement de vous y rendre, bien sûr. Pour votre propre sécurité » répondit l'infirmière en martelant les quatre derniers mots, ses yeux plongés dans ceux de la policière.

Sandrine ne répliqua pas. Son interlocutrice semblait agitée d'un léger tremblement, le bout de ses doigts tressautait le longs de sa tunique grise de manière presque imperceptible mais l'agent de police avait l'habitude de remarquer ces petits détails qui trahissent : La jeune femme semblait inquiète, sur ses gardes peut-être. Sandrine se promit d'en connaître la raison. Elle observa les deux battants, le couloir puis pénétra dans la pièce où un lit, une commode, un bureau et un fauteuil étaient fixés au sol avec de gros boulons d'acier.

« Dites donc, pas très engageant comme hôtel ! » Mais la plaisanterie ne prit pas.

Fadir s'éloigna dans le couloir, grattant son grand crâne chauve, en tirant sur ses oreilles sans plus lui prêter attention, et appuya ensuite sur un bouton poussoir situé sur le mur. Deux battants s'ouvrirent puis se rabattirent en chuintant et l'homme disparut de l'autre côté.

Glwadys l'observait toujours.

« J'espère que vous n'êtes pas venue pour foutre le chambard. D'où sortez-vous ? Où avez-vous passé ces quarante-deux derniers jours ? » le ton était beaucoup moins avenant que précédemment. Ces changements d'humeurs des uns et des autres déstabilisaient un peu la flic, mais pas trop. Elle était pour ainsi dire dans son élément. Elle sourit avant de lancée sa répartie, tout aussi mordante.

« Si je m'attendais à un interrogatoire ! Laissez-moi le temps de me doucher, ajouta-t-elle en lorgnant la rutilante salle d'eau, et je vous promets une biographie complète »

« Entendu » soupira Gwladys. « La salle de soins est à gauche puis à droite, en retournant sur vos pas. Nous vous y attendrons.»

Elle commença par s'éloigner puis fit demi tour, marmonnant quelque chose pour elle-même et ouvrit la porte sans frapper alors que l'étrangère se débarrassait de son lourd ceinturon de fonction.

« la caméra plafond. Si vous ne voulez pas que Fadir vous reluque, pensez à mettre quelque chose dessus » Et elle claqua la porte.

Lorsque un peu plus tard Sandrine pénétra dans le bureau, Clemens, au fond de la grande salle, derrière une rangée de bureau, campait avachi devant un vieil ordinateur, un machin qui datait des années 2000, ronronnant comme un chat. Le médecin, la mine sévère, les yeux plongés dans l'écran, pianotait sur le clavier à une vitesse folle, sans lever la tête à son entrée, ne semblant même pas l'avoir entendue.

« Vous relatez mon arrivée ? » supposa-t-elle à voix haute, histoire de détendre l'atmosphère.

« Je suis en charge de patients, pas de parasites qui espèrent se la couler douce en se réfugiant ici. Pourquoi être venue ici particulièrement, d'ailleurs ? Quelqu'un vous envoie ? Vous avez une sorte de mission ? » questionna-t-il sans adresser le moindre regard à la nouvelle venue. « Je vous préviens, le temps que vous êtes là, vous vivrez à notre rythme et respecterez les protocoles en vigueur dans ce secteur » Son regard était à la fois vide et plein de colère quand il daigna l'affronter enfin.

« Oh Oh ! Une menace à la fois, docteur Clemens. Avez-vous oublié que dehors, sur un rayon de cinquante kilomètres en tout cas, le monde tel que vous l'avez connu, n’existe plus ? Le virus non plus d'ailleurs, je supposerai bien qu'il s'est délité ne trouvant plus de quoi se rassasier, mais je ne suis pas une scientifique. En tout cas… »

« Comment est-ce qu'une petite bonne femme a pu survivre alors ? » tonna Farid, passant le seuil du bureau. « Faudrait m'expliquer ça, madame la flic. Votre couleur de peau ? Peut-être que ce fichu virus aime pas le noir hein ! » Les yeux de l'homme bougeaient sans cesse, de gauche à droite en une alternance quasi hypnotique. Il postillonnait, haussant le ton sur la fin de ses phrases qui ne laissèrent pas indifférente la jeune infirmière arrivée derrière lui ; elle lui claqua une tape pas vraiment amicale sur le derrière de la tête.

« Tu manques de classe, Farid, bon sang ! Aucune manière, aucune ! » L'aide-soignant se rencogna, Gwladys entra dans la salle de soins et s'assit sur un coin de paillasse, sous le rideau des caméras qui formaient un mur entier dans la grande pièce.

« Que racontent nos petits chouchous ? » s'enquit-elle d'un air joyeux, en jetant un coup d'oeil rapide sur les écrans, « est-ce que la distribution des médocs de vingt-et-une heure est ok ? Pas de problème avec la cuisibot cette fois ? »

« Bah nan, pas de soucis, j'ai réparé la cuisibot, c'était juste un fusible, heureusement que les boitiers sont accessibles » gloussa Farid, sans s'offusquer du geste de Gwladys. Elle leva les yeux au ciel.

« Installez-vous » lança-t-elle hilare, à Sandrine qui examinait depuis quelques instants les différents postes de télé accrochés au mur et n'en revenait pas de ce qu'elle apercevait ; l'image floue, rendue neigeuse par un grand nombre de petits points gris et blancs tressautaient sur l'écran. Une large ligne noire barrait les images environ toutes les cinq ou six secondes. Des silhouettes apparaissaient, sur une douzaine d'écrans, assises ou couchées, impossible de distinguer s'il s'agissait d'un garçon ou d'une fille, la qualité de la vidéo ne le permettait pas. En fait on n'y voyait presque rien, à peine la vision fantomatique d'une présence dans certaines chambres…
Et pourtant… 

« Qu'est-ce que… »

La fliquette se tut. Sans se départir de son calme, elle détourna les yeux des images qui poursuivaient leur sarabande grotesque, prit un siège, et répondit avec naturel,

« Je suis restée bloquée dans le sous-sol de la gendarmerie, figurez-vous. Il y a eu des débordements lors des derniers transferts de prisonniers, ça a tourné en bagarre générale, des collègues sont morts, la plupart en sont réchappé, avec ou sans leurs prisonniers, des familles de détenus ont pris d'assaut les locaux pour demander leur remise en liberté » 

Il y eut un flottement, chacun semblait ressasser des souvenirs similaires, évitant le regard de l'autre. Sandrine revit le sang, les tchac-tchac des armes automatiques, les cris de ses confrères remontant en trombe dans l'ascenseur, et elle a moitié assommée au fond d'une cellule, seule, tandis que les derniers détenus sautaient sur le dos des gars en uniforme et que la porte se refermait, déclenchant la mise en quarantaine de la zone. Ses yeux papillotèrent et l'officier de police reprit pied dans la réalité. Clemens avait lâché son ordinateur, il se tenait tout à côté d'elle et lui prenait le pouls d'un air dégoûté. Elle agrippa légèrement sa blouse, afin de s'asseoir plus confortablement.

« On va vous servir un truc à manger, vous êtes en hypoglycémie peut-être, à quand remonte votre dernier repas ? » questionna l''infirmière qui s'était rapprochée avec un tensiomètre.

« Je… mon dernier repas ? » Sandrine sourit… La table était mise dans le salon, ses gamins devaient revenir de l'arrêt de bus d'ici quelques minutes, — c'était repas en famille le soir — elle avait toujours insisté là-dessus, du moins quand elle était en repos. Mat ne s'y était jamais opposé, au contraire, c'était un mari affable, serviable et qui adorait passer du temps avec les enfants. Ce soir-là, il n'était pas encore rentré… Leur dernier repas ensemble. La veille du confinement ultime, le jour de l'annonce du Président Ridodon.

« 89 pour cent de saturation en oxygène, 17/24 de tension, pulsation cardiaque à 180 c'est pas bon, merde ! » souffla le doc en direction de son assistante.

La policière vit dans l'angle gauche de son champs de vision le gigantesque Farid se ruer sur elle avec un masque à oxygène et son premier réflexe fut de le repousser en grognant. Le doc grimaça au-dessus d'elle, ordonnant, la voix totalement détachée,

« Maintenez là ! Je vais lui faire une petite injection ! »

L'agent Mandikowski sombra dans un puits d'encre.

Elle reprit conscience dans sa chambre.
Sa tête pulsait tel un phare dans la nuit et le sang affluait douloureusement jusqu'à son cerveau. Sandrine tenta de se lever.

« Je serai vous, j'attendrai un peu ! » lui recommanda Gwladys en lui tapotant affectueusement la joue, « vous nous avez fait une petite crise d'angoisse, dites donc pour une flic, c'est pas banal. Vous êtes pas loin du burn-out ma belle » poursuivit l'infirmière sur un air de fausse confidence. « Bon, je vous laisse dormir à présent que vous avez repris connaissance, je reviendrai vous ausculter plus tard, avec Clemens »

Sandrine secoua sa main pour la retenir, elle avait mal, une perfusion était installée à hauteur de son avant-bras. Un pic à perfusion cliqueta sur ses roulettes lorsqu'elle s'agita pour sortir du lit.

Gwladys la repoussa sans ménagement cette fois, appuyant sa tête sur l'oreiller, le regard soudain fixe et d'une étonnante dureté.

« Vous êtes coriace hein, mais maintenant il faut dormir. Je ne voudrai pas qu'il vous arrive malheur, ma petite Sandrine. Alors restez tranquille, ou je vous colle des contentions et vous ne pourrez même plus vous grattez le nez, vous comprenez ce que je vous dis ? »

La fliquette acquiesça lentement. Elle commençait à entrevoir que l'idée de se réfugier dans ce bâtiment, au nom si avenant de « Les eaux vives », (après avoir tapé à la porte d'un nombre conséquent de maisons ou établissements en tous genres, trop vide ou au contraire, trop remplis de cadavres en décomposition), n'était peut-être finalement pas un si bon plan qu'il lui était apparu tout d'abord. Elle somnola un moment.

Il faisait vraiment nuit à présent. Sandrine le sut parce qu'elle avait travaillé si longtemps en horaires décalés que son corps savait ce genre de choses. Elle arracha la perfusion sans un bruit. Le carton qu'elle avait coincé maladroitement sur la caméra du plafond l'après-midi même était toujours en place.

La jeune femme avait prit sa décision sans même y réfléchir ; elle devait sortir de cette clinique psychiatrique. Il y avait quelque chose de pas net chez ses trois hôtes et elle ne souhaitait pas savoir quoi. Et elle avait vu les écrans dans le bureau…

Rejoindre des gens immunisés, a priori, comme elle, voilà quelle avait été sa réflexion lorsque la cellule automatisée où elle croupissait dans le commissariat s'était enfin ouverte, à la fin de la quarantaine, il y avait trois jours de cela. Et lorsqu'elle avait aperçu le visage de Clemens à travers la vitre blindée, un espoir l'avait submergé ; celui de survivre À présent, elle savait au fond de ces tripes que si survie il y avait, elle ne se trouvait certainement pas ici entre les murs de cette asile pénitentiaire.

Elles scruta la chambre ; sa tenue et son ceinturon de fonction avaient disparu.

À tâtons, elle commença par arracher précautionneusement sa perfusion et se coula alors hors du lit, en rampant sans bruit, instantanément glacée par sa reptation dès qu'elle atteignit le sol carrelé. Par dessus ses sous-vêtements de sport, on l'avait revêtue d'une longue chemise d’hôpital munie de pressions dans le dos, dont le col lui serrait le cou à chaque fois qu'elle avançait trop vite. Elle s’évertua à la patience et poursuivit sa progression. Au bout de quelques mètres, elle rencontra la porte menant au couloir et entreprit de se redresser, s'aidant du mur. La tête lui tourna légèrement mais l'impression ne dura pas, bien que devant ses yeux, de petits points blancs papillonnèrent. Cela lui rappela les écrans, les patients…
Elle voulu tourner à gauche, récupérer le couloir vitré qu'elle avait parcouru en venant, mais une porte fermée n'en permettait plus l'accès. Elle secoua doucement la poignée sans succès.

*Impossible de retourner en arrière, impossible d'avancer*, songea-t-elle,*et si je traverse le bureau, et la salle de soins, je serai de suite repérée, pourtant, c'est la seule sortie qui reste mènant au couloir bleu…*

Elle se sentait piègée comme un rat. Son cœur battait dans sa poitrine, sans qu'elle réussit à l'apaiser. Un instant, elle ferma les yeux, se concentrant sur sa respiration… Un bruit de pas lui parvint, lointain puis de plus en plus rapproché, accompagné bientôt d'un nicassement étouffé qu'elle identifia sans mal : Farid. *Il entame probablement sa ronde. C'est l'occasion ou jamais…* 
Effectivement, l'armoire à glace hors jeu, elle avait peut-être sa chance contre le doc freluquet et la jeune infirmière.

La policière s'immobilisa au coin de la porte, en apnée. Tout était plongé dans la pénombre. Il n'y avait heureusement pas de point lumineux de sortie de secours. *Etrange.* remarqua l'agent.

Farid passa le coin droit et se matérialisa à peine, ombre parmi l'obscurité qui régnait. Il marchait à bonne allure, sans se douter de sa présence. Sandrine sentit la masse physique de l’homme en mouvement déplacer l'air du couloir et cet air la frôler tandis qu'elle se renfonçait dans la porte close comme pour ne faire qu'un avec elle. Elle retint un cri. Farid s'éloigna, appuya sur le bouton opérant l'ouverture des portes battantes du secteur patients et elles l'avalèrent avec un gargouillis proche d'une déglutition.

Dès que les portes furent refermées, Sandrine fonça à travers le couloir jaune, tenant sa chemise contre son corps frissonnant, bifurquant une fois, puis une autre, elle se retrouva bientôt face au bureau dont seul les écrans dispensaient une lueur feutrée, pas un bruit ne s'en échappait.

Y avait-il quelqu'un ou bien le doc et l'infirmière étaient-ils partis se coucher pendant que l'aide-soignant veillait ?

Elle tergiversa juste le temps nécessaire pour entendre un léger ronflement.

*Il y donc quelqu'un là dedans !* Rumina la fliquette au désespoir. Elle n'avait plus d'arme pour se défendre, et ses capacités physiques, elle le sentait, s'avéraient résolumment amoindries…

*La perfusion ? Qu'y avait-il dans la perfusion ?* Songea-t-elle.

Le ronflement s'amplifiait puis s'amenuisait régulièrement. *Je parie sur ce bon docteur Clemens*, supputa Sandrine. Sans plus tergiverser, elle franchit le seuil de la porte. Le jeune homme dormait profondément au creux d'un large fauteuil, les mains croisées dans son giron, ses lunettes avaient glissé de guingois le long de son visage, jusqu'à sa bouche entrouverte d'où s'échappait à présent un douloureux soupir. Ses jambes et ses pieds négligemment étendus en travers de la pièce reposant sur un tabouret.

La fugitive n'avait d’autre choix que de l'enjamber pour passer.

*Et puis merde !* 
Au moment où elle s’élançait, elle sentit un effleurement dans son dos et se retournant, rencontra les deux billes charbon courroucées de Glwadys ainsi que ses bras tendus prêts à l'alpaguer, — une seconde trop tard cependant —, la policière avait déjà sauté par dessus Clemens et pistonnait sur ses courtes jambes, allongeant sa foulée : tant pis pour les crampes qui se profilaient dans ses mollets engourdis, protestant de cette soudaine violence qui leur était faite.

Elle passa sous le mur d'écrans, évitant de regarder le spectacle monstreux qu'ils diffusaient. C'était la chose la plus horrible qu'il lui fut donné de voir. Chaque poste affichait des images insoutenables, une vison qu'elle avait été apparemment la seule à percevoir tout à l'heure, alors que les trois paramédicaux s'attardaient sur les pathologies et petites manies de chacun de leurs pensionnaires. Sur les écrans, immondes et indécents, s'exposaient des corps depuis longtemps pourris dans des positions grotesques, des monceaux de gélules et d'aliments gâtés s'entassaient à coté d'eux, distribués par des robots ménagers indifférents. Par dessus tout, une horrible moisissure duveteuse recouvrait cadavres et détritus tel un linceul et le tressautements des images, ainsi que la qualité médiocre de la transmission ne rendaient pas la vision de ces scènes moins atroces…
— Les patients étaient devenus des momies que gardaient trois gardiens déments — 

L'agent Mandikowski ripa à la sortie du bureau, heurta violemment l'encognure de la porte de la salle de soins, miraculeusement ouverte, rebondit contre le chambranle, et s'élança dans le couloir bleu, poursuivant sa course sous les vociférations de l'infirmière et du médecin revenu du pays des songes.

« Putain ! choppe-là Clemens ! » hurla Gwladys à bout de souffle, poussant le doc rouge et échevelé devant elle, « choppe cette garce ! »

Le jeune homme ne prit pas le temps de répondre, il fonça à travers le couloir indigo, la fille dans son axe de mire. Il ressassait intérieurement. Il n'avait rien demandé lui, il n'avait rien à se reprocher. *Sauf que j'aurai pas dû la laisser entrer, le protocole !… s'en tenir au protocole, virus ou pas virus, j'aurai dû…*
Il poussa un grognement féroce et accéléra l'allure. Il atteignit le couloir vert.
*elle prend de l'avance la garc !* 
Il força encore l'allure et attaqua le couloir orange, la fille semblait voler au dessus du sol. Gwladys braillait des ordres qu'il n'entendait pas, plus loin derrière lui. Ses talons claquaient sur le carrelage.

Dans le hall central blanc, en aval de la poursuite, une porte claqua brutalement sur la gauche et Clemens ébaucha un sourire.
 — fin de ronde —

le géant surgit brutalement sur sa droite.
Sandrine n'eut pas le temps de se protéger le visage et encaissa l'énorme poing de Farid en pleine face, en tentant de poursuivre sa route.

« Hey ! Mais elle va où la copine ? Elle apprécie pas la compagnie ? » gloussa le colosse en la saisissant par les cheveux qu'elle avait long et crépus. Il la souleva de terre sans effort, la secoua brutalement, « dis donc, t'es pas très polie, fliquette, on t'accueille à bras ouverts et tu nous fais faux-bond dès potron-minet… Saloperie ! » Glapit Farid alors que Sandrine gesticulait comme un diable et essayait de le mordre en vain pour se libérer. Elle n'y voyait plus grand-chose, du sang coulait abondamment de sa blessure à la tête et s'étalait sur son beau visage, obturant son champ de vision.

Clemens déboula dans le hall, essouflé, suivit de prêt par Gwladys. Ils stoppèrent en ricanant dès qu'il prirent la mesure du tableau qui s’offrait à eux.

« Ah, madame l'agent est tombé sur un os » s'esclaffa l'infirmière.

Clemens réajusta ses lorgnons, redressa sa blouse et se tint devant la policière qui avait épuisé ses ressources et geignait doucement, tenu comme un chiot par la poigne de fer de l'aide-soignant.

« Vous nous causez bien du tracas, je pense que vous avez un petit soucis, chère madame. Je dirai que cette pandémie, votre incarcération involontaire prolongée, ont eu sur votre esprit un effet fort délétère » diagnostiqua Clemens.

«Choc post-traumatique ! » brailla Farid en la secouant de plus belle.

Des gouttelettes sanguinolentes s'éparpillèrent sur les tenues immaculées des trois fonctionnaires de la santé.

« Possible, cher Farid, ou bien une bouffée délirante, ce qui semble probable au vue de l'agitation actuelle du sujet » renchérit sa collègue.

Effectivement, l'agent Mandikowski boxait dans le vide en hurlant de rage mais ses forces l'abandonnaient.

Le doc, calme et résigné, leur fit part de sa prescription :

« Nous allons procéder à l'isolement temporaire de cette femme, euh de cette malade »

Gwladys sortit de sa poche un pistolet à injection et Farid, d'un rire hystérique, étreignit la policière de ses deux bras, gros comme des troncs d'arbre, en chantant « dodo, l'enfant do, l'enfant dormira bientôt… »

Gwladys éclata d'un rire franc, ses yeux malicieux cherchèrent Clemens mais celui-ci enfonça les mains dans ses poches, se contentant de donner un dernier ordre, d'une voix blanche. Une petite voix lui parlait à l'intérieur de sa tête *Mais qu'est-ce que je fous ici ?* braillait-elle à tue-tête.

« Suivez le protocole bon sang ! Farid, arrête tes conneries, que Gwlagwla la pique et la fouille. Je pense qu'elle a profité de mon inattention dans le bureau cette après-midi pour me piquer ma plasti-carte. Un peu plus et elle se faisait la malle ! »

Farid lui fit les yeux ronds tandis que la fliquette se débattait de plus en plus faiblement. Il semblait fascinée par sa nouvelle patiente.

« Un sujet de choix ! » décréta Gwladys en visant adroitement le postérieur de l'agent Mandikowski dans lequel se planta une mince fléchette tout en récupérant le pass qu'elle serrait convulsivement dans une main.

« Ensuite, termina le doc, imperturbable à présent, penchant la tête de coté et tapotant ses lunettes comme pour mieux réfléchir, « elle rejoindra le quartier des patients, on peut encore accueillir du monde de toute façon… chambre… Q-01/13 par exemple, elle sera très bien là-bas, ça pue pas trop. Heu, je veux dire, je veux dire…» Et il s'éloigna en traînant les pieds, qu’il avait fort longs, comme à regret.

« Chouette numéro, le treize, boss, pour notre première patiente immunisée ! » lui lança Farid, suant à grosses gouttes sous son uniforme. Il portait Sandrine Mandikowski comme un sac de son. Elle ne réagissait plus.

« j'te verrai à la caméra, lui sussurra-t-il à l'oreille, « t'inquiète pas ma poulette, on prendra soin de toi. Ici, tous nos patients sont chouchoutés… » lui confia-t-il, en la berçant dans ses bras tandis que l'agent de police, tétanisée, perdait lentement connaissance, les yeux exorbités par la terreur.

[toute ressemblance avec des personnes existantes serait… presque fortuite.]
