---
title: La mort du Loup-garou
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Souvenirs d'une terrible bataille"
epub: true
pdf: true
draft: false
---
En nous enfonçant dans la grotte, l'odeur devint si nauséabonde que nous hésitâmes à continuer. Après un court moment, nous continuâmes d'avancer, emplis de dégoût, les narines pincées.
Au détour de ce qui faisait office de chemin, nous aperçûmes une forme humaine penchée sur l'un des chasseurs, mais, à y regarder de plus près, nous nous rendîmes compte avec horreur qu'il s'agissait d'un loup-garou !

Il était agenouillé près des restes sanguinolents d'un des nôtres, un long filet de bave suintait de sa gueule sanglante jusqu'au sol. Nous nous portâmes en avant mais déjà la horde complice du monstre nous encerclait…

Nous fîmes front avec un frémissement d'excitation contenu. Notre périple touchait à sa fin, et sa conclusion, quelle qu'elle fut, se jouait maintenant.

Nos deux archers, en retrait, comme à leur habitude, bandèrent simultanément leur arc et deux flèches aux pointes d'argent traversèrent l'espace sans un bruit mais le loup garou, vif comme le vent, se porta de côté et évita en partie le trait de Lombrenoire grâce à son ouïe fine ; celui-ci se ficha profondément dans la cuisse épaisse de l'abominable créature.

Un loup vieux, à la toison d'argent, tenta la même prouesse que son maitre, mais Agalloch, second archer, anticipa sa réaction et la bête s'écroula, mortellement atteinte, dans un hurlement lugubre.

Le Loup garou, profita de la confusion et avec une force surprenante, tous crocs dehors, entreprit de mordre Slechtvalk ! Mais notre chevalier ne fut pas dupe, et son attention ne lui fit pas défaut en cet instant : l'infâme animal rencontra le fraoner béni en acier de vie du Dinturan. Quelques-unes de ses infâmes canines s'y brisèrent sans atteindre autre chose que le métal virant au rouge.

Nos sorcières tentèrent alors un coup d’esbroufe, voyant l’ennemi affaiblit, mais le loup garou resta de marbre fasse à leur magie par trop élémentaire et aussitôt son attention se porta sur elles ; il les jaugea d'un œil mauvais où perçaient la convoitise et la cruauté… Âmesombre et Bruneterre se retrouvèrent immédiatement en position délicate cherchant déjà comment s'extraire de ce mauvais pas.

Les douze survivants d'Ebenissia : notre nom n'est pas que mots et sons ! Nous sommes ce que nous sommes : forts, fiers et solidaires dans la mort qui nous unira tous. Nous admirâmes alors, saisis, notre compagnon, héros malgré lui, bousculer son destin.

Krisiun, rusé et agile, splendide tel un dieu grec, surgit en effet de nulle part et bondit soudain au milieu des loups tétanisés, son cri barbare et terrible l'accompagnant dans sa chute puis se répercutant dans la grotte. Il s'abattit lourdement sur l'Abomination, ses mains d'une puissance colossale se refermant sur son cou qu'elles ne le lâchèrent plus. Des râles épouvantables s'élevèrent, répercutés par les parois de la caverne mais Krisiun, campé sur ses deux jambes solides comme des colonnes de marbre, en transe, serrait la nuque de son adversaire dans une étreinte plus que mortelle. Dans un ultime soubresaut, les yeux vairon du loup garou roulèrent dans leurs orbites, marquant sa stupéfaction puis perdirent de leur éclat dément : un craquement sinistre et définitif se fit entendre et le sort du monstre fut scellé. Son âme apaisée franchissant enfin le seuil du royaume des morts, ainsi que celles de ses compagnons loups, mettant un terme à leur malédiction.

Nous avions vaincu le loup-garou et ses rejetons !
Nous détournâmes le regard pendant qu'il reprenait sa forme humaine, mais sa tenue de chasseur l'avait trahi : le chef des loups faisait lui-même partie des chasseurs partis de Tilien ! La bande de racaille avait trouvé son maitre. Nous ne fûmes guère surpris de trouver les marchandises disparues au fond de la caverne.

À qui profitait cette guerre entre les principautés ? Cette question nous occupa encore toute la nuit. La lumière des braises et celle de la lune se mélangeaient, faisant luire les armes et les marchandises pour Tural d'une teinte tantôt orangée, tantôt argentée, tandis que les sacs de pièces d'or des voleurs firent de bons oreillers. La compagnie dormit d'un sommeil doré salivant ce rôti d'ours sauce chasseur promit par le Duc et qui nous attendait au bercail !
