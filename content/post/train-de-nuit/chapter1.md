*Les passagers les plus malchanceux poussent valises, sacs et mallettes sur des chariots aux couleurs de la Transunion, mythique ligne de chemins de fer reliant Paris à Istanbul. Les plus heureux sont sans conteste ceux possédant quelques domestiques se chargeant de cette tâche incontournable mais fastidieuse que le chargement des bagages. Les plus volumineux resteront dans le wagon de queue, appelé, à juste titre, — la Soute —. Le personnel naviguant, de noir et blanc vêtus, droits et solennels, — mais souriant —, est posté à l'entrée de chaque wagon. Le contrôleur en chef, au pied du numéro 7, accueille un homme d'une quarantaine d'années, affublé d'un chapeau de daim sombre, d'un costume pied-de-poule du plus grand chic et d'un air passablement amusé devant l'enthousiasme débordant de l'employé des chemins de fer.*

 Le train en comporte 17 incluant la Soute, celui de maintenance, le numéro 16, et le wagon-restaurant, qui porte le numéro 11. Les 14 autres sont des wagons-studettes luxueux, aux boiseries réalisées par les plus grands ébénistes du siècle passé comportant une chambre, un salon ainsi que les commodités d'usage. La ligne, bien que centenaire, je vous l'accorde, n'a pas pris une ride. Elle est dotée de toute la technologie dernier cri, — voyez seulement ces pare-soleil rétractables par détecteurs de luminosité intégrés dans les hublots —, ainsi que l'effet zoom sur la vitre quadruple épaisseur (et accessoirement à l'épreuve des balles), que vous pouvez actionner du boîtier de commande de votre montre-billet, dernière innovation de la compagnie Transunion, pour admirer le paysage au plus près quand bon vous semble. Véritablement génial non ? Pourtant, ce voyage sera le dernier voyage du train 10 222, affectueusement surnommé le Byzantin. Infatigable, il traverse de nombreux pays et régions mais ne procède à aucun arrêt, sauf ceux d'urgence et de service, dans de petites gares improbables où aucun passager n'attend sur le quai. Les passagers embarquent à Paris, et débarquent à Istanbul. Je vous l'affirme tout net, en tant que chef contrôleur de ce train depuis de nombreuses années : on n'a jamais perdu — et encore moins gagner — de passager sur cette célèbre ligne ferroviaire, durant la traversée.

Je me permets de vous offrir le livret d'accueil, véritable bible du voyageur Transunion, pour qui utilise pour la toute première fois notre prestigieuse maison. C'est votre première fois n'est-ce pas ?

[…]

Le train, noir et blanc également, rutile sous les éclairages puissants du quai 7. Son damier baroque allonge sa ligne, il semble tout à la fois démesuré et d'une finesse impossible pour un engin fait de métal et de plastique. Des autres quais, les voyageurs moins chanceux lancent des œillades admiratives sur le Byzantin.

 Cent-quatre-vingt-douze passagers sont attendus en ce samedi matin, et cent-quatre-vingt ont d'ors et déjà passé l'étape de vérification des bracelets d'embarquement et se dirigent soit vers leur wagon, soit vers leur compartiment, soit enfin sirotent-ils déjà nonchalamment la coupe de champagne de bienvenue, un Kranken, bien entendu, — année 2023, une année bien funeste, j'en conviens, mais pas pour le vin — en attendant le départ fixé à 21h14. Un horaire inchangé depuis un siècle.

Pour ce dernier voyage, le wagon nommé la Soute embarquent une bien étrange bagagerie : Les douze derniers voyageurs. Étrange mais discrète.

À cet instant, la locomotive, aux allures de Tristar d'un autre temps avec sa replète cheminée en forme de jarre et son gros phare d'antan, fixé à l'avant de la cuve, siffle le premier appel. Il reste trente minutes pour embarquer. C'est ce moment que choisissent les derniers voyageurs pour faire leur entrée. Elle ne passe guère inaperçue : douze cercueils d'acier plombés, montés sur coussins d'air longent le quai dans un silence pesant sous les yeux ébahis des passagers, tels une charretée de chenilles processionnaires.

Le train est à présent au complet.

 Notre itinéraire nous portera tout d'abord à traverser les grandes plaines de l'est, ce désert immense et vide bordé par les mers souillées, comme on les appelle à présent. Ce préambule d'apocalypse ne laissera que peu de souvenirs, car la nuit enveloppera bientôt les voyageurs dans un sommeil oublieux des folies d’antan. Ils seront frais et dispos pour aborder le vrai et passionnant début de leur périple, à savoir les grands polders des pays du Nord, la Scandina. Le froid, vif à cette époque, rend les paysages absolument féeriques. Nous croiserons les troupeaux de rennes aux confins des steppes gelées, à la recherche de lichen pour assurer leur transhumance, accompagnés de leurs infatigables bergers, dont les traîneaux solaires filent comme le vent. Le soleil, glacial, mais bien présent tout au long de l'année désormais, a permis cette petite révolution que l'introduction de l’énergie solaire dans ces régions auparavant abandonnées par les hommes. Plusieurs villes souterraines, dont Rostok et Gdansk, y prospèrent, comme vous le savez peut-être. Puis le train longera la cote gelée jusque Riga, capitale de l’Eurasie occidentale (E.O.C.). Un périmètre dit — de précaution antiradiation — interdisant l'entrée dans cette zone de l'ancienne Europe centrale à tout transport, hormis militaire, obligera ensuite le convoi à contourner la dite région sinistrée.

Je vois à votre mine que vous n'êtes guère au fait de ces éléments. D’après les dires, ce territoire, précédemment riche de cultures et d'hommes ne laisserait survivre que quelques animaux microscopiques, et encore, certaines régions, semble-t-il, sont irrémédiablement stériles. Mais ce ne sont que des ragots, comme ceux circulant à propos des irradiés qu'on nomment immortels à cause des effets secondaires de leur exposition aux rayonnements… balivernes que tout cela ! Personne ne voyage en ces lieux, théâtre de la folie des hommes. Laissons-là cette désolation et revenons à notre merveilleux voyage.

L'itinéraire de notre Byzantin nous mènera ensuite jusqu'aux rivages de la mer de Barentz, avec sa mégapole éponyme, où nichent stations balnéaires, grands hôtels et premier centre boursier de l'E.O.C. Nous y ferons un premier arrêt de service d'une durée de quarante-cinq minutes, car après avoir traversé les grandes étendues froides de la taïga, il est toujours prudent de vérifier l'ensemble des wagons et la bonne tenue de notre double locomotive. La Tristar ne déplore que trois pannes en cent ans, gage d'un certain savoir faire, Transunion en tire une justifiable fierté. Une machine fiable, autant le dire tout net !

Nouveau signal sonore, plus long que le précédent.

 Voila la sirène qui retentit, il est temps de rejoindre le bord, je vous conterais la suite de notre périple bien au chaud, ne vous en déplaise, sur une des confortables banquettes de velours du wagon-restaurant, une tasse de chocolat ou café bouillant entre les mains, puisque l’alcool ne semble point illuminer vos appétits, à votre convenance.

Par ici Monsieur, attention au marchepied automatique, toujours un peu surprenant. Ensuite, prenez à votre guise le tapis roulant à votre gauche ou le couloir marcheur à votre droite À tout à l'heure, cher passager. C'est votre premier voyage n'est-ce pas ?

[…]

Il me semblait bien, — Je n'oublie jamais un visage —.

L'homme s'éloigne sans empressement, son air assuré, ses colts croisés sur ses hanches dégage une prestance indéniable. Le contrôleur sert dans son poing une carte magnétique créditée d'un montant impudique que vient de lui glisser discrètement l'inconnu avant de tourner les talons.

 Voilà bien un homme d'affaires de cette nouvelle génération ! Un tantinet hautain tout de même. Et cette manie de toiser son monde sans répondre. Agréable conversation cependant, bien que mon interlocuteur soit plutôt dans l'écoute qu'à s'épancher À peine s'il a murmuré son nom pour l'embarquement. Un certain John Doe. Cela me rappelle quelque chose, mais je ne parviens pas à mettre le doigt dessus. Cela viendra, nous avons quelques milliers de kilomètres pour y parvenir.

Les quais sont vides à présent et le train s'ébranle dans un chuintement discret, glissant sur les rails tout en prenant rapidement sa vitesse de croisière. Le contrôleur flâne, de wagon en wagon, faisant connaissance avec cette ultime palanquée de voyageurs, des familles aisées accompagnées d'enfants turbulents, des jeunes gens en mal d'aventures, des femmes esseulées, des couples de vieillards pour qui ce dernier voyage a une connotation toute particulière ; un panel divertissant, et ravi d'être diverti, pour la plupart attentif à ses explications, conseils et autres billevesées.

Puis le silence s'installe, les couloirs sont désertés au profit de lits douillets. L'obscurité s'enroule autour du Byzantin qui fonce à toute allure et couvre d'un voile de pudeur sombre les plaines désolées.

 J'aperçois Mr Doe qui s'en revient des wagons de queue, alors que j’entreprends une dernière ronde. Notre homme aux colts a des fourmis dans les jambes. N'aurait-il pas l'air légèrement préoccupé ? Pourtant, à bord du Transunion, on dort comme un bébé. Tout a été conçu pour assurer un confort optimum à nos remarquables passagers À l'observer, le bonhomme a tout du gosse de riche, et cependant, plus vieux que ne le laisse à penser ce visage lisse et juvénile. L'allure, les vêtements, la voix et le ton, c'est un homme à la maturité affirmée et sans nul doute habitué à diriger. Un homme du monde… pourtant, pourtant, quelque chose dénote chez ce grand gaillard.

Le voilà qui s'approche À cette heure-ci, nulle crainte, je puis lui tenir le crachoir jusqu'au petit jour. Pas un chat dans les couloirs, les passagers dorment et rêvent à poings fermés. Allons à sa rencontre, il se peut que j'en apprenne un peu plus sur sa personne, et de l'utilité de ces pistolets, qu'il trimballe avec tant de flegme, l'air de rien. Il accroche mon regard, — voilà ce qui cloche — ces yeux-là sont glacials. Ils tueraient plus aisément qu'une balle tant ce bleu vous transperce à donner le frisson.

— Qu'en est-il cher Mr Doe ?

[…]

— Je ne vous permets pas, lâchez ma cravate, je vous prie ! Lâchez-moi !

Mais l'inconnu serre le tissu autour du cou du contrôleur et l’entraîne, comme un chien au bout d'une laisse trop courte, jusqu'aux sanitaires les plus proches. Ses cris n'ont duré qu'un instant, il est à présent trop occupé à capter quelques particules d'oxygène pour s'inquiéter de sa cravate ou protester contre les manières gougeâtes de son passager.

** Bonjour ! Bienvenu dans les Sanipremium de Transunion. Pour un maximum de confort, cher client, merci de répondre à cette question : Petite ou grosse commission ?**

 Je regardai le contrôleur, la casquette vissée sur le crâne virer du rose au gris, chiffe molle entre mes bras. Il pesait son poids le bougre, et répondit :

— Grosse commission.

— Stalle 3, s'il vous plaît.

Une rampe lumineuse accompagna la voix de mon interlocutrice virtuelle. Je traînai mon cadavre en soufflant un peu, et l'assis tant bien que mal sur le trône. C'en était assez ridicule, mais avant de me débarrasser de ce corps, j'avais à prendre grand soin de le dévêtir. Le reste ne serait que jeu d'enfant. La trappe de service, sous la douche italienne, au fond, allait servir de tombeau provisoire à ce foutu bavard. Bavard et curieux. Ma grande faute aussi, d'avoir fait preuve d'un peu trop d'humour en choisissant ma nouvelle identité. Il faut bien rire dans la vie, ne croyez-vous pas ? Qu'est une vie sans humour, je vous le demande ! Regardez-moi celui-là à poil sur son chiotte, la figure bleue et la langue bien noire, quelle tableau d’épouvante ! Et pourtant, notre bon contrôleur en chef prête bien à rire, en cet instant.

** Merci d'avoir utilisé Sanipremium pour votre confort. **

Le contrôleur sort des sanitaires, rajustant sa casquette et fourrant sa cravate dans son veston côtelé. Il finit sa ronde, — pas un bruit, pas une âme — dans les couloirs des wagons-studettes. Par les fenêtres, la vitesse rend le paysage à peine distinct. Quelques bâtiments en ruines peut-être, le train fonce dans la nuit. Et l'homme aux colts — à présent cachés sous la grosse gabardine de l'uniforme de la Transunion company — semble s'y enfoncer avec lui.

 Arrêt prévu dans 15 minutes. Ceci est une arrêt de service. Les passagers ne sont pas autorisés à descendre.

Le haut parleur répète son annonce puis se tait. Une femme accoudée à la fenêtre admire l'aube naissante en grignotant une pomme. Nous approchons de Riga, mais les voyageurs n'en verront rien, la ville souterraine ne se révélera que trop brièvement, lorsque le convoi empruntera l'interminable tunnel intrinsèque au réseau ferroviaire de la ville. L'affaire de quelques secondes.

La femme fait signe au contrôleur.

— Madame ?

— Combien de temps avant de repartir ?

— Une quarante-cinq minutes, Madame.

— Votre voix est différente, êtes-vous enrhumé ?

— Non madame (la peur des microbes est omniprésente), une simple gêne due aux variations thermiques induit par la climatisation, je vous rassure.

Je s Je souris de toute mes dents. Je n'ai jamais été bon dans l'imitation de voix, une tare dans ma profession j'en conviens, et de toute manière, je ne supporte pas ces gadgets, qu'on positionne sous la gorge, et qui vous imiteraient Maria Callas mieux qu'elle-même entamant Carmen. J'ai des principes. Discutables, mais des principes. La femme semble se détendre à l'écoute de mon explication simplissime (ce sont souvent celles-là auxquelles on accorde le plus de crédit).

Je lui avais donc déjà parlé. Ma tête ne lui revient pas, heureusement, les bourgeois ne font jamais attention aux serviteurs. Une chance. Enfin, s'il on veut. Je m'étais tout de même préparé au rôle. Une mission de routine, si j'en crois mon employeur. Dans 12 minutes à présent, j'aurai rempli ma part du contrat.

La fille sourit. Elle est loin d'être laide, mais je ne batifole jamais durant le travail. Ça déconcentre.

— Vous avez des yeux magnifiques, si je puis me permettre.

 Bon sang, mes lentilles ! Comme quoi on peut avoir rouler sa bosse et faire des bourdes de jeunot ! Ne me reste qu'à ravaler mon orgueil, et déglutir bien à fond, le glaviot de la suffisance me restera sur l'estomac : une leçon pour un vieux chnoque. J'avale et réplique en imposant à mon buste récalcitrant une légère courbette.

— Madame est trop aimable.

Ses dents blanches attaquent la dernière face charnu du fruit (il doit valoir à lui seul le prix de toutes mes balles) tandis qu'elle se tourne de nouveau vers le panorama. Le contrôleur est déjà oublié. Et c'est tant mieux. Il faut que je me magne de rejoindre le wagon de queue.

Le contrôleur salue un passager, puis un autre tout en progressant vers la queue du train. Le convoi ralentit et le haut parleur reprend son monologue impersonnel. Les couloirs se vident, il est bientôt l'heure du petit déjeuner, chacun s'apprête à rejoindre le wagon-restaurant, sauf quelques couples encore amoureux, à qui la bagatelle offrira de quoi se sustenter. Le train est à l'arrêt.

 J'ai peu de temps pour agir, mais l'effet de surprise jouera en ma faveur. Sur le quai, une douzaine employés chargent et déchargent vivres et matériel pour la poursuite de notre voyage.

Ma cible est forcément de ceux-là.

Je peux éliminer les trop vieux et les trop jeunes, il m'en reste donc trois : Un grand type maigre comme une chèvre à traire, de peau mate, un autre tout aussi malingre, au visage juvénile, roux comme un écureuil et ce troisième, d'India peut-être, le poil et cheveu d'un noir de jais, qui semble fureter partout. Montre-moi ta figure, mon gaillard, — c'est toujours le visage qui parle —.

Les hauts-parleurs annoncent le départ imminent du Transunion 10 222. Prochaine destination, Barentz. Les employés s'activent sous le regard du contrôleur en chef. Soudain, celui-ci s'avise qu'un des trois manque à l'appel. Le contrôleur salue les techniciens qui s'éloignent enfin du quai, sans avoir remarqué l'absence de l'un des leurs. Il remonte dans le wagon et, suspendu un instant à la porte du sas, se permet une moue satisfaite.

 Je me tiens coi dans l'embrasure de la Soute. Elle paraît immense car il n'y a aucun aménagement dans ce wagon, hormis des racks métalliques. Les douze cercueils sont empilés au fond, ils luisent sous l'éclairage blafard et tremblent un peu tandis que le train s'ébranle. Il me reste à dénicher notre invité surprise. Un salopard de terroriste ! Je penche pour le coffre de survie, dont j’aperçois la porte, où un homme plutôt fluet peut facilement se faufiler pour commettre l’irréparable.

Ce wagon doit se pressuriser, mais j'ai fort heureusement interrompu le processus, — les sécurités sur ce rafiot sont inexistantes — si la Soute se verrouille, je suis fichu jusqu'à Barentz et mon cadavre congelé sera du plus bel effet à l'arrivée ! I

Allons, il est temps de faire preuve d'efficacité, et mériter mon faramineux salaire ; moins de cinq minutes ne me séparent d'un conducteur alarmé et de l'envoi de désagréables commissionnaires.

Mais fait de fouine, me voilà brusquement nez à nez avec un gamin qui n'a pas quinze ans ! D'où sort-il celui-là ? Un des gosses sur le quai… il m'a bien eut ! Mais pas assez pour que je ne lui enfonce ma lame à travers la panse avant toute initiative de sa part.

Silencieux comme un chat John Doe.

Une très belle lame que voilà, au fil parfait, équilibrée par un des maîtres couteliers de Thiers, une ville étonnante, savez-vous, — médiévale —, sa ville basse vous plonge dans un passé qu'on ne peut plus guère imaginer. Une des dernières villes anciennes du monde occidentale. Sa coupole de verre la protège des miasmes et la population, pour la majorité, vit au fond des volcans de l'Auvergne proche. Assez de géographie. Le voilà qui saigne comme un cochon, ce morveux, grand bien lui fasse.

Vous pensez : Mais pourquoi a-t-il deux pistolets celui-là, pour faire au bout du compte le boucher avec son coutelas ? Vous aurez raison et tort, ma foi, les pistolets sont là pour détourner l'atten…

Mais ?

Étrange, je me sens défaillir comme une mariée au soir de sa noce et ce liquide poisseux qui s'échappe de mon sternum… ette odeur de chair brûlée ! Ce petit saligaud s'est rebiffé et m'a pointé avec un laser ! La vermine ! Un travail de novice dont je vais subir les conséquences durant des heures avant de finir par en crever. Où va-t-il à présent ? Parbleu ! Il court desceller les cercueils ! Libérer ceux qu'on nomme immortels, ces créatures irradiées, ces monstres ! J'entends qu'il y travaille déjà ! Il ne s'est pas attardé sur mon sort après m'avoir proprement immobilisé, c'était bien inutile, j'en suis réduit à la mobilité d'une moule. Je ne lui ferai pas le plaisir de hurler, bien que la douleur m'étreigne. Saloperie ! Je dois l'empêcher de commettre cette horreur.

Le contrôleur-assassin résiste à l'envie de tourner de l’œil et observe son agresseur, muni d'un chalumeau à particules, s'attaquer à l'un des cercueils plombés. Des cris déchirants s'en échappent à présent.

 Ce petit con a réveillé les morts ! Si ces monstres s'échappent dans la nature, c'est la catastrophe assurée. Non, c'est bien pire que cela, c'est la fin du monde ! On y échappera pas une deuxième fois. Il s'en est fallut de trop peu en Europe centrale, on a bouclé le territoire, contenu ces monstres, mais là… Évident qu'il compte les libérer sur Barentz, à n'en pas douter, un lieu névralgique, où toute la haute société, les grandes infrastructures, tout, absolument tout, est concentré. Une catastrophe.

— Tout ça à cause d'un petit con d'illuminé ! Croassai-je, au plus mal.

— Tout ça a cause de votre négligence !

 La femme à la pomme me lance cette accusation si près du visage et d'une voix si douce que j'ai cru qu'elle allait me céder un baiser. Le baiser de la mort, oui, peut-être pendant que ses élégantes bottines à talons étalent mon sang rouge et visqueux sur le sol de la Soute. L'alarme rugit à présent, je ne m'en étais même pas rendu compte.

 Qu'il est déplaisant d'éliminer un aussi piètre concurrent. Il est à point le pauvre, pour faire face à la grande faucheuse. Accordons-lui quelques mots avant son imminent trépas.

— Erreur fatale, très cher assassin de mes deux, vous êtes tout bonnement en train de claquer ! Vous avez baissé votre garde beaucoup trop tôt !

— 

 Me souffle-t-elle, accroupi à mon chevet, l'air navrée, tout en me présentant son compagnon, un automatique dont le canon me paraît indiciblement long, noir et… ortel. Puis elle passe son chemin sans s'attarder, sa frimousse pleine de mépris à présent, — elle connaît mon destin —, et s'approche au plus près des cercueils afin d'avoir une vue dégagée sur sa cible.

Le gamin n'a rien vu ni entendu venir, tout à sa tâche, car le métal n'a pas encore cédé, bien que les bêtes griffent à présent frénétiquement les parois de leur prison en poussant des hurlements abominables. Pour un peu on n'entendrait même plus le ronronnement du Byzantin. Je tente de la suivre des yeux, mais je n'y vois plus guère, mes yeux se voilent, ce qui n'est pas de bonne augure, assurément.

Un coup de feu retentit. Un seul.

 L'aurait-elle buté ? Une pro, cette salope. Je ris, et ça me fait mal aux tripes, mais je ris tout de même, tandis que la femme réapparaît et me lance joviale,

— Le patron se doutait d'une entourloupe, il faut plus d'un cow-boy pour sauver le monde, mon vieux !

Son coup de pied gratuit, inattendu et lâche m'arrache un cri de souffrance. Je regrette de ne pas avoir plus de retenue, mais bon dieu que ça fait mal ! Elle m'observe, se penche vers moi. Je me rétracte, anticipant le prochain coup qui ne vient pas. Repoussant une mèche de cheveux sur mon visage en sueur, elle ajoute, presque tendre,

— John Doe ! Tu as un sacré sens de l'humour.

Elle semble réfléchir, femme fatale, pour moi comme pour le gosse qui gît un peu plus loin, sans plus de cerveau dans ce qui lui reste de boite crânienne, et fronce ses charmants sourcils arqués en adobe. La queue de cheval lui va à ravir. Ce blond cendré aussi.

— Peut-être aurait-on pu trouver une manière moins désagréable d'entreprendre ce voyage, enfin, on ne décide pas toujours. Je pensais que l'attaque aurait lieu plus avant, en Eurasie orientale (E.O.R.), vois-tu… Comme quoi, l'intuition, c'est de la merde.

— 

 Il y a presque un semblant de regret dans sa voix. Elle aurait pu jouer au théâtre, me dis-je, cette garce est dans le rôle. Le rôle d'une fameuse diablesse.

Elle s'accroupit un instant, et me fixe,

— T'as vraiment des yeux magnifiques, John.

Puis ses talons claquent sur le sol de la Soute, y laissant quelques traces d'un rouge sombre tandis qu'elle s'éloigne, abandonnant les cercueils d'où s'échappent d'horribles cris démentiels, ainsi que le faux contrôleur dont la respiration se fait inaudible, enclenchant d'une main ferme la pressurisation du wagon avant l'arrivée de quelque agent ferroviaire ou autrement dit, des ennuis.

Le train 10222 victime d'une avarie mineur auto-réparée par la maintenance automatique et robotisée, abordent à présent à pleine vitesse le méga-pont reliant les deux presqu’îles de la mer Baltica et poursuit sa route, — dernière virée —.

 J'ai toujours rêvé de voir Sainte Sophie… Ah, John, John, je ne te remercierai jamais assez de ce magnifique voyage.

La femme, vêtue d'un léger kimono, prend ses aises, étendant ses longues jambes graciles sur un sofa aux coussins profonds qui trône dans un coin de sa cabine, une large fenêtre lui fait face. Confiante, elle admire les terres orangées et arides annonçant pour bientôt la belle Istanbul. Son rire sonore emplit la luxueuse cabine, une petite fortune l'attend pour somme toute un contrat bien facile.

 Les cercueils arriveront à bon port. Le laboratoire Mpharma, de réputation international, sera satisfait, et son directeur général, le hautain Mr Morco, ce connard fini en costume latex moulant impeccable, très très content — il l'était manifestement déjà quelque peu lors de notre précédente rencontre —. ces monstres, de précieux cobayes, lui sont indispensables s'il veut arrêter ce qui se prépare et devenir le sauveur de l'humanité… Quelle prétention ! En attendant, il me claquera probablement une prime rondelette, du genre un week-end à deux sur la lune, ou…

Les hypothèses et fantasmes fusent dans ce cerveau d'assassin mythomane, tandis que, funeste et silencieux, se détache en douceur du convoi le dernier wagon, celui de queue, celui qu'on appelle la Soute. Il semble déjà faire du sur place, alors que, aveugle, le Byzantin, tracté par son infatigable locomotive Tristar vient de s'engager sur les voies de la station terminus. Zoomez un peu, vous apercevrez déjà le minaret de marbre qui, hélas, perce la coupole depuis l'holocauste.
