Grey rétrograda dans le virage, passant la seconde pour aborder le passage à niveau, à la sortie du coude. Les pneus ripèrent sur la bande gravillonnée, et le gros pick-up tangua avant d'amorcer en douceur la centaine de mètres le séparant du passage à niveau de la D 188. Le feu clignotant s'alluma comme il regardait distraitement à sa gauche, et dans la foulée, la sonnerie de la barrière, — une vieille barrière oscillante à l'ancienne avec rideau métallique tombant jusque sur la chaussée —, le sortit définitivement de sa rêverie, stridente, comme toujours. Il s'était souvent demandé si on l'entendait des patelins alentours. De sa cabane, elle était inaudible, mais sa destination se situait encore à une bonne douzaine de kilomètres de là. Et puis les bois atténuent les sons, se dit-il, une fois de plus, songeant à toutes les fois où il avait fait beaucoup de grabuge, — Un sacré barouf même —, sûr.

Il gloussa tout en stoppant le véhicule au pied de la barrière rouge et blanche. Des caténaires tissaient leurs toiles au-dessus de la chaussée en mauvais état. Un petit portillon muni d'un écriteau noir et blanc défraîchi invitait les piétons à ne pas traverser sans regarder et encore moins si la barrière était baissée. Planté juste là, un panneau déglingué et rouillé prévenait qu'un train peut en cacher un autre, alors que quatre rails luisants, composant la double voie, coupaient la route devant lui. Il s'agissait de lignes à grande vitesse mais de nombreux trains de marchandises l'empruntaient encore. Si c'était cette seconde catégorie que le signal assourdissant annonçait, il en avait pour quelques minutes à patienter, ces convois-là étaient longs comme un jour sans pain. Il souffla sur sa roulée, et aspira une bouffée de tabac brun. Du tabac à pipe, comme celui que fumait son vieux Pa. Ce salopard.

Grey cracha par la fenêtre cette bile qui lui remontait le long du gosier dès qu'il évoquait ne serait-ce que l'ombre de Georges Timber.

La sonnerie s'éternisait. Le clignotant battait la mesure. Un fracas sortit des enfers résonna le long des rails et s'amplifia sans que rien ne paraisse ni de droite, ni de gauche.

Merde, marchandise, jura Grey, jetant malgré lui un œil sur l'arrière du véhicule. Le hard-top noir, bâché, goûtait encore d'une averse récente, à part ça, rien derrière.

Pourquoi y aurait-il quelque chose ? Putain, mon vieux, cool ! S'enjoignit-il, balançant son mégot par la vitre entr'ouverte d'une vigoureuse pichenette.

Le vacarme lui vrilla les oreilles et la locomotive fit enfin son apparition, par la fenêtre du conducteur, suivie d'une ribambelle de wagons, qu'il identifia bientôt, tandis qu'ils défilaient devant lui, au passage à niveau. Il y avait une majorité de citernes, transportant ciment, et autre cochonneries de la cimenterie proche, mais aussi pas mal de plateaux, bourrés à craquer de voitures clinquantes aux chromes brillants, — pas une rayure —, et il se remémora cet abruti de Willy Surgette, le type qui rangeait les caddies, au supermarché de Cheule-sur-loing.

Ouais. Celui-là ne rayerait pas son pick-up deux fois.

Un sourire chafouin étira les coins de sa bouche en une grimace mauvaise. Greg aurait été bien peiné de savoir qu'il ressemblait à cet instant comme deux gouttes d'eau à son vieux pa.

Le convoi s'éternisait. Quelques plateaux encore, avec des fourgonnettes, et d'autres, gris, entièrement fermés.

La queue de peloton, espéra-t-il, sans oser se rouler une nouvelle clope afin de ne pas s'attarder dès que les barrières lui permettraient le passage.

Les vibrations se répercutaient dans la camionnette, faisant trembler le toupet de plumes de faisan accroché autour du rétroviseur central.

Je pourrais perdre mes dents à force, rigola-t-il nerveusement, au moment où le bruit des roues glissant sur l'acier s’estompait enfin, laissant derrière lui une dernière locomotive accrochée en queue de convoi. L'alarme se tut et Greg respira d'aise, lorsque ses oreilles cessèrent de distiller ce sifflement plutôt sinistre à la longue.

Les barrières semblèrent hésiter, trembloter, puis dans un effort visible, accompagné d'un grincement de poulies caractéristique de ces vieux matériels presque centenaires, elles se levèrent d'un brusque mouvement vertical, et claquèrent dans leur logement comme des hussards au garde-à-vous.

Le pied sur l'accélérateur, Greg sollicita le pick-up et celui-ci engagea ses deux pneus avant sur les rails, en douceur, puis les quatre autres. Le poids — imposant— du Chevrolet fit craquer les traverses de chêne.

C'est ce moment que choisit la barrière du passage à niveau de la D 188 pour faire un caprice, — ou une petite farce — à ce bon vieux Greg.

L'avertisseur sonore du passage à niveau se reprit soudainement à hurler de plus belle, et le feu rouge, celui situé de l'autre côté des voies, se mit à cligner de l’œil de sa grosse pupille rouge unique.

Re merde, est tout ce qui vint à l'esprit de Greg, et il s'apprêtait à engager la marche arrière lorsque dans un double — blang ! — retentissant, les barrières bicolores se refermèrent sur lui.

Merde, — décidément, son ciboulot tournait à plein régime, coté vocabulaire, — qu'est-ce qu'on est censé faire dans ce cas-là ? Se demanda Greg dans la fraction de seconde suivant la fermeture du passage à niveau.

Il souleva le bord de son chapeau, un antique Stetson, cadeau de sa femme. Oui, bon, il avait été marié, personne n'est parfait. Mais ça lui avait vite passé. Il avait d'autres choses à foutre dans la vie. Ouais.

La sueur perla à son front tandis que la sonnerie lui vrillait le crâne et le feu continuait ses clignotements obscènes.

Saloperie, éructa-t-il tout en portant son regard sur le plateau couvert du pick-up, puis sur la barrière dont les chaînes métalliques achevaient de cliqueter comme des dents cherchant à mordre.

Il appuya sur l'accélérateur, inquiet, mais pas trop, parce qu'il disposait d'un peu de temps avant l'arrivée d'un autre convoi, sûr, et puis il suffisait qu'il avance de quelques mètres, une dizaine, à vue de nez, alors… ais chose inhabituelle, s'il en est, le vieux pick-up cala.

Greg émit un râle, frappa du poing le volant. Il y eut une secousse. Une secousse qui ne venait pas du coup qu'il avait porté, non, non.

Il se retourna encore une fois pour zyeuter le hard-top. Rien n'avait bougé. Pourtant, il sentit des vibrations récurrentes, qui ne venaient pas non plus de la ligne de chemin de fers.

Putain de merde, le gamin s'était réveillé !

Greg suait à grosses gouttes à présent, et s'il ne paniquait pas encore, malgré la garce de loupiote et le zinzin hurlant dans ses oreilles, il craignait que le coffre ne résiste pas éternellement aux assauts d'un môme aux abois. Le vieux coffre de son pa.

Un flot aigre envahit sa gorge. Ce sale vieux Georges. La ceinture de Georges, les grosses paluches pleine de cambouis, l'odeur du cambouis. Ouais.

Greg bascula dans un ailleurs qu'il connaissait par cœur, faits de brutalités, de mots qui crèvent les tympans bien plus sûrement que ce diable d'avertisseur, et de coups d’œil lubriques bien pires que ce feu clignotant. Oh oui ! Rouges et palpitantes, ses pupilles étrécies le reluquaient sans cesse, et lui, lui, avec son cœur proche de l'arrêt cardiaque, proche de mourir de peur cognant dans sa poitrine, courbant l 'échine sous cet œil rouge, celui de son Pa, — le blanc en était tellement injecté de sang qu'il en était vermeil —. Ouais. Son souffle sur sa nuque. Son souffle empuanti d'alcool et de malice. Sa bouche s'emplit de miasmes, relent de son dernier repas, ils lui brûlèrent les joues.

Greg reprit conscience, les deux mains livides agrippées au volant comme un noyé agrippant une bouée dans la tempête.

C'était une sacrée tempête, et elle sortait tout droit du passé. Un passé qu'il avait décidé de faire taire. Il y a longtemps. Et il y était parvenu. Tuer ce passé qu'il abhorrait, où il n'était plus Greg, le gars qui paie un coup, au troquet du coin, le gars qui sait bosser dur, même le vendredi soir, le gars sur qui ses rares potes peuvent compter, pour une murge, ouais, mais aussi pour un tas d'autres choses. La chasse aux afro, le week-end, dans l'ancienne usine devenue squat, ou la casse dans les beuteries, une bonne branlée à ces saletés de gris, pour sûr, on pouvait toujours compter sur Greg. Il emmenait la batte de son Pa.

À la commissure de ses lèvres d'où le sang s'était retiré sous l'effet de ces ruminations, un filet de bile, vert et visqueux coula lentement jusqu'à son menton, où une fossette en dévia la trajectoire. Le glaviot alla s'écraser sur une chemise qui avait certainement connu des jours meilleurs. Il y a longtemps.

Greg tenta de remettre le contact, mais la camionnette ne voulut rien entendre, et après avoir toussoté comme un tuberculeux, retourna à son état inerte.

La clé de contact se balançait mollement.

Greg l'avait lâché. Il n'arrivait plus à avoir les idées claires avec tout ce boucan, il n'arrivait plus à détacher les yeux de ce feu flamboyant qui le narguait, sûr, qui se moquait de lui.

T'es bon a rien, sale petit con, vient ici, lui disait son père, frappant doucement du bout de sa batte sa large paume graisseuse. Vient là mon petit lardon, dépêche !

La cabine dansait un peu à présent, le gosse devait avoir trouvé la clé à griffes dans le coffre et essayait d'entamer le bois. La caisse était cerclée de bandes d'acier, il allait en chier, le môme, pensa Greg, esquissant un sourire étrangement doux. La moiteur de la cabine ramollissait son cerveau, ou…

Bon sang, faut que je sorte de là, grogna-t-il à haute voix, prenant peut-être seulement alors conscience que chaque seconde tenait du sursis et qu'il ne durerait pas. La faucheuse n'est pas une reine de patience.

Pourtant, il ne put décoller son cul du siège, — un vieux siège en velours grenat qui puait le chien mouillé, bien que Buki soit mort depuis belle lurette —. Les plumes de faisan voletaient devant ses yeux. Le passé avait décidé de le titiller, de gratter sa peau jusqu'à l'os, et de régler ses comptes, pourquoi pas ? Le passé est un vieux monsieur chagrin, il n'aime pas les seconds rôles. Ouais.

Viens petit puceau débile, viens voir ton pa ! susurrait le vieux salopard. Georges Timber avait deux passions dans la vie, et seulement deux : la bibine et sa batte. La première le nourrissait depuis son adolescence, et finirait par avoir sa peau, — ce ne serait que justice — et la seconde avait cessé son utilité première dès le collège. Le vieux n'avait pas passé les barrages éliminatoires de l'équipe de morveux qui la composaient, dont bien entendu, il faisait partie, et il n'avait jamais remis les pieds sur un terrain de base-ball, — il se contentait de fixer l'écran de télévision, beuglant des commentaires insanes, en vidant sa dixième ou onzième canette — mais il s'était vengé à sa façon, ça ouais.

Greg rugit, empoigna la portière et l'actionna frénétiquement.

L'avertisseur sonore du passage à niveau s'éteignit.

Le silence lui brisa les oreilles pi que le tintamarre. Le silence l'agressa, trou noir, vide intersidéral, et le renvoya vers cet autre, plus oppressant, plus glauque, plus sale, tellement sale, au fond d'une petite chambre cradingue et moche où un autre garçonnet pleurait et tapait contre la porte pour sortir.

La ceinture de sécurité passée de travers vint entraver ses efforts pour pousser la portière de l'habitacle qui n'avait pas l'air d'être enclin à le laisser s'échapper.

S'échapper ! Si le gosse sort du coffre, il va courir aller me dénoncer !

Cette idée donna un regain d’énergie à Greg, et il parvint à s'extraire de cette glu infâme que personne — non, personne — n'aurait osé appeler souvenirs. Sans parvenir à détacher sa ceinture, il l'emberlificota après le levier de vitesse, — un levier de frimeur, avec une boule en corne de bœuf au bout —, et passa la tête puis le buste par-dessus pour se dégager. Il remontait les jambes quand le pick-up se mit à hocher sur ses vieilles suspensions grinçantes. L'homme jeta un regard dans son rétroviseur et vit que la bâche ondulait.

Putain de merde ! putain… e petit enfoiré avait réussi à casser la caisse en bois ou pas loin !

Greg termina de se démener pour se libérer de la ceinture de sécurité. Il entendait à présent des cris étouffés provenant de sous la bâche du Chevrolet. Il ne distinguait pas de mots, mais il devait en avoir à raconter, le mioche, sûr.

Heureusement, la route, vide à perte de vue vers l'arrière, se dévidait à l'avant encore plus désertique. C'est bien pour ça qu'il avait choisi ce coin pour ses petites affaires, hein. La ville la plus proche se situait à vingt-cinq bornes et les quelques villages pas loin des quinze, pas de curieux pour venir l’emmerder quand il s'amusait un peu.

Il s'aperçut alors que le feu de signalisation le regardait d'un œil morne ; lui aussi s'était éteint.

Bon, si ça se trouve, Y a même pas de convoi qui passe à c'te heure, se rassura-t-il, si ça se trouve… et il frappa la portière de ses pieds enfin libérés.

Les barrières étaient toujours fermées.

Autant ne pas prendre de risque, hein, se barrer d'ici, fissa. Mais avant, assommer de nouveau le gosse, et le ligoter, cette fois. Il n'avait pas pris le temps, tout à l'heure, dans le parc. Il avait juste sauté sur l'occaz, comme disait son Pa.

Sauter sur l'occaz, ouais.

La ceinture, la batte, un gosse épouvanté et sans défense, c'était ça la bonne occaz de son Pa.

Il eut un haut-le-cœur. Salopard.

Lui, Greg, il avait essayé de réparer ça. Toutes ces putains d'années, il avait essayé, dieu seul savait combien ! Ouais. Il avait essayé.

Tous ces gosses malheureux, qui traînaient, dans les rues, dans les parcs, dans les bars, sans rien, ils leur avaient facilité la vie, oh oui, — Greg, le libérateur —.

La vie est une trop grosse merde pour qu'on en sorte tout seul, pas vrai, alors ce bon Greg, il aidait tous ces ratés, ces malheureux, ces moins que rien, ouais, Greg, c'était un gentil gars, tous ses voisins vous le diront, il vient réparer les robinets qui fuient, ramènent les chats écrasés à leur propriétaire… Un bon gars. Un peu simple, peut-être, ou un peu bizarre, ce regard qu'il a parfois, comme si ses yeux devenaient rouges, drôle de gars, bon, mais pas méchant, tous ses — rares — potes le diront. Et même la maîtresse d'école, au bout de la rue, le dira, ça oui, l'an denier, il a réparé sa vieille Ford pour qu'elle rejoigne sa vieille Ma à l'hôpital. Ouais.

Greg se permit un coup d’œil à travers le haillon arrière, et la vitre lui renvoya exactement ce qu'il ne voulait pas voir.

Le poinçon d'un tournevis dépassait de la bâche qu'il venait de trouer et au bout de ce petit outil, somme toute anodin, une petite main palote, et moins anodine, serrait un manche de bois bien trop grand pour elle.

Le saligaud, nom d'un chien ! Et ce disant, Greg s'extirpa enfin de la cabine du pick-up.

Presque.

Il avait oublié la ceinture de Pa.

Cette bonne vieille ceinture en cuir, avec son chapelet de rivets de cuivre qui la bordait, bon, à vrai dire, y en restait plus beaucoup de ces clous, mais le cuir, lui, avait bien vieilli, plus souple mais toujours aussi solide.

Et là justement se trouvait le hic.

La ceinture de Greg, qui ceignait son jean, il la portait toujours, — par habitude sans doute, certes pas par nostalgie — était retenue dans il ne savait quoi, un ressort de siège, ou sa manette de réglage ? En tout cas, le piège fonctionnait à merveille, le maintenant mi-pendu, mi-assis, au niveau du marche-pied de la camionnette. Il gesticula dans tous les sens, espérant en venir à bout.

Il regarda la voie ferrée, vide, et sut que de toute manière elle n'était plus sa priorité.

Le gosse était sa priorité. Ouais.

Il suait comme un phoque, mais la peur était passée à présent, c'était plutôt la colère qui le dominait, sourde et tenace. Une colère qui lui bouffait les tripes et fit encore remonter cette bile acide et dévastatrice le long de son œsophage à l'évocation de cette putain de ceinture paternelle.

Il n'eut pas le temps de s'appesantir.

Un cri de libération lui parvint du hard-top. Un bras malingre puis une tête rousse pointèrent de la bâche noire enfin éventrée. On aurait dit un jaune d'œuf sur une mer de goudron. Le gamin ne perdit pas une seconde à jeter un œil vers la cabine, mais rampa maladroitement sur le plastique glissant, les cheveux collés au visage par du sang à moitié séché. Il pataugeait sur le hard-top, en hurlant comme un forcené, bien plus fort qu'on eut pu le penser pour un si maigre gabarit, et Greg se mit à hurler de concert, bien qu'il ne fût pas en mesure de voir le gamin, coincé comme il l'était le long de la portière avant.

Par contre, il vit et entendit trois choses.

Le feu de circulation clignota de nouveau de cette couleur si semblable au sang.

Le signal sonore entama son chant de la mort.

Et un bruit, comme un sac de son qu'on jette sans précaution, se répercuta depuis l'arrière du camion. Des jambes nues de gamin se mirent à courir, non, rendons justice à ce gosse, volèrent littéralement au-dessus de l'asphalte trempé par l'averse. Greg étira la tête aussi loin qu'il put et aperçu le gosse. Celui-ci dérapa, se redressa et cette fois, s'éloigna pour de bon à la vitesse d'un daim invité à une chasse à courre.

Greg vit cela, mais surtout il vit venir face à lui deux feux jaunes qui le fixait droit dans les yeux, les feux d'une locomotive, — des phares ronds, deux soleils, qui grossissaient, grossissaient —, ils allaient bien finir par brûler Greg. Ouais

Et comme pour confirmer cette ultime supposition, le sifflet du convoi retentit par-dessus le vacarme de l'alarme sonore, tandis que les yeux jaunes ne cessaient de grossir.

Les yeux de Pa, songea Greg à la fois épouvanté et hypnotisé, alors que la locomotive, encore à pleine vitesse malgré ses freins hurlant sur les rails brillants et lisses explosait la cabine du pick-up, écrasant son conducteur dans des sanglots de ferraille.

Ouais.
