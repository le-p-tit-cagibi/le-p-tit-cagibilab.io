---
title: Repas de famille
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "La famille, ça a du bon. "
epub: true
pdf: true
draft: false
---
Repas de famille.

Ça y est : Ils sont tous là…

En bout de table, — la place du chef de famille —, l'air d'un oiseau tombé du nid, le regard noir et sévère de papy Jean s'attarde sur les protagonistes de cette journée. Dans sa tête, je vois les petits rouages qui s'activent et dressent l'inventaire — points positifs, points négatifs —, et je crains que chacun de nous n'en prenne pour son grade. Il a toujours eu beaucoup trop d'exigences : nous ne sommes que ses enfants !

À côté de lui, une chaise vide : c'est la place de mamie Luce qui virevolte, s'échappe loin de nous pour mieux revenir sans pour autant rester À chacune de ses apparitions, un plat dans chaque main, elle sourit, émue, à sa dernière petite fille, coincée dans sa chaise haute, tout juste face à papy. La petite babille et bave comme un vieux chien devant un os. Mamie Luce lui enfourne une cuillerée de sa sardinade maison — dont le secret est bien gardé —, et embrasse toute la tablée de ses yeux clairs et humides. Bien longtemps qu'elle n'avait pas réuni toute sa nichée dans sa grande maison.

Un éclat de rire déchire l'atmosphère embuée un instant de nostalgie : Davis n'a pas résisté à une énième délirante histoire drôle contée à forces de gestes et grimaces par mon jeune frère Étienne, incorrigible, agaçant, (voir plus si affinités), selon Angel, son actuelle compagne, dont le regard enamouré ne reflète pourtant pas ce jugement sévère. Le doux géant (un mètre quatre-vingt-dix pour cent-dix kilogrammes), tout à sa farce, ignore royalement sa dulcinée et poursuit son récit, encouragé par des auditeurs subjugués, dont Bruno, l’aîné de notre fratrie, qui tripote discrètement sa montre (à peine arrivé, on a toujours l’impression qu'il s'apprête à repartir). Devant un tel succès, — il a toujours eu besoin d'un public —, même et surtout lorsqu'il fait encore un de ses mauvais choix, Étienne enchaîne avec une blague un peu salace, accrochant au passage l'oreille distraite de Marc, mon beau-frère, étreignant fiévreusement dessous la table la main d'Annelise, son épouse, ma petite sœur, la benjamine, dites — Sœurette —. Une indécrottable fée amateur, à l'imagination débordante, aux réparties lunaires, écrivaine lutine, mutine, et bavarde. Oh combien ! Elle ne cesse de dégoiser depuis qu'ils ont débarqué de la banlieue parisienne avec leur deux gamins, présentement attablés avec leurs cousins et cousines dans un recoin de la salle à manger, qu'on nomme, un peu précieusement, —salon— tout près de la grande cage des perruches qui n'osent, dans le brouhaha général, plus ouvrir le bec.

Et Annelise qui n'en finit pas de jaboter.

Ah, zut ! c'est à moi qu'elle s'adresse, la belle chérie, et me voilà pris en flagrant délit d'inattention, trop étourdie par cette assemblée joyeuse et indisciplinée.

— Pardon, j'ai été distraite, lui avouai-je sans ambages.

Elle ne s'offusque aucunement, hoche la tête d'un air entendu, et toute son ample crinière à frisottis accompagne le mouvement, puis elle alpague aussi sec un autre interlocuteur ; Davis en l’occurrence. Le malheureux tente un stratagème, piquant du nez dans son assiette, où un filet de saumon fumé alléchant se prélasse sur un toast doré, mais à tout dire, il n'a que très peu de chance que mademoiselle la pie lui fasse grâce. Mieux vaut ne pas y compter.

Que dit-elle au juste cette pipelette ? Aucune importance. Une sororité de longue date nous tient au-delà de toute implosion. Ma mère m'adresse un clin d’œil de connivence, voyant à ma mine que je sature de tout ces mots jetés à tous vents, et puis, je me souviens, — oui, laissons-la parler, nous avons bien failli la perdre — Et sa voix se fait musique, sans que je distingue le sens de ses paroles, absorbée par cette si exubérante tablée que j'aimerais tant photographier, ou plutôt imprimer,

non, — graver, à tout prendre — dans ma mémoire.

De manière à ce qu'elle y reste vivante maintenant et toujours.

Je me prends un crachotis de tartinade sur la figure : La mouflette fait des bulles et sa mine friponne ne me laisse aucune illusion, elle me vise tout spécialement ! Les enfants sentent quand on n'en veut à leurs parents, les conflits non résolus, les non-dits, quand quelque chose dans l'air électrifie la moindre parole, le plus petit geste ; et ils vous le font payer.

Placidement, je torchonne mon visage gras, jouant l'indifférence, tout en admirant la dextérité de mes neveux âgée de huit à douze ans lancés dans un concours de jeter de tomates cerises assez époustouflant ! L'un vise adroitement la bouche de l'autre et hop ! L'autre tend un four impressionnant, gobant le petit projectile. Ils rient… Comme seuls rient les enfants, à gorge déployée, sans artifice, leurs petits corps tressautent tant ils rient.

J'essaie sans succès de me souvenir à quel moment j'ai cessé de rire comme cela.

On me pousse du coude,

— Tu n'aimes pas les chats de toute façon, avoue !

Ça sent le reproche à plein nez. Qui m'agresse ?

Ah ! Cette voix, moqueuse, et ces yeux-là, je les connais : ils sont miens — enfin, presque —.

Ma fille me taquine, défendant la cause féline, sujet d'une lointaine conversation dont me voilà prise à partie. Ma fille… Hier, elle marchait à peine, tremblant sur ses petites jambes toutes potelées, sa menotte serrant la mienne.

Où sont passées toutes ces années ? Le collège, le lycée, la fac, mon divorce ?

Mon silence prolongé l'intrigue, elle me dévisage interrogatrice À dire vrai, elle ressemble plutôt à un ange facétieux qui se ferait passer en catimini pour mon enfant et consoler la vieille dame que je suis devenue. J'ai toujours eu le sentiment d'avoir mis au monde une fille que je ne méritais pas. Je lui en suis hautement reconnaissante. Elle m'a réconcilié avec l'idée que j'avais de la maternité. Je n'ai toutefois pas le temps de méditer plus avant.

— Elle t'a coupé le sifflet dis donc ! Sélina, assise à croupetons devant le chat de la famille, sujet de la démonstration (et qui le sait à voir comme il se pavane crânement) se moque de son ex-belle-sœur (une manie de mon frère, il invite ses ex à chaque réunion familiale, et aucune n'a jamais la bonne idée de refuser).

Je m'ébroue de ces satanées pensées nostalgiques : ce jour m'appartient ! et tente une réplique, instantanément interrompue par notre chère pipelette qui tend le cou et se lance dans une joute verbale, fait une queue de poisson à mes trois mots balbutiés, me double et me sème : le chrono est imparable — J'en reste comme deux ronds de flans affalée sur ma chaise — Sacrée Annelise !

À voir ses gesticulations, que tente bravement d'imiter sa petite dernière, il est probable qu'elle soit partie en quête d'une de ses explications abracadabrantes. Elle est un peu fée, je vous l'ai confié (ne le répétez pas, certains pourraient nous croire un peu dingue, dans cette famille) mais c'est aussi une analyste, c'est involontaire (chez elle, tout est involontaire). On pourrait supputer une influence génétique : Que nenni ! Notre papy Jean est un rêveur, sa fille une décortiqueuse : chacun son truc. Une chose cependant les rassemble, ils sont têtus comme des mulets.

Au pied de Marc, le petit avant-dernier de la famille, la bouille ronde, portrait de mon frère Étienne quand il était bambin. De ses menottes dodues, il tire sur le gilet avunculaire, y apposant quelques empreintes grasses, quémandant (humble déduction, car son langage en est encore aux balbutiements) une de ces appétissantes figurines en chocolat qui orne la table. Il en est très friand.

Un gourmand.

La gourmandise nous rassemble. Nous sommes gourmands les uns des autres. Assurément. Parce que certains sont savoureux et d'autres plus piquants, mais tous, —tous —, me procurent une joie immense lors de nos — grands rassemblements — comme dit Bruno, qui a sans conteste le sens de la formule. Dommage que mon frère aîné, Bertrand soit absent, il aurait sans doute poussé dans ses retranchements ce grand introverti, toujours sur la défensive qu'est notre cadet. Et nous l'aurions peut-être entendu rire. Son rire me manque. Zina, son épouse, n'en tire pas un son plus haut que l'autre, tout en retenue. C'est un peu triste. Zina est toujours un peu triste. Pourquoi ne m'en étais-je pas soucié avant ? Je les observe tous les deux, si identiques, si prévisibles, toujours impeccables, et pourtant, aujourd'hui, je sens que le miroir aux alouettes se craquelle de toutes parts. Bruno réajuste une fois de plus sa montre. L'heure tourne, inexorablement. Et notre aîné, qui a eu un empêchement — sa femme nous a jeté un message succinct, au dernier moment, pour s'excuser —. Sans excuse à vrai dire. Qu'est-ce qui ne va pas chez eux ? Personne ne sait. Une parenthèse. Dans cette famille, tout finit par se savoir. En attendant, cette absence fait écho au regard vide de papy Jean, il a toujours haussé Bertrand sur un piédestal.

— Attention au diabète ! S'écrie ma sœur alarmiste et extrême comme de bien entendu, à la vue de la petite suçant une mignardise qu'elle étale comme une crème anti-ride sur sa jolie frimousse. De sonores protestations font taire la rabat-joie. Durant une minute, —peut-être moins —, Anne-lise baisse le nez, coupable, embrassant son bébé, calé sur ses genoux, sage et silencieuse, imitant sa mère.

Toute la tablée savoure cet instant. Les plats s'entrechoquent, les verres tintent, la parole est aux couverts crissant dans les assiettes, à la porte-fenêtre dont le battant cogne sous une petite brise.

Mais ne vous faites aucune illusion : c'est un moulin à paroles, un brin de vent et la voilà repartie ! Son tendre époux, le sait déjà et arrondit le dos, près à l'impact.

Annelise accroche mon regard,

— Qu'est-ce qu'elle me raconte ?— me dis-je, et je n'entends rien, ignorant même jusqu'à sa présence physique, mais sa voix me fait du bien. C'est un rayon de soleil. Ma sœur est un soleil, et il brûle, c'est vrai, c'est sa nature, il ne le fait pas exprès, alors mieux vaut porter un chapeau à bords larges, pour s'en protéger et pouvoir en profiter, — sans avoir mal —. Mais il est impossible de s'en passer. Ce serait toujours l'hiver dans la famille.

Revoilà maman Luce avec un plat qui réconcilie les bavards et les muets : poulets grillés. L'odeur, indescriptible donne inexplicablement très faim alors que chacun s'est déjà gavé de mises en bouche faites maison. D'un bel ensemble, nous tendons nos assiettes.

Je passe les plats à papy Jean car trôner au bout de la table n'a pas que des avantages : On peut tout aussi bien s'y retrouver affamé, sauf s'il on a des bras de trois kilomètres comme Étienne, coincé entre ses deux jeunes femmes, la nouvelle et l'ancienne, — évidemment lui n'a pas de problème de ravitaillement (sans jeu de mots)—, mais mon beau-père, assurément.

Bel élan de culpabilité en somme, cette amabilité soudaine, me dis-je un brin condescendante envers moi-même, décidément, rien ne me coûte aujourd'hui.

Louons ses efforts, beau-papa a sorti son barbecue (fabriqué maison), tôt ce matin, fendu des bûches juste à la bonne taille et préparé le feu juste ce qu'il faut, pour obtenir des braises parfaites, pour une cuisson à point. Il aurait bien aimé faire de même avec ses enfants, des gens juste comme il faut, parfaits si possible, et l'on ne peut que se sentir coupable quand on n'est pas à la hauteur des espérances de ses géniteurs. Il m'a supporté si longtemps en serrant les dents, quand adolescente, je lui rendais la vie impossible, que je ne peux que lui passer les plats et même le resservir — justice n'en sera que rendue —.

La viande sent si bon ! Ce sont les herbes du jardin qui la parfument, et me plonge dans ce passé désormais lointain. Pas besoin de drogue dure pour s'évader quarante ans en arrière, une branche de thym-citron et un brin de romarin suffisent !

Je fais circuler les plats, observant à la dérobée ce papy si maigrichon déjà, qu'il ne faudrait pas en plus le sous-alimenter ! C'est vrai ça, depuis combien de temps est-il si décharné ?

Pas le temps de m’appesantir, ma future belle-sœur Angel retient mon attention, tout là-bas, encouragée à se sustenter pour deux par mamy Luce, et qui s'y attelle de bon aloi, sans fausse modestie, désespérant que Gwendoline, très attendue et enfin trop à l'étroit pour apprécier encore le ventre maternel, ne se décide courageusement à découvrir le monde. En attendant, Angel s’empiffre de nourriture et de compliments sur sa grossesse, et sa bonne mine. Elle resplendit. Tout le monde applaudit. C'est tellement beau une femme enceinte. C'est vrai, Angel ressemble à une belle éléphante, se dandinant tout comme, les formes pleines, le teint rose, trompetant son bonheur à la face de tous. Elle n'a pas bu de champagne pourtant, mais qu'est-ce l'ivresse d'une liqueur, quand on a celle de l'amour, je vous le demande. J'aimerais que ce soit ma dernière belle-sœur. Pourquoi parle-t-on de l'inconstance des femmes ? Et les mecs alors ?

Soudain un déferlement de sons biscornus éborgnent nos oreilles.

— Qu'est-ce que cette musique infernale ?

J'inspecte rapidement les alentours. Le responsable de ce raffut campe dans le salon, et n'est autre qu'un des fils d’Étienne, qui joue sans vergogne à la console vidéo et nous abreuve de rifts discordants à défaut que nous le soyons de vin. Son père n'interviendra pas, l'autorité n'a jamais été son fort, il faut le reconnaître. Difficile pourtant de tolérer ce bruit qui couvre nos voix et les rend soudain lointaines, immatérielles. Pourraient-elles s'éteindre ? Pourrais-je un jour ne plus m'en souvenir ? Avoir oublié les intonations et manières de chacun de s'adresser aux autres ? Une angoisse m'étreint. J'aperçois la mine déconfite de Marc, mon beau-frère. Il n'apprécie pas plus que moi ces vociférations musicales, et papy Jean s'est rencogné sur sa chaise, mal à l'aise. Mamie soupire en servant les légumes.

Ah zut alors ! Les jeunes feront la révolution un autre jour ! Nous usons de cette majorité incontestable des adultes pour couper la machine à casser les oreilles. Protestations énergiques de l'adolescent : Je me félicite d'intervenir, nous aurions pu sans y prendre garde en faire un addict de ces folies nippones ! Nous en sommes quittes pour une franche bouderie qui passe inaperçue tant le

soulagement de la tablée se fait sentir : elle recouvre ses propres sons et musiques.

— Stop ! Il y en a trop papy, je voulais juste goûter, et Davis conduit !

Mais malgré ses contestations, voilà mon ange qui lèche avec gourmandise, comme un chaton, le verre de porto rosé, une délicieuse curiosité rapportée d'une villégiature par nos parents (ou grands-parents et arrière-grands-parents, selon qui les regarde). Un vin qui sent les fruits gorgés de chaleur, l'océan aux vagues coléreuses, les embruns salés qui vous battent le visage et qu'on respire goulûment, parce qu'on sait que ça ne dure pas — l'océan s'en va —.

Chacun savoure une lampée de soleil.

L’arôme nous enivre doucement, tandis que mon gendre se noie dans les iris clairs de Gabrielle, son ces deux lacs clairs dont on n'aperçoit pas le fond, — s'il existe—. Les amoureux…

Quand ai-je été amoureuse pour la dernière fois ? Mon cœur bat doucement en les observant à la dérobée. Ils sont si magnifiques dans leur amour tout neuf.

— Repus —

Par l'éloignement de chacun par rapport à la table, et le cumul des os de poulet, je m'aperçois que le temps a fui, et chacun parait plus détendu — ou distendu — par l'abus de bonne chère.

Ma mère lève le camp et, magie de ménagère, en un tour de passe-passe la table est débarrassée, puis embarrassée de même, avec force de gâteaux et de salade de fruits dont les parfums ravissent nos papilles pourtant déjà comblées.

Sœurette parle toujours, je ne sais à qui cette nouvelle diatribe s'adresse. Je goûte, plus que les saveurs et les odeurs de ce repas familial, le plaisir d'être emplie de leurs bontés à tous, de leur chaleureuse douceur d'être là avec moi, comme une couverture d'amour qui viendrait me protéger pour que je n'aie jamais froid. Peut-être est-ce là le bonheur simple d'être, ni plus, ni moins : Cette joie ressentie du bruit et du mouvement des autres, leurs gestes à peine esquissés, leurs paroles sans paroles (sauf qui vous savez…), ce tout qui parvient jusqu'au cœur et l'enfle. Je ne suis pas seule, et eux non plus. Nous nous appartenons, même si les uns m'agacent, même si les autres me fatiguent, et certains m'exaspèrent… Tous, je leur trouve mille défauts et je les chéris, ils sont eux, ils sont moi. Nous sommes ensemble.

— On y va maman ? Ces mots que je n'aime pas, ma fille me les murmure à l'oreille. Ses longs cheveux blonds sentent la prairie, sa peau de lune pâle sur fond de douce malice console mon cœur qui saigne. Elle regarde sa grand-mère, collusion transgénérationnelle — je suis la seule à ne rien comprendre assurément —.

— Partir ?

Pourquoi partir quand on se retrouve soudain pour un divin moment mieux encore que dans le ventre de sa mère ? songeai-je désenchantée.

— Partir ! — Mais oui ! bien sûr ! Il est tard, les jeunes ont d'autres obligations, des projets, des rendez-vous, des invitations, que sais-je encore ! Tout ce qui fait qu'à trente ans, on a jamais une minute à perdre alors qu'à cinquante, on en a tant qui sont si mal utilisées.

Cependant, aujourd'hui, réjouissons-nous, celles-ci ont été fort bien jetées par les fenêtres : heures, minutes, secondes tout y est passé ! Précieux instants où le microcosme familial est revenu se couler dans mon giron, temps jubilatoire ! Moment joyeux et un brin énervant qui transpire dans cette réunion familiale, à la fois sacrée et redoutée, où chacun s'agace et s'embrasse, se reconnaît et se dédie !

Et le voilà qu'il s'achève déjà ! Chacun bise une joue, un front, une bouche, en se reprochant de ne pas avoir dit la moitié du quart de ce qu'il avait à dire…

Vacarme de la migration de la fratrie, des chaises repoussées, des portes claquent, des petits-enfants rient ou pleurent, en remontant dans les voitures ; celle d’Étienne, une pittoresque guimbarde, où tous sont serrés comme harengs en caque, et ficelés avec grand soin par ma belle-sœur, ou encore la berline de Bruno, impeccable, que son fils a rejoint après avoir pris grand soin de secouer ses baskets crottées à l’extérieur, tout cela dans un même élan, tandis que papy Jean et mamie Luce se redressent, près du portail si grand, et eux, si petits, dans cette déferlante scène d'adieux qui est près de les engloutir. Ils saluent de la main, répondant à nos :

— Au revoir !

Par des :

— À bientôt !

Aux accents remplis d'espoir (faire bonne figure et faire fi de la tristesse !).

— Attention les radars, ajoute Jean, pragmatique, un large chapeau de paille masquant l'expression de son visage.

Luce ne dit plus rien. Elle tord entre ses mains son tablier à carreaux, comme nous tordons son cœur de nos départs insouciants, et elle nous regarde nous éloigner de la rue des Forges, aspirant à retrouver un peu de calme tout en le redoutant déjà ! Ma petite sœur reste encore, — Paris, c'est la porte à côté — la serrant dans ses bras, en jacassant bien sûr. Marc rappelle chiens et enfants égayés sur le trottoir.

Derniers coups de klaxon, les véhicules s'éloignent, les fenêtres ouvertes laissent échapper des chamailleries, et la voix de stentor d'Angel me parvient, tentant d'y mettre bon ordre.

Les mains sur les genoux, imperturbable, Zina branle du chef en direction des deux vieux, ratatinés sur le trottoir : elle me fait penser à ce petit chien en plastique, qu'on trouvait souvent sur les plages arrières des voitures dans les années quatre-vingt et qui hochait la tête à chaque mouvement de la voiture.

Les pots d'échappement crachent une fumée noire.

Je tourne machinalement le bouton de la radio, le silence à venir m'affole : il me faudra un temps d'adaptation, réintégrer ma vie, quitter la leur, jusqu'à la prochaine fois.

Par la fenêtre de nos voitures, nous agitons encore la main, et mon cœur aussi s'agite. Je cligne laborieusement des yeux, et emporte cette dernière image d'eux que j'aime.
