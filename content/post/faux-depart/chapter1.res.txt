Le bolide fonçait sur l'asphalte. La route défilait à toute allure, on n'apercevait à peine le 
paysage monotone à travers les vitres du véhicule.

La voie était libre.

À cette heure extrêmement matinale, la circulation s'avérait quasi nulle. La grosse cylindrée était 
à son aise tandis que les premières notes métalliques de blaphemous rumors prenaient possession de 
                                                         °°°°°°°°°° °°°°°°
l'habitacle aux doucereuses effluves de cuir neuf. La conductrice appuya un peu sur l'accélérateur. 
Le moteur ronronna d'aise.

Jaimie visionnait un film. La nuit était pour lui l'occasion de s'adonner à sa plus grande 
°°°°°°
passion ; le cinéma horrifique. Au volant de son quarante-quatre tonnes, affectueusement surnommé, 
Buffle, à approximativement quelques centaines de kilomètres de sa destination, l'homme prenait du 
bon temps. Il s'était décidé pour « Massacre à la tronçonneuse », un grand classique. Son gros cul 
calé confortablement dans le large fauteuil à suspension hydraulique, le regard vissé sur l'écran, 
il donnait l'illusion de se laisser conduire plutôt que de maîtriser son mastodonte monté sur huit 
roues. La cabine, rendue confortable par des ajouts et modifications personnels, tenait la dragée 
haute au meilleur intérieur de camping-car haut de gamme. Du petit électroménager, micro-onde, 
expresso, s'alignait dans de petites niches, à portée de mains. Le son, craché par un home vidéo 
dernier cri, vous arrachait des larmes lorsque la tronçonneuse entrait en action, et que les 
victimes commençaient à gueuler, voyant leur dernière heure arrivée. Jaimie s'immergeait 
                                                                     °°°°°°
totalement, il les tenait par les couilles, la chaîne d'acier tressautait sur les tibias et les 
cubitus ; que je te fais sauter une tête par ici, un bras par là… tandis que la remorque, quatre 
essieux, pleine ras la gueule de matériel de jardinage, chassait à peine sur l'arrière, malgré ses 
treize mètres cinquante ; Buffle la tractait comme s'il avait chargé un simple petit enfant. Les 
doigts dans le pif. L'homme sourit, il parcourait cette nuit son itinéraire préféré, un velours 
cette route, et pas un chat. Dès son entrée sur la RN4, sa vigilance s'était mise en mode sourdine, 
                                                   °°°
son index avait enclenché le bouton « on » d'un lecteur vidéo japonais tout neuf, une nuit de rêve 
pour l'étrenner. La route lisse comme une livre de beurre invitait à lâcher prise, ce que fit le 
routier sans se faire prier, à peine un doigt posé sur le volant, pendant que du sang giclait sur 
l'écran, et que les hurlements saturaient la cabine. Une nuit idéale.

Ted sirotait son café, accoudé au comptoir crasseux de la station. Il était d'astreinte sur cette 
bonne vieille RN4 ce soir, un mercredi. Un jour calme en général. Depuis dix ans déjà, il 
              °°°
appartenait à l'équipe d'entretien des routes de ce département. Un boulot en dents de scie, pas 
forcément palpitant, mais bien rémunéré. Parfois, il s'agissait de ramasser la carcasse d'un 
chevreuil passé au moulin à légumes par un véhicule, d'autres fois, des engins perdaient des 
morceaux de ferraille qu'il fallait évacuer, d'autres encore, c'étaient un gars qui avait salement 
plié sa bagnole et vagissait comme un nourrisson coincé dans l'habitacle, saucissonné par sa 
ceinture, et tentait de s'extraire sans succès de la caisse ratatinée. Plus rarement, heureusement, 
il était question de faire une cueillette bien funèbre, avec un petit sac plastique spécial, comme 
collecter des bouts de barbaque éparpillées dans une berline familiale réduite à la taille d'un pot 
de yaourt, où trônait une tétine, un ours en peluche, dans une mare de restes sanguinolents non 
identifiables. Ouais. Mais c'était rare. La plupart du temps, Ted parcourait ses cent kilomètres de 
route peinard, le tronçon 50, qu'on l'appelait, après ce premier arrêt à la station, histoire de 
s'enfiler un expresso bien serré, ristretto, même certain jour. L'agent de surveillance des voies 
publiques se massa machinalement les tempes, le nez au-dessus de son café brûlant. Mince, ses 
migraines lui fichaient la paix depuis un bon bout de temps ! Et voilà que ce soir, un élan 
lancinant lui torpillait l'occiput. Ça en annonçait une carabinée ! Mauvais signe ça. Il arrivait 
toujours des bricoles quand la tempête s'invitait sous son crâne. Il farfouilla dans les poches de 
sa veste en côtes de velours élimée, à la recherche d'un tube d'aspirine. Merde ! Il avait oublié 
ses médocs. Il plongea les yeux au fond de son gobelet. La nausée l'assaillit soudain. Il fit un 
signe à la serveuse qui s'activait au nettoyage, mazette, l'endroit, lugubre, en avait bien besoin 
et s'éloigna, poussant la porte d'accès à double battant. Il balança son gobelet et le reste de son 
contenant dans la grande poubelle en alu qui dégueulait de détritus odorants, puis d'un pas lourd, 
se dirigea vers son pick-up garé un peu plus loin.

Au poste, au kilomètre cinquante-cinq, l'agent Axel Bowers et son collègue Clemens Pattinson 
                                                    °°°°°°                 °°°°°°° °°°°°°°°°
terminaient leur rapport. Un rapport bien ennuyeux, en accord avec un début de service mortel.

Le tronçon de RN4 sous leur autorité cette nuit était vide comme un réfrigérateur le 29 du mois. 
              °°°
Ils avaient ramassé un petit jeune complètement bourré à hauteur de l'ancien silo. Le gosse n'avait 
pas fait d'histoire, bourré comme une outre. La caisse puait le vomi, le conducteur, permis moins 
de six mois, expérimentait la biture et le résultat n'était pas celui escompté ; un séjour dans un 
poste de police miteux, avec pour tout confort une planche agrémentée d'une couverture en guise de 
lit. Pourtant le jeune gars ronflait à présent. Clemens jeta un coup d'œil dans la cellule, avisa 
                                                °°°°°°°
son collègue d'un air entendu et poursuivit sa frappe maladroite sur un clavier déglingué. 
Emmerdante comme la pluie cette soirée ! Il préférait bosser le week-end, il y avait un peu plus 
d'actions, quelques poursuites, des arrestations musclées. Des accidents aussi. Ce soir, il 
rongeait son frein. Axel préparait déjà les pions, les noirs pour lui, les blancs pour son binôme 
de toujours. Rien de tel qu'un jeu de dames pour égrener le temps, partie après partie, l'aube 
finirait bien par pointer le bout de son nez. La radio grésillait à l'autre bout du bureau. RAS. 
Clemens lança l'impression, rangea son rapport dans la pochette dédiée, fit craquer ses doigts d'un 
°°°°°°°
air satisfait et s'attabla face à son collègue. Il se sentait en veine ce soir.

Le voyant se mit à clignoter. Le réservoir afficha sa mise sur réserve. Roxane sortit de sa torpeur 
et soupira. Il lui semblait avoir aperçu un panneau indiquant une station proche. Elle n'était pas 
inquiète, tant pis pour ce léger contretemps. Elle était déterminée. Elle regarda dans le 
rétroviseur ; pas âme qui vive. Autre contrariété bien sûr, mais non, au fond, elle n'était pas 
inquiète, tous ces petits soucis ne l'atteignaient plus. Elle entreprit d'accélérer encore, à 
l'invite de l’entraînant « Enjoy the silence », de Dépêche Mode, son groupe préféré ; la voiture, 
                           °°°°° °°°
complice, répondit instantanément, presque joyeusement, lui sembla-t-elle.

« Viens Roxane, on va se foutre en l'air sur la RN4 ! »
                                                °°°

La jeune femme sourit au rétro d'un air complice.

« Ouais, on va se foutre en l'air dès qu'on aura du jus ma belle. »

Elle se sentait planer un peu, peut-être ces pilules qu'elle avait piquées à son jules. Son 
ex-jules plus exactement. De son poing droit, elle fracassa le rétroviseur. L'automobile fit une 
belle embardée, ses pneus extra-larges crissant sur la route.

« Pas encore ! » se morigéna-t-elle, « Pas encore. »

Un nouveau panneau d'affichage lumineux clignota au-dessus de la voie :

Station à 7 km

« Parfait » se dit-elle en suçant le sang qui dégoulinait de sa main.
« Le plein, et on passe aux choses sérieuses. »

La vie avait définitivement cessé d'intéresser Roxane. En premier lieu parce que sa vie n'en était 
pas une. Plutôt un long tunnel sombre et glissant. Très casse-gueule. Elle se mit à imprimer de 
légers coups de volant au bolide qui dansa sur l'asphalte.

« Manque plus que le salto. Bientôt » murmura-t-elle pour elle-même avant de mettre sagement le 
clignotant, réduire sa vitesse, et s'engager sur la voie de décélération menant à la station 
essence. L'horloge du tableau de bord indiquait trois heures. Absorbée par ses pensées, elle ne vit 
pas le pick-up estampillé du logo du service routier s'éloigner au ralenti.

Ted plissa les yeux en avisant les phares puissants qui se reflétaient dans ses rétros. Le sang 
afflua à ses tempes comme le flux et reflux d'une mer démontée en guise de protestations.

Une grosse cylindrée. Bah, encore un quinqua en mal de sensation forte qui va se taper une 
branlette sur le ruban sans accroc que déroulait la RN4. Pensée automatique. Le tronçon D était une 
                                                    °°°
crème pour tous ces tarés. Une pointe à deux cent quatre-vingts voire deux cent quatre-vingt-dix ; 
ils lâchaient la sauce dans le slip et rentraient chez eux, rejoindre Madame, réjouis.

Le crâne de l'agent des ponts et chaussées pulsait comme un tam-tam déchaîné appelant l'orage à 
présent. Comme il détestait ces gros lards friqués !

Jaimie loucha sur la pendule : trois heures. Une moto noire comme la gueule d'un four déboula sur 
°°°°°°
sa gauche et le doubla à une vitesse indécente. Le chauffeur routier suivit le phare arrière des 
yeux, petit diablotin qui s'évanouit aussi rapidement qu'il était apparu. L'homme s'étira, lâchant 
le volant un instant, mais le semi-remorque ne broncha pas, ne dévia pas de sa trajectoire. Il 
avalait la route comme un boa une antilope, patiemment. Jaimie enclencha le second film de sa 
                                                        °°°°°°
soirée, « Simetierre ». Il en salivait d'avance. Par acquit de conscience, il fit le tour du 
          °°°°°°°°°°
tableau de bord, tous les voyants au vert lui intimaient de ne pas se faire de bile. Il pouvait 
s'accorder sa dernière séance, tranquille.

Roxane introduisit sa carte bancaire dans la borne et fit le plein. Tandis qu'elle maintenait le 
pistolet dans la gueule du réservoir, elle leva la tête vers les étoiles. Le ciel lui offrait un 
bien joli spectacle. Elle reconnut la grande ourse, chercha l'étoile du nord, puis Cassiopée, 
s'agaçant de ne point la découvrir. Son esprit emplit de brume peinait à réfléchir. La pompe stoppa 
automatiquement. La jeune femme décrocha son regard du firmament et remonta promptement dans son 
bolide. Elle démarra en trombe alors que sa carte bancaire surgissait de la fente de l'automate. 
L'alarme se déclencha. Le pistolet, dont le tuyau se déroula un instant, vint à manquer, et 
s'arracha brutalement du réservoir de la voiture, emportant un empan de carrosserie de luxe. Il 
brinquebala sur la chaussée dans un fracas de ferraille. La pompe endommagée se mit à sonner.

Les alarmes hurlaient. Hurlaient dans le vide.

La grosse cylindrée avait déjà disparu dans la nuit.

Les deux flics attaquaient la revanche. La première partie s'était révélée d'une facilité 
déconcertante pour Axel, et son acolyte s'était rapidement rendu, avant de l'écrabouiller sans 
remord en réussissant coup sur coup à lui faucher toutes ses dames au cours d'une seconde manche. 
La belle s'annonçait passionnante.

La radio choisit ce moment précis pour grésiller désagréablement.

— Poste 12, ici station Agia, tronçon D.
                        °°°°

Les policiers se dévisagèrent sans broncher, à peine curieux de la suite du message ; encore un 
connard partit sans payer la note et Rat d'égout, ainsi que les deux compères avaient surnommé la 
blondasse qui turbinait de nuit à la station, s'amenait pleurnicher pour qu'ils lui arrangent le 
coup. Une voix de crécelle s'égosilla de plus belle dans la radio.

— Possible CEI (comprenez conduite en état d'ivresse) Porsche 935 carbone. Pas de plaque 
           °°°                                                    ^^^^^^^
* 2 [66:73]  # #26174 / g3__gn_nombre_chiffres_1m__b2_a1_1:
  Accord de nombre erroné avec « 935 » : « carbone » devrait être au pluriel.
  > Suggestions : carbones

d'immatriculation. La conductrice a fait le plein en laissant sa carte bancaire dans la machine, et 
son aile arrière pour n'avoir pas réservoir avant de démarrer. La pompe est foutue, j'ai appelé les 
pompiers pour le RI (comprenez risque incendie). Allô ? Poste 12 ?

Les deux hommes soupirèrent de concert, attrapèrent leurs équipements sans prendre la peine de 
répondre à la radio. La voix nasillarde poursuivait son soliloque.

Rat d'égout allait trembler encore un peu, quitte à faire dans son froc.

Un instant plus tard, les portes de la grosse berline tous gyrophares et sirène allumés claquaient 
déjà ; il restait deux blancs et deux noirs sur le damier.

Rod doubla en souplesse le poids lourd, une bonne moyenne cette nuit, 240 km/h, la RN4, pas à dire, 
°°°                                                                                °°°
un velours, vrai de vrai ! Le motard devait effectuer sa livraison avant six heures, et pas de 
doute, il serait dans les temps, son employeur serait content. Peut-être une petite prime à 
l'arrivée… Grisé par les ombres et le déroulement incessant du macadam sous lui, il actionna un peu 
la poignée d'accélération, la bécane s'élança de plus belle, comme une amante sensuelle répondant à 
tous ses désirs.

Une lumière vive troua la nuit, juste en face, file de gauche. Elle semblait même lui faire de 
l'œil. Un rendez-vous avec le destin.

Non, songea Roxane, un rancard avec la mort.

Elle poussa le son, « Wrong » se répandit dans l'habitacle, elle pressa la pédale d'accélération, 
                      °°°°°
un léger sourire sardonique sur son beau visage ravagé, et le bolide prit de la vitesse, 
vrombissant à son aise sur la route large et lisse. La jeune femme serrait violemment le volant, 
les extrémités de ses doigts blanchirent. Sans s'en apercevoir, elle grinçait des dents, les yeux 
exorbités, la tête au plus près du pare-brise, elle aurait pu lécher la vitre, ignorante de son 
environnement, hypnotisée par ce falot de plus en plus vif qui se rapprochait à une vitesse 
vertigineuse. Le paysage avait totalement disparu, l'obscurité seule nappait la plaine, le long de 
la RN4. Roxane entendait obscurément les battements de son cœur ; un marteau sur une cloche de 
   °°°
bronze, boum, boum, un fracas régulier au tempo rapide. Elle toucha le plancher du véhicule avec la 
pédale d'accélération et la voiture réagit par un bond à peine détectable, tandis qu'au compteur, 
l'aiguille plongeait du côté rouge, un peu, puis un peu plus… Elle éteignit ses phares brusquement 
et vira en douceur sur la voie de gauche. La belle allemande obtempéra avec souplesse, malgré 
l'allure endiablée à laquelle elle était lancée.

*Je vais crever*, constata Roxane sans vraiment s'émouvoir, sans que le passé ne défile devant ses 
yeux, sans larme non plus. Juste un grand vide au fond de son cœur, juste un grand trou à la place, 
certainement quand on n'est plus rien. Est-ce un pêcher de se donner la mort ? Elle éructa un rire 
cristallin et dément ; la puissance du phare unique arrivant sur elle remplit soudainement tout 
l'espace.

Rod eut un doute salutaire à la dernière seconde, allez savoir, l'instinct, la pratique, la chance. 
°°°
Il lui sembla détecter une sorte de reflet, sur le goudron, un scintillement. Son cerveau encaissa 
le choc alors que sa main ralentissait, rétrogradait, que la moto passait sans accroc sur la voie 
en sens inverse, et qu'il croisait dans une incroyable fulgurance un bolide noir roulant avec les 
chiens de l'enfer à ses trousses. Peut-être était-il passé à un cheveu de la grande faucheuse, 
peut-être moins. Le motard ralentit encore, tandis qu'à travers sa combinaison, il suintait la 
peur, puante et gluante, par tous les pores de sa peau. Une peur bien vaine à présent !

Il se réengagea sur la voie de droite, à l'allure poussive d'un cyclomoteur, avisa un panneau 
indiquant une station-essence et entreprit de faire une pause, boire une mousse, casser une 
croûte ; profiter de la vie sans trop se presser. Une première.

Roxane hurla lorsqu'elle croisa sans le percuter le motard s'enfonçant déjà dans les ténèbres. La 
voiture se cabra, car malgré elle, la conductrice avait posé un pied heureusement léger, sur la 
pédale de frein. Ces bagnoles-là sont avantageusement dotées d'une souplesse sans nom, et 
l'automobile rattrapa habilement sa trajectoire, à vitesse plus réduite, tandis que Roxane 
hoquetait d'un effroi rétroactif. Un goût de bile lui remonta dans l'œsophage. Elle se sentit 
soudain en colère, contre elle-même, contre ce fichu motard de merde, contre le monde entier. Son 
poing meurtri frappa plusieurs fois le volant. Elle avait raté sa sortie ! Incapable d'avoir une 
vie et encore plus inapte à se l'ôter !

— Putain de merde, elle répétait ces mots en boucle, tel un mantra.

Ils eurent peut-être un effet sur le grand manitou, ou pas. En tout cas, deux gros phares jaunes se 
révélèrent au détour d'une petite pente de la route, et éclairèrent la nuit. Roxane se torcha le 
nez avec sa main endolorie, l'essuya contre son giron, et sourit à nouveau, concentrée. Le 
véhicule, à vue de nez, était à quelques kilomètres encore, juste le temps pour la jeune 
conductrice de remettre les gaz.

Rod posait la béquille lorsqu'une voiture de patrouille de la route débarqua se garer juste à côté 
°°°
de lui. La station était en ébullition, une équipe de pompiers s'acharnait sur les pompes qui 
avaient apparemment subi des dégâts. Une fille blonde et laide comme un pou chialait à côté de la 
porte d'entrée de la boutique. Le motard ne s'y attarda pas, rien ne lui paraissait important après 
sa récente mésaventure. Il s'installa confortablement au bout du bar, à deux pas de la porte 
ouverte et admira le spectacle de tous ces cons en train de s'exciter pour quoi en fait ? Hein ? Il 
tendit l'oreille lorsque les deux flics en aparté avec ce qui paraissait être le chef de 
l'escadrille de pompiers, tinrent conciliabule.

— Salut Robert, c'est quoi ce bordel ? Questionna brutalement Axel.

— Salut les mecs, c'est bon, on maîtrise, juste un problème de coupe-circuit, alors y a un peu 
d'essence répandue partout, mais on aspire tout ça, t'auras place-nette d'ici quinze minutes, 
précisa le spécialiste des feux.

— Va falloir trouver le responsable de ce merdier ! Gronda Axel.

— La, en fait. L'interrompit le chef.

— Hein ? Clemens pencha la tête comme le chien de la voix de son maître, avec moins de charme, tout 
         °°°°°°°
de même. Parfois, Axel lui trouvait un air un peu abruti.

— Une responsable. Une jeune brune à bord d'une Porsche 935 a tout défoncé. Elle doit pas être dans 
son assiette, résuma le chef des sapeurs.

— Bordel, grogna encore Axel.

Rod fit immédiatement le rapprochement avec sa kamikaze. Il fit un geste en direction des trois 
°°°
zigotos. Ceux-ci l'interrogèrent du regard, puis, voyant qu'il ne daignait pas lever son cul de son 
siège, s'approchèrent, affichant des mines de molosses à qui on a piqué son os.

— Un problème ? Clemens avait essayé d'y mettre les formes, mais ne transparaissait que sa 
                °°°°°°°
lassitude.

Rod entreprit de raconter sa rencontre avec la fille à la Porsche.
°°°

Deux minutes plus tard, les flics sautaient dans leur voiture de patrouille, et Rod, enfin, se 
                                                                                °°°
commandait une bière tranquillement installée au bar, écoutant presque avec sollicitude les 
jérémiades de Rat d'égout.

Jaimie leva les yeux avec regret.
°°°°°°

Putain c'était quoi ce con qui lui foutait ses phares en pleine poire ?
Le gamin venait de déterrer son matou, un des meilleurs moments, celui où la bestiole 
réapparaissait, s'annonçait… Si c'est pas dieu possible de déranger les gens comme…

Le camionneur plissa les yeux, la bouche ouverte, les mains crispées.
Les phares se positionnaient dans sa ligne de mire. Les phares de celui d'en face étaient tout 
justement, parfaitement en face de son gros cul de quarante-quatre tonnes !

Il apercevait la silhouette du conducteur, échevelée. Le bon vieux Buffle était face à une petite 
sportive, fine, basse de caisse, élancée, qui fonçait comme une fusée. Une vicieuse.

Plus loin derrière, Axel et Clemens piquaient droit sur le point d'impact imminent.
                            °°°°°°°

Jaimie fit le calcul le plus rapide de sa vie ; il n'aurait jamais le temps de l'éviter. Il 
°°°°°°
entreprit de ralentir, il connaissait bien son camion, s'il freinait trop vite pour modifier sa 
trajectoire, le mastodonte allait partir en accordéon et les envoyer dans le décor. Il fit aussi 
délicatement que ses grosses paluches s'en sentaient capables. Tout doux, il freina, appliqua une 
petite touche au volant .

Il s'aperçut que le chauffeur en face était une bonne femme au moment où le semi-remorque opérait 
une vrille sur son tracteur avant, et que la remorque commençait à se déporter sur toute la largeur 
de la chaussée. Les quatorze pneus crissèrent dans un seul hurlement terrible, alors que le camion 
continuait d'avancer en crabe, la cabine à présent retournée sur le toit, arrachant de grands 
lambeaux de cette RN4, un velours, oui, mais plus maintenant. D'autant que la cervelle de Jaimie 
                  °°°                                                                     °°°°°°
devait à présent faire reluire une partie non négligeable du bon vieux macadam.

Roxane gardait son calme.

— Cette fois, tu me la feras pas à l'envers, saloperie, brailla-t-elle à haute voix. Le pied collé 
à l'accélérateur, ne faisant qu'une avec son bolide, la jeune femme vida son esprit triste et 
morne, les yeux rivés sur le semi-remorque. Lorsque celui-ci entama son ballet en deux temps, il 
était trop tard pour que l'automobiliste change quoi que ce soit, aussi, ferma-t-elle les yeux. 
Regrettable erreur ! La 935 se rua sur la remorque, qu'elle traversa en son milieu, sans aucune 
égratignure, passant sous la travée métallique dans une cascade qu'on aurait pu supposer réglée de 
longue date. Une explosion se fit entendre après son passage et ce qui restait de la cabine du 
semi-remorque prit feu. Roxane rouvrit les yeux. La caisse filait droit devant elle à un train 
d'enfer, dans un ronronnement félin quasi diabolique. Et tout autour, des champs partout ! D'une 
platitude !

*Contre quoi allait-elle bien pouvoir se foutre en l'air, bordel de dieu !* Elle éclata en sanglots.

Les deux flics sursautèrent lorsqu'une torche de flammes illumina la route, au loin.

Ted alluma sa radio, il commençait à piquer du nez.

— Poste 12, appelle à tous les postes de la RN4 pour les tronçons 50 et 51. Postes 13 et 14. Appel 
                                            °°°
de renfort. Nous avons une folle suicidaire à bord d'une voiture de sport roulant à vive allure. 
Faites évacuer et fermer la route. Je répète faites évacuer et fermer la route ! Un accident s'est 
produit au kilomètre 380. Appel à l'unité de sapeurs-pompiers du 50 et 51 ainsi qu'aux agents 
techniques du 50 et 51. À vous !

Ted accusa le coup, puis saisit sa radio, après avoir vérifié sa position.

— Ted AT poste 50, kilomètre 365, j'arrive aussi vite que possible, je ferme les voies de 
pénétrations sur le tronçon.

— T'es derrière nous, connard, tu sers à rien ! Libère la fréquence !

Ted resta comme deux ronds de flan, devant cette harangue ubuesque, le micro à la main.

Mince alors ! Certainement l'histoire de sa vie, et il était trop court de quelques bornes !

— J'arrive en renfort sur l'accident, reprit-il d'une voix assurée. Il se détendit et ajouta, 
« connard. » Puis coupa la radio. Sa migraine dansait la gigue dans les circonvolutions en feu de 
son pauvre cerveau.

Rod entendit le message radio, la fille de la station s'était immobilisée, une statue.
°°°

— Plutôt moche, observa le motard.

Il y eu méprise, et Rat d'égout, derrière le bar, lui retourna un fuck magistral tout en astiquant 
     ^^ ^^^^^^^                                                   °°°°
* 1 [5:7]  # #33351 / gv1__conf_auxiliaire_avoir_être__b2_a1_1:
  Incohérence. La forme verbale “méprise” ne peut pas être utilisée avec l’auxiliaire “avoir”,
    seulement avec l’auxiliaire “être”.
* 2 [8:15]  # #31925 / gv1__ppas_pronom_avoir__b2_a1_1:
  Ce verbe devrait être un participe passé au masculin singulier.
  > Suggestions : mépris | méprisé
  > URL: http://fr.wikipedia.org/wiki/Accord_du_participe_pass%C3%A9_en_fran%C3%A7ais

une pile de verres

Rod saisit son casque, avec l'envie foudroyante d'être vite très loin de cette route, et de tous 
°°°
ces tarés. Il laissa un billet sur le comptoir, sans même finir sa bière.

— On la rattrapera pas, ronchonna Axel.

— Ouais, mec, mais on va tout de même aller au bout, reprit l'agent Pattinson.
                                                                    °°°°°°°°°

— Deux blanches. Sûr que je vais te mettre dedans en rentrant. Il pouffa.

— Crois-y ! Son collègue lui bourra l'épaule de quelques coups amortis.

— Tu crois qu'ils vont la chopper ? Reprit Bowers.
                                           °°°°°°

— 100 balles que non ! S'exclama Clemens.
                                 °°°°°°°

— Tenu mec ! 100 balles, dis donc, t'es généreux ce soir !

Ils ralentirent, adoptant une vitesse peinarde, et poursuivirent leur conversation tranquillement, 
au plus profond de la nuit, sur ce ruban, un velours, ouais, la RN4.
                                                                °°°

À l'abord du tronçon 51, cependant, tout était près pour recevoir le bolide fou à présent.

Roxane ne se doutait de rien, elle roulait vite, et cherchait des yeux quelques choses sur quoi 
diriger sa merveilleuse voiture, cadeau de son merveilleux partenaire, dans sa non moins 
merveilleuse précédente vie… mais il n'y avait même pas un caillou sur les bas-côtés. Elle avait la 
rage. Elle se mordait les mains, et l'intérieur des joues, ne tenant le volant que d'une paire de 
doigts, à peine, mais la Porsche imperturbablement, traçait la route, impeccablement.

Les agents de police Millie Forman et Jones Dunar, du 51, avaient dressé un barrage. De larges 
                     °°°°°° °°°°°°          °°°°°
bandes cloutées traversaient à présent la chaussée. Des gyrophares éclairaient toute la zone, les 
sapeurs, également du tronçon 51, attendaient en préparant leur matériel, sans hâte inutile ou 
débordement. L'atmosphère était tendue, mais on sentait les professionnels en action.

— Elle n'a aucune chance de passer, capitaine, assura Millie, la cinquantaine, réajustant sa 
                                                      °°°°°°
ceinture devenue, au fil des ans, de plus en plus serrée et lourde pour sa corpulence.

— Vous laissez votre voiture de patrouille derrière les herses ?

— Oui, elles ont été doublées, chef, enquilla le jeunot qui servait d'équipier à l'agent Forman. 
                                                                                         °°°°°°
« Elle a aucune chance, elle va se ramasser en beauté si elle tente de forcer le passage. Ça sera 
pas beau à voir, chef !

— Capitaine, reprit le sapeur, un peu hautain. Je suis le capitaine. Reste à espérer qu'elle ait la 
présence d'esprit de stopper son engin sans bobo.

— Faut prier pour, chef.

— Je ne suis pas curé, et c'est mon capitaine. L'homme se renfrognait au fur et à mesure de la 
discussion.

Millie tira son collègue par la manche. « Pousse la bagnole, mets-la plus loin, si on lui fait une 
°°°°°°
bosse, on va encore prendre un blâme ». Le jeunot s'exécuta en râlant.

Roxane vit le barrage, les lumières partout, un camion de pompiers, des flics et elle soupira 
d'aise, des obstacles ! Enfin ! Elle avait le choix ! Elle éclata de rire, la voiture rugit à 
l'unisson. Roxane beugla en s'arc-boutant sur la pédale de l'accélérateur ; la voix de chaude de 
Dave Gahan lui susurrait à l'oreille une ultime mélodie.
     °°°°°

La voiture de police de nos deux agents, bien rangée sur l'accotement, n'eut pas une éraflure.

Par contre, le vol plané de la Porsche 935 noire carbone, après son passage sur les herses, et un 
                                           ^^^^^ ^^^^^^^
* 1 [43:48]  # #26174 / g3__gn_nombre_chiffres_1m__b2_a1_1:
  Accord de nombre erroné avec « 935 » : « noire » devrait être au pluriel.
  > Suggestions : noires
* 2 [49:56]  # #26179 / g3__gn_nombre_chiffres_1m__b3_a2_1:
  Accord de genre erroné entre « noire » et « carbone ».

ripage incontrôlé contre la citerne des pompiers, l'avait pile poil amené sur l'aire de parking, 
juste derrière, où les agents Forman et Dunar s'était malencontreusement mis à l'abri. On voyait là 
                              °°°°°°    °°°°°
du sang, beaucoup de sang vermeil, mêlé aux fluides de l'automobile, gouttant en dessous de la 
Porsche accidentée.
Un vestige d'uniforme ensanglanté dépassait d'une roue arrière crevée.

— Putain un sacré saut ! Gueula le capitaine, sous les yeux ébahis de Ted, qui arrivait trop tard.

Tous, policiers, agents, se mirent à s'activer autour de l'engin de mort.

Ted s'avança, et n'en crut pas ses oreilles quand il entendit à l'intérieur du bolide, cabossé et 
tordu, une petite voix criarde et furieuse s'élever ;

— Merde aux airbags Putain de chiée, pourquoi j'ai pas désactivé ces putains de merde d'airbag. 
Bagnole de luxe de merde !

L'agent des ponts et chaussées s'aperçut que sa migraine avait disparu, d'un coup, comme par magie. 
Il soupira d'aise.
