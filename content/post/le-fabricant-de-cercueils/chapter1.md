(Conte en hommage à mes arrières-grands-parents, dont l'amour aujourd'hui encore continue à consoler mes peines)

L’homme portait un béret plat bleu marine, aux bords usés, qu’il repoussa de sa large main décharnée ; il se mit à se gratter la tête. Les reins appuyés sur un imposant établi de hêtre trônant le long d'un vaste mur dans son atelier de charron, Gabriel réfléchissait. Il se donnait toujours du temps avant de démarrer une nouvelle pièce, n’appréciant ni la précipitation, ni l’à peu près. Ce matin, au lever, l'idée lui était venue, comme elle venait toujours, sans rime ni raison ; il était temps, c'est tout. Aussi s'attacha-t-il en premier lieu à inventorier mentalement tout ce dont il allait avoir besoin,outils et matériau, puis au bout d'un moment, un long moment à vrai dire, se décida-t-il à scruter le fond de la boutique — ainsi sa femme nommait-elle l'endroit — où, sagement rangées, debout en appui sur un mur en pan de bois et torchis, de massifs plateaux de chênes attendaient de passer entre ses mains. Il s'avança enfin ; l'ombre régnait malgré un franc soleil depuis l'aurore. Dans l'air tiède, la poussière, omniprésente, se soulevait à chaque mouvement de l'homme, dessinant un halo doré autour de lui. Il était nimbé d'or — l'or du charron — la poussière des bois bruts qu'il travaillait sans relâche, avec douceur et ténacité, les transformant après de longues heures de peine et de joie, en une véritable œuvre d'art.

Il dégagea délicatement les plateaux sélectionnés, et les porta à coté de l'établi. Leur taille s'avérait parfaite, l'épaisseur, adéquate. Il ajusta ses bretelles, sous sa dure chemise de coutil bleu, fourailla dans la vaste poche de son pantalon de travail, bleu également, pour vérifier la présence de son mètre pliable en bois, et bascula sans effort le premier plateau sur l'établi qui se trouvaient là. Il passa une main caressante sur cette matière noble, admirant le fils du bois, la densité, la teinte puis saisit la varlope afin de dégauchir la planche.

Bientôt, de l'appentis, seul le bruit du va-et-vient du grand rabot parvint aux oreilles de Madeleine. Elle frissonna, comme à chaque fois, puis se remit à confectionner sa tourte. Son esprit vagabonda. Simone, leur fille, jouait encore dans la cour, il faisait bon, les géraniums dans leurs gros pots de terre cuite étalaient leurs rose et blanc sous un franc soleil de juin. Et puis Gabriel avait sorti des planches, du merisier, des petites.



Un parfum de viande cuite chatouilla les narines du charron. Il se redressa, étira son dos vermoulu par ces heures d'efforts intenses et répétitifs. Il passa le plat de la main sur le bois poli puis, satisfait, posa son outil et laissant là son ouvrage, rejoignit sa femme pour le déjeuner. Elle leva la tête lorsqu'il passa le seuil de la cuisine, elle chargeait la cuisinière d'une bûche de belle taille.

— J'allais t'appeler, dit-elle, posant sa serviette dans son assiette, avec tendresse.

— Ça sent rudement bon, dit l'homme.

— C'est pour quand crois-tu ? Lui demanda-t-elle, une pointe d'anxiété dans la voix.

— Peut-être ce mercredi ? Soupira-t-il, je n'aurai guère loisir de finir avant.

— Il te faudra quel sorte d'habillage cette fois ? Questionna-t-elle encore, lui servant une bonne part de tourte tout en remuant nerveusement la salade.

— Du gris, le gris avec les petites fleurs noires, tu sais, ce velours que j'avais mis, déjà, pour Madame Fébur.

Madeleine posa sa fourchette, la bouchée de tourte, coincée dans sa bouche, ne voulait pas passer. Elle se força à mâcher, mâcher encore, puis, d'un coup sec, elle avala.

— Une femme alors ?

— Non, un homme, un grand, jeune et fort. J'ai fait mon coffre pour un homme.

Sans savoir pourquoi, cela la rassura. Elle se remit à manger et il parlèrent de choses et d'autres, du chat qui devenait fainéant et des souris qu'on entendait galoper dans les murs creux, du temps, du jardin, des tourterelles, dans la volière, c'était le temps des couvaisons, et Madeleine allait régulièrement voir s'il était né de jeunes tourtereaux. Parfois, elle donnait la becquée pour « les pousser un peu ». Simone aimait tant les tourterelles… les blanches, surtout…

Durant les jours suivant, Gabriel rabota, dressa, arasa, scia, tant et si bien que les copeaux et la sciure s’amoncelèrent en un tapis soyeux dans l'atelier. Le bruit des pas se fit feutré, les planches furent ensuite ajustées entre elles, chevillées et les poignées fixées, quatre belles poignées de bronze. La caisse enfin, mérita son garnissage, Madeleine avait cousu la belle pièce demandée, devant le feu, aux petits matins, dans la cuisine. Elle se rappelait avoir fait de même avec tant de couleurs et de matières différentes, jusqu'à ce jaune bouton d'or, ce jaune que Simone aimait tant… Ce mercredi là, enfin, ne resta plus qu'à fixer le textile à l’intérieur.

Après le repas du midi, Gabriel s'agita, un peu fébrile, c'était un grand moment à chaque fois ; l'épreuve de vérité, en quelque sorte.

 — Tu viens voir ? Convia-t-il sa femme, sans trop y croire, car depuis le temps qu'elle portait le noir, Madeleine ne s'aventurait plus guère dans l'atelier. Elle ne lui apportait plus son café de quatre heures, se contentant de l'appeler timidement en entrebâillant la porte de la cuisine. Elle ne passait plus, assise sur le grand établi, des heures à le regarder peaufiner une planche, la rendre si lisse, si douce, que la main même la plus exigeante n'en découvrait aucune aspérité. Il comprenait, et ne lui tenait rigueur de rien. Chacun d'eux, figé dans leur solitude réciproque, acceptait les marottes de l'autre. 

— Le fils Maroni est mort cette nuit.

— Ah ? Gabriel leva un sourcil, attendant la suite.

— Il s'est encastré dans un arbre en revenant de boire de la ferme des Saules, à Montpothier.

— Quelle tristesse ! Pas vingt-cinq ans… omment…

— Il aura une belle place où reposer, coupa l'homme soudain rembrunit. Le croque-mort veut que je l'apporte quand ?

— Tout à l'heure, je lui ai dit que tu aurais fini avant le crépuscule. Si tu veux, il peut atteler sa charrette, tu t'économiserais un aller-retour avec le char à bras.

— Non, j'irai. J'aime bien voir si mes locataires sont bien installés.

Haussant les épaules, Madeleine sortit de table, repoussant sa chaise un peu brusquement et celle-ci pirouetta dangereusement sur trois pieds. L'homme la rattrapa de justesse et son regard triste soudain s'attarda sur le dos noir de Madeleine, ce dos recouvert d'un châle noir bordé de dentelle qui ne la quittait plus. Il ferma les yeux un instant, et instantanément, un beau sourire franc, enfantin, illumina son front, sous ses paupières. Le sourire de Simone. Il en ressentit une grande paix, et le courage aussi d'aller affronter sa femme.


Debout dans la remise du fossoyeur, Gabriel admirait la fraîcheur du teint que le thanatopracteur, habile trompeur de mort, avait restitué au défunt. Le cadavre, habillé comme pour une première communion, reposait dans un costume anthracite sur les coussinets gris mouchetés de noir. Il semblait prendre un peu de repos, avant la cérémonie. Son visage, juvénile, ne portait point les traces de son récent accident. Pourtant il dormait d'un sommeil sans fin dans le cercueil confectionné par Gabriel.

— Très bon travail, mon gars, comme à l'habitude, se permit le croque-mort, serrant la main du charron, « cercueil de chêne, premier choix, à la bonne taille, et parfaitement exécuté »

— Il sera bien là, c'est sûr, qu'il repose en paix, chuchota Gabriel, se recueillant un instant.

— C'est pour les parents que c'est dur, ajouta le croque-mort, hochant la tête.

— C'est toujours pour les parents les cœurs qui saignent et le grand vide, conclut Gabriel en s'en retournant.

 

Madeleine souffla à mi-hauteur de la pente, redressant son chignon affaissé par l'impitoyable montée, et se hissa au final, de marches en marches avec quelque difficulté, sur le promontoire, son bouquet de pivoines à la main. *Elles ne tiendront pas longtemps, les pauvres,* songea-t-elle. La colline était rase, exposée en pleine chaleur à la verticale d'un soleil de plomb. Puis sa pensée se durcit, *oui, leur dépérissement sera rapide, elles ne resteront pas des mois à pleurer de douleurs, elles ne se tordront pas les mains à fixer le plafond sans dormir, à suer sang et eaux en appelant la mort, alors qu'importe ? Simone les aimait tant.* Ainsi la vieille dame continua de grimper jusqu'à l’extrémité du petit cimetière, jusqu'à une pierre tombale de granit rose surplombant le hameau. Madeleine souffla alors un instant, épousseta sa blouse, se tint bien droite devant l'édifice, caressa rêveusement la pierre granuleuse et pleura.



Le village vivotait de quelques centaines d'âmes, aussi Gabriel ne fabriquait-il des cercueils qu'en de rares occasions. Seulement quand c'était nécessaire, ou plus exactement, quand il en éprouvait la plus extrême neccessité. Un certain matin, il se levait et ses doigts folâtraient sur l'établi fourmillant de raboter la fibre tendre du bois tandis que ses yeux cherchaient les planches adéquates à la clientèle du jour. Son premier métier avait été charron, et c'était un bien bon et rude métier à plein temps déjà. Point ne lui en fallait d'autre. Est-ce le destin qui lui affubla cette drôlesse de malédiction ? Est-ce le juste et terrible instinct paternel ? Le hasard dans son éminentissime ironie ?

Il était encore jeune et fort quand le sort avait décidé de lui jouer ce mauvais tour À l'aurore, un jour de juin, après un déjeuner rapide en compagnie de sa femme et sa fille, il s'était enfermé dans l'atelier, une commande de brouette à honorer. Mais le midi, il était sorti comme d'un songe lorsque sa petite était venue le chercher.

— On va bientôt déjeuner papa, s’était-elle exclamée gaiement, du haut de ses dix ans, un chaton noir entre les bras.

— Quoi ? Déjà midi ? Avait-il renchérit, découvrant avec stupeur quatre petites planches de bois, dont deux déjà parfaitement équarries, polies et gauchies, reposant de guingois sur l'établi ; un si mignon petit coffre en devenir cela ne présageait, ma foi, rien de bon, subodora-t-il. Quatre poignées, ajourées de feuillages et laquées de blanc gisaient à coté de quelques vis. Il s'étonna de son labeur.

— Où est donc la brouette du père Fabre ? Questionna la petite, découvrant l'ouvrage de son père encore loin d'être finalisé.

— Je… Bredouilla son père, comme perdu, absent. Aujourd'hui encore, il se rappelait comment il avait cligné des yeux, là, serrant soudain sa fille dans ses bras, qui sentait si bon le savon et d'avoir écrasé un peu, si peu, le chaton sur sa large poitrine, près de ce cœur qui battait si fort à cet instant, si désespérément. Il se souvenait des rires de l'enfant, de la cavalcade du petit félin sous les grandes roues esseulées, au fond de la boutique, les mitons de poussières soulevés, les genoux salis, les rires encore, l'appel de Madeleine…



— Mais qu'est-ce que tu fiches là-dessous ? L'interrogea sa femme inquiète en franchissant le seuil de la boutique.

— J'ai laissé tombé un paquet de vis, ma main tremblote, elle ne vaut plus grand-chose, ma vieille, lui renvoya Gabriel, tiré brutalement de ses souvenirs.

— Tu dis n'importe quoi, mon ami, souffla-t-elle en levant les yeux au ciel, ou plutôt, le long des larges poutres qui entravaient le haut plafond de l’atelier, « viens prendre un bon café et une petite goutte, le père Fabre a rapporté une mirabelle qu'il veut te faire goûter. Allez viens, laisse un peu le bois et les rabots… »

Elle le tira légèrement par le coin de la veste. De sa poche, le mètre pliant dépassait à peine, mais par mégarde, il s’accrocha dans le châle de dentelle et chuta dans la sciure avec un bruit mat, entraînant avec lui une chiffe de velours jaune bouton d'or, affadie, chiffonnée et sale.

— Oh ! Murmura Madeleine, sortant à reculons, se mangeant le poing d'une souffrance muette.

— Madeleine ! Supplia son mari, honteux soudain de son petit secret révélé.

Il se releva avec peine, engourdit de tristesse, les os vermoulus, sans prendre garde à sa hanche qui craquait et avec la plus grande délicatesse, pinça le petit bout de chiffon entre ses doigts puis le glissa de nouveau au fond de sa grande poche. Il y laissa sa main un instant, là, bien au chaud, et un trop court moment, il tressaillit du souffle tiède qu'il crut sentir sur sa nuque. Elle était si bien aux creux de ses bras, sa Simone !



Dans la ronde infatigable des jours, vint un autre matin où le charron s'affaira à nouveau à grand train, triant planches, rabots et poignées. Il fut dès lors accaparé par sa tâche, n'ayant de cesse de terminer ce nouveau cercueil.

— Quand penses-tu cette fois ? Osa madeleine, un soir qu'elle trouvait son mari harassé, dans le petit lavoir familial accolé à l'atelier, jetant un broc d'eau dans une bassine d'étain pour se rincer les bras et la figure.

L'homme prit le temps de se sécher le visage, un début de barbe grise accrochait quelques mailles à la serviette et peluchait sur sa vieille face abattue.

— D'ici la fin de la semaine, peut-être pourrais-tu voir à fournir une belle toile fleurie ? Dans les verts, ceux des étangs et des mares, ça serait parfait, je crois.

— Je regarderai chez Luce, elle a toujours de beaux coupons, ne t'inquiète pas. Viens te restaurer, tu as une mine blafarde, mon pauvre ami, il faut te reposer un peu.

— J'ai bien peur d'avoir un jour à me reposer définitivement. Il la regarda en coin, mi sérieux, mi taquin.

— Comme nous tous, Gabriel, as-tu peur ?

— Qui sera là pour ma dernière demeure ? Qui saura ? Qui avoyera la lame de la scie pour épargner les bois ? Qui ? Qui fera aller et venir le fer sur le plat du chêne ? Qui arasera les planches ? Gabriel gémit.

— Tu le sauras, mon ami, crois-moi, répondit-elle confiante, tu sauras cette chose-là comme tu as su tant d'autres depuis… Madeleine ne finit pas sa phrase. Elle tendit la main à son mari, par solidarité autant que par affection, et ils passèrent ensemble le seuil de la maison, oublieux de ce qui gisait inachevé, dans l'atelier.
Deux jours plus tard, les cloches sonnèrent le glas en l'honneur de la secrétaire de mairie. La pauvre femme après un mois de souffrance, suite à une infection bacillaire, s'en était allée rejoindre les anges, ou tout autre Saint voulant bien l'accueillir, elle était fort dévote. Elle adorait les nénuphars, et derrière chez elle, dans sa cour, passait des heures à lorgner quelques petites carpes, au fond d'une mare en plastique.
La veille des funérailles, la famille avait chaleureusement remercié Gabriel pour son magnifique travail.

— Vous êtes plus qu'un simple fabricant de cercueils, lui affirma un lointain cousin endeuillé, serrant sa main à rompre.

À n'en point douter, approuva le charron en son for intérieur, s'éloignant après avoir jeté un œil sur la dame roide et belle dans son coffre d'émeraude.



Parfois, des mois passaient sans décès annoncée au sein de la petite communauté. La vie semblait alors plus supportable au vieux couple. Il s'abandonnait aux réminiscences d'un passé heureux, tournait quelques pages d'un vieil album de photographies jaunies, et pleurait parfois, pleurait souvent, laissant de fines coulées salées sur le granit rose. Gabriel fabriquait encore quelques belles charrettes, bien que plus petites car les gens se tournaient vers les engins à moteur, pétaradants, grossiers, où l'aubier ne parlait pas sous la charge.
Puis vint ce matin où une maussade grisaille reçu le charron à son réveil. Madeleine, au fourneau, apprêtait des crêpes, la poêle chaude grésillante de beurre posée sur la cuisinière ronronnante. Elle jeta une louchée de pâte pâlotte en apercevant débouler son mari du fond de la chambre.

— Ça sent bon ! Huma-t-il.

— Et hop ! Une pas bien cuite ! Constata la femme déçue de sa prestation.

— Donne, dit l'homme en tricot de corps, s'asseyant lourdement sur sa chaise, j'aime bien !

Madeleine lança une nouvelle ration de pâte dans la poêle, les contours brunirent rapidement, elle retourna prestement le disque où cloquait déjà la pâte brûlée.

— Aie ! Celle-ci est bien trop cuite, j'ai peur, s'exaspéra la vieille femme, tandis que son mari, philosophe et aguerri à ce jeu de passes depuis des lustres, lui renvoyait un

— Donne ! Sonore et tendait son assiette, triomphant. « J'aime bien quand elles sont bien cuites !»

Le jeu se poursuivit jusqu'à épuisement des appétits, et Gabriel rejoignit la boutique.

Bientôt, hélas, le lent chuintement du rabot se fit entendre, et Madeleine dressa l'oreille, frissonna, serra son châle autour d'elle, puis entreprit de faire briller son argenterie.

La question vint pourtant, un peu plus tard, inévitable.

— Que dois-je préparer cette fois ? Demanda-t-elle, la gorge nouée, lorsque son mari avant de pénétrer dans la maison, fourbu, frappa son béret sur la jambe pour en ôter la sciure.

— Noir, un noir de jais, une matière confortable, lui confia-t-il, la fixant de ses yeux vides, exténué par l'ouvrage.

— Noir ? Elle sursauta, comme blessée par l'évocation de cette couleur qu'elle même portait avec constance. « Mais jamais jusqu'ici tu n'as garni un cercueil de noir. C'est presque une aberration, tu ne trouves pas ? »

— Je ne sais pas, c'est ce que je vois pour ce coffre-ci.

Elle l'observait en coin, il gigotait sur sa chaise, tel un grand enfant maladroit, mal à l'aise.

— Tu ne me dis pas tout Gabriel ! Ajouta-t-elle fermement. C'était un petit bout de femme frêle d'apparence, mais d'un caractère entier, et têtue, têtue jusqu'à la moelle. L'homme su qu'il n'échapperait pas à son inquisition réprobatrice.

— Ne me demande pas d'expliquer un phénomène incompréhensible, je t'en prie, ne me le demande pas.

Il s'offrit le luxe de s'éloigner un instant, l'instant où il poussait la porte de la chambre de Simone. Elle était paisiblement allongée sur son lit, inondée de soleil, suçotant un bonbon, un vieux livre de Martine, corné aux coins, étalé devant elle.

— Papa ? Elle sourit en lui tendant les bras.

— Gabriel ? Tu m'écoutes ? S'enquit Madeleine inquiète soudain de l’immobilité glacée de son mari.

— […]

Et il se retrouva brutalement dans la cuisine, le feu pétillait, un ragoût mijotait sur la fonte, le tic-tac de la vieille horloge égrenait le temps, indifférent aux tracas des hommes.

— Oui, poursuivit-il, fourrageant dans sa large poche, « essaie de me trouver le garnissage, s'il te plaît Madeleine, il me reste encore tant à faire ces jours-ci »

Deux journées durant, le charron s'activa sur son labeur, perdant l'appétit, se ratatinant d'heure en heure comme si le polissage du bois lui ôtait chair et vie. Madeleine prit peur. Elle profita d'une livraison que Gabriel devait honorer et se rendit jusqu'à la boutique. Elle hésita. Son corps ne le voulait pas, son cœur encore moins. Mais elle poussa enfin la lourde porte d'atelier, faites de bois et de carreaux de verre polis dont l'éclat se réfléchissait à l’intérieur dans un prisme multicolore enchanteur. Elle pénétra dans la boutique à tout petits pas, mue par une volonté plus forte que sa peine et son désarroi.
Sa vision se brouilla dès le seuil franchit, il n'y avait rien en route à l'intérieur, pas une planche, pas un outil dérangé. Au fond, sur les tréteaux, un petit cercueil blanc lui faisait face. Elle jeta un cri, se refusa à avancer, les larmes coulant sur ses joues, puis s'y résolut enfin, pétrissant dans ses mains le coin de son mouchoir soudain apparu.

— Simone, appela-t-elle tout bas, mais le silence seul lui répondit.

Elle approcha encore. L'enfant reposait sur un lit d'or, bouton d'or. Sa couleur préférée. Elle semblait faire une sieste. Simone.

Madeleine sursauta vivement. la vision disparue. Charbon, le chat, se frottait contre sa jambe et ronronnait doucement. L'atelier était noyé dans l'ombre. La vieille femme s'ébroua, dispersant ses souvenirs,mais des larmes coulaient encore.
Posées sur l'établi, quatre courtes planches, d'une finition parfaite apparurent à sa vue. Elles étaient prêtes pour l'assemblage. Les poignées, argentées, brillaient non loin de là dans la pénombre. Madeleine s'approcha, hypnotisée, et caressa le bois clair, lisse, doux, chaud, vivant presque. Quatre si petites planches. Elle ne put en supporter d'avantage et fit demi-tour le sang battant ses tempes, submergée de tristesse. Mais cette commande l'intriguait.

— Auras-tu fini bientôt ? Demanda la vieille dame alors que l’angélus sonnait ce soir-là.

— Demain sans doute, répondit le charron d'une voix sans timbre.

— C'est un enfant ? Questionna-t-elle encore, servant la soupe au dîner, le lendemain même.

— Probablement. Gabriel repoussa son assiette, jeta sa serviette qui chut au pied de la table et sortit dans la nuit.

Sa femme l'entendit rentrer très tard, la lune était levée et trottinait dans le ciel étoilé. Il traversa sa chambre, le parquet grinça malgré ses efforts de discrétion, ouvrit la porte de sa propre chambre, celle du fond, qui donnait dans la cour. Elle l'entendit encore déposer ses vêtements, sur la chaise, près du lit. Les pinces des bretelles cliquetèrent, il retira ses chaussures, s’allongea, le lit gémit sous son poids, puis le silence se fit. Madeleine attendit encore. Un doux ronflement s'éleva bientôt. Elle se leva alors, emmitouflée de nuit, fantôme menu, glissant sur le parquet qui ne la trahit pas.


— Madeleine ?

La cuisinière ne ronflait pas lorsque le charron se leva, et aucune odeur de pain frais ou de café ne se répandait dans la pièce. Le pressentiment qui le taraudait depuis quelques jours le fit se précipiter hors de chez lui jusque dans la boutique, à peine le bleu de travail enfilé. Il poussa un cri de bête en découvrant sa femme dans le petit cercueil. Elle ne respirait plus et pourtant, son fin visage aux rides creusées à force de courage et d'amour semblait heureux, serein pour la première fois depuis le décès de leur fille bien aimée.
La vieille dame reposait, tout de noir et de dentelles, sur un linceul bouton d'or. Ses mains jointes, sur son giron, tenaient un petit bout de papier, un de ceux qu'elle griffonnait pour noter une course ou faire un compte pour un client.

— Gabriel, aujourd'hui, tu ne mettras pas d'enfant dans ce coffre, c'est ma volonté, et le vœu le plus cher à mon cœur serait que plus jamais, tes belles mains si habiles ne dressent une seule planche en vue d'y coucher quelqu'un. —

Et qui sait ? Le destin s'est peut-être lassé du tour joué au pauvre Gabriel, après celui que Madeleine lui consentit. Et peut-être la malédiction du charron cessa-t-elle, pour de bon.
