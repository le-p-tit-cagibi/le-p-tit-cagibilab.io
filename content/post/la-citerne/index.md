---
title: La Citerne
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Un peuple caché. Un secret. Sera t-il bien gardé ? "
epub: true
pdf: true
draft: true
---
Je ne crois pas à la destinée. Heureusement, parce que si on m'avait dit : « Massimo, mon brave couillon, tu vas te retrouver piéger au fond d'un trou à rat pour avoir voulu reprendre ta vie en main et te faire un peu de fric », non, je l'aurai pas cru. La vie n'est qu'un heureux ou un malheureux hasard. la mienne a une prédilection pour tout ce qui est mal quelque chose.

J'aurai pu me méfier — J'aurai dû me méfier —.

Anthony (mon pote de toujours) m'avait prévenu, après avoir feuilleter l'article que je lui tendais l'air finaud.

— Ce patelin, qu'il m'a dit, « son histoire ressemble à plein d'autres du même acabit. C'est un peu comme un train fantôme, dans un parc d'attraction abandonné, mec, il est rayé de la carte depuis pff…
Il avait semblé réfléchir intensément, tripatouillant sa moustache grise comme lui seul sait le faire — au bout du compte, les extrémités rebiquent et ça lui donne un faux air de Dalí — Vous connaissez non ? Personnellement, je suis hermétique à ce genre de trucs ; des montres molles, bon, mais de là à ce que ça révolutionne la planète, un peu de sérieux.

— Depuis des lustres » avait-il reprit, « et personne n'a envie de prendre un ticket pour monter dedans. Une histoire à dormir debout ! Reprend ton poste à la laverie, mec, tiens-toi à carreau […] J'ai oublié la suite de son baratin. Anthony, avec sa sacoche rose et ses tee shirts blindés de paillettes, parfois, il manque vraiment d'ambition ! Ai-je songé.
J'ai fait ouais, d'accord, d'accord, et on est passé à autre chose.
Sauf que j'y suis tout de même allé dans ce bled : le besoin de tunes, ça se règle pas autrement qu'en se remuant le cul.
Anthony tient un kiosque à journaux au coin de Gilbertroad et Billmy. On se connaît depuis qu'on est gosse. J'aurai juste pu résumer mon plan peu avant mon départ, si des fois il voulait en être. Mais je revoyais sa mine qui ne fleurait pas bon l'enthousiasme, et ça m'a dissuadé de m'épancher. Il aurait balayé tout ça d'un autre — pff — et conclut, presque sérieux, — plan foireux mec ! —. Il disait toujours, ça, Anthony de toute façon, prudent comme un serpent ! Alors j'ai rien dit.
Mais ça ne m'a pas découragé.

Cette idée me trottait dans la tête depuis un petit moment À dire vrai depuis qu'un article m'était tombé sous le nez, dans un bistrot où je passais le temps (à l'époque, j'en avais beaucoup du temps). Dans la feuille de chou dégotté sur le comptoir, le journaliste, un certain Guy Naisse racontait l'histoire d'un village, Rochevertu, situé en bord de mer, dans un coin vraiment paumé de notre vaste pays, où les habitants avaient fini pas déguerpir à force d'être assailli par des chercheurs de météorites, prospecteurs, curieux, collectionneurs avaient débarqué dans le patelin et tout mis à sac.
Ouais, je vois d'ici votre tête — un peu celle d'Anthony, le nez pincé, la bouche en cul de poule —dis comme ça, je vous l'accorde, ça paraît pas transcendant cette affaire. Mais je suis pas débile. (Quoique ma situation actuelle pousse un chouilla à me contredire, on y viendra).
L'article original datait des années 70, et ressortait à cause de l'actualité : ce fameux village et ses environs (falaises, prairies, rivages) s'inscrivaient désormais dans une zone naturelle protégée. Les derniers habitants avaient été expropriés. L'article ajoutait que ces cailloux avaient la particularité était de contenir des métaux précieux.
Quand j'ai lu « métaux précieux » ça a fait — pop ! Pop ! — dans ma tête, comme un sachet de maïs à pop-corn qu'on jette dans une poêle chaude et huilée. Je m'y connais un peu, figurez-vous, en gemmologie. Quand j'étais en villégiature, (comprenez ce que vous voudrez, mais je ne suis pas un si mauvais garçon) j'ai eu le temps de reprendre mes études, ce que j'ai fait, et j'ai étudié la géologie, parce que je pensais que la pierre, ça avait de l'avenir. En fait ça dépend des pierres. Comme le temps ne m'était pas compté, j'ai poursuivi avec une spécialisation, gemmologie, puis une autre, lapidaire. En pratique, ça veut dire que tu sais reconnaître les pierres précieuses, leurs origines, et leur valeur. Tu sais les travailler, les tailler. Bon, sur le moment, ça ne m'a pas apporté grand-chose, que m'éviter l'ennui, et par la suite, encore moins, parce que personne ne veut embaucher un type avec mon pedigree dans une bijouterie, et encore moins chez un diamantaire.
Mauvais choix me direz-vous. La vie est faites de ça.
Mais bon, ça m'a titillé cet article. Je vous la résume en gros, depuis le début. Dans les années 1900, une pluie de météorites s'abat sur ce patelin À l'époque, ça n'a intéressé personne, sauf les chasseurs de curiosités, très à la mode avec cabinets de curiosités. Les villageois, pas commodes, ont eut vite fait de mettre les intrus au pas, d'après le peu d'indice que j'ai récolté. Difficile de trouver des informations là-dessus. Les météorites (de 1900) prélevées sur le site, ne sont jamais réapparues sauf une (vous allez comprendre). On a même parlé d'une malédiction, beaucoup de ceux qui avait trouvé ces cailloux ont eu des fins de vie brutales ou difficiles —La malédiction de Rochevertu —. Ça ne valait certainement pas celle de Toutankhamon, hormis quelques entrefilets par-ci par-là dans la presse, et puis l'histoire se termine comme un pétard mouillé. On oublie.
Mais plus tard, peu avant le premier choc pétrolier, des petites malins déterrent le pot aux roses suite à la vente d'une de ces pierres lors d'une banale succession : La météorite est d'un type rare (métalloïdes, mais ça ne vous avancera pas vraiment de le savoir, je vous la fais courte) et elle recèle des métaux précieux, comme l'iridium, un pourcentage élevé.
Qui dit rare dit cher, je ne vous apprends rien là.
Donc nouvelle ruée vers le village. Mais le mystère se corse car tous ces fouineurs en sont pour leurs frais : Rien absolument rien n'est découvert sur le territoire de Rochevertu. En 1974, il ne reste pas une trace de la présence de ces météorites alors que les pionniers, quasiment un siècle auparavant, en avait récolté des mannes (d'après de rares témoignages) — d'ailleurs pour ça que les culs-terreux du bled s'étaient empressés de chasser tout le monde —.
Mais (sans — mais — les histoires sont un brin moins attrayantes) ces pécores n'en ont pas tiré profit eux-mêmes, ni eux, ni leurs descendances, non. Là est sans doute aussi une partie surprenante de l'histoire. Bien au contraire, le village a subi dès lors un déclin spectaculaire et irréversible. On l'oublie.
Aujourd'hui, ne reste donc qu'un groupe de bicoques piquées sur un bout de falaise au-dessus de l'océan, et sur les landes alentour, ou planquées quelque part par les habitants, de possible météorites valant… leur pesant d'or (en vérité bien plus !) si on sait où et quoi chercher.
Elles ne peuvent pas avoir disparues d'un claquement de doigts À d'autres ! 
Pas débile, Massimo, vous étiez prévenus !

De retour parmi les gens, je veux dire les gens comme vous, je manquai cruellement de liquidités, et L'IDEE fit lentement jour dans ma caboche, pendant que je m'occupai à droite et à gauche. En fin de compte, le manque de fric, ça n'aide pas à la réinsertion. Je ressassai cet article, poursuivis un peu mes recherches, et finalement j'arrêtai mon plan.
Toutes voiles dehors sur Rochevertu, le patelin qui allait me rendre riche ! 
Sauf que rien ne se passe jamais comme prévu. Plan foireux, m'avait promis Anthony. Mec, t'avais raison. Je donnerai tout ce que j'ai pour revoir ta tronche. Je suis riche, en un sens, elles sont là…
Mon instinct ne m'a pas trompé, ces paysans cachaient leur trésor, et le couvait, plus sérieusement déterminés qu'une poule sur son nid ! 
Mais surprise !
Ah oui… le village est loin d'être sans défense ! (J'y viendrai…) 

En attendant, elles sont là, mes précieuses, certainement des centaines de météorites — la plus grosse ne dépasse pas la taille d'un ballon de basket —. Elle tapissent le fond de la citerne et pulsent légèrement, diffusant une lumière froide et bleue, un bleu céruléen.
Hélas, mes petites copines, sont là aussi (mes hôtes, si vous préférez), elles flottent tout autour de moi, sans hâte, me tâtant de leurs fines cerates cérulées fichées au bout de leurs longs bras caoutchouteux. Ces papouilles m'indisposent grandement, parce que trop nombreuses, impudiques, insistantes et… je dirai, gourmandes. Ça me fait l'effet du boucher qui tapote affectueusement Grasdouble le cochon, avant de lui assener un coup de surin fatal.
Mes hôtes sont des limaliens, comme les autochtones les appellent. Pour visualisez, pensez monstrueuses limaces bleues, dotées de bras que prolongent des filaments, avec un sac à air qui pendouille sur l'abdomen, et vous aurez, la trouille en moins, approximativement la même vision que moi. Sauf que vous, vous êtes à l'extérieur, et moi à l'intérieur. Dans la citerne. Piégé et foutu.
Mes nouvelles petites amies sont des créatures déroutantes : elles vadrouillent autour de moi, exploratrices attentives et polies. Leurs yeux aveugles (qui ressemblent plutôt à des cavités type ouïes, si vous voulez mon avis) se contractent à un rythme lent et régulier. Elles ne semblent ni pressées, ni inquiètes de mon intrusion (ou plutôt du fait qu'on m'est balancé manu militari dans leur refuge). L'air est saturée d'une odeur bizarre, et ça picote, un peu comme quand on ouvre une bouteille de coca, vous voyez ? Elles flottent.
Et je suis leur prisonnier.
Parce qu'il n'est pas question de m'échapper, j'ai bien compris ça tout seul, même si les deux costauds qui m'ont enchaîné là se sont chargés de me l'expliquer. Ils sont remplis d'arrogance et d'assurance tranquilles ces deux-là. D'ailleurs, j'ai toujours mon smartphone, preuve que ces villageois ne se font pas une once de soucis quand à une éventuelle évasion. Cela vous vaut de lire ma tragique aventure, à défaut de rencontrer le protagoniste en chair et en os. Je me dois de passer utilement mes derniers instants.
— Pas de réseau —, Évidemment.
Le secret restera secret ! Garanti !
Au fond de la citerne.


Dire que c'est la première chose que j'ai aperçu en atteignant le village ! 
On peut pas la louper : Elle est énorme ! Reposant sur de larges empattements rouillés, des inscriptions bleues illisibles ombrent ses flancs rebondis.
Je m'étais équipé un peu pour venir et pouvoir rester sur place quelques jours : quelques outils, tente, réchaud, sachets de nourriture déshydratées. Je m’aperçus en déballant mes affaires le long d'une grange encore en bon état, à l'abri des regards, que j'avais malencontreusement oublié d'emporter de l'eau…
Pas grave, me dis-je, il devait bien y avoir une source ou un puits dans les environs…
J'arpentais le village, quadrillant mentalement la zone à prospecter. Quatre fermettes constituaient les principales habitations, deux granges, des appentis, une éolienne (voilà mon problème d'eau résolu, supposai-je) et une citerne s'arrangeaient tout autour. C'était une sensation étrange que parcourir les quelques ruelles désertes, parce que les habitations étaient très bien conservées, l'expropriation datait de peu, et j'éprouvai un léger sentiment de gêne à l'idée de violer cette intimité qu'on sentait encore bien présente dans ce hameau. Je décidai de ne rentrer dans aucune maison tant que mes recherches auraient une chance d'aboutir dehors.
Ce premier soir, sûr que mes pieds avaient parcourus au moins quarante bornes. Mon estomac beuglait comme une vache au fond d'une étable et je rejoignis mon petit campement avec plaisir. J'étais de bonne humeur, mais de météorites, je n'en avais pas dégotté la moindre trace. Je suivrai mon plan, me dis-je optimiste. Il faudrait aussi que j'inspecte les alentours du patelin, car bien que la pluie de météorites s'était peu étendue, il était tout de même possible que certaines soient dans un rayon, disons, de deux ou trois kilomètres. Ce serait la partie la plus dangereuse du plan, il fallait absolument que je passe inaperçu, au risque de me faire virer, ou dénoncer, ce qui revenait au même : je ne pourrai plus revenir. Donc, faire profil bas. Je n'étais pas inquiet, le coin était paumé à souhait, sauf quelques geais qui n'avaient pas arrêter de jacter dans mes oreilles à chacun de mes déplacements. Saletés de bestioles. Jamais compris ces péquenots qui supportent ces moineaux à piailler sous leurs fenêtres, et qui les nourrissent par dessus le marché !
J'optais pour des nouilles chinoises, une soupe de poisson et croquait dans mon pain de mie avec un bel appétit. Le ciel, débarrassé de la pollution urbaine, offrait une jolie voûte étoilée que j'admirai un moment, puis une fois installé dans mon sac de couchage, je m'enfonçai paisiblement dans le sommeil.

Quand j'y repense, je n'ai rien vu venir, pourtant ces enfoirés ont du m'espionner toute la journée. Première erreur : pourquoi n'ai-je pas fouillé les baraques ? Pourquoi ? 
Une fois dans ma vie, je fais preuve d'un peu d'éthique (on pourrait voir ça comme ça, non ?) et ça me retombe dessus direct.
Deuxième erreur : je n'avais rien emporter pour me défendre. Mais n'étais-je pas sensé trouvé un village abandonné ? 
Anthony me serinerait, s'il savait que j'ai fait preuve de tant de négligences. Peut-être va-t-il s'inquiéter de ne pas me voir me pointer ? 
Non, il s'est jamais inquiété de rien. Ni de personne.

Au réveil, grosse surprise, je suis ligoté sur un rocking-chair branlant, à l'intérieur d'une cuisine. Il y à un vieux poêle à bois, deux buffets qui datent, peints en blanc mais le blanc est largement passé, ils sont malgré tout en bonne état, d'ailleurs la pièce elle-même dégage une impression d'occupation récente.
Je passe la matinée peut-être, à m’énerver et me débattre sur ce vieux siège, qui s'est révélé bien plus solide que je ne l'aurai cru. Je finis même par me laisser tomber à terre, comme on le voit faire dans les films (ça marche toujours dans les films) pour me libérer plus facilement, mais c'est la déconvenue. Impossible de desserrer cette corde entortillée autour de moi.
Saucissonné Massimo !
J'en ai eu marre de lutter, quelqu'un finirait bien par se montrer, je me suis dis. Alors j'ai attendu, guettant le moindre bruit, et au bout du compte, je me suis endormi.

Le réveil suivant est carrément désagréable : une main avait saisit mes cheveux (j'ai toujours aimé les porter longs) et deux autres attrapé le fauteuil qui se redresse sans que personne ne pipe mot.
Nous nous retrouvons assez brutalement face à face.
Deux types bâtis comme des armoires à glace. L'un plus large que haut, et le second inversement. Ce qui m'a surpris, c'est leur ressemblance frappante, des frères, au moins. Une face carré, juvénile, des yeux haut placés, un très grand front et une implantation de cheveu triangulaire sur l'avant, qui le rend rare. Une bouche boudeuse d'enfant gâté, si vous voulez mon avis (bien que sur le sujet des gosses, à par ceux de ma sœur, qui sont d'affreuses créatures geignardes, j'y connais que dalle) et un pif que ce brave Cyrano aurait pu adopter sans hésitation ! Une seule chose m'a fait vraiment tiquer dans leur dégaine : leur teint, laiteux, luisant, maladif, repoussant.
— Oui, repoussant —.

L'interrogatoire commence, j'étais résolu à me faire passer pour un promeneur perdu, un peu stupide, histoire que ces squatteurs me lâchent la grappe. Mais dès les premiers mots, tout a dérapé. C'étaient assurément pas des squatteurs, et j'étais démasqué.

— Qui sait que t'es ici ?

Pour le coup, je peux répondre sans mentir ou au contraire, 

— Un ami et ma famille, j'ai répondu pensant que ça pourrait faire pencher la balance en ma faveur.

— Mon cul oui ! Le plus large a répondu.

— Qu'est-ce que tu cherches ?

— Rien, je me suis perdu, je me baladais, c'est tout.

— T'as pas une gueule de touriste, toi ! 

La version longiligne m'a craché sa répartie au visage sans la moindre empathie.

— Racontes ! Ajoute-t-il, me menaçant du poing.

Ah ! On va en venir aux coups, me dis-je, bien. J'encaisse plutôt pas mal. Massimo, c'est pas un jeunot. Des trempes il en a pris plus qu'à son tour.
Là encore, je me fourrai le doigt dans l’œil.
Le grand, qui tient toujours ma tignasse, m'oblige à le regarder, et il approche son visage près du mien, plus près, encore plus près. Je me dis qu'on pourrait se rouler une pelle tellement nos têtes se sont rapprochées. Et puis la scène tourne au cauchemar, ses yeux, qu'il a grands ouverts, se retournent d'un coup, et seul leur fond blanc semble me fixer. C'est très flippant.
L'autre n'a pas bougé.
Son copain approche encore son visage : son front touche le mien. Je sens sa moiteur qui se colle à ma peau, et son souffle, imperceptible, qui pourtant effleure mon nez, mes paupières, mes lèvres. Vrai, je me mets à trembler. De son nez sortent des cerates bleues, qui s'allongent et s'allongent encore, encore, et encore. Elles commencent à palper mon visage. Je ferme instinctivement la bouche, j'aimerai verrouiller mes narines aussi, mes oreilles, je veux pas que ces machins pénètrent dans quelque partie de mon intimité qui soit.

— Il cherche les météorites ce con !

C'est le gros qui a parlé, son copain semble aux abonnés absents.

Je pense très fort à la laverie. Merde ! Pourquoi je suis pas allé bosser ce matin ? POURQUOI ?

— Trop tard pour les regrets, et d'ailleurs personne te regretteras, à ce que je vois, il dit.
Je sais que c'est débile, mais je ne peux pas empêcher une putain de question de jaillir de ma bouche.
— Vous lisez dans mes pensées ou quoi ?

— Bien mieux, mais l'image est amusante. Alors tu croyais vraiment devenir riche en débarquant ici, et rafler le magot ?

Je me tais. Il vient tout de même d'avouer qu'il y a bien des pierres quelque part. Je le savais !

— Le savoir est une chose, mais après ? L’armoire se marre.

Son copain m'ausculte toujours le visage, comme un chien qui vous lécherait la figure, mais sans la bave, c'est étrange. Ça fait pas mal.

— Pas encore, l'autre a repris son sérieux et s'adresse à son copain.

— C'est bon Loïque, assez joué. On sait ce qu'on voulait savoir.

— Je… les mots ont du mal à sortir. « Je vous promets de repartir, et vous n'entendrez plus parler de moi. Je disparais ! »

Le maigre a repris un visage humain, enfin, presque, il a encore un bout de tentacule qui dépasse d'une de ses narines. Obscène.

— Ça c'est sûr, tu vas disparaître, abruti !

Je viens d'entendre ma condamnation à mort.
Massimo, mon pote, c'est la fin de la route, je me dis. Ma langue se délie, peut-être parce que j'ai plus rien à perdre.

— Où sont les météorites ? Pourquoi vous les cachez ? Qui êtes vous ?

— Oh oh ! Ben dis donc, en voilà un interrogatoire, tu te prends pour un flic ? C'est le prénommé Loïque qui a parlé, il fait un signe à son complice.

— Carlo et moi, on va te raconter une histoire, vraie et un peu triste, prête attention Massimo, grosse couille molle.

— Merde comment il connaît mon prénom ? Me demandai-je — je sais, c'était complètement futile — mais c'est fou ce qui vous traverse l'esprit quand vous devinez que vous n’atteindrez pas les cent ans, ni même quatre-vingt.

— Alors, il était une fois, commence Carlo, un village sous une pluie de feu, une nuit d'il y a bien longtemps, enfin, pour vous, précise-t-il, « il y a un siècle. Au matin, les braves villageois allèrent cueillir quelques-unes de ces joyeusetés, noires et moches, il faut convenir qu'elles ne payaient pas de mine, mais ils s'en désintéressèrent rapidement, occupés qu'ils étaient à tomber malades, infectés par un hôte discret et peu scrupuleux, de la grande famille des limaliens »

J'ouvre des yeux comme des soucoupes.

— Eh oui, Massimo, continue le maigrichon, ils y passèrent tous, figures-toi, sauf, devine !

Loïque qui s'était redressé, se penche à nouveau au-dessus de ma figure.
Je me tais, dévoré par la peur à présent. Mon cerveau fonctionne mieux qu'un IBM à ce moment-là. J'évalue mes chances d'en sortir indemne.
Leur histoire tient pas debout, je me dis, quelqu’un se serait aperçu de quelque chose tout de même ! Tentai-je de me raisonner.
Et comme s'il avait entendu mon questionnement, le dénommé Carlo, poursuit le récit.
Mon ventre me fait mal comme si j'avais avalé des pavés, je me mets à transpirer.

— Il y passèrent tous, ces braves gens, mais en douceur. Dans votre monde humain, le décompte du temps est si … si… petit (il mime avec son pouce et son index). Personne ne soupçonna rien : Un village de plus victime de votre urbanisation.

Et nous sommes devenus les gardiens du temple. 

Ils s’esclaffent dans un bel ensemble.

— Mais… e m'étouffe et perd les pédales, me débattant sur le fauteuil auquel je suis cloué. Mes liens se resserrent, je n'y gagne rien sauf d'avoir mal.

— Mais… rien du tout, Massimo, rebondit le plus balèze, le temps passe et vous, vous ne voyez rien ! Vous poursuivez vos petites vies médiocres à laver des carreaux ! Nos hôtes ont donc gentiment pris leur quartier, ce sont un peu comme des explorateurs, vois-tu, ils voyagent dans leurs cailloux, sans plan précis, et s'ils peuvent, ils s'installent là où le vent les pousse, enfin, les jette, si je puis me permettre cette métaphore osée.

Il sourit, et je résume atterré :

— Des envahisseurs !

Je me dis que je vais convulser, tout mon corps semble battre la breloque.

— Bah pas vraiment, tu vois, pas de bol, nos amis n'ont pas d'avenir ici, notre belle planète bleue n'est pas une terre d'accueil pour eux. Un comble.

Je respire un peu plus facilement à cet aveu, ce qui est idiot, ça n'arrange en rien ma situation.

— Mais du coup, ils sont pas mal en rogne, impossible de mettre les voiless, avant que notre planète parte en morceau et les expédie ailleurs dans le grand cosmos !

Je déglutie. Ces deux types… ils sont…

— Vivants, pas peur, mec, on est vivant, simplement, cette infection nous a causé quelque dérèglement génétique que nos invités n'ont pas pu réparé. D'un autre coté, on y a gagné un sacré sursis. On fait de beaux centenaires non ? 

Sûr que je leur donne à tout prendre, entre 25 et 30 ans.

— Où sont-ils ? J'ai hurlé alors que mon esprit déraille, « je dirai rien, je vais leur expliquer !»

— Tut, tut, tut, on a une solution plus radical, tu vas voir, me lance Loïque sur le ton de la confidence.

— Oui, tu vas voir, Massimo, ça va te plaire. Carlo me donne une claque sur l'épaule et commence à me détacher.

Je n'aurai qu'une seule chance, me suis-je dis à ce moment-là, prêt à bondir, à mordre, à courir, bref, prêt à n'importe quoi.
Mais Loïque a brandi une bûche et je n'ai pas porté plus loin ma réflexion, il l'avait déjà abattu sur mon crâne.

J'émerge tandis qu'ils me transbordent dans la citerne. Loïque, déjà à l’intérieur, soutient mon corps flasque pour soulager Carlo qui me pousse comme un sac de grains par l'étroite ouverture À l'intérieur, une pénombre bleuté m'éblouit et je ne distingue tout d'abord rien. Ensuite ils viennent à nous. Les limaliens. Reposaient-ils au fond de la cuve, flottaient-ils déjà ? Je ne saurai le dire, mais soudain, ils nous cernent de toutes parts. Mes agresseurs n'y prêtent aucune attention, s'affairant à m’enchaîner comme un esclave au fond d'une cale de galère. Les bestioles se désintéressent d'eux d'ailleurs, et je semble être au centre de leur attention. Elles commencent leur investigations, les cerates frémissantes, et ma peau se hérisse de dégoût sous leurs chatouillis insistants.
Je suis de nouveau tout à fait conscient, et mon avenir se dessine froidement devant moi. Ces deux cinglés vont m'abandonner ici !

— T'as tout compris, mon pote, ricane Carlo, comment crois-tu que nous matons tous les indésirables dans ton genre ? 

— Vous pouvez pas me laisser là ! Détachez moi ! Je secoue mes chaînes comme un fou, et hurle à plein poumons.

Ils n'en sont guère émus.

— Tu peux t'égosiller tant que tu voudras, personne ne viendra. Personne ne vient jamais, Massimo. Loïque sourit.

— Nos amies vont prendre grand soin de toi. Elles vont t'adorer !

Je me mets à sangloter, à supplier, sans plus de résultat. Dès qu'ils ont vérifié que je ne peux en aucun cas me libérer, ils s'éloignent, remontent à l'échelle.

— Mais enfin ces machins sont quand même pas planqués là-dedans depuis plus de cent ans ! Dernier baroud d'honneur, je veux être sûr de comprendre ce qui m'a amené là. Mortelle virée.

— Que si ! Et les limaliens attendront le temps qu'il faudra. Ils sont patients, c'est leur force. Ils mangent peu, se contentent de ce que nous leur apportons, et ici, ils sont à l'abri.

— Ce sera ta contribution à leur survie, conclue Carlo.

— Bye bye Massimo, ajoute Loïque, en refermant le couvercle de la citerne.

Ces deniers mots résonnent longtemps dans ce bleu étranger.

Les limaliens, eux, prennent leur temps — j'ai compris leur manège —, ils savourent. Et moi, moi je vous raconte tout ça en attendant de passer à la casserole, à moins que je ne perde la raison avant. Elles se collent sur moi, ces saloperies, je les sens déjà qui me goûtent.
Aidez moi !

Le téléphone termine sa course au fond de la citerne, il y en a quelques autres déjà, ainsi que quelques vieux débris méconnaissables. Massimo n'a plus de quoi tenir le smartphone entre ses doigts qui se dissolvent doucement, et ses forces vives l'abandonnent. Il vit ses derniers longs instants dans d'atroces souffrances, lentement, très lentement digéré par les limaliens.
Dehors, le village, vide de tout habitant a de nouveau cessé d'être, figés dans une sorte d'immuabilité.

 


