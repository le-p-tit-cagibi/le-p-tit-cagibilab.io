---
title: Les invisibles; Service public
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: ""
epub: true
pdf: true
draft: false
---
Une vieille guimbarde apparut au bout du chemin, elle cahotait, crachant une fumée noire par son pot d'échappement percé. Elle ne ralentit nullement en me voyant, — un rap furieux accompagnait ses cahotements —, et arriva à ma hauteur. Je me rencognai sur le côté. Deux jeunes hommes, à l'arrière de l'habitacle, accoudés aux fenêtres, fumaient des joints gros comme des madriers et me hurlèrent des mots incompréhensibles entrecoupés de sifflements, de rires qu'accompagnaient de peu subtils gestes obscènes. Je n'hésitai pas une seconde et m'enfonçai dans le taillis, malgré les orties qui le bordait. Les bruits de voix et de musique décrurent rapidement.

Le danger, si jamais danger il y avait eu, était passé.

Alléluia !

Je me concentrai sur ma course, sur ce que j'étais venue faire là, et reléguai ces abrutis en voiture à l'emplacement qui leur convenait le mieux, dans l'état d'esprit où je me trouvais : le néant.

Je maintenais un bon rythme de foulée, — le sous-bois s'était rapidement clairsemé — et je progressai sans difficulté, sans trop m'essouffler. Mes muscles fonctionnaient à merveille, et la pompe qui alimentait le tout battaient sans excès dans mes tempes, quand je la vis qui descendait la pente que je m’apprêtais moi-même à grimper. Elle avait les cheveux en bataille et son corsage à moitié déchiré pendait tel un drapeau en berne. Ses pieds chaussés de sandalettes pailletées dérapaient sur les graviers, alors qu'elle tentait de s'aider de ses mains, labourant frénétiquement le sol meuble. Elle ne m'entendit pas, tandis que je l'observais déjà, mais finit par lever la tête, les jambes en crabe, dans l'espoir tenu de s'accrocher à des tiges de lierre tapissant le sol pentu un peu plus haut.

Son regard accrocha le mien et ses yeux parurent sortir de leurs orbites, Je reconnus ce sentiment qu'elle affichait dans un cri muet : la peur.

J’avisai son cou, portant une large trace d'un rouge violacé, au-dessus de la pomme d’Adam, et son arcade gauche gonflée. Elle bavait un sang mêlé de morve, ses genoux aussi pissaient de rouge, bien que déjà une croûte de terre recouvra le plus laid des plaies. Elle avait salement morflé.

Je mis mes mains sur les genoux, penchée au-dessus d'elle, puis lui tendit. Elle hurla, patinant pieds et mains confondus dans l'espoir incertain de faire demi-tour sans se vautrer.

Son poids emporta sa volonté — pourtant elle ne devait pas dépasser les cinquante kilos — et elle valdingua jusqu'en bas du raidillon, dans un nid de poule composé de vase et de feuilles pourries.

Je m'apprêtai à la rejoindre, le cœur battant à présent comme un vieux moteur 15 cylindres obligé de faire les 24 h du Mans.

Le sort est parfois bien cruel.

Alléluia !

À ce moment retenti le klaxon caractéristique d'un vététiste en goguette. Il m'avait vu, et ralentissait déjà. La fille n'avait pas repris connaissance. Comme le type s'avançait au bord de l'escarpement, il remarqua le bout de ses gambettes, plutôt fines et fermes, ce qui le déstabilisa, et il faillit dégringoler de son VTT. Il se rattrapa de justesse. Et se pencha légèrement, pour prendre la mesure du tableau dans son ensemble.

— Qu'est ce qui se passe ici ? Demanda-t-il en ôtant son casque, — grand bien lui prit —, et jetant son engin sur le bord du chemin.

— J'aimerai le savoir, dis-je.

— Elle est avec vous ? Demanda-t-il encore, commençant à descendre prudemment.

— Pas du tout, je viens de tomber dessus alors que je joggais, répondis-je, tout en l'imitant.

— Elle a pas l'air en forme, constata-t-il à mi-pente.

Je me contentai de la dévisager sans un mot. Son souffle était régulier, son corps immobile, ses yeux fermés.

L'homme, d'une trentaine d'années, à la calvitie prononcée, glissa dans les feuilles gluantes — et puantes, avec cette humidité — et s'affala à côté de la fille, toujours sans réaction. Il entreprit de la secouer.

— Peut-être n'est-ce pas une bonne idée ça, jeune homme, elle a peut-être quelque chose de cassé ! Hasardai-je.

Il me jeta un regard de reproche, comme si j'avais dit une absurdité, et l'installa avec précaution en position PLS, une fois son pouls vérifié.

— Vous ne voyez donc pas qu'elle a été agressée ? Il secoua la tête, et me fit signe avec sa main, « prenez mon téléphone dans ma sacoche, derrière la selle, et appelez les secours ! »

Voilà que je me retrouvais à jouer le rôle de l'empotée. Soit. Et encore Alléluia !

J’acquiesçai sans un mot, et fit mine de remonter. Mon cerveau — en plus de mon cœur —, palpitait à présent à tout rompre.

Un gémissement parvint jusqu'à mes oreilles.

Je rebroussai vivement chemin, m'accroupis, et saisit le pauvre visage tout boursouflé de la fille entre mes mains.

— Ne vous inquiétez pas, ça va aller, lui murmurai-je.

— Faites plutôt ce que je vous dis ! Vous l'étouffez ! Protesta le vététiste.

Desserrant à peine les dents, je me tournai vers cet imbécile et lui assenai vertement ses quatre vérités :

— Dites donc, puisque vous dites qu'elle a été agressée, la pauvre chérie, vous ne croyez pas qu'une présence féminine sera préférable, le temps qu'elle reprenne ses esprits ? Allez donc vous occuper de ce téléphone à la noix ! Je n'en possède pas moi-même, je déteste ces maudits machins ! Allez ouste ! Et j'accompagnai ma diatribe d'un geste sans équivoque.

Il bougonna, regarda la fille qui, la bouche ayant doublé de volume, ne pouvait plus guère émettre que des onomatopées sans signification aucune. Elle semblait par ailleurs abasourdie, mais rien de cassé, sauf ce poignet, peut-être, à moitié caché sous son torse, dont l'angle me paraissait quelque peu aberrant. Je n'aurais cependant pas juré de la véracité de mon diagnostic.

— Ok, je remonte, madame, pas la peine de vous emporter. On est tous tendus là, convint le cycliste amorçant la pente.

— Tendus —, ça oui, tu l'as dit, pensais-je. Puis je me concentrai sur la victime.

— Mais qu'est-ce que vous faites ?

Il ne s'était guère absenté plus d'une minute.

— J'essaie de la réanimer, elle a soudain viré au bleu et…

Il me repoussa avec une certaine violence, comme je masquais la scène, pour constater de lui-même l'exactitude de mon propos.

En effet, la fille, les yeux exorbités, la langue à demi sortit de la bouche, affichait un teint digne d'un ciel d'orage.

— Faut lui faire un massage bordel ! Dégagez ! Et il se mit à l'ouvrage. Il avait l'air à son affaire.

Je m'éloignai de mauvaise grâce, et gardai un œil sur ce goujat. Bien m'en prit, voilà la fille qui se mit à hoqueter, cracher et vitupérer, tout en même temps, se débattant de l'emprise de son sauveur.

Elle brailla, et de toutes ses forces encore, un gargouillis mal bâti, qui ressemblait, — je pense ne pas me tromper, ou si peu — à

— Attention !

Qui me fit pleurer tant elle avait forcé sur les aigus, cette petite garce. Mais la bonne grosse pierre que j'avais en main atteignit son but sans une hésitation : en plein sur le crâne chauve !

Alléluia !

Le vététiste s'écroula sur la fille qui essayait tant bien que mal de se dégager, en roulant des yeux emplis de terreur, un cri coincé loin dans sa gorge en feu, — normal, j'avais déjà serré comme un bourrin à deux reprises —. Mais bordel ! Elle avait de la ressource, cette saleté, je l'aurais pas parié quand je l'avais choppé à l'orée du bois, derrière un arbre à demi déraciné, en train de fumer un joint. Ça faisait un moment que je guettais cette bonne à rien, toujours à traîner en sortant du lycée. Je les vois, moi, ces raclures, à tourner autour des maris et des pères de famille À aguicher le mâle tout le long du jour et de l'année, se faire admirer et bien pire, bien pire ! Ces petites catins !

Ah ! La postière, elle voit beaucoup de choses, hein, elle voit, — elle lit —, toutes sortes de cochonneries, et bien d'autres ! La postière, un bien beau métier ça ! Bien pratique, à dire vrai, pour mon activité d’élimination de la vermine ! Qui ira suspecter une gentille dame comme l'Iris Mineau ? La postière ? Personne !

Eh ben l'Iris, elle, elle perd pas son temps, elle en abat, du boulot : elle fait le ménage dans sa commune ! Elle sépare l'ivraie du bon grain, comme dirait le père Clemens, quand il est en verve, ce bon curé, parce que des fois, y radote comme une vieille baderne qu'il est !

Pas possible, cette catin gigote encore !

Serre plus fort Iri ! Serre !

Et c'est ce que je fis. La garce retrouva un teint lilas foncé que j'appréciai éminemment, et après s'être très légèrement débattue, mes quatre-vingt bons kilos eurent raison de ses soubresauts ridicules. Sa respiration, rauque, puis sifflante, cessa enfin.

Alléluia !

Je la tirai par les jambes un peu plus loin, au milieu de sapins tordus mais denses, puis fis de même avec ce crétin de vététiste. Pas de chance que celui-là, j'aurai jamais cru autant de monde dans ce bois un lundi, en fin de journée encore, incroyable ! Je ne perdis pas de temps à admirer la mine déjà bouffie et pâle de mes deux cadavres, sortis de mon vieux sac à dos qui ne me quittait pas, la mini pelle pliante qui s'y lovait, au chaud, comme un serpent, attendant son heure, et entreprit de me débarrasser de ces déchets d'une humanité ordinaire.

Et je vais vous dire une bonne chose, vous là de l'autre côté de la page, qui faites une moue scandalisée, je gage, doutant de mes bonnes intentions, oui, vous !

Je vous le dis tout net,

J'aurai l'absolution pareil dimanche que si je plantais du jasmin !

Alléluia !
