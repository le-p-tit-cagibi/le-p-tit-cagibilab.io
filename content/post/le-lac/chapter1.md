— Regardez, il y a quelqu'un dans le sous-bois, vous l'apercevez après ce virage, sur le sentier. Encore plus haut, dans le contre-jour, relevez un peu la tête. Vous voyez ? Je vais vous conter son histoire, non parce qu'elle est extraordinaire, bien au contraire, beaucoup d'éléments vous sembleront familiers, les personnages y figurent un ordinaire frisant la caricature, et pourtant !



Honey Waters réajustait les sangles de son sac à dos. Elle soupira. Décidément, elle n'avait plus l'âge de monter ces expéditions. La brume se levait sur le lac et l'humidité lui transperçait les os. Ses chaussures de randonnée pesaient à ses pieds autant que deux plots de béton et sa pelle pliable battait douloureusement son flanc tel un tic-tac lancinant. Elle jeta un coup d'œil sur la vaste étendue d'eau loin sous ses pieds. De la pente abrupte du mont Doré, qu'elle descendait en zigzaguant, elle disposait d'une vue imprenable sur le lac. Les courbes du bassin se paraient de pourpres et d'or en cette arrière-saison. Ses rivages ne possédant aucune plage, la forêt s'interrompait brutalement et baignait ses pieds dans une eau émeraude. Un joyau reposant sur un écrin de terre brune que le soleil transperçait et dont il rehaussait l'onde de reflets mordorés. Les saules et les hêtres centenaires tenaient les berges, leurs racines s'ancrant profondément dans les bords argileux. Sur les pentes en revanche, les pins bleus, majoritaires, noyaient sous leurs vastes ramures des sapins implantés artificiellement depuis quelques décennies par les rares exploitants forestiers de la région. La vieille femme, appuyée sur la culée d'un large tronc foudroyé se permit un instant de contemplation, oublieuse de ce qu'elle venait d'accomplir, savourant simplement cette nature offerte. L'airain et l'ocre habillaient le sous-bois d'une robe chatoyante. Le silence couvait jalousement le panorama, contrarié par quelques geais piailleurs. Une colombe filait haut dans un ciel bleu nuancé de gris, à peine visible en raison des luxuriantes frondaisons.
C'était une belle fin de journée d'automne.
Vint le temps de se remettre en route, restait encore un bon bout de chemin à parcourir jusqu'à sa cabane, comme elle appelait son humble demeure. D'ici, elle en devinait l'appontement, fiché dans une anse au nord du lac, ses grands piliers plongeant profondément dans la vase surmontés de son ponton de planches couleur de sable où sa barque tanguait doucement, tirant sur l'amarre ; mais la maison, construite en retrait sous les arbres, était invisible de son actuel point de vue.
En ahanant, Honey reprit sa route, son pèlerinage s’achèverait lorsqu'elle pousserait une ou deux bûches dans le vieux poêle en céramique, un bol de soupe entre les mains et Zoustra, la chatte a trois pattes, dans son giron.
La sente s'élargissait peu à peu devant elle et si quelques pierres farceuses roulaient encore parfois sous ses chaussures crantées, la voie devint bientôt plus aisée et la pente plus douce. Honey maudissait ses articulations devenues récalcitrantes au fil des ans, dans un milieu géographique fait en majorité de plans inclinées ; le lac se situait au fond d'une ancienne vallée très étroite qu'entouraient de tous bords de hautes collines graniteuses aux flancs escarpés. Et cependant pour rien au monde la vieille dame n'aurait abandonné cette terre natale tapie de souvenirs. Elle s'y refusait, même si parfois au fond de son vieux cœur, elle eut souhaité se libérer de tout cela.
En parvenant sur le chemin nommé « le Tourniquet », qui contournait intégralement le lac (le seul carrossable pour tout dire), elle aperçut l'antique Ford Anglia du chef de la police brinquebalant au gré des ornières, à quelques cent mètres devant elle et s'éloignant de manière régulière. Comme à son habitude, il charriait un groupe de touristes. Drôle de représentant de l'ordre, à vrai dire, songea Honey. Il avait toujours eut la langue bien pendu ! Guide touristique qu'il bramait partout. Un drôle de guide, oui ! Gramison affichait un bon soixante-quinze ans, sa carrière, avouons-le, perdurait au-delà du raisonnable, mais ici, qui serait supposé le remplacer ? Soudain contrariée, Honey pressa le pas en espérant être passée inaperçue. De retour au bercail, elle soupira d'aise en nourrissant son âtre. Une bûche grésilla et une forte odeur de résine emplit ses narines. La vieille femme ferma les yeux tandis qu'une larme coulait le long de ses joues striées de rides.



— Non messieurs dames, il n'existe plus de village à proprement parler dans ce coin : ne subsistent, disséminées au sein des forêts alentours, non loin des rives, souvent avec un accès sur le lac, qu'une poignée de cabanes. En réalité, certaines mériteraient un épithète plus flatteuse. Quelques-unes, rénovées de fond en comble, avec tout le confort moderne dernier cri, séduisent les citadins les plus aguerris. Les propriétaires ne les habitent plus à demeure, mais les conservent par sentimentalisme, et les loue de façon très pragmatique une grande partie de l'année.

Mais voyez-vous, il n'en n'a pas toujours été ainsi. Dans son enfance, cette femme d'un certain âge, Honey, que nous avons observé pas plus tard que tout à l'heure, habitait une localité baptisé Bottomcity. Ne cherchez pas sur une carte, vous ne la trouverez pas ! C'est là d'ailleurs la teneur et l'intérêt de mon récit. Cette bonne ville de Bottom, a disparue pour la bonne cause : celle des gens de la ville justement, qui ont toujours des besoins que ceux de la cambrousse n'ont pas.
En l'occurrence, il s'agissait d'amener l'électricité dans la région. Eh ou ! A l'époque on s'éclairait encore au pétrole, voyez-vous, par ici. Mais le progrès a bien fini par nous ratrapper. La petite bourgade de Bottom se situait au fond d'une vallée laquelle s'avérait le seul obstacle à la construction d'un barrage de retenue des eaux.

Bien sûr, vous voyez là le scénario se dessiner et les problèmes avec !

À ce moment-là donc, localité affichait au bas mot cinq cent âmes, bûcherons, menuisiers, ou trappeurs de métier, avec leur famille. Isolée mais solidaire, la petite communauté ne manquait de rien. Le vieux Bock tenait le drugstore avec sa femme Amanda, Honey se souviendrait très bien d'elle : Une matrone à qui manquait les deux jambes — à cause du diabète — lui serinait sa propre mère, la gourmandant du caramel qu'elle quémandait à l'occasion d'un de leur rare passage en boutique. Honey n'était qu'une gamine à l'époque. Elle pestait contre l'avarice de ses parents, conséquence d'une gêne permanente, mais cela, elle ne le comprit que bien plus tard, évidemment.

Les enfants sont ingrats, vous ne trouvez pas ? 

À Bottomcity, La scierie et l'église constituaient les deux seuls bâtiments phares, chacun à une extrémité du bourg et bien malin celui qui aurait pu prétendre lequel des deux offices réunissaient le plus de fidèles. Le bois était omniprésent dans la vie quotidienne et les croyances tout autant ! 
Chacun menait sa vie au plus près des lois qui régissent ces communautés rudes et frustres. Je veux dire, l'entraide y était de rigueur mais aussi une grande pudeur. On ne se mêlait pas des affaires du voisin et réciproquement. En cas de différents, le père Gramison, il est décédé depuis bien longtemps, paix à son âme, s'arrogeait le droit de régler les choses au mieux des intérêts de chacun et rarement, de mémoire, fut-il contesté.
Le fut-il une seule fois ? J'en doute.

Quoiqu'il en soit, du haut de ses huit ou neuf printemps, Honey s'intéressait alors infiniment plus aux racontars de Virginy Pullet, sa voisine, qu'à tout autre chose. Pullet affichait treize ans et s'épanchait sur les prouesses de ses amoureux, notamment Malcom Pistroy et sa langue baladeuse.
Entre nous, le père du garçon, un drôle de zig, tenait un boui-boui qu'on nommerait pittoresque aujourd'hui À l'époque, il était surtout indispensable et inévitable car unique en son genre. On y servait de la boisson, mais aussi de l'essence, des outils, des munitions, des armes ; autant dire une boutique incontournable en toutes saison ! Et elle regorgeait d'ailleurs de visiteurs, clients ou non, papotant de ceci ou de cela d'un air entendu. Le père Pistroy faisait une fameuse commère, vous pouvez m'en accroire. Le fils, en vrai bon à rien, perdait son temps à séduire des gamines, et à ce moment de mon histoire, il s'était amouraché de Virginy — ou l'inverse — 
Forcément, Honey trainaillait aussi au magasin et s'accrochait aux basques de ces deux-là dans l'espoir d'en découvrir un peu plus sur ce mystérieux sujet qu'est l'amourette.

Ne souriez pas ! Même une vieille dame pétrie d'arthrose et ridée comme une écorce a connu au moins un amoureux.

Et je vais vous dire, celui d'Honey s’appelait Porter. Ni franchement beau ni vraiment gentil, comme tout gamin frappé d'une douzaine d'années, mais il s'intéressait à elle, dans le sens où elle pouvait le suivre partout sans qu'il la repoussa et parfois même, il lui apportait une part de tarte tiède ou un chaton soyeux… enfin, vous voyez, ce genre de choses qui charment les gosses. Les petits gars n'étant pas légion dans la vallée, la môme Honey se fit une raison ; son prince charmant s’appellerait Porter.
Le fait est qu'ils devinrent inséparables, ce qui ne fit pas le bonheur de tout le monde, vous pensez bien, notamment ses parents. Les pauvres ne supportaient pas de la savoir à — courir les garçons — et déjouaient leur projet de rencontre autant que faire se peut. Ils la traitaient régulièrement — d'intrigante — et lui assénaient un, voire deux magistraux coups de torchon quand elle rasait de trop près un des parents en colère ! 

Il est bon de préciser qu'à l'époque, la mixité n'était pas de mise. Aux filles le ravaudage et la cuisine, aux garçons les travaux du potager et l'exploitation de la forêt.

Eh oui, je conçois que vous ayez du mal à le croire aujourd'hui.

Le révérend Maters, une des notoriétés du village, dispensait la classe et le cathéchisme aux filles et aux garçons séparemment, armé d'une craie et d'une tige de saule dont il n'hésitait point à se servir aussi bien en cas de réponse étourdie en arithmétique, qu'au détour d'une erreur lithurgique. Mathers n'avait rien du pédagogue, vrai, mais il rendait service à sa façon toute particulière et prenait soin de ses jeunes ouailles. Il s'agissait pour l'homme de dieu d'une mission, pas d'une vocation : la municipalité, réduite au plus simple appareil, ne disposait tout bonnement pas de fonds afin de payer un maitre d'école. Le révérend, érudit reconnu sans conteste par les chefs de familles de Bottom s'acquittait donc de cette tâche. Il officiait dans ses murs, à l'église, bancs et tables accointés dans le cœur, sous l’œil moribond de Christ sur sa croix. Il va sans dire que Honey manquait ses classes plus souvent qu'à son tour et connaissait la caresse de la baguette de saule.

Bon, me direz-vous, chers impatients, où veux-je en venir ?

Disons que j'attire votre attention sur le fait que notre petite Honey n'était pas toujours enfant sage et attentive aux exigences parentales. Dans sa tête de très jeune fille, papillonnait des idées de toutes sortes mais bien peu où il était questions de corvées, de devoirs et autres balivernes de grandes personnes barbantes.

Et de cela plus que tout autre, découle la suite de notre histoire. J'y viens…

Ce fut donc une époque insouciante pour Honey. Elle batifolait avec Porter à la moindre occasion, lui repprochant tout de même une tendance à l'introversion, désespérant de son manque d'initiatives, et rapportant les lacunes de son galant au creux de l'oreille attentive de sa bonne amie Virginy. Cette peste lui prodiguait moults conseils farfelus ou moqueries ; Convenons que ces balivernes ne l'aidait guère à plus de maturité ! 

La communauté de Bottomcity s'empêtrait quand à elle dans un problème plus grave : l'implantation du barrage. Deux options s'offraient aux habitants. Soit ils déménageaient de leur plein gré contre dédommagement pour permettre à ELCOM d'implanter la digue et le lac qu'elle retiendrait, lac qui noierait le village, je précise, soit ils refusaient de quitter les lieux et subiraient une expropriation en bonne et due forme.
Un sacré dilemne ! 
Les citoyens de Bottom tinrent plusieurs réunions dans l'église, qui servait accessoirement de mairie. Les familles discutaient âprement du montant de l'enveloppe, tentant de négocier une compensation supplémentaire au regard de la future saison de chasse perdue, ou dans l'espoir naïf de retarder le commencement des travaux et que sais-je encore, mais ELCOM arrêta sa décision rapidement, tarissant le feu des récriminations avant son embrasement.Et après une dernière et houleuse réunion, il fut décidé d'une date pour l'évacuation générale.

Ordre était d'abandonner le village avant ce jour fatidique, qui marquerait le début des travaux : la mise en chantier de la digue en premier lieu, puis, une année plus tard, le lâchage des eaux pour alimenter le lac. Le bourg se trouverait noyé en son point le plus profond.

Vous ne pouvez vous rendre compte aujourd'hui, tant la topologie a changé, mais pour se faire, deux rivières ont été détourné de leur lit. Elles dévalaient en amont du village depuis les montagnes de Roya, celles-là même que vous apercevez encore, on distingue un brin de neige sur les plus hauts sommets en cette saison. Deux jolies rivières, c'est bien malheureux. l'Argent, qui devait son nom à sa couleur unique, traversait le village, alimentant le lavoir, le moulin, le maréchal-ferrand et d'autres tandis que la Rouille passait un peu plus à l'est, au pied des monts qui ceignaient Bottomcity.

Il va sans dire que Honey ne saisissait guère les enjeux de tout ce remue-ménage, sa vie se résumait à ses escapades avec Porter et les réprimandes qui s'en suivaient. Elle rêvait fiançailles, bague au doigt et tout le tralala, n'imaginant pas sa vie ailleurs que dans son village.

Village qui justement ne tournait plus très rond ! le révérend Maters peinait à maintenir ses élèves à l'école, et pire, sortant de l'office du dimanche, ses ouailles se chamaillaient comme des coquelets dans une basse-cour, buvant bières et gnôle au fond de la gargotte de Bock à discuter d'ELCOM, et de son directeur — Satan déguisé — clamait le vieux Pistroy à tue-tête.
Bien contre sa volonté, la gamine se trouvait souvent sous le feu ardent de sa mère prenant à partie son époux lorsqu'il rentrait gris mais elle n'en avait cure, au fond, car elle gagnait de ces temps troublés un surcroît inespéré de liberté.

Voilà le contexte est posé. Il est important, n'en doutez pas, les détails font les bonnes histoires, vous savez.

Comme vous le supposez, le jour du grand départ arriva malgré tout car il est bien rare que les volontés d'une gamine, même amoureuse, suffisent à modifier le cours des choses. Quoique…

Au fond d'une barque, sagement attachée le long de la rive de l'Argent, deux enfants dissimulés ne pipaient mot. Les cris des parents inquiets ne les firent pas sortir, bien au contraire. Honey se pressait contre son prince charmant. Dans la cour de la maison Waters, non loin des gamins, une voiture attendait, chargée jusqu'au toit. Un cheval impatient hennissait, tirant sur son mors, attelé à une charrette, elle aussi remplie à ras bord. Il régnait un grand désordre à travers tout le village, les familles s'entassaient dans les véhicules avec leurs biens. Tous, sauf les parents de Honey et Porter, qui continuaient de s'époumoner dans l'espoir de voir surgir leurs enfants.

Mazette, ce fut le jour le plus douloureux de la vie de notre protagoniste, je vous l'assure, la pauvre gosse.

Blanchot, le chien des Waters, aboya de tout son soûl après la barque à l'amarre et finit par donner l'alerte. Une taloche ramena les amoureux à la raison et le jeune Porter grimpa dans l'automobile, rudement encouragé par son père au comble de l'énervement. La colonne de tacots se mit en marche et ne fut bientôt plus qu'un petit point brun sur la route, et finalement plus de point du tout, et plus de Porter non plus.

Mais attardons-nous un moment à ce jour où toutes les familles prirent leurs maigres biens et partirent voir ailleurs si l'herbe était plus verte… Toutes les familles, hum, ce n'est pas la vérité, messieurs dames, pas tout à fait.

Les Waters restèrent à Bottomcity, ou plus exactement, quittèrent le village et se réfugièrent au plus près de la nature, dans la forêt. Le père, pas un mauvais bougre, mais rustre et têtu comme une bourrique, avait mit à profit les mois précédant l'échéance de la compagnie ELCOM pour bâtir une maisonnette haut dans les bois à l'insu de tous. Après la taloche réglementaire, Honey, boudeuse et désespérée se mussa donc dans la carriole, ce jour du 21 septembre, et la famille prit la route comme les autres, mais non pour rejoindre la ville. Ils bifurquèrent sitôt les culs des derniers autres guimbardes, chars à bras et automobiles disparus puis grimpèrent doucement parmi les pins, poussant l'âne et la mule chargées de tout leur barda. Il y avait là le Père donc, la Mère, une grincheuse, Myrayam l’aînée, toujours à râler aussi, Quentin, un petit bout d'homme timide et silencieux, Honey elle-même, et encore Bromette bébé, la cadette, encombrant les bras de la mère soufflant et suant. Enfin, on peut le supposer.

La famille se terra le temps que les travaux de la digue s'achèvent. Maman leur faisait l'école à sa façon, Papa poursuivait leur apprentissage du bois, de la chasse,de la cueillette des champignons, des baies et plantes agrémentant la forêt. Puis vint le jour où les ouvriers s'en furent avec leur camions-bennes, leurs pelleteuses et plus rien durant un temps ne vint les déranger. La vie reprit comme par le passé.

Mais Honey restait une petite romantique étourdie. Son père lui reprochait souvent de rêvasser, le nez au vent plutôt que de finir ses devoirs, ou ramasser du bois… Régulièrement sa mère la sermonnait durement, la surprenant près de la fenêtre, le raccomodage abandonné dans un coin, la tête ailleurs. Honey cachait un secret ; elle aspirait à rejoindre son amoureux…

Décidément, ma langue se délie bien facilement en votre compagnie, et je me surprends à dérouler des souvenirs d'il y a si longtemps comme si c'était aujourd'hui même. Chaque détail remonte à la surface comme surgit parfois du fond du lac un poisson mort, une algue pourrie, éructation nécessaire d'un passé douloureux. Où en étais-je…

Ce jour là, Honey rentrait d'une — corvée-punition — dirons-nous, alors que le reste de sa famille s'affairait près de la rivière pour pêcher et se baigner. L' automne s'avançait à grands pas, mais il faisait bon, le soleil frappait dur. Ses rayons rasants caressaient d'or les cimes majestueuses des pins, les couronnant rois, l'un après l'autre, au gré de sa course.
Un papier attira son attention alors qu'elle franchissait le seuil de leur cabane. Un papier s'agitant dans la brise, fixé à un vieux clou sur la porte d'entrée. C'était d'autant plus surprenant que la famille ne recevait pas de courrier depuis qu'elle était supposée ne plus habiter à Bottom.

Honey était curieuse, je vous l'ai déjà dit, et ce défaut lui fit arracher la note pour la lire :

— Je sais que tu te planque ici, Gus Waters, ça m'importe peu, que t'y sois ou pas, mais je voudrais pas qu'on noie tes enfants, alors faut que tu te tires avant demain samedi. Le 21 septembre, t'entends vieille bourrique ? ELCOM lâchera les eaux samedi.
Signé, Chef de la police Gramison 

Honey relut consciencieusement chaque mot plus d'une fois, l'adresse du chef de la police figurait au coin droit, le corps du texte se tassait au milieu de la page et la signature, rageuse, terminait hors de la feuille. Honey fourra le message dans sa poche.

Plus tard, elle songerait à ce moment, ce papier flottant au vent, elle qui en ânonnait chaque syllabe, le calme autour de la maison, le calme autour dans son cœur quand elle avait pris sa décision.

Maudite enfant !

Le soir suivant la découverte de la lettre fut comme tous les autres soirs ; probable qu'elle écopa d'une réprimande de bailler aux corneilles devant sa soupe, tandis que ses frères et sœurs s’apprêtaient déjà pour la nuit. Maman leur raconta une histoire, où comme chaque soir, il était question de travail, de devoirs, de respect, des choses bien tristes pour Honey, des choses qui ne lui disaient rien du tout, elle qui n'aspirait qu'à rejoindre Porter ! 

Et nous y voilà, vous vous en doutez bien.

— 21 septembre — 

l'aurore flamboyait derrière les arbres, une brume teintée d'or se levait à peine lorsque Honey sortit silencieusement À sa main, un baluchon contenant quelques frusques, un quignon de pain. Sitôt éloignée de la cabane, elle fila à toutes jambes rejoindre le village de Bottom, devenu village fantôme. Malgré tout, il contenait encore un trésor, un précieux trésor pour une gamine amoureuse et elle allait s'en servir pour réaliser son rêve. Son plan arrêté, c'était l'évidence même ! Mais il avait fallu le mot sur cette porte pour qu'enfin elle prenne les choses en mains. Bien sûr, ses parents seraient déçus ou fâchés, mais n'étaient-ils pas toujours fâchés ou déçus à son égard de toute façon ? Honey décida de ne plus y penser. Qu'importe leurs réactions ; elle serait loin ! Ils n'auraient qu'à déverser leur colère sur la si vertueuse Myrayam, le très sérieux Quentin, ou bébé Bromette.
Honey descendit le sentier, rêveuse, la tête emplit de projets plus fous les uns que les autres qui n'avaient d'excuse que son jeune âge.

Je vois votre moue, doutez-vous de l'innocence de notre petite Honey, de sa détermination, du destin peut-être ?

Retrouvons-la au bas de la sente qui mène droit au village, ou plus précisément, à l'Argent, vous vous souvenez, cette petite rivière, dont je vous ai parlé précédemment. Elle traverse Bottomcity de part en part.

Longeant la rive, Honey finit par dénicher ce qu'elle cherchait : la barque, son précieux trèsor ! Elle gisait à l'abandon, intacte, posée sur le flanc comme un phoque mort. Honey sourit. Un sourire heureux. Elle grimpa dans la barque et attendit, serrant contre elle son sac et ses espérances, sous forme d'une adresse griffonnée sur un chiffon de papier qu'était devenue la lettre.

Une drôle d'histoire que je vous conte là, me direz-vous, et quelle drôlesse d'enfant.
Je vous répondrai malicieusement que c'est là le propre de l'enfance, toucher ses rêves du bout des doigts en présumant que tout est possible.

Honey, confiante et certainement rompue d'attendre, s'endormit dans l'embarcation.
La houle ne la tira de son sommeil que beaucoup plus tard : Les eaux avaient été lâchées. Elle s'accrocha au banc de nage et se laissa dériver, persuadée que le courant allait l’entraîner en aval, vers son Porter…

Bientôt, elle aperçut effarée le clocher de son village, qui avait bien rétréci, noyé sous plusieurs mètres d'eau. L'esquif tournicota autour puis s'éloigna, coquille de noix jetée au gré des courants, heurtant ici le faît d'un toit, raclant là un obstacle invisible, ou encore cognant un des nombreux objets oubliés des habitants et débusqués par l'eau dont le niveau continuait de monter, monter, monter. Honey regardait droit devant elle, du fond de son canot.

Les parents, aux quatre-cents coups de l'absence prolongée de Honey, partirent à sa recherche avec le reste des enfants. Hélas, ils se retrouvèrent vite piégés au détour d'un chemin par l'eau affluant de toutes parts. Papa voulu sauver les enfants, mais son courage ne lui permit que de périr le premier, Maman ne put secourir ni sa Bromette, ni son Quentin, et Myrayam ne savait pas nager.

Oui mes braves gens, triste histoire, en effet, et j'ai de la peine encore aujourd'hui en vous la contant.

Les marécages bourbeux qui s'étaient rapidement formés alentours engloutirent les cinq membres de la famille Waters, les recouvrant rapidement d'un linceul semi-liquide, aux tons chatoyants de brique et de cuivre, en cette belle journée d'automne.

L'eau continua de tout engloutir, puis l'aurore se leva sur une barque cabossée, échouée au pied d'un monticule inondée de soleil où un petit chemin zigzaguait parmi les pins. Au fond de l'embarcation, gisait une petite fille endormie.

Oui, bien sûr, je peux vous conter ce qu'il advint de notre jeune aventurière, bien que ce furent là de navrantes aventures, et une fin plus navrante encore.

Le vieux Gramison recueillit Honey à la mort de ses parents et le jeune Gramison junior, Porter, de son prénom, supporta dès lors une haine sans partage de la part de la petite orpheline qui avait tout perdu cette nuit-là, sa candeur par dessus le marché. Dès qu'elle eut l'âge, la jeune fille s'en est allée vivre en recluse, au pied de la forêt, au bord du lac, vivant de bûcheronnage, de cueillette, peut-être de rapine, mon père n'en a jamais fait mention, il se reprochait tant qu'il lui passa beaucoup, bien qu'elle ne le porta jamais dans son cœur, lui non plus.

Et c'est pourquoi chaque année à l'automne, Honey s'apprête puis arpente le mont Doré, ainsi nommé, pour s'en aller planter un rosier sauvage tout là haut, en souvenir de ce jour où toute sa famille a été enseveli par le lac.

Moi ? 
Je patrouille, voir qu'elle ne se casse pas le nez en redescendant de cette foutue pente ! Un jour proche, je n'en doute pas, voyez-vous, Honey remontera dans sa barque et s'en ira au milieu de ce maudit lac elle aussi, pour ne plus revenir.

Assez causé à présent messieurs dames, je dois finir ma ronde, bien que les rives soient plus hantées par de vieux fantômes que d'autres choses. Ne tardez pas à rentrer, les nuits sont fraîches par ici, il ne faudrait pas vous perdre, au risque de tomber salement à la flotte.
Porter Gramison, vous salue bien.


