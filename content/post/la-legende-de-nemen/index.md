---
title: La légende de Nemen
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "L'amour, la survie"
epub: true
pdf: true
draft: false
---
*« Je fus témoin d'un événement unique, laisse-moi te conter, étranger, ce que je n'ai pu oublier, afin qu'à travers toi, le souvenir demeure. »*

Arkkhon déploie ses ailes et s'élance vers l'azur finissant.

Le rougeoiement des soleils couchants caresse sa nuque transparente. Elle virevolte tel un aigle des mers, à la fois gracieuse et puissante, bien qu'elle ne connaisse aucun animal de ce nom, ni aucun animal d'ailleurs. S'approchant de la surface de l'océan, flirtant avec les vagues, se laissant voluptueusement caresser par les embruns de cette fin de journée à l'apparence si paisible.

La jeune efferen sourit en apercevant au loin, sur Grandrocher, face à la Forteresse – sa Forteresse – son compagnon, Kinementaru.

Elle ralentit l'allure et atterrit doucement sur l'étroite roche grise. Leurs yeux, bien que totalement différents, différents j'entends, comme la glace et le feu peuvent l'être et bien plus encore, leurs yeux donc, se rencontrent. C'est un spectacle bien étrange que ces deux êtres : lui scintillant comme un diamant bleu taillé par le plus habile des joailliers et elle, un lys, que le miroitement de la lumière rend à la fois chatoyant, quasiment translucide et dans le même temps multicolore, pourrait-on dire, un kaléidoscope vivant au travers duquel l'on voit se dérouler le spectacle des éléments.

Une vague, plus téméraire que les précédentes, vient lécher leurs corps frissonnants. Mais Ki, comme le nomme affectueusement Arkkhon, n'a jamais froid – son sang – enfin, ce qui circule en lui, est plus glacé que l'azote liquide que nous connaissons, et il n'y a en cet instant que l'efferen pour ressentir la sensation glaciale que peut procurer la peur.

Car ce soir, elle va faire preuve d'amour, la plus grande, certainement, que l'on puisse consentir à celui qu'on aime et cela la terrifie. Contemplant leur ultime crépuscule, les deux amants s'acheminent inéluctablement vers leur destin, funèbre, sombre, certes, mais promesse folle d'un renouveau, d'une renaissance. Ce destin qui est inscrit sous les eaux, sous cet éperon de granit, plus exactement, dans une grotte sous marine, depuis il y a déjà fort longtemps.

Mais le temps n'a pas d'importance, seul l'avenir importe.

Arkkhon lance un signe de tête en direction de la Forteresse.

Bastion perdu sur une planète perdue investi par des colonisateurs naufragés dans un périple qui pris fin plus tôt et plus tragiquement que prévu, quelle ironie ; rien à coloniser ici !

Quatre mille cinq cents passagers en perdition – sept-cent survivants alors –

Cette Planète refuge, où nul ne peut survivre hors de cette étroite bande de terre, le long du littoral, à cause de vents si violents qu'ils vous arrachent littéralement la peau des os, fut nommée Nemen «sans joie» et sans joie – aucune – en effet, depuis 2 700 ans que dure leur calvaire, fut leur vie ici, dans les murs épais de la Forteresse, qui était là avant leur arrivée, et qui pointera toujours ses sombres tours lisses et noires quand les Sept derniers d'entre eux ne seront plus que tas de poussières immondes.

Ki interrompt les noires pensées de sa compagne, ses écailles bruissent dans un métallique tintinnabulement alors que ses yeux liquides couvrent, nostalgiques, une dernière fois, le panorama orgiaque composé de gris, d'argent et bleus mêlés d'anthracite, qui s'offre à eux impudiquement.

« Tu arrives bien tard ! j'ai cru que tu renonçais… »

Ses clappements de bec sont périlleux et trahissent l'inquiétude. La jeune efferen pose sa lourde tête sur l'épaule de son compagnon en signe d'apaisement et lui, en retour, caresse tendrement cette extraordinaire protubérance, que nous dirions difforme parce qu'elle n'a pas de nos références géométriques si pleine de condescendance, elle n'est, en effet, ni ovale, ni ronde, ni carrée, pourtant, en la regardant, on est touché par la douceur et la bonté qui se lit sur ce « visage ».

Elle reste silencieuse, se remémorant leur première rencontre, alors qu'elle croyait toute vie impossible sur cette planète… et qu'elle n'avait pas encore songé que l'océan pouvait abriter des hôtes fascinants…

Elle était alors si jeune, si désespérée de cet échec qui clouait ses compatriotes au sol pour l'éternité, envoyant leur mission aux oubliettes. Ils étaient encore nombreux dans la Forteresse, en ces temps reculés, survivant dans l'espoir de pouvoir recréer ici un monde nouveau – bien avant de se rendre compte qu'ils étaient tous devenus stériles en même temps qu'égarés –

Ce jour-là, donc, Arkkhon échappa à la surveillance des matrones qui, dans la Forteresse, régissaient tout à présent, de l'heure du lever à celui du coucher, et bravement, sortit affronter les éléments, afin de pouvoir voler, acte strictement interdit à l'intérieur. Elle s'éloigna rapidement en direction de la mer, battant des ailes de manière effrénée, joyeuse de son escapade, humant goulûment l'air salé. Mais alors si peu entraînée, que ses forces la trahirent et qu'elle s'épuisa rapidement : incapable de faire demi-tour, elle chuta bientôt telle une pierre dans l'océan.

– la mort –

Au contact de l'eau, ses ailes pesèrent soudain si lourd qu'elles l'entraînèrent sans espoir de retour, vers les abysses.

– la mort –

Mourir aurait dû être son lot ce jour-là.

L'homme-poisson l'avait arraché à l'agonie terrible de la suffocation, il avait éloigné le spectre de son douloureux trépas, et lui avait offert des jours et des nuits où couleurs, sons, sensations étaient l'essence même de la vie.

Elle s'était réveillée sur ce même rocher où elle repose ce soir.

Enlacée à cet être étrange dont elle comprenait si peu et auquel elle se mit à croire tant. Tant qu'ils devinrent amants, amis, avides l'un de l'autre puisque si différents, et pourtant si semblables en un sens où l'un et l'autre savaient la fin de leur temps proche et irremplaçable. Nulle descendance. Nul pas dans leur pas. Aucun écho à leurs connaissances ou leurs réminiscences…

Il lui apprit à nager sous l'eau indéfiniment, elle ne réussit jamais à lui enseigner le vol, mais l'homme poisson n'en prit pas ombrage, il aimait son océan et s'y mouvoir sans contrainte était ce dont il était le plus heureux.

C'est leur compréhension intime et réciproque, leur complicité de chaque instant, qui incita Ki à révéler le sanctuaire à sa compagne, lorsqu'il fut persuadé de sa détermination et de sa sincérité. En effet, elle renonça à réintégrer la Forteresse et fut, de ce fait, bannie par les siens.

Kinementaru avait découvert la grotte des centaines d'années auparavant. Elle se présentait en une salle immense, basse de plafond, ronde, avec en son pourtour, une multitude de petites niches, nous dirions rectangulaires, qui avaient peut-être contenu quelque chose à leur origine, mais qui, à présent, étaient vides.

Au centre de la grotte, un trou affleurait, par lequel on accédait à la mer et, éparpillées un peu partout, tout un bric-à-brac dont des sortes de planches d'un matériau étranger, lisses, dures, dans lequel était incrusté des images, des dessins, des reproductions d'êtres vivants mais d'où provenaient-elles ? de quand ?

Mille et mille fois, les deux amoureux s'étaient posés la question en effleurant ces surfaces énigmatiques qui racontaient à n'en point douter, une histoire dont ils étaient étrangers – jusqu'au jour où leur vint à l'esprit d'en devenir acteurs –

En effet, nombre de ces panneaux représentaient de petits êtres, non pas en taille, mais en âge, de jeunes êtres, immatures, ne se tenant pas encore droits, ni debout, alors que les adultes évoluaient, sur deux jambes ou pattes… il y avait donc eu des enfants !!!!

La horde de Kinementaru, peuple de la mer, n'avait souvenance d'aucune progéniture depuis qu'ils étaient sur cette planète… Ils étaient – Quasiment immuables –

Parfois, rarement, et au bout d'un temps extrêmement long, mais cela arrivait parfois donc, l'un des homme-poissons se désagrégeait d'un coup dans l'eau grise et disparaissait en fines particules retournant certainement à son état de liquidité premier…

Soudain, Arkkhon est tirée de sa rêverie par un furieux battement de nageoires. Ki s'impatiente de son silence, de son attitude. Elle explique :

« J'ai pris le temps de faire mes adieux à chacun des Sept. Ils m'ont laissé entré cette fois, et nous avons parlé. Leur espoir est en nous… et nous avons si peu d'espoir ! »

– Nous avons un espoir commun et cela suffit. N'en aies-tu pas persuadée ? doutes-tu maintenant ? que crains-tu ?

Il y a du défi dans cette dernière interrogation.

Les sons mélodieux qui sortent alors de l'imposante « tête » de la jeune efferen apaisent immédiatement son compagnon :

« Il n'est rien que je ne puis surmonter puisque toi, mon amour, tu es prêt à l'affronter »

Alors à son tour, il se rappelle, en écoutant ses sages paroles, quelle souffrance il a enduré au début de leur rencontre, à rester ainsi hors de l'eau… sans rien comprendre à ces drôles de hululements qui sortaient de cette fantasque proéminence osseuse dotée d'ailes à faire pâmer un ange, et de fortes griffes dont Lucifer aurait sans doute été jaloux… Encore eut-il fallu qu'il soupçonnât l'existence de pareille spécimen !

L'homme-poisson se souvient avec tendresse ? – oui… approximativement… en langage commun dont nous sommes friands – de la terrible chute dans l'eau, elle, coulant à pic et son élan à lui, immédiat, irrépressible, pour nager, la sauver, fusse au péril de sa vie… sa crainte de la perdre, déjà.

Puis, sur Grandrocher, la douceur de sa peau – qui n'avait pas d'écaille – mais plutôt un épais duvet, doux, très doux, si doux que le frôler était la plus divine des récompenses.

« Tu rêves mon ami ! » Lance t-elle en s'ébrouant, ce qui fait miroiter mille reflets de rouges sur son corps étrange.

Kinementaru détourne le regard. Il a terriblement envie d'elle à présent. Il l'attrape vivement par la taille, et l'entraîne dans les vagues mugissantes. L'eau est atrocement froide. Le désir d'Arkkhon croit à présent ; elle aussi ne souhaite plus qu'une chose : s'unir !

Mais il leur faut respecter le processus sans en négliger aucun prérequis. En ce jour, il n'est plus seulement question de faire l'amour, car de cela, depuis déjà des centaines d'années, leurs corps se sont repus – non – il est question de survie.

Ils nagent avec souplesse et rapidité dans les profondeurs de l'océan et atteignent sans difficulté l'entrée de la grotte.

Celle-ci est toujours jonchée de ces schémas, CD tordus, processeurs scio dégradés, boites mémo ancestrales, reco di-bivalents, et bien d'autres objets, dont, moi, pauvre petite créature, vous diriez, conscience ou encore âme de ma douce maîtresse Arkkhon, fille de Nemen, la planète maudite, je n'ai nulle connaissance !

Vestiges certainement, d'une ancienne base militaro-scientifique, expérimentale, pédagogique ou que sais-je encore ?

Mais ils sont là : tous ces témoignages d'une ère où les lignées perduraient, les fils soutenant les pères et les filles accouchant d'enfants. Des petits qui s'épanouissent encore sous les yeux des amants, chaque fois qu'ils se réfugient ici, dans cette mystérieuse cache sous-marine, écrin oublié, témoin d'un autre devenir. Arkkhon adore l'image où l'on aperçoit les modifications physiques qui se produisent lors de la gestation de la femelle. L'embryon lui ressemble, informe, difforme, mais il croît et oh merveille ! Il se métamorphose en une créature svelte, dotée d'un mode de déplacement élégant, de petites pinces très chic pour saisir, et sa tête respecte les proportions de son corps !

Reprenant son souffle, au sortir du sas naturel qui donne accès à la caverne, elle murmure :

« Nous sommes fous, n'est-ce pas ? »

– Fous ? Te rappelles-tu notre premier séjour ici ? Ce fut une révélation. Nous ne pourrons tout résoudre, Arkkhon, tu le sais, mon aimée, mais nous allons essayer… n'est-ce pas là le plus beau des cadeaux de deux êtres qui s'aiment ?

Les clappements de Kinementaru tombent comme une pluie d'été sur une terre desséchée. La jeune efferen enlace son aimé sans répondre. Puisqu'ils s'aiment à mourir, qu'y a-t-il à répondre ?

Elle saisit alors la lame scintillante, dont je t'ai caché la présence jusque-là car, désormais, nul doute que mon histoire aura une fin tragique n'est-ce pas, étranger ? Et ton cœur bat plus fort, parce que toi aussi, tu t'es pris à aimer ces deux créatures extraordinaires.

Ainsi donc, ma maîtresse brandit la lame effilée, et sans hésitation aucune, elle tranche l'appendice caudal, et les nageoires de son amant, puis pose délicatement l'arme au creux de sa main palmée avec un sourire indescriptible, rempli d'un amour si lumineux, que la grotte devient soleil et qu'ils sont deux astres lancés dans l'infini, sur une orbite trop intime pour être de nous, voyeurs intemporels, connue.

À son tour, l'homme-poisson sectionne à l'aide du poignard, l'une après l'autre, les deux ailes splendides de sa compagne qui choient de guingois, avec un bruit mat, sur le sol humide. Des larmes ? ou une substance qui s'y apparente coulent de ses yeux instables, et de ses blessures béantes un liquide épais semblable au mercure s'échappe, veinant le sol noir de leur antre de fins sillons d'argent.

La jeune efferen aussi semble pleurer, pourtant une grande sérénité se dégage d'eux. Il n'y a aucun cri, aucun bruit, aucune agitation.

Kinementaru saisit son « visage » et l'embrasse avec fougue, sachant que jamais il ne sera rassasié d'elle et Arkkhon lui rend son baiser, se blottissant contre celui qu'elle aime infiniment. De ses ailes amputées, se répand un fluide nacré, c'est sa vie qui s'en va… qui roule, lentement, se noie dans les sombres traces bleutées maculant déjà la roche autour d'eux.

Ils perçoivent leurs drôles de cœurs battre furieusement comme le vent quand il fait claquer les vagues contre la roche : ils sentent le flux qui les emporte, alors Ki s'enfonce en elle, comme on étreint un rêve – le rêve de la vie –

*« Voilà l'histoire telle qu'elle fut, étranger – Incroyable –

Oui.

Pourtant, tes yeux voient comme moi – mieux même – car je suis bien vieux – ce cocon translucide au centre de la grotte. Il est le résultat de ce jour-là.

Écoute ! On entend quelque chose battre à l'intérieur… Oui deux bruits fort distincts je te l'accorde, l'un tel un galop de pur sang, l'autre ressemble à un grand fleuve calme aux eaux abondantes. Le cocon semble s'éclairer de nacre et de reflets mercuriels… regarde !

Non, non, étranger !!! tu ne peux pas aller plus loin.

Je suis le gardien de ce sanctuaire.

Nul ne peut approcher d'eux – enfin, de ce qu'il en reste ou de ce qu'il en est advenu –

Je ne sais combien de temps durera ce phénomène mais en aucun cas il ne faut l'interrompre…

En aucun cas.

À présent, tu vas sortir d'ici comme tu es venu, par hasard, et la route qui mène ici sera de nouveau perdue.

Mais, au fond de toi, tu ne pourras feindre ignorer que peut-être une vie prend racine ici sur Nemen la stérile et alors, viendra le temps où un peuple nouveau t’accueillera à ton prochain passage. »*
