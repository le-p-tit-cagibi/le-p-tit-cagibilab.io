---
title: 20.000 lieux sous la Terre
author: Sophie Belotti
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "?"
epub: true
pdf: true
draft: true
---
Jack Mac Deneahy et Ruth Boulder stoppèrent devant la guérite bleue et blanche du sergent de la Rochelle.

L'homme les accueillit comme de vieilles connaissances.

— Bonjour Professeur, mes hommages docteur. Quel bon vent vous amène ? Vos badges s'il vous plaît.

Le conducteur, un homme d'une cinquantaine d'années, lunettes d'écailles sur un nez en bec de faucon, teint gris et bague d'une prestigieuse université à l'auriculaire de la main droite, salua de la tête, ébauchant un semblant de sourire de lèvres fines qu'il avait fines et d'un mauve maladif. Il tendit sa carte plastifiée avec désinvolture, sans engager la conversation avec le planton. Sa passagère, plus âgée, aux mensurations plus que généreuses — que l'on devinait sous sa robe en polyester du bleu réglementaire —, agita le chef, et sa lourde chevelure afro tangua dans tous les sens avant de se figer de nouveau, comme elle répondait, brandissant son badge sous le nez de son collègue, imperturbable.

— Bonjour Jacques-Antoine, quelles nouvelles du bout du monde ?

C'était une vieille blague entre eux.

— De bien brûlantes nouvelles, m'dam ! Et il salua, deux doigts effleurant rapidement le képi tandis que le véhicule franchissait la barrière de sécurité sous le regard impavide des nombreuses caméras de surveillance.

Le soleil tapait dur, effectivement, sur ce bout de terre perdue, et ils se hâtèrent de conduire leur voiture dans le parking souterrain.

Une fois à l’intérieur de ce qui ressemblait à un gigantesque blockhaus, un autre garde, moins accueillant, leur soutira une fois de plus leur identité, puis un ascenseur, un vieux crincrin hors d'âge, réclama lui aussi leurs sacro-saintes cartes plastifiées. Une fois insérées et régurgitées d'une fente située dans la paroi métallique de l'engin, une voix sans timbre s'éleva du haut-parleur encastré dans le plafond élucubrant son lot de recommandations auxquelles ni le professeur, ni le médecin ne prêtèrent attention. Ils s'enfoncèrent dans les profondeurs de la terre. L'ascenseur allait lentement, ahanant comme une vieille bête moribonde. Enfin, le panneau de commande clignota, puis l'engin stoppa à grand renfort de grincements alors que l'homme et la femme tiraient sur leurs vêtements en un geste automatique et d'un parfait mimétisme.

La voix atone annonça :

— Étage moins quarante huit, vous êtes arrivés.

La porte coulissante s'ouvrit sèchement, et la voix éructa encore :

— Étage strictement réservé au personnel autorisé, veuillez présenter votre badge au poste de garde 48A.

Les deux visiteurs se redressèrent malgré eux, et leurs regards, de connivence, se croisèrent un bref instant. Mac Deneahy soupira, offrit sa main à sa compagne pour franchir le seuil du sas de l'ascenseur, et ils s'éloignèrent rapidement, projetant leur ombre allongée dans un couloir large et haut qu'un éclairage à minima rendait lugubre.

Au bout de celui-ci, une sentinelle montait la garde. Sans un mot, et après une ultime vérification de leur identité, ils pénétrèrent dans la zone correspondant au but de leur visite. Le chemin leur était connu.

— Où allons-nous ? S'étonna pourtant Jack, alors que la femme prenait, contre toute attente, un petit couloir obscur, et posait, quelques mètres plus loin, sa grosse main brune sur une porte blindée. Un seul mot en barrait le battant, peint en rouge : Rebut.

— Il est plus que temps que tu te sortes les doigts du cul, cher Docteur. Assez fait mumuse, souffla-t-elle, imitant gracieusement une souple révérence auquel sa corpulence ne se prêtait pas et l'invitant à entrer.

— Qu'est-ce que signifie ces vulgarités, Ruth ? Bégaya l'homme, visiblement outré.

Mais elle disparut derrière la porte sans ajouter un mot. Mac Deneahy haussa les épaules.

Force lui fut de la suivre s'il voulait des réponses.

L'huis claqua lourdement en se refermant derrière eux. Des plafonniers de couleur orangée se mirent immédiatement à clignoter, révélant un tunnel d'une cinquantaine de mètres, carrelé de blanc sur toute sa surface, et aboutissant à une seconde porte blindée munie d'un large hublot. Le mot

—Rebut — en rayait également la surface, inscrit en gros caractères d'imprimerie.

— Vas-tu enfin m'expliquer ce qu'on fiche ici ? Questionna l'homme soufflant de dépit.

Sa voix résonnait dans le couloir désert. Il suivit néanmoins Ruth, bien décidée à ne pas ralentir, pas plus qu'à ouvrir la bouche pour éclairer son collègue de plus en plus dubitatif.

La nouvelle porte s'ouvrait grâce à une large manivelle, et la grosse femme, avec effort, fit tourner le mécanisme, tout en jetant un œil par la vitre. Jack, mains sur les hanches, commençait à perdre son calme.

—Mais enfin, qu'est-ce qui t'arrive Rut ? T'as péter un câble ou quoi ? C'est quoi cet endroit ? Pourquoi on a quitter la zone 48A ?

Puis la mitraille de questions stoppa net. Mme Boulder venait de se retourner, lui offrant son sourire le plus carnassier.

— C'est ici que commence LA véritable zone, cher Docteur. « Entre en vitesse ou je te promets une ou deux dents en moins ! »

— T'es cinglée ? Depuis quand tu me parles comme ça ? Se fâcha Jack, tout rouge, après cette tirade pour le moins féroce, et incompréhensible.

— Je n'ai jamais été aussi rationnelle qu'à cet instant, Jack de mon cœur, et aujourd'hui, je vais plus que de coutume, mériter mon salaire. Entre là-dedans ! Lui intima-t-elle en poussant encore un peu sur la porte pour l'ouvrir largement.

Un vent frisquet s'engouffra immédiatement dans le tunnel et gifla le couple. Leurs vêtements gonflèrent sous la rafale, un froid vif les saisit.

Au seuil de la porte, un antique paternoster, tanguait légèrement dans le vent. Les chaînes maintenant deux rangées de cabines cliquetaient sans discontinuer.

—Mais qu'est ce que c'est que ce machin ? Hurla Jack pour se faire entendre, alors que les bourrasques forcissaient et mugissaient à travers la cage d'ascenseur, et que les petites cabines de bois rutilantes, avec leur tabouret en abattant, — de l'acajou probablement —, montaient et descendaient avec lenteur sans à-coup, comme un vieux carrousel.

— Croyais-tu que ton job se bornerait toujours à écouter déblatérer tes soit-disant patients, bien planqué, et que, penché religieusement sur tes notes, plus studieux qu'un élève de prépa, tu mesurerais tranquillement leur… comment déjà ? Madame Boulder ayant perdu toute dignité postillonnait au visage de son prétendu collègue, animée d'une colère noire. Elle poursuivit, n’épargnant pas le costume cravate de Jack. « … uoi déjà ? Leur fabulation ? Elle renversa sa large tête en arrière et rit à gorge déployée. Elle semblait habité par un démon, et Mac Deneahy n'en menait pas large. Il se ressaisit pourtant, et opta pour la raison.

— Enfin Ruth, arrête de jurer comme un cochon, on dirait presque que tu me détestes ! Ça veux dire quoi tout ça ?

— Ça veut dire que je te demande de prendre ce foutu machin et rejoindre ton dernier sujet d'étude ! Maintenant !

Et soudainement, joignant le geste à la parole, elle empoigna littéralement son collègue par la peau du cou. Il se retrouva propulsé dans le réduit de gauche, légèrement contusionné et recru — bien qu'il eut tenté de protester —, ce qui n'eut aucun effet sur la détermination de Ruth et encore moins sur le résultat.

Coincé dans la paternoster, il se mit instantanément à descendre. Ses pieds et ses jambes disparurent.

— Mais ça mène où ce truc ? Hurla-t-il encore, à présent terrifié à l'idée de s'enfoncer dans des profondeur inconnues.

— Devine ! Espèce de grand naïf ! Tu vas pas en revenir !Va rejoindre tes petits camarades !

— Ruth ! Espèce de salope ! Brailla encore le docteur Mac Deneahy, puis sa tête grise disparut tandis qu'une cabine vide s'offrait déjà au prochain visiteur.

Ruth ne perdit pas un instant. Elle referma la porte blindée, réajusta sa tenue, et parcourut le chemin inverse, se soumettant aux contrôles de bonne grâce, puis réintégrant au bout du compte sa voiture, une vieille Datsun vert bouteille. Elle démarra sans se permettre de réfléchir et en chemin, composa un numéro. La sonnerie n'eut pas le temps de retentir qu'un

— Allô ? Allô ? autoritaire se fit entendre.

— Ne me refaites plus jamais ça, Stephen ! Dit-elle en serrant les dents.

— On se voit la semaine prochaine Ruth, arrêtez votre cinéma, sans moi, votre petit copain n'aurait pas tarder à faire de vous une star internationale dans la section rebus.

— J'espère que vous avez raison.

— Vous savez que j'ai raison non ? Sinon pourquoi auriez-vous envoyé l'éminent professeur Mac Deneahy dans les oubliettes ?

— Mais comment a-t-il…

— Vous doutez de ses capacité ?

— Non, bien sûr, mais c'est un patriote avant tout, je n'arrive pas à imaginer qu'il puisse passer à l’ennemi !

— Tout le monde a un prix, chère Ruth ricana le dénommé Stephen.

— Cessez de vous foutre de ma gueule. On se voit lundi.

Elle raccrocha. Ce type était barbant et plutôt imbuvable, et elle n'était somme toute pas très fière de ce qu'elle avait fait aujourd'hui. Ce crétin de Deneahy était cloué là-bas pour le restant de ses jours, sans espoir de retour. Mais bon, elle l'avait démasqué. Un coup de chance… uth frissonna des conséquences de son inconséquence : depuis combien de temps lui jouait-il la comédie ? Enfin, tout rentrait dans l'ordre. Stephen ne lui en tenait pas rigueur. Elle avait derrière elle une carrière honorable. Pourtant elle ne se sentait pas au meilleur de sa forme.

Une semaine plus tard, Cole Véranie et Ruth Boulder stoppèrent devant la guérite bleue et blanche du sergent de la Rochelle.

L'homme les accueillit comme de vieilles connaissances.

— Bonjour Docteur, mes hommages professeur. Quel bon vent vous amène ? Vos badges s'il vous plaît.

Le conducteur, un jeune homme d'une trentaine d'années, cheveux roux coupés très courts, lunettes aux monture de plastique transparente sur un nez aquilin, teint bronzé et montre de prix au poignet droit, salua roidement de la tête, ébauchant un semblant de sourire de ses lèvres pleines et rosées. Sa passagère, plus âgée, aux mensurations plus que généreuses — que l'on devinait sous sa robe en polyester du bleu réglementaire —, agita le chef, et sa lourde chevelure afro tangua dans tous les sens avant de se figer de nouveau, comme elle répondait en tendant leurs badges.

— Bonjour Jacques-Antoine, quelles nouvelles du bout du monde ?

C'était une vieille blague entre eux.

— De bien brûlantes nouvelles, m'dam ! Et il salua, deux doigts effleurant rapidement le képi tandis que le véhicule franchissait la barrière de sécurité sous le regard impavide des multiples caméras de surveillance.

Le soleil tapait dur, effectivement, sur ce bout de terre perdue, qu'on avait appelé, il y a bien longtemps la Zone 51, et qui aujourd'hui se prévalait de nouvelles attributions, après une restructuration en règle par leur département, — les petits hommes verts, c'était bon du temps des hippies —. Véranie et Boulder se hâtèrent de conduire leur voiture dans le parking souterrain à l'abri du vent sec et cinglant du désert.

Un autre garde, moins accueillant, leur soutira une nouvelle fois leur identité, puis l'ascenseur, un vieux crincrin hors d'âge réclama lui aussi leur sacro-sainte carte plastifiée. Une fois dûment insérées et régurgitées d'un panneau métallique, une voix sans timbre s'échappa d'un haut parleur encastré dans le plafond, énumérant son lot de recommandations, sans que ni l'opulent professeur, ni le jeune médecin n'y prêtent attention. L'engin les conduisit alors dans les profondeurs de la terre. Une lampe clignota, puis l'ascenseur stoppa enfin à grand renfort de grincements, alors que l'homme et la femme jetaient un œil discret sur le panneau qui réfléchissait leurs silhouettes bleues strictement réglementaires.

La voix annonça :

— Étage moins quarante huit, vous êtes arrivés.

La porte coulissante s'ouvrit sèchement. La voix crut bon d'ajouter :

— Étage strictement réservé au personnel autorisé, veuillez présenter votre badge au poste de garde 48A.

Cole Véranie laissa galamment passer sa plantureuse collègue, avant de s'engouffrer dans un couloir blanc menant à la zone A. Ils présentèrent leur carte à une factionnaire léthargique et poursuivirent leur chemin sans rencontrer la moindre difficulté. Cole tirait machinalement sur sa cravate anthracite où évoluaient des étoiles argentées. Sa partenaire filait droit devant, son large postérieur marquant la mesure à chacun de ses pas.

Une série de portes se déclinait le long du couloir, chacune portant un simple numéro gravé à l'encre noire sur le panneau de bois lasuré. Ruth déverrouilla la numéro 7 d'un simple coup de badge, et s'engouffra à l’intérieur sans un regard sur Véranie.

Des vociférations les accueillir dès l'entrée.

— Sortez-moi de là ! glapit une voix qui n'était étrangère qu'à l'un des protagonistes.

— Un ton plus bas, Jack, inutile de me casser les oreilles, ça ne sert à rien, tu ne rameuteras personne, prévint aussitôt le professeur Boulder à l'énergumène qui lui faisait face.

— C'est sûr ! A vingt mille lieues sous la terre, ça va être dur de se faire entendre papy, ajouta Cole, grimaçant une moue qui le rendit soudain beaucoup moins avenant. Ses dents, sorties hors de sa bouche, lui donnait un air requin. Derrière ses lunettes, ses yeux noirs, ronds, et enfoncés dans leurs orbites, accentuaient l'effet.

Le couple contempla un instant le prisonnier. Jack Deneahy s'agitait derrière une vitre qui séparait la pièce où ils se trouvaient en deux. Revêtu d'une camisole orange lui saucissonnant les bras autour du corps, sa physionomie, habituellement joviale s'était transformée en une sorte de masque d'épouvantail. Un casque d'aviateur, recouvert de fils électriques et de microprocesseurs pendait le long de son dos, en partie brisé et recouvert de sang. Son crâne saignait, dessinant des larmes carmines qui inondaient ses joues maigres, recouvertes d'un duvet gris. Ses lunettes, rafistolées avec du sparadrap, gisait de guingois sur son nez. Il respirait par à-coups, comme un poisson hors de son bocal manquant d'air. Il affichait un regard de dément. Pourtant il s'adressa au professeur Boulder avec calme.

— Relâchez-moi, Ruth.

— Tiens, on se vouvoie à présent, constata froidement Ruth, « vous avez trahi l'un des services du département le plus prestigieux et des plus nécessaires de votre gouvernement, espèce d'imbécile ingrat ! Vous pourrirez ici, mais…

— Va falloir nous causer, papy, le tança Cole, l'index pointant sur la vitre comme un pistolet. Il ajouta, vaguement inquiet, à l'intention de sa patronne « il a arraché son brouilleur, ça va pas poser problème ça… »

Deneahy cracha vers lui, oublieux un instant qu'un épais vitrage hautement sécurisé les séparait. Cole s'esclaffa grassement, suivant de son doigt le crachat qui dégoulinait le long du carreau blindé.

— J'ai rien à vous dire ! Aboya le docteur Deneahy, en reculant malgré lui.

— Le gouvernement réclame des explications, Jacky, poursuivit Ruth sans s'attarder sur le comportement irrévérencieux de son nouveau collègue.

— On m'a contacté, c'est tout. Vous le savez déjà ! Il ouvrit ses mains en signe d'impuissance.

— Oui. Mais je veux des noms, renchérit son ex-collègue d'un ton quasi affectueux. « un petit effort, et on te fiche la paix »

— Je n'en ai aucun à vous donner, bande de salopards. Dix ans de bons et loyaux services, et vous me lâchez pour une histoire à dormir debout ! Vous déconnez complètement. L'homme avait retrouvé de sa superbe.

— Qui a cherché à vous recruter ? Insista Cole Véranie, en rajustant ses lunettes sur son nez.

— Personne, je suis déjà acquis à la cause, je vous rappelle ! Qu'est-ce qu'un petit merdeux vient me faire la leçon ! Le captif regimbait à présent. « Ruth, c'est pas avec ton gros cul que tu irais t'infiltrer dans les groupes que j'ai fréquenté, espèce de… »

— Quels groupes ? Les écrivains ? Les musiciens ? Tout le monde fait ça, vieux singe, ferme donc ton clapet et écoute. Ce qui te vaut d'être là c'est…

Ruth s'aperçut que son prisonnier ne l'écoutait plus. Il fixait son visage À tout dire, il fixait un côté de son visage, et plus précisément son oreille gauche. Elle tenta d'y voir mais bien entendu, la gymnastique s'avéra impossible.

— Qu'est-ce qu'il y a ? aboya-t-elle en direction de Cole, qui paraissait n'avoir rien vu de l'étrange attitude de son ex-collègue.

Le jeune homme parut ne pas comprendre.

— Lance un test ! Lui ordonna-t-elle, sans se départir de son sang froid.

— Hein ? Que… près un flottement de quelques secondes, le docteur se dirigea vers une armoire à code fichée dans un des murs de la salle. Elle s'ouvrit dès qu'il eut fini de taper 4 chiffres, et l'homme récupéra un objet rangé à l'intérieur, sorte d'appareil photographique muni d'un zoom, — mais ce n'était pas un appareil photographique —, il s'agissait du dernier modèle de dénicheur. Le D 2068. Cole Véranie empoigna l'objet, et appuya sur une série de boutons colorés. Son sourire s'était encore élargi. L'objet s'anima, le zoom s'allongea, ressemblant à un tuyau d'aspirateur et tel un serpent, fit lentement le tour de la grosse femme avec force de cliquetis et de clignotements colorés. Le docteur Véranie visionnait en direct des images sur un mini écran fixé sur le boîtier de la machine.

— Alors ? Fit Ruth sans bouger d'un pouce. Sa voix trahissait pourtant un stress évident.

— Ben… ole prit son temps. « Ah merde ! Y'en a une qui se balance sur votre boucle d'oreille ! Ah trop drôle, la bestiole, putain, chef, ma première en direct ! »

— T'es vraiment le plus con de tous mes récents collègues, toi, hein, soupira Ruth. Elle regarda furtivement Jack, qui se marrait doucement et s'était de nouveau rapproché de la vitre.

Cole n'écoutait pas, semblant hypnotisé des acrobaties de l'intruse, par écran interposé.

— C'est un type fée, Ruth. Il ne sait même pas à qui il a affaire, ce nigaud ! Cria Deneahy.

— La ferme, tas de merde ! Boucle-là, l'invectiva Ruth, touchant malgré elle son oreille.

— Une fée ? Interrogea encore Cole, soudain rêveur. « Une vraie fée ? »

— Ton flingue, abruti, sort ton flingue ! Lui ordonna le professeur Boulder. « Shoote-là ! » ajouta-t-elle sans pour autant hausser la voix, — Les fées sont sensibles aux variations de la voix humaine, très sensibles —. Et très malignes.

Derrière la vitre, Jacky Deneahy explosa d'un rire dément. Il était trop tard, et il le savait parfaitement, Boulder aussi, à voir son air de plus en plus affolé et beaucoup plus grave, la fée aussi le savait.

Elle fit un clin d’œil à Deneahy, et, agita furieusement la tête. Un éclair lumineux comme un soleil sortit de son minuscule chapeau, un galurin à grelots qui émit une série de tintinnabulements ressemblant à s'y méprendre à un carillon miniature. Ruth renonça à la faire discrète et beugla en direction de Cole,

— Shoote-là !Explose lui sa gueule de lutin !

Dans la cacophonie générale, Cole, — hélas pour lui —, ne réagit pas assez vite.

Sa tête reçut l'éclair lumineux et explosa contre la vitre, sa cervelle dégoulinant à son tour le long du carreau — derrière lequel se tenait un Deneahy hilare —, laissant une traînée rosâtre à laquelle se mêlait de gros grumeaux grisâtres.

— Saloperie ! Brailla Ruth. Elle sortit de sous sa robe en polyester bleue réglementaire un pistolet non moins réglementaire.

— Elle ne te fera pas de mal tant que tu ne la vois pas ! Tu ne la vois pas n'est-ce pas ? L'enjoignit son ex-collègue balançant entre sarcasme et pitié. « les fées n'agressent que leurs agresseurs. Fiche-lui la paix ! »

Après tout, la grosse dondon avait partagé sa vie durant dix années ! Un attendrissement qui faillit lui coûter très cher, car Ruth bavait de rage à présent et se dandinait de manière agressive devant la vitre blindée. Pour autant Boulder travaillait pour le gouvernement, et le gouvernement lui avait confier une mission. Elle n'y dérogerait pas.

— C'est une de tes imaginaires ! L'accusa-t-elle s'approchant et tapant de son poing désarmé le carreau maculé de miasmes sordides.

— Même pas ! Hoqueta-t-il, « on me l'a refilé à l'occasion d'une partie de poker, et depuis, impossible de m'en débarrasser, Ruth, soit raisonnable, ce n'est pas si grave. Tu peux encore nous sortir de là. Je peux tout à fait gérer cette situation »

— Nous ? Espèce de simulateur ! T'es mort mec !Glapit la femme en tirant sans sommation dans la vitre. L'alarme, qui mystérieusement, jusque-là, ne s'était pas déclenchée, se mit à rugir.

Le doc s'était instinctivement baissé, même s'il savait la pièce à l'abri des balles. Il ne riait plus.

— Je vais te tuer ! Cracha Ruth, réarmant l'A.P.I. (l'annihilateur de particules imaginatives, pour les profanes).

— Pour avoir eu un peu d'imagination ? Demanda Jack de dessous le muret qui lui servait provisoirement de bouclier anti-ruth pète au casque.

Celle-ci se contenta de tirer de nouveau, mais manqua sa cible, alors que deux soldats entraient au pas de course dans la salle 7 suivi d'un grand type malingre, chauve, vêtu d'une longue blouse grise et d'épaisses lunettes. Les militaires la désarmèrent sans ménagement mais ne la ceinturèrent pas, ils se tinrent néanmoins à coté d'elle, dissuasifs. De l'autre côté de la salle, Jack se redressa. Ruth se tourna vers l'homme en gris. Son épais menton tremblotait sur sa non moins épaisse poitrine. Elle marmonna.

— Stephen, butez-moi ce délinquant et sa fée !

— Vous savez bien que c'est impossible, Ruth, vous perdez la boule ma parol ! Le gouvernement vous emploie justement à repérer et interner ceux qui nous les invente, ces êtres magiques, ces monstres horrifiques, ces…

— Pas le moment de faire un inventaire de tout ce cirque ! Dégagez-moi Clochette de ma boucle d'oreille ! Vitupéra Ruth en secouant sa lourde coiffure afro de gauche à droite à en avoir le tournis. Et effectivement, elle chancela un instant avant de reprendre sa litanie de plus belle. « Retirez-moi ça ! Retirez-moi ça !… »

— Il n'y a pas de fée dans notre monde, Ruth, soupira Stephen, calant ses grandes mains dans les poches de sa blouse, visiblement déçu du comportement irrationnel de son employée. Ce qui mit un terme quasi instantané aux braillements du professeur Boulder.

— Où est-elle ? Reprit celle-ci plus calmement à l'adresse de Stephen, ignorant totalement sa précédente tirade. « Où est-elle ? » Elle n'en démordrait plus.

— Jack saurait certainement le dire, puisqu'il la voit : elle lui appartient, en quelque sorte. Elle fait partie de son monde, de son imaginaire, crut bon d'expliquer celui qui se faisait appeler Stephen, sachant la femme perdue.

— Mettez un coup de détecteur, je veux savoir où se planque cette petite pute ! Le somma Ruth.

— C'est un être imaginaire, pas un être vivant. Cette chose disparaîtra quand celui qui l'a imaginé disparaîtra… fuiiit ! Temporisa Stephen, — il en savait bien long sur le sujet —. Il devait son actuelle et importante place au sein de la hiérarchie à de belles captures, notamment ce type, un écrivain, avec un nom à coucher dehors, répertorié sous le matricule PKD, mort depuis. Mais quels services il avait rendu ! Un imagination comme celle-là ne pouvait être laissée en liberté ! Il y avait veillé. Un ricanement le ramena à la réalité.

— Alors butez-le lui !! l'invita Ruth en indiquant son ex-partenaire, le docteur Deneahy, suffoqué par cette injonction brutale.

— Oui, oui, ça viendra, la rassura le-dit Stephen, responsable du département des affaires oniriques, en observant son prisonnier, mais d'abord, il nous faut répertorier toutes ces choses sorties de cette imagination débordante, avant de le disséquer proprement…

— Tu crois aux fées ! Beugla Jack en direction d'une Ruth éberluée, sans se préoccuper des propos inquiétants de Stephen.

— Je n'y crois pas du tout ! protesta l'autre et tout son corps tressauta dans un réflexe de dégoût.

— Tu la vois ?

Ruth s'approcha de la vitre, enjambant le corps de Cole Véranie sans y prêter attention. Elle scruta son ex-partenaire, cet éminent professeur, ce traître.

— Non, je ne la vois pas. Je vois rien. Elle le fixait droit dans les yeux en disant cela. « Je vais te buter toi et ta putain de fée de mes deux par la même occasion ! »

— Croire c'est voir ! Gronda Jack.

— Bien sûr que non, le harangua la femme, sa chevelure s'agitant autour d'elle comme autant de ressorts flamboyants. Espèce de vieux débris de chiotte ! Je chasse ces conneries depuis trois décennies, je les jette dans des culs de basses fosses pour éradiquer ces mondes imaginaires où certains peuvent encore se réfugier, échappant au contrôle de notre bienveillant gouvernement ! Alors j'y crois pas une seconde, connard ! Elle était rouge brique et gesticulait, comme atteinte de danse de Saint-Guy.

— Voyez-vous, Ruth, les interrompit Stephen, « le problème avec les chasseurs, aussi excellent qu'ils soient, c'est qu’ils ne savent que rarement passer la main avec dignité… ils finissent… au rebut.»

Stephen, imperturbable, fit un signe aux deux soldats,

— Embarquez-moi cette terroriste, conclut Stephen.

Tandis que les militaires la saisissaient, Jack lui tira une vilaine langue bleue, sur laquelle une fée minuscule, vêtue d'un bikini rose fluorescent, surfait à l'aide d'une coquille de noix encore plus minuscule.

Ruth roula des yeux comme des soucoupes, suffoquant sous le coup de la surprise.

— Allez embarquez-là !gronda Stephen à l'adresse des gardes. « Et revissez-moi ce chapeau sur la tête de notre ami le professeur, j'ai eu ma part de dingueries pour aujourd'hui.  »

Il appuya sur un bouton fiché dans le panneau de l'armoire. Aussitôt, une rangée de fléchettes sortirent des murs de la cellule où se trouvait jack. Celui-ci tenta de s'en protéger, mais une l'atteignit dans le cou, puis une autre sur le dos de la main et il s'écroula comme une masse, de nouveau sous le joug de ses tortionnaires.

De son côté, Ruth protestait de toute sa large personne, et donna du fil à retordre à ses geôliers. Sa robe en polyester, bleue et réglementaire, n'y résista pas longtemps. Elle fut bientôt traînée manu militari dans un couloir où une certaine porte à hublot attendait d'être ouverte… peut-être y aurait-il cette fois un panier, ou un tire-fesses ou encore une fusée, derrière la porte du couloir aux lumières orangées, pour l'envoyer tuer son imagination, à vingt mille lieues sous la terre…
