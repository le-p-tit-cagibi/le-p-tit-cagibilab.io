---
title: L'Oasis
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "A faire"
epub: true
pdf: true
draft: true
---
L'oasis

I

— Patron, vous êtes vraiment obligé de faire ça ?

Le jeune gars, un asiatique tiré à quatre épingle, cheveux gominés et cravate sobre, observait une mine dégoûtée.

Le patron en question se contenta de trifouiller de plus belle, avec le bout d'un stylo plaqué or, un cadeau sans doute, dans ce qui ressemblait — si peu désormais — à un cadavre. Il s'acharnait notamment autour du visage verdâtre. Le cou du macchabée avait triplé de volume et nonobstant le tragique de la scène, la caboche ainsi rendue minuscule aurait relevé du plus haut comique en d'autre circonstances. Mais ni l'un ni l'autre ne rigolait. Le jeune gardien de la paix respirait fort, et portait son regard au-delà de la viande avariée, tandis que le vieux capitaine, concentré, plissait les yeux derrière ses verres épais, des lunettes vintage hors d'âge. Qui portait encore des lunettes de nos jours ? Il ne semblait incommodé ni par l'odeur, ni par le mort. Celui-là valait qu'on s'y attarde un peu : Des cheveux longs et gris s'éparpillaient, collés par une substance indéfinissable sur ce qui restait de son visage. Le nez avait disparu, ainsi qu'une lèvre, comme ravalée par son propriétaire. La langue, d'un vert tirant sur le jaune sale, dépassait d'entre les dents de manière obscène.

— Qu'est ce que tu vois ? Demanda le patron, toujours accroupi près du corps.

— Une charogne dégueulass ! Commenta l'autre sans état d'âme.

— Un peu de respect, sergent Sarcy, contente-toi de faire ton job.

Parfois, il était difficile de savoir si le capitaine Claizieux plaisantait ou pas. C'était un type pince-sans-rire, un vrai dinosaure, comme on en trouvait quasiment plus en 2043. Mais un excellent flic À cinquante-sept ans, on ne pouvait pas dire qu'il eut fait carrière, loin de là, et pourtant il avait un vrai instinct dans les affaires les plus délicates. Enfin, son jeune collègue aurait dit, avec une certaine justesse, dans les affaires les plus pourries. Preuve en était qu'on se retrouvait à battre la campagne en plein milieu de l'Oasis, une banlieue abandonnée à l'est de Paris.

David Sarcy soupira et commenta d'une voix monocorde.

— Le type est claqué depuis une petite semaine À voir comme il est maigre et déglingué, je dirais qu'il se shootait, ou qu'il buvait, ou les deux.

— Rien de plus pertinent gamin ?

Michel Clairzieux de ses deux mains gantés de bleu appuya sur le torse du mort, et descendit en palpant la charogne jusqu'aux pieds. L'un était nu, et l'autre n'affichait que 4 doigts de pieds sales.

— Il s'est fait bouffer un orteil par un rat.

— Oui, possible, il y en a quelques uns ici. Mais encore petit ? Écoute bon san !

Il réitéra la manœuvre, palpant de nouveau le corps avec plus de vigueur.

— Arrêtez, ça fait un bruit tout à fait horrible, patro ! S'exclama David qui trouvait l'examen quelque peu indécent, même si ce type, vivant, n'était certainement pas grand-chose de plus qu'une raclure.

— Bien vu et ça me chiffonne ce bruit qu'il fait, ce cadavre. Comme s'il avait été mâchonné du haut en bas. Sauf le visage, le visage, c'est autre chose, ajouta-t-il pour lui-même.

— Bon, ok, le suspect est un ruminant… on recherche une vache cannibale patron ? Et le jeune homme se mit à rire.

Le patron se redressa sur ses pieds, ce qui eu un double effet, comme kisscool, — vous savez, cette pub rétro, un peu délirante —, mais en dix fois plus rapide. Pour vous dire que le vieux était tout de même auréolé d'une sacrée réputation. Ça eut pour effet numéro un de stopper immédiatement le rire idiot de Sarcy, promotion 2040, plus tout à fait un bleu, mais bon, il avait encore beaucoup à apprendre. Ce qu'il faisait, sous son air facétieux et sa dégaine de comptable, parce qu'il en avait dans le ciboulot, comme disait le patron. Deuxième effet, Sarcy retrouva une certaine jugeote.

— Z'avez remarqué tous ces corbeaux là-haut sur le muret de l'ancien supermarché ?

— Ah quand même ! Le patron fit une moue appréciatrice.

— Ça fait un bail que j'en avait pas vu autant… convint David Sarcy.

Ah oui, il faut que je vous précise qu'en 2043, la faune et la flore, sans avoir disparu des zones urbaines, y étaient devenues rarissimes. D'abord parce que, on peut s'en douter, la pollution loin de diminuer avait augmenté, mais aussi du fait du coût de la vie, prohibitif, qui engendrait disons, des comportements d'hommes des cavernes chez toutes les personnes crevant de faim. Mieux vaut une maigre pitance, que pas de pitance du tout. D'où notre étonnement à voir des rats et des oiseaux sur ce terrain vague agrémentés de ruines diverses, HLM, chapelle, mosquée, et aussi un hypermarché, une station essence (non, pas de fausse joie, Y a plus un brin d'essence là-dedans depuis la crise de 2030, vous faites pas d'illusion !) et un abattoir. Nous nous tenions présentement face à cet antique monument de barbarie, tout au fond de ce no man's land. Loin au-dessus de nous passait les trams aériens, silencieux et rapides comme le vent. Nous étions dans une zone morte. L'ombre des nouvelles constructions, là-haut, nous écrasait. J'étais pressée de retourner d'où je venais, ce coin ne m'inspirait rien de bon.

— Berru, t'as pris des notes ? T'as rien à dire ?

J'aimais pas trop quand le patron m'interpellait comme ça. Moi, j'étais juste la petite stagiaire, qu'on appelle aussi cadet, et accessoirement secrétaire. J'avais pas vraiment l'impression que mon opinion, ou mes remarques comptaient plus qu'une crotte de chien, mais bon, faut croire que l'inspecteur Clairzieux voulait nous donner une leçon de savoir vivre, ce matin.

— Un des oiseaux a une crête jaune, c'est plutôt étrange pour un corbeau non ?

Je me suis pensée, y va m'incendier, le vioque. Eh bien pas du tout.

— Étrange, je te l'accorde. Mais revenons à notre ami raide comme la justice.

— Il est à poil et il a des pustules assez répugnantes éparpillées sur tout le corps. Mais bon, pour les fringues, rien de bizarre, possible qu'elles aient été de suite recyclées, enfin, je suppose. Il y a toujours quelques SDF qui traînent dans ces zones pourries. Bien que celle-ci soit particulièrement désertée depuis longtemps. Le tatouage est fait main, pas de première jeunesse et pas un chef d’œuvre non plus. On dirait des initiales, — AF — peut-être ?

— Peut-être, répéta l'inspecteur. Il avait une façon de vous regarder. Ça faisait froid dans le dos parfois, comme s'il voyait à travers vous.

— Prends des photos, après on rentre.

Il s'éloigna de la scène de crime sans plus se préoccuper de nous. Sarcy me regarda, l'air — j'en sais pas plus que toi —. Je me demandais pourquoi Clairzieux avait pris cette affaire. Juste un crime crapuleux, un règlement de compte entre petites frappes pour une dose ou une histoire de territoire. C'était toujours la même rengaine non ?

Je haussai les épaules, me contentant de faire ce qu'on m'avait demandé, pressée d'en finir et de retourner au chaud, dans notre 2 pièces au quatrième de la rue de Lamarmite, dans le 27 ième district de Paris. Pas le plus reluisant, mais on était pas non plus dans les plus mal lotis. Avoir 30m2, c'était le pied, quand j'y repense. De vieux Mac prônaient sur nos deux bureau IKEA branlants. La connexion, pourrie, tenait encore et il y faisait chaud. Un luxe ! La crise du logement, la crise des énergies, la crises alimentaire, bref, on manquait de tout, à cette époque, sauf qu'on se rendait pas vraiment compte. On avait encore la TV, le nanophone, et toutes ces plate-formes pour nous abreuver de rêves, de besoins, d'envies et on avait le cerveau carrément ramolli. Enfin, pas tous, mais beaucoup. Dans cette perceptive, vous comprendrez mieux que notre petite équipe sortait du lot. On résolvait pas mal de petites affaires, sans faire de vagues, sans subvention, juste en suivant le flair du patron. Au 27 ième district, Clairzieux, c'était un peu une légende, déclinante, pas loin de la retraite, mais une légende tout de même. Il avait un beau tableau de chasse, et pourtant, une humilité de jeune communiant. Il recherchait pas la publicité et laissait tout le cirque aux pontes dans les bureaux luxueux au-dessus de nous. Le patron, il aimait la traque, fouiller la vase et trouver une trace. Il lâchait jamais. C'était un type de terrain.

Depuis sa sortie de promo, Sarcy bossait avec lui. Moi, ça faisait tout juste deux ans. Je me demandais parfois si j'allais pas passer ma vie à être stagiaire. J'en avait régulièrement marre de servir le café et taper des rapports, mais il était malin, Claizieux, il achetait ma bonne volonté en me traînant de temps en temps sur une scène de crime, et je marchais comme ça, au petit coup d'adrénaline. En l'occurrence, cette affaire-là fut un gros coup. On l'a plutôt pris par derrière d'ailleurs. Et si je suis là pour en parler, soyons honnête, c'est au patron que je le dois. L'histoire risque d'être un peu décousue, parce que je ne peux raconter que ce que j'ai vu, entendu, ou ce qu'on m'en a raconté. Le reste… faudra vous faire à l'idée qu'il y aura des trous. Comme un gruyère géant. Une enquête gruyère. Mais n'ayez craint ! Ça vaut le coup d'écouter le truc.

Après ça, rien n'a plus jamais été pareil. Mais je mets la charrue avant les bœufs, expression du patron, moi en 43, j'en avais jamais vu des bœufs. Peut-être que dans certains coins reculés de France, deux trois broutaient encore, mais bon avec la loi anti-viande, plus personne ne s’intéressait vraiment à ces gros bestiaux dont les races tombées en déclin étaient appelées à disparaître par manque d'utilité finalement. Quand le m2 est hors de prix, on adopte un hamster. Et encore, faut en avoir les moyens et un permis spécial. On rigole plus avec le bien-être animal.

Je devrais peut être me présenter, moi c'est Olive Berru. Avant d'intégrer l’équipe de Clairzieux, je patrouillais dans la maraude, et je poursuivais des études de droits. Avec les grèves universitaires de l'an dernier, comme les bourses ont été supprimées, — en représailles —, et que la moitié des universités sont plus en capacité de recevoir des élèves vu les ruines, et que l'autre moitié est désormais réservée aux nantis, dixit mon patron, jamais avare de nous enseigner un peu de vocabulaire, je pense pas que j'y retournerai À l'université j'entends. Sarcy me tuyaute pour que je postule à l'école de police, mais ni aujourd'hui ni demain ce ne sera pour moi. C'est juste une parenthèse. C'est tout du moins ce que je me racontais.

Et puis il y a eu cette affaire.

Autant que je vous en fasse profiter, c'est pas tout les jours qu'on se voit raconter une histoire pareille, si ça m'était pas arrivé, je vous rirais à la gueule et vous me traiteriez d'affabulatrice, mais essayez seulement, et je vous casse votre petit minois en deux. Car tout est vrai. Ne vous en déplaise.

Quand on est rentré au bureau, après un passage obligé au Nutristore, pour caler notre dalle, vu que le patron nous avait appelé aux aurores rayant l'option petit-déjeuner, j'ai transféré les photo, et chacun a passé un moment dans son coin à dépiauter la scène de crime pour en extraire quelque chose. Ici, notre devise, c'est : on avance, on avance, on avanc ! Même si, parfois, on avance tellement lentement que j'ai l'impression de reculer. Le patron dit que le temps n'a pas d'importance, seule la vie est importante. Sa phrase fétiche. Il nous la fourgue à tout bout de champ, — c'était une réplique d'un film des années 80 —, un truc vachement ringard, si vous voulez mon avis. David se fout du patron avec ça, mais jamais devant lui. Il aurait trop peur de s'en prendre une.

Bon, en épluchant tout ce fatras, deux choses nous ont tout de même sautée aux yeu :

Il avait des ongles entretenus, une peau et des cheveux propres, pas de vermine, hormis le fait que ce corps ait été comme mâché, comme aplati, ce n'était pas le corps d'un type qui traîne dans un foutoir comme l'Oasis depuis des lustres À se demander ce qu'il fichait là.

Secundo, il avait été déplacé. Il y avait trop de traces autour du corps pour en tirer quoi que ce soit, mais il n'avait pas été tué devant l'abattoir. Il avait plus une goutte de sang dans le corps, ça se voyait comme le nez au milieu de la figure (merci patron) mais pas de sang autour du macchabée.

II

— On a pas grand-chose, patron, risqua Sarcy en lisant le rapport d'autopsie le lendemain. La seule chose quelque peu remarquable est son taux anormalement élevé de cortisol…

— Cause de la mort ? L'interrompit aussi sec Clairzieux.

— Ben… le jeune flic feuilleta le dossier immatériel plusieurs secondes, comme pour vérifier quelques chose.

— Heu, je dirai inconnue. Tout les organes, bien qu'ayant subi une sorte d'écrasement, probablement post mortem, étaient en bon état. Par contre, il manque quasiment 4 litres de sang et il y a ces traces de brûlures, pas très grandes, mais profondes, de véritables petits cratères à différents endroits du corps.

— Un mort en bonne santé en somme, ironisa le patron, rabattant son vieux tee-shirt des Stones (encore un truc disparu depuis belle lurette, cherchez pas) dans un jeans étamé. Il était toujours fagoté comme l'as de pique (une de ses expressions favorites) et ce jour-là ne dépareillait pas. Au pieds, ses éternels mocassins en daim, et par dessus le tee-shirt, une veste en jeans, avec quelques pins (collector à l'heure actuelle, je suis sûre qu'il aurait pu en tirer une petite fortune, s'il les avait vendus).

— Venez les enfants, on va faire un tour.

Notre enthousiasme dut se lire sur nos figures juvéniles, non encore aguerries à simuler joie, peine, et orgasme sur commande. Mais il était déjà sorti.

III

— Me demande ce qu'il compte trouver dans ce monceau de ruines, dis-je, mi-figue mi-raisin en aparté à mon collègue plongé dans l'observation d'une flaque de boue où des traces de pas semblaient sortir de nulle part, près d'une immense bâtisse déglinguée.

On était de nouveau dans l'Oasis À la place du corps refroidi de la veille, à quelque distance d'où nous nous tenions, il y avait des rubalises LED qui formaient plus ou moins un rectangle autour d'un corps matérialisé par une impression phosphorescente qu'on appelait phospho, dans notre jargon, — les flics sont pas réputés pour leur imagination, alors pas de commentaire merci —. Le ciel était gris et autour de nous, pareil, du gris partout. Ça foutait le bourdon, cet endroit.

— Quand j'étais gosse, se mit à me confier David, faisant fi de ma remarque, les yeux toujours posés sur ces fichues traces de pas, — des pieds d'enfant j'aurai dit, vu la petite taille —, « le défi, l'année de tes dix ans, c'était de venir ici et de grimper en haut ». Il leva les yeux en direction d'une vieille cheminée d'usine écroulée.

— La cheminée de l'abattoir est tombée il y a quelques années, poursuivit-il devant mon air effaré. Il avait soudain l'air trop sérieux et cela accentuait sa jeunesse, et j'aurai dit, une certaine fragilité. Je ne savais pas qu'il était originaire du 35 ième district. Remarquez, personne ne s'en serait vanté.

Le patron baguenaudait un peu plus loin. Il avait une allure du diable ce jour-là. Comme il avait fort plut, muni de bottes en caoutchouc d'un jaune pétant et d'un imperméable du même acabit, on aurait dit Titi sorti de son cartoon, — un très vieux cartoon, je vous l'accorde — pour une virée dans la vraie vie. Il revint vers nous en s'ébrouant, car la pluie continuait de nous balancer un crachin glacial. Il essuya ses culs de bouteille et les remis sur son nez.

— Alors vous avancez ?

— Patron… Y a des traces de pas de gosses là… ommença Sarcy, dont le visage aux traits de poupée commençait sérieusement à ruisseler.

— Avez-vous remarqué qu'on nous observe depuis notre arrivée ?

Je tins mon chapeau, un truc en microfibre soit disant étanche mais qui commençait à ressembler à une serpillière, et fit un tour complet sur moi-même.

— Vous êtes sûr patron ?

Il se contenta de hocher la tête sous sa capuche jaune.

David fut plus prompt à découvrir les espions en question. Ils possédaient un plumage de suie, dégoulinant, et se tenait en rang serré sur un ancien poteau électrique qui gisait à terre, à quelques encablures de notre position, non loin du parking de l'hypermarché.

— Vous faites une fixette patron, c'est juste des piaf !

Clairzieux fonça vers eux. Enfin, foncer est un grand mot quand la boue aspire chacun de vos pas, mais il fit de son mieux pour amorcer une manœuvre rapide.

Avec Sarcy, on eut le même réflexe — très bête —, à vrai dire : le suivre sans discuter.

Bon, les bestioles ne nous ont pas attendu, et bien avant que nous les ayons rejointes, elles décollèrent de leurs ailes lourdes de pluie, sans un bruit. C'était une vision lugubre. D'autant que, en deux rencontres, pas un croassement n'était sorti de ces becs-là. Depuis quand les corbacs jouaient la discrétion ? Ils s'engouffrèrent dans ce vieux bâtiment de béton, dont le toit effondré révélait une grande partie de l'architecture métallique, le tout partiellement rouillé, tordu.

L'abattoir.

On voyait encore partiellement le nom au-dessus d'un fronton à demi défoncé :

ABATTOIRS MUNICIPAUX

Ville de Paris grande et petite Couronne

Le patron avait son idée pour les corbeaux, j'aurai parié un dollar chinois si j'en avais eu, mais il n'a rien dit. Il plongea simplement à leur suite dans l'obscurité de la bâtisse et disparut. Je resserrai mon sac sous mon manteau, mes notes à l'abri de la pluie au fond de mon grand cabas et suivis le mouvement. Sarcy évita une première ornière à l’entrée, mais plongea généreusement sa basket de marque dans la seconde. Il émit un juron.

— Chu ! Nous intima le patron, qui se tenait non loin du seuil, planté comme une statue, dans un recoin sombre puant la vieille graisse et le gazole. Nous obtempérèrent.

Dans l'immensité du hall, sous la musique lancinante de l'eau dégringolant sur les murs et la vieille ferraille qui composait notre abri, nous finîmes par distinguer des voix. Les oiseaux avaient par contre disparu.

— Ça vient d'en dessous, pronostiqua Sarcy en chuchotant, tentant d'essuyer maladroitement sa godasse foutue.

— Oui absolument. Tu connais un peu la géographie du lieu non, David ?

Ce n'était même pas une question, le patron en savait un peu sur notre loustic, tout au moins qu'il avait traîné ses guêtres dans le coin étant gamin.

— On a plusieurs sous-sols, avec pas mal de surface, expliqua mon collègue. Il avait pris sa mine d'élève studieux, agitant les bras dans toutes les directions à la fois me sembla-t-il.

— Ici, la réception des livraisons, le tri des bestiaux, aux sous-sols les lignes d'abattage, la découpe et les chambres froides. Mais… j'ai jamais foutu les pieds là-dessous ». Il prit une mine contrite de petit garçon en passe d'être puni.

Le patron poursuivait son idée.

— Combien de sous-sols à ton idée ?

— Au moins trois, plus sûrement quatre, pour tout ce qui est technique.

Je chuchotais aussi.

— Ça craint patron. Si Y a des indigènes, vaudrait mieux revenir avec du renfort, non ?

L'inspecteur Claizieux me regarda. Il semblait interloqué. Comme s'il avait momentanément oublié ma présence et que d'un coup, il se demandait ce que je fichais là ou pire, qui j'étais… Je n'ajoutais rien et attendit l'engueulade. Il en poussait de sévère parfois, mais à vrai dire, c'était plutôt contre le système qu'il s'emportait, rarement contre les hommes.

— Si j'avais voulu un régiment, Berru, il serait là. Si on veut enquêter ici, faut se faire petit. Tout petit. N'est-ce pas David ?

Lequel rentra les épaules sans moufter. C'était quoi leur problème ? Ces deux-là me cachaient quelque chose et manifestement c'était important. Ça m'a passablement énervé. J'ai râlé en chuchotant :

— Mais quoi alors ?

— Le patron a raison, ici, faut la jouer discret, Olive. Quand j'étais gosse, on s'est fait une paire de frousses dans cet endroit, et pour finir, nos parents nous on carrément interdit de jouer ici.

— Et tu as déménagé, ajouta le patron en ôtant sa capuche.

Ses cheveux poivre et sel luisaient, trempés, comme ses yeux, clairs et francs au travers des lunettes. Un regard auquel vous ne pouviez guère mentir, vous pouvez me croir !

— Oui. Un accident, un gamin s'est pendu à un câble en essayant de traverser entre deux poutrelles, par là, — il eut un geste vague en direction de la droite du hangar — et le câble a lâché. Il a fait une chute d'une dizaine de mètres. J'étais pas là quand s'est arrivé. Avec mes potes, on était passé par là une bonne douzaine de fois avant. Mes parents ont pas supporté.

Le petit Sarcy, avec ses grands yeux noirs en amande semblait sur le point de chialer. Il respira une grand coup et fixa le patron.

— Vous, vous savez autre chose, dit-il d'un ton forcé, presque accusateur.

Comme s'il n'avait attendu que cela, le patron nous embrouilla encore un peu.

— Juste que cette affaire, c'est pas une nouvelle affaire, dit-il sans émotion.

— Hein ?

Ça, on l'a dit en même temps — Sarcy et moi —. On était pourtant pas plus étonné que ça au fond. Depuis qu'on avait approché ce cadavre, la veille, le patron était pensif, et puis, il avait toujours une longueur d'avance. Ça s'expliquait donc. Une — cold case — comme on dit dans le jargon policier.

— En trente ans de carrière, ça fait quelques macchabées mâchouillés que je rencontre. Et toujours dans les parages de l'Oasis.

Il avait l'explication avare parfois, le patron.

Sarcy et moi nous regardâmes de connivence : le capitaine Clairzieux avait un squelette dans le placard.

— Ah ouais patro ? Pourquoi vous nous avez pas craché le morceau dès hier matin ? Le reproche et le dépit pointait un peu sous les mots du jeune flic, — il avait un ego surdimensionné pour un asiatique —, loin des lieux communs de l'humilité qu'on prête à cette race millénaire. Sa mère était française, ça expliquait peut-être cette incongruité. En tout les cas, il rongeait son frein, Sarcy. Curiosité, c'est un peu le maitre mot de notre job et là faut dire, le patron nous frustrait pas qu'un peu en faisant sa mijaurée ! Il comprit d'ailleurs tout de suite le message.

— Tout doux les enfant ! Rien dans les vieux dossiers. La juste répétition de celui du jour. Juste des types retrouvés sur ce secteur, et dans un sale état, se fendit économe, notre patron laconique.

— Avec ce tatouage AF ? J'avais soudain retrouvé ma langue.

— Oui, bien évidemment Berru, sinon il n'y aurait pas de concordance entre ces meurtres. Mais il souriait en disant cela.

— Ton copain t'es sûr que c'était un accident ? Ajoutai-je en direction de Sarcy.

Mais Clairzieux me prit de vitesse.

— En tout cas pas de tatouage, et pas de mâchouille, pensez si j'ai vérifié à l'époque !

— Bon, on descend ? David paraissait mal à l'aise, mais on aurait le temps d'approfondir plus tard, l'heure était plutôt à tenter de dénicher l’origine de ces voix qui nous parvenaient toujours de quelque part dans les sous-sols tandis qu'on faisait des plans sur la comète.

IV

Le patron initia le mouvement. Plus loin, dans les gravats, gisaient les restes de plusieurs monte-charges inutilisables. Encore au-delà, des camions et camionnettes déglingués, de ceux qui avaient une boite de vitesse et des pneu ! Pour vous dire les antiquités.

Ayant réussi à nous frayer un chemin dans ces décombres, nous dégotâmes en fin de compte les escaliers : Coté gauche du bâtiment, tous les gravas formaient un enchevêtrement devant les cages, il nous fut donc impossible d'accéder par ce côté. Nous eûmes de la chance cependant parce qu'à droite, en évitant soigneusement la zone du crash du pote de Sarcy, — il y avait une sorte de fosse, et je n'avais aucune envie de savoir ce qui s'y trafiquait du temps où l'abattoir tournait à plein régime —, nous découvrîmes une partie du toit se maintenant à environ deux mètre du sol, et les cages d'escaliers juste en dessous, y affleuraient, emplies de poussière, de cochonneries diverses, mais apparemment praticables.

À pas de loups, nous descendîmes sans piper mot. Parfois, par inadvertance, un débris roulait, et dévalait quelques marches. Nous stoppions alors notre crapahutage, et tendions l'oreille, à l’affût mais les voix poursuivaient leur soliloque inaudible, et nous reprenions alors notre lente et pénible progression.

Sur le pallier du premier sous-sol, la porte qui donnait sur le second avait disparu, et un grand trou béait sous nos pieds : les escaliers en direction des étages inférieurs avaient eux aussi disparu. On irait pas plus loin.

Le patron nous fit un signe de la main. Nous nous rapprochâmes de l'entrée du premier sous-sol. Les voix nous parvenaient nettes et claires maintenant. Ça sentait tout de même le traquenard, et je faillis m'en ouvrir au patron, mais j'avais à peine fait mine d'ouvrir la bouche que David m'agrippa par la manche. Nous écoutâmes, rencognés dans l'escalier.

— Va les cherchez Finaude, disait une voix graillonneuse, s'ils se ratent, on va les retrouver froids comme des limandes.

— Faut que je me sèche avant, cette pluie, ça te glace jusqu'au sang, répondit sans doute la dite Finaude, sur un ton pleurnichard.

— Laisse, Tom Chauve, dit encore une troisième voix, plus grave, « j'y vais ».

V

Un froufroutement se fit entendre, — nous étions plus immobiles qu'une pyramide dans la vallée des rois —, puis de derrière un pilier dont ne restait que l'armature de fer, un homme sorti, un homme assez remarquable par son allure. D'abord, il s'agissait d'une PSDN, — comprenez, personne de stature différente de la norme —. Un nain, quoi. Mais bon, ça fait belle lurette qu'on les nomme plus ainsi, rapport aux discriminations. La peine de mort, ça rigole pas. Vous avez intérêt à tenir votre langue en 2043, sinon, Y a beaucoup à parier que vous n'irez pas en centre de vieillesse finir vos jours dans une de ces maisons de retraite dorée (enfin, c'est ce qu'en dit la pub, je connais personne qui ait atteint cet âge-là moi, mes parents sont morts jeunes, de l'amazonite aiguë) mais ce gars-là faisait dans l'originalité : ses cheveux, une sorte de couronne duveteuse jaune, l'auréolait comme un Jésus dans les vieilles encyclopédies. Et quand je vous dit jaune, c'est jaune. Tournesol quoi. Bien que le tournesol, j'en ai pas connu en vrai, mais à la crèche, il y en avait une ribambelle peints sur les murs.

On s'est regardé tous les trois et ça nous a fait tilt, cette tignasse, mais on n'a pas moufté, un peu soufflés par l'assurance du petit gars qui s'approchait de nous tranquillement, comme s'il était en passe de recevoir de bons amis. Quand il ne fut plus qu'à quelques pas, nous constatâmes qu'il était âgé. Mais vraiment quoi. Pas comme le patron. Je doutais d'avoir vu un humain aussi vieux de toute ma vie. Son visage, ses mains, étaient parcheminés comme un antique manuscrit de la mer morte. Le nouveau venu ne se départit pas de son calme et nous jaugea l'un après l'autre, l'air serein. Émanait de cette étrange petite personne une certaine affabilité. Il se tenait face à nous, les mains dans une sorte de cote de travail bleue qui avait l'air d'avoir vécu. Il levait la tête pour nous regarder droit dans les yeux, avec effronterie, les pupilles très sombres. Il s'attarda surtout sur le patron. Je remarquait qu'il avait un tatouage à demi effacé sur son biceps gauche rachitique, je voyais pas trop bien ce que c'était, l'encre se perdait dans les plis et replis de sa peau. Sarcy croisa mon regard. Il avait remarqué aussi et discrètement enclencha son nanophone. Le petit bonhomme se mit à causer sans nous accorder la moindre attention. Une voix de vieillard.

— Bonjour Michel Clairzieux.

— Bonjour Triche, dit Clairzieux au bout d'un petit moment sans se départir de son flegme.

Alors là, avec Sarcy, on s'est vraiment senti cons. Le patron se foutait de notre gueule, s'en était inconvenant.

— Voilà un bail  ! Ce sont tes partenaires ? Fini le travail en solitaire ? Le dit Triche optait pour un ton légèrement moqueur et le tutoiement ! Lui aussi en savait plus long que nous sur le patron et cette affaire, ma main à couper.

Notre supérieur hiérarchique hocha la tête sans répondre, et nous présenta succinctement. Étrange moment, quand j'y pense. On aurait dû creuser tout ça, avec Sarcy, mais nous avons été, comme qui dirait, emporté par les événements.

La petite créature vieillotte nous a à peine calculer.

— Approche capitaine, c'est bien ça maintenant, n'est-ce pas ?

Claizieux barguigna et nous fit une fois de plus signe de le suivre, et j'avoue qu'on s'est exécuté sans poser de question. Trop curieux de voir la suite. C'était qui ce gugusse ? Il n'avait pas l'air d'un clodo celui-là, tout le contraire même, jolie prestance, même pour un vieillard, et propre sur lui…

Un peu plus loin, après avoir passé plusieurs couloirs et portes sur ce même palier, nous tombâmes sur un couple rassemblé autour d'un brasero. J'avais vu des photo, mais jamais eu le bonheur de connaître la chaleur de vraies flammes sur ma peau À voir la tronche de Sarcy, lui non plus. On eut un peu de mal à se concentrer sur nos hôtes.

Le petit vieux fit un tour de table.

— Je vous présente Finaude.

Une femme tout en os, avec une pelisse sur le dos, et un chignon haut comme une montagne, tremblota dans la pénombre. Elle aussi accusait un âge respectable.

— Et voici Tom chauve, termina-t-il.

Là je vous laisse deviner pourquoi. Celui-là par contre était le plus jeune, la cinquantaine tout de même, et sapé un peu comme mon collègue Sarcy, une tenue complètement décalée dans ce sous-sol miteux. Il portait un sabre dans le dos, — le genre sabre de samouraï—. Malgré moi, je tâtais ma poche intérieure, voir si j'avais bien mon holster et mon Remington dedans.

— Et moi c'est Face de rat, dit une voix grinçante.

On l'avait loupé ce rat-là ! Un costaud, si je puis dire, dans les deux mètres peut-être, au plus fort de ses belles années, à présent voûté comme une arche d'église, sortit d'un coin d'ombre. Il portait des dents en or, ou plaquées peut-être, tout le monde en portait de toute façon, une peau sombre comme la nuit et nous souriait largement tout en poursuivant sur le ton de la conversation, « Et ça me va comme un gan ! ». Il se mit à rire d'une voix perçante fort désagréable. Le blanc de ses yeux luit étrangement par dessus les flammes quand il se rapprocha du brasero. Il ressemblait à un rat, aucun doute, même ses oreilles, et sa moustache, — quelques poils tout au plus, mais longs ! —, on s'attendait à ce qu'elle s'agitent comme celle de ses prétendus congénères.

— Arrête de vouloir impressionner nos invités, le tança Triche.

— Vous casserez bien une graine avec nous autres ? Ajouta-t-il et cela déclencha un fou rire général chez ses comparses.

Je regardais David en songeant : mais où est-ce qu'on est tombé ?

Il haussa les épaules en signe d'impuissance.

Le patron semblait plutôt à son aise. Il prit place sur une grosse tuile ferrugineuse, tendit ses mains vers les flammes, savourant cette chaleur vivante et réconfortante, puis dé-clipsa son imper, et la palabre commença.

Peut-être la fatigue, ou ce qu'ils nous donnèrent à béqueter, voire les deux, dans tous les cas, j'aurai juré qu'on avait causé (surtout écouter, en ce qui concerne David et votre servante) cent ans, et pourtant, nous nous retrouvâmes à l'entrée de l'Oasis à peine une trentaine minutes après notre arrivée en bas.

Le minus et ses comparses nous avaient raconté une histoire qui tenait à peine debout. Le patron n'avait encore pas commenté, probable qu'il nous laissait digérer le truc. Mais c'était pas comestible, cette histoire.

En deux mot, leur petite bande se chargeaient de rediriger les intrus vers la sortie, pour qu'il n'y ait pas d'embrouille, parce que c'était leur ghetto, et celui de personne d'autre. Mais parfois, un errant passait au travers, et ils le récupérait dans un état lamentable, — un tantinet mâchouillé pour tout dire — au hasard de leur pérégrinations dans l'Oasis.

VI

— Une belle bande de baratineurs, sans vouloir vous offenser, patron, vos copains, d'ailleurs pourquoi s'éreinter à sortir des cadavres du sous-sol au risque d'attirer l'attention ? S'emporta le soir même un Sarcy qui malgré son jeune âge ne supportait visiblement pas d'être pris pour une bille.

On s'était de nouveau réuni dans notre crèche de la rue de Lamarmite. C'était le soir du délestage, du coup, on se caillait. Pas de chauffage ces douze prochaines heures. J'étais emmitouflée dans trois ou quatre couches de lainages, façon lasagne. Sarcy n'avait pas vraiment céder à la négligence vestimentaire rajoutant un très beau manteau de tweed par dessus son costard. Le patron, quand à lui, avait troqué son titi plastique contre une grosse veste de laine irlandaise doublé de peau de mouton — synthétique, évidemment —. La flasque de vodka, sur le coin du bureau, se trouvait presque rétamée. En un sens, ça aiguisait nos esprits, et puis ça nous ravigotait un peu sinon, on aurait gelé sur place.

— C'est vrai ça ! Ajoutai-je par solidarité envers mon partenaire.

— Calme les enfant ! La bande de Triche ment comme elle respire, commenta le patron avec patience, « mais on fait ami-ami sinon… »

Je lui coupai la parole, — une fois n'est pas coutume —, ce qui me valut tout de même un regard empli de reproche quand à mon manque évident d'éducation.

— Sinon on ne pourrait pas accéder au site, dis-je comme si je sortais là autre chose qu'une évidence.

Mais Clairzieux poursuivit tout de même pour mon édification personnelle et celle de mon collègue.

— Disons que ce ne serait pas facilitant. Ces lascars tiennent l'Oasis depuis sa fermeture.

— N'empêche, vous avez vu ce Triche ? Sa touffe jaune ? S'étonna à voix haute Sarcy.

— Comme le corbeau ? Ajoutai-je rêveuse.

— Ben oui, quoi, comme le corbeau ! Il fit la moue. Il n'y avait, pour le moment, pas d'eau à apporter au moulin, mais tout cela était pour le moins étrange.

— On ne les a pas revu ceux-là d'ailleurs, conclus-je à l'intention du patron qui nous écoutait dégoiser sans rien dire.

— Non… David devint pensif, et se plongea soudain dans ses ordinateurs à la recherche d'indices sur ce lieu que nous n'avions fait qu'entrevoir quelques heures plus tôt.

Le patron sirotait à présent un jus de racines, une de ses marottes, ça embaumait notre bureau. Mon ventre gargouillait, je crevais de faim.

— On se fait une pause et je vous raconte ce que je sais les enfants, ne soyez pas impatients, temporisa-t-il.

En fait, il se jouait d'illusions, le patron, à cette heure-là, on était pas impatients mais carrément claqués. Pourtant, il n'avait pas l'intention de nous lâcher. Dormir… on pouvait s'asseoir dessus !

VII

Plus tard encore dans la soirée, tandis que je grignotai le quignon de mon pain sans gluten, le patron nous repassa les images du macchabée de la veille et de bien d'autres, que David et moi ne connaissions pas. Des photo dégueulasses, mais bon, la faute à la technologie de cette époque-là. Les événements remontaient aux années 2015, 2020. Autant dire le crétacé pour la définition de l'image.

Ça me coupa le reste de l'appétit, car même de mauvaise qualité, les photo montraient tout de même des charognes hautes en couleurs, bien faisandées, avec des traces de profondes brûlures, des visages ravagés, et bien entendu, le fameux tatouage AF. On avait pas du tout avancé à ce sujet d'ailleurs.

— Dites, pendant la guerre mondiale de 1940-45, AF c'était pas un sigle ?

Voilà ce qui m'agace chez Sarcy, son côté un peu pédant à vouloir se la jouer super intello.

Il s'envoyait un bol de thé vert. Un truc hors de prix. Parfois, je supposais qu'il s'arrogeait des accointances un peu filoutes, mais quoi ! Je doutai et me reprochai vite mes soupçons devant son air ingénu…

— RAF jeune inculte. Le patron lui rabattit le caquet en deux deux.

— Vous savez que ce coin a subi une pluie de météorites au seizième siècle ? Ajouta Sarcy sans complexe.

— Ah ouais, ça m'en bouche un coin ! Et joignant le geste à la parole, j'enfournais mon dernier bout de pain au risque de m'étouffer tant celui-ci était rassis.

— Intéressant tout ce salmigondis, mais qu'en tirons nous au juste ? Questionna Clairzieux nous ramenant subitement les pieds sur terre.

— Ben je ne sais pas, peut-être que Y a pas que des vieux SDF qui habitent là-dessous ! S'exclama David.

— Des petits bonshommes verts ! Dis-je la bouche encore empâtée, roulant des yeux ronds en agitant mes longues mains pâlottes devant moi.

David devint rouge comme un radis puis éclata de rire. Même le patron eut du mal à retenir un rictus ironique.

— Pourquoi ? c'est possible non ? L'affaire du yéti, c'était bien ça ! Après presque cent ans de théories en tout genre, alors pourquoi pas ici ? Chouina Sarcy entre deux hoquets de rire.

Je froissais mon sachet en papier et l'envoyais effacer ce sourire idiot du visage de ce jeunot imbécile. Il évita mon projectile avec grâce.

— On nous baratin ! Lança Sarcy retrouvant son sérieux. « Pourquoi sacrebleu ? »

— Peut-être bien, approuva le patron, — il rangeait les reliefs de son repas dans une boite pour son petit-déjeuner. C'était le roi de l’anti-gaspi —. Il est vrai que, au prix des amendes, si t'avais le malheur de jeter le plus petit relief comestible —, fallait plutôt faire gaffe.

— Allez, racontez-nous patron, vous êtes descendu sous le premier niveau pas vrai ? L'encouragea David, avec son inénarrable petite bouille de — moi gentil garçon — Chiant, je vous l'accorde, mais irrésistible.

Je sorti mes notes. Mon nanobook était lent à démarrer, mais le patron encore pi. Comme s'il hésitait devant l'inévitable. Parce que, entre nous, il nous avait conduit où il voulait, donc il n'allait pas nous planter en si bon chemin, mais il prenait son temps.

Je reçu un coup dans les cotes, David me fit un signe de tête. Le patron avait commencé son histoire, et j'avais loupé le début, perdue dans mes rêveries intuitionnistes. Je raccrochai le récit en plein milieu d'une phrase.

VIII

— Et je n'ai pas eut le temps de me pencher là-dessus à nouveau. Ça c'était au début de ma carrière. Ton abattoir, tu verras, David, si tu cherches un peu, il a fermé en 2015. Je l'ai connu, ce quartier, quand ça marchait encore, l'abattoir et l'hypermarché aussi, enfin ça bouillonnait de vie À l'époque, Triche habitait l'Oasis — Il y avait des immeubles tout au fond, qui ont disparu — et tout le monde le surnommait déjà ainsi ! —. Ils étaient toute une bande, bien plus qu'aujourd'hui, composé notamment d'Eddy, dit Frankenstein à leur tête, un certain Serge, une allure d'armoire à glace, Rénan dit Taupe, parce qu'il y voyait goutte, même avec ses lorgnons, et Isabelle, que tout le monde appelait Belle. Ils bossaient tous aux abattoirs, désosseurs, bouviers, bouchers, ouvriers, c'était des boulots durs, qui payaient mieux que beaucoup dans cette partie de la ville mais c'était les dernières années avant les lois anti-viande, alors les gens qui bossaient dans ce genre de filières en prenaient plein la figure. Ils étaient dénoncés sur les réseaux, honnis et persona non grata dans les clubs, les bars, les cinémas… 'était pas loin de l'enfer pour eux, mais le travail manuel devenant une rareté, beaucoup y trouvaient tout de même leur compte.

— Persona quoi ? Dis-je avant de me rendre compte que si j'avais des questions, je ferais bien de les noter quelque part et d'attendre la fin du récit, car le patron me foudroya du regard, et Sarcy encore pi : il me tendit le Lexico d'un air entendu. Je me pelotonnai sous mes pulls sans un mot de plus. Clairzieux poursuivit sur un ton monocorde, le regard absent.

— Et voilà que les jeux olympiques sont prévus à Paris pour 2023, alors tout ferme boutique, les grandes rénovations sont lancées dans la petite et la grande couronne. Le quartier se vide de ses habitants, il est prévu de créer le village olympique dans ce coin, et ceux qui peuvent partir, — parce qu'à l'époque, c'est pas une rigolade la crise du logement —, partent.

— Et les autres ? Marmonnai-je, même si, de nos jours, les choses n'avaient guère changées ; il existait d'autres zones plus ou moins abandonnées, dans les dédales de l'arachnéenne mégapole urbaine qu'était devenue la capitale. Certes moins vastes que cet Oasis, mais tout autant peuplées de ruines.

Ni le patron, absorbé dans ses souvenirs, ni Sarcy qui gobait ses paroles, ne s'aperçurent que j'avais usé d'un petit commentaire.

— Bon, je vous la fais courte, les enfants, on m'appelle pour celui-ci, fin 2015. Clairzieux pointa une photo un peu floue du doigt. Le gars en question n'ayant plus figure humaine, je me fiai au résumé succinct qu'en fit notre narrateur.« Un jeune désosseur récemment au chômage, qui squattait dans les HLM vides attendant leur démolition. L'affaire a été vite bouclée, on avait rien. Sauf ces gugusses, la bande de Triche, qui hantaient déjà l'abattoir et tenait les éventuels squatteurs à distance »

— Et c'est tout ?

— L'étrangeté vint avec la répétition, jeune fille, poursuivit le patron avec indulgence. De temps en temps, on nous appelait, et on récupérait un nouveau cadavre bizarrement amoché. Les collègues restaient pas, et personne n'a vraiment fait de lien. Les meurtres s'étalent sur une vaste période. Et puis les victimes, parlons-en : des paumés, anciens habitant de l'Oasis pour l'essentiel, qui n'intéressaient personne.

— Avez-vous creusé de ce côté là ? Demanda Sarcy.

Mais le capitaine ne sembla pas l'entendre. Il poursuivait son monologue à présent, comme pressé de vider son sac.

— À chaque fois, je menais ma petite enquête, et ça ne donnait rien. Peut-être que, déjà à l'époque, je voulais que ça ne donne rien. Faire comme tout le monde, oublier l'Oasis. Et pour l'oublier un peu plus, la municipalité a construit cette enceinte, tout autour des ruines qu'était devenu ce quartier. Les nouvelles constructions ont pris de la hauteur, ici, c'est devenu le fumier d'un passé que tout le monde souhaite oublier.

— Oui je viens de voir ça dans les archives, dis-je d'un ton détaché (mais fière de ma trouvaille) un certain Edmond Tanros, lui et sa municipalité ont fait capoter le projet de village olympique, qui a été transféré dans les quartiers Nord, et à la place il a fait construire cette muraille. Vote à l'unanimité. Surprenant non ?

Au fur et à mesure de son récit, le patron semblait rétrécir au fond son vieux fauteuil de bureau. Même sa physionomie paraissait vieillir, se racornir alors même qu'il n'avait pas abordé les choses sérieuses, j'aurai parié gros là-dessus. Je tapais furieusement sur mon clavier, notant chaque mot tout de même, mais sincèrement, je n'en pouvais plus de ce récit laconique. Il manquait des éléments. Sarcy se posait des questions aussi, à le voir affalé sur un caisson débordant de bric-à-brac le regard dans le vague. Pourtant aucun de nous ne moufta. Le patron avait besoin de remonter le fil de son histoire à son rythme. Il s'étira comme un gros chat enfin décidé à partir en chasse sans pour autant donner la réplique à mes — pertinentes — remarques.

— Un soir, reprit-il après s'être de nouveau calé dans son fauteuil, « j'ai reçu un appel anonyme pour signaler du raffut à l'Oasis, c'était Y a dix ans. Je n'avais plus de partenaire depuis belle lurette, — restriction budgétaire —, et puis, ce quartier en capilotade, plus personne n'y mettait les pieds.

— Mais vous y êtes allé, avança Sarcy.

— Serge, Renaud, Eddy et Isabelle gisaient dans le couloir qu'on a arpenté hier, raide comme la justice, les os broyés de l'intérieur, et pourtant intacts, à peu de chose près, leurs tronches rabotées, plus une goutte de sang à l'intérieur… riche avoua rapidement être l'auteur de l'appel anonyme. Lui et le reste de la bande avaient trouvé les cadavres un peu plus bas.

— Quatre d'un coup ! La vache ça a dû faire scandale non ? David parcourait son écran tout en suivant le récit. Il se passait la main dans sa coiffure impeccable. Je le sentais préoccupé.

— Crois-tu ! Quatre minables en moins, ça n'a pas fait de vague, non. Mais je suis descendu pour voir le ventre de l'abattoir, j'en avais marre d'être refoulé par Triche et sa bande, mais en fait, c'est eux qui m'ont manipulé, parce que sans leur appel, je ne serai jamais descendu.

— Ils voulaient que vous sachiez ce qui se trame là-dessous, dis-je. Pourquoi ce revirement ?

— Parce que le phénomène prenait de l'ampleur.

— Phénomène ? Les meurtres vous voulez dire ? Corrigeai-je malgré moi.

— Si tu veux Berru. Mais laisse-moi finir, tu jugeras de la pertinence du vocabulaire ensuite.

— Bien qu'à l'époque tous les bâtiments ne fussent pas aussi dégradés, nous dûmes durement crapahuter pour descendre. C'est là que j'ai fait connaissance des corbeaux. Ils crèchent en bas.

Dans le premier sous-sol, il n'y avait rien de remarquable, mais au second, où se situait l'ancienne chaîne d'abattage et de découpe, ça puait comme en enfer. Une odeur qui te soulevait le cœur direct. Triche et la compagnie m'accompagnaient. On formait une drôle d'équipe, ils n'étaient pas trop incommodés par l'odeur, ni par la bave. Ça m'a surpris.

— Hein ? Je tiquai.

— La bave, j'ai dit, répéta le patron, comme si des milliers d'escargots étaient passés par là, ça luisaient partout sous le faisceau de nos lampes torches. L'air, humide et irrespirable oppressait un peu plus à chaque pas. Et à l'entrée du niveau 3, — les chambres froides se situaient à ce niveau —, j'ai perçu des espèces de borborygmes glougloutants.

— Des borborygmes glougloutants, répétai-je animée d'une certaine répugnance à imaginer la scène. « Et qu'est-ce qu'il vous ont raconté les zigoto ? »

— Qu'ils étaient des sortes de gardiens.

— Ah nan patron ! Là c'est du délire ! Des clodo qui se la pètent et prennent la grosse tête pour se faire mousser auprès de leur copain flic. J'y crois pas une seconde ! Explosa Sarcy. Il avait lâché le thé pour une larme de vodka. Ses joues rosirent un peu de son audace.

— Tu as raison David. Il se tourna vers moi. « Et toi Berru ? Quand nous aurions besoin que ta langue et ton cerveau fonctionne, tu restes muette et sans réagir ? »

Je soupirai.

— C'est pas ça patron, mais je préfère attendre la chute. Forcément Y a une couille dans le pâté.

— Comme tu dis, Berru. Et une grosse, bien que ta métaphore manque cruellement de poésie.

Clairzieux se leva pour dégourdir ses abattis et en profita pour admirer la nuit. Elle était d'un noir absolu. Le patron avaient connus la voûte céleste, avec ses constellations, sa lune ronde et saturne, brillante, qui lui faisait des clins d’œil. Moi, je l'avais découverte à l'école, notre ciel nocturne, — sur plan —. La pollution gâchait tout à présent.

— Sauf qu'il y a bien quelque chose là dessous, c'est ça patron ? Sarcy était nerveux.

Clairzieux acquiesça sans rien dire, fixant le petit asiatique.

— Alors quoi ? M'exclamai-je un peu abasourdie par la tournure que prenait les choses. On aurait dit un roman de science fiction à deux balles. La vilaine bébête sous terre qui croquent les petits imprudents. Hou ! J'ai peur.

Quoique à bien y réfléchir, je n’étais pas à l'aise avec ces deux énergumènes qui avaient l'air de dissimuler pas mal de choses.

David se tortilla sur sa chaise mais j'eus enfin le fin mot de son histoire à lui.

— Je ne suis pas allé bien loin, patron et une seule fois, je vous le jure ! j'ai entendu ce souffle affreux, — c'était un peu avant que Kevin tombe dans ce puits à la con —, et cette puanteur flottait dans l'air. C'était atroce. Je me suis barré.

— Je m'en doute, tu n'étais qu'un gamin, consentit le patron. Il n'y a pas eu d'enquête sur le gosse. Mais on a encore retrouvé quelques macchabées les années suivantes. Je pense que Triche les expose pour dissuader quiconque de venir traîner dans l'Oasis. Et je pense que nos huiles en savent bien plus là-dessus que nous tous réunis. Mais personne ne tient à ce que ça sorte de l'Oasis.

J'étais un peu larguer. Sarcy et Claizieux jouaient les mystérieux. S'ils supposait un quelconque complot, ça devenait carrément du délire.

— la Bande de Triche m'a remonté, poursuivit notre supérieur hiérarchique comme s'il nous racontait le dernier fait divers du coin. Il semblait las, le vieux, mais continua néanmoins son histoire. « Je ne me rappelle pas vraiment, sauf qu'à un moment, je ne pouvais plus respirer, mon corps était compressé, et j'étouffai, il y avait toujours ces glougloutements horribles, mais je ne voyais pas mon ou mes agresseurs. Pour finir, j'ai perdu connaissance »

— Pourquoi vous ont-ils sauvé d'après vous ?

— Pour qu'il soit leur témoin, espèce de retardé, dis-je. David me regarda de travers, offusqué par mon ton peu amène. Je continuai sur ma lancée. « Pour que quelqu'un sache. J'ai raison no ? »J'étais sûre que oui. Le patron me souriait d'un air las.

— Je dirais oui, Berru. Il leur fallait une sentinelle. Personne n'est éternel.

IX

— On va y retourner c'est ça ? Demandai-je le lendemain, tout en connaissant la réponse. Il me semblait que je comprenais pas mal de choses à présent, pourtant, j’étais bien loin du compte…

Le patron acquiesça encore. Il avait vieilli de dix ans depuis le début de notre conversation, — la veille —, pourtant, on sentait une tension et une détermination intacte.

Sarcy leva un sourcil interrogateur. Son cerveau ne connaissait pas la fonction pause.

— Sur le site de la ville, il est noté que l'abattoir est exempte de tout produits toxiques ou chimiques. Ça a été fait dans les règles.

— Il n'est pas question de ce genre de choses, je pense David, dis-je, tout en cherchant l'assentiment du patron. Il se contenta d'esquisser un pauvre sourire. « sinon, toute la ville en aurait pâti. Le phénomène, quel qu'il soit, est circonscrit à l'Oasis »

Je me levai et frissonnai, l'air matinal était frisquet, mais j’étais déterminée à présent.

— Allons-y, il est temps de voir ça de nos propres yeux patron, dis-je.

— Je crois aussi, et je crois savoir par où on pourra passer pour entrer dans les sous-sols de l'abattoir, approuva Sarcy, en finissant de télécharger de vieux plans d'urbanisme sur sa tablette.

Sans ajouter un mot, Claizieux nous lança nos frusques, et nous dévalâmes les escaliers comme si nous nous relevions d'une bonne nuit de sommeil. Chacun de nous avait pris son arme avant de sortir. Le patron avait saisit son vieux sac à dos des Crazy, un groupe de Slashmusic qui n'existait même plus. C'était la première fois que je le voyais emporter cette loque quelque part. Je me demandais si je devais m'en inquiéter.

X

— Vous voyez, on doit seulement emprunter ce couloir qui dessert les services techniques de l'hypermarché, ces couloirs sont communs à plusieurs établissements, dont l'abattoir.

Sarcy indiquait de son index le scan d'un plan jauni et fort ancien. Je n'y voyais goutte là-dessus. Juste de fins traits d'encre partout accolés à des numéros et une légende de dix kilomètres rédigés en pattes de mouches quasiment illisibles.

— Je te suis petit, répondit le patron confiant.

Nous montâmes donc à bord de l’auto-train, station 84, c'était la seule qui desservait encore ce vieux quartier oublié qu'on avait longtemps appelé La Villette dans une époque fort lointaine, et qui avait depuis hérité de ce sobriquet cynique d'Oasis. Une fois devant le mur, on prit la seule porte accessible, mais défoncée, qui se trouvait dans un fouillis de voitures et de caddies. Les corbacs nous lorgnaient perché sur un reste de store banne. Le jaune ouvrait le bec et s'ébrouait en silence. Les autres se dandinaient sur leurs pattes maigrelettes, — Je me demandais furtivement quel goût pouvais avoir leur chair —.

— Venez ! Nous tança David en pénétrant dans l'hypermarché. Vêtu de son costume impeccable à 300 bitcoins, il tranchait terriblement avec le délabrement général ambiant. Il affichait une mine sérieuse, concentrée.

Nous nous engouffrèrent sous un petit hall où subsistaient quelques vestiges de commerces. Une banque peut-être, ou une agence immobilière, la bannière avait disparu, à l'intérieur, des bureaux en piteux état, plus loin dans le prolongement, une boulangerie, avec sa devanture avantageuse au nom prometteur — *La belle miche* —. Il ne restait plus rien d’appétissant à l'intérieur, quelques rayonnages traînaient dans une épaisse poussière, et les vitrines, brisées, nous regardaient passer d'un œil morne. Nos pas résonnaient, lugubres, sur le carrelage défoncé, — nous ne disions plus un mot —. J'en ai encore des frissons d'y repenser ! Quel lieu de désolation… n face, un magasin de vêtement, — *Chez Zia, tout me va !* —, clamait encore l'enseigne à jamais privée de lumignons, la boutique ne contenait plus rien, de gros néons hors d'âge pendaient des plafonds éventrés. Enfin au coin, avant l'entrée proprement dite du supermarché, avec force de caisses et tapis roulants, comme c'était en ce temps-là, un bar. Lui aussi accusait son âge. Il dénotait un peu, avec son écriteau métallique à l'ancienne, tourné face contre terre, et sa devanture peinte en noire mate, avec les ouvertures surlignées de fines arabesques argentées. Vraiment rétro. Je m’aperçus que les oiseaux de malheur nous avaient rejoints, ils se dandinaient sur le comptoir du bar, enfin, de ce qui en restait. L'acajou avait cédé à la moisissure. Un des bords s'était fracassé contre le carrelage en damiers et en avait troué la surface. Le patron se pencha, et retourna avec force le panneau.

Je ne sais pas pourquoi, mais mon cœur s'arrêta une seconde. Je savais sans savoir. Sarcy stoppa net et sa maglite éblouit les mots gravés d'argent qui s'y trouvaient encore.

— Les Affreux Freux — d'une belle écriture gothique.

Sacrée coïncidence non ? Non, bien sûr, vous n'y croyez pas non plus, d'autant qu'en image, une bande de corbeaux cocasses se partageait la déco sur le panneau.

Je retins un cri. Mon collègue fut moins conciliant.

— Patron ! Vous allez encore me dire que ces initiales ça vous dit rien ?

— Ça me dit qu'il faut avancer ! Commenta le patron sans paraître s'émouvoir, « ce n'était qu'un repaire de pauvres bougres après le boulot ! Les gars de l'abattoir étaient refoulés de partout, mais ici, ils pouvaient encore boire une bière, un café sans déshonneur, c'est tout ce que je peux en dire »

Je lui tapotai le bras, un peu furax, comme il disait parfois, et saisis le regard de David, il approuvait ma petit rébellion ; lui non plus n'en pouvait plus des non-dits.

— Alors cette fois, allez au bout patron. Affreux Freux, AF, me dites pas qu'il n'y a pas de rapport ! Merde ! Patron !

— La vulgarité n'est pas nécessaire Berru. Ôtez vos paluches de là ! Gronda-t-il de sa voix de basse. « L'abattoir entretenait une sorte de confrérie, pas vraiment secrète, ça, non. Ce tatouage était un signe de reconnaissance, une façon d'être solidaire. Des compagnons de misère, en quelque sorte. L'époque était tellement dure » souffla-t-il, nous regardant à présent avec indulgence.

— Et vous n'avez pas cru bon d'en parler avant ? Lui reprochai-je. J'en avais ma claque, cette histoire prenait une drôle de tournure. J'avais, comme qui dirait, un mauvais pressentiment.

— Ça n'aurait rien changé Olive ! Et vous avez trouvé finalement, c'est enivrant de trouver le fin mot tout seul, comme des grands non ? Planté au milieu des décombres du bar, on se regardait tous les trois en chien de faïence (expression purement Claizieux).

Je ne pouvais le nier. Et à voir la tronche de mon ami asiatique, ses yeux comme deux fentes, il cogitait aussi. L'horizon, si je puis dire, (de la part de quelqu’un qui n'a jamais vu un coin de ciel bleu) s'éclairait. Tous ces gens étaient liés par une sorte de serment de compagnonnage. Et c'était leur confrérie qui était dans le viseur d'on ne savait quoi, au troisième sous-sol.

— Bon, dit Sarcy, on s'arrache ?

Peut-être qu'il voulait en finir, ou qu'il ne voulait pas agonir le patron. Je pense plutôt qu'il avait une dette à encaisser. Un truc en souffrance depuis longtemps. La mort de son pote, il ne se l'était jamais pardonné. Il avait eut la trouille. Je n'ai jamais connu ce David Sarcy là.

Il a donc foncé jusqu'au fond du magasin comme si le diable était à ses trousses, et Claizieux et moi, on n'a eut d'autre choix que de courir pour le rattraper. On sautait par dessus les détritus, qui ne manquaient pas. On volait presque.

Et on l'a presque rattrapé. Presque.

Mais au final, nous sommes arrivé trop tard, — Ces asiatiques, je vous l'ai dit ! Des cabochards prétentieux ! — Et voilà où ça l'a conduit.

Notez qu'il y aura deux trois tâches sur ce foutu papier à la con (un très vieux papier, mais impossible d'écrire cette historie sur un nanographe ou autre, sinon, je ferai pas de vieux os, c'est sûr) j'espère que vous arriverez à lire tout de même. J'aimais bien David. Et David est mort ce jour-là.

Un long couloir se prolongeait sous l'hypermarché, au décours du bar, à l'opposé des caisses du supermarché, la porte qui y menait n'opposa qu'une résistance de pure forme. Sarcy passa outre et s'enfonça dans le boyau. Heureusement que je m'étais munie d'une lampe de poche, car nous le perdîmes de vue rapidement. Mis à part des tuyaux, plutôt en bon état, qui jalonnaient les plafonds du couloir, il n'y avait rien à voir ici bas. Le patron respirait fort à mes côtés, refusant de lâcher prise. Au bout, un escalier, j'ai saisi dans mon faisceau un bout du manteau de David qui s'envolait au-dessus d'une rampe d'escalier rouillée, puis ses pas ont résonné entre les parois de béton, s’éloignant rapidement de notre position. Le patron ahanait maintenant, et j'osais pas le laisser seul. On est descendu plus bas, choppant un second couloir, dont les murs poissaient l'humidité, et puis sommes tombée face à une lourde porte métallique dont le mécanisme venait de se refermer derrière notre collègue. Je n'avais jamais vu ce genre de serrure et il me fallut un instant pour en saisir le fonctionnement.

— Dépêche-toi ! M’adjura Claizieux, qui me poussa sans ménagement, et se chargea de déverrouiller le battant. Son sac à dos pendait sur le côté, il soufflait, le teint chiffonné, et je voyais les carotides battre le tambour sous la peau rubiconde le long de son cou tandis qu'il opérait.

Le levier claqua de manière sinistre et nous pénétrèrent dans ce que je peux appeler aujourd'hui l'antre du monstre.

Oui. Riez si vous voulez vous ne rirez pas longtemps, je vous le garantis !

XI

Sarcy avait définitivement une petite avance, et c'est un premier hurlement qui nous fit redoubler d'efforts À une bifurcation, Tom nous attendait, il portait son sabre dans sa main, façon samouraï, et ça aurait été à hurler de rire dans d'autres circonstances. Là j'avais envie de hurler tout court.

— Vite ! Dit-il en s'adressant au patron.

— On va l'avoir cette fois ! Dit Claizieux en lui pressant le bras une seconde, puis il répéta, « on va l'avoir cette fois ! » Et reparti de plus belle, le ninja et moi sur les talons.

Ça se mit à puer tout de bon au croisement suivant. Les couloirs s’éclaircirent sans rapport avec le faisceau de ma lampe torche, en fait les murs et le sol luisaient étrangement. On avait changé de secteur : Nous avions rejoins les sous-sols de l'abattoir. En vérité, quelques mètres plus loin, nous franchissions une porte de bois totalement pourrie, et il n'y eut plus de doute possible. Nous remontâmes d'un niveau, zigzaguant à travers un véritable dépotoir. Le sol, rendu glissant par la prolifération de moisissures et champignons, exhalait des odeurs infâmes, à l'arrière goût cuivré, qui soulevaient le cœur.

Personne ne moufta cependant. Tom chauve patina sec au bout d'une rampe assez raide, et sur le mur, peint en rouge je pus lire :

— ***Attention zone de haute Réfrigération*** —

et encore :

— ***Tenue adaptée obligatoire*** —

Nous y étions. Troisième sous-sol.

XII

Les cris de Sarcy nous parvenaient clairement à présent, mêlés à d'autres, mais ce sont les glougloutements qui étaient devenus le plus flippant. Un peu comme une marmite sur le feu depuis trop longtemps, qui siffle et expulse ses vapeurs avant de cramer le dîner. Sauf que là, le dîner c'était nous.

Je ne vis pas grand-chose au début en entrant car une forte vapeur, épaisse et puante comme poix obstruait la vue À travers ce brouillard, j'entrevis cependant des ombres gesticulant, et entendis les premières explosions. De petites explosions. On se battait ici.

Les détonations cognaient les murs et renvoyaient dans mes oreilles des secousses à me faire tomber les dents.

Je m'approchai encore, le patron était parti devant, avec un Tom chauve hurlant comme une furie, sabre en avant.

Je ne vis pas de frigo à proprement parler, tout était en ruines, mais d'un coup m’apparut une masse, enfin, un gros quelque chose rose, qui semblait ramper sur le ciment et balançait des projectiles sur mes compagnons d'infortunes, encore à plusieurs mètres d'où je me tenais. Ma vue était totalement brouillée par ces émanations suffocantes. Je ne cherchai plus quelle était cette odeur dont mon estomac ne se privait pas de me signaler qu'elle l’incommodait au plus haut point : c'était celle du sang.

Je distinguai alors Finaude, allongée par terre, son beau chignon défait et ses cheveux répandus autour de sa tête. Ses membres avaient adopté des angles rocambolesques : elle avait rendu l'âme et ça n'était pas beau à voir À côté d'elle se tenait Triche, c'est lui qui lançait des pétards (enfin c'est ce que je supposai que c'était) sur la masse gélatineuse et rosée qui se tenait une peu plus loin. Les projectiles semblaient la tenir momentanément en respect, mais sans autre effet notoire. Je vis David aussi, appuyé par Face de rat. Il faisait feu avec son Luger dans la chose glougloutante tout en braillant dans sa langue natale, — je ne savais même pas qu'il parlais japonais —.

Une sorte de crachat sanguinolent atteignit le dessus de ma convers, et instantanément, elle chuinta crânement puis pris feu. Je hurlai et me mit en devoir de me déchausser illico presto. Le patron revint vers moi, j’aperçus ses yeux bleus dans ce merdier, et puis il se pencha pour tirer de son sac de quoi buter la chose monstrueuse, je supposai, j’espérai, je priai (pas sûr, mais possible non, quand on crève de trouille…). Je le perdis à nouveau de vue.

— Comme je vous ai dis, il y a des trous dans mon histoire parce qu'il est impossible de raconter ce que l'on ne sait pas, ou à fortiori ne voit pas. Je n'ai pas envie d'inventer, par respect pour eux tous, je ne veux pas déformer. Je vous livre ce que je sais, c'est tout —.

Et puis c'était un fameux bordel. J'y voyais rien. Il m'a semblé que Triche disparaissait en poussant des cris de sioux dans une sorte d'excroissance rosée, une peu comme une bouche de calmar, mais d'une taille indécente. Je ne jurerai pas de ce que j'ai cru voir. Il y eut beaucoup de hurlements, et toujours ces glougloutements. Et cette odeur atroce de sang. Ce goût ferreux dans la bouche.

Mes pieds se sont pris dans une lanière du sac à dos du patron et je l'ai ramassé machinalement. Pendant ce temps-là, plusieurs explosions, très fortes, et pourtant sans grand effet au final, se sont produites, le monstre gélatineux grondait toujours et je n'avais pas l'impression qu'on en venait à bout — après moi personnellement, je n'avais pas fait autre chose que tirer quelques munitions réglementaires avec mon Remington et surtout ôter ma satanée chaussure. Le glaviot du monstre brûlait quand même ma peau à présent, à bas bruit, mais sans relâche et je souffrais. Je compris que cette horrible chose avait de sacrées ressources.

Soudain, sortant du brouillard, David me rentra dedans, il perdait beaucoup de sang, j'en reçu sur le visage et horrifié, m'ordonnai de ne pas ouvrir la bouche pour ne pas risquer d'en avaler mais cette pensée fut rapidement remplacée par une autre, quand je vis furtivement le — visage — de la monstruosité. Enfin, non le, mais les visages, plus exactement. Une face de gelée rose composée de museaux, d’œils et naseaux déchiquetés de différents animaux qui avaient certainement assidûment fréquenter les lieux — et pas pour rigoler, entendons-nous bien — Des oreilles de cochons, ânes, bœufs, moutons, des cornes aussi, grandes, petites, tordus, ou torsadées, encadraient cette tête horrible qui surmontait un corps tentaculaire, massif et rosâtre. L'air était saturé par ce goût de sang. Je vomis sur mon pauvre David qui s'était écroulé à mes pieds, aussi mort que possible, ses jolis yeux brûlés par un jet de cette merde que propulsait l’horreur là-bas qui ne cessait d'émettre ces glouglou glaçants. Le patron attaquait à présent au pistolet automatique, ça résonnait comme dans une basilique, mais les glougloutements ne cessaient pas et je doutai qu'il vienne à bout de cette chose immonde de cette manière. Tom jouait toujours du sabre, il entaillait et découpait le monstre avec une grande dextérité, mais pas vraiment de précision, prenant soin de se protéger du mieux qu'il pouvait avec une sorte de bouclier sommaire — un couvercle de poubelle — m'a-t-il semblé. Son imper fumait de partout. Je me dis qu'il allait s'embraser et crever bientôt. Là aussi, je me gourai du tout au tout.

Le patron battit en retraite et ce fut la dernière fois que je le vis.

Une affreuse tentacule rosâtre lui enserrait la cheville droite. Il ripa sur le sol bétonné, perdant ses lunettes dans une lutte acharné pour se redresser. Tom le couvrait, tentant de cisailler la chose immonde qui le tenait prisonnier, sans succès. Je réarmai mon Remington, pur instinct de flic, et déchargeait la barre de munitions sur l'abomination. Sa tête semblait enfler et désenfler sans cesse, les yeux, cornes et oreilles, se mouvant dans un ballet cauchemardesque en tourbillons infernaux. Les corbacs, arrivés d'on ne sait où s'étaient joints à la fête et attaquaient la créature en piqués silencieux, claquant du bec au-dessus du tas de gélatine rose. Bien sûr, ça ne lui fit aucun effet, si ce n'est qu'elle rendit coup pour coup. Le corbeau à la collerette jaune reçu un mollard en plein dans sa petite caboche. Il tomba comme une pierre sur le dos de la chimère infernale qui l'absorba aussi sec. Je fermai les yeux. J'avais bien vu ce bec de calmar encore. Je ne voulais plus rien voir de ces atrocités. Ça ne pouvait pas exister. Je m'exhortai à y croire. Rien ne pouvait être vrai.

— Va t'en ! Me cria le patron, à bout de souffle. Je vis que sa jambe se consumait attaquée par plusieurs crachats du monstre. Ça devait lui faire un mal de chien. Pourtant il résistait encore, tentant de sortir de son sac, accroché à mon épaule, un objet que j'eus tout d'abord du mal à identifier. Je l'aidai comme je pus, une fois mon pistolet à nouveau vide. Le machin en question était retenu par une bretelle du sac à dos. Je tirai d'un coup sec, libérant enfin l'objet que Clairzieux saisit avidement. C'était une mine dernière génération. Je me demandai comment il se l'était procurée, question hors propos, je vous l'accorde, mais on ne maîtrise pas ses pensées quand une monstruosité gélatineuse faite de restes d'animaux de boucherie morts vous prend à partie dans un sous-sol glauque comme le cul de l'enfer. Le patron me sourit de toute ses dents, — de ce sourire triste que je lui connaissais bien — et brailla à mon oreille.

— Barre-toi Berru ! Et il me repoussa de toutes ses forces. Je m'agrippai à lui pour ne pas chuter, arrachant la manche de son blouson fétiche À ma grande surprise, le bras nu jusqu'à l'épaule révéla un tatouage que je ne connaissais que trop. Il me secoua sans ménagement tandis que je gardai les yeux rivés sur ces deux lettres gravées en gothique.

— Remonte ! Commanda le patron à Tom chauve qui suait sang et eaux juste à côté de nous mais ne tenait plus face à la riposte de mollards gros comme des noix que nous balançait sans cesse le monstre en rafale. Son bouclier était percé de toutes parts.

— Remonte-là avec toi ! Filez nom d'un chien ! Brailla-t-il encore, — il était déterminé, le patron, rien ni personne, je m'en rends compte aujourd'hui, n'aurai pu le sauver —, et je le vis armer la mine. On avait désormais plus qu'une poignée de secondes pour sauver notre peau, Tom et moi, mais mes pieds pesaient trois tonnes. Ma première brûlure s’aggravait, je sentais qu'elle mordait ma chair de plus en plus profondément. Une autre taquinait aussi ma hanche. Je n'arrivais cependant pas à bouger.

Les glougloutements se firent encore plus intenses et vrillèrent mes oreilles alors que Tom me saisissait par le coude et m'embarquait à moitié sur son dos, son foutu sabre toujours à la main.

Il courut autant qu'il put, abandonnant le patron à son attaque kamikaze et lorsque nous atteignîmes le premier des couloirs, après la rampe, une déflagration de tous les diables nous projeta au sol avec une violence inouïe. Mon nez racla méchamment le béton. Je m'écroulai sur le dos de Tom qui ne bougea plus. Un concert cauchemardesque de braiments, meuglements, bêlements, et autres cris — de ruminants en majorité —, atteignirent le couloir au moment où Tom chauve se redressait enfin, me traînant plus loin par le col comme un chiot par la peau du cou. Je hurlai de terreur, pissai sur moi, et finis tout de même par me remettre debout, courant alors comme une dératée vers la sortie, oubliant tout, menée par une frousse viscérale. Le samouraï me tannait le train.

Nous débouchâmes enfin à l'air libre, dans le supermarché. Les cris se turent d'un coup, comme si quelqu'un avait coupé le courant. Un lourd silence nous enveloppa.

Tom s’affala le long du panneau du bar des Affreux Freux, il avait nombre de brûlures et haletait, au bord de l'évanouissement. Je le rejoignis, m'effondrant à ses côtés, bientôt tétanisée par des tremblements involontaires et une bordée de larmes aussi violentes que douloureuses.

Un des corbeaux émergea peu après, et ses croassements — Croak, croak ! — trouèrent le silence, claquant comme des coups de fusil. Un seul rescapé, me dis-je. Il avait une aile roussie. J'en éprouvai de la peine, allez savoir pourquoi.

Et je réalisai alors que je m'en étais tiré !

Épilogue

— Dis donc le môme, qu'est-ce que tu fiches ici ?

L'adolescent leva la tête. Il avait des cheveux bruns, longs et crasses, une salopette à la propreté douteuse et son corps anguleux flottait dedans. Il venait de débouler de la station 83, et était pile en train de soulever les tôles cachant la porte du mur d'enceinte de l'Oasis quand je m'étais décidée pour ma petite ronde matinale quotidienne dans cette zone misérable — et dangereuse —, comme vous le savez à présent. Je sortis ma carte de police et lui collai sous le nez.

Le morveux blêmit et lâcha la tôle dans un fracas de ferraille épouvantable.

— Je m'en vais, dit-il en levant les bras. Je m'en vais tout de suite cheffe ! Et il mit sa parole à exécution avec une rapidité surprenante. Un freux déplumé le raccompagna incognito, voletant pile au-dessus de lui.

— Ouais, gueulai-je, « et que je te revoie plus dans ce quartier pourri gamin, sinon, je te boucle t'as compris ? J'te boucle et tu le regretteras ! »

Je lui montrai mon flingue, comme il faisait mine de se retourner.

Il prit définitivement ses jambes à son cou, slalomant entre les détritus qui jonchaient la route près du mur. Il y avait même des bagnoles abandonnées.

Claizieux m'en avait montré une noire, un jour, — enfin, elle était presque entièrement recouverte de rouille, déjà à l'époque —, et il m'avait dit que ça avait été sa voiture préférée.

Une **Subaru** Impreza WRX.

Fallait voir ! disait le patron, ça rugissait comme un lion ! Je voulais bien le croire, j'avais pas connu de lion, sauf au zoo d'Ermenonville, mais il était aussi impressionnant que l'impala miteux empaillé au musée de l'homme.

Moi, la seule vraie bête que j'ai connu, c'est ce monstre qui a bouffé mes amis. Je sais pas s'il est mort. Personne ne sait. Tom surveille dedans, et moi, dehors, pour éviter de nouvelles victimes. On espère qu'il n'y aura plus de type tout aplati qui sortira de là-dessous… On espère qu'on remontera jamais le patron, ou Sarcy. Ni les autres.

Quelques potes de Tom nous ont rejoint. Nous sommes les nouveaux gardiens de l'Oasis.
