---
title: Conte d'un jour particulier
author: Sophie Belotti
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Pâques en Juillet"
epub: true
pdf: true
draft: false
---
Conte d'un jour particulier

C'est un conte d'aujourd'hui, l'histoire d'une petite fleur qui se prénomme Sandy, Sandy du Chili.

Ce n'est pas par hasard qu'elle porte ce nom-là. Ses racines, nourries d'une terre très particulière, née des volcans, lui donne sa teinte orangée. Un orangé qui n'a rien à voir avec une orange, cela va sans dire, ni avec une carotte, ni avec rien que tu connaisses, c'est certain : cet orangé-là ne pousse que là-bas, sur cette île où gisent de grands bonhommes de pierres usés, muets, regardant inlassablement vers l'océan depuis des lustres.

— sans que l'on sache pourquoi —.

Se demande cette petite fleur fragile, postée comme une vigie sur un à pic basaltique dominant le cratère du volcan endormi. Aussi loin que son regard porte, la petite Sandy voit l'océan, — quand il grogne, quand il dort, et quand il fouette ses feuilles de ses embruns salés au plus fort de ses colères hivernales —. Elle a beau se tourner de droite et de gauche, à s'en donner le torticolis, elle est seule sur ce large plateau aride. Il y a bien, plus bas, des joncs jacasseurs et des herbes bleues, mais elles ont la langue trop bien pendue, et se moque de la petite esseulée dans ses jupons orangés.

Sandy se désole, alors que la saison s'avance, et que sa robe veloutée, inexorablement se fane. Ici, un pli dans le creux d'un pétale, là, une ride brune près du cœur, son cœur jaune comme le soleil à midi, — mais midi au Chili —.

Elle verse une petite larme.

— Merci ! Lui lance un oiseau posé à un pas d'elle, — et qu'elle n'a pas aperçu — en gobant la larme avant qu'elle ne chût sur la roche.

— De rien ! Répond-elle distraitement.

— De beaucoup, jolie demoiselle, rétorque l'oiseau. Je reviens de par-dessus la mer, où rien n'étanche la soif, et cette eau que tu as versée était la plus douce que j'ai jamais goûtée.

Sandy observe la minuscule hirondelle : elle n'a que les plumes sur les os, et sa fine tête grise ploie sous le poids de deux épingles de jais et d'un bec luisant.

— Je m'appelle Manutara, dit l'oiseau d'une voix de clochette, — mais une agréable clochette, légère, aérienne même, où tintinnabule une pointe de rire, un soupçon de gaîté, un rien d'insouciance —, une clochette de fête, si vous voyez.

— Je m'appelle Sandy, lui répond la fleur, songeuse.

Elle plisse alors tous ses froufrous, frissonne un peu, puis une seconde larme roule sur le bord de son cœur, vivement happée par l'oiseau. 

Rasséréné, et sans mener plus avant la conversation, Manutara se rencogne sous les larges feuilles au bas de la tige de Sandy, la petite fleur du Chili et s'assoupit.

Sandy n'ose plus bouger. Elle implore le vent de ne pas la faire tanguer, et la pluie, de ne pas tomber, pour le repos de Manutara.

Le crépuscule, remplit d'étincelles et de Faceronde, la dame de la nuit, vient sans que la fleur et l'oiseau n'aient bougé ne serait-ce une patte ou une radicelle.

Au matin, Sandy constate avec étonnement que Manutara a grandi.

C'est à présent une splendide hirondelle bleue, — ce bleu indescriptible, proche de l'argenté, presque noir parfois, aux reflets de moire, et si bleu pourtant ! Un bleu de mer solide, un bleu d'eau dans les flaques quand l'orage couvre la terre, un bleu extraordinaire !— La petite fleur sauvage n'en revient pas.

— Je suis tout à fait rétablie, dit Manutara, étendant ses ailes afin de prendre son envol.

Et il se passe alors une chose merveilleuse, comme dans tous les contes qui se respectent, je présume, — les contes du Chili, en tout cas —.

Poussière et cailloux tombent de son plumage, dans un nuage de poudre grise, qui fait éternuer la petite fleur, — et sourire Manutara —.

— Bon voyage ! Lui crie Sandy.

— Heureux anniversaire ! Gazouille l'hirondelle en prenant de l’altitude jusqu'à devenir invisible dans l'immensité du ciel et de la mer.

Sandy la petite fleur soupire.

C'était si fabuleux d'avoir une amie ! Se dit-elle.

Les joncs et les herbes, plus bas, entendent cela et se moquent de la naïveté de cette fleur excentrique, de cet orange qui n’existe pas, — pas tout à fait, en vérité, il est plus vif, plus doux pourtant, et plus pétillant que ceux que tu connais — À n'en pas douter, les autres plantes sont jalouses, derrière leurs grands airs et leurs virulents commentaires. Mais tout ce brouhaha ne console pas la petite fleur de la perte de Manutara.

Inconsolable, Sandy voit le jour décroître depuis le volcan, et s'endort sur son rocher, au bord du cratère, au bord de l'infini bleu de l'océan.

— Mais elle ne se réveillera plus jamais seule —, sourit encore Manutara, alors qu'elle franchit la cordillère des Andes, — loin, si loin à présent de l’île et de sa si singulière fleur orangée —, se laissant porter par les puissants courants d'air chatouillant les montagnes.

En effet, Sandy, la petite fleur du Chili ouvre les yeux, ce matin-là, 30 juillet,— elle connaît la date, c'est étrange pour une fleur, me diras-tu, mais ne sommes-nous pas dans un conte, je te le rappelle, et dans les contes, toute fleur a un minimum d'éducation, et elle sait la date du jour, — bien entendu —, surtout celle du 30 juillet. Donc Sandy se réveille et elle est tout bonnement stupéfaite !

Que voit-elle ?

De toutes petites choses grises !

Mais le gris de ces choses-là, est un enchantement sous les rayons de l'astre d'or ! C'est le gris du pelage d'un chaton de peu de jours, c'est l'aube du givre à l'hiver entre brouillard et premiers flocons, c'est la cendre du poêle mêlée de diamants, — c'est un monde entier que livrent leurs pétales, et leurs cœurs brillent comme des sequins oubliés par une couturière étourdie… —

Tu l'as deviné, ce sont des fleurs, mais quelles fleurs extraordinaires ! Sur leur pied minuscule, comme piquées sur la roche, elles se tiennent au côté de Sandy, la petite fleur du Chili, et se mettent à disserter, chacune son tour, et parfois un peu toutes en même temps, de la pluie et du beau temps, de l'océan et ses marées, des saisons, des chansons du vent, des jupons de soie, de brocart et que sais-je encore !

Et depuis ce jour, sur l’île de Pâques, les vieux bonhommes marmonnent à qui veut tendre l'oreille — précisément — l'histoire de cette petite fleur solitaire que Manutara exauça.
