Dans une maisonnette surannée, au fond d'une impasse oubliée, dans une ruelle sordide, au milieu d'une ville grise, chaque mercredi et samedi, un curieux rassemblement clandestin se tenait entre le salon étroit mais confortable dont l'unique mobilier se trouvait être un vénérable fauteuil de cuir et de teck, et la cuisine, aux pavés de tomettes de terre cuite inégaux, où ronflait une vieille cuisinière à bois en céramique bleue.

Quatre têtes juvéniles, penchées au-dessus d'un livre illustré, écoutaient sagement une femme dont l'âge était difficile à évaluer ; ses yeux d'un bleu profond, avait une lueur enfantine et donnait à son regard l'éclat de la jeunesse, cependant, en démenti, le contour de sa bouche aux lèvres d'un parme pâle, dessinait un entrelacs de rides. Elle avait revêtu un châle de laine lilas par dessus sa blouse fleurie et ses mains, recouvertes de mitaines de fine dentelle, tournaient avec vigueur les pages d'un grand livre illustré, pointant ça et là d'un index vif un détail de l'image.

Sa voix, musicale, portait haut dans la cabane et enveloppait chacun des occupants, d'une chaude couverture d'attention et de tendresse.

« Ainsi le prince, délaissant sa princesse, s'en fut de par les terres désolées de son royaume, ignorant la neige et le froid, dans l'espoir de reconquérir… »

« S'il-vous-plaît, Dame, pourquoi toujours nous conter des histoires de choses qui n’existent plus ? » Questionna un jeune et frêle garçon à la peau couleur de bronze, la tête couronnée d'une courte tignasse de jais nommé Yan.

« Parce que… » commença la femme, levant un sourcil dans une mimique légèrement comique nullement froissée d'être sitôt interrompue.

« Tant que nous en raconterons l'histoire, ces choses ne seront pas tout à fait disparues, nous garderons la mémoire du monde ancien jusqu'à sa réapparition… » récitèrent ensemble deux autres gamines rieuses, les jumelles Lin et Ch'ui, blondes comme un matin de printemps, les traits rubiconds et Jin enfin, de sa petite voix aigrelette, une jeune fille plus discrète, au teint de porcelaine, le visage parsemé de tâches de rousseur, couronné d'une longue tresse cendrée.

« Bon, je vois que la leçon est bien apprise » approuva la conteuse, cajolant distraitement la tête d'une minuscule souris grise, perchée sur l'accoudoir de son fauteuil.

« Marjorie, je vois l'heure de rentrer chez soi qui approche » lui dit-elle sur le ton de la confidence. L'animal se lança dans un récital de couinements aussi bref que vitupérant. « Yan, Jin, Ch'ui, Lin, veuillez-vous préparer les enfants » ajouta-t-elle, rajustant son châle sur ses épaules puis se levant avant de pousser une bûche de belle taille dans le ventre dodu du poêle.

Dans une belle et gaie pagaille, tout ce petit monde se prépara au départ.


« Vous êtes en retard, je me faisais un sang d'encre ! » les accueillit maman à la sortie de l'ascenseur. « Jin, file faire tes devoirs, ton ordi-instructeur va t'interroger à 19h » et elle poussa l'enfant en train d'ôter sa veste dans un couloir gris. « Fais vite ! » l'encouragea-t-elle, la poussant encore, un triste sourire émergeant de son visage oblong et fatigué.

« Elle sait quasiment sa leçon, ne t'inquiète pas, Ama » dit l'homme secouant son gilet recouvert d'une fine couche de poussière grisâtre. « Quel temps ! L'usine tourne à plein régime, l'horizon est complètement bouché ! »

Elle le foudroya du regard .

« L'horizon est bouché chaque jour, François, inutile de s'appesantir sur cette fichue météo ! Je préférerais que nous abordions un sujet oh combien plus sérieux, en l'occurrence, cette classe subversive à laquelle tu t'acharnes à vouloir que ta fille assiste chaque semaine de chaque année, et ce, au péril de sa vie ! Je refuse que cela continue plus longtemps » Elle prit une forte inspiration avant de poursuivre sa diatribe « Hier, la fille de Frida, avec qui je travaille, s'est fait surprendre avec une boite à musique. Une dénonciation, bien sûr, n'empêche, la mère écope de trois jour-amendes et la fille passera le reste de l'année scolaire au Centre. Je ne veux pas que notre petite Jin se retrouve loin de nous parce que son père n'admet pas la politique de son gouvernement ! Je ne veux pas !» scanda-t-elle.

L'homme la regarda d'un air douloureux mais n'eut que le temps d'ouvrir la bouche, car avant qu'il ait pu proférer un mot, sa femme poursuivit :

« Ta mère est une bonne enseignante, je sais ! Ne me rechante pas ce couplet là ! Je ne discute pas la forme mais le fond ! Elle doit cesser ! TU DOIS CESSER ! Cria-t-elle étouffant un sanglot et elle se précipita dans le couloir à la suite de l'enfant.

L'homme tourna les talons, rappela l'ascenseur, et s'y engouffra sans se retourner.


« Toi, tu as passé un sale quart d'heure » résuma la vieille femme, en voyant débouler son fils un peu plus tard. « Le couvre-feu est actif depuis plusieurs minutes, tu es coincé ici jusqu'à demain matin mon garçon » Elle lui tendit une tasse de tisane dont l'arôme de menthe enchantait les papilles, même les plus récalcitrantes.

Il soupira, accepta la tasse et se laissa choir à terre, sur un gros coussin de laine, à coté de l'antique fauteuil.

Depuis son perchoir, la souris agita frénétiquement ses fines moustaches.

Mathilde tendit une miette de gâteau sec à l'animal, monnayant momentanément un peu de calme, et s'accroupit face à son fils.

« Ta femme à raison » asséna-t-elle sans concession.

François ouvrit la bouche mais une fois de plus, on lui vola la réplique.

« Mais toi aussi, tu as raison ! » affirma-t-elle en lui prenant la main. « Il ne faut pas tout perdre »

« Nous avons déjà tout perdu maman. Bientôt, la révolution chinoise sera à son apogée. Notre civilisation est en train de disparaître, notre histoire est oblitérée, nos traditions remplacées… Je ne crois pas qu'une simple cohorte d'enfants ressassant de vieux contes et autres simagrées, pardonne ma franchise, je ne crois pas que cela parviennent à entamer une contre-révolution efficace ! » Souffla-t-il dépité, en dégageant sa main. « Je n'arrive pas à comprendre où cela nous mènera, et pourtant, à chaque fois qu'elle revient de chez toi, Jin semble plus… »

« Vivante peut-être ? » Lui suggéra sa mère.

« Peut-être oui. Mais c'est un secret lourd à garder pour une petite fille, un jour ou l'autre un des gosses parlera, ou se fera remarquer pour une raison ou une autre, une remarque étrange, un commentaire déplacé, un savoir ancien, que sais-je, et il paiera le prix fort, et tous, nous… nous disparaîtrons ! »

« N'est-ce pas ce que nous sommes en train de faire ? » Reprit, les dents serrées, la vielle dame. « Muselés comme des chiens, avec pour toute liberté une longe trop courte et un sifflet pour rappel ! Réveilles-toi François ! Le passé, c'est l'avenir ! »

« Ton militantisme est complètement daté, maman. Je suis d'accord pour tenter d'inculquer à nos enfants une autre vision du monde, pas de les endoctriner d'une autre façon… »

« Je ne les endoctrine pas, je conte »

« Reste-en là, s'il te plaît.»

« Dois-je ne pas répondre aux enfants lorsqu’ils me posent une question ? C'est cela que tu me recommandes ? Être muette à leur soif de savoir ? Être aveugle à leur souffrance d'un monde gris ? Alors qu'il existe tout un tas d'autres couleurs ? »

« Maman ! » François se recroquevilla sur le coussin, serrant la tasse fumante autour de ses longs doigts fins.

Il ne rentra pas chez lui ce soir-là, et se rendit directement à son travail le lendemain matin, — l'agence des objets interdits —. Il était chargé, ainsi que de ses collègues, d'inventorier tous les articles séditieux de l'ère anté-révolutionnaire afin de les centraliser à Fort Nox. Dans cette place forte, véritable citadelle, était entreposé un exemplaire, un seul, de ce que les collecteurs rapportaient des sites progressivement détruits de l'ancien monde. Ce pouvait être n'importe quoi, du plus futile au plus précieux ; un simple CD, une reproduction d’œuvre d'art, une sculpture de maitre… Il voyait beaucoup d'objets passer entre ses mains dont certains n'avaient déjà plus de nom ou de sens pour lui. Leur utilité ou leur fonction lui échappait. Il se rendait alors au bureau des historiens, où sa compagne était employée, à l'étage au-dessus, et transmettait le lot qu'il n'identifiait pas. Il était répertorié. Les critères de sélection se révélaient obscurs et emplis de parti-pris, mais ce qui était certain, c'est que ces objets ne réapparaissait plus dans leur monde gris.
— ils disparaissaient —, leur ancien monde disparaissait.
François n'avait pas choisi ce travail, il lui avait été assigné. Il se devait de l'honorer au risque d'être accusé de sédition, de le perdre et se retrouver emprisonné quelque part, entre Irkousk et Pékin.

Mais à sa manière, une toute petite manière, humble et discrète (tout du moins l'espérait-il), il résistait. Sa famille résistait. Sa famille avait toujours résisté.

Sa mère, comme sa propre mère avant elle et la mère de sa mère également, habitait le dernier ancien quartier de cette ville d'où ils étaient originaires, un lieu anachronique, bien que le gris ne l'épargna point. On y trouvait encore de petites résidences individuelles, quelques squares aux antiques réverbères, aux allées de graviers et des arbres rabougris y subsistaient, leurs ramures baignant dans une atmosphère de plomb.

« Tous les Osmonde ont habité cette cité » racontait souvent Mathilde avec nostalgie tandis que les enfants, rassemblés autour d'elle, buvaient ses paroles en écarquillant leurs jeunes yeux. « A l'époque, les saisons rythmaient encore l'année, les maisons de briques rouges ruisselaient de soleil au printemps et l'hiver, une neige abondante bordait les toits et donnaient l'occasion de fameuses batailles entre les gamins du quartier »

Invariablement, Ch'ui questionnait :

« La neige, Dame, comment est-ce ? »

« Doux comme le plumage de l'oiseau, mais froid à piquer la chair de vos doigts telles cent aiguilles à l'ouvrage et pourtant chaleureux au creux de la main, semblable au baiser d'une mère et puis mou et ferme à la fois aussi, mystérieux, et sur la langue, pétillant autant qu'une bonne limonade… »

Et invariablement, Jin croisait les bras, têtue, s'adressant à sa grand-mère avec reproche ;

« Nous n'avons pas eu loisir de goûter ta limonade ! Je te rappelle »

Invariablement la vielle dame soupirait.

« Effectivement, je n'ai pas remis la main sur ces sachets de bicarbonate… pourtant j'étais sûre de les avoir rangé quelque part… »

Le grenier de la maisonnette recelait encore maints trésors, cependant la plupart tombaient en poussière faute d'être entretenus, réparés et utilisés. Mais à se servir de vieilleries, le danger était partout ; un jour, Mathilde avait déniché sous un tas de briques un sifflet à oiseaux. Fait de terre cuite, il suffisait de le remplir d'eau pour que l'air vibre de gazouillis d'oiseaux enchanteurs… Bien entendu, elle partagea sa découverte avec enfants et leur accorda une démonstration qui avait tourné court, hélas, lorsque l'alarme anti-bruit s'était déclenchée. Le lendemain, le concierge du quartier, chargé de la citoyenneté, un vrai furet, comme elle le surnommait en secret, s'était présenté afin de s'assurer qu'aucun trouble coupable n'agitait sa maison. Et encore avait-elle de la chance, le quartier n'était pas encore rattaché au grand projet de rénovation urbaine, de ce fait, pour le moment, aucun découvreur ne s'était donc présenté munis du fascicule 45002…

« Maudit papier ! » Maugréa-t-elle tout bas.

Quelques temps encore, elle eut ainsi loisir de conter Cendrillon, le petit Poucet, les six frères cygnes et autres contes de Grimm, toujours attentive aux questions de ses petits-enfants chéris.

« Raconte-moi une récréation, Dame ! » (ils l'avaient toujours appelé ainsi, car dans les nombreuses lectures qu'elle partageait avec eux, le mot revenait bien souvent et possédait le charme du vocabulaire défendu)

« Parle-nous encore de cet étang d'avant où l'on pouvait pêcher car l'eau était verte… »

« Bleu, Ch'ui ! Tu ne retiens rien, sermonnait Lin, concentrée sur une illustration, réalisée par Bilibine, de la fameuse cabane de Babayaga, la sorcière aux pattes de poule.

Dehors, le gris continuait de se répandre, il était dans l'air, il tombait du ciel, il s'accrochait aux passants, et tout autour de la petite maison, la grisaille s'infiltrait partout.

Parfois, Mathilde surprenait un regard vide d'un ou l'autre de ses jeunes auditeurs et s'alarmait de leur soudaine apathie.

*Qu'allait-il advenir ?* Se tourmentait-elle.

Un soir, plus désespéré et emplit de culpabilité qu'à son habitude, François prit l'ascenseur pour se rendre chez sa mère, emportant avec lui une bien mauvaise nouvelle que sa femme venait de lui annoncer. C'était un mercredi, les enfants étaient à leur « école subversive », comme la cataloguait Ama, qui, en tant qu'historienne, s'était rendu chez Mathilde de nombreuses fois au début de leur rencontre, afin de voir de ses yeux tous ces objets devenus illégaux et dont la possession entraînait une mise à l'écart de cette société nouvelle et terriblement normée à laquelle ils appartenaient depuis la chute de l'Europe. Les menaces de sanctions, très lourdes, avaient progressivement dissuadé Ama de se rendre au chevet de ce grenier d’abondance. Elle avait peur et la naissance de Jin n'avait rien arrangé. Quand Jin avait commencé à visiter sa grand-mère avec ses meilleurs amis, Ama n'avait rien soupçonné. François non plus, mais lorsqu'il avait su que Mathilde contait des histoires défendues, il s'était bien gardé d'en parler à Ama… Jin n'avait jamais trahit sa grand-mère, ce qui inquiétait d'ailleurs le plus la mère de famille. Sa fille était donc assez grande pour se rendre compte du danger qu'ils couraient tous. Et pourquoi ? Pour rien ! Tout espoir était perdu, bientôt, toute la ville, toutes les villes seraient nettoyées, il ne subsisterait plus rien du passé. Il faudrait aller de l'avant, et admettre une bonne fois pour toute que le gris était l'unique couleur !


Mathilde laissa couler ses larmes.
À travers les fenêtres à vantaux de bois, elle serrait le poing sur un mouchoir de dentelle de Calais, tournant le dos à François, qui poursuivait son soliloque funèbre.

« … l n'y aura plus de délai supplémentaire, d'ici quelques semaines, au plus, les découvreurs seront à ta porte. Avec Ama, nous pensons qu'il est nécessaire d'expliquer tout cela aux enfants sans tarder, et à leurs parents, Terry a le droit de savoir ce qui se passe, et Annabelle et Fjor également. Tu ne résisteras pas à l'enlèvement des objets, je t'en supplie,maman, et tout se passera bien, ensuite…

« Quelle suite ? Comment vivre dans un monde sans saveur, sans couleur ? » Murmura-t-elle en regardant par la vitre souillée d'une suie grise. Il bruinait dehors, la cendre tombait sans discontinuer depuis des jours… »

François ne sut que répondre. Le désespoir de sa mère trouvait écho dans le sien. L'école subversive, c'était le rayon de soleil de toutes les générations Osmonde ! Chacun des trois cousins, Fjor, Terry et lui-même et avant eux leur oncles et avant eux… Tous bercés par les contes, les objets et souvenirs d'un passé qui s'éloignait chaque matin un peu plus… Et pourquoi ? En cela, sa mère avait raison, rien ne leur rendait espoir, rien ne les incitait à espérer…

« Va t'en chez toi, François, Ama et Jin ont besoin de toi ! » commanda la vieille dame d'un ton qu'il ne lui connaissait guère, tandis qu'elle rassemblait les enfants pour le départ.

« Je…

« Tu vas rentrer chez toi, demain, c'est le jour de Noël. Regarde ! Elle effleura un antique calendrier perpétuel fait de plaques métalliques et de bois. Il indiquait : vendredi 24 décembre »

« Maman, les fêtes religieuses sont…

« Peu importe ce qu'elles sont, Noël, c'est Noël » .Elle ferma les yeux, refusant de laisser son chagrin la submerger.

Il n'osa la contrarier plus encore, et, en fils aimant qu'il était, lui baisa tendrement le front en appelant l'ascenseur.

« Je reviendrais demain. Nous passerons tous les trois » .Il n'y eut pas de réponse.

Il prit la main de sa fille dans la sienne. Il la serra fort, peut-être trop, Jin regarda tour à tour son père puis sa grand-mère, quêtant une explication qui ne vint pas.

Ils s'engouffrèrent dans la gueule grise d'acier.


Le nez dans des caissettes de plastique défraîchies, Mathilde fouillait et retournait fébrilement les objets qui les encombraient, repoussant certains, jetant d'autres par dessus bord.

Depuis le départ de son fils et des enfants, une idée n'avait cessé de lui trotter dans la tête. Peut-être une bonne idée… Une idée — subversive — ! Elle rit de cet aparté et se remit à explorer une vieille malle effondrée dans le coin le plus éloigné du grenier.

Elle s'était réfugiée là après le départ de son fils pour réfléchir, pleurer peut-être, et puis cette idée avait germé, grandi et c'est avec une certaine frénésie à présent qu'elle poursuivait ses recherches.

*Où avait-elle vu ce truc la dernière fois ?*

Elle rampa sous le bord du toit, près des pannes centenaires. Elle s'écorcha les genoux, n'y prêtant nullement attention car elle avait enfin repéré le fameux bidule dans une valise à roulettes délabrée. L'objet se présentait sous forme d'une boite ronde, en cuivre, contenant un cadran ainsi qu'une paire aiguilles,le tout serti et hermétiquement clos par un couvercle de verre.

La vieille dame soupira d'aise en ramenant ses jambes vers elle et s'assit plus confortablement, tapotant l'objet dont l'une des l'aiguille oscilla, hésita, puis repris sa position initiale. Mathilde perdit le fil du temps.

Longtemps ce… ce… machin, le nom ne lui revenait décidément pas, avait trôné dans le salon, au temps lointain où elle était une fillette affamée de lecture. Sa propre grand-mère la recevait en riant, un livre dans une main, ébouriffant de l'autre sa chevelure en bataille.

Elle caressa l'objet poussiéreux avec un bout de sa robe, et un instant, de nouveau elle eut 9 ans. Elle se remémora ce moment avec sa grand-mère, comme si le temps s'était raccourci, les années disparues.

Mathilde passait devant cette chose qui ressemblait un peu à une horloge, pendue à un clou, face à la porte d'entrée, alors qu'elle posait son imperméable sur le porte-manteau et Grand-mère frappait doucement sur le cadran, marmottant :

« Quel temps pour ces jours-ci voyons, voyons… »

Sur la pointe des pieds, la gamine observait l’aïeul placer l'aiguille repère sur la grande aiguille indiquant la pression atmosphérique.

Un baromètre anéroïde ! Mathilde rit. Bien sûr, un baromètre. Elle caressa le boîtier métallique.

Sur le couvercle, quelques mots d'une belle écriture noire, ourlée, étaient encore lisibles :

— Variable, Tempête, Beau, et en plus petit, quelques chiffres, — 1000, 980 —.

Déjà à cette époque lointaine, le temps détraqué n'offrait plus que rarement autre variété de saison que la grisaille et le brouillard moite. Mais Grand-mère faisait revivre les hivers houleux et les printemps de sa jeunesse, selon son humeur, tout en tarabustant l'aiguille repère. Elle disait,

« Admettons, admettons que nous indiquions 1030 hectopascals. Alors là, ma chérie ! En été, quand ça montait si haut, la chaleur ! La chaleur que nous avions ! Un ciel clair, si bleu ! Ma chérie, si bleu ! Tes yeux en ont à peine dérobé une goutte, tu aurais vu tout ce bleu ! L'azur qu'on l'appelait ».

Puis Mathilde commença à ressentir le froid, à travers sa fine robe à motifs où des larmes avaient coulé. Aussi, s'arma-t-elle de courage et entreprit-elle de redescendre du grenier à quatre pattes, car il n'y avait guère de place, traînant derrière elle le fameux baromètre.

Demain, songea-t-elle, tout en progressant prudemment, François et sa famille le trouveront à sa place d'antan, et je leur conterai une merveilleuse histoire.

Il était tard lorsqu'elle se remit de son expédition en sirotant une bonne tasse de tisane à la menthe, couvant le feu en s’appuyant sur la barre de l'antique cuisinière. Son dos lui brûlait tandis qu'elle observait incrédule, le baromètre au mur. Elle souffla sur le liquide, plissa des yeux et relut encore une fois ce qu'indiquait la grande aiguille, posa sa tasse sur un coin de table, et s'approcha pour vérifier, sa vue n'étant plus aussi bonne qu'auparavant. Elle tapota de nouveau le cadran. La grande aiguille frémit, oscilla, puis reprit sa position initiale, têtue. Instinctivement, elle marqua l'indication en tournant la molette de la petite aiguille repère.
« Incroyable » s'exclama la vieille dame prenant à témoin Marjorie, endormie sur l'accoudoir du fauteuil. Elle supposa que ce vieux baromètre était un peu comme elle, un peu cassé, un peu perdu…


L'aurore la trouva endormie au creux du généreux fauteuil, son châle remonté jusque sous son nez en guise de couverture. Soudain, l’ascenseur émit son ronronnement mécanique et elle sursauta car jamais personne ne venait la voir à cette heure-ci. La bouche grise béat, éructant une Jin survoltée, suivie de ses deux parents en pleine discussion, le sujet semblait heureux, le couple se tenait la main, souriant à demi. Jin sautilla devant sa grand-mère, lui saisit les mains et la tira en avant.

« Debout Dame, debout, viens vite voir ! » brailla-t-elle à son oreille bien qu'elle ne souffrit pas le moins du monde de surdité.

Mathilde, quelque peu hagarde, secoua la tête, son chignon se retrouva de guingois, l'enfant s'esclaffa, François croisa le regard de sa mère et lui sourit, l'aidant à se lever. Ama, d'ordinaire si effacée, le visage collé à la vitre, la bouche entrouverte, l'invitait du geste et de la parole ;

« Venez, venez voir, il se passe quelque chose de merveilleux ! »

« Il neige grand-mère ! Comme le prince, je vois la neige, et papa a dit que dès qu'on serait chez toi, on irait dehors faire une bataille de neige ! Grand-mère ! Vite ! Viens voir ! » l'encourageait l'enfant, les joues rosies d'excitation.

Mathilde branla du chef, tant pis pour le chignon qui termina sa dégringolade. Elle rêvait certainement, se dit-elle. Elle rêvait forcément, il ne neigeait plus depuis… epuis…

Enfin, elle fut sur ses jambes, s'approcha à petits pas de l'entrée et jeta de biais un œil sur le baromètre dont la grande aiguille de bronze n'avait pas bougé depuis la veille et pointait fièrement le mot « Neige ».

« Maman, regarde, il y a des enfants dehors ! Venez ! On va les rejoindre ! »

La porte d'entrée s'ouvrit à la volée, Jin se rua dehors, et dehors, tout était blanc, éblouissant, oh bien sûr, ce n'était pas une couleur, ce blanc, tout ce blanc, c'était tellement mieux, c'était tout. C'était l'espoir, se réjouit Mathilde, se précipitant à la suite de sa petite-fille célébrer ce miracle de Noël.
