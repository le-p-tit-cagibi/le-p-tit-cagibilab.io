---
title: Le canal
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Une partie de pêche bien peu ordinaire pour Margot et son maitre, l'ancien éclusier. Mais les vieux réflexes ne se perdent jamais non ?"
epub: true
pdf: true
draft: true
---
— C'était un matin de printemps un peu brumeux, au bord de l'eau, il y a toujours plus ou moins d'humidité, alors ça n'avait rien d'anormal, pas plus ce jour là qu'un autre, même si, au décours du récit que je m’apprête à vous faire, tout deviendra suspect, bizarre, étrange glauque ou paranormal, surtout si vous êtes branchés X-Files ou Strangers things, comme mes petits-enfants. Mais au départ, ce n'était qu'un début de journée très ordinaire. J'avais avalé deux œufs pondus frais par mes vaillantes cocottes Tanguy et Laverdure, enfilé bottes, jeans et maillot à l'effigie de Batman, mon héros, le seul vrai héros d’ailleurs n'en déplaisent à beaucoup. Bref, pas un brin de vent, dans les quinze degrés, une météo idéale pour jeter quelques lignes dans le canal qui borde ma maison, une maison d'éclusier, large et basse, rouge de briques et de tuiles.
Les berges aux alentours sont un peu à l'abandon, il n'y passe plus guère de péniche depuis une vingtaine d'années, parfois quelques bateaux de plaisance, conduit par des fous furieux ne sachant pas naviguer et bousillant un peu plus les ciments déjà passablement émiettés de l'écluse un peu plus bas, en amont. Ma mère avait coutume de dire « La vie c’est comme une boîte de chocolats, on ne sait jamais sur quoi on va tomber » et vrai que, ce matin là, rien ne prédisposait à l'aventure qui pourtant arriva sous mes yeux, et dont une partie, non la moindre, me fut contée par le protagoniste lui-même. Margot, mon vieux bull terrier se dandinant dans mes pattes, je scrute la rive, misant sur un petit recoin d'ombre comme manne à poissons. Bon, elles sont devenues rares, ces bestioles depuis un moment, avec ce que crache en aval la cimenterie dernier cri. Ajoutez-y les poisons ordinaires des villages devenus dortoirs et des paysans parvenus chefs d'entreprises où le diktat - toujours plus - renvoie à l'overdose. Parlez-moi de rendement, je vous répondrais mon pied au cul.La flotte en a même viré de couleur ; de mon jeune temps, sans être clairet, le canal offrait un vert émeraude au travers duquel le fond, vaseux et marronnasse, s'entrapercevait aux heures chaudes de la journée ou encore l'hiver, lorsque le froid, armé d'un soleil éclatant à s'en péter les pupilles, rampait sur la surface immobile À présent, on y voit goutt ! Des eaux grises et des algues règnent sur les profondeurs.
Bref, après avoir repéré mon paradis du jour, je remonte chercher mes ustensiles composés de deux vieilles cannes bambous, un seau, un peu d'appât maison, bricolé la veille (quelques gouttes de pastis, c'est le secret d'un bon appât) et une nasse rafistolée, que je tiens de mon paternel et qui me gardera mes prises au frais la matinée. Margot sautille lamentablement, vrai, elle prend un âge presque aussi indécent que sa panse qui traîne par terre, mais son regard jovial me tient lieu de bonne compagnie, à défaut de celle des hommes. Bon, me voilà prêt et comme je m’avance tranquillement, au loin, quittant le pont, j'aperçois un cycliste qui s'engage sur la piste qui descend le long du canal.
Il zigzague un peu trop, le zig, pensai-je, haussant les épaules. Voyez, tous ces beaux chemins de halage bien droits, que les anciens utilisaient pour tracter les péniches, ils en ont fait des pistes cyclables !
Ma grand-mère, fille de charretier, me racontait les dures journées de labeur de son père et de son grand-père avant lui ; vingt heures pendant la saison des barges, à guider les chevaux, de bons gros chevaux de traits, aux pattes solides, aux croupes larges et vaillantes, habitués à tirer et tirer encore les monstres de bois, pleins jusqu’à la gueule, qui remontaient vers Verneuil. Et puis le progrès a bazardé tout ça, les chevaux et le reste, la vapeur, le fuel sont venus à bout des plus braves canassons. Le monde se délite, je vous l'affirme, et c'est pas juste pour en parler, non, je le vois du haut de mes un mètre quatre-vingt et mes soixante-dix-huit ans.

Enfin, alors voilà le barda installé au bord de la rive. Je me cale sur mon tabouret trois pattes, parce que je fatigue, debout, c'est comme ça. Le temps vous grignote tout, le ciboulot et les os. Je m'apprête à lancer, avec au bout de l'hameçon, un beau vers de terre, un bien gras, de mon potager, que je fume chaque automne. Grassouillet, je vous dis.
Mon regard s'égare de nouveau sur le chemin, le cycliste décidément n'est pas la réplique d'une flèche wallonne : Il prend à fond de train le sillon en bordure de rive, accrochant quelques herbes folâtres. Sa main se lève, mouline l'air devant lui,comme s'il chassait un insecte impportun. Il s'agite sur son bicloune, juste en face de moi, de l'autre côté du canal. Je peux voir son visage rubescent et mafflu, ses cheveux carottes, son short noir collant que c'est pas dieu possible de porter des frusques pareilles, et un maillot assorti, plaqué contre son torse à cause de l'accélération depuis qu'il a négocié le virage. Il va vite le bougre, trop vite… 
La bicyclette s’emballe au trois quart de la descente, il faut dire, pour qui n'a pas l'usage de ces chemins, ça peut être traître, on file là-dessus à toute berzingue et c'est le casse-gueule assuré : J'en ai vécu, gosse, des croûtes aux genoux à pleurer, garant ! Enfin voilà, malgré moi, je retiens mon souffle, Margot, par contrariété féminine, se met à aboyer de toute sa voix, sentant que ce gus ne se comporte pas de façon normale, houlà, voici qu'à présent il détend ses pieds pour tenter de freiner, le zèbre…

Et nous assistons, spectateurs médusés, au plus beau vol plané de cette dernière décennie, peut-être plus, quand j'y songe, avant qu'un « PLOUF ! » magistral me ramène à la réalité. Le gars est tombé à baille, et voire s'il sait nager, je ne saurai dire.

Je ne réfléchis pas plus, me faut ma gaffe, et en vitesse encore.
Mais mes guibolles vont moins vite que mon cerveau, je pistonne sec dans la remontée jusqu'à la baraque.

Où est ce que j'ai fichu c'te maudite gaffe ?


L'air est doux, idéal pour un tour à vélo. Christine ne me lâche pas en ce moment, « fais-ci, fais-ça, patati et patata ».
Alors pour une fois qu'on fait une virée jusqu'à Verneuil, la tentation a été sèvère. Finalement, j'ai sauté sur mon vélo à la fraîche, ni vu ni connu, Christine est encore dans les limbes. Je vais me faire quarante bornes pépère, et pourquoi pas cinquante ? 
Je souris béatement, liberté ; une idée en l'air qu'à dû lancer un idiot ou un fou un jour de marché ! 
J'ai enfilé vite fait un vieux short cycliste et le maillot assorti, échoués dans la maison familiale depuis notre dernier séjour, l'an dernier peut-être. Je respire un peu trop vite, mon cœur s'emballe comme un gamin qui va faire une connerie en cachette. Le plus difficile va être d'ouvrir le garage sans que grince la porte coulissante.

Au reste, j'ai de la chance, les dieux du vélo sont avec moi, pas de grincement, je saisis la bécane, qui date des années quatre-vingt, mon père l'avait trouvée sur une brocante : quarante francs, je m'en souviens. Le pédalier est un vieux monsieur récalcitrant, mais une fois lancé, l'engin file comme le vent. Trois plateaux, dix huit vitesses, de quoi se faire plaisir sans trop souffrir.

KLONG !

J'ai oublié que la porte claque en redescendant.

— Ramène les croissants Jacques ! Me crie un épouvantail en nuisette rose crevette par la fenêtre du premier. Je ne réponds pas, trop impatient de démarrer l'aventure et mon premier coup de pédale est à la fois rageur et libérateur. Je traverse les rues du bourg encore désert à cette heure matinale, les cheveux au vent, la fraîcheur de l'air me fait frissonner, j'avais oublié ce bonheur là. J'appuie sur le guidon, les premiers kilomètres vont être rudes, ça fait trop longtemps que je reste le cul vissé derrière un bureau de bois usé, un peu comme moi. Mes mollets me rappellent que je n'ai plus 20 ans, mais pas encore 50, laissons-nous le temps d'apprivoiser la machine et ce corps amolli.

Le paysage n'est pas très varié passé les dernières maisons ; ici, la campagne est plate, faite d'une succession de champs et… de champs. Je ne cogite pas long et décide de prendre, après la prochaine bifurcation, la petite départementale qui rejoint le chemin de halage. J'y serai à l'ombre et de ce que je me souviens, il n'y circule pas grand monde. Je me prête à penser que je n'ai vérifié ni les pneus ni les freins avant mon départ précipité. Je vais parier sur de la chance ? Je tâtonne le cadre, il y a une pompe à vélo accrochée ainsi qu'une petite trousse de réparation derrière la selle.

Je souris béatement. Plus fort que moi. La route lisse, le cycle glisse, les coucous se lancent quelques appels timides, rien d'autre. J'aperçois enfin l'intersection avec la piste cyclable et parallèlement, brillant d'une eau bien sombre, le canal des Promis. Il serpente de Verneuil à Courtines, ça me revient. J'appuie franchement sur les pédales et prend une belle allure À l’abord du croisement, grisé, je tourne sans freiner et l'engin s'écarte un peu, projetant des gravillons, protestant contre mon imprudence. C'est bon d'être seul, c'est bon de respirer, au petit matin, rien que soi, les arbres qui défilent, le canal qui scintille.
Et puis soudain l'aventure tourne au cauchemar.

Une douleur fulgure mon flanc gauche. J'éructe un hoquet ridicule. Le vélo file prenant de la vitesse dans la descente, alors que la douleur me coupe définitivement la respiration. Je porte un bras à mon cou, ma carotide ressemble à un fleuve en furie, des étincelles papillonent devant mes yeux : Rien ne va plus. Conscient que mon corps me lâche, de mes deux grands pieds pesant soudainement un poids terrible, je lache les pédales et tente de freiner. Mais la bicyclette s'est emballée, je ne maîtrise plus rien, mon cœur s'est mis à battre comme une grosse caisse de tambour et mon bras gauche ne répond plus. Pour couronner le tout, je m'approche dangereusement du bord. La berge ! Bon sang, je vais franchir la berge.

J'ouvre la bouche mais le cri est repoussé à l’intérieur de ma gorge par une eau vaseuse. J'étouffe, incapable de libérer mes jambes du vélo qui a chuté avec moi dans le canal.
Bordel ! Je pousse, je tire avec mon seul bras valide, je tente de me dégager, mes jambes sont devenues deux chiffes qui m'emportent vers le fond, les pieds verrouillés à la chaîne de vélo. Alors que je lève la tête, des bulles accompagnent mon geste. Je prend avec horreur toute la mesure de la situation : c'est ma vie qui s'enfuit avec ces petites bulles d'air, alors je gesticule de plus belle monopolisant mes efforts avec ce bras qui me répond encore. Je vois la lumière au-dessus de moi, le soleil, la vie… mais je ne parviens pas à m'extraire de mon vélo transformé en leste mortel.
Je ferme les yeux.
Il se passe mille ans - dix secondes à peine en vérité - En face de moi, un visage.
Je hurle.
J'ai oublié que je suis sous l'eau, que je me noie et j'avale encore une copieuse goulée d'eau fangeuse. Je suffoque, les yeux exorbités devant la jeune fille qui flotte là juste devant moi.
Et quelle jeune fille !
Il me vient à l'esprit que je suis peut-être déjà mort pour qu'une telle chose soit possible.
Elle, elle ne semble pas inquiète du tout : elle me sourit. Elle a une quinzaine d'année peut-être, ses vêtements sont en lambeaux et couverts de limon. Pire que cela, c'est son visage qui me terrifie. Ses cheveux tournoient autour de son corps tels de vivantes longues algues brunes. Mais son visage ! Qu'est-il arrivé à son visage ? Ses yeux, blancs, crevés, me fixent sans paupière, tandis que sa bouche, aux lèvres charnues semblables à deux répugnantes limaces orangées, ne cessent de murmurer…

— Viens danser avec moi promeneur, viens… viens… Laisse- toi couler, laisse-toi couler… 

Sa voix sans timbre est comme désincarnée. Je la détaille tandis que mon oxygène s'amenuise avec un sentiment partagé d'effroi et de dégoût.
Sur son front, de petits crustacés ont élu domicile, creusant des cratères dans sa peau juvénile et blafarde. Ses oreilles ont disparu, remplacées par deux larges moules de rivière, qui entrebâillent de façon des plus obscènes leur face nacrée, et laissent apparaître une substance rosâtre similaire à une immonde langue frétillante. Je voudrai pleurer, cette frousse que j'ai : cette fille… n'a rien d'une fille en fait. Je l'entend qui m'appelle encore.

— Viens danser avec moi promeneur, viens… viens… Laisse- toi couler, laisse-toi couler… 

Alors que je crois ma cause perdue, je sens le fond boueux du canal sous moi et mes dernières réserves d'air s'échappent en gargouillis funestes. Le vélo s'échoue, libérant brutalement mes pieds mais étragement, c'est mon épaule qui souffre, je prends des coups, sans distinguer mon agresseur. Je n'en peux plus. L'épuisement m'invite à l'abandon. Posés sur le fond du canal, des crânes laiteux aux orbites habités de crevettes et de larves se tournent vers moi et m'invitent à leur tour :

— Viens, promeneur, viens avec nous, laisse-toi couler… viens… 

Mon cœur griffe mes côtes espérant sortir peut-être, de sa cage thoracique, mais je sais que je meure à présent. Tout ça pour un tour de vélo ! Putain de Christine qui me prend la tête, aussi…
La fille-monstre-poisson m'enserre de ses bras putréfiés et pose sur mes lèvres un baiser froid et gluant. Je respire une dernière fois - haleine - aux relents de terre mouillée et d'herbes décomposées.
Je voudrais tant hurler.


Et ma gaffe a saisi le type. D'accord, d'accord, j'ai eu bien du mal, surtout qu'il m'a fallut contourner le pont, de là où je me tenais pas moyen de le chopper, il était bien trop loin ! Margot a failli me faire casser ma pipe dans le virage, toujours dans mes pattes, celle-là mais j'ai poussé sur mes vieux genoux, le mal que ça m'a fait, foutre ! Une fois de l'autre côté, j'ai dévalé le chemin de halage comme un jeunot et plongé ma pique une, deux, trois fois. Il coulait déjà, le type, et puis j'y voyais goutte, il piquait droit au fond.

Alléluia ! A la dernière tentative, j'ai crocheté le maillot et tiré comme un cheval : j'aurai pas dépareillé sur les chemins de halage, au temps de mon vieux papy, en bourrin valeureux, je vous le dis. Tout d'abord, il ne s'est rien passé, comme si le gars résistait ! J'ai fais levier sur le rebord bétonné avec la gaffe, pesant de tous mes ans, il s'est produit une secousse puis le corps du cycliste est enfin apparu. Il saignait, parce que mon outil, il a un bon crochet à l’extrémité, mais c'est aussi ce qui m'a permis de le repêcher.

À ce moment, une voiture passe sur le pont et deux jeunes gens viennent me donner un sérieux coup de main pour sortir le gars de l'eau. Il est salement amoché. Pâle comme la mort, les gamins constatent d'ailleurs judicieusement :

« Il est mort, vite faut masser ! »

Et voilà que bientôt notre apprenti noyé crache et fait des bulles d'eau avec son nez, ravigoté. Margot commence à lui laver la figure, ce qui achève de le remettre d'aplomb, je pense. Il reprend une couleur plus naturelle et commence à nous déballer cette histoire, son histoire, entre deux larmes, ah, ça, oui, il pleure le pauvre ; ma Margot a beau lui torcher le portrait, ça coule comme fontaine. Ses pieds veufs de toutes chaussures pissent le sang, entamés par la chaîne du vélo probablement. Les automobilistes secouristes munis d'un téléphone joignent les pompiers de Verneuil. Toutes sirènes hurlantes (pour une fois qu'ils en ont l'occasion, ces zouaves) ils ont bien vite débarqué, et embarqué notre rescapé.

À cet instant, avec Margot, on s'aperçoit que notre ligne de gauche a une touche, et ni une ni deux, on refait le tour par le pont aussi rapidement que mes pauvres guibolles peuvent encore me porter mais, bon, ce coup-ci, on est arrivé trop tard : plus de touche, et plus de canne non plus ! Encore un coup de la petite fée du canal, comme aurait dit ma grand-mère, rapport à une pauvre âme qui s'était noyée là quand elle-même était toute gosse et comme tant d'autres avant elle : l'époque était dure, pas comme maintenant que tout un chacun va à la tirette quémander un billet pour pousser l'autre.
La fée du canal … fichtre ! Qui voudrait bien loger dans une eau saumâtre comme celle là, je vous le demande ? Avec les saletés de la cimenterie pour tout déjeuner, hei ? Des contes de vieilles bonnes femmes, pas de doute là-dessus. Ma grand mère avait tout d'une superstitieuse.

Allez, m'sieurs dames, assez causé, je vous ai exposé l'affaire, comme réclamé, mais faut que je vous laisse, moi, hein, la causette, ça remplit pas l'épuisette, bien que ça fasse passer le temps, c'est sûr, bien que du temps, à tout réfléchir, j'en ai plus guère.

— Margot ! Ici ! Margo ! 


Et le vieux s'éloigne d'un pas tranquille, longeant la rive du canal, un bull terrier des plus laids se dandinant gauchement à son côté.
