---
title: Télépathie de supermarché
author: Sophie Belotti
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Nina n'a plus peur que le ciel lui tombe sur la tête : c'est déjà fait ! "
epub: true
pdf: true
draft: true
---
Nina tambourinait sur sa caisse enregistreuse dans un fracas d'apocalypse. La pluie, annoncée depuis la veille après une sécheresse de plusieurs mois, se déversait avec une violence inouïe. Elle heurtait les vitrines de la devanture du magasin, dégoulinant comme un torrent de montagne en crue. Le vent griffait les stores en aluminium les faisant gémir d'horrible façon et, au-dessus de sa tête, sur les toits de tôles galvanisées, la jeune femme avait l’impression que dieu ou un de ses nombreux comparses jetait sans discontinuer avec force et malice des tombereaux d'eau. Brusquement le ciel se zébra d'éclairs trouant le couvercle de plomb qui enveloppait la petite ville depuis le matin suivies d'un magistral coup de tonnerre qui ébranla toute la structure d'acier du supermarché. Les quelques clients qui se bousculaient près des portes, espérant encore rallier leur voiture, poussèrent un murmure d’effroi, et se rencognèrent à l'abri, troupeau craintif surpris par l'orage.

Les marchandises continuaient de défiler sur le tapis roulant, et il semblait à Nina que c'était là une rivière charriant des débris, dont le flux grossissait sous les flots incessants du ciel. Ses doigts se crispèrent involontairement au coup de foudre suivant, et le bâtiment fut plongé dans l'ombre. Quelques cris résonnèrent, le chuintement maladif d'un groupe électrogène se fit entendre et l'on sentit dans l'air comme un vrai soupir de soulagement, chacun regardant l'autre avec, dans le regard, ce « ah vous voyez, tout est prévu, tout va bien…».

Pourtant à la caisse trois, quelque chose n'allait pas bien. Pas bien du tout.

Un jeune type d'une trentaine d'années avait sauté par dessus le tapis et semblait s'en prendre à la caissière À vrai dire, affalée par terre, la mine aussi grise et sèche qu'un hareng abandonné sur une plage en plein midi, les yeux révulsés, elle n'avait pas l'air au mieux de sa forme. Un attroupement ne tarda pas à se former tandis que les premiers commentaires, inaudibles, se répandaient au-dessus du couple. Le mobilier escamotait la vue, et beaucoup poussaient sur leurs pointes de pied, agrippant leur voisin d'un « qu'est-ce qui se passe ? » angoissé.

Alan, lui, n’entendait et ne voyait rien. Il savait juste que pour une fois, son brevet de secourisme passé l'an dernier allait lui être utile.

Une minute plus tôt, il rêvassait sur la poitrine généreuse de la caissière engoncée dans son tee-shirt vantant les mérites d'une célèbre marque de cosmétiques, quand il avait vu le flash lumineux descendre du plafond et fondre sur la fille : un fil arachnéen, bleuté, qui lui avait directement transpercé le crâne au niveau du lobe pariétal gauche. Ça n'avait duré qu'une fraction de seconde, probable que personne d'autre que lui n'est rien vu ; trop absorbé par le déluge dehors et la brusque coupure de courant ensuite. Il avait vu la fille se chiffonner comme un vieux papier, et dégringoler par terre.

Il ne parvint pas à déceler un pouls, ni une respiration, et entama sans attendre un massage cardiaque. Il s'estima chanceux, la victime aussi d'ailleurs, car moins de cinq minutes plus tard, elle ouvrait les yeux, ahurie, mais le teint légèrement rosé, et pudiquement, il réajusta son tee-shirt sur son torse frissonnant.

Elle s'assit maladroitement, et tint sa tête entre ses mains sans un mots.

Alan risqua un prudent :

— Ca va ?

Auquel la caissière répondit par un vague hochement de menton.

Une sirène retentit et les secours firent leur entrée à grand renfort de « poussez-vous ! » « laissez passez ! » projetant devant eux des sacs de matériels rebondis. Un trentenaire arborant une tignasse d'un roux flamboyant, au visage en lame de couteau, chaussé de petites lunettes rondes s'approcha avec autorité. Les badauds s'écartèrent avec effort.

Le doc Clemens posa sa mallette à ses pieds et salua Alan. Celui-ci rendit compte des soins qu'il avait pratiqué en urgence sur la jeune femme, suite à l'électrocution.

Il écouta puis appela les brancardiers.

La fille protesta mollement lorsqu'il fut question de la transporter à l’hôpital.

Puis tout fut fini. L'orage aussi.

Les clients de la caisse 3 récupérèrent leurs marchandises et se dirigèrent vers les autres caisses, pressés de rentrer chez eux raconter l'accident au supermarché !

Alan n'en eut pas le courage, il laissa ses courses en plan. Il sentait l’adrénaline refluer lentement de son corps ; le massage intensif qu'il avait pratiqué quelques instants plus tôt l'avait épuisé. Il franchit lourdement les portes automatiques, et l'air frais lui fit du bien. Il décida de rentrer à pied. Rien ne le pressait.

— Je souhaiterai effectuer une batterie de tests, si ça ne vous dérange pas.

— Si justement, ça me dérange doc, je veux juste rentrer chez moi. Je vais bien.

Nina était assise sur brancard dans une des salles de consultation des urgences. Ses yeux noisette s'ourlaient de cernes noires, ses traits étaient tirés, mais elle n'avait pas l'air mal en point pour autant, et sa détermination fleurait la confrontation imminente avec l'autorité médicale.

— Ce ne serait pas raisonnable Mlle Gardener, restez au moins cette nuit en observation…

À cet instant seulement, la jeune femme songea à son petit copain.

— Jules va venir me chercher, il me surveillera…

— Il n'est pas médecin ! Objecta le doc.

De toute façon, techniquement parlant, ce n'est pas de votre fait si je suis vivante, c'est ce gars, le type qui travaille à la bibliothèque, qui m'a réanimé, alors…

— Alors raison de plus pour garder un œil sur vous. Une électrocution, c'est un accident très grave, qui peut laisser des séquelles Avez-vous seulement remarqué que vous avez une brûlure sur le crâne, au point d'entrée de la foudre ? Votre cuir chevelu est à vif sur un bon centimètre carré…

Le doc s'emballait, il voulait convaincre. Il fixait la jeune patiente à travers ses verres épais. Celle-ci pris un air buté, secoua à plusieurs reprises sa chevelure brune, épaisse comme une crinière de quelque cheval de Mongolie, puis se tint la tête, les deux mains serrées sur ses oreilles. Au bout d'un court et pesant silence, elle prononça, criant presque,

— C'est non !

Clemens ressentit physiquement le poids de ce refus comme s'il recevait sur ses épaules un sac de ciment de 50kg. Il se tassa malgré lui. Rien ne lui vint à l'esprit pour étayer sa demande d'observation de 24 heures. Il n'arrivait pas à réfléchir. La fille gardait ses paumes sur ses oreilles, sans le regarder. La scène ne dura pas plus d'une poignée de secondes, pourtant le doc eut l’impression, rétroactivement, d'avoir effectué une joute oratoire interminable, d'avoir lutté, de s'être débattu et, en définitive, d'avoir perdu. Il la regarda se lever, un peu désemparé, mais sans faire un geste pour la retenir.

Nina s'obligea à respirer posément et cette clameur qui, un instant plus tôt, lui vrillaient les tympans diffusa dans son corps et s'y perdit. Elle ferma les yeux devinant les allers et venues dans le couloir, se concentra sur ces bruits anodins et le calme se fit en elle presque instantanément.

Le doc Clemens n'ajouta rien.

Elle fit un pas prudent hors du lit et constatant qu'il n'ébauchait aucun geste s'y opposant, enfila son vieux gilet angora posé sur une chaise ainsi que ses ballerines rose à pois noirs puis se saisit de son sac à mains en jeans élimé.

— Je vous promets de vous donnez des nouvelles, ajouta-t-elle plus doucement.

Et comme le médecin ne pipait mot, le dos voûté, sur sa chaise, elle sortit sans plus de façon.

Elle s'accorda une journée de congé après son aventure au supermarché. Le patron, un vieil homme tout en rondeur qui aurait pu être son grand-père, surnommé Gradie, n'était pas une peau de vache, et puis, il lui ferait rattraper ses heures, elle n'en doutait pas. De toute façon, elle n'avait pas vraiment les moyens de rester dans son studio à ne rien faire. Les factures tombaient tous les mois. Jules était au chômage, vivant de petits boulots dégotés au jour le jour qui payaient mal. Quand il en trouvait - ou plutôt quand il en cherchait -

Ce matin là, Nina se rendit compte qu'elle posait sur celui que beaucoup dans leur entourage nommait son fiancé, un regard sans concession. Mais après tout, depuis deux ans qu'ils étaient ensemble, qu'avaient-ils réalisé ? Absolument rien. Ils végétaient à deux au lieu d'être chacun dans leur coin. Il ne lui apportait rien, ses assauts maladroits et répétés l'importunaient maintenant plus souvent qu'ils ne la flattaient, et certainement qu'elle ne lui apportait pas grand-chose non plus. Une fois le nez sortit de sa caisse enregistreuse, Nina se plongeait dans les romans d'aventures. Des romans romanesques. Elle détestait la télévision alors que Jules y gaspillait le plus clair de son temps, elle aimait cuisiner, il dévorait des saloperies à longueurs de journée, il aimait sortir voir les matches le dimanche, elle ne supportait pas ses copains aux attitudes puériles, se chamaillant partout où ils se réunissait.

Elle s'étira, tâtonna pour trouver la plaie, perdue dans sa chevelure emmêlée ; ça ne lui faisait même pas mal. Elle s'aperçut qu'elle avait dormi comme une pierre depuis son accident de la veille et en se levant, toutes ses pensées nouvelles qui lui venaient, claires, intransigeantes l'interrogèrent. Se dissimulait-elle tout ça depuis longtemps ?

Jules n'était pas là, elle avait essayé de le joindre à l’hôpital déjà, mais il était parti faire une virée chez son pote Martin, dans la ville à coté. Elle avait reçu un SMS laconique. « Une virée » c'est-à-dire au mieux une soirée de biture et au pire il l'a tromperait encore, ou irait commettre un petit vol ou une connerie de ce genre… Martin avait déjà fait un peu de tôle, elle ne savait pas pourquoi. Jules pas encore. Elle s'étonna de nouveau de son amertume puis résolue de chasser toutes ces pensées parasites qui allaient lui gâcher son unique journée de repos.

La bibliothèque était une bâtisse d'un autre temps, la porte tambour qui en permettait l'accès, à elle seule, transportait dans une autre dimension. Une fois à l'intérieur, Nina leva la tête, bien qu'elle sut parfaitement ce qu'elle allait trouver sur le plafond du hall. Une peinture monumentale, copie du célèbre tableau de Couarraze « La colonne du savoir ». Sous cette perspective, l’œuvre sembler vouloir avaler le visiteur, qui pour un peu aurait mit ses deux mains sur sa tête pour se protéger d'une avalanche de bouquins. Nina s'engagea sur un sol en damiers couleur ivoire et passe-velours du plus belle effet. Elle se dirigea de suite à petites pas dans la première salle, à sa gauche, le hall ne servait qu'à intimider les plus hésitants et à recueillir les vieux parapluies dans une vasque disposée près de la porte. Dès le seuil, des livres s'affichaient partout, du sol au plafond dans de profondes étagères de bois clair. De chaque coté de la la vaste pièce, plusieurs tables de tailles respectables et chaises confortables bien que surannée, à l'assise de cuir craquelée invitait à passer un moment en ces lieux. Un calme tranquille régnait. De hautes fenêtres laissaient passer la lumière, éclairant jusqu'au fond de la pièce. Une estrade y surplombait la salle, et, assis sur un tabouret à vis, l'assistant bibliothécaire, penché studieusement sur son bureau, un massif meuble style empire, avec tiroirs en guise de pieds et sous-main de verre dépoli couvrant en toute la surface. L'homme paraissait presque minuscule derrière cette splendide antiquité. Il leva à peine la tête à l'arrivée de la jeune femme, puis comme elle s'avançait, et qu'il la reconnaissait, il ajusta ses lunettes, posa son crayon mine et se leva pour la recevoir. Elle ne savait pas vraiment quoi lui dire, un merci paraissait totalement insuffisant, mais elle n'était pas très démonstrative de nature. Elle dansa d'un pied sur l'autre tandis qu'il lui serait la main en s’enquérant de sa santé. Elle opta pour être le plus naturelle possible. Tant pis s'il s'attendait à une reconnaissance plus marquée. Mais il n'en s'en offusqua pas, tira sur le veston de son costume, un velours pied de poule un peu passé, et l'invita à s'asseoir dans un fauteuil tout proche. Elle posa ses livres sur la table d'acajou, un prétexte s'aperçut-t-elle, pour remercier son bienfaiteur.

En sortant, quelques minutes plus tard, elle avisa ses mains vides, et fut surprise de ne pas se souvenir d'une seule phrase échangée. Un brouillard épais enveloppait ses souvenirs ainsi que ses pensées.

Alan la regarda s'éloigner. Un beau petit lot. Il l'avait déjà croisé ici avant son accident. Elle avait l'air remise de sa mésaventure, mais bon sang, quel regard étrange - Glaçant – Il frissonna, ressentant encore ce froid à la fois incommodant et subtil émanant de la jeune femme tandis qu'elle portait sans cesse ses mains à ses oreilles comme si quelque bruit l'importunait. Il se recroquevilla malgré lui. Il n'arrivait pas à réfléchir, tant et si bien à vrai dire qu'il n'avait même pas songé à lui offrir un petit café, ou une pâtisserie, au salon de thé juste en face de son lieu de travail. Pas une fille pour toi. Pour une fois, il se rallia sagement à sa petite voix, non, vraiment pas une file pour lui, mais il était content de lui avoir sauvé la vie.

Elle sirotait un thé à la cardamone quand la moustiquaire de la porte grinça doucement. Il était tard, et Jules ne s'était pas manifesté depuis son SMS.

Un magazine sur les genoux, enveloppée dans un plaid de laine, Nina guetta l'arrivée de son compagnon. Une colère sourde montait en elle à une vitesse faramineuse. Elle rassembla sa longue chevelure et la fit tomber sur un seul côté, lissant les mèches rebelles. Un homme d'une trentaine d'années, des yeux gris pâle, crâne ras, fine barbe blonde masquant son expression, blouson sportif et jeans, passa la tête dans l'encadrement de la porte du salon.

— Hello !

Comme Nina ouvrait la bouche, il fit un geste de la main, anticipant ses reproches.

— Écoute, on a fait un poker, tu sais ce que c'est.

— Non, justement.

Sa répartie claqua comme une porte après une scène de ménage. Mais l'embrouille n'avait pas encore eut lieu ; Pas la peine se dit Nina, qu'il se casse ce naze, c'est tout. Qu'il… s'évapore comme flaque d'eau au soleil ! Pfuiittt. Dégage.

L'homme grimaça, indécis, toujours sur la pas de la porte.

Il ressentit physiquement cette poussée de dégoût qui s’affichait de façon sibylline sur le visage de sa compagne et se recoquilla malgré lui. Il eut du mal à rassembler ses esprits pour plaider sa cause qu'il sentait perdue d'avance. Nina jouait avec sa tignasse, penchait la tête, ostensiblement fâchée, il le devinait à ce regard lointain et fixe. La scène ne dura pas plus d'un instant, pourtant Jules se sentit épuisé, d'un coup. Elle se boucha les oreilles lorsqu'il entreprit de s'approcher dans une tentative de réconciliation. En général, il s'en sortait bien pourtant non ? 

La porte claqua brutalement, ce qui fit sursauter la jeune femme, toujours sur le canapé. Le magazine avait glissé à terre, la pièce avait l'air chamboulée, pourtant seul le silence lui tenait à présent compagnie. Elle courut jusqu'à la baie vitrée qui menait au portail, sur le devant de leur petite maison, et ne vit que l'ombre de jules, un gros sac de sport à la main, s'éloignant rapidement dans l'allée… Elle entendit la voiture démarrer sur les chapeaux de roues, un éclair bleu deviné au travers la haie, et plus rien. Elle écarquilla les yeux, réalisant sans comprendre vraiment que Jules venait probablement de disparaître de sa vie. Pour toujours.
 
— Bon débarras.

Elle accusa la violence de son propos, car elle avait prononcé ces mots à haute voix. Une voix melliflue qu'elle ne reconnut pas pour sienne.

Qu'est-ce qui lui arrivait ?

Un sentiment d'étrangeté la saisit au tréfonds de son âme, elle étouffa un sanglot, consciente qu'un drame c'était jouer en sa présence, mais sans elle, sans qu'elle soit maîtresse de ses actes, de ses paroles. Elle avait complètement disjoncté ! Que c'était-il passé ? Bon d'accord, avec Jules, c'était une relation en dent de scie et probable qu'un jour ou l'autre, leurs chemins auraient pris une traverse mais pas aujourd'hui… aujourd'hui, elle était juste énervée, il l'avait énervé, et elle s'était senti… mal. Nina essayait tant bien que mal de recoller les morceaux des minutes précédentes, mais rien ne traversa la brume de sa mémoire.
 
Dès le jour suivant, un samedi, elle se trouvait de nouveau à son poste de travail, au supermarché, les fesses vissées à son petit siège de torture pour deux périodes de quatre heures interrompues par une maigre pause de quinze minutes. Le rêve !

Les clients se bousculaient a qui mieux mieux, indifférente à leur agitation, Nina passait inlassablement les produits ménagers, pizzas surgelées, et autres produits de consommation avec une dextérité acquise au prix d'une longue expérience. L'inanité de ses efforts pour contacter Jules, jusque très tard dans la nuit, avait mit en exergue la violence de la dispute et sa probable irrévocabilité.
Dans les hauts-parleurs, une voix nasillarde invitait les clients à s'attarder au rayon viennoiseries, des promo, des promos, des promo ! A gogo ! 

Et ces crétins vont se précipiter, songea-t-elle, ça va foutre plein de miettes sur mon tapis…
Tous les samedis, c'était la folie, Gradie abattait toutes ses cartes pour atteindre un nouveau chiffre d'affaires record. Il tarabustait le personnel, chipotait les pauses, réprimandait une attitude d'un employé un peu trop laxiste ; en gros, c'était le cirque chaque samedi.

Une dame à l'embonpoint débordant se pencha par dessus sa caisse.

— Ma petite pas le moment de ralentir, Y a du monde qui attends là derrière.

Son regard se porta sur la file qui s'allongeait derrière la caisse numéro 3.

Nina suivit son regard. Une demi douzaine de personnes attendait derrière la grosse dame.

Un sentiment de lassitude envahit la jeune femme. Elle se frotta énergiquement le cuir chevelu, toucha sa cicatrice récente par inadvertance, et grimaça. C'était légèrement douloureux.

Alors quoi ? Un homme d'une quarantaine d'année l'interpella en bougonnant.

Ça ne doit pas être facile tous les jours hein, convint la face rubiconde qui lui souffla dans le visage un relent d'ail et de persil.

À présent, les mains de Nina ne frappaient plus le clavier, ils ne poussaient plus la file ininterrompue de marchandises. Les bras ballants figés le long du corps, les yeux perdus dans le triste horizon du fond du supermarché, la caissière, statique, percevait les bruits alentour à travers un filtre occultant, ne distinguant qu'un vague brouhaha, à peine audible. De même, elle pressentait cette foule autour d'elle s'agiter et râler sans en éprouver que cette lassitude extrême, d'où naquit bientôt une colère rageuse. Elle afflua à peine à sa conscience, mais elle était tapie là, dans l'ombre, et n'avait de cesse de grandir insidieusement et se répandre, plus vite qu'un poison, bien plus vite. En un battement de cils, cette colère la submergea discrètement, elle perdit le contrôle de son être sans soupçonner un instant les conséquences de cette vague destructrice.

Qu'ils aillent voir ailleurs si j'y suis ! Marre de tout ça ! Allez faire la queue ailleurs bande de cons ! Songea-t-elle seulement.

Le client râleur derrière la femme imposante recula soudain, bousculant tout le monde, un couple d' adolescents, juste derrière lui, hésita, puis quitta la file, suivit bientôt deux autres clients.
Rose ressentit physiquement le poids de cette répulsion à son égard, comme une gifle et se tassa malgré elle. Rien ne lui vint à l'esprit qu'un brusque besoin de s'éloigner de là. La caissière en face d'elle, statique, gardait ses paumes sur ses oreilles, regardant au loin. La scène ne perdura pas au-delà d'une poignée de secondes, pourtant Rose eut le sentiment, rétroactivement, d'avoir mené un combat à mains nues, son corps empâté, soudain épuisé demandait grâce. Elle n'arrivait plus à réfléchir.

Nina cligna des paupières aux braillements intempestifs du haut-parleur. Des marchandises passèrent devant ses yeux : paquet de pâtes et de farine, brocolis, melon, bouteille d'eau et de soda, flacon de shampoing, beurre, tous se précipitaient au bout de son tapis roulant et, comme les moutons de Panurge, ne trouvant âme charitable pour les recueillir, chutaient au sol dans un fracas assourdi.

— Madame ? Murmura Nina.

Mais il n'y avait plus de madame. Il n'y avait plus personne d'ailleurs à la file de la caisse 3.

Du rayon « cafés et chocolats », le doc Clemens déboula en traînant son caddy, il se dirigeait vers les caisses. Il aperçu la jeune femme de l'autre jour, celle qui s'était pris la foudre en plein sur sa petite caboche. D'un regard froidement professionnel, il la détailla un instant, lui trouvant une maigreur nouvelle, une mine affreuse, un teint crayeux, une bouche pâle, comme si le sang ne l'alimentait plus. Puis il s'en détourna aussitôt, mû par un sentiment d'urgence qu'il n'identifia pas. Et pour cause, ce n'était guère quelque chose que l'on ressent en allant faire ses courses dans un supermarché : cela se rapprochait fort de l'instinct de survie. « Fuis » lui hurlait-on quelque part en dedans sans qu'il l'entende, mais il le ressentit suffisamment pour s’installer à la caisse 6, la plus éloignée de la fille électrocutée. En sécurité.

Nina s'engonça dans ses couvertures. Elle n'avait jamais autant manqué le travail en si peu de temps. Qu'est ce qui se passait ? Elle composait avec une fatigue inexplicable et un ras le bol généralisé. Son entourage la fuyait, elle s'en rendait compte, il se passait un truc pas clair. Quelque chose clochait – Lamentablement -
Jules n'avait toujours pas donné signe de vie malgré le harcèlement téléphonique qu'elle exerçait à son égard, le Doc Clemens, récemment contacté, n'avait « pas de place dans son agenda pour la caser » en ce moment, un docteur qui ne voulait pas recevoir sa patiente ! Du jamais vu ! Alan avait décliner, à son dernier passage à la bibliothèque, une invitation à une conférence sur les contes russes qui se tenait pourtant sur son propre lieu de travail ! Si elle prenait le bus, elle se retrouvait isolée dans un coin avec une demi-douzaine de sièges vides autour d'elle. Son conseiller bancaire lui avait gracieusement offert un délai supplémentaire pour régler son découvert, chose absolument improbable au vue de ses résultats bancaires de ces derniers mois en la fichant dehors de manière cavalière, et au supermarché, voilà que plus personne n'utilisait sa caisse pour payer ses courses… si bien qu'elle se retrouvait affectée aux stocks, pour un temps indéterminé, et encore, ceci après avoir négocier hardi-petit avec Gradie. Encore n'avait-il cédé qu'à contre cœur, elle n'était pas folle. Il l'avait supplier de la laisser en paix ! Qu'est-ce qui se passait bon sang ?
 
Elle s'endormit ballottée par ces questions sans réponses. Sa cicatrice la démangeait, elle se gratta plusieurs fois le sommet du crâne, à sang, mais le sommeil la tenait fermement, elle rêva d'orages.

Elle ne sortit plus que rarement. Affronter le monde extérieur, les rues encombrées, le bruit, tout lui était devenu insurmontable. Il lui fallait tout de même se rendre à son travail, puis en revenir et Nina prenait sur elle. Elle faisait le trajet à pied, renoncant au transports en commun, peuplés d'emmerdeurs.

Elle rentrait chez elle et l'aperçut comme il réceptionnait sa livraison au resto chinois où, il n'y a encore pas si longtemps, il leur arrivait de dîner en tête à tête. Sa voiture, une vieille berline déglinguée, vert bouteille était garée un peu plus loin, le long du trottoir.

Elle ne pouvait guère l'éviter sans avoir l'air d'une gourde, et la reconnaissant au dernier moment, l'homme ne cacha pas sa surprise. Leurs regards se confrontèrent une seconde.

— salut !

— […]

— Tu n'es pas obligé de me faire la gueule, jules.

Une nostalgie soudaine et incongrue la fit poursuivre. « On pourrait peut-être… »

Une portière claqua et une grande brune au teint bronzé s'approcha aussi vite que lui permettaient ses hauts talons. Une longue jupe camel battait son flanc, un chemisier semi transparent laissait deviner une lingerie fine.

— Un problème mon Ange ? demanda-t-elle, s'adressant à Jules d'un ton de propriétaire.
— Aucun, répondit l'intérressé dévisageant Nina, espérant être dans le vrai.

Nina serra les lèvres. Jules pris la main de sa compagne et recula, le sac de bouffe chinoise tenue devant lui tel un bouclier de pacotille.

— On s'en va, ajouta-t-il à l'adresse de Nina.

Cette salope ! rumina instantanément Nina, cette salope ! A peine j'ai eu le dos tourné qu'elle lui a sauté sur le râble ! 
Elle l'a connaissait de vue, cette garce travaillait dans un magasin de jeux vidéo, deux rues plus bas.
Et l'autre là, qui lui fait des yeux de merlan frit ! Immobile, elle fixait les escarpins vertigineux de sa greluche.

Profitant du répit, le couple s'éloignait déjà quand la jeune femme tituba brutalement et s'étala de tout son long sur la chaussée. Jules lâcha ses courses et vint au secours de la malheureuse, le nez et les genoux en sang. Il lança un regard accusateur vers Nina. Elle n'avait pas bougé et se tenait, mains sur les oreilles, la tête légèrement penchée, semblant totalement absente.

Elle reprit ses esprits au son d'un klaxon. Une voiture verte reculait à vive allure, en échelant légèrement sur le trottoir. Arrivée à sa hauteur, une vitre s’entrouvrit. Un jules effaré et furieux lui montrait le poing.

— Ne t'approche plus de nous ! Hurla-t-il, le visage apoplectique, ou je te promets que la prochaine fois je vais voir les flics direct t'as compris ?

Le véhicule émit un hoquet, repartit en avant et s'inséra dans la file de voitures, non sans se faire copieusement houspiller.



Parvenue à la porte de son domicile dans un état proche de l’égarement, Nina collecta machinalement son courrier, s'en saisit et verrouilla la porte derrière elle. Un sentiment d'épuisement intense s'abattit sur elle. Elle s'effondra comme une masse sur son canapé quand une des enveloppes qu'elle tenait à la main attira son attention. Elle était à l'effigie de son entreprise. Ce n'était pas le jour de paie.
Prise d'une intuition malsaine, elle déchira précipitamment l’enveloppe et parcourut avidement la lettre ainsi extirpée. Elle relut le feuillet une demi douzaine de fois.
Rencognée dans la profondeur moelleuse des coussins, l'incompréhension et le découragement la saisirent mais elle retint le sanglot qui se frayait un chemin dans sa gorge comprimée. La colère monta.
Sans plus réfléchir, elle franchit de nouveau le seuil de sa maison et se précipita dans la rue. Gradie allait voir de quel bois elle se chauffe ! Oh oui ! Il allait le regretter !


Devant le supermarché, la petite foule habituelle des clients réguliers s'engouffrait en cette fin de journée d'avant la fermeture. Coraline devait être à la caisse, avec Mathis, et le patron dans la réserve à compter son sale argent. Elle fila directement vers l'arrière du magasin sans saluer ses collègues, tordit le cou à la vue des vitres grillagées protégeant le bureau, mais à sa grande déconvenue, celui-ci était vide. Un instant déstabilisée, elle fit demi-tour, et cette fois se dirigea comme un missile sur sa cible en direction de la caisse 3. Une femme d'un certain âge, aux épaules voûtées, trônait derrière la caisse. Une couronne de cheveux poivre et sel en bataille, un nez patricien, deux yeux légèrement globuleux derrière de petites lunettes cerclées de rouge (seule coquetterie affichée de la quinquagénaire) et une bouche fine, aux coins marqués de ridules disgracieuses la regardèrent s'avancer avec surprise. Ses mains passaient machinalement des articles devant le boîtier numérique.
Le « bip, bip » assommant de la caisse électronique heurta douloureusement les tympans de Nina.
Un client, un vieil homme en gabardine, une canne posé sur le bras, rangeait méthodiquement ses courses dans un vieux panier en osier posé dans son caddie.
La caissière leva la tête, faisant face à la jeune femme encore haletante de sa marche forcée jusqu'au supermarché.

	— T'as décidée de faire des heures supplémentaires ?

— Où est Graddie ?

— Bonjour Nina, toujours aussi aimable ! Le patron est pas là, un problème de chargement, il est parti voir, demain c'est la réception des fruits et légumes, on peut pas se permettre un retard de livrai…

— Rien à battre, il est où ?

— Ben à Brienne ! Mais qu'est-ce que t'as enfin ? T'es blanche comme une morte ? Nina ? 

— Tu vas me dire que t'es pas au courant ?

— Je t'assure, je vois pas de quoi tu causes ! Merde Nina, tu me fais peur là ? C'est quoi ton problème ?
Coraline perdait pied. Sa collègue ne semblait pas dans son état normal, elle balançait la tête, les mains sur les oreilles, le regard absent.

— Nina ? Elle avait crier.

Le tapis s'était arrêté, et le client, instinctivement, s'était prudemment écarté des deux femmes.

— Tu vas me dire que tu sais pas que je me suis fais virer ? Hein ? Tu vas oser me dire ça ?Éructa Nina d'une voix sourde, que sa collègue ne lui connaissait pas.

Mathis, soupçonnant une dispute sans parvenir à entendre leur échange, tenta d'intervenir de son poste de travail.

— Nina, un soucis ? On en parle à la fermeture si tu veux, lança-t-il à travers le magasin.

Tous les clients à proximité se retournèrent et cherchèrent du regard à qui il s'adressait. Bientôt tous les yeux se braquèrent sur Nina.

— Ta gueule espèce de fayot ! Occupe-toi de tes oignons !

— Mais enfin Nina, repris Coraline, se mettant à trembler, Y a certainement une explication, je te jure que je sais rien ! Pas la peine de faire une esclandre devant tout le monde.

— Où est cet enfoiré de Graddie ? Demanda Nina d'un ton lugubre.

— À l’entrepôt ! Merde ! Pourquoi je te mentirai ? 

— Vous êtes tous des salopards de menteurs !

Les clients, à présent, s'étaient approchés pour écouter. Un silence inhabituel régnait dans le supermarché. Mathis ne mouftait plus, et Coraline tétanisée par ce flot de violence verbale soudaine ouvrait et fermait la bouche comme un poisson échoué sur la grève.

— Toi, espèce de garce, tu voulais ma caisse, et tu l'as ! L'apostropha Nina, les yeux exorbités,. Elle fixa Coraline d'un air mauvais et se tut un instant, avant de murmurer entre ses dents serrées au point qu'une lame de couteau n'y serait point passée,

— Je voudrais que ce putain de magasin brûle ! Qu'il crame avec tout ce qu'il y a dedans !

Coraline se tassa malgré elle. Rien ne lui vint à l'esprit pour calmer sa collègue, pâle comme un linge, en transe devant sa caisse. Elle ne parvenait pas à réfléchir. Nina serrait ses paumes sur ses oreilles, dodelinant de la tête et soudain, médusée, Coraline la vit détaler comme un lièvre à travers le magasin, prendre les portes automatiques, traverser le parking et disparaître. La scène n'avait pas duré plus d'une poignée de secondes, pourtant Coraline eut l’impression que sa vie venait de prendre un tour nouveau — nouveau et inéluctable — 

Nina se réveilla sur son canapé. La sensation d'avoir fait un très mauvais rêve lui fit reprendre ses esprits, elle s'affaira, allumant machinalement la télévision et passant dans la cuisine se préparer un café serré.
Quelle heure était-il ?

Elle jeta un coup d’œil à la pendule murale : huit heures… du matin ! Elle avait dormi une douzaine d'heure, au bas mot.

La cafetière émit un rot sonore et démarra.

[…]
Dans le salon, une voix féminine commentait les dernières informations régionales, mais le sens du propos échappait à Nina.

La jeune femme extirpa sa tasse favorite du vieux mado hors d'âge qui lui servait de vaisselier ; une tasse avec une couronne grossièrement figurée, où était inscrit : « l'employée de l'année ! » En lettre d'orées. Un cadeau de ses collègues. Ses collègues… Une réminiscence effleura sa conscience, et un sentiment de culpabilité l'étreignit sans qu'elle ne puisse sciemment l'identifier. Un malaise naissant lui fit crisper la main sur le comptoir.

À la télévision, la journaliste commentait, d'une voix solennelle un événement dont le sens du discours parvint enfin à la compréhension de Nina.

— […] Le feu est circonscrit depuis maintenant une heure, et à cet instant, deux faits sont avérés. Premièrement, l'incendie qui s'est déclaré peu de temps après la fermeture est selon toute probabilité de nature criminelle, des bouteilles type allume-feu pour barbecue ont été retrouvées dans le bureau du chef d'entreprise. Deuxième information, plus tragique encore, un corps a été sorti des décombres il y a quelques minutes. Nous n'avons pas plus d'information à ce sujet pour l'instant. L'identification est en cours. Les pompiers, toujours à l’œuvre […]

— Nina lâcha sa tasse. Elle se rua vers le poste de télévision, écarquillant les yeux sans oser y croire. Elle vit le supermarché, son supermarché, du moins ce qu'il en restait : des poutrelles métalliques noircies et tordues, un toit effondré, une fumée grise qui s'échappait encore par endroit…

Elle se prit la tête à deux mains, grattant furieusement sa petite cicatrice, toute petite, au milieu du crâne, enfouie sous son épaisse chevelure de jais et se mit à hurler. Elle se remémora confusément sa dispute avec Coraline, les clients stupéfaits, ses propos haineux, sa colère immense, dévorante. Le choc la fit tomber à terre. Elle avait provoqué tout ça ! C'était une évidence, sa vie avait brutalement tourné au cauchemar et pourtant, elle ne savait l'expliquer. Elle avait fait tomber cette fille dans la rue, elle avait fait peur à Alan, elle avait menacé Jules, elle…
Elle se mit à sangloter, noyée sous des réminiscences abjectes de tout ces événements de ces derniers jours. Mais qu'est-ce qu'elle avait fait ? 

— JE SUIS UN MONSTRE, hurla-t-elle en s’arrachant violemment du sol et se précipitant dans le couloir (peut-être dans le but de fuir un instant ces pensées qui tourbillonnaient en elle ).
Elle tomba en arrêt quelques mètres plus loin devant la psyché qui se tenait juste à l'entrée de sa chambre. Elle y aperçut une femme dépenaillée, décoiffée qui la regardait un sourire mauvais collé sur ses lèvres violacées. Son teint translucide et sa physionomie n'avait aucun semblant d'humanité, Nina ne se reconnut pas : elle hurla de plus belle, s'adressant à son image monstrueuse :

— MEURTRIERE ! 

Le reflet acquiesça d'une moue boudeuse et sardonique déformant encore un peu plus son visage.

— ASSASSIN ! Putain ! qu'est-ce que tu m'as fait ? Hein ? Qu’est-ce que tu m'as fait ? 
Son image spéculaire ne répondit pas ; elle l'a dévisageait, les yeux injectés de sang, la bouche pincée, une mince flammèche bleutée palpitait au-dessus de sa tête, à l'emplacement de sa blessure récente.

— Mais qu'est-ce que tu m'as fait ? Répétait Nina, incrédule, et effrayée. « Je… Je voudrais que tu crèves ! Saloperie ! Crève !! crève ! CREVE ! »

Elle contempla ce simulacre mercuriel d'une Nina au regard froid et rusé posé sur elle, se tenant les paumes sur ses oreilles, son corps filiforme se dandinant de gauche à droite. Cette chimère la lui faisait face, pleine de malice, tandis que l'autre Nina, au comble de l'épouvante gueulait sans discontinuer, 

— CREVE !CREVE ! CREVE !

Et Nina n'eut soudain plus rien à dire.

Son corps s'effondra lourdement le long de la psyché. Sa bouche s'ouvrit grand, muette, alors que son cœur, suivant son propre vœu, s'arrêtait de battre.

À la télévision, la journaliste au ton grave entamait un nouveau compte-rendu :

— […] Nous apprenons à l'instant que le pyromane vient d'être interpellé dans l'affaire de l'incendie du supermarché. Il n'a pas cherché à s'enfuir. Il s'agit d'une femme, une des caissières du magasin dont nous ne sommes pas en mesure de vous donner l'identité pour le moment. Elle a été retrouver en complet état de choc derrière la grande surface, un bidon de substance inflammable encore à la main. Les enquêteurs vont poursuivre leurs investigations afin de comprendre […]
