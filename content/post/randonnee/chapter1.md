Fascinée par le panorama, Annie se permet une petite pause. Un long ruban de brume s'étale sous ses yeux, caressant la rivière sortie de son lit. un ciel rose et bleu colore l'horizon de manière fantasque À perte de vue, des étendues sauvages faites de chênes majestueux sur des îlots encore à sec où de bas buissons d'épineux moutonnent à l'infini. L'eau partout présente, serpente en méandres paresseux entre les différents coins de terre tourbeux, où une multitude d'oiseaux, certains au longs cous gracieux, d'autres aux pattes allongés, s'ébattent en toute quiétude. L'air est très frais. Bien avant l'aube, la jeune femme et ses compagnons de randonnée se sont apprêtés hâtivement, mais chaudement, pour assister à ce spectacle de cette nature encore sauvage et libre de toute trace d'activité humaine. Le parc naturel, ancienne friche industrielle du vingt-et-unième siècle, s'étend sur des centaines d'hectares où les petits mammifères, lapins, ragondins, rats musqués, belettes, chats sauvages, putois, zibeline pullulent, ainsi que quelques grands mammifères, lynx, renards, loups gris. Dans cette zone humide, part belle est faite aux batraciens, reptiles et oiseaux qui s'en régalent ; les grues, aigrettes, bécassines, sarcelles s'en donnent à cœur joie. Un concert de cris, sifflements, jacassements et autres musiques résonnent dans cette aurore presque irréelle, au milieu d'une faune qui s'éveille à peine.

Annie ne regrette décidément pas son choix. Plusieurs randonnées la tentait, et elle a hésité jusqu'au dernier moment, quand Éric, son vieux copain de toujours, lui avait signalé cette offre particulièrement intéressante concernant ce circuit. Une très bonne affaire. Elle sourit aux anges, bien décidée à évacuer les miasmes de cette rude journée de marche, laissant les autres marcheurs s'éloigner en commentant la beauté du site puis, saisissant son téléphone, réalise quelques clichés.

Une vraie touriste badasse, songe-t-elle. Hélas, l'achat d'un véritable appareil photo n'était absolument pas envisageable. Annie est institutrice dans une petite école de campagne Estfrance. Son salaire lui permet à peine de dégager une humble cagnotte, pour un voyage au bout du monde (plus ou moins, selon la mine rebondie de la bourse en question) environ une fois tous les deux ou trois ans ans. Elle mitraille la flore alentour tandis qu'au loin,les bavardages du groupe la bercent et l'isolent un peu plus dans cet écrin végétal.

Elle ne comprend pas beaucoup l'anglais, et Éric, non plus… les boulets ! la jeune femme sourit encore. Jusqu'ici, ils s'en sont malgré tout bien sortis ! Le plus terrible pour elle a été de prendre l'hélivion, sa légère claustrophobie ayant tendance à se majorer dès qu'elle circule enfermée dans un espèce de sarcophage métallique à plusieurs kilomètres au-dessus du plancher des vaches !

Mais ensuite, l'aérobus cahotant et les jeepskis surannées ont remplis leur fonction, l'inconfort étant un « incontournable » de ce genre de virée dans la nature. Depuis lors, après deux jours à dormir à la dure, son dos criant grâce, mais ses jambes fonctionnant parfaitement, elle s'est résolue à avaler quelques anti-inflammatoires en cachette, au coucher et se sent quasiment toute neuve à chaque réveil -quasiment-

Profitant de sa longue pause pour s'étirer, Annie se décide à envoyer un selfie à son petit frère, ambulancier, qui n'a pas pu l'accompagner comme prévu parce que Mathilde a décidé d'avoir son bébé juste avant leur trekking.

Constamment à se faire remarquer celle-là ! Soupire intérieurement la jeune femme, qui ne supporte que moyennement sa belle-sœur, invariablement affublée d'au moins un enfant en bas âge accroché à ses basques.

Elle tend le bras et prend la pose dos aux marais resplendissant en contrebas, affiche son plus beau sourire, secouant vivement sa longue chevelure brune afin de ramener quelques mèches par dessus ses oreilles qu'elle trouve trop grandes, et appuie enfin sur le petit bouton noir, fixant de ses prunelles sombres le centre de l'écran.

— Ce sera l'ultime photo qu'Albert, son frère, recevra d'elle — 

Le groupe a déjà préparé le bivouac lorsque la jeune femme s'installe auprès de son ami Éric, tendant ses mains glacées aux flammes généreuses.

— Tu n'aurais pas dû retirer tes gants, lui souffle-t-il, les yeux dans le vague.

— Tu as bu, lui lance-t-elle avec un air de reproche qu'elle maîtrise difficlement.

Peter, leur guide, lui tape amicalement sur l'épaule et lui tend une gourde, qu'elle refuse.

— Tu as tort, essaie, tu verras, ça réchauffe et ça efface les courbatures, l'encourage Éric.

Peter semble approuver la remarque, baragouine dans un anglais rocailleux et approximatif qu'elle ne comprend pas ; tout le monde rit. Il y a là Violette et Maude, deux quinquagénaires et Victor, la trentaine, tous trois d'Europe de l'ouest, comme Annie et Éric. Serrés sous une même couverture, les jumeaux Matt et Tony, à peine un demi-siècle à eux deux, ingurgitent un réchauffé de ragoût en boîte qu'ils font passer à grandes gorgées de vodka. Eux sont du cru, Europe de l'est, et d'ailleurs ils échangent régulièrement avec leur guide dans une langue gutturale et râpeuse qu'ils sont seuls à manier et à comprendre. Enfin, tremblante comme la flamme d'une bougie, Anna, la doyenne, une asiatique qui se targue d'avoir à son actif plus d'une centaine de trek extrêmes. Une moue blasée ne la quitte pas et il peut se passer des heures sans qu'elle n'accorde d'attention à aucun du groupe hormis Ruth, le grand malamute, qui porte tout au long du jour deux grandes sacoches contenant les tentes et l'équipement de survie. Un animal impressionnant de part sa taille et ses yeux, d'un bleu si pâle qu'ils semblent blancs et lui confèrent un regard étrange, sarcastique, presque humain.

Le jour a filé depuis longtemps quand enfin chacun se replie sous son abri.

Leur installation est spartiate et Annie a bien du mal à s'assoupir. Les genoux d'Éric lui rentre dans les reins ; il s'est effondré dans les bras de Morphée à peine couché, emmitouflé dans son moelleux sac de couchage de plume. La jeune femme envie son insouciance et sa résistance. Ses ronflements réguliers emplissent le silence nocturne rarement troublé par plus d'un hululement de chouette ou le léger trottinement d'un rongeur.

Elle se tourne et se retourne dans le petit espace confiné de la tente sans trouver le sommeil. D'habitude, elle profite à fond, se laisse porter par ses sensations, les découvertes au grand air, la nature superbe… Mais ici, elle a du mal à décrocher, quelque chose l'oppresse, un sentiment d'urgence, une inquiétude qu'elle n'explique pas et qu'elle ne maîtrise pas plus mais qui croît au fur et à mesure des jours qui passent.

Elle grogne et s'enfonce au plus profond de son sac.

Au petit matin, évidemment, elle est la dernière debout. Peter manie l'humour acerbe dès l'aurore, une blague qui semble se rapporter au fait qu'il peut perdre un certain pourcentage de traînards… Ceci avec un grand sourire carnassier. Comme toujours, il plaisante, mais Annie, mal à l'aise sous le feu des regards de connivence des randonneurs, s'active pour rassembler ses affaires et se promets de ne pas se laisser distancer dès les premières heures du jour. Elle avale un thé tiède, harnache son sac, resserre les lacets de ses chaussures, vérifie la balise pendue à sa ceinture puis rejoint la file s'étirant déjà dans un défilé encaissé qui doit les mener sur une colline promettant un point de vue exceptionnel. La végétation devient rapidement dense, presque oppressante et les randonneurs sont silencieux tout à leurs efforts pour progresser à la cadence rapide de leur guide. Le chien va et vient le long de la sente, aboyant joyeusement.

Annie admire son entrain, elle qui peine à suivre l'allure, pourtant encouragée par Éric, quelques mètres devant elle. Un oiseau la surprend en sifflant à ses oreilles et elle le cherche des yeux. Il est là ! Créature minuscule, perché à un mètre d'elle à peine, sur une branche d'épineux rabougri. Le plumage d'un bleu mordoré, le bec orangé, il la fixe avec aplomb de ses trois petites billes noires.

— Trois ? —

Annie cligne des yeux, mais il est trop tard, l'oiseau s'envole dans un discret battement d'ailes. La randonneuse hausse les épaules, accélère l'allure tout en se morigénant;

Je devrais profiter de l'aventure au lieu de rêvasser, ça va trop vite passer et je regretterai d'avoir gaspillé mon temps comme une névrosée, à me faire des films.

Enfin l'horizon se dégage et de nouveau, les marais étalent sous leurs yeux ébahis toute leur splendeur. Du sommet du monticule, le soleil, d'un jaune falot, illumine d'airain la végétation en contrebas où un dernier ruban de brume dessine des runes par delà les étendues d'eau. Dans leur dos, la combe qu'ils surplombent avec ses déchiquetés rocheux, ses apics et saillis illustre les difficultés qu'ils viennent de traverser.

— Magnifique ! Commentent les jumeaux en mitraillant le paysage de leur puissant Nikon.

Chacun pose son bardas, rapidement repu de beauté et la petite troupe s'offre la pause déjeuner tout en commentant le panorama qui varie à chaque heure du jour. Éric a compris quelques bribes de conversation, un peu plus tôt, qu'il s'empresse de raconter à son amie.

— Apparemment, lors de la dernière rando, il y a eu un soucis avec un couple de belgeest.

— Quel genre de soucis ? Demande Annie, soudain incapable d'avaler une bouchée de plus.

– Je n'ai pas trop compris, ils se sont perdus je crois, quelque-chose comme ça. Mais bon, on a tous une balise de détresse, ajoute-t-il en remarquant la pâleur soudaine de son amie, il ne faut pas s'inquiéter. Il lui tape gentiment sur l'épaule, dans un élan de réconfort presque fraternel.

Le soir venu, Annie, rompue, se traîne autour du camp à la recherche de bois mort afin d'alimenter le feu et chasser l'humidité des chaussures qui les rend si désagréables à porter. Elle marche sur un tapis de mousse à quelques jets de pierres des autres, lorsqu'elle distingue une sorte de gros chat cupride en train de dévorer une proie qu'elle n'identifie pas. L'animal ne l'a pas senti, ni entendu, il s'affaire goulûment, fourrageant dans les entrailles de sa victime en émettant des roronnements de plaisir. Annie recule vers le campement. Malencontreusement, la jeune femme glisse sur un tapis de feuilles en tentant de s'éloigner. le félin, de suite alerté, volte vivement.

Annie pousse un hurlement; la bête n'a pas du tout la face d'un chat mais une gueule rose démesurée armée de plusieurs rangées de dents fines comme des aiguilles et brillantes comme des diamants. Elle ouvre sa monstrueuse mâchoire, qui s’avère rétractile, vers la menace qu'Annie représente, sifflant et grognant tout en reculant dans les profondeurs du sous-bois dans lequel elle finit par disparaître. Annie se rend compte qu'elle n'a pas remarqué d'yeux, juste l'énorme gueule de cette créature, semblable à celle des requins (qu'elle a eu l’occasion de découvrir au musée d'histoire naturelle de la capitale l'an dernier, lors d'un voyage scolaire).
Suffoquée, le cœur cognant dans sa poitrine, elle demeure assise sur ses fesses telle qu'elle est tombée jusqu'à l'arrivée de Peter, alerté par son cri. L'homme la relève sans ménagement et la sermonne, pas besoin d'être anglais pour comprendre, sa mine fâchée est suffisamment révélatrice de son discours. Il la raccompagne un peu rudement jusqu'au camp tandis qu'elle montre les fourrés. De mauvaise grâce, le guide envoie le chien renifler quelque trace, en vain. Ruth ne relève aucune piste. Ils finissent par abandonner, sans oublier de ramasser le bois qu'Annie avait récolté.

Dès son retour, alertés de sa mésaventure, les autres randonneurs s'attardent autour d'elle avec empathie. Violette et Maude lui tendent un cordial qui sent terriblement la menthe et l'anis. Un mélange écœurant qu'Annie refuse poliment pendant que sous le feu des questions, elle tente maladroitement de raconter son aventure. Les mots lui manquent, son auditoire s'impatiente, sauf la vieille asiatique, Anna, assise sur une pierre, un peu en retrait, comme toujours, qui l'observe, l’œil sec et la bouche pincée. Éric ouvre de grands yeux dubitatifs en écoutant le récit de son amie et la jeune femme doute soudain qu'aucun ne la croit, même son compagnon.

La soirée se termine sans elle. Réfugiée sous sa tente, elle tente sans succès de trouver du réseau pour envoyer un message à son frère ; elle se sent désemparée, vulnérable. Une certaine colère la mine aussi ; ou bien se sent-elle un peu ridicule ? A-t-elle imaginé le monstre ? Peut-être a-t-elle tout simplement mal vu… Les larmes roulent sur sa combi autorégulatrice et elle frissonne malgré tout, pianotant rageusement sur l'écran fluo de son portable qui reste désespérément muet. La zone semble totalement blanche.
Le sommeil la fauche enfin sur un surprenant a cappella des jumeaux, une mélodie puissante et rauque qu'ils entament en choeur tandis qu'ils couvent de leurs grandes mains les braises encore rougeoyantes du feu de camp. Leurs voix, chaleureuses et graves habillent la nuit du fond de laquelle les étoiles semblent briller de manière surnaturelle dans cette flore semi aquatique.

Cependant, la nuit n'est pas réconfortante pour la jeune randonneuse réveillée à plusieurs reprises en sueur et tremblante ; la vision du terrible félin la hante.

Dès le lendemain, le groupe atteint une cabane sur pilotis faites de bois et de joncs. Il semblerait qu'un village ait existé, il y a de cela plusieurs décennies, mais il n'en reste rien À moins qu'Annie ait mal compris l'explication de Peter, jovial comme à son habitude et qui ne paraît pas lui tenir rigueur de sa mésaventure de la veille. Il sort de son sac une merveille de longue vue, avec laquelle il invite le groupe à observer la faune et la flore des environs. Plus particulièrement des batraciens, très colorés, et quelques espèces de serpents aquatiques inoffensifs dont la taille impose le respect.

Annie passe son tour d'observation et après un léger casse-croûte sur le pouce, entreprend de suivre le ponton se prolongeant au-delà du cabanon. Elle croise bientôt la vieille Anna, toujours silencieuse, qui fume un cigarillo, les yeux mi-clos, accoudée à un appontement en ruine. Celle-ci lui indique d'un hochement de tête un panneau mangé de rouille où est encore lisible l'injonction "no trepassing" suivi de sa traduction dans d'autres langues, indéchiffrables pour Annie.

Elle sourit à l'asiatique, puis, sans plus réfléchir, autant par défi personnel que par curiosité, enjambe la chaîne barrant l'accès au ponton suivant et remonte tranquillement le chemin de bois qui résonne agréablement sous ses pieds, soulagée d'échapper un moment aux jacasseries du groupe de randonneurs. Éric est occupé à capturer des sortes de minuscules sauriens, espèces endémiques de ces marécages. Il ne l'a pas aperçu s'éloigner. Elle en est rassurée, sa sollicitude lui pèse parfois.



Après quelques centaines de mètres, se présente à elle une nouvelle bifurcation. Un chemin sinue de long de la rive, un autre s'enfonce dans le marais où il se perd sous couvert de grands arbres malingres. De leur écorce suinte un liquide épais et jaunâtre, à l'odeur légèrement ammoniaquée. la jeune femme l'a de suite identifiée : elle déteste l'odeur de l'eau de javel. Le silence règne dans ces parages, pas un cri d'oiseau, même le clapotis de l'eau contre les piliers soutenant les pontons est comme amorti. Annie se sent oppressée, sans trop savoir pourquoi mais elle s'entête, poursuivant son chemin. D'embranchements en embranchements, franchissant d'un bon pas les traverses pourries ou manquantes, elle se perd peu à peu dans ce dédale marécageux, de même que dans ses pensées.

Le jour baisse quand apparaissent les vestiges d'un village sur pilotis. Un silence sépulcral l'accueille. Annie s'arrête un moment pour examiner les parages. Quelques humbles maisonnettes carrées, en partie délabrées lui font de l'œil de leur unique fenêtre. Réalisées dans un bois gris piqué de vers à l'aspect malsain, les toits de joncs sont également rongés par une moisissure noire, épaisse, striant de veines sombres l'ensemble du bâti.

On se croirait dans un film d'horreur, songe-t-elle, comme son corps soudain sonne l'alarme devant ce spectacle hideux et hors du temps. Des frissons la parcourent comme si la température avait baissé de 10 degrés en quelques minutes. La jeune femme resserre prestement sa combinaison autour de ses membres glacés, et, l'oreille alertée par un gargouillement singulier, penche la tête, attentive, afin d'en identifier l'origine. Ce bruit semble provenir d'une cabane un peu excentrée… 
Non, à la réflexion, ce n'est pas un gargouillis, corrige-t-elle, il s'agit d'un rire. Un rire d'enfant. Annie hausse un sourcil, dubitative. Sa moue en dit long sur le débat intérieur qui l'agite à cet instant ; avancer au risque de passer encore pour une imbécile, peut-être même être victime d'une mauvaise farce, qui sait ! ce Peter ne lui revient décidement pas. Faire demi tour, et revenir après avoir convaincu Éric… 
Mais ses pieds sont déjà dans l'action : ils se dirigent droit sur la masure dont une étroite fenêtre se révèle au bout d'un ponton vermoulu À l'intérieur, quelque-chose tremblote, une vague lueur verdâtre, et ce rire ! Joyeux ! C'est bien celui d'un gosse. Annie fait taire tous ses clignotants intérieurs; son souffle, trop court, ses battements de cœurs, déchirants sa poitrine, ce sang, son sang, filant à toute vitesse et lui battant les tempes À petits pas, courbant l'échine, elle progresse en se dissimulant au mieux jusque sous l'ouverture, et constate alors qu'il n'y a pas de vitre protégeant la fenêtre. Elle jette furtivement un coup d'oeil. Des ombres s'agitent à l'intérieur et ce rire cristallin, heureux, à présent tout proche, lui transperce les tympans.



Peter jette un coup d'œil circulaire sur le campement qui se dresse peu à peu pour la nuit. Quelque chose le chagrine, mais il n'arrive pas à en trouver l'origine. Les jumeaux se chamaillent, comme d'habitude, pour le meilleur emplacement, Violette et Maude sortent leur carnet de bord où elles relatent chaque soir la suite de leur aventure, Victor sirote un café lyophilisé en les écoutant distraitement caqueter comme deux poules qui aurait trouvé un mégot, Anna s'active déjà à sa tambouille, le visage aussi indéchiffrable qu'un papier de riz vierge. Un peu plus loin, Éric barbote toujours dans le marais, encore affairé à la chasse aux grenouilles. Quel emmerdeur celui-là, songe le guide, et sa copine bizarre qui traîne toujours la patte… 

Ni une ni deux, il se précipite en pataugeant dans la bourbe pour parvenir jusqu'à Eric, le secouant sans ménagement.

— Où est la fille ? Questionne-t-il.

— Qu… quoi ? Balbutie le garçon surpris et mal à l'aise face au comportement soudain agressif du russe.

— Où est ta putain de copine bordel ? Braille l'autre, dans un anglais toujours très rugueux.

— Je ne sais pas, je crois qu'elle avait besoin d'un peu de tranquillité, elle…

Mais Peter en a assez entendu, il traîne le garçon à sa suite, à travers le camps, lui jetant son sac à dos à la figure et saisissant également le sien, qui contient en plus de la balise de détresse, un pistolet, un vieux TT30, héritage de son grand-père, officier dans l'armée, à la grande époque.

— On va la récupérer ! Lance-t-il rageur en poussant devant lui Éric qui n'ose pas protester, le poids de la culpabilité lui titillant déjà les intestins.

— Elle a franchit la barrière là-bas, intervient l'asiatique, sur un ton toujours égal, comme si la tension, palpable à présent, n'avait pas atteint son espace privé.

Peter plisse les yeux et fixe la femme qui fait de même sans se démonter. Il ouvre la bouche assez stupidement, la referme de même, lève les yeux vers la direction indiquée où le panneau d'avertissement grince dans le vent de manière sinistre, presque provocatrice. Le guide repousse méchamment Éric pour l'obliger à s'asseoir, tout en continuant de l'invectiver hargneusement, inutile de connaître la langue de Gogol pour le deviner. Furieux, il laisse lui-même lourdement choir son sac à terre.
Personne n'ira par là.



Alors que le temps semble s'engluer dans le marais, Annie sort enfin de l'indécision qui la paralyse pour regarder à l'intérieur de la maisonnette. Si des gens habitent ici, elle doit en avoir le cœur net. Le trek sur les contours du parc naturel ne comprend normalement aucune zone habitée, c'est d'ailleurs pour cela qu'elle l'a choisi : Être loin de toute civilisation afin de s’immerger dans la nature sauvage.
Elle relève progressivement la tête afin que son regard parvienne au raz de la fenêtre.
Dans la pénombre règnante, elle découvre un homme debout, lui tournant le dos, appuyé contre un vieil évier de pierre, dans ce qui peut être une cuisine. Le mobilier, délabré, est hors d'usage. Les murs intérieurs, suintent eux aussi cette horrible gelée saumâtre. Le gamin, dont elle n'aperçoit que les épaules voutées, affublées d'un vestige de veste de travail bien trop grand pour lui, poursuit son jeu sans se retourner. Ni l'un ni l'autre n'ont remarqué Annie, accroupie derrière la fenêtre. L'enfant s'esclaffe encore, essayant d'attraper ce que l'homme, son père peut-être, lui jette dans un seau cabossé. Le rire, d'une musicalité exquise, s'élève et emplit l'espace d'une note joyeuse en total désaccord avec cette pestilence de l'air et ce décor lèpreux.
Avec l'obscurité environnante, Annie distingue tout d'abord difficilement les deux étrangers, puis sa vue s'accomode à cette grisaille et son cœur s'emballe. Sa poitrine se soulève douloureusement lorsqu'elle tente de reprendre une goulée d'air. Ses fichus pieds, gourds, sont devenus deux blocs de ciment impossible à déplacer.
Et pourtant il le faut ! Maintenant ! s'exhorte-t-elle, Fuis ! 
La main sur l'appui de fenêtre, oubliant toute prudence, les yeux exorbités, elle contemple la scène familiale dont chaque horrible détail se révèle à présent : 
Avec une vivacité foudroyante, l'enfant attrape au vol des projectiles sanguinolents que l'autre lui lance avant qu'ils n'atteignent le seau. En cas de succès, il émet ces sons de gorge si particuliers qui n'ont rien d'humain.
Le gamin est habile, l'échange se poursuit quelques instants, mais suite à un essai moins frutueux, il opère un grand pas de côté afin de saisir puis fourrer la nourriture dans sa bouche, et, ce faisant, se retourne.

Le voilà qui fait face à la jeune femme.

Le sang d'Annie se fige. Peut être a-t-elle espéré crier, hurler et prendre ses jambes à son cou, mais la terreur l'emplit brusquement comme une outre vide et elle ne bouge pas, lourde et gauche, n'émettant finalement aucun son, tandis que s'offre à elle le spectacle le plus monstrueux qu'elle aura jamais l'occasion de voir.
Le jeune — garçon — n'a pas de visage, juste une énorme bouche aux contours rose d'où saillent plusieurs rangées de dents biseautées, posées sur une mâchoire rétractile. Et c'est ainsi qu'il s'avance vers elle maintenant, sifflant bruyamment, la gueule menaçante largement ouverte tandis que — L'homme — se retourne à son tour, alerté. Annie sent son estomac se révulser.
Sa physionomie est plus terrifiante encore, il lui offre une gueule démesurée, à la denture cauchemardesque tout en piaulant de façon effroyable. Il fonce sur elle.
Annie sent son corps se liquéfier, ses muscles, ses tendons, sa volonté, l'ont définitivement lâchés, submergés par la peur.

Elle n'a pas véritablement le temps de formuler sa pensée — je vais mourir —

Les deux monstres ont déjà saisi ses bras et l'attirent à l'intérieur.



— Activez la balise ! Ordonne Éric, faisant preuve d'une soudaine autorité dictée par la trouille.

— Il est trop tard, éructe Peter, je vous ai fait signer un contrat. Vous vous êtes engagé à en respecter les termes, bon sang ! Vous faites de l'antétourisme, je vous rappelle, pauvre débile, visiter l'ancienne Europe, c'est dangereux.

— Vous êtes cinglés, leur lancent les jumeaux d'une seule voix.

— Non, il a raison, assène l'asiatique, saisissant le bras d'Éric. Elle lui agite sous le nez une antique pancarte aux couleurs fanées, couverte de symboles jaune et noire où l'on déchiffre difficilement un mot.

— Чорнобиль —

— ça veut dire quoi votre truc, vieille folle ? s'insurge Eric, à bout de patience.

— ça veut dire, jeune homme, qu'avant de boire la coupe d'herbe amère jusqu'à la lie, vous auriez dû, vous et votre amie, vous renseigner sur ce genre de trek. Ce territoire, c'est ce que l'on appelait l'Ukraine, il y a une bonne centaine d'années et plus précisément ici, c'est un lieu historique. Tchernobyl, que ça s'appelait, jeune inculte.

— Annie a disparu et vous me faites un cours d'histoire ! Qu'est ce que j'en ai à f…

— Votre copine a réveillé le croquemitaine en allant fourrer son nez où il ne faut pas, poursuivit la femme, secouant la pancarte de plus belle. Normalement, si on ne les dérange pas, il n'y a aucun danger. Chacun chez soi. Il ne faut pas quitter la zone.

— Mais qui ? Mais c'est quoi ces conneries ? Hoquette Éric, totalement suffoqué par les reproches de ses supposés compagnons de randonnée.

— Ça va faire des histoire tout ça, soupire le couple de quinquagénaires.

— Je ne pense pas, sourit l'un jumeaux, Matt, celui qui possède une fine cicatrice sous l’œil droit, fixant le marais au loin, puis leur guide, d'un air entendu.

— Je ne veux pas être témoin de ça, éructe l'asiatique, jetant avec force la pancarte à terre en s'éloignant.

Éric, soudain inquiet, ouvre la bouche pour protester, mais il reçoit la crosse du TT30 en pleine mâchoire et s'effondre inconscient.



Il ne paraîtra qu'un entrefilet, plus tard, beaucoup plus tard, aux infos locales, quand le frère d'Annie s’inquiètera de ne plus avoir de nouvelles, quand les autorités alertées auront fait ce qu'il y a à faire, c'est-à-dire rien (tant qu'on ne les dérange pas, c'est tranquille alors…).
Entrefilet concernant la disparition malheureuse d'un couple d'antétouristes imprudents, portés disparus sur le vieux continent au cours d'une randonnée…
