---
title: Le marchand de peaux de lapins
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: ""
epub: true
pdf: true
draft: true
---
La mère brossait un pantalon de coutil sur la pierre à eau, chantonnant un vieil air, tandis qu'un soleil rasant traversait la fenêtre de la cuisine annonçant le crépuscule d'une laborieuse journée. Assis à la table familiale, je mâchonnais rudement mon crayon de papier.

— Tu vas rester toute la soirée à bailler aux corneilles devant tes problèmes ?

Elle ne haussa point le ton, poursuivant durant sa harangue, sa lente torture sur le tissu mousseux, le triturant d'un bout, puis de l'autre, vigoureusement, le geste nerveux et précis contrastant avec sa stature de toute petite bonne femme. J'étais à huit ans bientôt plus grande qu'elle, noueuse, le visage anguleux sous son fichu à fleurs, seul artifice féminin qu'elle se permit. Elle tourna la tête vers moi et sourit. Une mimique mélancolique, la seule que je lui connus. Elle essuya ses mains, longues et prématurément usées après son tablier. Elle les cousait elle-même, avec de vieux coupons de tissus qu'elle récupérait chez ses patronnes du mercredi et du samedi, en plus de ses gages à la manufacture.

— Eh bien quoi Étienne, tu as laissé Maude te manger la langue ?

La dite Maude, gros matin sans race défini, au pelage de feu, leva sa grosse gueule pataude de sous la table, et sa mimique interrogatrice me fit enfin réagir.

— J'arrive pas les mathématiques.

<!-- -->

— Voyons voir mon grand, ça ne doit rien avoir de compliqué !

Un subtil parfum de lavande m'enivra lorsqu'elle se pencha par dessus mon épaule. Le père passa la porte bien plus tard, et la trouva ainsi, gommant patiemment une dernière erreur avant de m'autoriser à refermer enfin mon cahier.

Au coin de la cuisinière, mitonnait le dîner, le couvercle de la cocotte en fonte chuintait et une odeur appétissante de civet aux herbes se répandait, taquinant mon estomac toujours affamé, comme peut l'être un gamin en pleine santé. Un cri émergea de la chambre, Mudine, ma petite sœur, se rappelait au bon souvenir de tous, réclamant la tétée. L'homme qui venait d'entrer, et de poser pudiquement un baiser sur le front de la femme, ébouriffa mes cheveux. Pesamment, il passa précautionneusement l'embrasure de la porte, fort basse, car il se présentait telle une force de la nature, le torse large, le cou épais, et cependant tout en délicatesse, lorsqu'il poussa le battant de la chambre d'où s’égosillait la petite drôlesse qu'il saisit dans ses bras. Une minuscule tête brune apparut dans l'encadrement, riant aux éclats après le bref orage de larmes.

— Regarde bien ton frère, qui se bat avec ses nombres ! Il va finir par en chier une brouettée à force de se pencher dessus tous les soirs !

<!-- -->

— Maxime !

Les yeux ronds et charbonneux de ma mère le réprimandèrent à demi pour cette grossièreté convenue.

Je m'autorisai à sourire, et une autre voix, éraillée, juvénile, pouffa dans mon dos. Mon aîné, Luth.

— Maman t'as encore aidé ! Sacagou\*\*\* !

<!-- -->

— Arrête de m'appeler comme ça !

Le père tira une chaise, le bébé sur les genoux, intimant sans plus de mot le silence entre nous.

Luth facétieux, toujours gai comme un pinson en avril, s'amusait de tout, et souvent de mes déconvenues avec l'école, lui qui, le certificat d'études à peine en poche, travaillait à présent à la fabrique, avec le père. La fabrique nourrissait les villages alentour, et à la Bastide-Maujoie, rares ceux dont aucun membre de la famille ne s'y ruinait la santé. Mais on y gagnait mieux qu'au champs. Le père besognait près des fours ; il poussait les grands dressoirs où s'affalaient les tuiles à cuire, vermeils, humides et molles et celles qui venaient de l'être, devenues d'un rouge sang de bœuf, aux bords flammés, typique de ce coin de région. Luth, comme tous les jeunots, se voyait confier une large pelle, appelée agueuloir\*\* et au pied du broyeur, tâche ingrate, raclait et ramassait tout ce que ce gigantesque engin régurgitait sans cesse. Les femmes aussi besognaient dur. Debout, dans la chaleur à peine supportable, elles voyaient défiler devant elles les tuiles et les éprouvaient une à une, ôtant celles que le moindre défaut rendait invendable. La mère y travaillait quatre jours par semaine, en journée, tandis que le père y faisait les tournées, du lundi au samedi, le jour comme la nuit, les fours ne s'arrêtant point au lever de la lune. Une vie rude, mais aucun ne se plaignait au village, sauf Lamy, la bonne du curé, qui, ne brassant pratiquement que de l'air, entre l'église et le presbytère, s'accordait le luxe de geindre de tout.

Chaque matin, je musardais bon dernier à la maison, attendant l'heure de me rendre à l'école, située à seulement deux kilomètres, dans un bourg nommé Bastide-Murat. Mes parents m'attribuaient quelques corvées à réaliser avant mon départ et dont je m'acquittais sans manquer, car le père n'admettait pas que chacun, dans la maisonnée, ne fit sa part. Malgré mon jeune âge, je réalisai la chance qui s'offrait à moi de ces heures languides où seuls les poulets, le cochon Fanfan, la vache Clémentine, les canards et les oies me réclamaient tandis que Maude, aboyant à tue-tête, m'escortait joyeusement. Je savourais ces instants, en doux rêveur, d'autant que je savais ce temps éphémère et sans retour possible. La mère m'accordait ce répit, et le père, de manière plus bourrue, le verbe pudique, toujours m'encourageait à vivre mes expériences tant qu'elles ne nuisaient à personne. Je me sentais libre. Et je leur dois très certainement ce que je m'apprête à vous raconter, puisque sans aucun doute, ils m'ont fait là un cadeau, un secret, qu'eux-même abritaient déjà, au creux de leurs mains rêches et de leurs cœurs rompus à l'ouvrage, pourtant oublieux du pénible quotidien pour une pincée de tendresse, un peu de magie, de sacré, d'étrange, comme vous voudrez le nommer.

Et voilà qu'un dimanche, à peine l'aurore arrosait-elle les toits de son généreux voile doré, que nous l'entendîmes arriver par la grande rue ; à dire vrai, le village tout entier s'agglutinait le long de cette voie unique À intervalle plus ou moins régulier, elle se fendait d'une ruelle transversale, plus ou moins large, plus ou moins entretenue, selon l'opulence de la propriété sur laquelle débouchait la-dite ruelle. Nous étions sept familles à vivre à la Bastide-Maujoie À l'entrée du hameau, au fond d'un chemin caillouteux sinuant entre des pins rabougris et d’ancestraux oliviers, les Lacour, des éleveurs de moutons ; deux frères avec femmes et enfants, et leurs vis à vis, les Tuipre, après quelques pas sur une sente terreuse, et le franchissement d'une vaste cour derrière une palissade au bord de la faillite. Nichait là toute une smala composée une ribambelle d'individus, plus ou moins apparentés, dont tous, garçons et filles, œuvraient à la manufacture. Ceux-là formaient plus un clan, d'ailleurs, qu'une famille au sens habituel du terme, s'échinant à l'usine de génération en génération, repliée sur eux-même, peu amènes, vivant proche de l'autarcie, braconniers à leurs heures, ne se mêlant à personne, et se mariant entre eux, au grand dam de notre curé. Plus loin, sur la grand route, au bout d'un chemin pavé bien entretenu agrémenté d'une haie de lilas, les Marcouville, un couple de vieillards, dont les enfants depuis longtemps partis à la ville, ne donnaient plus de nouvelles à leurs vieux parents. En face, une allée bancale jonchée de rognures diverses annonçait la grande famille Mounon, affublée d'une pléthore d'enfants, tellement de gredins, à vrai dire, qu'en évoquant un chiffre, on passait forcement à coté, on oubliait un des mouchards ; le petit dernier, dans le berceau, ou celui à naître, que madame Mounon traînait déjà dans son giron. Au centre de cet axe est-ouest, long peut-être de quelques huit cent mètres, l'église, et son presbytère, qui servait également de bureau de poste, de mairie, et peut-être encore d'autres choses, dont je n'ai plus souvenance. En face de l'église, après quelques pas le long d'une sente bordée de massifs de pivoines et de rosiers, les Louis, nous donc, et pour finir, tout à coté, au fond d'un large passage herbeux marqué par les rouées des charrettes et des engins de culture, la ferme Apati, des ritals qui exploitaient un petit vignoble et quelques vergers, en plus de chèvres maigrelettes. Dans leur ligne de mire, avec son préau donnant quasi sur la rue, l'atelier du père Ricoton, charron, accessoirement maire du village, avec sa femme, sa vieille mère, une véritable harpie, et leurs trois enfants. En mentionnant l'église, j'ai sans doute usé de prétention, je devrais plutôt évoquer une chapelle, tant la bâtisse, romane de style, se révèle massive mais minuscule,et je n'ai pas nommé notre prêtre, parce qu'il habite à la Bastide-Murat, et que son nom m'échappe aujourd'hui, mais sa bonne s'occupait du presbytère où elle logeait à demeure. Lamy, un surnom, je l'ai jamais entendu appelée autrement, ce lui allait bien, d'ailleurs, une vieille fille gouailleuse, la quarantaine, peut-être plus, quand l'insouciance de vos huit ans vous fait trouver la plupart des gens très vieux ! Après les Apati, la route menait à La Bastide-Murat ; l'école s'y trouvait implantée, ainsi que d'indispensables boutiques ; un barbier-coiffeur, un cordonnier, et « la ruche moderne », une boutique d'alimentation générale, faisant office de droguerie et vendant également quelques articles de bazar À l'opposé, après les Lacour, la route filait jusqu'à la tuilerie, avant de foncer droit vers la sous-préfecture, La bastide-Méran, une quinzaine de kilomètres plus loin. Entre les deux, se cachaient quelques fermettes encore, ainsi qu'un bûcheron-charbonnier, une région encore bien sauvage, comme disait souvent la mère, par ses gens, et par la rudesse de ce coin perdu. L'hiver nous faisait grelotter, glacial, et l'été, les œufs cuisaient dans les pondoirs si l'on oubliait de les y retirer avant les après-midi caniculaires. L'automne apportait des rigoles de pluie dégueulant des gouttières, et comme, bien entendu, il ne se trouvait point de trottoirs dans les rues, l'eau ruisselait et s'infiltrait partout ; gare à celui qui aurait eu la bêtise de construire son logis sans surélever le rez-de-chaussée d'une, voire deux marches ! Le printemps me direz-vous ? Le printemps s’affichait à la hauteur de nos espérances, gazouillant, pavanant cent teintes de vert, et le ciel nous fichait la paix. Sans conteste, la saison préféré de tous les habitants !

Nous l'entendîmes donc, et comme à l'accoutumée, nous précipitâmes accueillir le cortège avec un mélange d'effroi et d'excitation. Effroi, parce que la vieille Marcouville nous menaçait sans répit à la moindre broutille de nous dénoncer au passage de celui qui présentement s’avançait dans la rue principal, alléguant qu'il nous emporterait, sans remord, au loin, sans espoir de retour. Effroi, car contre toute attente, certains habitants du village observaient à l'égard de notre visiteur une certaine déférence peu coutumière de ces rustauds plutôt enclin aux verbes hauts en couleurs qu'à la politesse. Excitation, parce qu'il remuait en notre âme enfantine, tous les mythes et les croyances fourbis dans les contes de bonnes femmes, auxquels bien sûr, nous n'accordions, officiellement, aucun crédit, mais qui dans les nuits les plus sombres, nous taraudaient le bas du ventre, tortillant nos boyaux dans un ramdam dantesque pour nous laisser hagards et frileux jusqu'au petit jour.

Le haquet brinquebalait dangereusement à chaque pas déhanché de Ptolémée, une improbable autruche de plus de deux mètres de haut, aux pieds à deux doigts de la taille d'une bonne bûche. Elle bombait ses larges ailes noires, repliées sur son torse puis les rabattait vivement et ce tapage, irréel et lancinant, répercuté par le moindre obstacle et renvoyé à travers l'air de cette douce matinée, battait inéluctablement le rappel. Un harnachement de cuir, qu'on distinguait à peine sur son large et duveteux poitrail blanc, en faisait un animal de bas acceptable pour le marchand de peau de lapins. Il cheminait lentement accordant son pas à celui de la bête, une longe brune en corde de jonc tressée serrée dans la main. La charrette, antique, cahotait sur la route, à l'entrée du village et avec elle les planches et les clous tenant ensemble par miracle, ou quelque mauvais sort, débordant d'un bric-à-brac invraisemblable À l'arrière, une marche donnait accès à un étroit boyau menant à une multitude de casiers au fond de la carriole, dans un petit espace couvert d'un toit de peaux tannées, tendues et huilées, s'ils offraient à la vue, les casiers ne révélaient point leurs contenus. De chaque coté des rambardes, dansaient d'autres peaux plus ou moins identifiables, lapins, putois, ragondins, séchant au soleil, tandis que d'autres encore, prêtes à l'usage, se trouvaient rangées dans de grands casiers sous la guimbarde. Un fumet douceâtre et musqué se dégageait de l'ensemble.Des colifichets de plumes, coquillages, figurines de bois sculptées, champignons et fleurs séchées, agrémentaient l'engin, et couraient le long des bras de la charrette, accompagnant le balancement indolent du drôle d'animal. Voilà un spectacle dont nous n'aurions pas manqué une seule représentation ! Maude accourut en jappant se joindre au tintamarre, mais se tint à distance de l'impressionnant oiseau exotique.

Nombreux sortaient à présent de leur maison pour accueillir le marchand de peaux de lapin. Également chiffonnier à ses heures, son commerce interpellait par sa diversité, et son incongruité, à la fois respecté et honni, il représentait l’incontournable et indispensable marchand ambulant, tout comme le repasseur de couteaux (mais le père cramant empuantissait l'air de ses relents de vin à deux sous, sacrait tout le jour, et détestait par dessus tout les marmots dans notre genre). Le nouveau venu, quant à lui, ne dédaignait pas notre compagnie, tout au contraire.

Planté devant son bout de chemin, Maurice Lacour intima le silence à sa marmaille ainsi qu'aux épouses, l'une, la sienne, maigre comme un clou, qu'on appelait Civette, et une autre, plus maigre encore, dont le nom ne me revient plus, mariée à son plus jeune frère, Thiméon. Il engagea derechef la conversation avec le nouveau venu. Nous rejoignîment discrètement l'attroupement.

Le marchand guida son formidable oiseau jusque la bordure herbue, et il se mit sans plus attendre à se régaler de pissenlits, ignorant les regards intrigués des plus jeunes. En face, les filles Tuipre, je reconnus Anna, sans aucun doute la plus jolie de la tribu, et Galia, parce qu'elle se rendait à l'école avec moi, et que nous partagions la même maîtresse, s'approchèrent, accompagnée d'une femme dans la fleur de l'âge, poussant une belle brouette remplie jusqu'aux ridelles d'un capharnaüm innommable.

Le marchand les regardait, indifférent au raffut de la marmaille, prenant son temps. Sa stature, imposante, le rangeait d'emblée dans les géants ; un bon mètre quatre-vingt-dix, pour un quintal sûrement, mais noueux, tout en muscle, avec de grandes mains brunes aux ongles noirs, et une tignasse d'ébène, longue et luisante comme une chevelure de jeune fille, auréolant son long visage. Pas un seul crin d'argent. Il affichait pourtant la soixantaine, sous son grand front où couraient de profondes rides, jusqu'aux coins des yeux. Le regard franc, noir aussi, sous d'épais sourcils qu'on aurait cru coiffés, le nez un peu crochu et long attestait de son indéniable origine, en plus de sa couleur de peau, semblable au bronze ; amérindien À chacun de ses passages dans notre communauté, je ne pouvais m'empêcher de le comparer à mon héros de BD favori, Hototo, chef Hopi à qui il arrivait moult aventures au fin fond des Four corners, en Arizona. Mais mon héros possédait un cheval pie, pas une autruche. Sans m'en rendre compte, je fixais le marchand ambulant depuis un moment et ses yeux de jais, rieurs, captèrent mon léger dépit. Il interrompit ses palabres avec mes voisins, pour me frictionner vigoureusement le crâne de ses mains dures et râpeuses.

– Bonjour, petit Louis.

Les jacasseries recommencèrent avant que je ne trouve le bon sens de répondre. Mon frère se mit à me frotter le dessus de la tête, hilare. Je protestai mollement, tout à mon observation.

— J'ai dix peaux de lapins premier choix, et douze de castors, tu regardes pas les chiffonnades des bonnes femmes de trop près, tu m'embarques le tout, pour un prix honnête, je te le demande du fond du cœur, débarrasse-moi de leur fourbis !

Maurice s'échauffait, un peu rougeaud, gesticulant. Ses femmes attendaient sans mot dire, un peu en retrait, les bras encombrés de linge usé jusqu'à la trame, à peine bon pour la papeterie auxquelles la plupart des hardes étaient destinées.

Fin négociant, leur interlocuteur ferrait ses clients sans émettre un seul son. Il écoutait les doléances des femmes Tuipre à présent, qui sans façon se mêlaient à la conversation.

— Me faut un bon prix de ce tas là, mon brave, de bons chiffons, vois ! Et la plus vieille des trois piqua jusque la brouette et souleva les vieilles nippes une à une, un air de défi dans ses yeux clairs.
— Eh tes vieilleries sont moches comme la laine d'un âne, Mariette, y te les prendra pas, ou faudra que tu donnes de ta personne ! Plastronna Civette, le défi dans la voix.

Enyeto, le marchand de peaux de lapins, resta de marbre aux joutes verbales habituelles, et autres fanfaronnades, il décrocha sans plus de façon une timbale de sa chariote, et l'un des petits qui furetait dans ses jambes la saisit, courut la remplir à la pompe la plus proche, puis sans en renverser une goutte, la tendit à l'indien. Celui-ci se fendit d'un large sourire, en guise de remerciement, but à longs traits l'eau fraîche et claire, s'approcha ensuite de Ptolémée et déposa le restant devant sa petite tête triangulaire. Le silence se fit, l'animal abandonna la verdure pour tendre son long cou, goûta le liquide au fond du récipient, émit un bruit de gorge grave et puissant qui tira des gosses une tripotée de ho ! et de ha ! Alors, la tête dans la timbale, il but goulûment l'eau offerte. Il fit ensuite quelques pas vers sa petite troupe d'admirateurs, et certains eurent d'instinct un geste de recul : vrai, l'animal en imposait ! Une minuscule gamine lui tendit un rogaton de pain noir et dur, que l'autruche ne refusa pas, prenant délicatement l'offrande dans son puissant bec. Nous retînmes notre souffle ; la bête parfois, pouvait se révéler assez fourbe et pincer copieusement. Mais pas cette fois. Un soupir de soulagement emporta l'assemblée.

Et les palabres reprirent de plus belle.

Nous suivîmes les pérégrinations de l'indien toute la matinée. Vers midi, il s'octroya un sobre petit déjeuner fait de pain et de miel. Il régala la bande de gamins curieux d'un quignon dur recouvert d'une fine cuillerée d'or des abeilles que chacun de nous tint religieusement entre ses mains crasses, avant d'entreprendre de le dévorer à belles dents, arrosé d'une lampée de lait de brebis bu à même le pichet offert par un des fils Lacour. Peu de mots échappaient de nos lèvres, Enyeto nous détaillait, de ses yeux d'aigle, serein, indifférent à nos chamailleries d'enfants. Assis sur le talus, et nous tout autour, nous étions sa cour et lui le roi, un roi simple et bon, bourrant d'un tabac odorant une pipe au tuyau long et brun, avec force de gestes précis et précautionneux. Enfin daigna-t-il nous adresser quelques mots.

— Que voulez-vous entendre ?
— L'histoire du loup !
— Êtes-vous certains ?
— Oui ! Gueula l'assemblée juvénile.

Maude, assise auprès de moi joignit ses jappements aux glapissements sonores des gosses.

Le marchand de peaux de lapins caressa la chienne qui se tut soudain, et se coucha sagement le long de mes jambes.

Il sourit, de ses lèvres minces comme un fil, et commença son histoire. Nous écoutions, sans un bruit, retenant même notre souffle, pour ne pas en perdre une miette.

– C’est l’histoire d’un enfant iroquois, à l'est des Amériques, qui, comme tous les enfants, joue, rêve et se pose beaucoup de questions. Un jour, après une dispute avec son meilleur ami, il va voir son grand-père, le chef du village.

Il lui demande :

— *Dis-moi, Grand Père, qu’est-ce qu’un homme ?*

*Alors son grand-père, avec ses mots, l’emmène en voyage. Il lui parle de territoires immenses, connus et inconnus, des montagnes aux cols immaculés, où combattent les mouflons pour gagner la horde des femelles, des plaines aux terres rouges, que survolent le grand aigle brun à la recherche de proies, serpents et rongeurs imprudents, de régions montueuses, nappées de forêts sombres et des loups qu’on y trouve. Il lui explique qu’il y a d’une part ******le loup noir,****** colérique, hargneux et menaçant, qui hurle la nuit, se cache, se bat avec les uns et dévore les autres, domine par la peur et tue sans nécessité. Et d’autre part, il lui dit qu’il y a aussi****** le loup blanc******, accueillant, équitable, solidaire et fraternel. Celui-là protège les siens et soutient les autres.*

Le conteur nous prit à témoin, son regard papillonnant au-dessus de nos têtes, sondant nos cœurs. Les petits se tenaient la main à présent, la suite leur était inconnue, ils l'attendaient, suspendus à chaque mot. Pour nous, les plus grands, le conte, bien que maints fois entendus, à l'instant, nous apparaissait telle un office divin, un temps hors du temps, et nous poursuivions en songe nos propres loups.

Puis le colporteur reprit, sa voix de rogomme emplie de fumée odorante,

Alors le grand-père lui dit : 

— *- Vois-tu, enfant, l’homme possède ces deux loups en lui. Chacun de nous abrite un loup noir et un loup blanc qui ne cessent de s’affronter.*

L’enfant réfléchit et lui demande : 

*- Lequel des deux gagne à la fin ?*

*et le vieux sage lui répond doucement : 
- Celui qui gagne, c’est celui que tu nourris.\*\*\*\* *

**Les dernières paroles, les grands les murmurèrent avec une ferveur candide en même temps que le marchand. Un grand silence s’instaura, une paix soudaine sur nos âmes d'enfants sembla effleurer nos têtes, puis le jacassement d'une pie nous ramena sur le talus, au milieu du village. **

**Les gosses s'éparpillèrent, rameutés par l'appel impatient de leurs parents respectifs à l'heure des corvées, et seuls quelques uns, dont je faisais partie, restèrent au cul du haquet reprenant sa tournée.**

— **Petit louis, veux-tu tenir la longe de Ptolémée ?**

**Je rougis imperceptiblement de l'honneur qui m'incombait et restait sans voix ; mon frère me gratifia d'un coup de coude, et pouffa sans méchanceté. Une grande complicité nous unissait, un peu virile, un peu nigaude, elle n'en fondait pas moins un amour fraternel inextricable.**

**La grande autruche se laissa mener, je sentais sa petite tête curieuse au dessus de mon épaule et pensait un instant à son puissant bec.**

— **Elle ne te pincera que si tu portes en ton cœur de mauvaises pensées, me confia le marchand **en m'adressant un clin d’œil pétillant de malice.**
— **Tu dormiras chez nous, vieil homme ? S'enquit mon frère, avec une effronterie qui me laissa béat.**
— **Les bonnes habitudes demeurent, Luth Louis, confirma le marchand de peaux de lapins en franchissant le seuil du chemin menant chez les Mounon. **

**Trois molosses s'approchèrent en grognant, reniflèrent Maude, qui fila se rencogner derrière mon frère, penaude, puis humèrent l'homme affublé d'une ample veste de daim surannée, d'un pantalon taillé dans la même matière fauve, aux effluves de terre, de mousse, de sueur et de fumée. Des mocassins d'un cuir couleur brou complétaient l'ensemble qui parut convaincre les cabots ; ils s'éloignèrent la queue entre les jambes, le museau rasant le sol, la mine servile.**

Les barguignages se répétèrent de plus belle dès l'apparition de Mme Mounon, grosse comme moitié d'une baleine à bosse de son prochain loupiot. Ses jacassements emplirent la cour jonchées d'objets hétéroclites, de détritus, auxquels seul sembla s'intéresser l'autruche. Des poules picoraient sans vergogne entre ses longues pattes grises, des jars cacardaient férocement, lui signifiant leur déplaisir à son intrusion en leur domaine. Un vrai cirque ! De nouveau, des gamins réapparurent, en plus de la nichée Mounon, tandis que la mère parvenait à ses fins, fourguant à l'indien des chiffons d'un abord répugnant, et des peaux, principalement de chevreuils, ragondins et de lapins ainsi que quelques chats peut-être en sus, bah, pour fabriquer le feutre, tout poil convenait !

Le marchand n'ouvrait point la bouche, se contentant de hocher la tête, et tâter chaque peau avec intérêt. L'affaire entendue, la femme Mounon lui tendit un pot de confiture, qu'il ne dédaigna point, et cueillit dans sa grande main brune. Celui-ci disparut rapidement dans un tiroir, au fond de la petite carriole. Couramment l'homme se voyait rétribué de petites oboles, hétéroclites, voire insolites, qu'il acceptait sans mot dire.

La fin de journée s’avançait, nous filâmes nous acquitter de nos tâches, nombreuses, en ce jour de repos, anticipant notre soirée. Ma mère sourit de nous voir nous activer comme jamais, à nourrir les cochons, remonter la paille derrière le cul de la vache, après un nettoyage en règle de sa litière. Luth et moi-même, accompagnés de Maude, infatigable, et de Mudine, tremblotante sur ses courtes jambes de bébé, rassemblâmes la volaille et vérifiâmes poulailler et étable fermés à double tour en prévention des méfaits de goupil (qui s'avérait parfois un animal sournois, à deux pattes, et se nommant Lacour, par dessus le marché ; mon père jurait souvent que cette famille de rapiats allait lui rendre des comptes, mais le dimanche suivant, les deux chefs de famille chassaient ensemble, partageant une gnôle lignifiante dans le petit matin frileux…)

Nous retrouvâmes le marchand ambulant devant la jolie maison des Marcouville. Près du couple de vieillards, s'affairait Lamy, la bonne du curé, maltraitant un large sac de toile plein ras la gueule de chiffes colorées. Mr Marcouville présentait de belles peaux de fouines et d'écureuils, admirablement séchées, d'un roux flamboyant, sur lesquelles le marchand, en connaisseur, passait sa paume caressante, faisant miroiter l’éclat et l'aspect soyeux du poil. Madame Marcouville, engoncée dans un grand châle mauve, gardait ses distances, la mine fermée, les mains dans son giron, triturant une petite croix en or. Les négociations perdurèrent jusqu'à l'invite du charron, Mr Ricoton, au coin du soufflet de forge, d'une chicorée si serrée qu'elle aurait fait grincer les dents d'une mule crevée. Le soleil rasait la plaine à présent. L'indien sirotait le liquide brun dans un grand mazagran de gré, laissant le charron débiter quelques commérages. Nous nous étions rassemblés, quelques gamins Lacour, Mounon, ainsi que les deux grands fils du charron, autour des hautes flammes orangées, chuchotant à voix basses.

Soudain, un grand fracas, venu de la rue, nous tira brusquement de nos papotages. Des gémissements furieux parvinrent à nos oreilles, des éclats de voix éclaboussèrent vivement l'air encore tiède de cette fin de journée tandis que nous nous élancions à la rencontre du charivari de la route. Mon frère, arrivé le premier sur les lieux, me saisit brutalement contre sa poitrine, et me tira violemment jusque notre sentier, bien que je me débattis comme un beau diable. Il ne céda pas, et j'eus beau faire, les bras puissants de mon père à leur tour m'emprisonnèrent et je pénétrai dans la maison, soufflant comme un damné, les larmes sur mes joues, donnant des coups de pieds, gesticulant comme un beau diable. La porte de la maison de referma. Mais pas assez vite.

Le tableau offert à mon regard quelques secondes ne me permettait pas de douter ; Ma gentille Maude gisait écrasée sous le charroi du fils Apati, qui comme à sa détestable habitude, fonçait toujours comme un cerf en rut sur la route avec son gros tombereau de fumier tiré par deux vigoureux mulets. Le museau orangé de ma chienne gisait dans une flaque vermeil, et son immobilité ne laissait place à aucune alternative. Ce grand nigaud d'Apati gesticulait, la voix grésillante comme une vieille radio, et les gens du bourg s’avançaient déjà, s'attroupaient, allaient bon train de leurs commentaires. Les femmes se signaient, serrant leur chapelet.

Mon père me déposa en douceur sur le plancher.

— Reste là.

Le ton me fit frissonner, il me calma instantanément. Je me tins à sangloter, affalé sur le plancher, tandis qu'il s'éloignait, enfonçant rageusement son chapeau sur sa tête.

Dehors, le charron ne perdit pas une minute, il poussa les bêtes de trait, aidé d'Enyeto et les fit reculer. Il dégagea alors le cadavre de la chienne de dessous le tombereau. Mon père se saisit de la triste dépouille, et revint sans un mot à la maison, après être passer déposer l'animal dans le bûcher.

Ma mère me passa un paletot sur les épaules, geste de grande sollicitude, que je n'appréciai guère sur l'instant, tout à ma peine. Maude et moi avions traversé l'enfance côte à côte. Ma douleur, immense, me soulevait par vagues déferlantes et me suffoquait. Je buvais la tasse de la souffrance à grandes goulées amères. Mon frère me couvait de l’œil sans oser intervenir. Mudine babillait dans son parc de bois, et tapait dans ses menottes boudines.

Un long moment passa ainsi. La nuit vint, la mère alimenta la cheminée de grandes bûches de sapin qui fleuraient bon la sève et crépitaient dans l'âtre. Le père tout près des flammes lisait « le Seuil » la feuille de chou qui nous rencardait des événements du pays. Il froissait le journal sans rien lire, jetant un œil sur ma pauvre figure d'un air désolé.

— Écoute mon grand, finit-il par dire, du bout des dents, l’œil humide, on l'enterrera demain, le long du rosier de ta mère, Y a rien qui pourra la déranger à cet endroit, d'accord ?
— D'accord.

Je fus incapable d'ajouter une seule syllabe.

On toqua à la porte sur ces entre-faits. Mon père reprit une voix d'homme et cria,

— Entre Enyeto !

Le marchand de peaux de lapins se faufila à l’intérieur, s'essuya les pieds sur la paillasse, à l'entrée, et rejoignit mon père, qui lui indiqua de la tête un tabouret, au coin du feu.

Il croisa mon regard éperdu, saisit l'assise de bois et tendit ses mains aux flammes.

— Les nuits sont encore fraîches, dit-il comme pour lui-même.
— As-tu remisé ta carriole et Ptolémée dans la grange ? L'interrogea mon père.
— Avec ta permission, oui.

Mon père se tourna vers Luth,

— Va donc mettre une mesure de grain à cette grande bringue, et de l'eau, et tu me boucles bien tout derrière toi !

Mon frère fila sans mot dire, faisant grincer la porte en sortant.

— As-tu déjà planté ton tipi, vieux sacripant ? Reprit mon père lui tendant un broc de tisane qu'il accepta bien volontiers.
— J'ai monté mon tipi après être passé chez les Apati.

Un silence pesant accueillit cette nouvelle.

— Je gage qu’aucun de mes petiots cette année ne se récrira pour passer la nuit sous tes hospices, Enyeto. Une triste soirée, je pense, une bien triste soirée. La vieille Maude nous manquera.

Chaque année en effet, depuis notre plus jeune âge, nous supplions nos parents pour rester avec l'indien, sous son tipi fait de cuirs tendus. Et chaque année, ils refusaient, arguant le mauvais temps, l'humidité, le dérangement, que sais-je encore ! Jamais nous n'avions pu approcher le marchand sous son abri ancestral. Dans nos lits, les pieds sur la brique chaude, nous l'entendions psalmodier en sourdine, à travers d'épais volets clos, des chants dans sa langue natale accompagnés de quelques instruments étranges, dont un remarquable hochet de cerf, à la peau tendue, cousue telle une bourse, et remplie de petits galets qui rendaient un son très particulier. La mélodie, lancinante, répétitive, résonnait au plus profond de la nuit et se répercutait sur les murs de pierres, s'incrustait sous les crânes où elle semblait se dissoudre, comme l'eau ruisselante d'une timide cascade. Mon père m'expliqua un jour que cet instrument servait lors de cérémonies funéraires.

La porte couina et mon frère apparut au coin, portant une nichée de chatons au creux de ses bras. Il me les tendit sans un mot. Je n'avais point envie de ces bêtes, mais sa tentative louable pour me consoler me toucha, et les bestioles rampèrent bientôt entre mes jambes, réalisant mille cabrioles. Je les regardais sans les voir, sur le vieux tapis, derrière la porte, quelqu'un manquait. Une large tête plate au regard doux, au poil brouillé par les vadrouilles, les pattes tendues vers le feu, manquait. Mes parents m'observaient à la dérobée, je sentais le poids de leur tristesse s'ajouter à la mienne et tentait, sans grand succès, de donner le change. D'être un homme.

Mon père reprit sa conversation avec son vieux complice.

— Il y a quelques peaux remisées dans la grange, lui dit-il sur le ton de la confidence.
— De belles peaux. Tu sais tendre et sécher la matière pour qu'elle ne se gâche pas, je les prendrai, si tu n'en demandes pas une caissette d'or, mon ami. L'indien hocha la tête d'un air entendu.
— Juste le prix qui convient. Il y a quelques chevrettes, et deux braquets, un chat sauvage aussi, notre région regorge de gibier. Parfois je me demande comment cela est possible, autant de bêtes, alors que nous prélevons tout au long des ans. Et les braconniers pullulen ! Mon père leva les yeux et les bras comme pour prendre à témoin le créateur en personne.
— Mais la nature est prolixe, elle profite dans vos terroirs de belles plaines encore vierges, de grands refuges, et elle se renouvelle vite.

…

Le reste de la conversation se perdit dans les limbes. Je ne repris conscience que dans mon lit, où je gisait recouvert d'une grande couverture de laine et d'un édredon de plumes. Au loin, une complainte sauvage berçait mon cœur inconsolable. Je fermais les yeux.

Au matin, tandis que je me préparai pour l'école, sans entrain, j'ouvris la porte, mon cartable sur l'épaule, et constatai le départ d'Enyeto. Plus trace de sa carriole, ni de l'autruche aux grandes plumes soyeuses. Je m’avançai sur la route, me préparant à cette morne journée quand un aboiement joyeux troua mon cœur en lambeaux. Je fis la sourde oreille, parce que les morts n'aboient pas, et que seul l'esprit entend leur voix, leur voix qui pleure ou qui rit. Pourtant les aboiements redoublèrent et une grosse boule de poils orangée se faufila dans mes jambes. Je tombai à terre, accueillant Maude dans mes bras, accueillant sa chaleur et son amour de chien.

C'est mon père, au jour de sa mort, bien des années plus tard, qui me confia la conclusion de l'histoire. Vous pouvez ne pas y croire, je n'y crois pas moi-même, mais j'accepte. Je choisis le loup blanc.

L'indien, assis en tailleur, devant sa hutte, alimentait un petit brasero de brindilles odorantes et fredonnait doucement, accompagné de son hochet. Il dessinait sur le sol dur, à la pointe de son vieux coutelas à gratter les peaux, des signes qui rappelaient un peu les hiéroglyphes égyptiens. Autour de lui, une bande d'écureuils se disputait une noix, un chat sauvage, aux yeux jaunes, perché sur la basse branche d'un vieux noyer, les observait sans animosité. En retrait, deux chevreuils au coin du tipi posaient sur la scène un regard craintif, et des lapins fauve, bruns, blancs, tachetés, s'égayaient un peu partout dans le jardin. Un craquement se fit entendre derrière lui, dans l'ombre. L'homme ne se retourna point. Il interrompit sa mélodie. Les bêtes se figèrent comme les santons d'un tableau de crèche au soir de Noël.

— Tu l'as apporté ? Murmura-t-il.
— Je l'ai avec moi, souffla l'autre, caché dans l'obscurité.
— Va t'en, commanda l'indien.

L’intrus déposa son fardeau, les bras tremblants et s'en fut.

Jamais je ne pourrai remercier mon père (et nonobstant ma mère, sa discrète complice) du cadeau qu'ils me firent cette nuit là, passant outre croyances, convictions, et tabous. Je ne sais si je dois haïr ou admirer la force qu'il fallut à mon père pour écorcher ma chienne encore tiède, et sa confiance, inconcevable, abyssale en l'indien.

Jamais je ne revis Enyeto.

\*\*\*\* c'est une histoire que j'ai lu. Elle ne m'appartient pas.

\*\*\*sacagou : mot inventé je te demande de le garder tel quel

\*\* agueuloir : mot inventé je te demande de le garder tel quel

\*Le **prénom Enyeto** signifie ‘*qui se promène comme un ours*’ en langue amérindienne.
