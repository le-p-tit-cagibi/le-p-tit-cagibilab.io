---
title: Fin de parcours
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "La mort vous fauche toujours quand vous ne vous y attendez pas. Et si en plus elle vous réservait une ou deux surprises, histoire de vous rendre l'expérience encore plus terrifiante ?"
epub: true
pdf: true
draft: false
---
