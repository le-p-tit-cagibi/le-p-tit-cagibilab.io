J'ai trouvé ce carnet de croquis à côté d'un enfant mort. Il affichait dans les douze ans, peut-être moins, plutôt grand pour son âge, une tignasse de jais. Ses yeux blancs fixaient un ciel limpide. J'ai pris ça pour un signe ; consigner ce qui arrive aujourd'hui, peut-être s'en souviendra-t-on demain. Sur le premier feuillet, une esquisse de loup gris, au repos, le nez sur la queue, un jour de grand froid.

– un mauvais jour – Comme aujourd'hui, un jour qui n'en finit pas.

Je n'ai croisé personne encore, personne de vivant. Hier, le monde était celui que l'on connaît, ses bruits, son air empuanti, ces gens s'agitant partout, en voiture, au bureau, quelque part, nulle part. Ce matin, rien. Les rues, vides d'hommes, offrent un nouveau terrain de jeu à nos hôtes d'habitude discrets ; les mésanges chantent à tue-tête, un chat somnole dans la cour. Une pie danse sur le macadam, le soleil éclabousse son habit noir et blanc d'une moire irréelle.

Comme je disais, mon premier cadavre m'attendait au tournant, si je puis me permettre ce mauvais jeu de mot. Je m'étais réveillé dans mon hamac, les doigts encore serrés autour d'une bouteille de vodka, vide, bien entendu, j'avais passé la nuit à la belle étoile. J'ai eu un peu de mal à reprendre mes esprits, un merle s'entêtait à me vriller le cerveau avec son chant de bienvenu… J'ai fini par retrouver un certain équilibre, suffisant pour faire quelques pas, et je l'ai vu, ou plutôt, il m'a regardé, assis dos au mur. J'ai reconnu mon voisin, il me fixait droit dans les yeux. Des yeux qui ne voyaient plus rien. Une raideur cadavérique le rendait plus vrai que nature, la bouche tombante, l'air déçu. On l'aurait été à moins. Plus mort que les bûches sous mon avant-toit. Une fourmi lui courait sur la joue. Stupidement, j'attendais ce geste, celui qui chasse les insectes qu'on a sur la joue, celui qui nous fait vivant ; mais celui-là n'appartenait plus à ce monde. L'alcool a un effet anesthésiant, je n'ai pas crié, je n'ai pas appelé, je n'ai pas réfléchi en somme, et je me suis avancé encore un peu, dans la rue. La gamine au carnet m'attendait. Je l'ai détaillé comme on détaille une part de pizza, voir si tout y était ; il ne manquait rien, sauf la vie. J'ai choppé le cahier, elle a penché dangereusement vers l'avant, ses petites mains violacées tendues toutes flasques sont retombées le long de son corps mou, et là, là seulement, ça a fait tilt dans ma putain de tête. Mon bide a joué au yo-yo une minute, avec tout ce qu'il y avait dedans, et puis j'ai dégobillé mes tripes. La fille regardait par terre à présent. Mon corps a tremblé, j'ai opéré un demi-tour lent, et abasourdi, me suis réfugié chez moi en courant. Mes pas résonnaient étrangement, je les entendais pour la première fois dans ce silence trop grand, trop intense, inhumain, qui ne m'était pas familier. J'ai verrouillé la porte. Et puis j'ai regretté de ne pas avoir fermé les volets. J’étais incapable de ressortir. Je me suis posté devant la fenêtre, affalé dans mon vieux rocking-chair, et j'ai tendu l'oreille, écarquillé les yeux aussi. Les deux mains serrées, à m'en tordre les doigts, histoire de sentir quelque chose. Dehors, il ne se passait rien. Dans la ruelle, rien. Dans la cour, rien. Dans la rue, aucun mouvement ni hommes, ni machines, forcement, car finalement, la machine n'est qu'un substitut du vivant, comme un regret, une aspiration. Une chimère grotesque. Au-dehors, rien que des tourterelles, se relayant sur leur nid, ainsi que des moineaux piaillant et sautillant dans les branches des vieux sumacs, devant la maison, de fichus oiseaux. Un lézard sur le mur retint mon attention tandis que d'autres sûrement ont échappé à mon regard humain, lent, et si peu clairvoyant.

J'ai saisi mon téléphone machinalement ; l'écran ne s'est même pas allumé. Je me suis retourné vers la télévision. J'étais tétanisé. Ma main avait triplé de poids et de volume, et me saisir de la télécommande me sembla un réel défi ; j'y renonçai la boule au ventre, non pas une boule, un rocher. Je continuai mon observation du dehors, silencieux, comme le dedans, mais indéniablement moins rassurant. Une légère bise de printemps taquinait les arbustes aux feuilles immatures. Les rayons du soleil, à l'aplomb de midi dessinaient une cage dorée entre les mailles du grillage séparant ma cour du… reste du monde ? Je fermai les yeux. Une pensée me vint, violente, insoutenable ; ma maison est mon tombeau. J'en dégringolai à terre, mes jambes flageolantes ne soutenant plus ce corps privé du bruit des autres. Quand je nageais, dans le monde d'avant, l'apnée me procurait ce sentiment de calme indicible. Décidément, je ne savais pas ce que c'était ! Sous l'eau, les sons bien que différents heurtent violemment les oreilles alors qu'à cet instant, ici, rien, rien hormis les macchabées du bout de la rue. J'avais fermé la fenêtre sans m'en rendre compte, certainement avant de tomber, mais je n'en conservais aucun souvenir. Je touchai mes jambes, rassuré de sentir leur existence propre. Je restai ainsi, immobile, à contempler, allongée de guingois sur le plancher, une tranche de ciel, sans rien à écouter, ni à entendre que ces foutus oiseaux. Ils me tapaient sur les nerfs. Pour un peu, j'aurai supposé qu'ils se foutaient de ma gueule, dans le genre, « hey, mec, on t'a bien eu, y a plus personne, plus personne ! » Mon sang parcourut douloureusement mes veines et artères, ma tête se mit à ressembler à une horloge, sauf qu'au tic tac joyeux se substitua un poum poum solennel ; le sang coulait, indubitablement, au dedans de moi. J'étais vivant – pourquoi moi – Je dessinai un O avec mes lèvres, hésitai, et renonçai à prononcer quoi que ce soit. Ne pas déranger le silence. Cela me semblait indécent. Je refermai ma bouche, et même respirer me parut trop bruyant.

J'ai dû perdre connaissance, et comme je ne la possédais pas avant, elle ne m'est pas venue après. Méchante ironie.

J'ouvre les yeux, le plancher sous moi, décidément, je suis infoutu de tenir debout. Je suis conscient et quelqu'un m'observe. Il faut que je me retourne pour voir l’intrus, merde, j'ai même pas une matraque, ou n'importe quoi pour me défendre. Je n'ose pas bouger, ça va alerter l'autre, autant que je le surprenne, s'il est agressif. Parce que les morts là dehors ne sont pas morts tout seuls. Je roule les yeux dans les orbites, j’aperçois le bout d'une pantoufle. Ah ouais, Liam, ça va le faire, comme arme défensive, attaque avec le talon, pif paf, vous êtes fichu monsieur le méchant ! Conneries ! Tant pis, je vais me lever d'un coup en braillant, au moins, il y aura un effet de surprise, ça mettra peut-être l'autre en déroute. De toute façon j'en peux plus, je m’ankylose déjà. Allez, courage mon pote. Et résolu comme jamais, je ne cogite pas plus avant de me dresser sur mes jambes en hurlant,
Banzai ! Le truc le plus débile qui soit, mais quand on a peur, je vous le garantis, on a peur, et on ne cherche pas à faire de la grande littérature.

L'autre n'a pas bougé, elle me regarde tandis que je reste sans voix en découvrant son identité.
La gamine de tout à l'heure, celle à qui j'ai piqué le carnet… Mince j'aurai pas dû…
Je me morigène devant tant de pensées ridicules. Putain, elle était morte !
Elle, elle me dévisage toujours, les orbites habitées de deux billes de nacre, le teint peut-être plus jaune que la dernière fois que je l'ai observé. Je ne peux m'en empêcher, je parle.

— Qu'est-ce que tu fous là ? J'ai aboyé. Lamentable. Ma voix rend bizarrement, comme un écho intérieur.

Comme je n'obtiens pas de réponse et que la gamine n'est pas dangereuse, supposai-je, je reprends du poil de la bête, et c'est moi qui deviens limite agressif.

— T'as entendu ? Je t'ai vu dans la cour, t'étais mal en point pour tout dire, alors qu'est-ce que tu fais là ?

Le macchabée tend son index, et opte pour une moue, je dirais mi-moqueuse, mi-commisérative, c'est difficile à analyser, un visage, quand il ne s'y ouvre plus le regard. J'ajoute, en braillant comme un âne,

— T'es morte bordel !

— Ben toi aussi.

Elle a parlé. Très étrange sensation de s'être fait entuber quelque part sans savoir ni le où, ni le comment.

— Comment ça moi aussi ? Je t'ai vu de mes yeux vu, assise dehors, raide comme un passe-lacet y a… y a je sais pas exactement combien de temps.

— T'étais bourré. Je t'ai aperçu juste avant de clamser, tu ronflais dans ton hamac. J'ai appelé. T'as même pas bouger.

— Mais qu'est-ce que tu racontes, moucheronne ? Je suis pas mort. Le voisin, lui par contre…

— Ils l'ont emporté pendant que t'étais dans les vaps, juste avant que tu claques. Ils ne t'ont pas repéré, avec la végétation. Ils ne discernent pas tout, mais ils sont drôlement malins quand même.

— Hein ? Qui ça « ils » ? je dois avoir la même expression qu'une poule qui se retrouve face à un mégot.

— Je vais te montrer.

— Ah non, bon sang, je vais nulle part avec un cadavre.

— Tu l'es tout autant ! Sa voix gronde. Pas une voix de petite fille ça.

Dressé sur mes deux pattes, je m'ausculte, je me tâte. Elle me fiche les foies cette gosse.

— T'es mort et c'est facile de le savoir, monsieur le braillard. T'as plus d'ombre, et si tu vas jusqu'à ta salle de bain, te regarder dans le miroir, tu constateras que t'as les yeux comme moi. Des yeux de poisson mort. Voilà !

Elle trépigne, tape du pied à présent, pas contente la môme. Je deviens moi-même un peu hystérique, la moutarde me monte au nez, c'est quoi l'embrouille ? Je me mets à gueuler.

— Mais c'est quoi ce vieux canular pourri ? Qui est l'abruti qui m'a joué ça ? Hé ho ! C'est pas mon anniversaire. Remballez le maquillage, sortez de vos trous les connards !

Je tourne sur moi-même, grand-guignolesque à souhait, dans l'espoir de découvrir mes potes planqués quelque part en train de se bidonner comme des nazes. Mais le silence qui accueille mes frasques à vite fait de me calmer. La gamine a attendu que la crise passe, pas du tout impressionnée. Elle ne m'en tient pas rigueur, elle ne se moque pas non plus. On dirait une grande personne camouflée dans un corps de gosse. Je devrais m'en méfier, elle n'est pas normale. Assurément, puisqu'elle est morte…
Elle me prend par la main, le geste est purement factuel, je ne sens rien, ni le poids de sa menotte dans la mienne, ni la contraction de ses muscles ou des miens. Je prends lentement conscience de ce qu'elle baragouine depuis quelques minutes ; je suis mort.

Nous nous éloignons de la maison, elle, me tractant par cette main qui n'est rien de plus qu'une pince froide au bout d'un manche tout aussi froid. Et moi, en plein questionnement métaphysique ; Bon sang de soir, qu'est-ce-que je suis devenu ? C'est ça être mort ? Je n'ai pas osé jeter un œil sur la carcasse que j'abandonnai dans le salon. La nostalgie n'est pas de rigueur quand on est aux abois. Surtout quand on vient de rendre son dernier soupir.

— Viens, je vais te faire voir, mais attention ! Ils peuvent nous attraper !

— Ils ? Voilà quelque chose qui a l'air de la terrifier.

— Qui…

— Chut !

Le ton est péremptoire, bon, je vais devoir attendre pour les explications.

Dans la rue, la luminosité nous frappe comme une gifle qu'on attendait pas. Toujours ce même calme dans ce paysage ordinaire, aucun bruit, ce silence, épitaphe de l'inactivité des hommes me vrille les nerfs. Je suis un mort nerveux, voilà. Elle, elle n'a pas l'air d'avoir d'état d'âme, elle se faufile dans la première ruelle s'offrant à notre vue. Il y en a quelques-unes ici, assez discrètes, il faut connaître la topographie de l'endroit pour les dénicher et se repérer une fois que l'on baguenaude dans ces boyaux étroits, à l'abri de la végétation, dissimulant venelles entre deux toits, goulets étranglés par des murs épais, parfois agrémentés d'escaliers bancals aux marches de pierres mal taillées. Certains endroits sont à demi effondrés ; ces lieux datent d'avant-guerre, les ronces ont pris leur aise, les orties règnent en maîtresses revanchardes. La gamine n'en a cure, elle trottine, sûre de sa destination, déterminée, placide. Je renonce à poser des questions, de toute façon, depuis mon réveil, pardon, depuis ma mort, il me semble que je pose des questions encore plus connes que lorsque j'étais vivant… un effet pervers de cet état ?

Nous arrivons face à la place de la mairie, la ruelle débouche sur un puits très pittoresque doté d'une haute margelle qui nous cache à la vue. La gamine s'est accroupie, et moi aussi, par mimétisme, puisqu'il ne peut s'agir de l'instinct de conservation, si ? Quand on est mort, est-ce qu'…

— Regarde, ils sont là ! Ne fais pas un geste !

Son ton est si emprunt de frayeur que je me fige, plus cadavre que nature !

Deux hommes en costumes trois pièces noires, la mine pas commode, se tiennent devant la large porte cintrée de la maison communale. Chacun d'eux parle dans son téléphone respectif,

— Donnez-nous le décompte actuel, dit l'un, un grand brun barbu.

— Affichez les secteurs nettoyés, réclame l'autre, une véritable armoire normande d'un roux flamboyant.

Ils portent tous deux des lunettes noires, des chaussures vernies – tout à fait flippant ces types –
Au bout de la place, en haut d'une rue, déboule une jeune femme, en nuisette transparente. Dans une autre situation, il aurait été tentant de se rincer l’œil, mais je suis à cent lieues de penser à la bagatelle, enfin pas tout de même cent, cinquante peut-être… Ah oui, les charognes peuvent-elles baiser ? Encore une question stupide. Si accéder au paradis dépend de mon intelligence dans l'au-delà, je suis cuit ! La gamine me balance un coup dans les côtes, et ça fait mal, mort ou pas. Comment elle a fait ça ?

Le grand type à la barbe marmonne encore dans le combiné mais, je ne saisis pas le sens de ce qu'il dégoise. Son collègue le rouquin, imperturbable, tape sur l'écran de son smartphone, et finit quand même par indiquer de la tête la rue qui monte vers la boulangerie, avec la fille qui gesticule, comme un pantin de bois. La route en question passe juste devant nous, à quelques pas de nous. C'est le moment de faire le mort.

Avec un bel ensemble, les mecs ont dégainé une sorte de mini raquette de Jokari, assez folklorique, et si le moment n'était pas si dramatique, j'aurai pu piquer un heureux fou rire, tellement ils ont l'air barrés ces deux grosses bûches, à galoper derrière la gazelle rose. Ils se rapprochent rapidement d'elle, un petit bip sonore, et voilà la raquette qui s'illumine, avec, tout autour, comme un filet de lumière bleutée, qui semble flexible, se mouvant aux rythmes des pas des deux lascars. La fille à moitié à poil a l'air trop désorientée pour réagir, le soleil nappe sa magnifique chevelure blonde, tel une aura, c'est beau, mais quand je baisse les yeux, sur le macadam, je n'y vois pas d'ombre, merde, c'est une trépassée, comme nous ! Les deux mecs n'ont pas chômé, ils sont quasiment à sa hauteur lorsqu'elle tente de s'échapper. Poil de carotte joue de la raquette, la lumière bleue atteint la fille dans le dos, et tel une fumée dans un aspirateur, la voilà qui disparaît, absorbée par la raquette ! J'en reste comme deux ronds de flan. Je chuchote,

— Mais qu…

— Ta gueule ! Pas polie cette gosse. Elle n'a pas froncé un sourcil à l'effacement pur et simple de notre concitoyenne.

Les deux brutes remballent leurs joujoux et poursuivent l'ascension de la rue, peut-être vont-ils prendre une café-croissant chez Margotte, elle est lunatique la boulangère, mais sa viennoiserie, y a pas au-dessus… Je passe mes doigts dans mes cheveux. Mais qu'est-ce-que je suis en train de déblatérer ? Un vrai moulin à paroles intérieur…

La gamine ne perd pas son temps, elle. La voilà qui bondit de sa cachette pour se rencogner dans le boyau suivant, le long de la mairie dès que les deux affreux et leurs raquettes à absorber les macchabées ne sont plus visibles. Elle daigne enfin aligner trois mots quand je la rejoins fonçant tête baissée, sous un lierre centenaire, qui grouille d'insectes bedonnants, scarabées, gendarmes, sauterelles, et j'en passe… Détestant la cohabitation, je décide d'avancer un peu, tout en l'écoutant attentivement, puisqu'elle paraît prête à se confier un peu. Elle est immobile, sous le feuillage, je la distingue à peine, sa voix s'étiole, je crois, je tends l'oreille. Je me demande si c'est pas encore une connerie, ça, tendre l'oreille quand on est refroidi… La gosse interrompt le flot de pensées ridicules qui m'assaillent. Une bénédiction.

— Ça a commencé hier, la sirène de l'usine s'est déclenchée…

— Tu veux dire l'alarme ?

J'ai pas pu m'empêcher de la couper dans son récit, ça me paraissait tellement improbable, trente ans que l'usine tourne à plein régime, sans une seule anicroche…

— Tu me laisses raconter, grogne-t-elle, un filet de voix retrouvé, ou il faudra que tu te débrouilles tout seul, et vu comme t'étais bourré, ça m'étonnerait…

— Ok, ok, raconte !

Bon, merde, je l'ai interrompu de nouveau, pourtant j'essaie de faire preuve de bonne volonté et cette fois, elle se permet une grimace tout à fait effrayante, à me montrer des dents drôlement mal ficelées dans cette bouche, tandis que ses yeux blancs s’écarquillent plus que ne l'autorise la physiologie humaine, en tout cas, à ce que j'en sais. Mais bon, quand on est calanché, on devient peut-être élastique. On devient n'importe quoi, pourquoi pas ? En tout cas, elle est bien moche comme ça, et surtout, elle a gagné ; je la ferme immédiatement.

— Les gens sont sortis, l'usine était déjà en mode évacuation.

J'ai pas commenté, je jure, pourtant elle a répondu à la question qui me taraudait.

— Mon père était pompier à l'usine. Il a été appelé en urgence. Pas longtemps après, des types bizarres comme ces deux-là ont barricadé le village, et depuis, c'est la chasse.

Là, j'ai récidivé, bon sang d'un petit bonhomme, j'ai pas pu me la fermer,

— La chasse de quoi ?

— Putain, t'es vraiment plus branque qu'une bite.

Dans la bouche d'une gamine, ce langage me choque, et je me suis retenu de lui mettre une baffe, je crois qu'elle l'aurait mérité, même si elle n'aurait probablement rien senti, au moins pour le principe, et ceci autant pour la tragédie qu'elle me déballait depuis notre rencontre fortuite, que pour ses grossièretés d'ailleurs. J'ai pris le parti de faire comme si je n'avais rien entendu.

— Comme ça l'usine a sauté ? Parce que, si la sirène s'est déclenchée, y a pas deux poids deux mesures, c'est alerte explosion, raisonnai-je à voix haute. Les habitants de ce patelin ont eu droit à une réunion d'information tous les trois ans, depuis l'implantation du complexe, alors j'en sais quelque chose ! Enfin, quand je dis explosion, je devrais plutôt dire fuite, de toute manière, le problème n'est pas là, c'est la combustion des matières toxiques, le gros, gros hic, ce qui s'échappe dans l'air quoi, ces trucs avec des noms compliqués, ces substances qu'on ne voit pas, mais qui font leur travail de sape, en deux temps, trois mouvements, un sacré putain de problème je dirais. Ma voix est partie dans les aigus. Quel foireux je fais.

— Je ne sais pas ce qui s'est répandu. Ça sentait rien, il y a eu un petit incendie, trois fois rien, et de la fumée blanche, rien d'alarmant. Elle parle comme si elle récitait une leçon. La dernière, à voir où nous en sommes arrivés tous les deux. Je passe machinalement ma main dans mes cheveux, c'est comme passer sa main sur un vieux paillasson. Dégueulasse. La lippe emplie de dégoût que j'affiche alors a dû accélérer la réaction, mais bon, même les morts craquent.

La gamine me fixe et des larmes se sont mises à couler des deux trous opaques qui remplacent ses yeux. Les morts peuvent pleurer ! en voilà une nouvelle. Et elle pleure comme une grande, cette môme, dans la retenue, j'en suis presque ému. D'un coup, peut-être à cause de la gosse qu chiale ou peut-être pas, je réalise que tout le monde est mort ici et tout autour de l'usine probablement. C'est la réalité. Une fin du monde à échelle réduite. Ma fin du monde. Un agent chimique… qui se répand dans l'air… ça ne laisse pas grande chance aux autochtones de s'en sortir… Une autre pensée idiote affleura : Je ne me souvenais même plus où j'avais pu ranger ce foutu masque qu'on nous avait distribué, il y a si longtemps. J'ai protesté pour la forme à ce scénario catastrophe.

— Mais on voit pas de secours ! Il n'y a pas d'ambulance, pas de pompiers, ou de samu, enfin tu vois ce que je veux dire…

Elle me regarde en dessous, comme si j'étais demeuré. No comment. Je me rends compte que ce n'est plus vraiment nécessaire les secours, c'est pas comme s'il y avait urgence vitale. Je rectifie le tir, maladroitement, j'en conviens.

— Enfin, il n'y a pas de corbillard, les pompes funèbres, rien quoi !

— Si, tu vas voir, il y a un lieu de stockage des corps. Cette manie qu'elle a de me clouer le bec. Elle renifle de manière répugnante. Je fais abstraction de la morve qui tapisse le bas de son visage.

— Des corps ? Comment ça ?

— On meurt, quoi qu'il en soit, daigne expliquer la gamine, notre enveloppe charnelle on l'abandonne, mais eux la ramasse.

Évident. J'aurai pu y songer tout seul. J'ai envie de la frapper. Faisant abstraction de mes pulsions, décidément bien débridées pour un mort, je demande,

— Ils nous pourchassent les men in black ? Ceux qui sont ressuscités comme nous, ils les traquent ?

— Tout le monde est ressuscité.

Le ton est emphatique, totalement inapproprié à une gamine de cet âge, elle n'a pas l'air de s'en rendre compte, elle poursuit,

— Peut-être que Dieu a décidé qu'on méritait une seconde chance, ma grand-mère disait ça en parlant des chats, qu'ils avaient sept vies que leur accordait le seigneur. Alors pourquoi pas une seconde pour nous autres les hommes ?

Sa voix grave me fait froid dans le dos, et tant pis si ça ne veut plus rien dire. Même les morts ont la trouille. J'ordonne,

— Montre-moi !

Ma propre voix me fout les jetons. J'ai changé, y a pas à dire, avant j'étais un type sympa : je suis un macchabée détestable. On s'est remis en route. Nous prenons la venelle suivante en direction de la rivière, puis une autre, nous débouchons sur une sorte de prémontoire rocheux dissimulé sous des pans de végétation. De là, nous avons une perspective dégagée sur tout ce chambardement: Eclairant mais effroyablement angoissant. 
Effectivement, il y a de l'agitation dans ce coin, et un de ces boucans aussi ! Un énorme camion est stationné près du pont, à l'entrée du village et le secteur parait barricadé. C'est à ce moment-là que je me suis rendu compte de la présence des barbelés, disposés tout autour du bled. Pour ce que j'en aperçois, des blocs de béton haut comme des murs sont en train d'être déchargés et montés à l'aide d'une grue. Sur un des blocs, on lit clairement, même à cette distance, « propriété privé, usine YAMO, zone strictement réservée au personnel YAMO »

— Ils déploient les grands moyens ! éructai-je sous le choc, constatant qu'à quelques mètres de ces constructions nouvelles, gisent des dizaines de boites en métal plombées ressemblant fort à des cercueils.

La gamine acquiesce.

— Ils veulent tout faire disparaître. Pas de mauvaise publicité, pas de journaliste, t'as vu, ils ont même coupé internet !

J'aurai bien pris ma tête dans mes mains, et pleuré un bon coup, mais à quoi bon ? Être mort rend un peu cynique j'ai l'impression… je me suis demandé à haute voix,

— Combien reste-t-il de gens dans le patelin ?

— Vu le nombre de boites déjà fermées, très peu, moins d'une demi-douzaine, me répond la gamine.

— Tu as vu leurs dépouilles ? ou après qu'ils sont morts, tu as croisés d'autres… d'autres… je ne trouve pas de qualificatif qui nous corresponde, j'opte pour celui-là, « d'autres revenants…? » Fantômes m'était venu à l'esprit en première intention, mais ça faisait très romantique et on est loin de Lady Chatterley là.

— Ni l'un ni l'autre… Tu as été le seul mort …vivant que j'ai croisé jusqu'ici.

On peut dire ça. Je la regarde pourtant comme si elle venait de prononcé (encore) une grossièreté. J'ai tellement vu de films de zombies. Etre la vedette du long métrage de ma propre mort ne me rend pas spécialement euphorique.

— Bon, on fait quoi ? On se barre de ce patelin ? l'urgence de la situation me prend aux tripes, même si mes tripes se délitent.

Et là, j'ai carrément pété un plomb. La fillette me détaille de ses orbites creuses avec un dégoût qui me fout la gerbe en deux secondes à peine. Je suis prêt à abandonner mes semblables à leur triste sort sans le moindre regret. Quel connard je fais ! je mériterai même pas un enterrement si un jour l'occaz se présentait : brûlez-moi ce naze, par pitié, pas de paradis pour cet enculé. Je suis devenu un monstre. Égocentrique en plus. Mais imperturbable, elle décide d'ignorer ma dernière lâcheté et suis son propre chemin, tordu peut-être, mais elle a un certain courage, surtout pour une gosse morte.

— On retrouve les autres, peut-être qu'on pourra en prendre un ou deux avec nous…

— Sauver des morts ? Purée de merde en boite, pourquoi faire ? On sait déjà pas ce qu'on va bien pouvoir foutre là dehors, alors on va pas s'encombrer…

— Je me suis bien encombré de toi, et t'es vraiment un cas.

Belle pique. Sûr que sauver un alcoolo à la dérive, elle doit le regretter un peu la mouflette. Mais est-ce que finir dans la raquette de Monsieur Muscle est si terrible que ça ? La vision de la fille en rose, aspirée comme un vulgaire détritus, m'aide singulièrement à prendre ma décision.

— Ok, on fait comme t'as dit ; le tour du patelin par les traverses, mais si ça devient trop chaud pour nous, on met les voiles.

— En dernier recours, on met les voiles. D'accord.

Tellement sérieuse. J'ai l'impression d'entendre parler mon institutrice. Ce ton solennel, trop adulte, je n'arrive pas à m'y habituer.

Nous nous sommes mis à parcourir le patelin, cent-vingt âmes en temps ordinaire, sans mauvais jeu de mots. A deux reprises, nous avons croisé les deux grands costauds, heureusement d'assez loin pour éviter les embrouilles, ainsi qu'un autre couple vêtus de la même manière, version féminine. Les deux agents en jupette ont pourchassé une grande bringue en robe de chambre, mais les raquettes sont entrées en action avant qu'on ait la moindre chance de la rattraper, encore moins de l'aider. Le rayon bleu l'a touché dans les jambes, et l'a goulûment avalé, ni vu, ni connu. Plus loin, au-delà d'un muret longeant une venelle que nous traversions au pas de course (et ce qui est bien, quand on y réfléchit cinq minutes, c'est que quand la mort vous fauche de sa grande faux bien aiguisée, vous pouvez courir à perdre haleine, ben voyons, et ça, c'est à la fois très drôle et très flippant. Le souvenir vous taraude que le palpitant a besoin de se reposer de temps en temps, mais ce n'est qu'une idée en l'air, un phénomène de déjà-vu, si vous voulez, qui n'a plus lieu d'être, les morts sont champions du monde de course à pied). Et donc nous suivions cette ruelle engoncée entre deux murets pierreux, et je dois dire que madame Chance s'est invité en pique-assiette bienvenu : un bruit de pas nous est parvenu de nulle part, on a plongé à terre s'avisant que juste au-dessus de nos caboches, un vieux type barbu en salopette tentait de sauter au-dessus du vide ! plus mort que mort, mais souple comme un serpent, agile comme une biche, sauf que pouf ! lumière bleue, et plus personne. On en menait pas large, la tronche enfouie dans la mousse du chemin. Mais on n'a pas été repéré.

Ensuite, après une nouvelle galopade, nous nous sommes retrouvés au niveau de l'ancien lavoir, abandonné depuis longtemps, la rivière ne l'alimente plus depuis belle lurette. À nouveau, la sensation d'être fatigué, essoufflé, mais c'était totalement superflu ; ça m'a presque manqué. Gros coup de blues. La môme aussi avait l'air secoué, maladif, même pour une morte, elle a viré du jaune au vert. Décomposée. Je me demandai bien la dégaine que je pouvais avoir, faudrait…

— On devrait partir, qu'elle a dit, sa voix virait contralto. Une voix de vaincue. Ca présageait rien de bon, si mon mentor en culotte courte lâchait l'affaire, et puis, c'était moi le mesquin, le trouillard qui voulait sauver sa peau…

— T'es sûre ? je surjouai.

— Tu as vu tous ces cercueils plombés ? Et ces tarés en noir ? On va finir dans la raquette, si on reste ici, on se fera avaler.

— Nous mourrons une seconde fois ! j'en rajoutai des tonnes. Quel con. La fille se liquéfiait, si c'est possible, de vert, on virait au mauve.

— Très drôle ! Maugréa-t-elle.

Bizarre, elle n'eut pas l'idée de me refaire le coup du monstre grinçant des dents et qui se prend pour un ruby cube.

— Ça n'a pas vocation d'être drôle, ma petite, mais comme je ne me suis pas vu mourir la première fois, je n'ai pas l'impression d'une redite.

— Tu m'énerves à réfléchir alors qu'il s'agit de sauver sa peau !

Alors là ! elle me soufflait ; dire que tout à l'heure, elle envisageait de sauver le monde, enfin, ceux qui restaient à sauver de ce bled maudit, et maintenant, elle virait sa cutie sans aucune gêne…

— Plus son âme que sa peau, la tienne part en lambeau, petite.

— Tu t'es pas reluqué vieux débris ! Elle a craché ces derniers mots comme une glaire restée trop longtemps coincée au fond de sa gorge en décomposition.

Eh bien voilà, elle a l'air en colère maintenant. Sa bouche s'élargit par spasmes, elle perd ses cheveux par poignées, sacredieu quelle horreur ambulante. Mais c'est bien vrai que je ne me suis pas regardé, et je me refuse même tout bonnement à l'idée !

J'ai le cul calé sur le rebord du bassin tandis que je déblatère en sourdine, les pieds ballants. Une posture d'une grande vitalité, en somme. Mes entrailles jouent un match de boxe avec mes organes décomposés, j'émets un renvoi puant. Incroyable, j'ai faim. Comment c'est possible ? Grimpée sur un saturne en pierre sans queue ni tête, la gosse ergote encore, la voix rauque, au bord de la crise de nerfs. Peut-être que est-ce le premier signe de l'effondrement prochain de cette seconde vie ? tristesse, l'abattement, une charogne peut-elle avoir les sentiments et états d'âmes d'un vivant ? ou est-ce juste l'ironique réminiscence d'un passé encore tout frais ?

Ma tête bourdonne. Un silence soudain nappe l'endroit comme une gelée gourmande sur une tarte aux framboises, deux pauvres framboises rabougries.

Je m'aperçois que je suis au bord du délire. Il faut agir ! planifier quelque chose, on ne peut pas s'éterniser ici, les gugusses en costard vont forcément nous mettre le grappin dessus. Je me lance.

— On pourrait alerter les médias sur ce qui se trame ici… tu te rends compte que, d'ici quelques jours, l'usine aura absorbé le village, et ce terrible accident n'aura jamais existé.

— Ce n'était pas un accident.

J'ai beaucoup de mal à l'entendre. Elle devient aphone, cette gamine.

— Comment ça, pas un accident ? Qu’est-ce que tu en sais ?

— J'y étais.

Je l'ai choppé sur son perchoir, elle aurait pu se débattre, mais elle ne l'a pas fait, c'est d'un loufoque, j'agrippai cette créature éthérée qui avait soudain pris de la consistance, un peu comme si ma volonté, ou ma colère, ou les deux, l'avait d'un coup rendu solide, un gaz passé à l'état solide. J'hallucinai probablement. J'ai tiré sur son bras, sa peau gélatineuse me dégoûtait, ses yeux crevés me dégoûtaient, mais il fallait à présent qu'elle crache ce qu'elle avait à dire, et vite, les macchabées n'ont aucune patience. Ils l'ont certainement éclusée dans leur vie d'avant, j'en sais que dalle, mais l'envie me taraudait de lui foutre sur la gueule, parle, parle, saleté, parle ! Ma tête me tambourinait pis que les cloches sonnant l'angelus. À sa décharge, elle n'a pas paniqué, juste couiné quand sa tête a tapé la margelle du lavoir, et encore, ce fut un petit couinement de souris blanche.

— Alors c'est quoi la soupe que tu vas me servir à présent ? chuchotai-je rageur, n'oubliant pas que nos poursuivants peuvent être tout près. Je la secoue sans y faire attention, par principe, peut-être, parce qu'une gosse mal élevée, il faut bien la remettre dans le droit chemin non ?

— J'y étais à l'usine, crache-t-elle dans un soupir. « Je t'ai dit que mon père était pompier ? Parfois, il m'emmenait avec lui, parce que maman travaille le week-end, la nuit, à l’hôpital des vieux, à la ville, alors il n'aime pas que je reste seule dormir à la maison. Il m’emmenait et je dormais dans les vestiaires des agents de service, il est pas occupé. »

— Mais c'est un vrai inconscient ton con de père !

— Sois respectueux envers les morts ! Tas d'os imbibé d'alcool ! Sa voix chuinte comme un vieux robinet qui fuit. Mes oreilles en ont frémi. Ses dents pendent de guinguois à l'extérieur de sa mâchoire, une mâchoire de requin.

Elle se débat brutalement, et je la lâche, son contact m'est tellement répugnant – à moins qu'elle ne m'ait tout bonnement glissé entre les mains –

— Soit, mais c'était pas bien malin, non ?

— Non. Il m'aimait, il a pas pensé à mal. L'autre nuit…

— Quelle autre nuit ?

— La nuit de la sirène.

Sa bouche s'est tordue, indicible souffrance ou remord indicible ? me suis-je pensé, la regardant s'empêtrer dans une explication frisant le délire, son visage déformé partant dans tous les sens.

— Accouche bordel ! Qu’est-ce que t'as fait ? j'ai haussé le ton malgré moi ; elle met son doigt sur sa bouche, m'intimant la prudence ; les chasseurs, attention aux chasseurs à raquette.

— Je me suis réveillée cette nuit-là, dans le vestiaire, j'avais envie de pisser, je suis descendue par un escalier qui indiquait des toilettes, mais j'ai dû me tromper, et, et…

J'ai senti que j'allais avoir le fin mot de l'histoire, alors mon attitude ne fut peut-être pas cordiale, mais qu'en avais-je à battre ? Dans ma situation de mort en sursis d'un devenir mystérieux, ou de pas de devenir du tout, seul m'importait de trouver un responsable à cette situation pourrie. Pas que ma vie d'avant était extraordinaire, mais c'était quand même une petite vie peinarde. Je me jetai en avant et lui appliquai un coup de poing en plein plexus, la sensation de frapper dans un pudding, écoeurant. Et en même temps mes tripes me taraudèrent encore, alors je frappai une fois encore. Elle s'effondra sans un mot. Jusqu'à son visage qui affichait un teint violacé foncé maintenant, un peu couleur aubergine, en beaucoup moins appétissant, beaucoup moins. Putain, qu'est-ce qui me prend de parler bouffe en plein milieu d'un interrogatoire décisif… J'ai vraiment plus aucune suite dans les idées. Je suis même victime d'une grosse, maousse fuite des idées, je dirai. Après tout, mon cerveau n'est plus. Ma petite camarade a repris ses esprits, elle déballe sans vergogne, de sa voix d'outre-tombe, l'apothéose, afin d'enfoncer le clou de cette histoire sordide.

— J'ai fini par suivre un couloir puis un autre, je me suis perdue, et il y avait cette pièce, personne nulle part, des portes, des salles vides, j'avais trop envie de pisser, ça commençait à me couler dans les baskets, alors, j'ai appuyé sur tout ce qui se trouvait sur le tableau, dans un des couloirs, puis sur une console, boutons rouges, bleus, noirs…

— Putain ! C'est toi qu'a déclenché ce merdier ? C'est à toi que je dois ce bordel ? Qui m'a tué ? Qu'as fait raser le village ?

À présent je secoue la môme comme un prunier, j'ai une haine incommensurable, une envie folle de la griffer, de la taper, de… Et puis j'ai les crocs, j'ai tellement faim ! Alors soudain, illumination ! J'ouvre ma gueule devenue large, plus large qu'avant, et pleine de dents bien aiguisées, je sens leurs pointes râper ma langue, une langue encombrante dans ce museau que je devine s'allonger, pas besoin de miroir, ma tronche ne doit plus beaucoup ressembler à mon portrait de premier communiant. Cette faim vertigineuse ! Elle, elle sanglote à demi à présent, elle a perdu toute sa superbe, si je puis dire, parlant d'une raclure de gosse morte il y a presque une journée, tandis que je l'invective encore, salaud que je suis. Insupportable saleté de vestige humain ! J'ai les crocs, et dans les tréfonds de ma raison, tout se délite, il n'y a plus que cette dalle primale qui m’envahit, me submerge, me met à genoux. Je n'ai plus rien d'un homme.
D'un revers vif et précis, je lui arrache la moitié de sa sale petite caboche de merdeuse.

Fin de parcours.

À moi de trouver seul la prochaine étape.
