---
title: Le concert
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Musique et inspiration ne font pas toujours bon ménage. "
epub: true
pdf: true
draft: false
---
L'amphithéâtre, — une antique arène romaine — dont les vestiges se dressaient en un cercle irrégulier était comble. De gigantesques écrans, relayés par de plus modestes, diffusaient des images de la scène d'où se répandaient fumées colorées et lumières épileptiques. En guise de préambule, le guitariste se lança dans un long rift plaintif et la foule réunie dans la fosse acquiesça dans un silence presque religieux. Une fumée opaque s'élevait paresseusement au-dessus des spectateurs, empuantissant l'air déjà lourd d'une soirée de juin potentiellement orageuse. Le groupe, cinq trentenaires échevelés, aux allures de badboys, inaugurait la première représentation de son nouvel opus, intitulé The last stand.

Pour Véranie, être au septième ciel, ce devait être à peu près ce qu'elle éprouvait en ce moment même : une sourde vibration au fond de ses entrailles, une envie de pleurer, de crier, et la nette impression que le sol allait se dérober sous elle quand le chanteur des Beethoven's heart la gratifierait de son intro, a capela, bien entendu, comme toujours. Tout son corps résonnait aux battements des premiers accords de batterie, et il lui semblait qu'il s'enflait au fur et à mesure que les percussions prenaient leurs aises, investissaient tout l'espace, ne laissant place à rien d'autre que la musique. Le show promettait d'être grandiose.

Accompagné d'une ovation monumentale, Grant Donerty fit son apparition, vêtu d'un simple jeans et tee-shirt à l’effigie de son groupe, — une tête de saint-bernard montrant les crocs, dans une posture prête à l'attaque —. La jeune fille hurla comme les autres pour l'accueillir, unissant son cri à la frénésie de la foule qui faisait corps avec elle.

The show must go on, songèrent d'un même élan musiciens et foule en délire.

Une heure quarante plus tard, Véranie redescendait sur terre avec peine. Après un troisième rappel, et une standing ovation mémorable, les artistes s'étaient éclipsés, laissant le public redescendre, et lentement évacuer l'amphithéâtre surchauffé. Munie de son pass VIP, qu'elle serrait entre ses doigts moites de sueur, elle savourait sa chance d'avoir obtenu le saint Graal d'un simple concours de circonstances, un jeu à la radio locale. Elle finissait son service au bar ce jour-là, dans le cabaret où elle travaillait à mi-temps, écoutant d'une oreille distraite les jacassements du poste radio lorsque le présentateur avait lancé une bombe :

Durant une minute, tous les auditeurs qui appelleraient le standard recevrait gratuitement une place pour le prochain concert de Beethoven's heart ainsi qu'un pass VIP. Ce petit rectangle de plastique ouvrait la porte de la loge des artistes, et permettait aux heureux élus de rencontrer les membres du groupe après le concert durant quelques minutes. Elle s'était ruée sur son smartphone sans y croire, et pourtant ! Elle était bien là aujourd'hui, jouant des coudes à contre-courant pour rejoindre son groupe préféré dans sa loge.

Connor Dowell, le bassiste, un grand échalas tout en nerfs et tendons, fut le premier à franchir la porte de la loge — réservée aux artistes — et s'affaler dans un large fauteuil à l'allure confortable, suivit de son partenaire et complice de toujours, Grant Doherty qui prit directement place devant la coiffeuse, ruisselant de sueur. Il commença à se dévêtir en maudissant cette chaleur horrible. Vinrent ensuite en ordre dispersé, chahutant comme de coutume les inséparables Davis Mour et Jimi Law, guitaristes, puis, fermant la marche de son allure nonchalante et romantique, le batteur Alexandre Dobor.

Le dernier arrivant ferma la porte à double tour et rit de bon cœur.

— Sacré concert les mecs !

Il embrassa ses camarades d'un coup d’œil satisfait, ôtant lui aussi son costume, — Les autres étaient déjà nus —, pendant que Jimi servait dans des verres sales une large rasade d'un excellent whisky hors d'âge et de prix. David s'en roula une avec une dextérité qui aurait pu étonner plus d'un fan. Car ces cinq-là n'avaient rien de terrien bon chic bon genre. Leur physique démentait d'ailleurs toute appartenance, même très lointaine, au genre humain.

Imperturbable aux effusions d'Alexandre, David, de son pied unique, ressemblant à celui d'un gros gastéropode, grattait pensivement une tête minuscule et glabre aux multiples yeux de mouche. Son corps ressemblait approximativement à celui d'un vers annelé, mais doté d'une carapace chitineuse, de couleur vert pâle.

— Des aliens —.

On toqua vivement à la porte, puis un cliquètement se fit dans la serrure. La bande au complet opéra un vif repli derrière un vieux paravent déglingué qui se trouvait là, et c'est ainsi, blottis comme une nichée d'oisillons, qu'ils accueillirent leur agent, le célèbre Mr Gink, Ralf de son prénom.

Un personnage massif et rougeaud obtura furtivement l'encadrement de la porte et se coula à l'intérieur plus souplement qu'on eut pu s'y attendre d'une personne de son gabarit.

Dès le seuil franchit, et la porte soigneusement refermée, il se mit à beugler de la plus vulgaire façon.

— Bande de corniauds attardés, mais qu'est-ce que vous foutez ? Vous vous croyez où ? Sur Mars ? À la plage ?

Grant papillota de tous ces yeux, mais n'ouvrit pas le pédoncule rose qui lui servait de bouche. Les autres l'imitèrent, toujours derrière leur barricade de fortune. Ils avaient du mal à se retenir de rire. Le rire était commun aux deux espèces, et à dire vrai, très en vogue chez ces créatures venues d'ailleurs. Elles étaient d'un naturel facétieux, ce qui n'arrangeait pas les affaires de leur agent.

Celui-ci leur faisait la leçon comme un instituteur à ses élèves récalcitrants :

— Vous manquez cruellement de prudence les gars, de vous révéler dans votre forme primitive, alors que vous recevez vos fans VIP dans quinze minutes ! Vous êtes des abrutis inconscients ! Après tous nos efforts ! Vous voulez tout ruiner ou quoi ? Vous êtes pas bien, ici, peinards sur cette petite planète ? Vous voulez remonter là-haut rejoindre le vieux bonhomme ronchon ? Hein ? Têtes de linotte ! Rien dans le ciboulot !

David et Jimi en convinrent d'un hochement de tête. Connor et Alexandre se dandinèrent sur leur gros pied visqueux devenus rouge, signe de leur extrême et soudain embarras. Grant se décida à prendre la parole.

— Y faisait tellement chaud là-dedans ! Se justifia-t-il en signalant d'un regard leur épais costume d'humains échevelés jetés en vrac dans un coin de leur loge.

— Taratata ! Pas de ça avec moi hein ! Y fait pas chaud peut-être dans votre home sweet home ? Gronda leur agent, écarlate.

Ils émirent un petit cri empli d'effroi.

— Si, si, convinrent les cinq musiciens, la mine navrée à présent.

Les remontrances de Gink avaient toujours raison de leurs débordements, heureusement pour leur carrière d'ailleurs. Ralph s'occupait de tout, et savait toujours corriger le tir, comme il disait.

— Va falloir corriger le tir, mes cocos ! Et vite ! Ajouta l'agent au bord de l'apoplexie.

Et ils remédièrent.

En un instant, chaque artiste avaient renfilé son déguisement et se tenait sagement assis, sirotant un soda.

Mr Gink déverrouilla la porte et passa sa grosse tête à l'extérieur.

— Y en a combien ? S'enquit Grant.

— Ils sont une vingtaine, lui répondit l'agent, « j'ouvre à nos visiteurs, faites le show, mes petits, et soyez bons ! » Il roula des yeux pour appuyer son invite.

Ralph s’effaça, et encouragea les fans à entrer, tout en leur intimant de rester calmes. Le petit groupe d'admirateurs était majoritairement composé de jeunes gens, filles et garçons, qui s'acheminait en file indienne vers l'étroite loge du boy's band. Véranie était la dernière et certainement la plus âgée, avec un type d'une cinquantaine d'années, qui tenait dans ses mains des CD, certainement pour les faire signer de quelques autographes. La file progressait lentement. Véranie tordit le cou à plusieurs reprises, mais se trouvant entre le couloir et la loge, elle ne voyait pas ses idoles. Elle les entendit cependant glousser plusieurs fois, surtout Grant, le plus jovial de tous. Les guitares de Jimi, de différentes marques prestigieuses, s'alignaient sur un rack métallique, dans le couloir. Véranie prit quelques clichés, en attendant mieux. Au bout d'un certain temps, enfin, certains fans commencèrent à refluer, raccompagnés par Ralph Gink, et Véranie sentit son cœur battre trois fois plus vite que la normale, car la vue se dégageait sur les musicos, et elle apercevait à présent le visage poupin de Grant, et celui sec et tranchant de Connor. Les deux guitaristes étaient affalés dans un vieux sofa, elle ne distinguait encore que le bout de leurs célèbres santiags. La file progressa encore, la loge se vida progressivement, et bientôt, ils ne furent plus qu'une demi-douzaine d'élus. Le batteur frappa le bras du fauteuil sur lequel il était juché, et Véranie eut l'immense plaisir d'écouter un court récital : Grant entonna — Greatfull deads —, chanson qui contait l'histoire d'une bande de copains perdus dans le désert attendant la mort en se remémorant leurs meilleurs souvenirs. La jeune femme adorait cette chanson. Dans la petite loge, la voix rauque du chanteur prenait une autre dimension, et la nostalgie les imprégna tous, comme un voile doux préserve des premières gelées les fleurs printanières. Ce fut un moment émouvant. Véranie avait les yeux humides quand elle se retrouva devant Jimi Law. Il lui tendit un mouchoir en papier. Dans son autre main, il tenait un stylo, s'attendant à ce qu'elle quémande un ou deux autographes. Elle se morigéna en vain de ne pas avoir préparé de carnet, à vrai dire, elle n'y avait même pas songé, s'estimant heureuse si elle parvenait seulement à les voir de près et leur parler peut-être…

— Tu veux t'asseoir ? Lui proposa Jimi, grattant distraitement les six cordes sa Gibsons.

Véranie sentit la peau de son visage virer au rose, et elle resta un instant bouche bée, réalisant avec difficulté qu'il s'agissait bien de Jimi, le grand Jimi Law, qui s'adressait à elle. Le guitariste tapota le siège sur lequel il se tenait, et entama sans plus attendre une mélodie inédite. Les cordes s'animaient sous ses longs doigts agiles, et la jeune femme l'admira sans mot dire tout en posant une fesse timide sur le sofa entre lui et David qui fumait tranquillement sans lui prêter attention, essayant de raccrocher les accords que lui envoyait son complice. Il hochait la tête en connaisseur sur les plus ardus, tandis que Véranie se laissait bercer, espérant que ce moment n'allait jamais finir. Dowell décrocha son Ibanez préférée, une quatre cordes des années quatre-vingt-dix. Il tapota la caisse en acajou, et inspirant très fort, sourit à la jeune fan extasiée tout en effleurant les cordes qui rendirent un son de bourdon. Le caisson raisonnait bien, mais le musicien ne semblait pas convaincu. Il reposa l'instrument, et engagea la conversation, haussant un peu la voix pour se faire entendre.

— C'est quoi ton petit nom ? Demanda-t-il en guise de préambule.

— Véranie.

— Original ça. On pourrait écrire une chanson sur un prénom comme ça. C'est une couleur ? Un parfum ? La questionna-t-il encore.

Véranie gloussa, se racla la gorge, et se lança. Tant pis si elle passait pour une idiote ! Au moins passerait-elle pour une idiote qui parle.

— Véranie, c'est le prénom que m'ont donné mes parents, ils aimaient ces deux mots et les ont unis pour toujours dans ce prénom.

— C'est une belle histoire, dit Mour, interrompant son jeu. « Tu as d'autres belles histoires ? »

— Ce que je peux dire, c'est que je vous remercie du fond du cœur pour l'inspiration que vous m'apportez. Je suis artiste peintre, je poursuis un cursus d'art contemporain, et votre musique joue un grand rôle dans mon processus créatif.

Tous furent soudain très attentifs. Ralf avait discrètement renvoyé les derniers fans, il ne restait que la jeune femme. Plus aucune note ne flottait dans l'air.

— Tu peux nous montrer ce que tu peins ? demanda Grant, esquissant un geste d'impatience envers son agent, immobiles, ses petits yeux porcins fixés sur la jeune femme comme un python sur une gerbille.

Ralf se reprit et offrit une mine plus avenante, encourageant Véranie à partager quelques œuvres.

Elle ouvrit son smartphone, et afficha les toiles qu'elle exposait en ce moment même dans une petite galerie, en province.

— C'est un petit succès, commenta-t-elle, en leur dévoilant une toile très colorée.

— Quelle lumière ! S'exclama Dobor, en reprenant sa basse. Il en tira cette fois quelques accords qui formaient un ensemble assez étonnant mais harmonieux.

Les autres le regardèrent, puis regardèrent Véranie, et elle se sentit soudain un peu trop sous le feu des projecteurs.

— Vous savez, ce n'est pas encore abouti. Elle dévoila d'autres toiles.

Gink se mit à respirer très fort par le nez, comme s'il manquait d'air et Jimi le regarda de travers.

Il toucha l'écran du téléphone et sourit à la jeune femme.

— Tu as un sacré talent, c'est évident !

— J'ai encore trois ans d'études pour peaufiner ce — talent — comme vous dites ! Elle rit. « C'est vous les artistes ! »

Ils la questionnèrent encore un peu, Connor Dowell semblait le plus passionné, puis, l'heure tournant, ils se quittèrent, avec force d'embrassade, quelques selfies, et la promesse de visiter l'expo de peintures pour les uns, et de venir au prochain concert pour l'autre.

Sitôt qu'elle eut tourné les talons, la bande et son agent se concertèrent, fébriles.

— Tu penses que ce serait bien de le faire avec elle ? Demandèrent-ils en chœur à Ralf Gink.

L'agent prit son temps pour répondre, puis, arguant que le précédent fan sélectionné avait permis la finalisation de leur dernier album en date, un gros succès, qu'ils avaient joué ce soir d'ailleurs pour la première fois en concert, il leur certifia que cette Véranie était la candidate idéale.

Jimi tapa sur la coquille de son pote David. Alexandre et Grant se congratulèrent à la manière de deux gastéropodes amoureux, tandis que leur agent se frottait ses petites mains qu'il avait délicates, presque enfantines. Mais son sourire avait plutôt l'air de celui d'un requin, — un gros, un très gros à vrai dire —, genre le blanc, celui qui vous bouffe un bras quand il vous salue d'un bien le bonjour madame !

Leur conciliabule n'avait pas duré en tout et pour tout plus de quelques minutes, aussi se précipitèrent-ils à l'extérieur de la loge sitôt renfilés leurs fastidieux déguisements, leurs cheveux, par trop repérables, astucieusement disciplinés et dissimulés sous de simples bonnets de marins. Ils étaient prêts à passer au plan K — comme kidnapping —.

Dowell scanna les écrans extérieurs qui affichaient à présent les centaines de retardataires s’écoulant tranquillement vers les couloirs de sorties. Avec ses yeux d'insecte, il n'eut aucun mal à repérer la jeune femme, le long des rambardes de la section B. Elle prenait son temps pour descendre les escaliers, certainement en train de se passer en boucle ces dernières vingt minutes passées avec son groupe favori.

Ces humains ! Des nostalgiques impénitents ! Avec des sentiments pareils, ils ne pouvaient que concourir à leur propre perte. Connor soupira et glissa l'information dans le conduit auditif de ses amis. Ils traversèrent hâtivement le centre de l'arène pour couper la route à leur proie sans attirer l'attention.

Jimi éclata d'un rire franc, intérieur, mais néanmoins aussi terrible que le rire d'un ogre affamé. Il l'avait aperçu, plus bas, et se mit en chasse. Les autres la rabattraient, mais c'était toujours lui qui les choppait.

Véranie ressort de la loge au petit matin dans un état de désorientation et d'épuisement intense. Elle est seule, — nulle trace de Grant ou Connor, ni des deux guitaristes, ni même du désagréable agent Gink —. Ses idées, confuses, ne lui permettent de se remémorer ni passé, ni présent, sauf une scène étrange qui revient en boucle, encore, et encore :

Dans celle-ci, une créature sortie tout droit d'un cauchemar, ressemblant à un gros crabe vert unijambiste surmontée d'une tête de lutin avec des yeux de mouche, parle avec une autre, du même acabit, — qu'elle ne distingue pas —, sauf ses grandes pinces étreignant un bocal dont elle se sert pour aspirer goulûment le contenu, un liquide grumeleux rosâtre agrémenté de particules blanchâtres. La première bestiole, nerveuse, demande,

— C'est bon ? T'as tout ?

La seconde, surexcitée, répond entre deux goulées,

— Ouais mec, c'était un très bon choix, ce cerveau ! Très inspirant ! Ces couleurs ! Cette vivacité, cette profondeur ! La trame est si délicate ! Ah mec ! Le prochain album sera un carton !

Et son acolyte de protester,

— Laisse m'en un peu sale égoïste !

Quelqu'un ou quelque chose gueule quelque part,

— Va falloir corriger le tir mes cocos !

Hélas pour Véranie, le reste de la conversation se perd dans les limbes d'une boite crânienne désormais stérile, emplie d'une répugnante matière verdâtre.
