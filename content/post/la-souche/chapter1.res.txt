La souche

— Ousse que tu m’emmènes, Joseph ?
  °°°°°

— T'occupes, on y sera bientôt !

Le dénommé Joseph crapahutait dur à travers le sous-bois dense, passablement gêné par sa patte 
folle (Il l'avait perdue jeunot, à jouer dans l’entrepôt d'une scierie, au milieu de caisses vides. 
Un empilement avait lâché sous son poids, les esquilles dans sa chair avaient apporté l'infection, 
et depuis, sa jambe était raide comme une bûche). Jean le suivait en soufflant, sa grosse panse 
devant lui, maintenue par une paire de bretelles distendue. Les croquenots du premier écrasaient 
les arbrisseaux maigrichons qui tendaient leur cou vers le ciel, et la serpe du second laissait un 
tapis de branchettes mortes derrière eux. Ils avançaient sans hâte, la musette sur le dos, un gros 
sac de jute contenant leurs outils dans une main. Un foulard à carreaux scindait leur cou rouge et 
trempé d'une sueur rance. Les moustiques zonzonnaient autour d'eux, piquant leur cuir dur sans 
qu'ils semblent seulement s'en apercevoir. Les deux larrons portaient la même sorte de chemise, une 
chemise de bûcheron, rouge et noire. Fait était que ces deux-là exerçaient ce métier, et depuis 
leur jeunesse. Joseph reprit son souffle, les mains sur les genoux tandis que Jean scrutait la 
forêt alentour.

— Ma Y a rien pour nous dans c'te brousse, s'exclama-t-il un peu déçu.
                                  ^^^^^^^
* 1 [34:41]  # #7863 / g2__conf_non_verbe_après_préverbes__b5_a3_1:
  Incohérence avec « te » : « brousse » devrait être un verbe.


— Attends donc de voir, assena l'autre, lui claquant une large pogne sur l'épaule. Tu vas pas en 
croire tes yeux, mon gars.

— On est fort loin de nos pénates, reprit Jean, doutant tout de même. Il était des deux le plus 
défaitiste, tatillon parfois, raseur même, aurait dit certains au village. Un peu ladre, pour sûr, 
mais avec un je ne sais quoi qui poussait à apprécier malgré tout le bougre. Un courageux, fait est.

— La mule peut passer, c'est l'essentiel, on aura pas de soucis pour le débardage, soit pas frileux 
comme une commère.

— Ch'ui pas frileux, ch'ui curieux, à tout dire. Ça fait bien deux heures qu'on dame dans ce 
  °°°°°              °°°°°
labyrinthe, Joseph, et j'en vois point le bout.

— Viens, c'est plus loin, ouvre grand tes mirettes, allez, en route, vieille baderne !

Joseph le harcela tant et si bien que son compagnon le suivit tout en ahanant, sans plus de 
commentaire.

Ils parcoururent encore une chênaie rabougrie, avant d'entendre le bruit rafraîchissant de l'eau 
qui court sur les pierres.

— On y est ! Commenta Joseph, sans se retourner vers Jean qui soufflait comme une forge. Il 
accéléra le pas, sur quelques dizaines de mètres, et déboucha sur une clairière longue et étroite, 
coupée en son milieu d'une avenante rivière.

— Mais nous v'là revenu sur la Bruxenelle dis donc ! Jean n'en revenait pas.
            °°°°               °°°°°°°°°°

— Oui mon gars ! Sommes sur la parcelle du vieux Loïc Loches, et la Bruxenelle la traverse dans 
                                                                    °°°°°°°°°°
toute sa largeur.

— Loche ? Ah ouais ? Celui qu'à disparu depuis un bail ? Même que sa maison est restée depuis comme 
au jour de sa disparition, qu'y paraît ?

— Celui-là même, mon brave, approuva Joseph, l'air satisfait.

Jean regarda attentivement la saulaie bordant le cours d'eau. Sa grosse face rougeaude sembla 
enfler encore, sous l'affleurement d'une mimique satisfaite.

— Y a des beaux fûts, commenta-t-il en connaisseur.

— Beaux ! Tu plaisantes, magnifiques ! Regarde la taille ! On a pu d’affouage comme ça depuis au 
moins vingt piges ! Corrigea l'autre un tantinet vexé.

— Vrai quel'maire est maousse radin.
       °°°°°°°°°°

— Radin et communiste, c'te comble ! Ajouta Joseph en s’esclaffant. On va se rattraper avec ce 
cadeau du ciel, vois-tu mon Jean. Ce bois, on va s'en occuper, et on aura une chauffe de tous les 
diables pour quelques années, j'te le garantis ! Foi de Joseph Goyeur ! On va en monter des 
                                                               °°°°°°
stères ! Pas un bois exceptionnel, mais gratuit, ça oui ! Regarde la parcelle, Y a de quoi faire à 
profusion !

— T'es rusé, mon cochon, mais faut rester discret, pas que ça s'ébruite, sinon…

— Sinon quoi ? Le propriétaire suce les pissenlits par la racine, c'est l'évidence ! Et personne 
vient par ici, toi-même t'étais à cent lieues de savoir où que t'allais poser ton gourbi y a encore 
une heure !

Jean ne pipa mot. Il observait attentivement les saules, droit comme des i, et qui montaient haut 
avant de fourcher. De quoi faire des grumes, se prit-il à songer. Vendre l'aubier le plus noble 
pour faire de la futaille, le reste pour les échalas, et le rebut, à la chauffe… ui, de quoi gagner 
                                                                                 °°
quelques sous.

— Vrai que ce sont de beaux spécimens, faciles à couper, je gage, se contenta-t-il de dire, gardant 
ses pensées pour lui.

— Et vois le diamètre ! Aucun en dessous du mètre ! Approuva Joseph.

— On va en chier pour les mettre à terre !

— Tu doutes, vieux frère ?

Joseph se tenait négligemment un pied sur une souche de belle taille. Il la heurta du plat de sa 
main.

— Et celle-ci hein ? Il a bien fallu que quelqu'un l'abatte ce saule, pour qu'y reste que c'te 
vieille souche grise !
^^^^^^^
* 1 [95:102]  # #7863 / g2__conf_non_verbe_après_préverbes__b5_a3_1:
  Incohérence avec « te » : « vieille » devrait être un verbe.


— Vrai, convint Jean, en regardant plus attentivement ce sur quoi se penchait son compagnon. « On 
dirait un gros crapaud endormi, c'te souche. Un très gros crapaud »
                                     ^^^^^^
* 1 [135:141]  # #7863 / g2__conf_non_verbe_après_préverbes__b5_a3_1:
  Incohérence avec « te » : « souche » devrait être un verbe.


La souche, en effet, agrémentée quelques rejets avortés à cause d'une quelconque maladie, 
— certainement le chancre noir —, ressemblait effectivement à un dos de crapaud couvert de 
pustules. L'effet s'en trouvait renforcé par deux saillies arrondies, en son milieu, qui pouvaient 
figurer deux yeux ronds fermés. L'écorce, grise et profondément creusée en ridait la surface, 
créant des reliefs chimériques.

— Alors on se met à l'ouvrage ? Fanfaronna Joseph, tapant du bout de sa godasse ferrée la cule 
inerte.

— Pardieu ! Fit jean pour toute réponse, levant les yeux au ciel puis se penchant vers son sac afin 
d'y prélever sa grande hache, aiguisée de frais — il y avait passé la soirée de la veille penchée 
sur le fil de lame, la pierre à affûter allant et venant jusqu'à un résultat parfait —.

Joseph fit de même, attrapant quelques coins et une hache de moindre envergure.

Ils déambulèrent dans la saulaie, frappant sur un tronc, en caressant un autre, puis, au bout d'un 
temps, se décidèrent pour un grand mariolle à la tête chauve. Il était encore vert, mais un coup de 
vent avait brisé net le houppier.

— V'la un qui va nous épargner du travail, résuma Joseph d'un air entendu.
  °°°°

— On s'évitera de faire du feu ce jour, pas plus mal vu c'te chaleur ! Incroyable pour une fin 
                                                             ^^^^^^^
* 1 [61:68]  # #7863 / g2__conf_non_verbe_après_préverbes__b5_a3_1:
  Incohérence avec « te » : « chaleur » devrait être un verbe.

d'octobre, faut pas dire, Y a pu de saison ! Renchéri Jean, toujours prêt à suivre son comparse.

Ils se mirent à cogner.

Des éclats d'écorce volèrent en tout sens aux premiers coups, Jean frappait fort, et la lame entama 
le cambium, puis tailla son chemin à travers l'aubier vers le cœur de l'arbre. Joseph positionnait 
les coins au fur et à mesure, pour aider au sens de la coupe, et à la fin, influencer la chute de 
l'arbre coté rive, — et non dans la rivière où il eut été fort malaisé de l'en sortir à seulement 
quatre bras —.

La bedaine de jean tressautait en mesure tandis que l'outil mordait le bois, arrachant des copeaux 
aux odeurs d'humus, et mordait encore, jusqu'aux premiers craquements.

— L'est bon Jean, pousse-toi que je retire c'te coin puis cet autre !
                                                ^^^^
* 1 [48:52]  # #7863 / g2__conf_non_verbe_après_préverbes__b5_a3_1:
  Incohérence avec « te » : « coin » devrait être un verbe.


L'homme s'écarta de quelques pas, et, rencontrant la grosse souche grise de tout à l'heure, 
s'affala dessus, essuyant son front mouillé de sueur avec la manche de sa chemise.

Joseph retirait le second coin à l'aide d'un merlin et le saule se mettait à pleurer la vie qui le 
fuyait déjà, lorsque sous le bûcheron assis, un éternuement retentit. Jean lui-même se mit à 
vaciller, tandis que la souche, ou ce qui semblait l'être, s'ébrouait, sous les yeux effarés de 
Joseph. Il suspendit son geste.

— Mais que diable ? Fut tout ce que son acolyte eut le temps de prononcer avant de se voir 
proprement avaler par une gueule rose, large comme un four à boulanger.

Un cri suraigu sorti de la gorge de Joseph tandis qu'il voyait son ami poussé au fond de cette 
gueule immonde, par une langue tout aussi rose et immonde.

Deux yeux globuleux s'ouvrirent, là ou gisaient les saillies d'écorce encore un instant auparavant, 
et des pattes, quatre, des pattes de crapaud, se plantèrent fermement dans la berge, accompagnant 
le mouvement compulsif de déglutition de la souche-crapaud.

Joseph se précipita en arrière, et chut sur le cul, les yeux exorbités, son cri ravalé 
— profondément — jusqu'à ses poumons, sans doute, qui présentement manquaient d'air. Il devint 
successivement rouge brique, parme puis blanc.

Un éprouvant — slurp — parvint de la gueule du monstre alors que son affreux clapet se refermait 
               °°°°°
sur un des croquenots de Jean, l'engloutissant comme le reste de sa personne.

Fins à choix multiples
trois fins vous sont proposées :

[Classique](#classique)
[Fantastique](#fantastique)
[Horrifique](#horrifique)

À vous de choisir !

## classique

La souche avait repris sa forme initiale.

Le saule, affaiblit par la cognée et les coins retirés, ballottait dans la brise de droite et de 
gauche, semblant hésiter sur le lieu de sa dernière demeure. Le coin restant sauta de la plaie 
suintante. Le bûcheron leva la tête au craquement net et soudain du tronc, mais son corps n'obéit 
pas assez vivement. Le saule s'abattit sur lui, enfonçant ses chairs dans sa propre chair, et le 
tout dans la terre souple et odorante de la clairière, servant de linceul au malheureux Joseph.

Le silence, qui avait envahi la clairière quelques instants plus tôt, aux gémissements de l'arbre, 
s’effaça aux premières notes d'un pinson, et fut suivi de la gente ailée toute entière réunie dans 
ce lieu. Ou devrai-je dire sanctuaire. Les gargouillis de l'eau, sur les pierres, célébraient cette 
belle journée d'automne, et l'on n'entendit plus jamais parler de Joseph et de Jean, partis de bon 
matin couper du bois dans la forêt.

## fantastique

Le monstre fixa alors Joseph de ses prunelles jaunes.

— Un vœu tu peux, étranger, nourriture mérite récompense, dit-il d'une voix gutturale.

Joseph avala de travers, et son cerveau battit la campagne avant que la réalité ne se fit jour : le 
monstre le remerciait de lui avoir fourni à manger ! Son ami jean en casse-route ! Le bûcheron 
saisit sa hache, et entendit faire la peau à cette horrible créature.

— Je ne suis pas ingrat, un vœu, vite ! Ordonna la souche-crapaud, sans manifester la moindre 
crainte.

Le bûcheron brandit sa hache.

Le crapaud-monstre ne bougea pas d'une écorce. Seuls ses globes safranés le regardaient avec 
insistance.

— Un vœu ! Pour la nourriture, gronda-t-il encore.

Joseph se gratta la tête de son béret, et abaissa son outil. Ses jambes tremblaient, mais il y 
avait là peut-être fortune à gagner. Il s'enhardit.

— N'importe quel vœu la bête ? Hasarda-t-il.

— Un vœu j’exaucerai ! Gloussa la souche-crapaud.

— Je… ommença Joseph, avant de réfléchir.
      °°°°°°°

Mais le monstre le pressa.

— Un vœu ! Coassa-t-il.

— Ma besace de lingots d'or ! Hurla le bûcheron sans plus tergiverser.

Les oiseaux alentour semblèrent rire. Les arbres, saules, épineux, bouleaux, hêtres en devenir, 
arbrisseaux, frissonnèrent dans la brise.

La besace, abandonnée au bord de l'eau, s'enfla soudain et déborda bientôt de lourds lingots 
scintillants. Joseph écarquilla les yeux, trop heureux pour y croire. Lâchant sa hache, il se 
précipita vers le butin, passant ce faisant à un pas de la vieille souche grise.

Mal lui en prit !

La gueule s'ouvrit encore plus large que la première fois, et goba l'homme comme un simple 
moustique, la tête en premier, poussant avec sa langue démesurée sur ce qui dépassait, et dans un 
ultime effort, englouti Joseph.

Un — slurp — écœurant résonna dans la forêt.
     °°°°°

Les pépiements, criaillements, hululements, belottements, grincements, chuintements, sifflements 
                                            °°°°°°°°°°°°
couinements, cacabements, babillements, glapissements, reprirent de plus belle : Chaque habitant de 
             °°°°°°°°°°°
la forêt commentait l’événement.

Deux champignons, sous la mousse, jasaient :

— Que n'a-t-il choisi la vie celui-là au lieu de l'or ! L'imbécile ! commenta le premier, au 
chapeau rond et blanc.

— Que n'a-t-il choisi la vie, en effet ! Renchéri le second, au long pied brun, les lamelles 
tremblotant sous le rire. Un rire de champignon.

La souche avait repris sa forme initiale.

Le saule blessé entama sa chute à cet instant, tournoyant dans l'air, indécis, puis, comme muni de 
volonté, s'abattit en travers de la rivière, soulevant une pluie de feuilles, de gerbe d'eau et de 
terre, dans un grand barouf qui résonna loin dans la forêt.

Mais aucun homme ne fréquente cet endroit, que les lutins et farfadets nomment depuis ce jour — le 
pont de la souche —. C'est pourtant une belle clairière, où coule un délicieux ruisseau. Sur ses 
berges, des saules majestueux tendent leurs rameaux gracieux dans le courant tranquille, et une 
vieille souche grise veille.

## horrifique

Le saule blessé à mort gémit, prémisses d'une chute inévitable.

La souche-crapaud entama alors une transformation éclair qui fit taire jusqu'à la plus petite des 
créatures de la forêt. Elle se mit à tourner, tourner, tourner… et devint un tourbillon de couleur 
jaunâtre, et ce tourbillon se dirigea vers l'arbre mal en point, qu'il entraîna dans sa ronde 
frénétique. Des bruits étranges envahirent les bois, éclairs et tonnerres retentirent dans la 
clairière noyée dans un brouillard jaune tourbillonnant et mugissant tel une tornade.

Et aussi soudainement, elle cessa. Une pluie de fins copeaux de bois inonda le sol, véritable tapis 
de neige, et forma sur la rivière proche comme autant de minuscules et précaires embarcations.

Le tourbillon, sans plus de force, recracha le saule qui n'en était plus un. Plus vraiment. Il ne 
restait plus rien de l'arbre. Enfin, plus grand-chose.

Qui sait ?

La souche-crapaud repris sa forme initiale.

Près d'elle gisait à présent une autre souche, à peine moins remarquable, mais différente. De bois 
plus clair, elle sentait le tanin du bois fraîchement coupé, et son écorce luisait faiblement. Deux 
saillies marquaient sa surface pâle, lui donnant l'ombre d'un regard tandis qu'à ras de terre, 
dépassant d'une cavité aux allures de bouche mal dégrossie, des dents de métal, — on aurait juré 
les coins de Joseph — semblaient sourire en attendant leur heure.
