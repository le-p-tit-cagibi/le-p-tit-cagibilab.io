---
title: Le bus de l'horreur
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Ou les aléas d'une retraite anticipée "
epub: true
pdf: true
draft: true
---
Le bus de l'horreur

— Mais qu’est-ce que vous attendez là planter comme un navet sous l'orage ?

Abel prit le temps de fermer son smartphone, puis releva la tête et découvrit deux vieux débris plantés devant lui, bras dessus bras dessous. S'il avait dû deviner l'âge de ces créatures ratatinées et ridées autant que de vieux fruits secs, il aurait avancé sans conteste le chiffre de 200 pour la paire tant le couple s'apparentait plus à des momies échappées du musée le plus proche qu'à des promeneurs faisant le tour de leur quartier entre chien et loup.

Il opta pour un minimum de politesse, peut-être étaient-ce des voisins. Autant ne pas se les mettre à dos, à peine débarquer dans le coin. Il sourit à ses interlocuteurs, mais n'eut pas le temps d'en placer une.

— Vous comprenez ce qu'on vous demande jeune homme ? Questionna la femme, fichu mauve bas sur le front, sac cabas déglingué chevillé au coude, longue pelisse dont il aurait été bien imprudent d'avancer la couleur d'origine, mais qui pour l'heure recouvrait la vieille d'une peau grise, se mariant à merveille avec son teint, et contrastant au-delà du raisonnable avec sa gouaille. Elle ne désarma pas devant le silence qui l'accueillit et se tourna vers ce que Abel supposa être son mari.

— Il m'a l'air passablement abruti, ce garçon, à moins qu'il soit drogué…

Le vieux lui tira la manche, avec un clin d’œil.

— Ou bien il carbure à la bibine, allez laisse donc ce jeune citadin tranquille, Maude.

Le vieillard portait une gabardine, et dessous, un costume trois pièces, nœud papillon et chaussures vernies. En opposition totale avec l'accoutrement de sa moitié, dont les savates, éculées, quémandaient à grands cris un petit tour chez le cordonnier. Paraissant prêt à se briser en deux au moindre cahot de la route, voire au plus insignifiant coup de vent, son squelette faisant en quelque sorte office de porte-manteau à l'élégant costume, les yeux du vieux bonhomme, d'un jaune chassieux pétillaient cependant de malice. Il s'avança, ôta son chapeau, un melon quelque peu râpé, et interpella à son tour le garçon qui les observait mutique, n'en croyant pas ses oreilles.

— Dites, ma femme et moi-même, nous nous demandions où vous croyez aller comme ça ? Nous habitons un peu plus haut, dans votre rue.

Ces vieux schnocks l'avaient donc espionner un peu, se dit Abel qui ne les avait jamais vu jusqu'à cet instant.

— Parce qu'entre nous, jeune homme, si vous savez lire, vous pouvez voir là que le bus ne ramasse plus personne depuis 1979. Oui, me regardez pas comme ça, c'est marqué là, sur la plaquette du plan et itinéraire : réalisé en 1979. Commune de Folle-les-vents.

La momie agitait son index sec comme une brindille devant le nez de son interlocuteur.

— Et vous aurez le cul plein d'escarres et les pieds soudés au sol que de bus, vous en aurez pas vu la couleur, ajouta la version masculine.

Abel se leva, ce qui eut pour effet de faire reculer ses deux contradicteurs, sans que la vieille ne cesse pour autant de faire la causette.

— Vert, qu'il était, tout pimpan ! Mais bon, levez vos fesses de là, on vous dit ! Le bus passe plus chez nous. Vous faut aller à la gare, comme tout le monde, prendre le monorail pour descendre à Claive.

— Vous empruntez le monorail ?

Abel n'en revenait pas, il avait aperçut l'antique engin la semaine précédente avant de monter jusqu'au village.

— Ma foi il sait causer ! La vieille rit de bon cœur, et se présenta. Maude Fadras, enchantée jeune homme d'entendre votre voi ! Mazette j'ai cru que vous nous arriviez droit de chez les mabouls. Faudra travailler vos expressions, celle de bienvenue n'est pas très convaincante, m'avez l'air un peu frondeur, comme…

— Comme vous pouvez entendre, Maude parle pour tout le monde, enchanté, je suis Mr Fadras, la coupa le gentleman au chapeau melon, et pour répondre à votre question, bien entendu que nous empruntons le monorail ! C'est l'engin le plus sûr qui soi !

— J'aurai préféré prendre le bus, répondit Abel sans oser serrer avec trop de fermeté les mains parcheminées. Je l'ai entendu passer, une espèce de cloche a sonné, il y a de ça un quart d'heure. J'ai pensé que c'était pour avertir.

— Avertir ? Ça fait belle lurette que l'avertisseur a cessé de fonctionner, mon bon ! Rétorqua la femme.

— Des lustres, pour ça oui. Renchérit son mari.

Abel ne s'attarda pas sur leur scandaleux petit mensonge, — une petite farce entre eux, probablement, pour rire un peu sur le dos du citadin —. Il avait belle et bien entendu une cloche, et émit à haute voix le fond de sa pensée.

— Mais si pas de bus, je vais commander un taxi.

— Un tax ! Deux paires d'yeux exorbités le dévisagèrent avec reproche et s'écrièrent en chœur : Vous allez faire une peine de cheval à notre bon Léo !

— Et il est rancunier Léo ! Insista Mr Fadras, en se grattant le menton.

Abel soupira, c'était quoi encore cette histoire ? Il attendit la suite ; avec ces deux-là, il n'aurait certainement pas à attendre longtemps pour une explication.

— Ici, tout le monde prend le monorail, le bus marche plus et les taxis, ils aiment guère monter jusqu'ici, surtout l'hiver.

— Mais nous sommes au début du printemps, donc ça devrait aller non ?

Le jeune homme prit résolument son téléphone et pianota quelques instants, avant de se rendre à l'évidence : pas de taxi disponibl ! Rie ! Incroyable.

Les deux antiquités le regardaient, hochant de connivence leur vieilles têtes racornies.

— Vous voye ! Quand à votre cloche, peut-être une vache des Hauts que vous avez entendu, mais certainement pas le bu ! Maude étira ses lèvres en une sorte de sourire réconfortant. Pas de taxi, mais il vous reste le monorail !

Abel n'en démordit pas mais intérieurement seulement, il avait bien entendu une cloche ! C'est ce qui l'avait décidé à sortir d'ailleurs alors qu'il s'évertuait à rassurer Frances, qui le bombardait de SMS. Mais il renonça à ergoter avec ces deux empeignes.

— J'irai à vélo jusqu'à Claive.

— C'est possible, mais ça monte sec, le prévint Mr Fadras, tapotant de son pied bien chaussé la route pavée.

— Très, très sec, on pourrait dire, surenchérit sa femme. Roland, tu te rappelles la dernière fois que…

— Taratata, tais-toi Maude, tu importunes notre nouveau voisin avec toutes tes jérémiades.

Il sourit en direction d'Abel, qui, quelque peu surpris, hésita entre lui tirer la langue et lui faire sa plus vilaine grimace. Il commençait à en avoir soupé des deux badernes. Pourtant, il prit sur lui.

— Écoutez, c'est gentil de me renseigner, mais je vais me débrouiller.

— Ah ça, faudra bien, je vous porterai pas sur mon dos, répliqua Maude du tac au tac. Mais tout de même, pas prendre le monorail, c'est une aberration jeune homme, croyez-moi.

Mr Fadras lui serra résolument le bras et l’entraîna à l'écart, réajustant par la même occasion son chapeau sur sa tête d'où émergeait en ordre dispersé, quelques mèches d'un blanc de neige.

— Salutations, mon garçon. Et ne restez pas là, l'arrêt de bus est vétuste, rien n'est entretenu dans ce bled, on manque de bras pour faire, vous allez vous prendre le toit sur la trogne.

— Ou la pancarte va se détacher et vous couper la figure, ajouta Maude, décidément en verve.

Abel leva un sourcil, puis les deux, les plantant là. Il rebroussa chemin non par peur d'un accident, mais celle de perdre son sang froid face à ces deux gremlins.

Une semaine qu'il était installé, et les premiers colons qu'il croisait semblaient complètement déjantés, songea-t-il en se moquant un peu de lui-même. Pas de quoi lui donner quelque inspiration pour son mémoire de fin d'année.

C'était la quiétude qu"il était venu chercher ici. — Six mois — il avait perdu six longs mois sans rédiger une ligne, et la fin de l'année approchant, il lui fallait se mettre d'urgence à la tâche : Passer quelques semaines isolées dans un ancien village mauresque lui avait paru la plus lumineuse des idées, — sur le moment —.

Il haussa les épaules, fit de loin un signe de la main au couple de pipelettes, se reprochant déjà son manque de politesse, mais continua de s'éloigner sans écouter plus longtemps leurs jacasseries.

La vieille avait une voix de scie égoïne : la fuite devenait une question de survie pour son auditio !

Il remonta la rue principale, qu'on repérait aisément puisque qu'il n'y avait qu'elle. Le hameau, parce que village c'était tout de même présomptueux pour un endroit où disons une vingtaine de maisons de pierres, — dont une partie en ruines —, s'agglutinaient le long de l'unique route partant à l’ascension du col de la Bougeotte, mille quatre-cent mètres d'altitude tout de même. Les montagnes formaient un goulot étroit autour de Folle-les-vents. Dans l'autre sens, le chemin descendait jusqu'à Claive. Abel était arrivé avec une charretée d'ânes, sur l'instant, il avait trouvé charmant l'idée de voyager quelques heures sur une de ces bêtes impavides et courageuses. Fait et dit qu'il ne s'était guère renseigné sur les commodités du lieu ; un de ses copains de fac lui avait proposé cette retraite, possession familiale inutilisée, et lui, fauché comme il l'était, avait sauté sur cette occasion inespérée de changer d'air avant d'imploser. Frances avait regimbé, ce qui l'avait conforté dans sa décision. Sa fiancée pouvait se révéler charmante, mais elle avait également à cœur qu'il réussisse dans la vie, et ne cessait de le lui rappeler. C'en était devenu un sujet de disputes. L'idée d'une retraite au fin fond d'un massif montagneux paumé ne l'enchantait pas, peut-être parce qu'elle ne lui faisait pas confiance pour terminer sa thèse, peut-être. Doutait-elle de lui ? Il était parti. Elle était restée dans leur confortable appartement prêté par ses futurs beaux-parents.

Il pressa le pas, espérant chasser ces vaines ruminations.

Le soleil se couchait à présent et pas un chat ne parcourait les lieux. Quant au habitants, ils s'avéraient invisibles À croire qu'ils l'évitaient, purement et simplement. Folle-aux-vents était un village atypique, sans lieu de culte, sans école, sans mairie… Il n'y avait là que des constructions de pierres trapues et cubiques, alignées comme à la parade le long de la route, elle-même accrochée à la montagne dont elle épousait les anfractuosités. Un mur, en pierres bien entendu, séparait chaque habitation. Et chacune possédait un petit jardin potager, une cour, et le wc au fond de celle-ci. Point d'eau courante, mais un puits et une pompe. Dans la cuisine de Abel, trônait une remarquable pierre à eau, d'au moins quatre fois la taille de l'évier qui ornait la kitchenette de son appartemen ! Il aurait parié qu'à l'identique, elle agrémentait chaque maisonnette du hameau. Pas de réfrigérateur mais un cellier, une cave, et même une terrasse, au-dessus de la maison sans toit : ici, la couverture des bâtisses se composait de terre, et certaines possédaient de ce fait une terrasse. Abel avait vu cela sur les photographies que lui avait montré son pote pour achever de le convaincre, mais il n'y avait pas mis les pieds. L'homme dénommé Tintin, — le meneur d'ânes, qui l'avait conduit jusqu'ici, et ouvert la maison, le numéro 13 —, lui avait bien précisé que la terrasse n'était plus praticable car il y avait un risque d'éboulement. Vu l'état général de la maison, rien de bien étonnant, songea Abel avec un certain fatalisme lorsqu'il poussa la porte d'entrée de toute ses forces pour arriver à se créer un passage suffisant et se glisser à l’intérieur. Il y faisait frais et sombre. Le village était malgré tout relié à l'électricité, aussi tourna-t-il le commutateur et s'affala-t-il avec soulagement dans le fauteuil décrépi qui se trouvait là.

Il se morigéna ; il aurait dû descendre en ville pour quémander un nouveau délai pour son mémoire auprès du doyen et de son directeur de thèse qu'il avait lâchement laissé en plan sans plus d'explication que celle d'avoir besoin de se ressourcer.

Se ressourcer, ouais, mon cul tiens !

La cafetière fut bientôt en route sur le petit réchaud, et il en savoura un grand bol fumant avec quelques casse-croûtes. Cela le réconforta, tandis qu'il repensait à sa rencontre de tout à l'heure.

Pourquoi ne pas prendre le monorai ?

Parce que tu l'as vu passer un certain nombre de fois cette semaine, que ce machin grince de partout, qu'il tremble et gémit comme ces vieux, et qu'on croirait qu'il va tomber en morceaux à chaque instan ! Pard !

Quels cinglés ! Abel tenta quelques pages d'un magazine qui traînait là, sans succès, et se replongea dans son smartphone, non par désœuvrement, mais par lâcheté. Oui, il se trouvait lâche de ne point être capable de finir ce travail de fin d'études et de ne point vouloir l'admettre.

J'irai à Claive en monorail demain, se promit-il avant de piquer du nez dans son fauteuil, aidé par quelques verres d'un mauvais gin. Une nouvelle manie à ne pas encourager, se reprocha-t-il avant de sombrer dans un sommeil râpeux.

Ainsi n'entendit-il pas la cloche, résonnant à travers Folle-les-vents, au cœur de la nuit. Ni ce bruit de moteur, un peu asthmatique, qui parut envahir le hameau un court instant avant de disparaître au premier chant des merles, ces lève-tôt !

Il plut les jours suivants jusqu'à désespérer du climat montagnard. Abel devait sortir, il se le répétait chaque jour, telle une litanie. Et puis quelques verres plus tard, il abandonnait l'idée. Elle lui paraissait carrément absurde vers midi et insurmontable en début de soirée. Il s'écroulait généralement ensuite, après avoir envoyé des messages incompréhensibles à Frances, qui lui répondait par des interrogations de plus en plus inquiètes. Elle avait tenté de l'appeler, mais lui répondre était au-dessus de ses forces. Il se traitait de perdant, sombrant dans un sommeil tourmenté, se levait fatigué.

Au bout d'une semaine, elle l'avait menacé de rompre sa promesse de ne pas le déranger, s'il ne décrochait pas à ses appels. Il avait mis le smartphone en sourdine. Depuis, elle l'avait laissé tranquille. Il ne se sentait pas fier pour autant, et craignait de la voir débarquer, avec son brushing parfait, sa jupe sans un faux pli, et son corsage impeccable. Pourquoi lui en voulait-il tant ? Elle l'encourageait depuis le baccalauréat ! Il était injuste. Il n'y voyait plus clair, sa vie, ses études, tel un manège de chevaux de bois, tout tournait dans sa tête à lui donner mal au cœur. Il était déprimé, convint-il. Il était bourré, mais fit abstraction de cette partie du problème.

Cette nuit-là, Abel se réveilla subitement avec une furieuse envie d'uriner, et il s'en félicita ; il aurait pu pisser dans ce vieux sofa, qui l'accueillait chaque nuit et n'avait plus rien à sauver, mais tout de même.

Quelle déchéance, lui souffla une petite voix moribonde tandis qu'il soulevait la lunette des toilettes.

La cloche tinta à ce moment précis.

Toute envie de pisser le quitta, et il se rua dehors, le pantalon encore sur les jambes.

Dong, dong, ding, dong ! Martelait la cloche alors qu'il franchissait le seuil de la maison et tentait de repérer l'origine du son. Il faisait très sombre, aucune lumière aux fenêtres voisines. Il supputa une heure tardive, sans pour autant en être sûr car il n'avait pas de montre, et nullement songer à saisir son smartphone, avant de sortir.

Dong, dong, ding, dong !

À travers la nuit, il entendit quelque chose encore et s'immobilisa.

Un moteur tournait au ralenti. Poussif et mal entretenu, certes, mais un moteur tout de même. Abel rit en son fort intérieur. Ah les vieux cons ! Ils l'avaient bien eu avec leur — meuh non, monsieur, enfin, il n'y a plus de bus à Folle-aux-vent ! — Par contre, un bus nocturne dépassait l'entendement. Peut-être que le chauffeur préparait l'engin de bonne heure, pour partir faire sa tournée à l’aurore, ou peut-être…

Dong dong ding dong !

La cloche semblait lui avoir sonné directement dans l'oreille gauche. Une douleur fulgurante lui transperça le pavillon.

Il se tint l'oreille, tentant d'y voir, mais ne distingua que dalle dans cette nuit aux allures de soupe au goudron. Les yeux écarquillés, il finit pourtant par deviner une ombre au souffle de vent qu'elle projeta sur lui, passant au bord de la route, ainsi qu'au bruit du moteur crachotant qui crut et décrut rapidement jusqu'à s'éteindre.

Le silence se fit aussi soudainement que la cloche avait sonné, et bien qu'Abel scruta les ténèbres en dansant d'un pied sur l'autre — son envie de pisser était de nouveau plus forte que jamais — durant de longues minutes, il ne se passa plus rien. Il n'y tint plus et s'en retourna, fonçant à toutes jambes jusqu'aux toilettes.

Ça faisait cinq bonnes minutes qu'il toquait à la porte et la colère lui montait parce qu'il avait aperçu l'homme traverser son potager en passant le nez par dessus le muret. Il cogna plus fort encore sur les planches mal dégrossies et allait recommencer lorsque enfin, elle s’entrebâilla.

— C'est-y que vous voulez tout casser ma parole ! Z'êtes fou mon garçon !

— Je veux descendre à Claive.

— Je ne vous retiens pas, je ne suis pas votre nourrice, allez où bon vous semble, jeune homme, ironisa l'autre.

Il referma la porte aussi sec.

— P\*\*\* ! Abel rugit à travers le vantail plein.

— Mr Léon, je veux prendre le monorail pour Claive ! Ouvrez !

Le vieux bonhomme ne répondit pas. Abel, rouge et trempé ruisselait de flotte. Il tambourina de plus belle. De nouveau la porte livra passage à son propriétaire. Une face plate, aux yeux ronds et dont la bouche aux lèvres épaisses paraissait démesurée.

— Mais vous avez le diable au corps ! Vous voyez pas qu'y pleut ? Lança le conducteur offusqué, son ventre rebondi pointant en avant comme pour faire barrage à l’ennemi.

— C'est quoi le rapport ? Aboya Abel peu disposé à écouter.

— Le rapport c'est que le monorail roule pas par gros temps ! Se moqua Mr Léon, vous avez vu la pente ? Les gorges ? Comme c'est escarpée, la route ? Vous l'avez vu ou non, jeune effronté ?

Abel se rencogna sous la minuscule marquise.

— Mais je dois contacter quelqu'un à Claive…

— Eh bien prenez un ticket, comme tout le monde, et priez pour un peu de clémence météorologique, ironisa le râleur, comme si c'était là l'évidence même.

Abel se frappa le front, où l'interminable cataracte traçait des sillons lui dégoulinant dans le col de chemise, en maugréant.

— Mais je suis vraiment dans un patelin de tarés ! Donnez-moi un ticket !

— Le guichet est fermé! Et la porte se referma, elle aussi, délivrant une pancarte accrochée à sa poignée :

— Ouverture de la billetterie du monorail tous les jours (ouvrables) de 14h à 16 h.—

— Non mais je rêve ! Il est 16 h 10 !

— Revenez demain ! Rien ne presse, le monorail ne roulera pas de ce temps-là, hurla le vieillard à travers la porte. La voix avait perdu toute jovialité, et se faisait plus menaçante.

— Partez à présent ! Répéta-t-il véhément.

Abel, furieux, pataugea dans les flaques jusqu'à se retrouver moitié embourbé au bout de quelques mètres. Remonté à bloc par les réflexions délirantes du conducteur de monorail, il entreprit, sous le coup de la colère, de descendre à pied tout de suite et pas plus tard que maintenant jusqu'à Claive. Ses bottes en plastique, mal ajustées, trouvées dans la remise, clabaudaient d'eau mais il avança résolument sur la route pavée détrempée, le vent fouettant ses tempes, et le repoussant à chaque pas À la sortie du village, il luttait toujours contre les éléments déchaînés, fit encore quelques dizaines de mètres, avant qu'une tuile sortie d'on ne sache où (d'autant qu'aucune n'ornait les toits du hameau) ne heurte son crâne de manière violente, le plongeant dans une inconscience somme toute salvatrice.

— Va mieux le jeunot ? Demanda Mme Fadras, une pointe d'inquiétude dans la voix quand il daigna reprendre connaissance. .

— Gnnnff

— Il va mieux, tu peux m'en croire. Mr Fadras lui tâta le sommet de la tête et Abel retint un cri tandis que sa femme apposait un cataplasme avec une délicatesse à laquelle il ne s'attendait pas. Il se tint tranquille.

— Avez-vous la tête creuse de faire le guignol sous la tempête ? Par ici, quand ça dégringole, faut rester à l'abri, mon garçon, le morigéna le vieux bonhomme.

— On en a chié des ronds de chapeaux pour vous traîner à l'intérieur ! Ajouta Maude se frottant le bas du dos.

Les deux petits vieux portaient chacun le même imperméable jaune vif à large capuche et les mêmes bottines jaunes également. Abel les regarda, quelque peu abasourdi par sa récente mésaventure — on ne reçoit pas une brique volante sur le coin de la caboche tous les jours — et ils lui firent penser à un couple de poussins, de très vieux poussins. Ce qui déclencha un fou rire irrépressible chez le jeune blessé, hoquetant et s'étranglant en même temps, devant ce spectacle comique.

— Il va mieux, diagnostiqua Mr Fadras s'adressant à sa femme d'un air entendu, puis sans transition, prenant congé, outré de l'attitude peu reconnaissant du convalescent.

— Restez ! Lui intima Abel entre deux hoquets.

Mais la porte claqua derrière Mr Fadras sans qu'il se retourna.

— La susceptibilité de la gente masculine, assena Maude en haussant les épaules. Tenez-vous tranquille, jeune imbécile, vous auriez pu vous tuer là dehors !

— Ça ne vous aurait guère dérangé ! Vous n'aimez pas les nouveaux venus, par ici ! Lança Abel en reprenant son sérieux.

— Bougonnez si vous voulez, mais ne refaites plus ce genre d'idiotie ! Je vous laisse, reposez-vous, et quand le temps sera décidé à nous laisser sortir, vous pourrez descendre jusqu'à la ville. Prenez patience !

Elle s'éloignait déjà, quand il ne put s'empêcher de l'interpeller de nouveau sur ce qui le tracassait.

— Vous savez, la cloche sonne la nuit, et j'ai même aperçu quelque chose, et puis ce bruit de moteur que j'ai entendu…

— Ah vous n'allez pas ressasser comme une vieille bigote ! C'est une histoire impossible que vous me contez là ! Le coupa-t-elle revêche.

— Arrêtez de me faire marcher, je sais ce que j'ai vu et entendu ! Grogna-t-il.

— Ne vous mêlez pas de notre village, faites ce que vous êtes venu faire, personne ne vous en voudra, et repartez !

— C'est une menace ? S'exclama Abel surpris par le ton sentencieux de sa voisine.

— Non, non, dit-elle en se radoucissant, c'est un simple conseil. Les autres n'apprécieraient pas…

— Les autres ! Ils ne se sont même pas présentés ! Tout le monde se cache ! Ne me dites pas qu'ils ont tous attrapé une grippe qui les tient au lit depuis que je suis arrivé ! Pourquoi…

— Assez ! Assez ! Scanda Maude. Elle se dirigea de nouveau vers la porte palière, puis sembla hésiter, et sans cesser d'avancer, ajouta :

— Venez dimanche très tôt jusqu'aux Pesières. Vous comprendrez mieux. Mais faites comme si vous nous découvriez par hasard, Mr Fadras ne sera pas content, et les autres…

— Pesières ? Abel n'avait aucune idée de quoi parlait la vieille chouette.

— C'est le grand virage, un peu au-dessus, en montant vers le col, vous ne pourrez pas le manquer.

La porte claqua une seconde fois, et le silence seul répondit à la question d'Abel :

— Mais enfin, qu'est-ce qui se passe ici ?

Samedi, le temps se calma enfin et dimanche, le soleil levant déchira la couverture grise qui recouvrait le ciel. Abel renonça à son petit-déjeuner pour ne pas se mettre en retard, et prit la route. Il regretta de ne pas avoir essayé d'appeler Frances, qui ne s'était pas manifestée depuis plusieurs jours. Étrange. Et puis elle lui sortit de la tête.

La route, étroite, montait méchamment dès la sortie du hameau, bordée d'un côté par un précipice dont on ne voyait pas le fond, — dissimulé par de grands pins — et par la paroi vertigineuse de la montagne de l'autre. Ses pensées se concentrèrent bientôt uniquement sur ses pas, tant il peinait au bout de quelques centaines de mètres franchis. Il douta que les habitants du village, âgés et cagneux, d'après ce qu'il en savait, se trouvent au-delà, mais intrigué par les propos de Mme Fadras l'avant-veille, il poursuivit son chemin, ahanant comme un soufflet de forge. L'oisiveté ne me vaut rien décidément, ronchonna-t-il. L'alcool non plus, murmura sa saloperie de petite voix.

À l'abord d'un énième virage en épingle, il s'arrêta stupéfait. Une dizaine de vieillards se tenaient sur la route, face au précipice. Un cairn imposant fait de bric et de broc, à l'équilibre précaire, semblait se cramponner à l'étroit bas-côté pour éviter la chute. Des fleurs gisaient à sa base, ainsi que quelques bougies qu'un inconnu, accroupi, tentait d'allumer — sans succès —.

Tous les autres se retournèrent aux bruit de ses pas, arborant une mine peu engageante. Abel aurait été bien en peine de traduire leur accueil : surprise, méfiance, colère ? Un peu des trois ? Il prit le parti de jouer à l'imbécile, après tout, c'était ce à quoi ces momies s'attendaient non ? Depuis son arrivée, elles le snobaient, et jouaient au chat et à la souris avec lui.

Seule Maude lui rendit un regard neutre. Elle avait revêtu une chasuble noire, et un cierge encore emballé de plastique pendait de sa main maigrichonne. Comme les autres, également de sombre vêtus, son attitude était emprunte de solennité.

Abel ne se démonta pas.

— Bien le bonjour, habitants de Folle-les-Vents. Je suis bien aise de faire enfin votre connaissance, charmants voisins…

— Z'allez cesser de dégoiser, jeune homme ? Vous voyez pas qu'on se recueille ici ? Le tança Mr Léon, le bidon toujours aux premières loges, remontant ses bretelles comme s'il chargeait une arme prête à ouvrir le feu sur l'ennemi.

— Z'avez pas honte de déranger des braves gens ? Lui lança rondement une mégère volumineuse planté sur de fines pattes d'échassier.

— Ouais, allez faire vot'e promenade ailleurs, étranger, vous dérangez des braves gens, des pénitents, ajouta un vieil homme en redingote, une brassée de marguerites délicatement posées dans un panier d'osier à ses pieds.

— C'est indécent de nous espionner enfin ! S'écria Mr Fadras, vous ne vous faites pas honneur, jeune homme, en vous comportant comme un goujat !

Abel ne pipa mot mais progressa jusqu'au petit groupe sous les récriminations puis se figea devant le cairn. Il n'y avait aucune inscription.

— Vous vous demandez certainement ce qu'est ce monument, dit Maude de sa voix de crécelle.

— Qu'il se demande ce qu'il veut ! Grogna un petit homme en bras de chemise, un béret enfoncé jusqu'aux oreilles.

— Oui, ça le regarde pas, c'est nos affaires, renchérit encore une belle femme dont l'âge indéfinissable ne marquait guère les traits. Ses yeux clairs débordaient de larmes, et elle serrait au creux de sa main décharnée une photographie.

— Il est là maintenant, vous lui devez bien une petite explication, l'est pas mauvais ce garçon ! La contredit Maude, serrant le bras de son mari alors qu'il ouvrait la bouche pour une de ses réparties cinglantes.

— En effet, je ne vous veux aucun mal, soutint Abel, j’aimerais comprendre votre acharnement à m'ignorer, et vous deux, — il pointa le couple Fadras — à me mentir ! La voix d'Abel, sourde, porta plus qu'il ne s'y attendait, et ricocha le long des pierres, s'engouffra dans l'abîme bordant la route, puis remonta, emplit d’échos inquiétants.

Fet… ndre… r… ux… r… r…

Le groupe se resserra autour de Mr Léon, sans un mot.

— Maude ! C'est toi qui l'a envoyé ici, dit vertement Mr Fadras avec un regard lourd de reproches à sa femme.

— Il est assez grand pour manigancer tout seul, se défendit-elle, alors que le jeune homme en question attrapait la photographie de la main de la femme au mouchoir.

— C'est votre époux ? Questionna-t-il sans porter la moindre attention aux chamailleries du couple Fadras.

— Mêlez-vous de vos affaires, aboya l'élégante dame.

—C'était son Maxime, confirma Maude, haussant le ton.

Un murmure de réprobation général accueillit cette révélation.

— Maude tu passes les bornes mon petit ! La voix de Mr Léon grinça comme une vielle serrure, et il se mit entre elle et Abel, dans l'espoir de la faire taire sans doute.

— Je suis lasse de ce secret, Léon, et vous tous, combien de temps encore tiendrons-nous ? Les harangua-t-elle.

— Tais-toi, pour l'amour du ciel ! L'implora son mari.

— C'est en rapport avec la cloche ? Avec le bus ? Insista Abel, sentant que, malgré leurs résistances et leurs allures de molosses, ces gens étaient au bout du rouleau. Qu'est-ce qu s'est passé ? Qu'avez-vous fait ?

— On a rien fait du tout ! Éructa Mr Costa-vagra, l'homme au béret, en lui jetant un regard de braise. Y a eu un grave accident au village, c'est tout.

Abel mit les poings sur ses hanches. Décidément, ces vieux machins le prenait — vraiment — pour un débile ! Il soupira avant de questionner encore,

— Ce n'est pas tout, je le sais très bien !

— On se rendait au col, comme tout les ans au solstice de printemps, commença Mme de Ribeauville, triturant la fine dentelle de sa robe, fixant la photographie jaunie, de ses yeux qui à présent brillaient, larmoyants. Elle était furieuse, mais infiniment triste, constata Abel, sans rancune.

— Et un daim est sorti d'on ne sait où ! poursuivit Mr Léon, marmonnant à peine assez fort pour que sa voix parvienne aux oreilles de tous.

— Le bus à pas eu besoin de plus pour se foutre dans le ravin, continua Mr Francis, les mains dans les poches de sa redingote.

— Et nous qui sommes ici aujourd'hui, qu'on était resté en bas, parce qu'on avait la tourpillante, alors quand on les a pas vu revenir, le soir, on est parti courir la route, et on a trouvé le bus qui finissait de brûler là, Mme Remarque, toujours perchée sur ses longues jambes maigres et arquées de cigogne fit un geste en direction du précipice. Tous brûlés, scanda-t-elle, ou écrabouillés dans ce maudit trou, qui z'ont dit les pompiers bien plus tard, à cause que le bus l'a pas pu éviter un broutard ! Et au travers de ce tragique et succinct compte-rendu, suintait la tristesse et l'abattement de tous les habitants du village.

L'écho avala leurs confidences, n'en retournant que bribes sans aucun sens pleurant dans le vent, balayant la route, et les laissant dévastés.

Us… r… rrr…

— La tourpillante ?questionna Abel.

— La chiasse, si vous préférez, avec une fièvre de cheval que même si on avait voulu, on aurait pas pu se tenir droit ! Aboya Mr Léon. Alors on est pas morts,nous autres, pour sûr, mais ils sont pas d'accord, nos femmes, nos fils, nos maris, qu'on les ai laissé partir tout seuls.

— Oh ! Maude se mit à pleurer bruyamment, suivi des autres, qui se lamentaient et caressaient le cairn en gémissant.

— Ça fait combien de temps ?

— Fait vingt-cinq ans, avoua Mme de Ribeauville.

— Vingt cinq ans ! Abel était abasourdi. Mais enfin, il faut faire votre deuil, braves gens, c'est une tragédie, bien sûr… il était prêt à continuer sur ce même mode lorsqu'il s'avisa que tous le regardait bizarrement. Ou plutôt le regardait comme s'il était idiot.

Et il l'était ! Il se tapa sur le front,

— Mais oui… Évidemment, la cloche ! Le bus qui passe dans le village ! Ils vous appellent c'est ça ?

— Ils veulent qu'on les rejoigne oui, Mme Remarque baissa les yeux, et pâlit. Mais ce sont des monstres, oh Seigneur !

— Y a pu de seigneur, pour sûr z'ont brulés l'église, et tout le reste… ajouta Mr Léon.

Abel compris tout. Enfin, il le crut, ce naïf.

— Vous voulez dire l'église ? L'école aussi ?

— C'était au début. Ils nous ont laissé les maisons, leurs maisons, mais ils sont haineux, ils ne sont pas contents, non, pas du tout contents et ils veulent nous emporter, débita à toute vitesse Mr Fadras, caressant le cairn. Quand le bus fait sa tournée, la cloche sonne pour avertir, et il s'arrête s'il voit l'un de nous, et quelques-uns se sont laissés prendre, comme Simone, l'épouse du chauffeur

de bus. Et même un randonneur, qu'on a jamais revu, ma main au billot qu'il y est passé, happé par la mauvaiseté de ces choses !

— S'acoquiner avec ces monstres, quelle horreur, commença Maude.

— Ces zombies ! Hurla Mr Francis en se cachant le visage. Mon pauvre Romain, ma Charboise !

L'écho parcouru le précipice, s'enfla et s'en donna à cœur joie.

ain… in… ise… ise…

— C'était son fils unique et sa jolie femme, commenta Maude, se mouchant bruyamment. On a tous perdu quelqu'un ce jour-là. Nous, c'était notre fils. Pour une fois qu'il pouvait se libérer un dimanche ! Il travaillait dans l’hôtellerie, à Claive… a voix mourut.

— Des zombies ? demanda encore Abel, suspicieux, même si jusque là il compatissait à leur histoire.

— Oh oui ! Des monstres verdâtres, au teint jaune, ils sont tout pourris, vous pouvez en douter, jeune homme, mais vous avisez pas de les regarder ! Menaça Mr redingote affichant une lippe terrifiée.

—Vous dormiriez plus la nuit ! Poursuivit la cigogne d'un air entendu.

— Bouchez-vous les oreilles quand la cloche se fait entendre, lui recommanda Mr Léon, rajustant ses bretelles autour de son bedon.

— Alors on se réunit là pour leur dire qu'on les oublie pas, et que quand notre temps viendra, eh bien, on ira les rejoindre, conclut Mr Costa-gravas triturant son béret. Bien entendu, on sera heureux de les retrouver.

— Mais pas dans le bus ! Protesta vigoureusement Maude, retenant un sanglot avec difficulté.

— Non, pas dans le bus ! S'écrièrent-ils tous, effarées.

Abel ne sut que répondre et s'abstint de tout commentaire superflus. Il s'écarta des survivants et les laissa se recueillir, puis ils redescendirent ensemble au village, en silence, chacun retournant ses pensées et ses réflexions, mais les gardant pour soi.

Le jeune homme quitta Maude, la dernière à être restée avec lui, en lui promettant de ne rien révéler à personne lorsqu'ils quitterait Folle-aux-Vents. N'y croyant pas lui-même — en ce qui concernait la seconde partie de l’histoire, tout du moins, ces soi-disant zombies —, il n'eut aucun scrupule à s'engager. Toute cette aventure de la journée lui avait mit un coup, et il eut violemment envie de parler à Frances, mais personne ne décrocha. Pourquoi ne répondait-elle plus ? Était-elle si fâchée que cela ? Avait-elle décidé de rompre avec lui ? De jeter aux orties leurs fiançailles et se tourner vers d'autres horizons ? Elle l'avait harcelé au début de sa retraite allant jusqu'à le menacer de débouler sans prévenir et s'en suivait à présent un silence déroutant. Il se mit à broyer du noir, au fond de l'antique sofa, aussi laid qu'inconfortable, songeant aux vieux accablés de douleur le long de la route, leurs confidences, sa solitude, sa débâcle universitaire, Frances, le Dong dong ding dong ! de la cloche et tout se mit à tourner dans son esprit, en une sarabande insupportable, le ramenant sans cesse à son inaction, sa faiblesse, et sa récente découverte au sujet du village. Il se réfugia dans sa déjà — bien mauvaise habitude —, comme il surnommait le fait de boire comme un puits sans fond, et sa méprisable marotte le jeta fin saoul dans les bras de Morphée bien avant minuit.

Dong dong ding dong ! Dong dong ding dong ! Dong dong ding dong !

Abel émergea brutalement d'un semblant de cauchemar, où un chevreuil portant une casquette bleue conduisait un autocar tandis que tintait frénétiquement une cloche qu'Abel agitait dans sa main, assis sur un strapontin déglingué, d’où dépassait de l'assise une mousse verdâtre semblable à de la moisissure.

Dong dong ding dong ! Dong dong ding dong ! Dong dong ding dong !

Abel se redressa, saisit son jean et enfila ses sandales sans même réfléchir.

La cloche sonna encore.

Il reconnut au loin un bruit toussotant de moteur, et n'hésita pas une seule seconde.

Le bus !

Déboulant hors de la maisonnette, une nuit noire comme l'encre l'accueillit. Le vrombissement s'accentua, l'engin montait la pente, devina-t-il, et ne tarderait pas à passer devant l'arrêt de bus. Le garçon s'y rendit aussi vite qu'il put, tâtonnant et battant l'air devant lui pour ne pas se casser la figure sur une mauvaise pierre — ou une autre tuile volante —. Il sentit bientôt sous sa main un des poteau vermoulu de l'abri. Et deux phares trouèrent soudain la nuit, scrutant le chemin comme pour le débusquer.

Dong dong ding dong ! Dong dong ding dong ! Dong dong ding dong !

Les tintements lui semblèrent plus fort que les autres fois. Le moteur crachota, et à travers les vitres du bus, Abel vit des ombres se mouvoir dans l'allée de l'autobus, elles étaient au moins une douzaine. Il ne pu retenir un cri de surprise. Une sueur froide lui coula soudain le long de la colonne vertébrale, et ses cheveux, il l'aurait parié, se dressaient en partie, comme sous le feu d'un courant électromagnétique. Il plissa des yeux, tentant de résister à la peur. Le bus parcourut les derniers mètres avec une rapidité qui le surprit, se gara proprement le long de la guitoune, les freins geignirent et les portes en accordéon s'ouvrirent dans un chuintement sinistre, suivi d'un claquement, et ses narines, dilatées par un effroi lui remontant le long de la gorge jusqu'à le faire suffoquer, inspirèrent alors un souffle de vent putride.

La cloche avait cessé.

Le moteur tournait au ralenti, de manière anarchique, tout prêt de rendre l'âme, et des râles parvinrent alors aux oreilles dressées d'épouvante de Abel.

Il eut voulu tourner les talons,et courir jusqu'à perdre haleine, alors qu'une main d'un vert de vase se tendait vers lui et l'invitait maintenant à franchir les quelques marches menant à l’intérieur du bus.

Transi par une peur animal, il suppliait à ses yeux de ne pas regarder, de ne pas voir, car à cette main, il avait reconnu, brillant dans la pénombre, sous la simple clarté des lumignons de l'autocar, la bague de fiançailles de Frances. Au doigt — cet os brunâtre ressemblant à un doigt — de cette chose montreuse, squelette dégingandé recouvert de chairs mortes, chose dégoulinante aux parfums de latrines, et sa tête ! Oh Misère ! Cette tête fracassée d'où sortait un limon infâme aux relents de vielle marée. Et tous les autres, assis sagement à l'arrière du bus à présent, qui attendait, grognant, soufflant, le suppliant de les rejoindre.

Frances saisit la main de Abel, elle croisa son regard de son œil unique et crevé, ses lèvres déchiquetées semblaient sourire. D'une voix gutturale et immonde, la créature implora le jeune homme, et du geste l'invita à monter et lui, grelottant de peur à présent, grimpa d'un pas pesant dans le bus de l'horreur.

— L'est parti le môme ?

— Ch'ai pas, ses affaires sont là, mais je l'ai point vu depuis ce dimanche. Bien possible, répondit Maude, en train de désherber le pas de sa porte. Tu rentres boire un café, le temps que Roland décharge la mule ?

— Parbleu, une autre fois ! Mauvaise nouvelle, que j'apporte, les anciens, reprit l'homme, — songeant à la goutte à laquelle il allait fallait devoir renoncer — en montant, j'ai aperçu une voiture fracassée à mi pente, dans le ravin. Faut pas que je m'attarde, soupira Tintin, et quérir les pompiers de Claive.

— As-tu pu voir quelque chose sur le lieu de l'accident ? Demanda Mr Fadras, rejoignant sa femme dont il croisa le regard alarmé.

— Y a pas d'accès mon Roland, tu connais le paysage ! Juste vu un sac à main gros comme un cabas, et une valise rose éventrée avec des frichtis, jupes et chemisiers, escarpins et j'en passe, qu'ont volé dans les branches des pins rabougris alentour, poursuivit le dénommé Tintin, tirant un âne bâté armé d'une mauvaise volonté évidente. Bah, une bonne femme au volant, dans le col de la bougeotte, tu penses bien que la catastrophe était prévue d'avance ! Conclut l'homme en tirant sans empathie sur la longe de la bête.

Et le couple se retint de répondre.
