---
title: Trafic
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Un casse qui tourne mal . "
epub: true
pdf: true
draft: true
---
— Suivant !

Un grand type malingre s'approcha du comptoir où trônait Gypz, le tenancier de cette drôle d'échoppe, assis sur un voltaire, ses grosses fesses molles calées entre deux coussins. Son menton, triple, tremblotait comme de la gelée de groseilles, mais tout cela n'était qu'apparences trompeuses. La main de l'homme était sûre quand il fit signe à l'étranger d'avancer et ses petits yeux chafouins dégageaient une fine et cruelle intelligence.

— B'jour.

Accent du sud, se dit le négociant.

— M'a dit qu'vous cherchiez des partenaires…

On t'a lourdement trompé en ce cas, je cherche des cambrioleurs. Et des bons.

— Ouais, Y a qu'à vous renseigner, patron, moi c'est Houaro, ch'ui votre homme.

Un rire de hyène sortit de ses grandes bouches superposées, tandis que son œil unique affrontait le regard de l'autre.

— Je veux des gens discrets.

— J'le serai si c'est nécessaire, répondit le svelt du tac au tac.

Gypz se doutait que non, les svelt avaient une sale réputation dans le coin, et on en retrouvait parfois au petit matin flottant le long des docks, dans un sale état, ou tout bonnement mort. C'était surtout dû à leur sale caractère, des créatures belliqueuses, sournoises… Mais très habiles avec leurs pattes…

Son interlocuteur semblait suivre le cheminement de sa pensée, il agita ses pinces devant le négociant, s'impatientant.

— Y a toutes les chances d'y rester.

— Y a une chance de gagner l'pactole non ?

Il avait de la suite dans les idées, celui-là. Gypz rit de bon cœur, faisant tressauter toute sa graisse autour de ses bras, de son cou, de son ventre et l'autre l'accompagna un instant. Ils redevinrent sérieux de concert, se jaugèrent puis le négociant lui tandis son moignon.

— À qui se le dit, nul dédit.

— Vieille formule, mais ch''ui d'accord avec ça. Il tendit une de ses pinces.

— Suivant !

Une jeune fille se prosterna devant le comptoir. Sa chevelure d'un blond de blé mûr comme il n'en existait plus, ondulait, vague marine, autour de son joli minois. Quinze ans peut-être. Sauf que cette podedgar avait largement passé la centaine, et sans conteste dans une autre galaxie, réalisa Gypz.

— Quel talent mets-tu en exergue pour candidater aujourd'hui ?

La — fille — se redressa promptement, aussi vive qu'un chat, et, appuyant sur sa nuque du bout de sa mignonne menotte, se ratatina soudainement devant le maitre d’œuvre, imperturbable.

Ces étrangers ! Ils croient toujours que leurs tours de passe-passe vont suffire à m’éblouir… ongea Gypz.

— Et tu comptes m'être d'une quelconque utilité dans ta forme initiale ? Demanda-t-il d'un ton mordant à la petite bestiole, pas plus haute d'un criquet, et lui ressemblant de manière frappante, qui se tenait à présent devant lui.

Une voix métallique s'exprima, produite par le frottement des antennes de la créature.

— Ma petitesse te sera utile, oui, ainsi que mes tours.

— Je connais tes tours, tu n'as rien qui m’intéresse, passe ton chemin, podedgar !

L'insecte grogna comme un ours, et l'air devint électrique. Gypz sentit ses rares cheveux se dresser sur sa tête.

— Faquin ! Couina le marchand, écrasant vivement de son pied l'indésirable sans plus de politesse.

— Belle saloperie hein ? S'exclama dans son dos une voix autrement charmeuse.

Merde ! Je me suis laissé distraire, se morigéna le négociant. Puis laissant s'exprimer son dépit,

— Saloperie de swerg !

— Saloperie toi-même, et négociant, mon cul. Que vas-tu trafiquer sur les docks, mon bon prince des bas-fonds ?

Un coutelas large d'une bonne main et long de deux caressait le cou de Gypz. Il se figea comme du beurre frit dans une poêle froide.

— Eh bien l'ami, discutaillons-en face à face.

Et il se prépara à l'affronter À l'affronter visuellement. Les schwerg étaient vraiment horrible à voir, ils peinaient à se contenir à cause de la pesanteur, et leur enveloppe, d'un blanc de champignons des prés, et du même velouté, dégoulinait sans cesse, produisant des vapeurs écœurantes.

La créature chuinta jusqu'au tabouret du comptoir, délaissant l'encolure grasse de son futur employeur.

— Y a quoi dans ce fort-péniche ?

— Peut-être rien.

— Aussi vrai que mon nom est Podeza, ta langue me crache de la merde ! Et il cracha sur le comptoir. Une matière blanchâtre s'y étala, puis se rassembla et enfin rejoignit le corps fantasque d'où elle avait été expulsée à peine un instant plus tôt.

— Arrête tes tours, ça ne prends pas !

— Arrête tes bobards, ça ne prends pas non plus !

— Le navire est à l'amarre depuis près de cent ans, reprit Gypz, comme s'il poursuivait une conversation courtoise, débutée il y a fort longtemps, « Gardé comme la dernière noisette de l'humanité par une armé d’écureuils nostalgiques »

— Des écureuils armés jusqu'aux dents. Me semble avoir aperçu quelques belles batteries de cuisine à l’extérieur.

— Deux mitrailleuses Vickers de 12.7 mm et une Lewis Mark I de 7.62 mm oui, approuva le négociant, « et des tours de garde à grand renfort de dissek à ne plus savoir compter, et des chiengres »

— Putain ! Ça, je déteste ces vacheries ! Quand elles plantent leurs crocs dans ta chair !

S'il était revenu d'une telle expérience, (les chiengres possédaient des canines et une queue empoisonnées, et il était de notoriété publique qu'on survivait plus que rarement à leur attaque…) peut-être était-ce là un larron à recruter d'urgence, se dit Gypz.

— Une dernière question, l'ami, poursuivit-il imperturbable.

— On progresse, j'entends, le coupa l'autre, sa peau-corps gluante glissant sur le bar dans un bruit de succion en direction du moignon de son interlocuteur.

— Tous ne reviendront pas, dit encore Gypz, retirant vivement son membre tronqué du comptoir dans un geste instinctif de dégoût.

— J'en reviendrai moi ! Aboya l'autre.

Et ils conclurent là leur affaire.

— Je vous présente Naca, elle se chargera des gardes Dissek, dit Gypz sur un ton aimable.

— Putain de gardes ! Explosa Podeza.

Naca gloussa, s’ouvrit en éventail, du haut en bas, jusqu'à être aussi large qu'une armoire normande puis se rétracta gracieusement, devenant aussi fine que la tranche d'une feuille de papier.

— Naca est fine herboriste, alchimiste à ses heures, elle créé des parfums… dont on ne revient pas, précisa le gros négociant.

— Les gardes se méfieront ! Prévint le svelt, « ils ont le cerveau en compote, mais ils ne sont pas pour autant totalement stupides nos petits soldats recyclés ! »

— Les gardes seront déjà mort quand ils me distingueront, susurra l’alchimiste — sans doute une race des régions limbiques de la ceinture d’Orion — supputa Houaro, tandis qu'elle lui pinçait l'oreille de ses doigts fins comme des lames : le svelt ne l'avait pas vu venir.

— Doucement ! cria-t-il de ses nombreuses bouches aux lèvres charnues.

Naca redevint visible, éventail de couleurs et de formes géométriques réunies en une structure complexe d'à peu près un mètre de côté.

— Tu m'écrases les pieds ! Protesta encore Houaro sans formaliser sa complice.

— Et il fait quoi la pieuvre ? Interrogea Podeza d'un air narquois, imitant de sa texture molle un point d'interrogation.

— Il coupe les canons des mitrailleuses, répliqua Houaro hargneux.

— Coupe ! Coupe ! Naca piailla le mot à plusieurs reprises en se tortillant autour du svelt de façon tout à fait veule et ridicule.

— Cesse ! Houaro lui claqua une pichenette qui la fit se rétracter. Elle se perdit dans le décor, sans épaisseur.

— Tu peux faire ça, le poulpe ? Podeza, dubitatif, se mit à tâter les bras de son compagnon, sans que celui-ci ne bronche une seule de ses pinces.

Quel stoïcisme, le félicita intérieurement Gypz, réprimant un rot de dégoût. La vue du swerg lui était difficilement supportable. Il savait que l'autre savait, et abusait d'effets visant à le provoquer. Il résistait en l'ignorant de toutes les façons possibles, et la principale était d'éviter de le regarder sauf en cas de nécessité absolue.

— Bien sûr que j'peux ! Ch'ui pas une brêle mollasse moi ! Fanfaronna Houaro.

Le svelt avait lâché sa bombe l'air de rien, mais le susceptible swerg les aspergeait déjà de sa substance constituante infecte. Gypz prit conscience qu'une trace de la mixture s'insinuait entre ses dents. Naca se mit à éventer à toute vitesse pour les débarrasser de ces incongruités, pendant que Podeza déliquescent, riait de manière lascive et appuyée, montrant par là le peu de cas qu'il faisait d'eux.

— Nous devons travailler ensemble, cessez vos enfantillages ! les morigéna le négociant.

— Drôle de mots, un total non sens ! éructa le swerg, récupérant ses particules précédemment jetées aux quatre vents. « les nôtres ne sont jamais des enfants ! » crut-il bon d'expliquer.

— Les hommes ont des expressions d'hommes, les autres n'ont rien. Laissons ces chamailleries langagières, et finalisons notre plan, bande d'idiots ! explosa Naca retrouvant soudain une taille respectable, un teint d'un orange agréable, et une apparence mi humaine, mi simiesque sur un fond de plis et de contre-plis.

— Podeza s'occupe des chiengres.

Tout les quatre se turent.

C'était la partie la plus délicate, considéra le négociant, observant tour à tour ses partenaires. Et la plus dangereuse aussi. Le point de non retour. Ils pouvaient tous crever à cette étape. Les chiengres étaient au nombre de trois ou quatre, selon ses meilleurs informateurs. Ils hantaient l'entrepont et la cale.

— Podeza va se faire bouffer sa chair molle, glouglouta Naca, brillante comme un soleil.

— Podeza va se faire bouffer les couilles, oui, se gaussa Houaro, hilare.

— Cessez, j'ai dit, glapit leur employeur sur un ton sans réplique. Nous verrons demain qui bouffera qui !

Le swerg grimaça par devers son apparente flasque consistance, mais ne répliqua pas. Naca se réfugia, fine comme un fil de soie, derrière le bar, et Houaro se nicha sous une table, ferma son œil unique et plongea instantanément dans un sommeil sans rêve.

Les rêves aussi appartenaient aux humains —Autant dire qu'ils n'étaient plus légion —. Gypz enfourna distraitement une fourchetée de moussou, cette pâte protéinée abjecte contenant les nutriments essentiels à sa survie, dont les gens du commun en était arrivé à se nourrir faute d’autres choses à ingurgiter — dans un contexte de pénurie totale, faute de grives, on mange des merles — se dit l'homme, puis il s'avisa qu'il ne savait pas ce qu'était une grive, — ni un merle —.

Une aube grise les enveloppa dès leur arrivée sur les quais le lendemain. Le fort-péniche en question tanguait paresseusement à la bite d’amarrage numéro 36911122. Gypz fit signe à Houaro. Le svelt passa immédiatement à l'action tandis que les autres avançaient à couvert. Les tourelles du navire, immobiles, semblaient hors d'état. Une rouille lépreuse les rongeait sur toutes leurs surfaces. Les canons des mitrailleuses ressemblaient à des yeux aveugles et morts. Hormis le clapotis de l'eau sur la coque, pas un bruit. Le groupe se planqua à l'abri d'un container, à quelques encablures du navire. Houaro rampa, invisible dans les ombres qui s'étendaient encore, sa peau sombre se noyant à merveille dans le paysage. Ses grands bras caoutchouteux le projetèrent d'un coup sur le capot d'une tourelle dont il sectionna le canon avec une facilité déconcertante, puis il se projeta sur la seconde alors qu'elle pivotait sur son axe à la vitesse d'un escargot. Le second canon tomba dans un bruit de casserole, et le svelt était déjà perché sur la troisième tourelle, qui opérait elle aussi un lent mouvement de rotation, grinçant de tous ses antiques rouages. Rendue impuissante en deux vigoureux coups de pince, elle n'émit bientôt plus un bruit. Houaro leur fit signe, il allait falloir maîtriser les gardes, et vite : il distinguait déjà du raffut à l’intérieur de la péniche. 

Naca sortit de sa cachette, se replia sur elle-même et disparut à la vue. Houaro, accroché à la dernière tourelle, comme un crabe sur un rocher, ne mouftait pas.

Un boucan de tous les diables parvint à leurs oreilles alors qu'ils désespéraient de la réussite de l'alchimiste, mais celle-ci réapparut bientôt, l'éventail ouvert, haussant ses brins, toute d'écarlate vêtue. Un relent ferreux les frappa aux narines, pour ceux qui en possédaient, et le silence régna de nouveau.

Gypz et Podeza se décidèrent à rejoindre leurs acolytes, et c'est ensemble qu'ils déboulèrent devant la porte de cale du fort-péniche. Elle était enclose dans le pont du bateau. Faite d'acier, elle ne comportait aucune altération du temps, une simple lumière rouge clignotait aux quatre coins, comme le battement d'un cœur immortel.

Podeza dégoulina de tous cotés, et s'infiltra dans la cale par d'invisibles interstices. Les autres s'immobilisèrent autour de la porte. Les lumières cessèrent de clignoter, un aboiement rugueux et sauvage s'éleva de dessous leurs pieds (pour ceux qui en avaient), Gypz leva un sourcil, et rencontra le regard cyclope de Houaro :

Les chiengres passaient à l'attaque.

Naca souffla, l'instant propice à la confidence :

— Il s'en est fallu de peu qu'ils me reluquent de trop près ces saletés ! Ils sont furax !

Leur commanditaire imposa le silence d'un simple moignon levé.

Dans l'entrepont, Podeza se coula de couloirs en couloirs en direction de la cale et lorsqu'il entendit les chiengres, s'aplatit et se figea immédiatement. Une bête déboula, reniflant bruyamment l'air aux méchantes effluves de renfermé. Sa tête, couronnée de petites oreilles arrondies, pivotait en tout sens espérant découvrir l'origine de tout ce chambardement. Un second chiengre le rejoignit, mais resta prudemment en retrait pour assurer leurs arrières. Podeza sentit les pattes énormes du premier monstre lui griffer l'ectoplasme en rasant les parois du bateau. Il sentait la puissance de la bête à travers le cliquetis de ses mâchoires qui s'ouvraient et se refermaient sans cesse, prises de frénésie, il évalua son poids aussi, et sa musculature nerveuse. Un troisième chiengre hurla depuis les profondeurs de la cale, invisible.

Podeza patienta, il lui fallait engluer les deux dans son piège, il n'aurait pas d'autre occasion. Les bêtes gémirent, se concertant dans une série de cris plaintifs et assommants. Leur pelage rayé chatoyait dans la pénombre et leur yeux dorés fouillaient chaque recoin avec méticulosité. Podeza attendit encore. Les deux chiengres firent un pas, puis un autre, battant les parois métalliques de leur longue queue à la pointe venimeuse, tout comme leur dentition. Elles s'impatientaient. Podeza attendit. Ses adversaires grognèrent, avançant tout de même, mais d'un pas prudent.

— Pas assez —.

Podeza réunit son entité molle par dessus les bêtes, les englobant dans sa masse dense et se mit à resserrer ses pores, aussi vite qu'il lui était permis. Il lui fallait les étouffer, et vite. La matière infecte qui composait le swerg s'infiltra dans leurs gueules et s'y durcit avant qu'ils ne puissent la refermer pour mordre, de même que leur queue ne put battre cette gelée puante, à la fois élastique et terriblement ferme. Même lutter pour leur vie fut en un instant une impossibilité physique. Ils moururent sans trop protester tandis que la silhouette du rescapé se profilait le long de la passerelle inférieure. Cependant Podeza devait s'accorder un répit pour se regrouper, assainir sa forme, et s’immiscer dans l'escalier, où la bête, là-bas, — plus maligne que ses congénères — n'avait pas initié un seul mouvement.

Podeza retrouva sa totalité d'être et de matière. Avisant un trou, dû à un vilain boulon desserti, il s'infiltra dans ce minuscule passage, pour se retrouver en suspension juste à l’aplomb du dernier gardien du fort-péniche. L'instinct de celui-ci ne le trompa guère, il feula, sentant sans la débusquer la présence belliqueuse et étrangère. Le swerg se laissa tomber comme un filet de pêche sur le râble du monstre, qui malgré l'effet de surprise, piqua de sa queue la texture molle de son agresseur avant que celui-ci ne l'enserre et ne l'asphyxie instantanément.

Podeza poussa un hurlement strident qui se répercuta à travers tout le bâtiment. Les trois autres déboulèrent bientôt, pour assister à sa dernière liquéfaction. Ce n'était pas là un joli spectacle, d'autant que des vapeurs plus écœurantes que de coutumes s'échappaient du swerg, étalé comme un vieux chewing-gum sur le caillebotis de la cale.

— Peut-on faire quelque chose pour toi ? demanda Gypz, pragmatique.

— Gnfff… urmura le swerg au comble de la souffrance que lui infligeait le poison.

— On comprends rien !s'impatienta Naca.

— Y va crever, allons-y ! conclut Houaro, peu enclin à s'apitoyer sur quoi que ce soit.

— Gnfff… eprit l'agonisant.

— Bien joué mon pote, mais j'te l'avais dit, hein, qu'y te boufferaient les couilles ces bestiaux des enfers ! crut bon de rajouter son (ex) partenaire, enjambant ses restes sans plus de façon.

— Gnff… soupira Podeza expulsant des bulles puantes sur toute sa surface qui ressemblait à s'y méprendre à une galette de blé noir marbrée de cratères sombres.

— Paix à son âme ! s'exclama Gypz dans un élan insoupçonné de compassion.

— Je ne pense pas qu'il en ait jamais possédé une, soupira Naca, claquant des panaches de sa forme éventail avec dédain.

La dépouille gisait entre eux, plus flasque qu'à l'accoutumée, et plus répugnante encore. S'absolvant d'un recueillement somme toute superflu, — ils n'étaient en rien proches amis — les trois compères détalèrent dans les entrailles du fort-péniche.

Les caissons s'empilaient à fond de cale, tous plus décevant les uns que les autres : Ils ne contenaient que du papier. Des milliers et des milliers de pages jaunies et noircies de minuscules caractères rendus indéchiffrables par l'humidité et les ans (et le fait notable qu'aucun parmi les trois n'était savant érudit).

Naca refit le coup de l'armoire normande, écrasant quelques classeurs d’où une fine poussière et une odeur entêtante de moisie s'échappa sans lui arracher pourtant le moindre repli.

Gypz n'en démordait pas et arpentait la cale à la recherche d'un compartiment secret, d'une cache, ne pouvant ce résoudre à un tel désastre. Sa réputation s'en trouverait amoindrie. La ville se foutrait de lui pendant des lustres derrière un tel échec !

Houaro, imperturbable, poursuivait inlassablement la tâche, fort ingrate, de découpage des casiers À chaque ouverture, leurs regards se remplissait d'une brève lueur d'espoir, puis le svelt répandait des confettis sur le sol en guise de manifestation de contrariété.

— Ah ! Ça sonne le creux par ici, les amis !

— Amis ? Peu me chaut ! Bâtards ! S'enhardit Naca, après tous les tracas qu'elle avait traversé depuis l'aube sans réconfort aucun.

— Venez, c'est creux, vous dis-je ! Le négociant frappait la coque, et effectivement, elle rendait un son peu commun.

— Blindé ! S'exclama Houaro.

— Ou le métal est étranger, supputa l'herboriste s'avançant à tâtons, sur la tranche.

— Je peux couper ces boulons ci, mais pour ceux là… il s'attelait cependant déjà à la tâche.

— Tu vas les avoir sans problème, l’encouragea Gypz, excité par sa découverte, son corps adipeux tremblotant légèrement.

— Sans problème mon grand, murmura Naca, éventant le svelt rougissant sous l'effort.

Les pinces claquèrent et claquèrent encore, sans pour autant de résultat. Naca pouffa, se fit lame, scalpel aux pouvoirs venus d'ailleurs, miroitant dans la pénombre putride. Les derniers boulons cédèrent, offrant à leur vue un coffre-fort de facture ancienne, dont l'ouverture s’exerçait par une combinaison de chiffres sur une molette et un volant-manivelle.

— Des chiffres ? Interrogea Gypz dubitatif.

— Quels chiffres ? Répéta Naca comme un perroquet.

— Et si on ne connaît pas les chiffres ? Questionna raisonnablement Houaro.

— Aucune importance, je vais nous concocter un petit acide qui rongera toute cette ferraille en un rien de temps, assura Naca, renflant des épaules, se donnant un — faux — air de catin.

Le svelt la regarda de travers, se demandant pourquoi elle n'avait pas usé de ses talents pour décimer les boulons récalcitrants dès le début… En retour, le regard de sa comparse, éventail rose, dont la chair offerte pulsait doucement, lui offrit une réponse d'une claire évidence, qu'il n'apprécia guère. Il déglutit — comme un homme — et l'ignora de même.

Naca sortit de ses dessous froufrouteux quelques fioles, les mélangea, se replia, se déplia, s'éventa puis enfin leur tendit un petit pot emplit d'une substance mauve.

— Attention ! Ça dépote ! N'en fichez pas partout, soyez minutieux, précisa-t-elle en leur tendant la mixture.

— Fais-le toi même, se dédouana Houaro, fatigué de ses manières.

— Pas question ! J'ai plus que ma part à mon actif. Et sur ces paroles, elle se fit subtile, épaisse d'un cil, Houaro la perdit de vue.

— Suffit vous deux ! Trancha leur employeur, se saisissant de l'ampoule pour en répandre le contenu sur la roue chiffrée dont ils ne pouvaient venir à bout.

Le métal se mit à bouillir aussitôt et Gypz put manipuler le volant sans difficulté notable. La porte bailla.

— Encore de la paperasse, s'écria le swerg la mine dégoûtée.

Naca réapparut, feuille de papier de soie multicolore, se froissa, ne dit rien, puis s'éventa à grand bruit faisant voleter quelques pages. Oiseaux blancs dans un ciel d'encre.

Gypz en saisit une à son envol, décryptant tant bien que mal le vieux parler de l’ancien continent.*** — Archives de l'agence centrale de renseignements concernant l’assassinat de JFK le 22 novembre 1963 —***

Il soupira, mal à l'aise.

ça ne lui disait rien du tout.

Il saisit un passage plus loin, souligné d'un rouge que les ans avait délavé, sans pour autant l'effacer.

— Trois pour cent (3%) des documents concernant l’assassinat de JFK n'ont jamais été publiés. Ils sont réunis ici. Leur divulgation ne pouvant être envisagés, la CIA s'est engagée à protéger ces documents classés secret-défense jusqu'à la fin des temps dans ce fort-péniche. —

—Y se pourrait qu'on y soit à la fin des temps ! S'énerva Houaro en gesticulant de toutes ses pinces.

— 

— On s'en fout de ces vieilles histoires, c'est qui ce JKF d'abord ? S'interrogea Gypz.

— JFK, un horrible humain sans doute, hypothéqua Naca, suçotant ses bouts colorés d'un air absent.

— Et pourquoi pas un svelt ? Protesta avec véhémence Houaro au comble de la frustration.

— Parce qu'aucun horrible svelt n'était là il y a un siècle ! Réfléchis crétin ! Naca claqua ses panaches, refermant d'un coup sec plis et contre-plis, puis, se tenant sur la gorge, roula loin d'eux, s'affina dans l'ombre et disparut dans un soupir lassé.

Les deux autres chiffonnaient toujours les feuillets, incrédules.

— Autant d'histoire pour 3 % d'un truc secret dont on ne se souvient même plus ! C'est bien ma veine ! Se lamenta le négociant.

— Attend ! Y a une autre cache ! Écoute, vieux rabat-joie ! Trompeta Houaro un air de triomphe à ses multiples bouches.

Il cogna contre la coque de ses pinces et effectivement, la paroi rendait un son creux.

— Tu vas arriver à tout couper ? Interrogea Gypz soucieux de la taille imposante des différents boulons verrouillant le panneau.

Houaro sourit de ses bouches à ressort, et agita la fiole qui contenait encore un reste d'acide.

— Avec l'aide de ceci, sans doute !

Ils s'acharnèrent, et un autre coffre-fort, plus imposant, se fit jour dans le renfoncement de la paroi. L'inquiétude et la fébrilité des deux compères engendrèrent de nombreuses questions d'une fatuité navrante.

— […]

— On l'ouvre ?

— Des papiers ?

— On verra bien !

Ils versèrent le contenu de la petite bouteille sur le mécanisme, qui fut rapidement dissous — Naca possédait un vrai talent de perceur de coffre-fort —, et de nouveau, une porte s’entrebâilla sans hâte.

Mais courte fut leur exaltation : un long feulement très alarmant s'en échappa tout aussitôt, suivit du claquement d'une mâchoire — qui à l'entendre, abritait une ribambelle de dents —, le tout traduisant la colère la plus noire d'une bête ayant fait le deuil récent de sa famille entière.

Le quatrième chiengre ! Songea Gypz.

— Tu avais dit trois ! lui reprocha Houaro battant des pinces à reculons.

— J'avais dit trois ou quatre ! Se défendit le gros négociant, tremblant de toutes parts comme un jelly cake, — la couleur en moins —.

Et sans se concerter, hurlant comme des putois, ils prirent leurs jambes à leur cou (ou ce qui pouvaient en faire office).
