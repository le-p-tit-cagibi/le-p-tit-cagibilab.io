Il y avait des pavés mouillés, l'odeur de la pluie, un ciel gris. Il s'est approché de moi en disant :

— Manque le rituel habituel…

Et il a ouvert les bras.
Sa veste beige, ample et moelleuse, chaude de l'empreinte de son corps m'a accueillie toute entière.
C'était réconfortant.
Je lui ai répondu, tandis que je serrai cette grande silhouette décharnée,

— Je t'aime. Je t'aime tellement !

Un amour fraternel. Immense et minuscule, doux et frustre, puissant et fragile. Un amour qui coulait en moi, ruisseau clairet, chantant, filant entre deux rives étroites de sous-bois.
Je ne bougeai pas d'entre ses bras.

J'oubliai tout.

Il se mit à parler doucement.

— Je pars demain.

Et je sus que je le voyais pour la dernière fois.

Tout avait commencé dans un vacarme assourdissant. La minute d'avant, je m'apprêtai à descendre au restaurant pour le petit-déjeuner. La minute d'après, mon corps ne me répondait plus, et une poussière fuligineuse m'empêchait de déglutir. Ma bouche sèche et râpeuse comme du papier de verre transformait mes cris en coassements ridicules. Autour de moi, des décombres aussi loin que porte mon regard : plaques de béton armé fendues, poutrelles d'acier tordues, et le plus terrible, une irrespirable mixture composée de particules de matières isolantes, de plâtre, ciment broyé et que sais-je encore, qui rendait l'air palpable, presque compact. Je m'asphyxiai, coincée sans savoir ni comment, ni pourquoi, ne pouvant bouger.

Fébrile, je tentai de me dégager en lançant des coups de pieds sans force et tapai dans des choses molles sous moi, que je n'identifiai pas mais sur lesquelles il m'était impossible de prendre appui pour progresser vers le haut. Du moins espérais-je que ce fut le haut… J'avais un besoin irrépressible de respirer et paniquais de ne pouvoir le faire.
Je m'obligeai à l'immobilité un court instant et écoutai alentour, guettant un appel, une activité quelconque. J'aspirai à petits coups.

— Rien —

Un ruissellement de débris s'abattit sur mon dos. Je hurlais.
M'étouffai.

Il s'adressa à moi comme si nous nous rencontrions par hasard au coin d'un square, un jour de printemps, d'une voix posée, rassurante.

— Ohé ! Vous n'êtes pas seule. Essayez de taper dans vos mains, que je me guide vers vous.

— Où êtes-vous ?

L’écho de ma voix éraillée et terrorisée me fit dresser les cheveux sur la tête.

J'attendis.

— Je crois que je suis derrière vous. Ou au-dessous. Continuez de vous signaler, je viens vers vous.

Je fit l’inventaire de mes maigres possibilités : une de mes mains se trouvait sous mon ventre, bloquée, je ne pouvais l'utiliser, mais de l'autre, je saisis avec ferveur une brique et frappai de toutes mes forces les gravats qui me cernaient. La poussière devint plus dense encore. Je piétinai pour m 'élever, mais patinait lamentablement sur la surface molle du dessous sans avancer d'un iota.

— j'y suis.

Sa voix me saisit en même temps qu'on me tirait vigoureusement sur le pied gauche.
Je hurlai de nouveau.

La voix ne se départit pas de son calme,

— N'ayez pas peur. Je vais vous aider à reculer jusqu'à moi.

— Reculer ? Non, non ! Je paniquai de plus belle, tendant le front vers ce qui me semblait logiquement devoir être le ciel, la liberté, la vie.

— Notre salut est dans les caves, j'en viens. Elles ne se sont pas affaissées, si nous devons nous en tirer, ce sera en plongeant vers les entrailles du bâtiment.

— Je suis coincée, m'exclamai-je dépitée.
Et je m'agitai en tout sens sans obtenir aucun résultat.

— Laissez-vous faire. Je vais tirer sur vos pieds, et simplement, si vous le pouvez, poussez en prenant appui sur ce que vous trouverez devant vous.

— Ça va s'écrouler !

— Voulez-vous rester là à attendre la mort ?

Sa question me fit l'effet d'un pavé lancé dans mon estomac. Tout mon corps se contracta sous la violence de cette image de moi enterrée, sans autre espoir qu'une fin rapide.

— Ne me laissez pas suppliai-je d'une petite voix.

— Qu’avez vous dit ?

— Tirez !

Je sentis ses mains glacées saisir mes pieds et la traction qu'il exerça me soutira un cri de souffrance. Mais je m’étais déplacée de quelques centimètres.

— Ça va ? La voix inquiète interrogeait cette pièce de viande que j'étais. Molle et impuissante. Je rassemblai toute mon énergie et poussai de ma main vers lui, là derrière.

— C'est bien, mais faisons ensemble. Quand je dis on y va, vous poussez et je tire, d'accord ?

— D'accord.

Il affermit encore sa prise sur mes mollets et compta.

— un deux trois, on y va !

D'un coup sec, je gagnai plusieurs centimètres, au pris d'une éraflure à la joue qu'une tige en ferraille arracha à mon visage comme prix de mon passage. Du sang coula dans mon œil, mais je n'y prêtai pas attention.

— Un deux trois, on y va ! Répéta-t-il encore, la voix égale.

Nouvelle coulée de poussière. Je l'entendis tousser et cracher longuement.

Silence.

— Ça va ? Cette fois, c'est moi qui ai crié la question.

Je m’aperçus que mes reptations avaient libéré mon autre bras. Je ressentis une joie indicible à me confronter à cette si humble victoire.

— Encore un effort, vous allez bientôt sortir de ce goulot. Un deux trois, on y va !

La douleur irradia mes hanches tant il manoeuvrait avec force. Mon ventre se rétracta sous le contact des surfaces irrégulières aux angles tranchants. Je perçu une chaleur inhabituelle sur mon torse et restai interdite,constatant froidement pour moi-même que saignais de quelque part. Mon sauveteur ne me laissa pas le temps d'investiguer plus avant l’état de mon pauvre corps et exerça de nouveau une dernière traction libératrice.
Je glissais soudainement comme un nouveau-né hors de sa matrice et un espace se libéra, où je chus pêle-mêle, dans d'autres bras. Des bras sans corps et des corps sans bras. Jonché dans ce trou, un tas de cadavres démantibulés me faisait un matelas de chair sur lequel je reposais.

— Ne criez pas, anticipa-t-il.

Sa main se referma sur mon bras gauche. Je hurlai malgré tout et me débattis comme un diable. Des débris tombèrent sur moi avec fracas, ce qui me fit brusquement taire. Et je cessais de bouger. Je scrutai la pénombre afin de voir mon sauveur. Les corps enchevêtrés me rendaient la tache difficile. je ne voulais plus les regarder.

— Où êtes-vous ? Finis-je pas demander, après l’avoir chercher en vain.

— Ici !

Je plissai des yeux, me concentrant sur le son de sa voix.

— Ici ! répéta-t-il encore.

J'entrevis enfin son visage, blanc de plâtre, et le blanc de ses yeux, où, mobiles, deux azurites me fixaient calmement. Il gisait de guingois, sous les corps entassés. Deux imposantes poutrelles métalliques formaient une sorte de passage étroit. Un trou de souris, pensais-je.

— Venez à reculons jusqu'à moi !

— Je dois passer là ?

Il compris ce qui chiffonnait mon esprit contus. Le franchissement de l'obstacle impliquait que je rampe par dessus ce monceau de restes humains. Une tête de femme, dont le maquillage était parfaitement préservé, me fixait d'un air interrogatif. Son corps avait disparu, sa trachée reposait sur une pierre comme un serpent endormi.

— Perdre la tête —

Cette pensée me fit exploser d'un rire démentiel. Ce rire hideux se répercuta parmi les décombres. Un éboulis roula de plus haut, dont des bribes cognèrent mon crâne, me rappelant à la raison, sans m'apaiser loin de là.
Je commençai à convulser.
Un violent pincement à la cuisse me déchira au niveau du nerf crural.
Ma crise hystérique passa instantanément.

— Ça va mieux ?

J'avais envie de mettre un coup de talon sur ce visage inquiet, que j’apercevais sous moi. Mais c’était déjà fait apparemment : il saignait du nez, un sang rouge qui tranchait au milieu de tout ce gris.

— Je suis désolée, murmurai-je.

— Ce n'est pas grave, continuez de venir vers moi, je redescends, je vous attends et je vous tire un peu, mais ne ruez plus, ajouta-t-il.
Ce trait d'humour me fit un bien fou au milieu de cette apocalypse et je reculai précautionneusement sur les restes humains empêtrés dans les décombres. Je glissai sur eux. Et gagnai des décimètres et encore un peu. Soudain, je n'eus plus la pression de ses mains sur mes jambes. La panique me noua les tripes et je m'immobilisai.

— Venez ! Il tapotait ma cheville.

Me penchant un peu, je vis que les cadavres laissaient place à un plus large passage par lequel l'homme s'était faufilé.
Il disparut.
Un grand bruit résonna jusque sous mon crâne à travers la structure démolie.

— Venez répéta-t-il patiemment.

Je reculai, plus habile et moins attentive à ce qui m'entourait À quoi bon ?
Mon corps atteint la béance et récupéra sa liberté de mouvement dans une chute non contrôlée mais sans conséquence. Nous nous retrouvâmes face à face.

— Elliot, dit-il dans un souffle, et il me présenta sa main.

— Claire, me présentai-je à mon tour, saisissant cette main offerte, comme un noyé la bouée salvatrice.

— Vous n'avez rien de grave ? Vous saignez.

— Non, je ne crois pas. C'est superficiel.

Nous nous tenions à croupetons dans la pénombre grise.

— Pas le temps de faire un inventaire, il faut rejoindre le dernier sous sol rapidement.

— Comment pouvez-vous savoir ?

— Faites-moi confiance.

Désignant un trou qui s’enfonçait droit sous eux, il repris sa reptation, facilitée par la largeur plus généreuse de ce semblant de tunnel inespéré.
Comble de l'horreur, ma vue se voila tandis que dans un dernier effort, je laissai derrière moi tous ces cadavres morcelés.

J'étais dans le noir total.

Ma respiration s’accéléra sous l'effet de la panique. J'entendais qu'il avançait toujours. Le bruit s’atténuait, j'allais le perdre. Rester seule ici, mourir dans le noir.

— Il y a quelque chose qui ne va pas ?

Sa voix tout près de moi.
Je lui exposai mon état.

— Ce doit être passager, dû au trauma, ou au stress, ne vous inquiétez pas.

***Ne vous inquiétez pas ?*** Je hoquetai et sentis un objet froid s'enrouler contre la peau de mon poignet droit. Je sursautai.

— J'attache ma ceinture après vous et je vous guide. Vous devez suivre, c'est tout. Vous pouvez faire ça ? Rampez comme vous pouvez sur un bon mètre, et ensuite nous pourrons avancer casser en deux. Venez !

Notre progression reprit, chaotique à souhait. Je le gênais sans cesse, butant sur tout, n'y voyant goutte et pourtant il ne cessait de m'encourager et de s'encourager lui-même.

— Encore un petit effort.

Et plus loin,

— Oui, voilà, baissez-vous, nous attaquons un autre tunnel, attention…

Au moment où je respirai plus librement, une détonation épouvantable se fit entendre, suivies d'autres. Les décombres tremblèrent, des grincements suivis de gémissements métalliques se répandirent et sifflèrent méchamment dans nos tympans. Il me toucha l'épaule.

— Avançons pendant que le passage existe, si la structure bouge, notre chance d’en sortir peut se réduire à zéro. Venez !

La tension de la ceinture accrochée autour de mon bras me tordit méchamment le poignet, tandis qu'il reprenait sa progression dans les ruines, toujours plus bas. Mes yeux pleuraient, sans peine pour les alimenter et je supposai que la poussière irritante en était responsable. Assoiffée, Je gouttai avec délectation ces larmes sales et pourléchai mes lèvres.

— Ne faite pas ça, vous allez avoir des crevasses et vos lèvres vont se mettre à saigner.

— Je boirai le sang : j'ai soif !

— Avancez ! La voix ne fit aucu cas de cette réponse provocante, elle était à la fois encourageante, caressante et déterminée.

D'autres déflagrations se firent entendre sans que je puisse les situer, mais nous eûmes d'autres chats à fouetter : Les plaques sur lesquels nous nous déplacions cédèrent brutalement, et nous entamèrent une chute vertigineuse accompagnée de débris divers, dont la taille, heureusement, ne dépassait pas celle de bille. J'en reçu plusieurs sur le dos et au niveau des côtes, ça faisait un mal de chien.
Nous nous réceptionnâmes sur un matelas doux tandis qu'une nouvelle grêle de cailloux s'abattait sur nous encore quelques longs instants. J'avais couvert ma tête avec mes mains et sentait le poids de son bras au bout de la ceinture. Cela me revigora. Je ne pris même pas la mesure à ce moment-là, de la chance que nous avions d'être encore vivant !

— Bon, nous avons été plus vite que prévu à descendre. Cela nous fait gagner du temps, allons-y !

Puis j'entendis une lamentation.

— Un soucis ? demandais-je sur le qui-vive.

— Rien d'insurmontable, mais je me suis blessé, je pense, au niveau de la jambe droite.

— Vous pourrez marcher ?

— Il faudra bien.

— Je peux vous aider…

— Il n'y a rien que vous puissiez faire à part me suivre. Vous pleurez ?

L'intonation de sa voix s'était faite plus empathique sur les derniers mots. Je sentis un instant ses doigts passer sur mon visage. La douceur du geste m'attendrit, je pleurai pour de bon.
Il retira vivement sa main, comme s'il s'était brûlé.

— Allons-y !

Nous avions eu une chance inouïe d'atterrir sur d'épais morceaux de panneaux isolants, souples et élastiques. Nous repartîmes immédiatement, tenant presque debout cette fois, dans cette partie de tunnel. Je tombai néanmoins à genoux un peu plus loin, exténuée. Son halètement m'indiquait que sa blessure le taraudait plus qu'il ne le faisait savoir.

— Encore un effort, j'entends l'eau circuler quelque part. Il prit le temps d'une courte explication. « L'eau passe dans les couloirs de service de l’hôtel, donc, nous sommes au bon endroit, il nous suffit de trouver une sortie au nord »
— Mais qu'est ce que vous racontez
l'agressai-je recouvrant un peu de bon sens. « Vous nous avez fait descendre sous les décombres ! Comment voulez-vous qu'il se trouve à cette profondeur une sortie avec un accès libre ? Il doit y avoir des milliers de tonnes de pierres, de ciment et de feraille partout au-dessus de nous ! »

— Non. Croyez en votre bonne étoile.

— Je n'ai pas ce luxe ! soupirai-je, impitoyable envers mon bienfaiteur.

— Alors croyez en moi, conclua l'homme d'une voix désarmante. « Je suis égoutier. J'ai passé ces sept dernières années à arpenter ces galeries. Au nord, les tunnels sont à flanc de coteaux, à l'air libre. J'en mettrai ma main au feu »
j'émis un petit rire contrit.

— Excusez moi.

J'étais en train de devenir folle.

— Ce n'est rien, Claire.

Cette voix toujours si rassérénante. Je me fis honte.

— Si, je vous ai manqué de respect.

— Non, c'est la guerre qui nous manque de respect.

— Je suis désolée, je ne vous suis d'aucun secours, et en plus je vous crie dessus.

le silence accompagna mon mea culpea.
Des gouttes d'eau nous aspergèrent alors que les bombardements reprenaient quelque part, au-dessus de nous, dans un monde qui, à cet instant, n'était plus le nôtre. Un monde dont les préoccupations nous étaient si éloignées, que je me pris à me demander si j'avais jamais un jour appartenu à ce monde-là.
Sa main toucha la mienne puis il s'approcha tout près de moi, guida ma tête contre son épaule et m'enserra dans son vêtement avec une tendresse toute maternelle amors que je tremblais de la tête au pied sans m'en être seulement aperçue.

— Un petit câlin pour un petit enfant, disait ma maman quand j’étais en peine.

Je me laissai aller un moment sur son épaule, respirant son corps chaud, m’imprégnant de sa force puis me dégageai à regret, un peu gênée.

— Continuons ! Appuyez-vous sur moi, pour soulager votre jambe.

Quelques enjambées plus avant.

— Voilà les premiers embranchements, il y a moins de dégâts ici !

— Je ne vois hélas toujours rien !

— Une chance que je sois nyctalope.

— Vous avez la vision infra-rouge ?

Il rit franchement de mon ignorance tandis que nous avancions à petits pas, moi aveugle et lui boiteux.

— Je vois dans l'obscurité.

— Vous êtes très surprenant.

— C'est un défaut de la génétique.

— Merci.

— Je n'y suis pas pour grand-chose.

— Je voulais dire…

Je sais ce que vous vouliez dire, mais je n'y suis pas pour grand-chose de toute façon : Vous avez décidé de vivre mademoiselle Claire.
Je me doutais qu'il souriait, car la chaleur de son sourire me parvint et effeura ma peau avec plus de douceur que la caresse d'une mère.

— Je vous dois la vie, Eliott.

— Vous la devez à vous-même. Venez, ne perdons pas de temps.

Et nous crapahutâmes dans un long défilé de couloirs puants.

— Il y a des puits de captage de ce côté-ci, nous finiront par tomber sur une échelle de service.

— Vous ne vous découragez jamais ?

— Et vous ?

— Je vous fais confiance.

— Alors je ne me décourage jamais. Comment vont vos yeux ?

— Ils ne pleurent plus, j'ai l'impression que le noir est moins noir…

— Vous voyez, c'est bon signe !

— Je n'y vois rien tout de même.

— Mais c'est un rien plus que rien.

Un sourire las ravagea mon visage, tendant douloureusement les coins de ma bouche.

— Vous avez un bien joli sourire, mademoiselle Claire.

et il me poussa en avant, sans rien ajouter. Les tunnels succédèrent aux tunnels, mais il était évident que cette partie des constructions était en bien meilleur état que ce que nous laissions derrière nous. L'épuisement nous gagnait, la soif agrippait de ses griffes nos gosiers secs, mais nous progressions toujours.

— Prenons ici, dit-il un peu plus tard.

— Encore ici, ajouta-t-il beaucoup plus tard.

Quand il s'apprêta de nouveau à proférer ce que je supposai être une nouvelle et énième indication je lui soufflais,

— Arrêtons-nous quelques instants, je n'en peux plus.

Il détacha la ceinture à mon bras.
Je pris peur.

— Que faites vous ? couinai-je misérablement.

— Montez !

La peur d'un abandon me percuta l'esprit avec violence et me laissa stupide.

— Quoi ?

— Saisissez le barreau là !

Et, alors que je tâtonnai à trouver le barreau de l’échelle, il me guida et posa fermement ma main dessus la patte de fer.

— Montez !

— Il y a beaucoup à grimper ?

— Le savoir ne vous aidera pas à atteindre la sortie plus vite. Montez, c'est tout !

Il me tapa dans le dos un peu familièrement et ajouta avec douceur,

— Allez Claire, on y va !

Je ne pensai pas que mon corps malmené pourrait me soulever, ni encore moins me mener d'un barreau à l'autre, plus haut, toujours plus haut…
— À flanc de coteau — avait-il dit, mais quelle dénivellation ? Combien de tunnels verticaux allions-nous devoir escalader ? Je dérapai entre chaque prise et avançai avec une lenteur exaspérante.
Des explosions, clairement identifiables, retentirent à travers le dédale souterrain. Quelques bruits de chutes de pierres accompagnèrent ce tonnerre d'acier. Je rentrai instinctivement la tête dans les épaules et faillis lâcher prise.
Eliott s'exclama,

— Avancez ! S'ils bombardent ici, ça ne tiendra pas !

Je ne me demandai pas comment et d’où il sortait cette énergie et cette volonté pour grimper avec une jambe blessée. Je me demandai juste s'il n'allait pas soudain tomber comme un sac de son et former un nouvel empilement de cadavres tout cassés, marionnettes sans plus d'humanité.

— Avancez ! Sa voix énergique me vrilla les tempes en même temps qu'une nouvelle salve de pétarades, plus fortes encore que les précédentes.

Puis soudain un brin d'air glacial se coula sous mes vêtements ; J'avais oublié que nous étions en hiver. Ma tête buta un instant plus tard contre un obstacle.

— Une grille. Mince il y a une grille.

— Ce n'est rien Claire, j'ai la clé.

— Comment ?

— Les grilles sont toutes sécurisées, mais dans le cadre de mon travail, je possède une clé universelle. Poussez-vous sur le coté, je viens à coté de vous.

Je sentis que ces dernières coudées lui arrachaient des larmes. Il souffla et grogna, mais parvint à ma hauteur. Il tendit le bras, et un grincement résonna suivi d'un autre. Après avoir exerçé une dernière poussée, la grille retomba à l’extérieur avec un bruit mat.

— Je vais voir.

Il m'écrasa un doigt en se faufilant le long de l'échelle, mais je m'abstins de protester, je sentais ses dernières forces lancée dans cet assaut.
Une minute plus tard, sa main agrippa mon avant-bras.

— Nous avons réussi, Claire.

— Il fait nuit ?
— Non, je ne crois pas, mais le ciel est saturé de cette sale guerre.

— Sale ?

— Sale oui.

Il n'en dit pas plus. Je sortis la tête à mon tour.

— Je vois des ombres, dis-je, autant parce que c'était vrai que pour détourner la conversation.
— De mieux en mieux, bientôt, vous recouvrirez la vue, soyez confiante.

— C'est difficile.

— Bien sûr, c'est plus facile de sombrer, mais vous n'avez pas envie de sombrer, Claire, je le vois à vos sourcils qui se plissent têtus, à vos paupières mobiles, quêtant un chemin, et à vos dents qui pressent nerveusement votre mâchoire, elles ont faim de vie.

— Vous êtes poète aussi ! Le taquinai-je, goûtant là un tel instant de réconfort qu'il me submergea d'émotions contradictoires.

— Venez, il faut nous mettre à l'abri quelque part, et puis nous rejoindrons le central.

— Le central. Je l'avais totalement oublié.

— En cas d'attaque, rejoindre le Central —

Une sorte de check point pour civils.

— Je ne suis pas certaine d'en n'avoir envie, ne pourrions-nous rester là encore un peu ?

— Nous sommes potentiellement sous le feu ennemi.

Je soupirai. Son ton raisonnable suffit à me convaincre. Il rattacha sa ceinture à mon bras, sans ajouter un mot et nous nous mîmes en marche, à l'air libre cette fois, longeant prudemment un mur d'enceinte en ruine qui se perdait dans la grisaille.

Quelque temps plus tard, il s'immobilisa.

— Des véhicules approchent !
Je tendis l'oreille mais n'entendis rien, ***est-ce que mon ouïe avait subi des dommages en plus de ma vue ?***

— Baissez-vous !

Je plongeai derechef dans ce qui me sembla être de hautes herbes sèches et distinguai bientôt de fortes pétarades suivies de bruits de chenilles mécaniques arrachant la terre et l'herbe là où la route avait disparue. Un rayon de lumière crue balaya l'espace et je me réjouis de percevoir quelque chose, un halo clair, passant et repassant devant mes yeux, avant de me mettre à trembler.
Une voix impérieuse déchira la nuit.

— Montrez-vous ! Pressa la voix, s'approchant.

— Restez dans l'ombre, chuchota Eliott, tandis qu'il me maintenait vers le sol et se relevait en criant :

— Je suis là, ne tirez pas ! Ne tirez pas ! je n'ai pas d'arme !

Des froissements, des ordres beuglés de part et d'autre, encore le ballet d'un projecteur, tout vint en même temps heurter mes sens. Un rayon lumineux intense grilla mes pupilles aveugles derrière mes paupières closes, je me redressai malgré moi, les mains au-dessus de ma tête.

	— Montrez-vous ! Répéta la voix impatiente.

— Ils sont deux, rapporta une autre voix.

Le cliquetis de fusils qu'on réarme se fit entendre. Un claquement, comme la foudre à mes pieds, puis une main brutalement posée sur mon épaule.

— Laissez la ! Elle n'a rien fait ! Elle était dans le Louxor qui s'est fait bombardé ! Ne la touchez pas ! Protesta Eliott tout en se laissant molester. La scène m'était aussi clair que si je l'avais observée de mes yeux, je craignis qu'on le blesse mais ne pipai mot.

— Identifiez-vous ! Ordonna la voix qui commandait.

Eliott marmotta quelque chose que je ne compris pas, pendant qu'on me poussait vers les véhicules, supposai-je. Je trébuchai à chaque pas, fermant résolumment la bouche. La peur me tenait dans un carcan étroit. Je me contentai donc d'ahaner et d'avancer, cette main répugnante m'exhortant sans ménagement à me presser.

— Elle est aveugle, faites attention !

— Aveugle ? Répéta une voix qu je n'avais encore pas entendue.

— Depuis le bombardement oui ! Gronda Eliott d'un ton que je ne lui connaissais pas.

Après quelques pas, ils stoppèrent.
Palabres incompréhensibles. Puis,

— Une civile. Sergent, foutez-moi ça dans le camion et ramenez-la au camp illico !

La main sur mon épaule me bouscula pour avancer.

— Je n'irai nulle part, grommelai-je sans ébaucher un mouvement. J'avais dans l'idée d'avoir hurlé à plein poumons, mais personne ne sembla me prêter attention.
Eliott occupait leur centre d'intérêt.
Le projecteur balaya mon visage puis une zone proche, je devinai des ombres, ou les imaginai.
Conciliabules.

— Passez lui les menottes, ce salopard est un déserteur, intima la voix de commandement. « On se le garde au chaud celui-là ». Il se racla la gorge.


— Vous êtes en état d'arrestation.

— Attendez ! Cette fois le silence se fit. On m'écoutait. La main relâcha sa pression sur mon épaule.

— Votre nom la fille, vite !

— Claire Duroy. Cet homme m'a sauvé la vie. J'étais prisonnière des décombres de l’hôtel Louxor, je vous en prie !

— Mme Duroy, dites au revoir à votre sauveteur.

Une poussée me projeta brutalement en avant. Je chancelai. Les bras d’Eliott, entravés par des menottes, m'accueillirent maladroitement.

— Filez ! Murmura-t-il et son souffle chaud chatouilla le creux de mon oreille. De son torse, émanait une chaleur diffuse qui parvenait jusqu'à moi, apaisante. Il m'engloutit dans ses bras un instant
— instant qui dura une vie entière —
Vous êtes en sécurité à présent, Claire, ils ne font rien aux civils, ce sont des militaires. Vous comprenez ? Filez jusqu’au camion, répéta-t-il lentement, comme s'il voulait s'assurer que je comprenne.

— Ils vous ont arrêté ! M'exclamai-je, mes protestations couvertes par un bruit de ferraille. Un tank faisait demi-tour, les chenilles crissaient désagréablement. Je distinguai sa masse sombre non loin de moi ; des ombres s'affairaient tout autour, faisant peu de cas de notre couple.

— Venez, ayez confiance, poursuivit-il. Ne vous inquiétez pas. Vous aurez bientôt un endroit pour dormir et vous verrez un médecin pour vos yeux. Venez.

Ses mots apaisaient mon inquiétude. L’écouter, c'était s'étendre au pied d'un pin parasol et observer la courbe du soleil à travers les branches protectrices, bercé au chant des grillons.
Il m'écarta de lui avec délicatesse.

— Avancez ! La voix éructa l'ordre sans commisération pour nos corps écorchés.
Nous dûmes parcourir quelques centaines de mètres avant d'atteindre un camion dans lequel un soldat me propulsa avec rudesse. Il y avait d'autres personnes à l'intérieur, l'air hagard. Je devais certainement afficher cet air-là.

— Toi le déserteur ! Tu vas dans le VAB.

— Eliott !

Ma voix ne porta pas jusqu'à lui.

— Vous êtes en sécurité Claire, jeta-t-il par dessus son épaule, n'ayez plus peur !
Il avait deviné ma détresse.

Le camion démarra.

Au-dessus de nous, des avions de chasse vrombirent dans un déluge de tonnerre. Très loin, une salve d'arme automatique retentit de manière sinistre.
Le camion cahotait. Des détonations retentirent tout près, soulevant des gerbes de terres autour de nous. Des cris, suivis d'ordres braillés aux quatre vents. Le camion accéléra. Certains passagers criaient, d'autres pleuraient, d'autres encore priaient. Pour ma part, rien ne me vint qu'une immense solitude. Je n'écoutai plus.
Le convoi pris encore de la vitesse et s'éloigna sous le feu des bombardements.


Je rejoignis le Central.
— Voilà, un peu de patience, quelques jours de repos et le résultat est là ! Le docteur Clemens tenta un sourire. Ce militaire, d'une trentaine d'année, se cachait sous de petites lunettes épaisses cerclées de métal. Ses cheveux ras et sa barbe de quelques jours lui donnait un air faussement juvénile. Il prenait en charge la médecine civile dans le camp. Ses bons soins, bien que rudimentaires, avaient permis à mes yeux de guérir rapidement.
Et je vis partout la désolation.
La pire de toute était son absence : Elle creusait un large sillon au fond de mon âme. Aussi, dès que je recouvrai suffisamment mes forces, je n'eus de cesse de retrouver Eliott.
Il me fallut un laissez-passer pour circuler dans le camps. J'insistai tant et tant auprès des gardes, auprès des services administratifs, jour après jour, qu'un militaire à la mine sévère, me reçut entre deux portes.

— Que puis-je pour vous Mlle Duroy ?
Je reconnus sa voix.

— Vous n'êtes pas secrétaire.

— Vous êtes perspicace. Sa voix tranchait l'air comme une lame.

— Je voudrai voir Eliott.

— Le type qui était avec vous ?

— Oui. Il m'a secouru, j'étais coincée sous les décombres de l’hôtel. Il m'a sauvé la vie vous savez.

— Il va perdre la sienne. C'est un déserteur, Mademoiselle Duroy, la loi martiale a été instaurée, c'est la guerre, les déserteurs sont envoyés sur la ligne de front.

— Vous ne l’exécuterez pas ?

Ma question naïve le prit au dépourvu, il repoussa son béret et s'épongea le front avec un grand mouchoir, prenant le temps de me répondre.

— Ce sera bien pire, Mademoiselle. Le front broie tous les hommes, et nous en manquons cruellement.

— Je veux le voir ! Je me reprochai aussitôt cette requête à laquelle je n'avais pas mis les formes.
L'officier ne s'offusqua pas. Il renfonça son béret sur son crâne, me salua et donna quelques ordres en s'éloignant.

Un soldat s'approcha me toisant sans façon.

— Claire Duroy ?

Je restai muette.

— Suivez moi !

— Comment ?

— Suivez moi, j'ai pas toute la journée. Alors vous venez ou vous restez. Maintenant !

Il se mit en chemin. Mes pieds se mirent en marchent à leur tour sans que j'ai le temps de la réflexion. Nous traversâmes le camps.
Le chaos.
J'aurai préféré ne pas voir.
Une rangée de hangars se profila alors que nous sortions du camps des réfugiés, le Central. Le soldat, qui ne m'avait pas adressé la parole depuis notre départ, m'indiqua le premier bâtiment d'un geste las.

— Il est là-dedans. Le colonel vous donne quinze minutes avec le déserteur. Il ajouta comme pour lui-même, « voudrais bien savoir pourquoi, ça, je voudrai bien ! Salopard de déserteur. C'est à cause d'eux qu'on perd la guerre. Vous comprenez ? »

Il me regarda avec de la haine au fond des yeux. Il était mon ennemi autant que les ennemis en face qui lançaient des bombes et enterraient les gens sous des décombres. Il me scruta encore, empli d'une joie mauvaise, proche d'une infinie désespérance et cracha,

— Bah, bientôt, justice sera faite. Venez !

Nous reprîmes notre marche, traversâmes le hangar glacé et après quelques brefs arrêts aux différents postes de garde, mon escorte s'adressa à l'un deux, planté devant une série de box grillagés, assemblés à la hâte à l'arrière du grand bâtiment.
Je m’avançai.
Il était là. Assis en tailleur, la tête dans ses bras.
Je n'eus pas à prononcer son nom que le mien volait comme un papillon au printemps jusqu'à mes oreilles.

— Claire !

Ses yeux bleus souriaient et croisèrent les miens. Je le découvrais vraiment pour la première fois !
Le garde ouvrit la porte de la cellule et se retira.

Il y avait des pavés mouillés, l'odeur de la pluie, un ciel gris. Il s'est approché de moi en disant :
Manque le rituel habituel.
Et il a ouvert les bras.
Une menotte pendait à l'un de ses poignets.
Le sol se déroba sous mes pieds, mais il me retint de ses grands bras, me cajolant comme une enfant.
— Je suis heureux de te revoir Claire.
Ma voix me fit défaut. Je ne pus que le serrer fort, très fort, et le regarder. Son sourire lumineux me fit oublier la gravité de sa voix et inéluctabilité de ses quelques mots à mon oreille ;

— Je pars demain pour le front.

Et quand quinze minutes plus tard, la porte se referma, je ne me retournai pas, ç'eut été insultant que de lui offrir la tristesse de mon deuil alors qu'il se tenait droit, vivant son dernier jour.


