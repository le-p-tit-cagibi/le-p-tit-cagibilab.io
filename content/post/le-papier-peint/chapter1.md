— Mesdames ont choisi ?

Les deux femmes se regardèrent avec un sourire de connivence.

— Oui, celui-là sera parfait ! Répondit la plus jeune, caressant le papier peint dont un pan s'étalait jusqu'au sol de béton de la solderie. De gracieuses carpes aux reflets d'airain tournoyaient parmi de larges nénuphars aux tons pastels. Il se dégageait de la scène, répétées à l’infini, une intime sensation de sérénité, comme un coin de paradis niché là, au milieu d'un fouillis indescriptible.

— Un bon choix, confirma le vendeur, hochant vigoureusement la tête. Ses yeux très noirs et pourtant pétillant de bonne humeur leur souriaient, « ça ne se fait plus guère ce genre de papier aujourd'hui. Tout le monde préfère les autocollants ou les magnétiques. Ah ! Le progrès ! » Machinalement, il tapota un poisson sur le papier et, illusion troublante, l'animal parut onduler sous ses doigts.

La vieille dame se saisit du rouleau, parvint tant bien que mal à le caneter, puis le fourra dans un carton avec d'autres identiques.

— Vous prenez le lot ? Questionna encore l'homme.

— Oui, je préfère, répondit de nouveau la jeune acheteuse, je ne suis pas experte, si je me trompe avec les raccords, ou rate un lé, j'aurai de quoi faire.

— Sage décision, valida le marchand, secouant la tête en connaisseur.

Ayant payé, les deux femmes s'éloignèrent et rejoignirent leur voiture, une vieille Ford dont le large hayon s'ouvrit en grinçant lorsque qu'elles basculèrent le volumineux carton à l'intérieur.

Le vendeur les regarda s'éloigner en branlant du chef, approbateur. En son for intérieur, il tiqua tout de même un instant, il pensait avoir jeté depuis belle lurette ces vieilleries ! Puis l'instinct du commerce chevillé à son large torse, sa devise lui revint à l'esprit :

— Des clients satisfaits, chez Momo, toujours des clients satisfaits !

Il se dirigea derrière un antique comptoir de merisier ; la plate-forme de zinc, à l'aspect polie refléta sa bouille replète, ses petites lunettes rondes posées sur un nez anguleux où deux iris d'un violet pâle banissaient son appartenance au commun des mortels. Il leva sa généreuse bedaine, s'aidant de ses deux mains, pour se percher sur un ridicule tabouret et se cala contre le bois chaud.

— Toujours satisfaits, répéta-t-il à haute voix comme une litanie, avant que sa silhouette ne se trouble tel un mirage lointain au détour d'une route surchauffée et se délite littéralement dans la structure du meuble rétro avant de disparaître.

— Qu'en penses-tu ? Ça rend bien ? La jeune femme en nage, essuya d'un revers de torchon son front moite, le gratifiant d'une trace de colle.

La vieille dame recula, son index droit appuyé sur le menton, en connaisseuse.

— Splendide ! Affirma-t-elle, réjouie. La pose n'est pas trop difficile ?

— Non, ça va, ne t'inquiète pas, repris sa fille découpant soigneusement la bordure de papier au ras du mur, le long de la plinthe de la chambre. Il ne me reste plus beaucoup à faire, tu auras une belle pièce !

— Sans conteste, tu as raison. Ce papier dégage une sorte de paix, c'en est presque envoûtant. On s'attendrait presque à ce que ces carpes se métamorphosent en nymphes. Une scène d'une telle mélancolie ! A trop la contempler, on s'y noierait, ajouta-t-elle rêveuse.

Sa fille s'esclaffa, et répartit malicieusement,

— Sérieux, maman, c'est zen. Si tu as peur de boire la tasse, passe une bouée pour dormir !

Elles rirent.

Le soir même la chambre était réaménagée. Mère et fille admirèrent leur travail.
Derrière la tête de lit, le panneau de papier peint, mis en valeur par une peinture très légèrement teintée porcelaine, resplendissait. Les poissons vibrionnaient, indifférents au monde, créatures somptueuses, aux écailles d'or, les nageoires souples, domptant l’élément liquide telles des danseuses sur une partition connue d'elles seules.

Un authentique morceau de poésie.

La vieille dame se retourna.

— Mais ? Tu as fais un autre panneau en face du lit ? Constata-t-elle surprise.

— J'avais largement de quoi ! Regarde, il me reste encore trois rouleaux ! Je pourrais presque faire un mur dans ma bibliothèque ! Rétorqua la jeune femme puis elle ajouta un peu inquiète :

— J'ai bien fait ? Tu aimes ce second panneau ?

— C'est parfait, tu as du goût et d'excellentes idée ! On se sent totalement immergé dans ce décor. Merci ! Elle la serra contre son cœur, reconnaissante et lui murmura à l'oreille,

— Prend le reste du papier, fais ta bibliothèque, ma grande.

— Je disais ça comme ça, mieux vaut le garder, au cas où…

– Bien sur que non ! prends-le et fais à ton idée dans ta maison, ce serait stupide de garder tout ça pour rien, fais-toi plaisir.

Ce soir là, la jeune femme repartit avec, à l'arrière de la Ford, le carton contenant les trois derniers rouleaux. Son regard tomba sur l'intitulé d'un coin d'étiquette chiffonnée apposé sur le plastique d'emballage, et elle déchiffra non sans mal les quelques mots à demi efffacés « jeux de dupes ».
Un titre un peu pompeux pour ce genre d'article tout de même, s'amusa-t-elle en claquant le hayon qui protesta bruyamment.

Quelques jours plus tard, en fin d'après-midi, la vieille dame attrapa son téléphone portable et ouvrit le message qui s'affichait. Une photographie l'attendait. Elle scruta l'image, car ses yeux n'étaient plus ce qu'ils étaient et découvrit la bibliothèque de sa fille. La grande pièce au lourd plancher de chêne, arborait le nouveau papier peint sur un mur rencogné près d'une grande fenêtre à triple battants. Tout autour, sur les trois autres murs de la pièce, s'ordonnaient les multiples rayonnages d'une bibliothèque conséquente. Au coin de la photo, apparaissait le visage allongé et pâlot de sa fille, souriante.

Le message vint presque simultanément :

**Tu en penses quoi ? Chouette non ?**

La dame pris son temps pour répondre, les touches étaient si petites, elle réfléchit puis se lança.

**Formidable ma fille ! Ainsi, nous resterons toujours en contact, d'une certaine façon.**

Elle relut, se demanda pourquoi elle avait écrit cela. C'était parfaitement ridicule. Elle effaça la dernière phrase et appuya sur la touche « envoi » .

**Formidable ma fille !** La jeune femme découvrit le court message avec satisfaction. Depuis quelque temps, sa mère semblait s'éloigner d'eux, c’est-à-dire de ses enfants, et encore bien plus de ses petits enfants. Peut-être l'un des effets pervers de l'âge où le monde actuel semble trop bouillonnant, trop incompréhensible peut-être, à une dame née au siècle précédent ? 
En tout cas, c'est ce qui l'avait encouragé, avec son jeune frère, à entreprendre quelques travaux de rénovation chez leur mère. Dans l'espoir de lui rendre un peu de cet allant, de cette joie de vivre qu'elle possédait auparavant.
Avant quoi ? La jeune femme réfléchit longuement, mais ne put déterminer le moment (ou l’événement) déclencheur de ce changement. *Oui*, se dit-elle, *ce n'est certainement que le temps… le temps qui passe le balai dans nos âmes. Le temp ! Ce laveur de carreaux obsessionnel, infatigable, récurant les vitres de notre petite maison intérieure, notre petit chez soi où sont crayonnés, fragiles, nos souvenirs intimes ; la senteur d'une chevelure aimé, la douceur d'une peau d'enfant. L'inexorable temps qui jamais ne prend pitié de nous.*

Elle soupira et admira à nouveau son œuvre ; les carpes paressaient dans les feuilles, dessinant d'hypnotiques volutes sous l'onde silencieuse.

Sa main frôla le papier. Elle sursauta et ôta vivement ses doigts, les agitant dans l'air, s'attendant à sentir encore la fraîcheur de l'eau qu'elle avait cru percevoir en les apposant sur la surface du papier tout juste posé.

Elle se moqua d'elle-même, et y appliqua résolument sa paume entière. Les carpes restèrent muettes et immobiles, l'eau ne frémit pas.

*La fatigue me joue des tours*, se morigéna-t-elle. *Allons déjeune !* 
Elle poussa gentiment du pied son grand chat noir et blanc, figé, le poil hérissé, au coin de la porte de la bibliothèque.

*Quel mauvais coucheur celui-là* songea-t-elle en le contournant, sans y prêter plus attention. Une fois descendu l'escalier de meunier pour rejoindre le rez de chaussée, son appétit envolé, elle saisit ses clefs, son sac et claqua la porte en sortant.

— Encore un peu ?

– Ah non, je vais éclater ! Elle accompagna sa parole d'un ample geste de refus, adouci par un large sourire.

Comme souvent le jeudi, la jeune femme déjeunait avec sa mère À contrario d'une corvée, c'était un moment heureux. Ensemble elles évoquaient d'anciens et récents événements qui déclenchaient fou-rires ou pleurs, car la vie est ainsi : arrosée de larmes, et c'est de cette façon qu'elle croît. Cependant les larmes ne sont pas toutes gorgées de peines, il y a aussi celles qui perlent lors de grandes joies. Elles n'en étaient pas avares. L'après-midi s'étira, mélancolique.

— Tu rentres déjà ?

— Il faut que je dorme un peu maman, je travaille ce soir. On se voit jeudi prochain, et je t'appelle.

Elle embrassa la vieille dame rapidement, et ne remarqua rien ; ni ses yeux plus usés qu'à l'habitude, ni sa peau, plus translucide que de raison, ni ses mains maigres aux articulations déformées par une arthrite de longue date, ou elle s'y refusa, niant ainsi inéluctabilité, attrapant vivement ses affaires dans l'entrée et filant dans le brouillard.

Dans la nuit, elle émergea lentement d'un cauchemar terrible. Elle interrogea son réveil — trois heures du matin — et se tint fébrilement les épaules avec ses bras, assise en tailleur sur son lit, affligée d'un sentiment de vide et d'abandon jamais éprouvé jusqu'ici.
*Quel rêve affreux* se remémora-t-elle, encore toute chamboulée. Elle y avait reconnu sa mère jeune, attifée de loques, décoiffée, sale et décharnée, mendiant au coin d'une rue de son village natal. Du village, il ne subsistait que ruines. Au loin, une fumée épaisse et noire s'enroulait autour de la petite basilique dont le clocher et une grande partie du toit d'ardoises s'étaient effondrés.

La dormeuse déglutit, un goût de cendre dans la bouche, et se houspilla durement, bien décidée à chasser ces images abominables. Ses pensées errèrent un instant quand son regard fut attiré par le papier peint nouvellement posé.

Les carpes évoluaient dans l'eau sombre, leurs écailles miroitant au gré de leurs déplacements. Elles bougeaient vraiment. Leurs yeux d'or semblaient la fixer, tout en poursuivant leur farandole hypnotique. La jeune femme n'en revenait pas. Elle se dressa au-dessus de son lit et courut efleurer cette étrange surface mouvante. En sentant la peau rêche d'un poisson râper sa main offerte, elle cria de surprise, ferma les yeux et s'approcha encore, jusqu'à coller son corps contre le mur froid.
Sans transition aucune, elle fut immédiatement engloutie.

L'eau se révéla fraîche mais supportable. Un nénuphar dériva tout près de son épaule où il s'échoua, soudain plus méduse que splendide fleur aquatique. Elle émit un gloussement d'incompréhension. Une eau au goût d'humus pénétra dans sa bouche. Ce n'était pas désagréable, et elle n'y prêta pas attention, tapotant délicatement la fleur de nacre immergée. Sous ses doigts, elle discerna, fascinée, la douceur même des pétales et taquina les larges feuilles qui dérivaient doucement. Les carpes, nullement effarouchées, tournoyaient autour de son corps, leur bouche ronde comme prête à lui parler.

La jeune femme s'aperçut que des bulles s'échappaient de ses oreilles et de son nez. Elle paniqua soudain, comment respirait-elle ? Sa gorge s'ouvrit grand afin d'aspirer une goulée d'air, mais ce fut l'eau, l'eau saumâtre, au léger goût de vase qui lui emplit soudain la bouche. Hystérique, elle se débattit, moulinant des bras, s'entortillant dans les racines caoutchouteuses des nénuphars, enfin pleinement consciente du danger. Les carpes, indolentes, se méprirent sur le sens de ses gesticulations pour un jeu, les prenant, à en croire, pour un jeu. Elles entamèrent une valse grâcieuse, suivie d'une chorégraphie plus rapide, presque convulsive, autour de la jeune femme qui se noyait, les yeux écarquillés, regardant, incrédule, de l'autre coté de la surface liquide, sa grande bibliothèque plongée dans l'ombre.

Son réveil — véritable — fut difficile. Elle toussa, à bout de souffle, et toussa encore, puis sanglota au milieu de sa literie en désordre. Un malaise indéfinissable la saisit et ne la quitta plus.

Les jours suivants, son cauchemar l’obséda ; elle n'osait plus s'approcher à moins d'un bon mètre de son magnifique panneau de papier peint. Les carpes d'airain la narguaient de leurs yeux morts entre les nénuphars éternellement épanouis.


— As-tu passé une bonne semaine ? Tu as une petite mine, tu es sûre que ça va ma grand ? s'alarma la vieille dame en accueillant sa fille de jeudi-là.

— J'ai fait un drôle de rêve, commença-t-elle, mais elle ne put poursuivre. Qu'allait-elle raconter ? Une sombre histoire de noyade imaginaire, pour affoler sa mère ? « Rien d'important, on se le mange ce gâteau au citron ? » Ajouta-t-elle faussement enjouée, forçant le ton. 

Ces simagrées n'échappèrent cependant pas à l’œil encore vif de la vielle dame. Elle sonda le visage creusé de son enfant et devina qu'elle lui cachait quelque chose… Elle soupira. Les enfants ! De petites créatures têtues, qui en grandissant n'en font qu'à leur façon, toujours en cachette ! Éternels enfants !

Une embrassade plus tard, elle agitait la main, rongée d'inquiétude ; la Ford démarra sur les chapeaux de roues.


Au cours de la semaine suivante, la jeune femme se sermonna plus d'une fois, mais son esprit revenait toujours au papier peint. Elle regretta même de ne pas avoir vérifié celui de la chambre de sa mère… peut-être là-bas aussi les majestueux poissons avaient une vie secrète, dans cette mare étrange où l'encre était eau et le papier chair. Dans sa tête, sans cesse, tournoyaient les carpes, tournoyaient d'étranges pensées. Qu'est ce que tout cela signifiait ? Devenait-elle folle ? Ou l'était-elle déjà ? Surmenée peut-être ? Déprimée ? La jeune femme ruminait. Elle dormait peu, dans la crainte de rêver de nouveau, dans la crainte de mourir peut-être, envoûtée, absorbée par ce ballet aquatique; elle aussi alors tournoierait éternellement parmi les carpes chatoyantes.


Un soir, épuisée plus que de raison, elle se résolut à coucher dans son salon, sur son inconfortable canapé scandinave ; son chat grincheux eut beau protesté, elle n'en démordit pas et sombra bientôt dans un sommeil profond.

Aussi fut-elle très étonnée d'ouvrir les yeux dans sa chambre. Un rayon de soleil lui taquinait le visage, les lourds rideaux de velours n'étaient pas tirés. Elle s'avanca jusqu'à la fenêtre pour admirer la vue splendide : son jardin verdoyant accueillait dans un écrin arboré la trille joyeuse des oiseaux. Lorsqu'elle longea le panneau de papier peint, les carpes commencèrent leur lent farandole, bousculant les fleurs pâles qui s'ouvraient à peine dans cet aurore naissant. Elle se recula d'instinct et cligna des yeux, ce qui ne changea absolument rien. Les poissons tournillaient sereinement dans l'eau, battant des nageoires, offrant à la vue leur dos cuirassé d'écailles chatoyantes. Elles miroitaient, changeantes, plus obsédantes à chaque mouvement et la jeune femme eut soudain envie de vomir. Elle se tint le ventre d'une main, posant l'autre sur sa bouche et se précipita aux toilettes.

Alors seulement cette fois encore, elle émergea de son cauchemar avec de puissants haut de cœurs. Et s'élança véritablement jusqu'à la salle de bain, rendant ce qu'elle avait mangé la veille et même ce qui n'y était pas. La bile lui brûlait la bouche, la sueur collait ses cheveux sur son front brûlant.

**Mais qu'est -ce qui m'arrive ?**

Elle prit sa décision après une bonne douche, ayant retrouvé ses esprits.

— Allô ? Maman ? J'ai pris un peu de vacances, si ça te fait plaisir, je viens passer quelques jours chez toi, tu es partante ?

— Bien sûr ! La joie de sa mère s'éteint au moment où elle vit sa fille dans l'entrée, son sac de voyage sur le dos, le visage encore plus émacié depuis sa dernière visite, les cheveux en bataille, vêtu d'à peu près, ce qui ne lui ressemblait aucunement.

Elle n'osa pas lui faire part de ses inquiétudes et s'abstint même de toute remarque la concernant, se consolant en pensant que si sa fille était venue, elle agissait peut-être ainsi parce qu'elle désirait prendre le temps de se confier.

La vieille dame se berçait d'illusions ; la jeune femme fut prise d'hyperactivité, promenant sa mère dès le premier jour sur nombre de chemins ombragés le long des remparts du bourg, admirant la flore, débordante, généreuse en cette fin de printemps. Mais pas un mot ne fut prononcé de ce qui la tourmentait. La jeune femme s’installa dans la plus petite chambre de la maison, une sorte de grand cagibi, jouxtant celle de sa mère. Elle parlèrent beaucoup, de tout et de rien, mais la vieille dame n'eut, à sa grande déconvenue, aucune confidence, et ne sut ce qui hantait sa fille.


— J'ai froid, dit la jeune femme ce soir-là, alors qu'il c'était mis à pleuvoir pour la quatrième nuit qu'elle passait dans son réduit, serrant un châle autour d'elle.

— Faisons une petite flambée, ton frère m'a rapporté de belles bûches, il y a du petit bois dans l'appentis, va en chercher deux ou trois brassées. Et mets un gile ! 

La jeune femme sauta dans ses chaussons, enfila la large veste de laine bariolée de sa mère et s'engouffra sous la pluie. Elle se sentait mieux depuis quelques jours. Comme à l'abri. En sécurité.
Oh, bien entendu, ce n'était pas une solution que de fuir, mais elle songeait aux derniers événements en raisonnant, et tout cela lui paraissait désormais beaucoup plus irréaliste que lorsqu'elle se trouvait seule chez elle. Elle en arrivait à la conclusion qu'elle avait subi un petit épisode dépressif, ça arrive à tout le monde, après tout. Elle profiterait de sa semaine dans le cocon familial, puis rejoindrait son logis et tout irait bien. Pour preuve d'ailleurs, aucun rêve bizarre ne troublait ces quelques jours de vacances : Pas de poissons gigotant dans les murs, pas de nénuphars non plus. L'eau ne ruisselait de nulle part, le papier peint, inerte, cessa de l'obséder. Pourtant les carpes l’accueillaient tous les matins, lorsqu'elle traversait la chambre de sa mère, pour se rendre dans le reste de la maison ; elle y jetait un coup d’œil rapide, et passait son chemin. Elle redevint plus sereine, à constater que tout semblait normal.
**Du papier fait de papier, des desseins imprimés d'encre, rien de plus.**
Se haranguait-elle en son for intérieur.

Le feu se mit à brûler hardi petit dans la cheminée, les deux femmes se blottirent dans un sofa qui avait vu défiler quelques trois générations, soufflant sur leur tasse de tisane fumante et odorante.

— Attention, c'est très chaud !

— Maman ! Je ne suis plus une gamine ! La tança avec malice la jeune femme, ouvrant un album de photographies. Elles les détaillèrent une à une ; certaines, fort défraîchies, laissaient à peine deviner les visages des aïeux, disparus depuis des lustres. Le temps passa doucement, chacune respiraient l'autre, appréciant sa présence, leur connivence. Aucune ne rompit le charme de cette soirée en décidant de rejoindre son lit. Plus tard, la jeune femme caressa affectueusement les cheveux blancs de sa mère endormie, tirant le vieux plaid sur ses épaules décharnées.

La pénombre s'installa, les bûches crépitaient dans l'âtre, les flammes dansaient, leurs ombres ondoyaient le long des murs dans une gigue singulière. La nuit vint.

Le feu baissa, des brandons déjà viraient du rouge au gris ; la jeune femme s'ébroua et rechargea le foyer. Elle était seule à présent dans le grand salon. Un léger bruit la fit tressaillir. Un clapotement.

Elle s'avança sur le seuil de la porte menant à la cuisine, et ses pieds nus se rétractèrent en sentant l'humidité au sol. De l'eau recouvrait le plancher, le teintant de sombre. Elle s'élança jusqu'à la cuisine, supputant que c'était le lieu probable de la cause du sinistre. IL y régnait silence et obscurité, mais l'eau recouvrait généreusement ses pieds à présent. Elle alluma le plafonnier mais après un inventaire rapide, ne décela aucune fuite à proximité de l'évier de pierre, ni nulle part ailleurs dans la pièce. Elle se dirigea alors vers la salle de bain, rejoignant un couloir qui menait également aux chambres. Elle progressait difficilement, l'eau atteignait maintenant ses genoux.

**Comment était-ce possible ?** s'interrogea-t-elle au comble de l'étonnement. En appuyant sur l'interrupteur, elle ne fut pas réellement surprise de ne trouver aucune fuite apparente. Un panier de linge surnageait dans l'eau brunâtre, triste rafiot traînant à bâbord un pavillon blanc qu'elle reconnut sans peine ; une fleur de lotus devenue chiffon. La jeune femme poussa un unique cri, son corps rua soudain prit de spasmes incontrôlables et elle se précipita, chancelante, dans la chambre de sa mère.

Elle fut instantanément engloutie ; un instant, elle se tenait là, une main sur le chambranle de la porte, vacillante, trempée jusqu'aux hanches et la seconde suivante, elle évoluait parmi le banc de carpes, dont les larges nageoires caudales la frôlaient délicatement. Elles virevoltaient autour de ses jambes, de ses bras, mêlant leurs corps élancés à la chevelure de la jeune femme. Enjôleuses, leurs yeux ronds semblables à l'onyx, impénétrables, ourlés d'or, la fixaient sans compassion.

Elle fit mine de reculer, sentit dans son dos une résistance molle, et entraperçut un rideau de feuilles de lotus, au travers duquel l'habitation ne se révélait qu'à peine. Sa fuite était coupée… 
Bonne nageuse, elle s'élança sans plus réfléchir, dans le but d'atteindre la surface. Les poissons poursuivaient leurs arabesques sans animosité aucune, accompagnant ses efforts sans sembler préjuger de la gravité de la situation, l'incorporant à leur chorégraphie aquatique et silencieuse.

**Il n'y a pas de surface** constata-t-elle dépitée alors qu'elle tentait de porter le regard vers le plafond.
Une obscurité liquide l'emprisonnait.
Seules, les carpes scintillaient dessinant une parure fantasque autour de la jeune femme. Celle-ci repoussa rageusement quelques tiges de lotus, aux rhizomes flexibles, qui venaient se coller à son corps, pousser par un léger courant.

**Du courant ?**

Les carpes s'agitèrent, la jeune femme n'apercevant aucune issue se demanda combien de temps elle pouvait respirer ainsi, ou plutôt ne pas respirer.
Cette pensée suffit à déclencher en elle l'épouvante qui s'était tue jusque là. Elle voulut hurler, mais ne le put et l'eau, traîtresse, envahit sa trachée offerte, se précipitant dans ses poumons au bord de l'asphyxie. Les poissons, frénétiques à présent, se trémoussaient sans grâce en la cognant, leur bouche ouverte dans une interrogative incompréhension tandis qu'elle convulsait, manquant d'air, son corps lourd l'entraînant irrémédiablement vers le fond. Elle tenta désespérément d'y voir quelque chose, mais la nuit seule la recouvrit dans son entièreté. Elle perdit connaissance.

— Madame ? Madame ?

Un rayon lumineux lui déchira les pupilles. Elle cligna des yeux en hurlant, se rapetassant instinctivement sur elle-même .

— Madame, ouvrez les yeux, je suis le docteur Clemens, hôpital général. Vous êtes aux urgences. Madame ? Ouvrez les yeux, s'il vous plaît.

La jeune femme frissonna de toute son âme et écarquilla douloureusement les yeux.

Un regard très bleu lui sourit en retour.

— Voilà, c'est mieux, tout va bien, vous avez été hospitalisée il y a deux jours dans un état d'urgence vitale suite à une intoxication par un gaz qu'on appelle CO2. Vous comprenez ce que je vous dis ? Madame, hochez la tête si vous me comprenez.

Mais la jeune femme ne lui répondit pas tout de suite. Elle se saisit brutalement de sa main, et se rappelant soudain la cheminée, la soirée devant une tisane, les photographies… Elle sanglota.

— Maman ?

Le médecin ne souriait plus.

— Les secours sont arrivés trop tard pour votre maman, expliqua le docteur, « vous avez eu de la chance, apparemment, beaucoup de chance, de vous trouvez dans la chambre au fond de la maison, quand on vous a découverte, recroquevillée derrière la tête du lit. Ca vous a sauvé la vie. Votre mère, elle, n'a pas survécu, elle était beaucoup trop près de la source de l'intoxication, je suis désolé. Madame ?
