---
title: L'amour du mercredi
author: SB
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
summary: "Sornettes ! L'amour ne rend ni aveugle, ni sourd, il tire les ficelles voilà tout."
epub: true
pdf: true
draft: false
---
Une lumière implacable me grille les yeux. C'est arrivé d'un coup, en traître, comme d'habitude, une semaine pour m'y préparer et cette soudaineté me prend encore au dépourvu. Pendant quelques minutes, un disque blanc oblitère entièrement ma vue. Je suis brutalement soulevé, si brutalement que j'en ai des hauts de cœur à rendre mon déjeuner, sauf que je ne me rappelle pas la teneur de mon dernier repas.

Enfin, mon bref voyage s'achève. Mon derrière repose présentement sur une matière douce et épaisse, coussin ? Couverture ? Je ne sais pas, mon regard a la fixité de celui des cadavres. Mais je me sens mieux. Je sais ce qui m'attend : mon rendez-vous !

Ça ne rate pas ; lorsque je récupère enfin la vue, c'est pour être ébloui de nouveau, mais cette fois par trop de beauté.

Elle est là !! Juste en face de moi, trônant sur un fauteuil en osier recouvert de molleton couleur crème. Aujourd'hui, elle porte une splendide robe longue de taffetas rose, recouverte de fine dentelle ivoire que rehausse de tous petits volants violet. Sur sa chatoyante chevelure blonde scandinave, un petit diadème en argent repose en équilibre. Ses mains fines et longues sont gantées de blanc et sur sa gorge, son sempiternel collier de coquillages, saugrenu, qui dénote, chez cet ange, une si pardonnable faute de goût !

Je la détaille avec gourmandise, car je sais pertinemment, qu'elle ne fuira pas mes regards appuyés, elle m'en jette aussi quelques-uns, me semble-t-il, prête à m'avouer son tendre sentiment à mon égard, bien que sa bouche, ourlée de lèvres mauve si délicates, reste constamment crispée…

Oh ma belle ! Élue de mon cœur ! Que j'ai attendu ce moment !

Mon regard court sur ses formes généreuses, mais strictement proportionnées : un buste de reine, de longues jambes dont je n’aperçois pas le galbe mais que je devine à la cheville fine d'un rose tendre, au pied court et menu chaussé d'un escarpin fantasque à paillettes dorées.

Elle garde la pose telle une star, pourtant, devant elle, sur la petite table ronde, sont disposés comme à l'accoutumée, quelques viennoiseries fort alléchantes, auxquelles cependant moi-même je renonce, non par respect de ma silhouette mais parce que mon estomac est trop noué d'émoi pour songer à se sustenter.

Une ombre se profile au-dessus de nous, couple immobile, moment désagréable, je le pressens.

En effet, je reprends de l'altitude un instant : Heureusement que mon estomac est resté sagement vide !

J'ai quitté ma place.

À présent, me voilà debout devant cette beauté froide… Je remarque, posé sur ses genoux, un ravissant et minuscule sac à main en perles, sur lequel est inscrit en lettre de feu « Nora ».

Est-ce son prénom ?

Ou la marque de son sac à main ?

Cette question me taraude à chacune de nos rencontres puisque la demoiselle reste obstinément muette. Pourtant je sens que ses yeux parlent, qu'ils m'envoient des signaux, je ne la laisse pas indifférente, j'en suis plus que convaincu.

Et soudain, elle aussi est debout et nous marchons… enfin, nous sautillons plus exactement.

Du coup, sa splendide et vaporeuse robe se révèle un peu encombrante pour ce genre d’exercice, et il n'est pas rare que je me prenne quelques coups, car ma dulcinée ne voit pas exactement où elle pose ses ravissants petits pieds ! Mais je suis un gentleman, point de « ouille » ni autre jérémiade déplacée.

Je l'accompagne dignement en tressautant, autant que cela puisse se faire et nous traversons la chambre, puis le salon, et revenons enfin par la cuisine, le couloir, et de nouveau le salon, puis la chambre. Ne me soupçonnez de rien !! Cette chambre n'est pas la sienne, voyons !

Tout à coup, la scène romantico-burlesque s'assombrit et me revoilà brutalement assis, désormais séparé de ma tendre amante acidulée, par cette satanée table. La théière penche dangereusement, pourvu qu'elle n'aille pas brûler ma belle Nora et sa peau de pêche.

J'ai décidé que malgré tout, c'était son prénom.

Bien sûr, elle doit avoir un nom de famille à particule ! On ne peut porter aussi dignement une robe rose bonbon sans être de noble naissance…

Voudra-t-elle d'un gueux ? Cette question-là aussi me pose soucis…

– Mais une chose à la fois -

Pour l'instant, il me faut encore l'apprivoiser, car sans être farouche, la damoiselle jouerait facilement les indifférentes. Comme à cet instant, où sa tête si délicate a opéré une rotation à 90° ce qui revient à dire qu'elle me tourne la tête ! n'est-ce pas signe d'une nature capricieuse ?

Subitement, l'ombre inquiétante se tient à notre verticale, et cette fois je me sens poussé, tassé, écrasé dans la calèche de Madame sans ménagement ! Maudite calèche !

Qui est l'imbécile qui a fabriqué un moyen de transport qui ressemble tant à un engin de torture ?

Mes jambes, grandes, minces et élancées, que ma moitié ne peut qu'admirer, ne rentrent pas dans cet engin aussi spacieux qu'une boite à cigares ! Au comble du désarroi, je vois l'élue de mon cœur, ma malheureuse moitié, précipitée robe par-dessus tête dans la voiture. Je voudrais fermer les yeux et feindre de ne pas remarquer – admiratif malgré moi, rêveur et ce n'est qu'image pieuse – les dessous affriolants, couleur cerise, minuscules et fragiles, de ma courtisane silencieuse et ma foi, cachottière. Le plus superbe dans sa coquine décadence étant la jarretière, fuchsia, scintillante, froufrouteuse, que je rêve de caresser d'une main aventurière.

Un bruit peu distingué clos mon bref fantasme, la dame vient de choir sur son si mignon séant assez violemment. Du coup, je me noie dans le taffetas, je plie sous la guimauve, mon monde est devenu un champ de rose, mais la plus douloureuse épine reste que je ne vois plus du tout mon aimée : mon champ de vision est totalement obstrué ! Je suis à ses côtés mais ma tête reste roide et de fait, condamné à observer droit devant moi !

oh perversité ! Terrible cruauté !

Je regarde fixement… La tête du cocher ? Du cheval ?

Las !! non, je contemple une main dodue d'enfant saisissant soudain le carrosse de plastique couvert de papillons et de fleurs kitsch, pour le faire voler, improbable vaisseau des airs, une fois vers le haut, une autre vers le bas, puis quelques loopings agrémentent ce show aérien à travers toute la pièce dans un concert de cris sauvages. La séance dure plus que de raison, ma beauté a perdu sa superbe, nous essuyons une ultime et périlleuse voltige quant au loin, un grondement s'élève.

« Christine ça suffit !! tu vas encore renverser tes poupées et les estropier ! Elles vont devenir impossibles à réparer à force ! Range-moi tout ça tout de suite et viens goûter ! C'est mercredi, je t'ai préparé des crêpes ! »

À ce moment fatidique, j'aimerais vraiment être ailleurs, car la fin de notre si tendre rendez-vous, se termine dans une violence qui m'est toujours douloureuse. Nous n’amorçons pas une descente de calèche, encore que, le plan de vol est tel qu'il serait surprenant qu'un atterrissage ait été envisagé, même à demi. Sans nous écraser, ma Nora toute attifée de travers, les chaussures perdues, et moi, cabossé, les bras tourneboulés et les mains plus baladeuses que ne l'accorde la bienséance, sommes d'un coup précipités sans aucune précaution, par cette indélicate Christine, dans un énorme coffre de bois où j'ai tout juste le temps d’entrapercevoir tant d'autres compagnons de souffrance !

Un instant avant le vacarme final concluant notre chute, me frôle un visage aux yeux bleus, si clairs qu'on croirait regarder son âme au travers d'une simple vitre ; un paysage où je sens un frisson qui nous parcourt comme lorsque le vent fait frémir les épis de blé bien mûrs qui se dressent dans la campagne généreuse en plein mois de juin. Oh ! Ces yeux ! Ils m'appellent, ils essaient de s'attacher aux miens comme une liane, au plus prêt, comme une liane, ils m'enserrent

– non -

Définitivement, non, hélas.

Elle tombe et moi aussi .

Il fait tout à fait noir à présent.

Nos regards se sont perdus, et cette promesse d'amour est de nouveau partie remise…

Peut-être mercredi prochain ma somptueuse Nora sera-t-elle enfin mienne.
