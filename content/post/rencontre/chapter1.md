Le cheval déboula derrière elle sans qu'elle le vît.

Ce qui l'alerta, ce fut la terre qui tremblait sous ses pieds, tandis qu'elle cheminait sur un sentier bordant la forêt. Elle tourna la tête, découvrit un destrier rutilant lancé à pleine vitesse obstruant rapidement son champ de vision, et sauta vivement dans le sous-bois pour l'éviter. Elle effectua, dans son élan, un roulé-boulé qui, loin d'être gracieux, lui permit de se rétablir sur ses deux jambes sans endommager sa vieille carcasse. Le bruit des sabots lui parvint encore un instant, écho déjà lointain, puis les geais se mirent en devoir de protester de tout ce charivari, criaillant au-dessus d'elle, alors qu'elle rassemblait ses esprits, son chapeau, et réajustait sa pochette-banane contenant son téléphone, ses clefs et sa gourde. Elle tâtonna dans la terre moussue, mais ses air-pods restèrent introuvables.

— Quel abruti ! — Fut sa première pensée. Puis, se remémorant la scène, force lui fut d'admettre qu'elle n'avait pas vu de cavalier. D'ailleurs jusqu'ici elle n'avait jamais vu de chevaux dans les parages non plus.

*— *Un cheval emballé ? — la deuxième.

L'hypothèse lui parut plausible et la décida à remonter le chemin afin de vérifier si le propriétaire de l'animal s'était lancé à sa poursuite.

Une bien étrange monture à vrai dire, se remémora-t-elle, affublée d'une sorte de cotte de maille, le chanfrein masqué, elle l'aurait presque juré… Une bête tout droit sortie d'une gravure moyenâgeuse. Elle haussa les épaules, la scène avait été trop rapide pour vraiment être sûre de ce qu'elle avait entraperçu, d'autant qu'il lui avait collé une belle trouille !

Elle parcourut un kilomètre sans croiser personne, en dehors d'une bande de pies, jacassant dans les chênes. Elle était rompue, et quelques courbatures tiraient douloureusement ses muscles à présent. Elle décida de renoncer à ses recherches, prit une traverse et s'en retourna au village.

Se moquant de sa mésaventure, elle songea que cette anecdote alimenterait les potins lors de sa réunion hebdomadaire au club de lecture.

Mais elle n'en fit rien.

Cet incident la troubla plus qu'elle ne se l'avouait. Plus d'une fois, elle s'éveilla au milieu de la nuit aux sons tambourinant de pesants sabots arrachant la terre grasse du sentier. Les mottes de terre giclaient autour de la bête menant un train d'enfer. Sous son poitrail enferré, la sueur ruisselait de sa musculature, et l'écume s'échappait de sa bouche tandis qu'elle tirait hardi sur le mors. La rêveuse ne rééditait pas son saut abracadabrantesque, au lieu de cela, le cheval de guerre la piétinait ; elle sentait le goût du sang sous sa langue tandis que les fers la martelaient, la tailladaient, chairs et os.

Elle craignit bientôt autant de s'endormir que de s'éveiller, et tomba dans un marasme déprimant.

Un monceau d'obligations inabrogeables la dissuadèrent quelque temps de s'aventurer dans une nouvelle excursion, à son corps défendant : Elle s'étiolait entre ces murs aimant baguenauder dans les bois environnants, l'esprit jeté aux quatre vents.

Enfin vint un répit, annonce d'un automne finissant : l'hiver approchait. Ouvrant ses volets un matin, elle constata que le gel s'était invité à la faveur de la nuit. Arbres et herbes alentour arboraient un givre scintillant dans la lumière feutrée du soleil levant. Elle en resta saisie comme sa respiration s'accompagnait de volutes de fumée blanchâtre ; l'air était froid, revigorant. Elle déjeuna rapidement, revêtit sa grande capeline par-dessus son vieux manteau, chaussa ses bottes fourrées, enfonça un bonnet élimé sur sa tête et attrapant sa sacoche, ferma sa maison pour un temps.

Au-delà du village, une route désaffectée, que longeait une voie ferrée inusitée, menait à la vieille usine. Il fallait traverser ce lieu malplaisant et vide d'hommes pour atteindre la campagne, puis enfin la forêt. Elle retrouva son rythme de marche au bout de ces quelques kilomètres fastidieux et les miasmes de ces derniers mauvais jours s'évanouirent.

Elle aborda l'orée du bois allègrement.

De fins stalactites pendaient aux branches basses de chênes blancs et de hêtres aux troncs lisses. Plus loin, bordant la rivière, des peupliers aux dernières bribes de feuillages jaunissant tremblaient comme des vieilles femmes aux premiers frimas de l'hiver. Un calme souverain régnait.

Elle bifurqua à la croisée d'une sente à peine visible, le lierre et les feuilles à terre craquèrent sous ses pas. Un sansonnet, culotté, grattait la mousse, perché sur une vieille souche, elle ne l’effraya pas, passant son chemin puis son regard porta au-delà, attiré par un mouvement où s'élevait une clairière et sommeillait un étang. Il lui sembla distinguer une frêle silhouette sombre parmi les toiles arachnéennes de brume rampant sur l'eau ainsi que sur la berge lointaine qu'elle distinguait à peine. Cette présence piqua sa curiosité, elle s'avança à pas menus, évitant de faire le moindre bruit, mais le sansonnet pisota, ses trilles crevèrent le silence et la silhouette rabougrie se faufila plus loin puis, à sa grande surprise, grimpa dans une barque échouée sur une étroite crique. Les rames crevèrent bientôt la surface de l'étang dans un clapotis à peine perceptible. Le ou la batelière, courbée sur l'étrave, appliquait un mouvement lent et régulier à l'embarcation tout en se dirigeant vers l’île, véritable joyau d'émeraude posé au centre de cet écrin liquide. Une toute petite île, couronnée de saules imposants dont les branches effleuraient les rives douces. Leurs racines noires et tortueuses plongeaient sous la surface, s'ancrant dans le sol meuble des grèves. Quelques troncs foudroyés gisaient le long des bords où des cormorans, la tête sous l'aile, se serraient sur ces perchoirs improvisés, semblant ignorer l'esquif qui s'approchait.

La marcheuse, intriguée, les pieds dans l'eau, porta ses mains en visière et observa le manège de la barque mais celle-ci disparut bientôt derrière un promontoire cachant un minuscule embarcadère naturel.

Elle tendit le cou, mais ne vit plus rien de ce côté-là. L’écho d'une voix lui parvint dans le brouillard environnant. Elle en chercha l'origine, scrutant le paysage alentour.

À l'autre bout de l’îlot, sur un accul de sable pâle, elle découvrit une autre scène, tout aussi improbable, qui la laissa coite. Un personnage vêtu de bleu, de forte carrure, béret vissé sur le crâne, se penchait sur un tableau d'école, un tableau noir, de même qu'une gamine à queue de cheval attifée une robe fleurie. L'enfant récitait quelque chose, elle en aurait mis sa main au feu. Elle tendit l'oreille, abasourdie d'entendre ânonner l'alphabet ! Une vapeur froide s'évacuait de la bouche de l'enfant et de ses narines en cadence.

— K, L, M… soufflait la jeune l'écolière.

La silhouette maigrelette de tout à l'heure réapparut et s'approcha du couple à pas pressés. Elle portait un châle noir traînant jusqu'à terre qu'elle rabattit d'un geste lent et précis sur ses frêles épaules.

Et cela souleva bien des questions chez l'observatrice, pour qui ce geste et cette scène paraissait familière.

— Mais qu'est-ce que… —

Sa question resta en suspens.

Un bruit de galop fracassant retentit sur la rive opposée. Un cheval caparaçonné d'airain sortit de la brume et se révéla, se taillant un passage d'un écart à travers des taillis. Sa queue fouettait l'air, ses naseaux crachaient une fumée épaisse. Sur son dos, une ombre était penchée, accordant son mouvement à celui de la monture.

La spectatrice hoqueta, incrédule. Elle se frotta les yeux, supposant que peut-être, en tirant le voile obscurcissant sa vue, elle retrouverait le monde tel qu'elle le connaissait. En vain.

La bête à présent lancée poursuivait sa cavalcade.

Impressionnée, elle recula jusqu'à la berge, moulinant frénétiquement des bras et des jambes : Son cœur s'emballa et l'obligea à s'asseoir pour recouvrer un peu de calme et de lucidité. Un froid vif la saisit immédiatement. Elle posa sa main à son côté et sentit sans la voir une douce fourrure, épaisse et palpitante. Elle sursauta, et tendit la tête sur le côté. Le regard doré d'un grand chien loup, couché le long de son flanc croisa le sien. Il leva la tête et gémit doucement, sa langue rose entre les dents. Machinalement, elle caressa la bête sans comprendre.

Cela faisait bien trois quarts de siècle que cette bête qui se tenait devant elle était morte. C'était sa chienne. Une larme roula sans qu'elle ne fît rien pour la retenir et, les yeux brouillés, elle se tint immobile, attentive à la vie qui battait contre sa cuisse.

le bruit de la cavalcade allait crescendo ; l'échappée cavalière venait à sa rencontre.

Elle tressaillit, inquiète, mais le grand loup posa simplement sa large tête grise entre ses pattes sans manifester le moindre émoi.

Le cheval aborda la berge où elle se trouvait et ralentit pesamment. Son cavalier, tout en armure, se redressa et l’échine de la femme tressaillit. Un froid mordant se fit soudain connaître accompagné d'une brise aux arêtes plus aiguisées qu'un poinçon. Respirer devint douloureux. Le monde qui les entourait sembla se pétrifier totalement.

Le chevalier de métal démonta sans grâce, et le plumet noir qui surmontait son heaume tangua dans le jour blafard. Pourtant, sa prestance, indéniable, et son ombre, couvrant la femme toujours assise, à demi tournée vers lui, dégageait un fumet de peur sans nom. La main de la promeneuse se crispa dans le pelage épais, aux senteurs sauvages, du loup immobile. Elle croisa son chaleureux regard doré. Mieux que des mots, ses pupilles parlaient. Elles révélaient des mondes, des êtres que la femme n'avait jamais soupçonnés. Elles révélaient la profondeur même du monde sur lequel elle n'avait fait que marcher d'un pas insouciant et sans consistance.

Elle se détourna, soudain honteuse de son ignorance.

L'ombre grandit encore, tandis que sidérée, elle gardait les yeux baissés, admirant les grèves et solerets qui agrémentaient les jambes du cavalier. Des enluminures couraient le long des pièces, semblant vivre leur vie propre, tournaillant sur l'armure entière, traçant des runes étranges. Une cape de jais nouée sur les épaulières battait ses flancs. La visière du heaume se releva, et un abîme ténébreux plongea vers elle.

Elle sentit son regard plus qu'elle ne le vit quand bien même ses yeux le regardèrent enfin. Il fouaillait en elle, agrippait tendons, muscles et viscères : il l'explorait, voyait tout. Elle était comme nue devant lui. Elle pleura doucement.

Le cheval hennit puis fit un pas, et un souvenir vint effleurer l'esprit tourmenté de la femme. Un mot lui effleura les lèvres,

— Émeraude !

La jument s'avança vers elle, les plaques de sa barde cliquetèrent quand elle tendit son museau rose bordé de bai, et souffla dans les cheveux de la femme qui découvrit l'étoile blanche sur le chanfrein de la bête, en partie cachée jusque-là par l'armure et la maille. Elle soupira.

— Émeraude de la mer ! Mais comment ? Comment ? S'interrogea-t-elle, car c'était là sa jument indubitablement !

Ses larmes coulaient sans qu'elle n'y prête attention, car l'étrange cavalier, immobile, poursuivait son investigation, tiraillant sa peau flétrie, grattant ses os. Elle voulut chasser l'importun comme on chasse un moustique, et l'autre choisit ce moment. Il l'aborda.

— Je te connais Liane, dit-il d'une voix accorde.

Pourtant les mots n'avaient pas parcouru l'espace entre eux, ils s'étaient simplement inscrits dans son esprit.

Il se porta plus près d'elle tout en saisissant les rênes de sa monture.

Le chien loup en profita pour se faufiler entre les pattes puissantes de la jument et disparut ventre à terre dans le sous-bois.

Alors seulement Liane se souvint, tandis que le vieil homme au béret, qu'elle reconnut pour son grand-père, qui lui avait appris à lire, ainsi que la vieille qui les avait rejoints, qui n'était autre que sa grand-mère, accompagnés de la gamine endimanchée, lui faisaient signe de la main de leur île soudainement très proche du rivage.

— Grand-père ! S'exclama-t-elle, étranglée par l'émotion. « Grand-mère ! » Elle ne sut en dire davantage. À ces mots, les trois spectres s'amenuisèrent, perdant peu à peu leur substance, et s'évanouirent, brume dans la brume qui allait à présent en s'épaississant.

— La vieille femme, c'était…

— Qui tu voudras invoquer viendra s'il est dans mon royaume, la coupa le chevalier.

Elle se rendit compte qu'il ne la torturait plus, bien qu'un froid glacial persista. Il lui tendait sa main libre à présent.

— Mais, argumenta Liane, sans savoir que demander — tant de questions se superposaient dans son crâne trop petit pour tout contenir —

— Regarde encore, l'invita-t-il, de sa voix sans voix, aux tonalités rauques et chaudes comme une braise couvant sous un feu.

Elle s’exécuta et porta son regard au-delà de son hôte. Un couffin d'osier gisait sous le saule le plus proche, un enfant gigotait et babillait, bien au chaud, sous une chaude couverture. Elle parcourut sans bouger un cil les quelques mètres qui la séparait du nourrisson, hagarde.

— Mon enfant !

Le loup hurla de quelque part comme elle avait furieusement envie de le faire elle-même.

— Qui tu invoqueras viendra, Liane, s'il appartient à mon royaume, répéta patiemment le chevalier.

Au côté de l'enfant, se tenait un homme d'âge mûr, qui la regardait tendrement.

— Alex ? Elle crut s'évanouir. Comment son mari, son enfant, tous deux disparus depuis de longues années se tenaient devant elle ?

Le berceau se résorba, comme aspiré par la brume, le babille du nourrisson perdura un instant, puis ce fut le tour de son époux dont les contours se brouillèrent puis vinrent se confondre avec le paysage.

Seule la grande jument se tenait encore auprès d'elle. Elle se mit à parler. Sa voix appartenait à son maitre, elle en était le prolongement, tout comme son apparence, réalisa subitement Liane.

— Ceux qu'on invoque ne s'attardent pas dans le monde des vivants, dit-elle d'une voix douce.

— Mais vous êtes encore là ! Toi et ton cavalier ! Et je ne l'ai pas appelé, moi ! Protesta Liane.

— Non, en effet. Le temps a décidé pour toi. Ton temps est achevé Liane. Nous t'avons réclamé.

À ces mots, Liane un éclair de compréhension fit jour dans son esprit tourmenté. Elle se mit à crier et se débattre, tentant de s'extirper de cette gluante torpeur qui l'avait assailli depuis la venue de l'étranger.

Le chevalier saisit sa main avec une douceur inattendue.

— Ce qui est à moi est à toi à présent, dit la Mort.

— Puis-je refuser l'offrande ? Questionna Liane d'une toute petite voix, au bord de l'horreur.

— Refuser ? Le chevalier vrilla du néant de ses orbites caverneuses celles terrorisées de la femme. « Il est trop tard. Tu appartiens déjà au passé » répondit la Mort affable. L'ombre caressa du bout de son gant de daim la paume de Liane. Le geste était plus léger que le frôlement d'une plume sur la peau. Elle cessa de s'agiter.

— C'est la question d'usage, et l'usage est d'accepter, conclut-il doucement.

— Alors j'accepte ! Rétorqua la femme, un soupçon de défi dans sa voix tremblotante.

La Mort n'attendait que cela. Elle installa Liane sur la jument en un geste ample, comme si la femme ne pesait rien, et enfourcha à son tour le destrier qui volta sans effort. La cape claqua dans le crépuscule grisâtre, et Liane, parcourant une dernière fois des yeux ce lieu familier entrevit alors son pauvre corps reposant à demi dans l'étang. Sa tête dont les cheveux formaient comme une maigre couronne, s’enfonçait dans l'eau saumâtre, et une de ses mains, crayeuse, serrait en son poing une poignée de potamots sauvages.

— Morte —

La jument regimba, le cavalier tacla ses flancs musclés de ses éperons argentés et la bête prit le trot dès qu'elle regagna le chemin, puis engagea sans tarder un impétueux galop. La maille bruissait autour du couple dans un vacarme assourdissant. L'ombre de la mort les précédait, et aveuglait Liane à demi tandis que soudain, la nuit les recouvrit.

Ils disparurent sur le chemin de la lune, ou celui des étoiles, ou d'autres chemins encore : Il restait à Liane un royaume à découvrir !
