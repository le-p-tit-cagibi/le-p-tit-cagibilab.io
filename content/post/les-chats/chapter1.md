C'était un patelin perdu, à l'est du pays. La rue principale, déserte, ressemblait à une rue de cinéma en carton pâte, les jours fériés. Le soleil ne traversait pas la brume colorant le paysage d'une lénifiante teinte jaunâtre. Les trottoirs étaient encombrés de vieux débris à peine identifiables et les maisons serrées les unes contre les autres comme si elles avaient froids donnaient à l'ensemble un air de désolation et d'abandon. On n'avait guère envie de s’attarder là.

Je cherchais juste de quoi me réchauffer et un peu d'essence. Je me garai et entrepris d'explorer un peu le centre-ville ; personne ne pointa le bout de son nez. Je me rappelai le temps où cet endroit ressemblait encore à un village. Ma mère était venue se réfugier ici, après une beuverie paternelle de trop. Ma grand-mère y possédait un pavillon individuel, chose rare à l'époque, au numéro 7 de la rue de la gare (gare qui n'existait plus depuis bien longtemps !) et elle nous avait dégoté dans l'urgence et l'embarras où nous nous trouvions alors, une modeste bicoque à louer près du parc, route du moulin de Faux (le moulin aussi avait déjà disparu lorsque nous emménageâmes). Pas de numéro : il faut bien avouer que l'endroit avait tout du coin paumé.

Mes pas me menèrent dans le dédale de petites rues, alors que j'étais plongé dans de vieux souvenirs. Sur les poteaux électriques qui marquaient la mesure le long de mon chemin, apparurent quelques vieilles affiches agrafées dans le bois, battant la mesure au gré d'une petite bise frisquette. Je m'approchai pour en décrypter une qui me parut plus nette ou moins abîmée, selon le théorème du verre à moitié vide ou plein. Une photo estompée par les intempéries servait d’en-tête, on y distinguait le visage d'un homme d'une cinquantaine d'années et juste en dessous, imprimés en caractères larges et gras, je décriptai avec peine :

« On recherche Dick Brown disparu le vendredi 23 septembre 1991, cheveux bruns, maigre, taille 1,80 m, poids 55 kg.

Merci de contacter le comité de soutien des personnes disparues au… » suivait un numéro de téléphone rendu illisible avec le temps.

Sale type ce Dick. C'était mamie Honey, toujours si charitable qui avait tenu à éditer ces affiches… En une seconde tout me revint dans la figure ; je me souvins de l'oiseau, du grand chat blanc, si maigre, des cages, des drôles de sacs poubelles et soudain, j'eus dans l'idée que mes poumons n'aspiraient plus assez d'air. Je parcourus de nouveau la rue principale me retenant de sprinter, évitant désormais soigneusement de porter le regard sur ces affichettes lugubres et sautai dans ma voiture, résolu à tomber en panne n'importe où sauf ici, Lunaville, le village des chats.


— Salut Honey, quoi de neuf ce matin ? 

Dick Brown poussait son vieux chariot à balais, la casquette vissée sur le crâne, un éternel mégot éteint au coin de la bouche. Sur la poche gauche d'une salopette trop large pour lui, d'un orange délavé, était brodé d'une couleur indéfinissable à peine lisible, le nom du village qu'il parcourait chaque jour en tant que garde champêtre : Lunaville. L'homme s'attarda sur le bout de trottoir de Mme Pirson, Honey de son prénom. Le circuit qu'il opérait tous les jours était devenu un rituel depuis ces 5 dernières années et les habitants les plus conservateurs avaient à présent leurs habitudes avec lui.

— Ça va, ça vient, Dick, un petit café ? La voix parvint de derrière une porte tout près de là.
Le dénommé Dick se gratta la tête, relevant son couvre-chef et marquant ainsi l'effort de concentration que lui demandait une telle question. Il sembla peser le pour et le contre d'une halte chez cette vieille dame, ancienne institutrice, qui passait le plus clair de son temps à tricoter des couvertures multicolores.

— Allez, on dira que je suis en avance pour cette rue, j'ai droit à ma pause » Il sourit gauchement reposant sa casquette de guingois, jeta son mégot dans la poubelle fixée sur le chariot et gravit sans se presser les six marches du perron en se tenant à la rampe.

— Encore tes articulations ? S'inquiéta Honey, en ouvrant plus largement sa porte.

— Toujours ces satanées articulations ! Depuis la guerre, mes os, l’humidité, y supportent plus.
Il franchit l'entrée en bavardant de la pluie qui manquait et de la vigueur des géraniums en cette fin de saison.
Ron, le matou de la maison, noir comme un morceau de charbon, les moustaches frémissantes, cracha dès qu’il entrevit l'intru au coin de la cuisine et s'éclipsa par la chatière sans se retourner.

— Il est débile ce chat, soupira Honey, honteuse, une fois de plus, de l'accueil réservé à l'agent de la ville par son compagnon à quatre pattes.

— Bah, y s'habituera, vous inquiétez pas, à force, y s'habituera. 
Les yeux anthracite de Dick s'étrécirent en deux fentes encore plus obscures tandis qu'il suivait le départ précipité du félin.
Il prit son café, déjà prêt au coin de la table de la cuisine tandis que la vieille dame sirotait un thé, couvant l'homme à tout faire d'un regard presque maternel.

Un peu plus tard, Dick fit une halte chez Joey, le pharmacien, lui aussi en retraite. La matinée se trouvait déjà bien avancée, et ils partagèrent une bière fraîche sous la véranda, évoquant l'invasion de frelons qui avait lieu quelque part dans l'un de ces pays chauds, loin, si loin de leur petit monde. Un gros chat roussâtre du nom de Prout - car il émettait des gaz on ne peut plus odorants dès la dernière bouchée de pâtée avalée - s'était réfugié, sans que personne ne s'en étonne outre mesure comme chaque jour de la semaine à cette heure là, tout au fond de la buanderie, derrière les cageots que son maitre utilisait à l'automne pour ramasser et stocker les pommes.

Joey se réjouissait des visites du garde. Dans le bourg, ne restait quasiment plus que des retraités et des personnes d'un âge avancé depuis la fermeture de la dernière mine, quinze ans auparavant. Toute cette région subissait de plein fouet l'hécatombe industrielle et se vidait de son sang; les jeunes migraient à l'ouest, seuls les anciens ne voulaient pas partir. Ils tenaient à leur petite maison, leur bout de jardin et mourraient là. C'était comme ça. Alors papoter un quart d'heure avec un jeune gars encore plein de vigueur tel que Dick même si leurs de sujets de conversation ne s'avèraient pas très variés, était un petit moment au paradis en comparaison du radotage barbant de cette chère Mme Brey, par exemple, sa voisine la plus proche, sénile et fatigante à souhait. Elle qui perdait sans cesse quelque chose et venait sans vergogne le réclamer chez lui à n'importe quelle heure du jour ou de la nuit.
Depuis quelques années, Mme Brey égarait fréquemment ses affaires et même ses petits protégés : cette folle avait recueilli jusqu'à neuf grippeminauds dans sa bicoque branlante. Régulièrement, elle paniquait de leurs absences prolongées voire définitves et confiait ses inquiétudes répétées à Joey. Le pharmacien soupçonnait qu'à force d'oublier de les nourrir, les bestioles avaient tout bonnement décidé de crécher ailleurs. Pas plus tard que la veille, la vieille piquée lui avait fait part de la disparition de Fifi, sa belle chatte tigrée. Depuis quelques semaines, il ne lui restait plus en définitive que cet affreux Neige, un greffier haut sur pattes, maigre comme un clou, aux os saillants, dont le pelage loin de son surnom, évoquait plutôt la couleur pisseuse d'un bol d'urine. Seuls ses yeux étaient remarquables ; des yeux bleus en forme d'amande, curieux et intelligents.
Tandis que Joey songeait à tous ces petits tracas de voisinage, Dick parlotait sans discontinuer. Le pharmacien ne perdit pas pour autant ses bonnes manières et régala son hôte d'une petite mousse supplémentaire.

Assis sur le perron d'en face, Neige, le mal nommé, gronda mais n'esquissa pas un mouvement tandis que sa maîtresse, encore en robe de chambre, la mine défaite, le bras légèrement tremblotant, lui tendait affectueusement un trognon de chou rance. le félin ne s'en offusqua pas. Il ne quittait pas un instant du regard les deux hommes apparus sur le seuil de la maison d'en face qui se saluaient amicalement, l'ignorant totalement. Dick, le garde champêtre et Joey le pharmacien se quittèrent sous son regard appuyé.


Dick commençait son circuit de l'après-midi par le petit parc municipal, il ramassa en maugréant les sempiternelles déjections canines et félines, les papiers gras, pas beaucoup à vrai dire, et rencontra l'un des derniers gamins du village, assis sur le dos d'un banc, un oisillon dans les mains.

— Graves Donovan, tu fais quoi à cette heure de la journée ? T'as manqué l'école ? 

Le garçon qui ne devait pas avoir plus d'une dizaine d'années leva brusquement la tête d'un air coupable qui faisait peine à voir.

— Heu… Il referma ses mains sur l'oiseau blotti au creux de sa paume et affronta les deux billes de dur silex qui le fixaient soupçonneuses et en aucune façon amicales.

— Donne-moi cette saleté, et je chercherai pas à savoir ce que tu fichais ici ! Je doute que ta grand-mère soit fière d'un petit gars qui manque l'école et traîne les bois. 

— Je l'ai trouvé par terre, protesta le gamin, « je vais le ramener à la maison, maman et moi on lui préparera de la bouillie »

L'homme avait beau être décharné, il était plus leste qu'il n'y paraissait ; il tacla les mains de l'enfant et sous la violence du choc, elles s'ouvrirent grand, laissant apparaître un oisillon rose qu'il saisit et fourra vivement dans sa poche.

— File chez toi galopin, le houspilla le garde champêtre, les deux mains dans la salopette, serrant les poings.

Graves frissonna, sauta de son banc et se refusant à songer que dans une de ces grandes mains desséchées se trouvait l'oiseau, s'éloigna en courant sans reprendre haleine.
Dick haussa les épaules. Il y avait pas mal de vermine à s'occuper ici. Pas vraiment de temps à perdre pour de sales gamins. Il sorti de sa poche un paquet rose ensanglanté et le jeta vivement dans sa poubelle, reprenant son travail en sifflotant.

Le garde champêtre arpentait les ruelles situées autour du centre-ville en fin de service puis terminait invariablement sa journée par la Tambouillerie. C'était ainsi que tous ici nommait l'unique magasin du village. Mme et Me Tranquet, les propriétaires, y vendaient toutes sortes de marchandises : des allumettes en passant par du fil à pêche, du beurre mais aussi des piles, bref, un beau capharnaüm. Le couple comptabilisait largement 160 ans, de petites personnes à l'apparence fripée, ratatinées, que Dick nommait en son for intérieur Mr et Mme Troll, mais des personnes affables, bien comme il faut, toujours à rendre service.

Lucien le mari, l'interpella lorsqu'il rasa la boutique poussant son chariot bancal devant lui.

— Dick ! J'ai reçu ta commande garçon, viens donc cinq minutes, ça t'évitera de retourner ! » Cria l'ancien, un verre dans chaque main. « Une goutte pour se requinquer ? »

Dick s'avança, il n'aimait pas que l'on crie à travers tout le patelin qu'il « recevait des commandes », ça ne regardait personne après tout. Il entra autant pour faire taire le vieux chnoque qu'à l'idée d'une gnôle bien méritée. Ils trinquèrent à la vie, regrettant ceci ou cela, accompagnés par le grincement du rocking-chair où Mme Tranquet, Cerise, de son prénom, peignait doucement une chatte angora beige. L'animal, qu'elle surnommait Minette, ne ronronnait pas. Il était assis dans une position qui prêtait plutôt à penser qu'il allait bondir d'une seconde à l'autre. Il fixait Dick de ses yeux d'agate et pas un instant, un seul mouvement du garde ne lui échappa. L'autre ne lui jeta qu'un coup d'œil distrait, il songeait à son colis, au plaisir de le déballer bientôt, en espérant que l'objet soit bien celui attendu.

Ce dernier arrêt fut donc bref et, ayant fini ses heures, Dick rangea son chariot derrière la mairie, dans la remise dont il possédait un double de clés. Il vida la poubelle du jour dans la grande benne municipale, quitta sa salopette de travail pour une autre, en jeans, qui n'affichait aucun logo. Il pressa le pas dès la place de la mairie quittée, son colis sous le bras, pressé.

Graves grattait les oreilles de Mina, le chaton se mit à faire le pitre, elle n'avait que six mois mais chassait déjà et exécutait toutes sortes d'acrobaties, à la plus grande joie du garçon. Mina était un cadeau de maman, peut-être parce qu'elle culpabilisait de travailler à la ville et de ne rentrer que tard le soir ou peut-être parce qu'elle savait que son fils aimait les animaux et serait heureux d'en posséder un à lui tout seul plutôt que partager Ron avec grand-mère Honey. Mina était une chatte siamoise, oh pas une pure race, les Donovan n'avaient pas les moyens, mais malgré tout, la patronne de sa mère lui avait certifié qu'elle en avait « toutes les caractéristiques ». La seule qui intéressait vraiment Graves était son côté intrépide. Maman avait interdit qu'elle sorte du jardin, par peur qu'elle ne se fasse écraser, aussi jouaient-ils ensemble la plupart du temps dans sa chambre. Pour l'instant, le chaton ne manifestait guère d’indépendance et trouvait beaucoup de souris dans la maison ce qui ravissait Mme Donovan.

Juché sur le rebord de la fenêtre, le gamin leva la tête juste au moment où Dick Brown s'avançait le long de la route, transportant une boite en carton plutôt volumineuse. Instantanément, Graves se remmémora la scène de tout à l'heure avec l'oisillon et en ressentit un profond malaise. Le garde rentrait chez lui. Il habitait tout au bout de leur rue une cabane que le maire lui prêtait pour rien, vu l'état, un genre de vieux mobilhome posé au milieu d'un jardin en friche. L'homme devait s'y plaire car depuis qu'il avait débarqué ici, ce qui remontait à quelques années déjà, il n'avait jamais cherché à déménager.

Le gamin intrigué, posa la chatte et descendit dans la cuisine.

— J'vais faire un petit tour m'man.

— Rentre pour le dîner, 19h30 tapante s'il te plaît ! » Lança sa mère affalée dans le divan, de retour depuis peu du travail, épuisée, comme chaque soir.

Graves épiait parfois son voisin, qu'il trouvait étrange À vrai dire, il le trouvait à la fois effrayant et étrange. Il n'aurait su dire exactement pourquoi, mais à chaque occasion qu'il croisait sa route, il aurait fait dans son froc s'il n'avait pas serrer les fesses. Ce type avait un drôle de regard, une façon de dire les choses qui mettait mal à l'aise. Et puis parfois, il agissait très bizarrement, comme aujourd'hui avec l'oisillon.

Graves préféra ne pas se remémorer une fois de plus la scène de l'après midi. Il longea le grillage sur une centaine de mètres, traversa la rue, sans habitation de ce côté là, puis de nouveau longea une haute clôture métallique qui cernait le terrain vague et le mobilhome de Dick. Il fit discrètement le tour de la propriété et pénétra sur le terrain du garde champêtre en enjambant quelques orties et surtout un assortiment de vieilles caisses empilées sur un angle de la clôture invisible depuis le cabanon. Un adulte aurait écrouler la pile en tentant l'escalade mais le garçon était fluet et agile; il se récupéra sur ses pieds de l'autre côté, sans un bruit. Le terrain était empli de hautes broussailles, de vieux restes de motoculteurs, de pneus, de ferrailles, de tôles et de tout un tas de matériels rouillés et cassés. Autant dire qu'il était facile pour un enfant de passer inaperçu dans ce champs de détritus. Il progressa courbé en deux à travers le jardin d'immondices, se rapprocha au plus près du mobil home, s'accroupit et observa.

Dick refermait la porte de son bungalow d'un coup de pied. Il posa son colis sur la table encombrée de vaisselle sale, arracha le papier kraft et saisit un couteau afin de détacher la ficelle qui cerclait la boite en carton. Enfin, le cœur battant, il découvrit sa nouvelle cage-piège. L'acier rutilait. Le balancier grinça lorsque l'homme toucha la cage et se mit en devoir de tester la sensibilité du mécanisme de bascule, comme l'indiquait le mode d'emploi : 
« Pour attirer le nuisible, placez un appât au fond de la cage ouverte. Captivé par l'appât, l'animal pose les pattes sur le balancier et déclenche le système, la trappe se referme »

La porte du piège claqua sur le bras de Dick dès qu'il posa un doigt sur la plaque qui servait de contrepoids. Il sourit béatement, se remémorant ses premiers poussins.
À cette époque, il logeait dans une métairie, sa mère et son père travaillaient, tâcherons qu'ils étaient, et lui effectuait quelques maigres corvées surtout concernant la volaille. Donner le grain, nettoyer les pondoirs, gratter les perchoirs, ramasser les œufs… Il avait trouvé par hasard un poussin mort. Le pauvre animal était desséché déjà, sa mère l'ayant couvé sans se rendre compte qu'elle l'avait écrasé ou étouffé. Dick le conserva longtemps dans sa petite boite de bois où il rangeait ses soldats et ses billes. Il lui aménagea un petit coin à part, sur un carré de tissu volé dans la travailleuse de la patronne profitant d'un moment d'inattention générale : Un petit carré de soie jaune bouton d'or, une ravissante couleur. Le poussin se décomposa lentement sous ses yeux au fil des jours, mais Dick ne parvenait pas à s'en séparer. La dépouille finit par tomber en poussière et le gamin qu'il était alors en conçut une peine et une frustration inconsolable.

Plus tard, songea-t-il encore, poursuivant le fil de ses réminiscences, ses doigts courant rêveusement sur la cage, il s'éprit d'un chaton malade errant derrière la bauge des cochons. Il le recueillit, observa sa lente agonie, attendant patiemment de pouvoir disposer du corps afin de l'arranger dans sa petite sépulture de fortune. Lui aussi reposa longtemps sur le morceau de soie jaune bouton d'or, une rose séchée entre ses petites pattes étiques et pelées : Ainsi était née la passion de Dick pour les chats.
Les chiens étaient trop gros, et leurs propriétaires les réclamaient à grands cris, à contrario, les chats manquaient rarement à leur maitre, surtout dans ces années-là. Dick eut toujours dès lors un compagnon qu'il affectionnait au fond de sa boite secrète. Parfois, il se l'accaparait déjà mort, sec et miteux, abandonné dans un coin, parfois les asticots grouillaient encore au milieu d'une charpie de poils et d'os, mais ce qu’il préférait, c'est lorsque l'animal était encore tout chaud. C'était très rare. Un vrai coup de chance.

Alors il avait commencé à piéger les chats.

Un seul à la fois, seulement pour avoir toujours un petit compagnon sage et silencieux à ses côtés. Il avait longtemps fait avec les moyens du bord et parfois ça ne se passait pas bien. Dick perdait son calme et cela finissait invariablement dans le sang. C'était dégoûtant.
Dick grandit sans que cette manie ne s'apaisa. Tout au contraire À une certaine époque, il regardait ces émissions sur la pêche et la chasse qui passaient en boucle la nuit sur sa mini télé. Un spot publicitaire avait vanté les mérites de la cage magique. Une sacrée découverte ! 
Dick sourit.
Sa première cage avec sa première paie. Son premier job ! Il était resté quelque temps dans ce patelin, comment déjà ? Mons oui… Mais il avait eu quelques ennuis et avait dû partir. Depuis lors, sa vie n'avait été qu'une longue suite d'étapes plus ou moins prolongées dans de petites villes comme Mons.

Mais il espérait secrètement que Lunaville serait son havre. Ici, que des petits vieux gâteux, pas de mégères, pas de pères surexcités par la perte du chat de la famille, pas de flics non plus. Ici, il entrevoyait finir ses jours tranquillement, avec une boule de poils puis une autre, au fond de sa boite, reposant confortablement sur un carré de soie bouton d'or. Et il y en aurait toujours une pour lui tenir compagnie, il n'en manquerait jamais, car dans ce bled, il y avait beaucoup, vraiment beaucoup de matous.

Graves décolla sa frimousse de la vitre noire de crasse, il avait rampé près d'une fenêtre. Il n'y voyait pas grand-chose, le type avait reçu une espèce de boite métallique, pas de quoi s'époustoufler. Il redescendit précautionneusement jusqu'au bas du jardin défoncé d'ornières où gisaient maints déchets comme autant d'obstacles sur son chemin alors que la nuit s’avançait. Il cogna brutalement la poignée d'une brouette dont la benne avait disparu et le son sembla se répercuter jusque dans l'espace. Le gamin s’immobilisa. La porte du mobilhome s'ouvrit d'un coup. Graves se mit à courir sans réfléchir, invisible en raison des grandes herbes et des tôles. Il fonça, grimpa le grillage, retomba sur les caisses, et roule-boula jusqu'au sol sans oser respirer. Il attendit là. Des pas résonnèrent dans le terrain vague, un fracas retentit mais personne ne s'approcha de son coin. Il resta dissimulé un moment encore, le cœur battant, se reprochant sa couardise. La porte du bungalow gémit une fois de plus, l'homme devait être rentré à l'intérieur. Graves fila chez lui, il arrivait tard, sale et crotté, sa mère le corrigea d'une taloche et l'envoya au lit sans dîner. Il n'en fut pas malheureux. Sous sa couette, il s'endormit en se demandant pourquoi il avait éprouvé une telle trouille encore tout à l’heure. Une peur bleue.

Dick avait entendu du bruit dehors, et cru que sa ruse avait fonctionné. Il s'empressa d'aller vérifier sous le bungalow là où il l'avait placé quelques heures plus tôt, mais non, il n'y avait rien dans sa vieille cage. La porte abaissée n'emprisonnait que du vide ! C'est ce qui l'avait d'ailleurs décidé à renouveler son matériel. Depuis quelque temps, le piège se déclenchait systématiquement sans succès, trop usagé sans doute, le mécanisme ne fonctionnait probablement plus correctement… Pourtant la pâtée n'était plus là ell ! Rumina Dick. Quelque chose mangeait l'appât à l’intérieur de la cage et ressortait du piège sans dégât. Cela le rendait fou. Parfois il en pleurait de rage. Mais jusqu'ici, il n'avait pas perçé à jour le stratagème qui permettait au voleur de s'en tirer sain et sauf.
De dépit, il saisit la vieille cage et la balança avec fracas dans la gueule béante d'un tonneau de plastique bleu juste derrière le séchoir et s'en retourna dépité à l'intérieur.
Autant installer ma nouvelle acquisition tout de suite, pensa-t-il avec un regain d'entrain. Pour l'occasion, il avait même gardé un superbe morceau de côtelette de porc. Le fumet qui s'en dégageait serait irrésistible…
Dick décida de changer de coin, sans trop s'éloigner parce qu'il voulait être sûr d'entendre le déclenchement du mécanisme ; il positionna le piège à coté d'un vieux réfrigérateur qui reposait ventre ouvert à quelques mètres de la fenêtre de sa chambre. Invisible de la rue bien sûr : Les gens ne comprenaient pas vraiment ceux qui aiment les cages et les bêtes mortes.
Ce soir là, il se coucha serein, confiant en les performances de son nouveau jouet.

— Salut Honey, quoi de neuf ce matin ? »

— Vient prendre un café Dick, le gamin est là, c'est mercredi, je fais des crêpes, t'en mangeras bien une ou deux avec nous ?

— Ça se refuse pas une offre généreuse comme ça, déclara le garde champêtre en pénétrant dans la cuisine, sous le regard méfiant de Graves.

— Salut gamin, ajouta-t-il d'un ton froid. L'enfant pris ses crêpes dans sa main et recula jusqu'au salon mais Mina se fourra dans ses pieds et il s'étala de tout son long.

Honey se précipita le relever en le grondant.

— Oh ! qu'il est beau le petit chat, s'exclama Dick hypnotisé par la jeune bête s'approchant de lui avec force de cabrioles.

Le siamois s’avançait en sautillant de manière comique, la queue relevée, le poil hérissé et l'homme se mit à rire grassement. Il étendit son bras pour rafler la bête qui passait à sa portée mais ne rencontra que le vide : Graves avait été plus rapide. Il tenait Mina dans son giron et s'éloignait déjà tandis que l'homme lui jetait, de dépit,

— Je l'aurai pas mangé hein ! Hein Honey ? 

— T'inquiète pas, Dick, les gosses, c'est comme ça, ils nous manquent de respect. Elle ajouta en soupirant, se tournant là où son petit fils avait disparu, « Grave ! ne t'éloignes pas ! Tu as tes devoirs à faire »

Mais le gamin resta invisible.

Dick prit congé de Mme Pirson, essayant de se concentrer sur son travail mais plusieurs fois son esprit revint à la facétieuse petite créature aperçut chez Honey. Il eut hâte de terminer sa journée et vérifier s'il avait attrapé quelque chose. Il s'autorisa tout de même une petite eau de vie, une poire maison, chez Lucien puis fila au bungalow dès dix huit heures tapantes.

Le portillon de sa toute nouvelle cage-piège était abaissé. Incroyable ! Dick souffla, la colère lui monta aux tripes. Il réenclencha la porte après avoir repositionné un gros morceau de jambon en guise d’appât. Il ne décoléra pas de la soirée et plutôt que de se vautrer dans son antique canapé avec une bière, décida qu'il était en guerre et comme à la guerre, il résolut d'organiser une surveillance de l’ennemi, ne pas le lâcher d'une semelle. Il surveillerait autant que néccessaire, mais découvrirait le fin mot de ce mystère.
En oubliant jusqu'à se sustenter, la cigarette à demie consumée collée à la lèvre inférieure, Dick s'installa le long de la fenêtre donnant sur le vieux frigo, le derrière calé sur un vieux pouf, la tête appuyée sur le carreau sale. Il passa son mouchoir sur la vitre afin de voir dehors. La nuit tombait, silencieuse, hormis les rossignols habituels ; le garde avait une vue parfaite sur sa belle cage. Il attendit. Les heures passèrent lentement, il se mit à somnoler et se sermonna de ne pas tenir le coup rien qu'une seule nuit. Trouffion, il avait subi bien pire sans être à l'abri sur un coussin moelleux.

Un grattement lui parvint de l’extérieur qui le fit se redresser ; il plissa les yeux pour y voir. La lune éclairait le jardin à présent, aussi n'eut -il aucun mal à repérer l'intrus. Un grand chat blanc et maigre : Neige ! Se dit-il en écarquillant les yeux.

En effet, Neige grattait tout près de la cage. L'animal introduisit une patte, puis l'autre précautionneusement, tout en évitant la balance. Dick retint son souffle, son rythme cardiaque s'éleva brusquement. Le chat, astucieux, faisait preuve d'une dextérité remarquable ; il détendit sa patte gauche et ramena à lui la viande du bout des griffes tandis que, le corps arqué sous l'effort, son épaule maintenait la porte. Un clac se fit entendre et Dick sourit avant de jurer : Le félin s'était reculé suffisamment rapidement, emportant la nourriture et laissant le piège se refermer trop tard.

Le garde champêtre fulminait. Il sortit et invectiva la terre entière à voix basse. Il vérifia et réarma sa cage. Il se promit de venir à bout de cette sale bestiole qui le narguait, il se le jura et sa dépouille, lavée de frais, brossée, de nouveau blanche et douce irait rejoindre sa boite, oui, il voulait celui-là et aucun autre.

Les jours succédèrent aux jours, Dick faisait sa tournée, dormait peu car dès le crépuscule, Neige apparaissait et désactivait le piège en emportant l'appât. L'homme devenait fou. Il se mit à le pourchasser parfois dans le jardin, et se prit un soir la jambe dans un vieux bout de grillage masqué par les hautes herbes. Il n'eut d'autre choix que de se rendre chez son ami le pharmacien pour faire recoudre cette mauvaise plaie. Joey ne lui demanda aucune explication cependant Dick sut qu'il devait faire preuve de plus de prudence. Il risquait d'attirer l'attention, surtout dans un bourg où il ne se passait jamais rien !

Rentrant chez lui un jeudi entre chien et loup, il fut surpris par de petits miaulements provenant de son terrain. Il se précipita : le piège s'était miraculeusement refermé sur Mina. Dick battit des mains, comme un gosse, vérifia d'un coup d’œil que la rue était vide et avec d'infinies précautions, remisa le chaton tel un trésor, dans un des coffres qui lui servaient de siège dans son mobilhome. La jeune bête protesta énergiquement, miaulant tant et plus mais Dick s'amusa toute la nuit de ses cabrioles, oubliant sa cage, dehors, dont la porte claqua pourtant ironiquement à l'aurore après une manoeuvre subtile du démoniaque Neige.
Dick était indécis.
D'un côté, Neige tardait à se faire prendre et de l'autre, la bestiole, il avait afflubé le chaton de ce petit surnom, offrait une possibilité tout aussi réjouissante de combler sa boite à souvenirs désespérement vide. Après moults tergiversations intérieures, le garde, victime d'un dilemme shakespearien, reporta sa décision. Il continuerait de chasser Neige, et profiterait des facéties de la bestiole, elle était si amusante ! C'était aussi prendre des risques…
Il lui faudrait redoubler de vigilance et de prudence avec ce fouineur de gamin ; il allait certainement remuer ciel et terrre pour retrouver son chato !
Et c'est moi qui l'ai ! Dick rit comme un gosse. Il était heureux. Heureux du tour qu'il jouait au gamin autant que de son futur nouveau compagnon, qui bientôt rejoindrait sa boite, installé sur son petit foulard jaune. Il faudrait qu'il lui rapporte une joli fleur, un myosotis peut-être, ou un coquelicot.

Au matin du vendredi, il réarma le piège car sa détermination n'avait pas faiblit, il voulait la peau de Neige. Et il l'aurait, en guerrier revenu de l'enfer, il n'allait pas se laisser avoir indéfiniment par un maudit chat galeux, il se l'était juré.

Le reste de la journée ne passa pas assez vite à son goût. Il bâcla son travail, négligea le parc. Lucien l'attendait avec un petit verre mais l'entrevue ne fut pas si agréable que d’habitude, le vieillard lui demanda s'il n'avait pas aperçu un chaton, le petit Donovan avait perdu le sien…

Enfin, le chariot une fois remisé, Dick se hâta jusqu'à chez lui.

Il cassa la croûte joyeusement, partageant son repas avec Mina qui cherchait à s'échapper, sans succès.
La nuit vint le surprendre. La rancune tenace, Dick se posta donc aux aguets une fois encore. Bientôt, un grincement familier se fit entendre…


Pendant ce temps, depuis la veille, Graves, atterré, faisait le tour de ses voisins, mais personne ne semblait avoir aperçu Mina. Il pleurait, se sentant terriblement coupable de la fugue du chaton. Mamie Honey l'avait même consolé, ce qu'il n'avait plus accepté depuis ses huit ans au moins.
Sa mère rentra du travail et l'aida refaire le tour de la maison, elle avait du mal à imaginer la petite bête aller très loin. Graves se réfugia sur l'escalier, incapable de réfléchir. Sa peine grandissait à chaque instant, il cherchait vainement où pouvait bien être passer Mina. Pensif, il leva la tête par la lucarne installée à mi-hauteur du premier étage et soudain, sa respiration s'arrêta.

En face de lui, par dessus quelques terrains vides et vagues, il apercevait le bungalow de Me Brown. Et sans savoir vraiment pourquoi, ni comment, il eut la conviction que c'est là-bas qu'il trouverait son petit compagnon à quatre pattes.

— M'man je vais refaire le tour du quartier ! 

— Ne va pas trop loin, il fait noir déjà dehors, je doute que tu la retrouves maintenant, tu ne veux pas attendre demain ? Mais voyant la mine dévastée de son fils, Mme Donovan renonça à pinailler.

— Va, mais pas de bêtise. Elle lui ébouriffa la tignasse et le suivit des yeux, galopant à toutes jambes dans l'allée.

Graves croisa Neige alors qu'il arrivait devant le grillage et les caisses.

— Tiens, toi aussi tu cherches Mina ? 
L'enfant caressa la tête jauni du grand chat. Ses yeux bleus, splendides dans la pénombre brûlaient tels de la glace. Il miaula doucement puis d'un bond agile suivi d'un autre, il bascula côté jardin du garde champêtre. Il y avait de la lumière dans le bungalow. Graves attendit un peu avant de suivre Neige. Il se décida enfin et, sautant lestement sur les caisses, se rétablit dans l'herbe grasse sans difficulté. Il avançait en catimini vers le carré de lumière, prenant garde de ne culbuter aucune des vieilleries qui parsemaient le terrain vague quand soudain il remarqua la porte qui s'ouvrait doucement. Il redoubla de prudence, et s'approcha afin de mieux voir sans être toutefois à découvert. Une méchante trouille lui mordit le bas du ventre et tordit ses intestins. Il grimaça, une sueur glacée lui coulait entre les deux omoplates quand il distingua nettement la silhouette du garde champêtre se découpant dans l'ombre de la porte.

Dick croisa les yeux du greffier, ils brillaient et se riaient de lui ; cela mit le garde en rogne. Cette fois, il s'était armé d'un bâton, un bon bâton, bien solide et il allait casser les reins de ce vieux matou. Ah ça ! Oui. Il allait le pulvériser. Ce bon vieux Neige passait les bornes.

Le chat, guère impressionné par l'homme immobile sur le seuil du cabanon, passait déjà avec habileté sa grande patte maigre à travers la cage et grappillait le lard sur la bascule. Encore une fois, il recula et s'éloigna vivement du piège esquivant par la même occasion le gourdin que l'homme avait soudain violemment projeté devant lui. Sain et sauf, il feula crânement en direction de son agresseur.

Graves s'alarma des grondements félins qui lui parvenaient et tendit le cou de derrière un train de pneus usagés recouvert d'une tôle rouillée où il avit trouvé réfuge. Il discerna autre chose qui lui sembla familier ; des miaulements, ou plus vraisemblablement, des piaulements de chaton apeuré qu'il situait non loin du drame qui se déroulait devant ses yeux. Il ouvrit grand ses yeux et ses oreilles.Le grand chat blanc, arqué sur ses pattes, reculait sans perdre le garde champêtre de vue tandis que celui-ci récupérait son baton et s'apprêtait à le balancer de nouveau à la tête de ce pauvre Neige. Mais cette opportunité lui passa sous le nez : d'une brusque détente Neige détala ventre à terre, sans perdre le fruit de sa rapine tenue ferme dans sa gueule. Dans sa fuite, il frôla le gamin trop surpris pour réagir. Dick jura et se mit en tête de le poursuivre. Graves se dit qu'il n'avait aucune chance, la bête s'avérait bien plus leste que le bonhomme. Pourtant, Neige s'arrêta devant le grillage, le dos rond, semblant acculé. Il éprouvait certainement quelque difficulté à grimper avec sa pitance brinquebalante en travers de la gueule. Brown était presque sur lui lorsque contre toute attente, il s'élança de nouveau avec souplesse par dessus le treillis metallique et s'éloigna dans la rue sans se presser. Furieux, son poursuivant s'attela à sauter lui aussi l'obstacle.

Bizarre ce chat, se dit l'enfant incrédule, presque à croire qu'il attend ce fou de garde champêtre…

Des miaulements désespérés redoublèrent le ramenant à plus urgent. C'était sa chance, l'homme était loin, il devait en profiter maintenant pour s'introduire dans le bungalow récupérer Mina. Il était sûr que c'était elle qui pleurait quelque part. Il progressa vers la bicoque le plus rapidement possible, oubliant toute discrétion.

Dick, tout à sa chasse, ne se rendit compte de rien. Il grimpa après le grillage qui plia sous son poids et se récupéra tant bien que mal sur les caisses mais celles-ci cédèrent sous leur fardeau. S'en suivit un vacarme qui aurait pu réveiller tout le quartier, il perdit l'équilibre et atterrit maladroitement à une dizaine de mètres de Neige environ. Le bonhomme, enragé par sa traque jusque là infructueuse, se dressa rapidement sur ses pieds, récupéra son arme de fortune et se remit immédiatement à la poursuite du matou. Il se dirigeait vers la sortie du village. Au croisement, il bifurqua à gauche, Dick sur ses talons.

Le chemin de l'ancienne mine, supposa le garde champêtre. Il fulminait de sa maladresse, allant de déconvenue en déconvenue. Une de ses jambes, blessée, l’élançait et il se mit à boitiller. Loin de renoncer, traquant des yeux la tâche blanche qui s'évanouissait et réapparaissait furtivement parmi les ombres de la nuit, il s'orienta au jaugé et poursuivit implacablement sa marche, résolu à aller au bout de sa vendetta.

Graves entra dans le bungalow. Mina, il n'eut plus aucun doute là-dessus, miaulait comme si sa vie en dépendait et c'était peut-être le cas à vrai dire. Il fureta frénétiquement dans l'entrée, les cris s'intensifièrent mais il ne voyait toujours pas le chaton. Il pénétra ensuite dans le salon, et à son grand soulagement, un museau rose apparut à travers une grille d'aération des coffres de bois faisant office de banquette. Il repoussa les coussins sales et puants, ouvrit le couvercle de bois avec fracas et le chaton lui sauta dans les bras. Son cœur battait la chamade.

Graves ne se perdit pas en réflexion.
Il fallait filer et vite, ce type était cinglé, un kidnappeur de chat ! Et que comptait-il en fair ? Ces pensées l’assaillaient vainement tandis qu'il franchissait le seuil du cabanon et s'élançait, courant comme un dératé jusqu’à la barricade. Il avisa trop tard une grande poubelle qui lui barrait le chemin. Il la percuta de plein fouet, s’affalant dans de grands sacs noirs et gluants. Il perdit quelques secondes à reprendre ses esprits, glissa encore et réussit enfin à se remettre debout, l'oeil rivé, incrédule, sur les sacs. L'un d'eux plus particulièrement, qu’il avait crevé dans sa chute, attira son attention : celui-ci débordait de viscères verdâtres et de sang, beaucoup de sang coagulé en caillots visqueux abjectes. Il s'en détourna saisit d'un hoquet de dégoût.
Instinctivement, il vérifia qu'il n'avait pas lâché Mina, la serra un peu trop fort contre lui, ce à quoi elle ne protesta que mollement, pelotonnée dans son giron. Le gamin se sentait désemparé et cette petite présence ronronnante le réconforta. Il regarda autour de lui.
La nuit profonde et silencieuse étreignit Graves avec une grande violence. Sous un pâle et timide quartier de lune, des arbres rabougris penchaient de la rue leurs bras squelettiques. Leurs ombres fantômatiques semblaient emplies de malveillance. Pris d'une panique soudaine, il s'enfuit, volant littéralement par dessus le grillage sans se soucier de ce que les caisses, en miettes, ne pourraient plus le réceptionner. Il alla chuter lourdement sur le macadam À vrai dire, il n'en éprouva aucune douleur, pourtant son pied racla le sol et se tordit. Il n'en stoppa nullement sa course pour autant, hurlant intérieurement de frayeur, les poumons en feu, springtant avec opiniâtreté, plus rapide que le vent - plus rapide que la mort - 


Dick courait lui aussi à présent. Sa jambe lui faisait un mal de chien, mais il ne voulait pas se laisser distancer, le matou avait un peu d'avance mais pas beaucoup ; il l'aperçut à l'entrée de la grande porte déglinguée qui obstruait la mine.

— Ah ! il est coincé cette fois ! se réjouit-il, raffermissant sa prise sur la matraque. Pourtant Neige ne ralentit pas devant l'obstacle et se faufila entre deux battants de bois mal joints. Il disparut. Toujours sur ses talons, Brown se mit à frapper furieusement sur les planches jusqu'à créer une ouverture suffisamment large pour s'y introduire.
Une pénombre sépulcral l'accueillit. De temps en temps, clignotait une vieille lampe au-dessus de ce qui restait de la porte principale, et un chapelet de veilleuses se relayaient tout le long d'un boyau qui s'enfonçait au-delà de son champ visuel.
— T'es cuit, sale chat miteux ! hurla le garde hors de lui. Les sons se répercutèrent sur les parois de terre et de bois de la galerie sans trouver le destinataire de l'invective.
Tout en progressant avec circonspection, il braillait d'autres insultes et apostrophait l'insaisissable félin.

— Où es-tu bâtard de chat de mes deux ? Viens par ici que je t’étripe ! Minou minou… 

Neige finit par se manifester un peu plus loin, au détour d'un coude plongé dans une obscurité absolue. Il se tenait placidement installé sur un tas de vieux sacs de ciments sous une lampe dont la lueur vacillante faisait flamboyer ses pupilles et rougeoyer son pelage, donnant à sa physionomie toute entière une allure quasiment diabolique. Son ombre mi féline, mi spectrale, se projetait en dansant sur les murs suintants de la mine.
En l'apercevant, Dick perdit toute contenance : il se rua en avant et lorsque se découvrit juste sous ses pas, le sol effondré donnant sur un puits, il était déjà bien trop tard. L'homme bascula sans grâce et sans un cri. Neige, qui n'avait pas bouger ne serait-ce un poil de moustache, le surplombait à présent ; il le regarda tomber.


Graves avait retrouvé le sourire, son coeur ne battait plus la chamade et si sa cheville, contuse, le lançait, il n'en avait cure. Confortablement installé sur les marches de l'escalier, un grand bol de lait chaud posé non loin de lui (bol servi par sa mère, heureuse du dénouement de l'affaire que Graves s'était bien gardé de lui raconter en détail), il jouait avec le chaton miraculé. Ce petit intrépide, déjà remis de ses émotions avait investi la boite de couture de Mme Donovan abandonnée sur le palier du premier étage. Mina entreprit d'en extirper, à grand renfort d'acrobaties, bobines, cordons, froufrous, ganses et bien d'autres choses encore. La boule de poil parut s'attarder un instant à réfléchir lequel de ces trésors elle allait bien pouvoir chiper et soudain déterminée, entama l'attaque en règle d'un long ruban dont elle eut bientôt le plus grand mal à se dépêtrer. Graves n'en pouvait plus de rire, mais il eut pitié de sa petite compagne, la souleva délicatement par la peau du cou et démêla le cordon qui la saucissonnait. Il la libéra enfin. Ce faisant, le joli ruban, happé par un malicieux courant d'air, virevolta par la fenêtre du palier et s'emberlificota dans l'antenne de toit de Mr Brown. Il se mit à flotter au vent tel un étendard. Il était d'un joli jaune, à dire vrai, un jaune bouton d'or que Mina admira un instant, puis le chaton se détourna, l'air satisfait, et rejoignit son jeune maitre en cabriolant.


L'homme n'en finissait pas de dégringoler dans ce puits étroit où neige l'avait conduit. Une chute vertigineuse dont la réception fut catastrophique pour le squelette de Dick.
Il s'évertuait à hurler mais bientôt coincé tout au fond, il ne pouvait plus guère bouger ; sa mâchoire s'était brisée, il essaya tant bien que mal de brailler mais ces borborygmes ne tardèrent pas à se perdre dans les bulles de sang qui sortaient de sa bouche. Un de ses bras faisait un angle étrange, et l'une de ses jambes avait disparu sous lui.
Neige miaula. Dans la pénombre, des falots jaunes, gris, verts apparurent soudain autour du puits, des ombres à quatre pattes dansaient au-dessus de Dick.
- Au-dessus du piège -
Dick tenta encore un mouvement mais son corps n'était plus qu'esquilles d'os et souffrance.
Les chats se rapprochèrent encore, penchant l'échine au-dessus du trou en grondant. Il y avait là des matous de toutes sortes, certains étaient inconnus du garde-champêtre, au bas mot une trentaine de bêtes, peut-être plus, l'observaient. Il reconnut parmi eux Ron et Minette et Prout et Neige bien sûr. Ce dernier feula de manière terrifiante et ce cri rallia tous les félins présents qui s'élancèrent, crocs et griffes en avant à l'assaut de leur tortionnaire, prisonnier impuissant.

La dernière pensée de Dick fut pour sa jolie boite abandonnée avec sa dernière dépouille… Fifi, la jolie tigrée, reposant sur un carré de soie jaune, jaune bouton d'or.
