---
title: Hommage à Mr King
author: SB
rights: Creative Commons Non-Commercial Share Alike 3.0
language: fr-FR
date: 2021-05-25
summary: "« Cher Mr King, Je suis votre plus fidèle lectrice… »<br>Et merde, encore une… à laquelle il va falloir répondre ?"
epub: true
pdf: true
draft: false
---
« Cher Mr King,

Je suis votre plus fidèle lectrice depuis trente ans et je tenais par cette humble lettre, à vous… »

Une main métallique aux longs doigts argentés froissa rageusement le papier puis une voix s'éleva quelque part, cliquetante et glaciale, chevrotante comme celle d'un vieillard.

« Putain, Méphisto, encore une barge qui va me refiler sa prose en PJ dans deux ou trois phrases… »

En deçà des mains mécaniques, une armature d'acier, démunie de chair, constituait les bras de l'étrange chimère et des câblages bleus et rouges de diamètres divers prenaient lieu et place de veines et artères. Cet assemblage filait vers les hauteurs d'une immense salle s'apparentant plus à un entrepôt qu'à un espace de travail. Au milieu du plafond, à la croisée de larges poutres rouillées aux boulons dangereusement dessertis, les câbles se rassemblaient en un long serpentin qui courait et rampait autour d'un treuil hydraulique, puis s'enfonçait dans l'ombre pour se ficher dans une sorte de petit terminal rectangulaire de verre translucide, de la taille d'un petit aquarium. À l'intérieur, malgré la hauteur, on distinguait… un cerveau. Un cerveau humain, qui luisait et pulsait d'une vie inorganique.

Un chat étique, au pelage charbonneux se faufila langoureusement parmi les flopées de connexions serpentant entre les deux bras articulés et l'unité centrale, tout là-haut. Il ronronnait. De ses larges yeux dorés, son museau trop plat et ses longues moustaches filamenteuses exhalait une malfaisance inquiétante.

Au-dessus du cervo-bloc, une large plaque, certainement rutilante en des temps reculés, arborait à présent un méchant ton de cuivre terni. Elle pendait de guingois, et, plissant les yeux dans les ombres de la singulière bâtisse, l'on pouvait déchiffrer : KING STEPHEN modèle XXI

La voix grinçante reprit,

« Méphisto, Méphisto, que des tarés, j'te jure ! Ah ! Où en étais-je ? »

Le félin atterrit avec souplesse non loin des articulations métalliques, froufroutant comme une danseuse ivre ce qui n'entama en rien leur ballet frénétique. Les doigts d'acier poli frappaient un double clavier lumineux posé sur un bureau hors d'âge ; Le sous-main de cuir, ancien, tanné, se marquetait d'encre sur les coins. Entre les pieds du vieux meuble de larges tiroirs débordaient de parchemins jaunis. Un plumier d'obsidienne renversé sur un buvard rose accompagnait une paire de serre-livres de bronze en forme de dragon aux ailes déployées qui menaçaient de dégringoler de l'épais plateau de chêne composant le bureau.

Quelque part au-dessus de chat et maitre, un zonzonnement familier indiquait la présence d'une imprimante, dont les buses couinantes n'étaient plus de première jeunesse. À intervalle régulier, réglé comme un métronome, une feuille de papier chutait de quelque-part sous l'entre-toit, tourbillonnait, exécutant un ballet de pétales pâles mué par un vent d'automne éternel. Elles dégringolaient parmi les fils électriques, gaines, et autres cordons puis, avec grâce et précision atterrissaient sur le bureau de Mr KING, formant à l’extrémité de l'écritoire une pile brouillonne sur laquelle Méphisto veillait jalousement. De ses griffes effilées, diaprées, avec délicatesse, il saisissait alors chaque feuillet et le positionnait minutieusement à l'intérieur d'une pochette noire cartonnée. Des centaines se serraient déjà dans l'humble réceptacle.

« Plus vite Méphisto ! Il ne s'agirait pas de faire attendre nos fol-dingues lectrices et leur conjoint non moins déjanté. » Encourageait la voix piaulante.

Un feulement dément jaillit de la gorge du grippeminaud à l'injonction du maitre du lieu et le félin aux prunelles bordées de feu reprit son rituel avec une vélocité accrue : les feuilles semblaient posséder leur propre volonté et rejoignaient à un train d'enfer leur dernière demeure.

Le cliquetis des doigts sur les claviers devint assourdissant. Des lumières clignotantes, multicolores et unicolores agrémentaient la démonstration aux quatre coins du hangar projetant des ombres hagardes et repoussantes. De temps à autre, une fumée verdâtre s'élevait paresseusement au-dessus du bureau.

D'un coup, un cor retentit.

C'était un « Ahouuuououuo… » d'un autre temps. Un cor de guerre d'un autre âge, une plainte interminable expulsée d'un antique gosier de cuivre. « Ahououuouuuououou… »

Mr King cessa instantanément sa rédaction et tout l'assemblage, câbles et fixations compris se pencha dans un même élan vers le sol. Ses mains-tiges de robot déglingué saisirent le bac à litière de son compagnon, situé quelques encablures plus loin et le portèrent vers une énorme boite à ordures rouge et noire à l'effigie de la marque Coca Cola, gisant dans un des coins de l'entrepôt.

Celle-ci ouvrit soudain sa grande gueule avide et Mr King y déversa précautionneusement les déjections maronnasses enrobées de sable tapissant le bac.

La poubelle soupira d'aise, immonde et repue, tandis que le matou, exalté, virevoltait autour des membres d'acier démesurés avec force de miaulements appréciateurs.

« Méphisto, je suis ton esclave, ma parole. Pousse-toi ! » le tança la voix.

Mr KING repris place à son bureau aidé par la grue hydraulique qui se balançait en geignant mollement. Il agrippa au passage quelques feuillets fripés qu'il disposa au fond du bac à litière avec un sourire grotesque.

« Une fort belle litière que voilà. Litteraire dirai-je. » Il gloussa.

Sur les imprimés, l'on pouvait déchiffrer quelques fragments de phrases ;

« (…) Bien sûr, je comprendrais que vous ne me répondiez pas personnellement, vous devez avoir de nombreuses admiratrices, néanmoins, un autographe me comblerait de… »

et plus loin une autre page ;

« Vous êtes mon héros Mr King, je me demande d'où vous viennent toutes vos idées saugrenues, mais ne changez rien surt… »

Les mains articulées rapetassèrent l'ensemble au fond du bac, puis, vivement, ajoutèrent deux bonnes pelletées de sable bicolores par-dessus.

« Voilà mon ami, tu peux chier et pisser sur tout ce salmigondis de mots en décomposition. »

L'étrange Mr King se mit à rire, non à gorge déployée, car il n'en possédait point, mais à câbles détendus, pourrait-on dire. Et ce rire épouvantable secoua le serpentin tout là-haut, et le petit cerveau dans son bocal, telle une gelée de fruits.

Le félin survolté vérifia sa caisse – propre – flaira longuement, minauda, les moustaches frémissantes au-dessus des granules absorbants gris et blancs, sembla enfin satisfait et grimpa souplement sur la table de travail. Il se remit à l'ouvrage, dans un va-et-vient précis et véloce de coups de griffes allant du tas de documents noircis vers la pochette cartonnée tandis que Mr King barguignant et confus, clignotant de toutes parts, activait de nouveau furieusement les touches des claviers avec une rapidité frôlant la folie au gré de son inaltérable imagination.
