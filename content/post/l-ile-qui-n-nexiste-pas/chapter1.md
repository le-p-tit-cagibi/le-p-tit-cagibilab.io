Sacrebleu ! Qu'est-ce dont ?

D'aspect, je suppose un coffre maître.

Diantre ! Quel genre de prince a besoin d'un coffre d'un tel gigantisme ? Et quelle nef encore plus gigantesque serait à même de le transporter ? Quelle cliquetailles peu bien remplir un tel mastodonte ?

Jacques touche la paroi métallique de la pointe de son épée longue.

Ça m'a l'air solide !

Et lourd, maitre, regardez comme il s'enfonce profondément dans le sable du rivage !

As-tu déniché une ouverture ?

Pas encore, Foulques m'a accompagné ce matin comme nous nous apprêtions à méditer sur ces rochers, c'est alors que nous découvrîmes ce… ce coffre.

S'il possédait des roues, on le confondrait aisément avec un fourgon militaire, songe Jacques, dubitatif.

Geoffroy s'emporte, piqué par la curiosité de leur découverte.

Un fourgon ! Pour quelle route ? Vous n'y songez pas, il est par trop encombrant, maître Jacques.

Alors qu'est-ce donc ? Le maître en question prend le temps de la réflexion ; d’inoffensives vaguelettes dansent non loin de ses sandales. Il se décide.

Va quérir l'alchimiste, Geoffroy. Je suis certain que la nouvelle a déjà fait le tour de notre communauté.

Jacques fronce les sourcils, comme il sait le faire lorsqu'il est contrarié. Il rappelle son gonfanonier et sénéchal, qui s'éloigne déjà.

Geoffroy ? Enrôle Gauthier également, ils ne seront pas trop de deux bonnes têtes pour venir à bout de ce mystère.

Oui maitre, j'y cours.

Et il convoya jusqu'à la commanderie, Foulques sur ces talons.

Ce n'est pas deux mais six qui s'en viennent au bout d'un temps, peinant dans le sable humide.

Gérard l'alchimiste trotte au coté de Foulques, serrant son manteau autour de sa fine silhouette, lui-même précédé de Geoffroy, le gonfanon fièrement levé devant lui. Plus inattendu, Nicolas l'herboriste et Pensard, l'érudit suivent, devisant à forte voix pour se faire entendre par-dessus les rugissements de l'océan. L'un et l'autre, ont prudemment relevé leur longue pelisse, pour éviter qu'elles ne trempent dans l'eau salée et pataugent de leurs pieds nus, cernés d'écume. Gauthier, l'ingénieur, ferme la marche, une lourde sacoche pend à son épaule complétée d'une ceinture de cuir débordante d'outils de toutes sortes.

En voilà une procession ! Messieurs, la curiosité est un bien vilain défaut !

Telle est la tirade par laquelle les accueille malicieusement Jacques. On gagerait qu'il n'a pas bougé d'un pouce depuis le départ de Geoffroy.

Son tabard blanc bat à présent ses jambes avec un claquement sec sous une forte brise et la houle déchaîne les vagues qui, à leur tour, heurtent furieusement les parois du caisson métallique échoué devant eux.

Ne manque que Bernard, ajoute le maître, « Où diable est-il pour n'avoir point oins de sa bénédiction votre cortège ? »

Il prie, Monseigneur.

Ah ! Jacques n'en fait cas qu'un instant. « Eh bien soit, chacun est ainsi à la place qu'Il lui a assigné »

Toute la colonne acquiesce d'une remarquable unité.

Frères, auriez-vous l'habileté de faire rendre gorge à cette lourde cantine, là, qui semble vouloir nous narguer. Elle est plus fermée que les cuisses d'une vierge. Gauthier !

Oui maitre, je vais de ce pas m'approcher pour l'étudier.

Je t'accompagne !

Gérard est inhabituellement enthousiaste, son attitude semble réjouir l'ingénieur. Il guide son compagnon jusqu'à l'arrière du caisson. Les autres adoptent une certaine réserve, se tenant à distance.

Le vent repousse la capuche de Jacques, les pieds toujours fermement plantés dans le sable, et un visage juvénile apparaît, doux et grave, ses longs cheveux blonds volent dans le vent.

Crois tu que le temps se prête à ces calembredaines ? Questionne Gérard, précautionneux, toujours emmitouflé dans ses linges.

Le temps s'y prête ! Affirme Geoffroy.

Nul ne le conteste.

Il est en quelque sorte le boutre que chevauche, confiante, leur petite communauté. Avec lui, chacun sait naviguer au sec. Un havre.

L'homme, brun et barbu, est taillé comme un forgeron, de carrure et d'esprit. Un homme tourné vers le travail manuel, et qui ne dédaigne point la difficulté.

Tous s'affairent autour de l'objet naufragé, d'une manière ou d'une autre, et certains plus que d'autres.

Pensard revient de son étude trempé comme chien sous l'orage, sa tunique pend, flasque, mais son visage s'illumine dès qu'il rend compte des avancées.

Geoffroy et Gauthier ont mis à jour une porte ! Elle est pour l'heure à demie enfouie sous les eaux, maitre. Le reste de la boite, tout de métal, semble hermétique.

Point de réponse aujourd'hui à ce drôle de miracle, c'est cela ? Dit-il d'un ton acide. Lui-même, trempé par les embruns, crève d'ennui et de froid.

Les autres hochent la tête d'un air entendu. Tous pataugent à présent jusqu'aux hanches dans une mer de bouillons. Geoffroy et Gérard s'en reviennent penauds : L'océan commande aux hommes de s'éloigner.

Par tous les saints, il est temps d’avoyer ! Hurle Jacques contre le vent.

Si Geoffroy est est le boutre, Jacques est le capitaine, juché sur la poupe, tenant bon le gouvernail, maintenant le cap d'une poigne ferme.

(Bernard les assimile en secret au tenon et mortaise, dont l'un sans l'autre se révèle superfétatoire tandis que l'un avec l'autre relève du génie.)

Pensard, bas le rappel, commande Jacques, « nous reviendrons passé la brune, dès la prochaine marée basse.  »

Au castel de l'Umbre, le repas est l'occasion d'un débat comme on en a jamais vu jusqu'alors entre ces murs. Jacques, sérieux dans sa charge, se taraude du manque de discipline inaccoutumé, de l'air surchauffée, des mots qui s'envolent à travers les hauts murs de tous les bouts de table, mais lui-même requiert les avis de ses frères d'armes pour venir à bout de cette mystérieuse régurgitation de l'océan.

Doit-on y voir un signe ? Médite-t-il tout en écoutant ses hôtes avec attention.

Gérard envisage le recours au feu grégeois, idée vitement décriée par les autres.

Gauthier reste persuadé qu'il viendra à bout du mécanisme de la porte, après une étude approfondie. Foulques lève sa lourde carcasse du banc et s'en va secouer les tisons pour se donner bonne mesure. Il suit enthousiaste le train des conversations de ses yeux vairons hyper-mobiles, allant de l'un à l'autre, mais sans proposition aucune ; après tout, Foulques est un soldat, songe Jacques.

Nicolas dresse l'inventaire des outils à disposition. C'est une jeune garçon dynamique, dont le charisme naturel attire toutes les sympathies. Il hoche du bonnet plus que de raison, et les longues brides de sa cale gifle ses joues émaciées.

Geoffroy se rallie à la cause raisonnable, briser la clenche.

Chacun converse plus qu'à son tour dans un tohu-bohu guère en accord avec la solennité du lieu. Jacques s'évade. Le jubé lui fait de l’œil au fond de l’abside. Le chœur, où ils sont rassemblés, comme à l'accoutumée, les embrasse de ses deux hautes mais courtes travées tandis qu'au-dessus de leur tête, le clocher-forteresse, carré, massif, s'élance pourtant avec aisance à l’assaut du firmament. Il s'est réveillé là un matin, pour n'en plus repartir. La veille, brisé par son procès, il avait remis son âme à dieu, confiant en ses plans. Et au matin… sainte Cécile, l'unique, la merveilleuse, les recueillait en son sein de briques rouges, lui et ses bons amis, ses frères, au milieu de cette île qu'on aurait juré sortie tout droit de l’éden original.

Et depuis, le temps s'écoule pour eux sans hâte, tandis que sous le baldaquin ornemental de pierres sculptées jouxtant l'entrée de la cathédrale, huit gisants à leur effigie serrent leur épée de parade en croisant les mains sur leur poitrine.

Jacques bâillonne ces résurgences d'une autre ère et frappe vigoureusement du plat de sa main sur la large table de chêne.

Tudieu ! Un peu de calme, vous autres ! Qu'en penses-tu Bernard ?

Le prélat, assis en bout de table, pioche sans conviction un brin de victuaille au fond du bol qu'il partage avec Gauthier.

Pour parler franc, Jacques, est-ce raisonnable de s'esbigner sur ce coffre dont on ignore la provenance ?

Doutes-tu de qui nous l'envoie ?

Je n'en ai pas l'idée.

Les six autres protestent. Jacques impose le silence de son bâton. Il frappe le sol trois fois, et le silence s'instaure, vite brisé par un éclat.

Mais ce ne peut-être qu'une sorte de… écompense, ou un défi, à tout prendre ! Bernard, tu vois la malice partout, ce n'est pas raisonnable ni prudent ! A trop la réclamer…

Il suffit Gérard, modère tes paroles, mon frère, à défaut de tes pensées. La voix de Jacques claque comme une discipline sous la profondeur des voûtes. « Bernard nous rappelle à la raison. Il est d'une prudence qui n'a d'égal que ton intempérance ! Suffit ! Vous tous !»

Il parcourt la petite assemblée, le regard sévère, sous ses traits encore verts.

Il est l'heure de regagner nos cellules. Demain, Gauthier tentera de faire céder la porte et nous aviserons.

Dès l'aube, comme l'océan se retire vers l'horizon, tous se mettent en chemin. Pour l'occasion, quelques outils, réunis dans un tombereau que poussent l'ingénieur et Nicolas. Foulques, armé jusqu'aux dents, s'est proposé éclaireur, et cingle à pas de géant vers la plage, talonné par Geoffroy dont la bannière s'agite onctueusement dans le vent. Gauthier barguigne avec Pensard, les mots aussi s'agitent dans le vent, tandis que le maitre et le prélat, silencieux, forment l'arrière-garde.

Les huit ardent de se mettre à l'ouvrage et mènent bientôt rondement l'affaire. La matinée s'évapore, tout comme la brume. Et tous suent sous leurs affublements.

L'arrière de l’énorme caisson est à présent dégagé, et c'est bien là une porte devant laquelle il soufflent, se passant une gourde de vin clairet.

Est-ce une attrapoire ?

Non, j'en jurerai, protesta Geoffroy.

Bernard lui jeta un regard plein de reproches.

L'ingénieur poursuivit,

Regardez, cette barre de fer traverse là et là et encore là, et voyez, en bas, elle est scellée par un sceau.

Un sceau ! Jacques, interloqué, s'approcha tout près. Il détailla le sceau, rond et plat, le retourna « il est de gueules mais rien ne nous renseigne plus en avant sur son propriétaire… »

N'est ce point suffisant ! S'exclama Pensard, toujours enthousiaste.

Certes, j'accrois volontiers que vous êtes empressés de fourbir vos yeux de furet dans ce caisson emplies de promesses, tant d'années que nous sommes à flétrir sur cette île, sans y croiser mieux qu'un rongeur indigène ou prendre une chiure de ces foutres de Mouettes ! Mais… un peu de retenue Messieurs mes frères ! Le petit discours de Bernard fait son effet.

Tous se tournent vers Jacques, sur sa réserve. Ils les jauge tous. Il les aime tous, ses compagnons d'exil. Depuis si longtemps qu'ils languissent là, dans ce castel de l’Umbre comme ils le surnomment. Oh ! Rien ne leur fait défaut, ils vaquent à de saines occupations, et remplissent scrupuleusement les commandements de leur règle. Mais la coutumière habitude lamine la plus sincère des volontés ! Et ils s'étiolent, ici, prisonniers de cette vie routière. L'arrivée impromptue de cet objet extraordinaire, le premier à atteindre leurs rivages, provoque une émulsion peu commune chez ces hommes d'action, due à la grande lassitude de cette vie si bien réglée.

Jacques sait tout cela, il ne s’avère pas différent d'eux tous ! Avec gravité, il lève son bâton et prend la parole.

Gauthier, si tu sais forcer le sceau et briser les fers, exécutes-toi dans les délais les plus brefs. Je t’adjoins Nicolas, et Foulques vous couvrira, au cas où le mécanisme baillerait à mal.

Puis-je en être ? Questionna Gérard, une pointe d'amertume de sa voix flûtée.

Jacques soupire, l'alchimiste et son arrogance !

Soit, accompagne-les. Nous autres, en retrait ! Nous nous tiendrons prêts à répondre à toute velléité de la part du contenu, s'il en est !

Les trois s’avancent dans le sable spongieux, et quelques crabes fuient devant eux de leurs grandes pattes maladroites. Les hommes disparaissent à l'arrière du caisson tandis que le reste de la compagnie se positionnent alentour.

Il file peu de temps avant que Gauthier transborde de la charrette pelles, pioche, épieu, et même une vieille hallebarde. Des bruits de cognée résonnent dans l'immensité du ciel sans déranger mouettes et goélands qui les survolent en riant.

Nous y sommes ! Annonce Foulques.

Le sceau a cédé. Tous se rapprochent.

Le mécanisme d'ouverture en lui-même est simplissime, commente Gauthier satisfait.

Soulève cette barre-ci en premier, l'encourage Gérard d'un ton plus autoritaire qu'aucun ne lui reconnaît.

sois prudent, ajoute Nicolas, le plus sage de tous.

Une série de claquements se fait entendre, puis un lent grincement : la porte couine sur ses gonds.

Arrière ! Lance Jacques à la cantonade, en bondissant, le bouclier d'argent à la croix pattée de gueules dans la senestre, et l'épée longue sortie du fourreau au creux de sa dextre. Il frappe d'estoc dans l'ajour, et pénètre diligemment dans l'obscurité.

— premier un pas dans le grand caisson mystérieux —

Nicolas, prévoyant, introduit avec précaution une torche qu'il avait réservée, et passe le nez à l’intérieur, tandis que Foulques et Geoffroy, la bâtarde au coté, franchissent ensemble le seuil de la porte.

Pas âme qui vive ! On sent une vive déception dans cette simple constatation, mais Jacques, les yeux vite accoutumés à la pénombre, aperçoit un amoncellement d'objets hétéroclites.

Qu'est-ce fatras ?

De moyennes et petites boites cylindriques métalliques dans une grande boite rectangulaire métallique, Seigneur ! fulmine Gauthier, entérinant le désappointement du maître. Voilà un ensemble gigogne grandement étrange ? Un jeu ? Se moquerait-on de nous !

Pardieu ! Ne soyez pas si certain que quiconque se préoccupe de nous, mes frères, scande Bernard.

Foulques amorce bravement un pas en avant et, d'estoc, embroche un de ces nombreux cylindre répandus à même le sol du grand caisson de fer.

Fichée au bout de la pique, la boite émet alors un bruit de succion désagréable.

—pfffuiittt —

Foulques sursaute en constatant que sa lame goutte d'un liquide carmin tandis que Gauthier et Jacques sont fouettés au visage par une sensation liquide et acidulée. Ils se déploient immédiatement, cherchant l'origine de l'attaque. Les autres s'échauffent, comme lions en cage, puis battent en retraite, sauf le jardinier et son lumignon.

Tu es couvert de sang, Jacques !

Le maitre regarde le soldat lui-même enduit de pourpre.

Diablerie ! Clame Pensard, depuis le seuil.

Jacques, tête en arrière, part d'un long éclat de rire lugubre qui fait onduler sa longue crinière blonde.

Un comestible, énonce-t-il, ayant reprit son calme. Et il en étale la démonstration en se léchant le doigt qu'il avait rouge de cette matière étrange et collante.

Méfiez-vous ! Ce pourrait être un poison ! Susurre l'alchimiste, retenant le geste de son maître.

Bernard approche une boite intacte au plus près de ses yeux.

Une notice est collée sur chacun de ces cylindres !

Peux-tu décrypter cela ? L'enjoint Gauthier remit de sa surprise, ainsi que la compagnie.

Les lettres sont communes, mais le sens m'échappe ! Pensard ! Fais part de tes lumières érudites !

L'homme s'approche, parcourt l’étiquette en épelant chaque lettre :

S.A.U.C.E T.O.M.A.T.E.S R.U.B.I.S M.A.D.E I.N C.H.I.N.A

Bigre ! Des rubis ? S'interroge Gérard.

Écrasés ? Questionne le sénéchal.

Broyés ? Corrige Nicolas, pourvu d'une longue pratique du mortier et du pilon.

Finement mélangés à une substance liquide, mais dans quel but ? Se demande encore l'alchimiste, oublieux qu'il a usé de sa voix au lieu de sa pensée.

Dans quel but ? Jacques fait écho à la dernière des questions.

Quelles vertus donc ce rubis, frères ?

Chacun y va de son propre raisonnement. Les questions ricochent le long des parois du caisson et reviennent chatouiller les oreilles des compagnons, tous à présent peu ou prou à l'intérieur, serrés comme harengs en caque. Foulques s'acharne à ôter la petite boite intitulée

— S.A.U.C.E T.O.M.A.T.E.S R.U.B.I.S M.A.D.E I.N C.H.I.N.A —

de la pointe de son épée, où elle goutte encore de ce liquide sanglant.

Cela n'a aucun sens, résume Bernard, les bras croisés sur son torse, une moue d'incompréhension se dessinant sur sa face rubiconde.

Ici, mes frères ! La voix de Nicolas résonne étrangement.

Les autres s'enfoncent dans la grande caisse, auprès de l'herboriste, officialisé éclaireur au grand dame du soldat. Entassés là s'éparpillent d'autres petites boîtes encore, cubiques cette fois.

Encore une devinette ! Constate Foulques désabusé.

Jacques en embroche une, sans hésitation.

Jacques ! Le prélat lève les bras.

Il n'y a nulle malice ici ! Voyons, Bernard !

Vous voilà bien imprudent après une vie d’ascète ! Siffle Bernard, vexé.

Il fend le groupe, son physique l'aide assurément, s'avance et palpe cette boite d'un autre genre, faite d'une matière inconnue…

Une sorte de parchemin renforcé, décrète Bernard, intrigué par le contenant, qui pend au bout de l'épée de son maitre, réfléchissant à voix haute. « Regardez toutes ces couleurs, ces dessins… oilà une signalétique que l'on peut décrypter… ne…

Main !oui Diantre ! Pas difficile celui-là ! Se vante Foulques, satisfait de sa répartie. Bernard en reste le bec cloué.

Pensard déchiffre studieusement :

— G.A.N.T.S A U.S.A.G.E U.N.I.Q.U.E M.A.D.E I.N C.H.I.N.A —

Quelle est donc cette langue ? L'alphabet est commun, mais… Marmonne Bernard, revenu de sa surprise.

Il y a une fente ! Là! Dessus la boîte ! Et quelque chose semble vouloir en sortir ! Indique Geoffroy du bout de la pique de son gonfanon.

Tire ! Commande Jacques qui tient toujours la boite au bout de son épée, tandis que le prélat se recule.

Geoffroy hésite. Les autres campent sur leurs positions sans manifester de hâte à exécuter l'ordre du maître.

Tire donc ! Rugit celui-ci. Le ton ne prête à aucune alternative.

C'est Gauthier qui tire d'un coup sec, trop sec et de toute sa force, et voilà le contenu de la boite répandu à terre ! Une multitude de petits gants bleus.

Cela ressemble aux méduses jonchant les plages à l'automne, résume avec ironie Jacques se saisissant d'un exemplaire.

Maudites méduses et piquants venimeux ! Éructe l'alchimiste, chargé tous les ans d’ôter les picots des imprudents promeneurs puisqu'il fait, sur l’île, office de guérisseur.

Diantre ! Des gants de peau bleus ! Voilà qui surprend l'herboriste.

Drôle de peau, le corrige Gauthier en froissant un de ces objet entre ses mains, on dirait… n dirait…

Rien de connu, le coupe l'alchimiste.

Voyez comme il s'étire, poursuit l'ingénieur, sans prendre garde au commentaire de Gérard.

Jacques s'échine à en enfiler un, sans succès.

Qui possède des mains aussi fines ? Se plaint-il en regardant ses frères.

Des gants de Donzelle, assurément, conclue Bernard, dépité.

Le maître tourne l'objet dans ses doigts, l'examine une dernière fois, le triture encore, puis le jette à terre.

Mous, fins et ridiculement petits, je ne conçois l'utilisation d'une telle vêture. Ces découvertes restent bien intrigantes !

Mais ses condisciples l'écoutent à peine, et se pressent déjà à l'extremité du caisson, cernant une dernière curiosité, de taille plus imposante.

Un cheval de fer ! Voyez là ! La selle ! Hypothèque Pensard, ayant recouvrer ses esprits et un peu de courage.

Drôle de cheval allons ! sans tête ! Jacques effleure l'insolite destrier de son poignard de poche, dans cet espace plus réduit.

Et sans patte, ajoute Bernard. Même Helhestr en possède quelques unes… ironise-t-il pour lui-même.

Et ceci ? Ce ressemble à des poignées accolé à ses flancs, détaille encore l'érudit.

Le poignard applique une légère poussée sur l'objet désigné qui se met à tourner.

Ah ! Quel singulier mécanisme, Gauthier s'approche et manipule le pédalier, sur lequel la chaîne cliquette.

Soyez prudent ! Réitère l'alchimiste alarmé par leur hardiesse.

Bernard s'échine à son tour sur une nouvelle étiquette.

— V.E.L.O D.'.A.P.P.A.R.T.E.M.E.N.T M.A.D.E. I.N C.H.I.N.A —

Encore ce C.H.I.N.A ! Cette langue est familière, mais le sens m'échappe totalement, soupire Bernard bougon. « Le seigneur nous met à l'épreuve ! Ce mot C.H.I.N.A, sa récurrence… serait-ce le titre du maître d’œuvre ? Ou du commanditaire ?»

Ton prochain sujet d'étude, mon frère, dit Jacques en lui tapant sur l'épaule.

Plus pragmatique, Geoffroy s'est éloigné du groupe, et passe la tête à hors du caisson puis revient faire son compte rendu, l'air sombre.

La marée reflue, il faut se hâter Messeigneurs !

Que fait-on de tout ceci ? Le sénéchal montre l'empilement d'objets hétéroclites.

Il faut partir, presse Foulques, « l'eau vient »

Et fait et dit, des vaguelettes lèchent la porte du caisson.

Emportons quelques exemplaires, nous méditerons là-dessus au castel d'Umbr ! Allons !

Jacques fait volte face et sort sans rien emporter. Il est fort déçu.

Qu'était ce là ? Une farce ? Le Seigneur se jouait d'eux depuis leur exil ? Dans quel but ?

Quel sens enjoindre à tout cela ?

Nicolas et Gauthier, nullement découragés charrient — V.E.L.O D.'.A.P.P.A.R.T.E.M.E.N.T M.A.D.E I.N C.H.I.N.A — à l'extérieur et l'installent sur la carriole à bras. Pensard se saisit de quelques — G.A.N.T A U.S.A.G.E U.N.I.Q.U.E M.A.D.E I.N C.H.I.N.A — et enfin, Foulques entreprend de jeter dans le tombereau une brassée de — B.O.I.T.E D.E S.A.U.C.E T.O.M.A.T.E.S R.U.B.I.S M.A.D.E I.N C.H.I.N.A—

Tous cheminent sur la crête de sable tête basse et dos courbé par le poids de leur questionnement.

La mer reprend ses aises comme ils s'éloignent et pénètre dans le caisson abandonné.

Il n'en restera plus grand-chose demain, la marée risque de tout emporter, confie Geoffroy à son maître tout en pataugeant.

Bah, et quand bien même ? S'insurge Jacques ! Qu'avons nous à attendre de tout ceci ? Il tend la main en direction du chariot brinquebalant les étrangetés.

Cette aventure n'a guère de sens, convient Bernard soucieux.

Une épreuve peut-être ? Interroge Jacques, fixant le prélat de manière appuyée.

Dont le but m 'échappe, maitre, répond Bernard en lui rendant son regard.

Les huit réintègrent le bercail.

Une salle inoccupée, au fond de la cathedra, est, d'un commun accord, dédiée à leurs mystérieuses trouvailles.

— V.E.L.O D.'.A.P.P.A.R.T.E.M.E.N.T M.A.D.E I.N C.H.I.N.A —

— G.A.N.T A U.S.A.G.E U.N.I.Q.U.E M.A.D.E I.N C.H.I.N.A —

— B.O.I.T.E D.E S.A.U.C.E T.O.M.A.T.E.S R.U.B.I.S M.A.D.E I.N C.H.I.N.A—

sont étudiées minutieusement.

Gauthier enfourche finalement le premier, se triturant les méninges pour comprendre l'utilité d'un cheval de fer privé de mouvement, (il a été retourné et gît à présent sur son empattement) mais cela semble dépasser son entendement, tandis que Gérard usant gant après gant, constate avec effroi qu'ils ne protègent de rien qui fut connu À quoi un tel objet peut-il bien servir ? Il renonce à pousser l'expertise. Nicolas, enfin, responsable de la cuisine, le plus jeune et le plus hardi sans doute, entreprend de mijoter un lièvre à base de cette mixture à la couleur du sang dont tous les frères se méfient, hormis le maître. Ils s'en régalent tous deux au premier jour gras, pendant que les six, après avoir reniflé le plat avec suspicion, croquent dans leur pain sec, sans prélever une louche de cette mixture étrangère.

Pourquoi diable enfermer de la nourriture dans une boite en fer et la jeter à la mer ? Pensard retourne la question dans tous les sens, mais la réponse se fait attendre !

Bernard, troublé, abandonne un temps ses compagnons à leurs extrapolations et médite longuement dans la crypte, sous le chœur du castel Sainte Cécile.

Il tire au bout du compte la conclusion qui s'impose et la partage avec ses frères, le troisième jour.

C'est une récompense divine, affirme-t-il, « mais en pêcheur que nous sommes, nous ne possédons pas les clefs de la compréhension de leur signification entière.» Le prélat est d'humeur sombre mais à son grand soulagement, ses compagnons plussoient à son raisonnement.

Jacques, hoche la tête, heureux du dénouement. Il est grand, et beau, songe Bernard en l'observant à la dérobée. Le maître a revêtu sa tenue d'apparat pour l'occasion ; un jupon d'arme blanc recouvrant le haubert, lui-même sur un gambison de laine, une calotte protégeant son cou du heaume rigide et une paire de chausses de cuir gainant ses longues jambes nerveuses lui confèrent belle allure. Il a cependant renoncé aux plates et opté pour de la maille, fine et brillante. Des souliers d'amer complètent l'ensemble. Sa grande cape immaculée, à la croix pattée de gueules resplendit sur ses épaules bien découplées. Il lève la main, et s'adresse à sa communauté, solennel.

Sanctuarisons le coffre et son lieu d’échouage, qu'il nous rappelle notre devise et nos lois, dont l 'humilité est la règle : Peut-être notre Père à tous adresse-t-il à ses fidèles un avertissement : Soyons prêts !

Soyons prêts, acquiesce Bernard, — un jour, nous comprendrons tout ! — ajoute-t-il pour lui-même. Il serre ses mains sous ses robes et se recueille avec ferveur.

Les autres reprennent en chœur,

Soyons prêts !

Gérard, un brin subversif, comme à son habitude, lève sa coupe d'eau claire, et lance un toast.

À notre maître éclairé, puisse-t-il vivre et nous conduire encore mille ans !

et les autres entonnent à l'unisson,

À notre maître, Jacques de Mollay, amen !

Geoffroy sourit, et agite la bannière qui ne le quitte guère, arborant écartelé, au 1 et 4 d'argent à la croix pattée de gueules et au 2 et 3 d'azur à la bande d'or, au-dessus de l'assemblée.

Bernard de son tabouret, marmonne « Dieu pardonnera à ses soldats du christ. Peut-être ces objets échoués sur la plage trouveront leur utilité lors d'une prochaine croisade ? » Il sonde son cœur. « Assurément, assurément […] »

Nicolas le poussa légèrement du coude, grisé par la déclaration enflammée de son maître.

Allez Bernard, ne soyez pas rabat joie, une graine est plantée, c'est l'espoir, mon frère.

Au sud de l'île, au-dessus de l'océan, un avion de reconnaissance, un Falcon 50, de la marine nationale entame sa dernière ronde. Trois opérateurs planchent sur les cartes à la recherche d'un navire dérouté, potentiellement abîmé en mer d'Oman. Deux pilotes aguerris tiennent les commandes.

Une mission de routine.

Ici Tango 0102 répondez ! over !

Roger ! Ici Foxtrot 2323 over !

Roger ! Nous approchons de… latitude et longitude (l'énumération de chiffres se perd dans un brouhaha de carlingue). Une île se profile à l'horizon. Over !

Roger ! Répétez Tango 0102 ! over.

Roger ! Je répète : Nous abordons l’île située aux coordonnées… (se perdent encore ces mesures barbares dans le vrombissement des 3 réacteurs Garett TFE 731-3-1C.) Over !

Roger ! Vous faites erreur Tango 0102. Vérifie ! Over !

Roger, J'ai présentement l’île sous les yeux. Je confirme Foxtrot 2323 ! Ove !

Un des opérateurs valide en levant le pouce, le nez calé sur ses radars. Le pilote entame un virage un peu sévère et s'approche de la côte.

Son copilote gigote visiblement perturbé.

Hé ! Y a des ruines là-dessous !

Hein ?

Putain regarde toi-même, Jake, Y a une espèce de château ? Un truc comme ça ! Putain regarde !

Le pilote baisse le nez de son coucou et passe en raz motte, si l'on peut dire, car avec les vents capricieux, il n'ose cependant pas descendre trop bas. Un filet de brume pernicieux gêne le repérage, mais il est cependant évident qu'il y a au moins un édifice de pierre quelques 4 000 mètres plus bas sous leurs pieds.

L'opérateur à l'arrière pianote, frénétique, sur son clavier.

Roger, nous entamons la procédure de vérification de notre côté. Revenons à nos moutons, des traces du porte-containers — ELRONDE — ? Over !

Le pilote se retourne, et interroge du regard ses trois opérateurs.

Roger ! Aucune trace Foxtrot 2323. Ove !

Foxtrot 2323 insiste. (Grésillements.)

Roger ! Aucun container flottant dans les environs non plu ? Over !

Roger ! Négatif, Foxtrot 2323. Over !

Fox trot 2323 en perd son fair-play légendaire.

Merde !over !

L'avion a pendant ce temps fini son piqué et le pilote distingue clairement un homme vêtu de blanc sur le faîte de la tour de ce qui semble un château fort en mauvais état.

Mais qu’est-ce que c'est que ces conneries ? Braille le pilote.

Il regarde son copilote sidéré À l'arrière, les opérateurs se tordent le cou pour jeter un œil par l'unique hublot disponible.

Et l'avion remonte, malmené dans les courants d'air, et s'éloigne un instant, tournant le dos à l’île.

Roger, pour votre information Tango 0102, je confirme qu'il n'y a rien de répertorié aux coordonnées…(la carlingue vibre trop pour qu'on comprenne quelque chose.) Je répète, il n'existe pas de terre immergée à des milliers de km à la ronde Tango 0102 confirmé ! Ove !

Le pilote se marre, sans répondre.

\*\*Sans déconner Foxtrot 2323!\*\* pense-t-il au comble de l’énervement. On va voir ce qu'on va voir !

Et il vire aussi sec pour faire un second passage au-dessus de l’île.

Silence radio.

Le Falcon réalise un virage impeccable et leur offre une vue imprenable :

L'immensité de l'océan miroitant de reflets cobalt et… rien d'autre. Que de l'eau ! L'île a disparu !

Le pilote bégaie et son copilote avec lui.

Il jure se permettant de cligner et d'écarquiller les yeux. Mais rien n'y fait !

L'océan est vide ! Totalement vide !

Roger ! Tango 0102, je confirme il n'y a pas d’île et pas de porte-containers non plus ! Over ! ! admet enfin Tango à contre-cœur.

Roger ! Faut arrêtez la bière à gogo les gars hein ! Une île ! Over !

—Warf Warf Warf —

et les deux aviateurs se regardent déconfits, entendant les rires gras de leur collègues de la tour de contrôle dans la radio.

Les voies de Dieu sont impénétrables, dirait Bernard sentencieux.

Et Jacques approuverait sûrement.

Si l'île existait.

Notes

convoyer = aller

avoyer = partir

la brune = la nuit

la cale = le bonnet

arder = brûler

un affublement = un vêtement

une attrapoire = un piège

bailler à mal = avoir de mauvaises intentions

Helhestr = cheval de légende nordique à trois pattes

la cliquetaille = l'argent

l'ajour = l'ouverture

QDC =&gt; Mirage ? Miracle ? A quoi allez-vous croire ?
