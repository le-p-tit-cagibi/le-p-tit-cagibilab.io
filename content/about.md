+++
title = "À propos"
date = "2022-04-09"
author = "SB"
+++

"Asservir une fiction au contrôle de l'auteur, c'est de la construction narrative. mais si vous attachez votre ceinture, et que vous laissez l'intrigue prendre les rênes, alors, et seulement alors, vous ***raconterez*** une histoire. Et raconter des histoires est un processus aussi naturel que respirer ; La construction narrative, n'est, par analogie, ni plus ni moins qu'un poumon artificiel." S. King

C'est par cette petite citation que je vous accueille, j'essaierai de suivre le conseil simplissime et au combien difficile à mettre en œuvre, de ce fascinant auteur. Il a commencé sa carrière en rédigeant des nouvelles.

J'en posterai régulièrement, toutes de mon cru, en vous souhaitant bonne lecture !

