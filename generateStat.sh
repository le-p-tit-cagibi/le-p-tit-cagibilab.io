#!/bin/bash

for d in content/post/*/ ; do

  notdraft=$(ag --stats-only -Q "draft: false" $d/header.txt | head -n 1 | grep -oE '[0-9]+')
  fautes=$(ag --stats-only -Q "> Suggestions" $d | head -n 1 | grep -oE '[0-9]+')
  litiges=$(ag --stats-only '^.*°+.*$' $d | head -n 1 | grep -oE '[0-9]+')


  if [ "$notdraft" -gt 0 ]
  then
   echo "Deja publié $d : ("$fautes")"
  else
    if [ -e $d/chapter1.md ]
    then
      echo Fautes restantes dans $d ":" $fautes
      echo Litiges restants dans $d ":" $litiges
    else
      echo "PAS de chapitre a corriger"
    fi
  fi
done
