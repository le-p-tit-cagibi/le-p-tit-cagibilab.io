#!/bin/bash

for d in content/post/*/ ; do
    if [ -e $d/chapter1.md ] && [ -e $d/title.txt ] && [ -e ressources/footer.md ]
    then
      pandoc --pdf-engine=xelatex -V mainfont="Liberation Serif" -o $d/livre.epub $d/title.txt $d/chapter1.md ressources/footer.md
      git add $d/livre.epub
      git commit -m "MAJ Epub $d"
    else
      echo "Generation Epub echouee: fichier manquant"
    fi
done
