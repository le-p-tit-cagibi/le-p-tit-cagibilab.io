#!/bin/bash

for d in content/post/*/ ; do
    if [ ! -f $d/chapter1.md ] && [ -f $d/*.odt]
    then
      echo "Ajout MarkDown $d"
      pandoc $d/*.odt --wrap preserve -t markdown_strict -o $d/chapter1.md
      git add $d/chapter1.md
      git commit -m "Generation du fichier MarkDown $d"
    fi
done
