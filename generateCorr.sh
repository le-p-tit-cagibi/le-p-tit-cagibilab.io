#!/bin/bash

for d in content/post/*/ ; do

  notdraft=$(ag --stats-only -Q "draft: false" $d/header.txt | head -n 1 | grep -oE '[0-9]+')

  if [ "$notdraft" -gt 0 ]
  then
    echo "Deja publié $d"
  else
    if [ -e $d/chapter1.md ]
    then
      echo "MAJ corrections $d"
      grammalecte-cli -off apos -ff $d/chapter1.md
      git add $d/chapter1.res.txt
      git commit -m "MAJ corrections $d"
    else
      echo "PAS de chapitre a corriger"
    fi
  fi
done

git push
