## Etat des publications

- :checkered_flag: 26-secondes
- :tools: 20000-lieux-sous-la-terre
- :checkered_flag: amour-du-mercredi
- :tools: au-commencement
- :checkered_flag: conte-d-un-jour-particuler
- :checkered_flag: deambulation
- :checkered_flag: fantome-du-passe
- :checkered_flag: faux-depart
- :checkered_flag: fin-de-parcours
- :checkered_flag: fourmiland
- :checkered_flag: hommage-a-mr-king
- :checkered_flag: johnny
- :tools: journal-de-guerre
- :tools: la-citerne
- :checkered_flag: l-alambic
- :checkered_flag: la-legende-de-nemen
- :checkered_flag: la-mort-du-loup-garou
- :checkered_flag: la-souche
- :tools: le-banc
- :tools: le-barometre
- :tools: le-bus-de-l-horreur
- :tools: le-canal
- :checkered_flag: le-concert
- :tools: le-fabricant-de-cercueils
- :tools: le-lac
- :tools: le-marchand-de-peaux-de-lapins
- :tools: le-papier-peint
- :tools: les-chats
- :checkered_flag: les-invisibles-service-public
- :tools: les-rails-de-la-destinee
- :tools: le-vieil-homme-et-le-puits
- :tools: l-homme-oiseau
- :tools: l-ile-qui-n-nexiste-pas
- :tools: l-oasis
- :tools: pandemie
- :tools: randonnee
- :tools: rencontre
- :checkered_flag: repas-de-famille
- :tools: satan-le-coq-qui-portait-trop-bien-son-nom
- :tools: telepathie-de-supermarche
- :tools: trafic
- :tools: train-de-nuit
- :checkered_flag: un-conte-de-noel
- :tools: vous-avez-un-message

## Statistiques du restant à faire :
1476 suggestions de corrections sur 43 fichiers.

## Contenu des sous-dossiers de post :

* *.odt (fichier original)
* chapter1.md (chapitre(s) du livre)
* chapter1.res.txt (fichier de corrections potentielles grammalecte)
* header.txt (header de publication pour script)
* index.md (fichier résultant pour publication par Hugo)
* livre.epub et livre.pdf (fichiers de publication téléchargeables)
* title.txt (fichier de titre pour publications électroniques epub et pdf)

## Contenu de ressources :
* footer.md (fichier contenant le logo pour publications électroniques)
* logo-circle.png (logo)

## Traitements
Le script generate.sh execute les mises à jour successives des dossiers de post uniquement si il y a un fichier chapter1.md dedans.

### Pré-requis (installation)
```
apt-get install pandoc texlive-latex-base texlive-latex-recommended grammalecte-cli hunspell hunspell-fr
```
### Générer fichier markdown (odt vers md)
```
pandoc  fichier_in.odt --wrap preserve -t markdown_strict -o chapter1.md
```
### Générer fichier des corrections (sans traitement des apostrophes)
```
grammalecte-cli -off apos -ff chapter1.md
```
### Correction interactive avec Hunspell
```
hunspell -d fr_FR chapter1.md
```

