#!/bin/bash

ag -l "> Suggestions" ./content/post/ > suggest.txt
ag -l "draft: true" ./content/post/ > draft.txt

plop=""

sed -i 's/chapter1.res.txt//' suggest.txt
sed -i 's/header.txt//' draft.txt
sed -i 's/index.md//' draft.txt

#grep -Fxf suggest.txt draft.txt

while read -r line; do

  matches=$(ag --stats-only "> Suggestions" $line | head -n 1 | grep -oE '[0-9]+') 

  if grep -Fxq "$line" draft.txt
  then
    plop+="$line"chapter1.md" "
  fi

  echo " $matches "fautes dans "$line"chapter1.md

done < "suggest.txt"

#rm -f suggest.txt draft.txt

#geany $plop
